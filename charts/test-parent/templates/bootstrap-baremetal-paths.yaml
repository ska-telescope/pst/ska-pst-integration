{{- if .Values.bootstrapMount.enabled -}}
{{- $primaryVolumeMount := .Values.bootstrapMount.primaryVolumeMount -}}
{{- $pst := index .Values "ska-pst" }}
---
# Job
apiVersion: batch/v1
kind: Job
metadata:
  name: {{ .Release.Namespace }}-{{ .Release.Name }}-{{ $primaryVolumeMount.name }}-bootstrap
  labels:
    app: ska-pst-test-parent-bootstrap
    helm.sh/chart: {{ .Chart.Name }}
    app.kubernetes.io/version: {{ .Chart.AppVersion | quote }}
    app.kubernetes.io/managed-by: {{ .Release.Service | quote }}
    app.kubernetes.io/name: ska-pst-test-parent
    app.kubernetes.io/instance: {{ .Release.Name }}
spec:
  backoffLimit: {{ .Values.jobBackoffLimit | default 1 }}
  template:
    metadata:
      labels:
        app: ska-pst-test-parent-bootstrap
        helm.sh/chart: {{ .Chart.Name }}
        app.kubernetes.io/version: {{ .Chart.AppVersion | quote }}
        app.kubernetes.io/managed-by: {{ .Release.Service | quote }}
        app.kubernetes.io/name: ska-pst-test-parent
        app.kubernetes.io/instance: {{ .Release.Name }}
    spec:
      restartPolicy: Never
      {{- with .Values.imagePullSecrets }}
      imagePullSecrets:
        {{- toYaml . | nindent 10 }}
      {{- end }}
      {{- with $pst.core.nodeSelector }}
      nodeSelector:
        {{- toYaml . | nindent 8 }}
      {{- end }}
      affinity:
        nodeAffinity:
          requiredDuringSchedulingIgnoredDuringExecution:
            nodeSelectorTerms: {{- toYaml $pst.core.nodeSelectorTerms | nindent 14 }}
      {{- with $pst.core.affinity }}
        {{- toYaml . | nindent 8 }}
      {{- end }}
      {{- with $pst.tolerations }}
      tolerations:
        {{- toYaml . | nindent 8 }}
      {{- end }}
      securityContext:
        {{ toYaml $pst.core.securityContext | nindent 8 }}
      containers:
        - name: bootstrap
          securityContext:
            {{ toYaml $pst.core.securityContext | nindent 12 }}
          image: "alpine:latest"
          imagePullPolicy: {{ .Values.image.pullPolicy }}
          command: ["sh"]
          tty: true
          args:
            - "-c"
            - mkdir -p {{ $pst.core.primaryVolumeMount.hostPath }} && chown -R {{ $pst.securityContext.runAsUser }}:{{ $pst.securityContext.runAsGroup }} {{ $pst.core.primaryVolumeMount.hostPath }}
          envFrom:
            - configMapRef:
                name: ska-pst-core-paths
          resources:
            {{- toYaml .Values.resources | nindent 14 }}
          volumeMounts:
            - name: baremetal-base-path
              mountPath: {{ $primaryVolumeMount.baseHostPath }}
      volumes:
        - name: baremetal-base-path
          persistentVolumeClaim:
            claimName: {{ lower $primaryVolumeMount.name }}-pvc
---
# PVC
apiVersion: v1
kind: PersistentVolumeClaim
metadata:
  name: {{ lower $primaryVolumeMount.name }}-pvc
  labels:
    app: ska-pst-test-parent-bootstrap
    helm.sh/chart: {{ .Chart.Name }}
    app.kubernetes.io/version: {{ .Chart.AppVersion | quote }}
    app.kubernetes.io/managed-by: {{ .Release.Service | quote }}
    app.kubernetes.io/name: ska-pst-test-parent
    app.kubernetes.io/instance: {{ .Release.Name }}
spec:
  storageClassName: local-storage
  volumeName: {{ .Release.Namespace }}-{{ .Release.Name }}-{{ $primaryVolumeMount.name }}-pv
  accessModes:
    - ReadWriteMany
  volumeMode: Filesystem
  resources:
    requests:
      storage: {{ $primaryVolumeMount.size }}
---
# PV
apiVersion: v1
kind: PersistentVolume
metadata:
  name: {{ .Release.Namespace }}-{{ .Release.Name }}-{{ $primaryVolumeMount.name }}-pv
  labels:
    app: ska-pst-test-parent-bootstrap
    helm.sh/chart: {{ .Chart.Name }}
    app.kubernetes.io/version: {{ .Chart.AppVersion | quote }}
    app.kubernetes.io/managed-by: {{ .Release.Service | quote }}
    app.kubernetes.io/name: ska-pst-test-parent
    app.kubernetes.io/instance: {{ .Release.Name }}
  namespace: {{ .Release.Namespace }}
spec:
  accessModes:
    - ReadWriteMany
  capacity:
    storage: {{ $primaryVolumeMount.size }}
  local:
    path: {{ $primaryVolumeMount.baseHostPath }}
  nodeAffinity:
    required:
      nodeSelectorTerms: {{ toYaml $pst.core.nodeSelectorTerms | nindent 8 }}
  persistentVolumeReclaimPolicy: Delete
  volumeMode: Filesystem
  storageClassName: local-storage
{{- end }}
