{{- if .Values.testutils.enabled -}}
{{- $primaryVolumeMount := index .Values "ska-pst" "core" "primaryVolumeMount" -}}
{{- $pstSDP := index .Values "ska-pst" "sdp" -}}
apiVersion: apps/v1
kind: Deployment
metadata:
  name: pst-testutils
  labels:
    app: ska-pst-test-parent
    helm.sh/chart: {{ .Chart.Name }}
    app.kubernetes.io/version: {{ .Chart.AppVersion | quote }}
    app.kubernetes.io/managed-by: {{ .Release.Service | quote }}
    app.kubernetes.io/name: ska-pst-test-parent
    app.kubernetes.io/instance: {{ .Release.Name }}
spec:
  replicas: 1
  selector:
    matchLabels:
      app: ska-pst-test-parent
      app.kubernetes.io/name: ska-pst-test-parent
      app.kubernetes.io/instance: {{ .Release.Name }}
  template:
    metadata:
      {{- with .Values.podAnnotations }}
      annotations:
        {{- toYaml . | nindent 8 }}
      {{- end }}
      labels:
        app: ska-pst-test-parent
        app.kubernetes.io/name: ska-pst-test-parent
        app.kubernetes.io/instance: {{ .Release.Name }}
    spec:
      {{- with .Values.imagePullSecrets }}
      imagePullSecrets:
        {{- toYaml . | nindent 8 }}
      {{- end }}
      securityContext:
        {{- toYaml .Values.podSecurityContext | nindent 8 }}
      containers:
        - name: testutils
          securityContext:
            {{- toYaml .Values.securityContext | nindent 12 }}
          image: "registry.gitlab.com/ska-telescope/pst/ska-pst/ska-pst-test-support:{{ .Values.image.tag | default .Chart.AppVersion }}"
          imagePullPolicy: {{ .Values.image.pullPolicy }}
          command: ["bash"]
          tty: true
          env:
            - name: TANGO_HOST
              value: {{ .Values.global.tango_host }}
          resources:
            {{- toYaml .Values.resources | nindent 12 }}
          volumeMounts:
          # ska-pst-core storage
            - name: ska-pst-lfs
              mountPath: {{ $primaryVolumeMount.containerPath }}
          {{- range $index, $volume_configuration := .Values.extraVolumeMounts }}
            - name: {{ lower $volume_configuration.name }}
              mountPath: {{ $volume_configuration.containerPath }}
          {{- end }}
          # sdp shared storage
            - name: sdp-shared-storage
              mountPath: {{ $pstSDP.containerPath }}
      {{- with .Values.nodeSelector }}
      nodeSelector:
        {{- toYaml . | nindent 8 }}
      {{- end }}
      {{- with .Values.affinity }}
      affinity:
        podAffinity:
          requiredDuringSchedulingIgnoredDuringExecution:
          - labelSelector:
              matchExpressions:
              - key: app
                operator: In
                values:
                - ska-pst-core
            topologyKey: kubernetes.io/hostname
      {{- end }}
      {{- with .Values.tolerations }}
      tolerations:
        {{- toYaml . | nindent 8 }}
      {{- end }}
      volumes:
      # ska-pst-core storage
        - name: ska-pst-lfs
          {{- if eq $primaryVolumeMount.type "emptyDir" }}
          emptyDir:
            sizeLimit: {{ $primaryVolumeMount.size }}
          {{- else }}
          persistentVolumeClaim:
            claimName: {{ lower $primaryVolumeMount.name }}-pvc
          {{- end }}
      {{- range $index, $volume_configuration := .Values.extraVolumeMounts }}
        - name: {{ lower $volume_configuration.name }}
          {{- if eq $volume_configuration.type "emptyDir" }}
          emptyDir:
            sizeLimit: {{ $volume_configuration.size }}
          {{- else }}
          persistentVolumeClaim:
            claimName: {{ lower $volume_configuration.name }}-pvc
          {{- end }}
      {{- end }}
      # sdp shared storage
        - name: sdp-shared-storage
          persistentVolumeClaim:
            claimName: {{ printf "%s" (index .Values.global "data-product-pvc-name") }}
{{- end }}
