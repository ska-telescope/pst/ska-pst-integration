{{/*
Expand the name of the chart.
*/}}
{{- define "ska-pst.name" -}}
{{- default .Chart.Name .Values.nameOverride | trunc 63 | trimSuffix "-" }}
{{- end }}

{{/*
Create a default fully qualified app name.
We truncate at 63 chars because some Kubernetes name fields are limited to this (by the DNS naming spec).
If release name contains chart name it will be used as a full name.
*/}}
{{- define "ska-pst.fullname" -}}
{{- if .Values.fullnameOverride }}
{{- .Values.fullnameOverride | trunc 63 | trimSuffix "-" }}
{{- else }}
{{- $name := default .Chart.Name .Values.nameOverride }}
{{- if contains $name .Release.Name }}
{{- .Release.Name | trunc 63 | trimSuffix "-" }}
{{- else }}
{{- printf "%s-%s" .Release.Name $name | trunc 63 | trimSuffix "-" }}
{{- end }}
{{- end }}
{{- end }}

{{/*
Create chart name and version as used by the chart label.
*/}}
{{- define "ska-pst.chart" -}}
{{- printf "%s-%s" .Chart.Name .Chart.Version | replace "+" "_" | trunc 63 | trimSuffix "-" }}
{{- end }}

{{/*
Common labels
*/}}
{{- define "ska-pst.labels" -}}
helm.sh/chart: {{ include "ska-pst.chart" . }}
{{ include "ska-pst.selectorLabels" . }}
{{- if .Chart.AppVersion }}
app.kubernetes.io/version: {{ .Chart.AppVersion | quote }}
{{- end }}
app.kubernetes.io/managed-by: {{ .Release.Service }}
{{- end }}

{{/*
Selector labels
*/}}
{{- define "ska-pst.selectorLabels" -}}
app.kubernetes.io/name: {{ include "ska-pst.name" . }}
app.kubernetes.io/instance: {{ .Release.Name }}
{{- end }}

{{/*
Create the name of the service account to use
*/}}
{{- define "ska-pst.serviceAccountName" -}}
{{- if .Values.serviceAccount.create }}
{{- default (include "ska-pst.fullname" .) .Values.serviceAccount.name }}
{{- else }}
{{- default "default" .Values.serviceAccount.name }}
{{- end }}
{{- end }}

{{/*
CORE
*/}}
{{- define "ska-pst-core.labels" -}}
{{ include "ska-pst.labels" . }}
{{ include "ska-pst-core.selectorLabels" . }}
{{- end }}

{{/*
PST Facility
*/}}
{{- define "pstFacility" }}
  {{- $telescope := coalesce .Values.telescope "ska-low" }}
  {{- if eq (lower $telescope) "ska-mid"  }}
    {{- printf "Mid"}}
  {{- else }}
    {{- printf "Low"}}
  {{- end }}
{{- end }}

{{/*
CORE Selector labels
*/}}
{{- define "ska-pst-core.selectorLabels" -}}
app: ska-pst-core
{{- end }}

{{/*
CORE FQDN
printf "%s-ska-pst-core.%s.svc.%s" .Release.Name .Release.Namespace .Values.global.cluster_domain
*/}}
{{- define "pstCoreFQDN" }}
  {{- if and (.Values.global) (index .Values.global "cluster_domain") }}
    {{- printf "%s-ska-pst-core.%s.svc.%s" (index .Release.Name) (index .Release.Namespace) (index .Values.global "cluster_domain")}}
  {{- else }}
    {{- printf "%s-ska-pst-core.%s.svc.cluster.local" (index .Release.Name) (index .Release.Namespace) }}
  {{- end }}
{{- end }}

{{/*
SEND
*/}}
{{- define "ska-pst-send.labels" -}}
{{ include "ska-pst.labels" . }}
{{ include "ska-pst-send.selectorLabels" . }}
{{- end }}

{{/*
SEND Selector labels
*/}}
{{- define "ska-pst-send.selectorLabels" -}}
app: ska-pst-send
{{- end }}

{{/*
JupyterLab
*/}}
{{- define "ska-pst-jupyterlab.labels" -}}
{{ include "ska-pst.labels" . }}
{{ include "ska-pst-jupyterlab.selectorLabels" . }}
{{- end }}

{{- define "ska-pst-jupyterlab.selectorLabels" -}}
app: ska-pst-jupyterlab
{{- end }}

{{- define "ingress_path_prepend" }}
    {{- if .Values.jupyterlab.ingress.namespaced }}
        {{- printf "/%s%s" .Release.Namespace .Values.jupyterlab.ingress.pathStart }}
    {{- else }}
        {{- printf "%s" .Values.jupyterlab.ingress.pathStart }}
    {{- end }}
{{- end }}

{{/*
SDP Helpers
*/}}
{{- define "sendDpPvcName" }}
  {{- if and (.Values.global) (index .Values.global "data-product-pvc-name") }}
    {{- printf "%s" (index .Values.global "data-product-pvc-name")}}
  {{- else }}
    {{- printf "%s" (index .Values.core.send.dataProductPVC.name) }}
  {{- end }}
{{- end }}

{{- define "sdpNamespace" }}
  {{- if (.Values.sdp.namespace) }}
    {{- printf "%s" (index .Values.sdp.namespace) }}
  {{- else }}
    {{- printf "%s" (.Release.Namespace) }}
  {{- end }}
{{- end }}

{{- define "sendArgs" }}
  {{- if (.Values.send.dpdIntegration) }}
    {{- printf "--data_product_dashboard %s" ( .Values.sdp.namespace) }}
  {{- else }}
    {{- printf "%s" (.Release.Namespace) }}
  {{- end }}
{{- end }}

