# -*- coding: utf-8 -*-
#
# This file is part of the SKA PST project
#
# Distributed under the terms of the BSD 3-clause new license.
# See LICENSE for more info.
"""This module contains tests for the BEAM component manager class."""

import json
import logging
import pathlib
import queue
import random
import sys
import time
from typing import Any, Callable, Dict, Generator, List, Optional, Union, cast
from unittest.mock import MagicMock, call

import pytest
from ska_control_model import (
    CommunicationStatus,
    HealthState,
    LoggingLevel,
    ObsState,
    PowerState,
    PstProcessingMode,
    SimulationMode,
)
from ska_pst.common.constants import GIGABITS_PER_BYTE
from ska_pst.lmc.beam import PstBeamComponentManager, PstBeamDeviceInterface
from ska_pst.lmc.component import (
    PstProcessApiSubcomponentManager,
    PstSubcomponentManager,
    SubcomponentEventMessage,
)
from ska_pst.lmc.dsp.dsp_disk_model import DEFAULT_RECORDING_TIME
from ska_pst.lmc.dsp.dsp_util import generate_dsp_scan_request
from ska_pst.lmc.job import TaskExecutor
from ska_pst.lmc.util.background_task import BackgroundTaskProcessor
from ska_tango_base.executor import TaskStatus
from tests.unit.lmc.conftest import _ThreadingCallback, calc_expected_beam_channel_block_configuration

from ska_pst.common import TelescopeConfig, TelescopeFacilityEnum

##########
# Fixtures
##########


@pytest.fixture
def background_task_processor() -> BackgroundTaskProcessor:
    """Create Background Processor fixture."""
    return MagicMock()


@pytest.fixture
def communication_state_callback() -> Callable[[CommunicationStatus], None]:
    """Create communication state callback fixture."""
    return MagicMock()


@pytest.fixture
def component_state_callback() -> Callable:
    """Create component state callback fixture."""
    return MagicMock()


@pytest.fixture
def patch_submit_job() -> bool:
    """Patch submit_job."""
    return False


@pytest.fixture
def expected_output_path(
    scan_output_dir_pattern: str,
    eb_id: str,
    subsystem_id: str,
    scan_id: int,
) -> pathlib.Path:
    """Get expected output path given eb_id, subsystem_id and scan_id."""
    pattern_values = {"eb_id": eb_id, "subsystem_id": subsystem_id, "scan_id": str(scan_id)}

    output_path = scan_output_dir_pattern
    for k, v in pattern_values.items():
        output_path = output_path.replace(f"<{k}>", v)

    return pathlib.Path(output_path)


@pytest.fixture
def device_interface(
    device_name: str,
    beam_id: int,
    smrb_process_api_endpoint: str,
    recv_process_api_endpoint: str,
    dsp_disk_process_api_endpoint: str,
    dsp_flow_through_process_api_endpoint: str,
    stat_process_api_endpoint: str,
    communication_state_callback: Callable[[CommunicationStatus], None],
    component_state_callback: Callable,
    property_callback: Callable,
    telescope_facility: TelescopeFacilityEnum,
    scan_output_dir_pattern: str,
    subsystem_id: str,
) -> PstBeamDeviceInterface:
    """Create device interface fixture to mock the BEAM.MGMT tango device."""
    device_interface = MagicMock()
    device_interface.smrb_process_api_endpoint = smrb_process_api_endpoint
    device_interface.recv_process_api_endpoint = recv_process_api_endpoint
    device_interface.dsp_disk_process_api_endpoint = dsp_disk_process_api_endpoint
    device_interface.dsp_flow_through_process_api_endpoint = dsp_flow_through_process_api_endpoint
    device_interface.stat_process_api_endpoint = stat_process_api_endpoint
    device_interface.device_name = device_name
    device_interface.handle_communication_state_change = communication_state_callback
    device_interface.handle_component_state_change = component_state_callback
    device_interface.handle_attribute_value_update = property_callback
    device_interface.beam_id = beam_id
    device_interface.facility = telescope_facility
    device_interface.scan_output_dir_pattern = scan_output_dir_pattern
    device_interface.subsystem_id = subsystem_id
    device_interface.health_check_interval = 1000

    return cast(PstBeamDeviceInterface, device_interface)


@pytest.fixture
def pst_task_executor(logger: logging.Logger) -> TaskExecutor:
    """Get a PST TaskExecutor."""
    return TaskExecutor(logger=logger, max_parallel_workers=1)


@pytest.fixture
def component_manager(
    device_interface: PstBeamDeviceInterface,
    logger: logging.Logger,
    background_task_processor: BackgroundTaskProcessor,
    monkeypatch: pytest.MonkeyPatch,
    patch_submit_job: bool,
    pst_task_executor: TaskExecutor,
    subcomponent_event_queue: queue.Queue[SubcomponentEventMessage],
) -> Generator[PstBeamComponentManager, None, None]:
    """Create PST Beam Component fixture."""
    component_manager = PstBeamComponentManager(
        device_interface=device_interface,
        logger=logger,
        background_task_processor=background_task_processor,
        pst_task_executor=pst_task_executor,
        event_queue=subcomponent_event_queue,
    )

    if patch_submit_job:
        from ska_pst.lmc.beam.beam_component_manager import _RemoteJob
        from ska_pst.lmc.util.callback import Callback

        def _remote_job_call(
            remote_job: _RemoteJob, *args: None, task_callback: Callback, **kwargs: Any
        ) -> None:
            remote_job._completion_callback(task_callback)  # type: ignore

        def _submit_task(job: Callable, *args: Any, task_callback: Callback, **kwargs: Any) -> None:
            job(task_callback=task_callback)

        monkeypatch.setattr(component_manager, "submit_task", _submit_task)
        monkeypatch.setattr(_RemoteJob, "__call__", _remote_job_call)

    yield component_manager
    component_manager._pst_task_executor.stop()


@pytest.fixture
def request_params(
    method_name: str,
    pst_configure_scan_request: dict,
    scan_request: dict,
) -> Optional[Any]:
    """Get request parameters for a given method name."""
    if method_name == "configure_scan":
        return {"configuration": pst_configure_scan_request}
    elif method_name == "scan":
        return scan_request
    elif method_name == "go_to_fault":
        return {"fault_msg": "putting BEAM into fault"}
    else:
        return None


def _mock_subcomponent_command(
    subcomponents: List[PstSubcomponentManager],
    command_name: str,
    monkeypatch: pytest.MonkeyPatch,
) -> List[MagicMock]:
    mocks: List[MagicMock] = []
    for sc in subcomponents:
        mock = MagicMock(name=f"{sc.subcomponent_name}_{command_name}")
        monkeypatch.setattr(sc, command_name, mock)
        mocks.append(mock)

    return mocks


@pytest.fixture
def mocked_subcomponents(
    component_manager: PstBeamComponentManager, monkeypatch: pytest.MonkeyPatch
) -> Dict[str, MagicMock]:
    """Mock subcomponents of the component_manager."""
    result: Dict[str, MagicMock] = {}
    subcomponents: List[MagicMock] = list()
    for scm in [
        "smrb",
        "dsp",
        "recv",
        "stat",
    ]:
        mock = MagicMock()
        monkeypatch.setattr(component_manager, f"_{scm}_subcomponent", mock)
        subcomponents.append(mock)
        result[scm] = mock

    monkeypatch.setattr(component_manager, "_subcomponents", subcomponents)

    return result


def _initialise_subcomponents_health_state(subcomponents: Dict[str, MagicMock]) -> None:
    for scm, mock in subcomponents.items():
        if scm == "dsp":
            mock.dsp_disk_health_state = HealthState.OK
            mock.dsp_flow_through_health_state = HealthState.OK
        else:
            mock.health_state = HealthState.OK


def _set_subcomponent_health_state(
    subcomponent_name: str, subcomponent: MagicMock, health_state: HealthState
) -> None:
    if subcomponent_name == "dsp":
        if random.choice([True, False]):
            subcomponent.dsp_disk_health_state = health_state
        else:
            subcomponent.dsp_flow_through_health_state = health_state
    else:
        subcomponent.health_state = health_state


##########
# Tests
##########


@pytest.mark.parametrize(
    "curr_communication_status, new_communication_status, expected_update_states, expected_power_state",
    [
        (
            CommunicationStatus.DISABLED,
            CommunicationStatus.NOT_ESTABLISHED,
            [CommunicationStatus.NOT_ESTABLISHED, CommunicationStatus.ESTABLISHED],
            PowerState.OFF,
        ),
        (
            CommunicationStatus.ESTABLISHED,
            CommunicationStatus.DISABLED,
            [CommunicationStatus.DISABLED],
            PowerState.UNKNOWN,
        ),
        (CommunicationStatus.ESTABLISHED, CommunicationStatus.ESTABLISHED, [], None),
    ],
)
def test_beam_cm_handle_communication_state_change(
    component_manager: PstBeamComponentManager,
    communication_state_callback: Callable[[CommunicationStatus], None],
    component_state_callback: Callable,
    curr_communication_status: CommunicationStatus,
    new_communication_status: CommunicationStatus,
    expected_update_states: List[CommunicationStatus],
    expected_power_state: Optional[PowerState],
    monkeypatch: pytest.MonkeyPatch,
) -> None:
    """Test component manager handles communication state changes correctly."""
    connect_mocks = _mock_subcomponent_command(component_manager._subcomponents, "connect", monkeypatch)
    disconnect_mocks = _mock_subcomponent_command(component_manager._subcomponents, "disconnect", monkeypatch)

    component_manager._communication_state = curr_communication_status

    component_manager._handle_communication_state_change(new_communication_status)

    if len(expected_update_states) == 0:
        communication_state_callback.assert_not_called()  # type: ignore
    else:
        calls = [call(s) for s in expected_update_states]
        communication_state_callback.assert_has_calls(calls)  # type: ignore
        assert component_manager._communication_state == expected_update_states[-1]

    if expected_power_state is not None:
        component_state_callback.assert_called_once_with(  # type: ignore
            fault=None, power=expected_power_state
        )
    else:
        component_state_callback.assert_not_called()  # type: ignore

    if new_communication_status == CommunicationStatus.NOT_ESTABLISHED:
        for mock in connect_mocks:
            mock.assert_called_once()
        for mock in disconnect_mocks:
            mock.assert_not_called()
    elif new_communication_status == CommunicationStatus.DISABLED:
        for mock in connect_mocks:
            mock.assert_not_called()
        for mock in disconnect_mocks:
            mock.assert_called_once()


def test_beam_cm_calls_abort_on_subcomponents(
    component_manager: PstBeamComponentManager,
    monkeypatch: pytest.MonkeyPatch,
) -> None:
    """Test component manager calls Abort to sub-component devices."""
    task_callback = _ThreadingCallback()

    # set the subcomponents into a state that can be aborted
    for sc in component_manager._subcomponents:
        sc._obs_state = ObsState.SCANNING

    abort_mocks: List[MagicMock] = _mock_subcomponent_command(
        component_manager._subcomponents,
        "abort",
        monkeypatch,
    )

    (status, message) = component_manager.abort(task_callback=task_callback)

    assert status == TaskStatus.IN_PROGRESS
    assert message == "Aborting"

    task_callback.wait()

    for abort_mock in abort_mocks:
        abort_mock.assert_called_once()


@pytest.mark.parametrize(
    "method_name, subcomponent_commands, component_state_callback_params",
    [
        (
            "configure_scan",
            ["configure_beam", "configure_scan"],
            {"configured": True},
        ),
        (
            "deconfigure_scan",
            ["deconfigure_beam", "deconfigure_scan"],
            [{"scanning": False}, {"configured": False}],
        ),
        ("scan", "scan", {"scanning": True}),
        ("end_scan", "end_scan", {"scanning": False}),
        (
            "obsreset",
            ["obsreset"],
            {"configured": False},
        ),
        ("go_to_fault", "go_to_fault", {"obsfault": True}),
        (
            "reset",
            ["reset", "connect"],
            [{"scanning": False}, {"configured": False}],
        ),
    ],
)
def test_beam_cm_remote_actions(  # noqa: C901 - override checking of complexity for this test
    component_manager: PstBeamComponentManager,
    device_interface: MagicMock,
    component_state_callback: MagicMock,
    method_name: str,
    request_params: Any | None,
    subcomponent_commands: Union[str, List[str]],
    component_state_callback_params: List[dict] | dict | None,
    csp_configure_scan_request: dict,
    pst_configure_scan_request: dict,
    monkeypatch: pytest.MonkeyPatch,
) -> None:
    """Assert that actions that need to be delegated to remote devices."""
    if method_name == "obsreset":
        component_manager._smrb_subcomponent._obs_state = ObsState.ABORTED
        component_manager._recv_subcomponent._obs_state = ObsState.ABORTED
        component_manager._dsp_subcomponent._obs_state = ObsState.ABORTED
        component_manager._stat_subcomponent._obs_state = ObsState.ABORTED
    elif method_name == "scan":
        # ensure we have previous scan config
        component_manager._curr_scan_config = csp_configure_scan_request

    task_callback = _ThreadingCallback()

    component_manager._update_communication_state(CommunicationStatus.ESTABLISHED)

    if type(subcomponent_commands) is not list:
        subcomponent_commands = [subcomponent_commands]  # type: ignore

    for cmd_name in subcomponent_commands:
        _mock_subcomponent_command(component_manager._subcomponents, cmd_name, monkeypatch)

    func = getattr(component_manager, method_name)
    if method_name == "configure_scan":
        (status, message) = func(task_callback=task_callback, **csp_configure_scan_request)
        request_params = {"configuration": pst_configure_scan_request}
    elif request_params is not None:
        if isinstance(request_params, dict):
            (status, message) = func(task_callback=task_callback, **request_params)
        else:
            (status, message) = func(request_params, task_callback=task_callback)
    else:
        (status, message) = func(task_callback=task_callback)

    assert status == TaskStatus.QUEUED
    assert message == "Task queued"

    task_callback.wait()

    if request_params is not None:
        assert isinstance(request_params, dict), f"Expected request_params for {method_name} to be a dict."
        for m in subcomponent_commands:
            for sc in component_manager._subcomponents:
                cast(MagicMock, getattr(sc, m)).assert_called_once_with(**request_params)
    else:
        [
            cast(MagicMock, getattr(sc, m)).assert_called_once()  # type: ignore
            for sc in component_manager._subcomponents
            for m in subcomponent_commands
        ]

    if component_state_callback_params:
        if isinstance(component_state_callback_params, dict):
            component_state_callback_params = [component_state_callback_params]

        calls = [call(**params) for params in component_state_callback_params]
        component_state_callback.assert_has_calls(calls)
    else:
        component_state_callback.assert_not_called()  # type: ignore

    calls = [call(status=TaskStatus.COMPLETED, result="Completed")]
    task_callback.assert_has_calls(calls)
    if method_name == "go_to_fault":
        device_interface.handle_fault.assert_called_once_with(fault_msg="putting BEAM into fault")

    if method_name == "obsreset" or method_name == "reset":
        cast(MagicMock, device_interface.update_health_state).assert_called_once_with(
            health_state=HealthState.OK
        )
    else:
        cast(MagicMock, device_interface.update_health_state).assert_not_called()


def test_configure_scan_when_in_ready_state(
    component_manager: PstBeamComponentManager,
    device_interface: MagicMock,
    component_state_callback: MagicMock,
    csp_configure_scan_request: dict,
    pst_configure_scan_request: dict,
    monkeypatch: pytest.MonkeyPatch,
) -> None:
    """Test that PST can be reconfigured when in READY state."""
    component_manager.processing_mode = PstProcessingMode.VOLTAGE_RECORDER

    task_callback = _ThreadingCallback()

    component_manager._update_communication_state(CommunicationStatus.ESTABLISHED)

    for cmd_name in ["deconfigure_scan", "deconfigure_beam", "configure_beam", "configure_scan"]:
        _mock_subcomponent_command(component_manager._subcomponents, cmd_name, monkeypatch)

    (status, message) = component_manager.configure_scan(
        task_callback=task_callback, **csp_configure_scan_request
    )
    request_params = {"configuration": pst_configure_scan_request}

    assert status == TaskStatus.QUEUED
    assert message == "Task queued"

    task_callback.wait(timeout=1.0)

    for sc in component_manager._subcomponents:
        for cmd_name in ["deconfigure_scan", "deconfigure_beam"]:
            cast(MagicMock, getattr(sc, cmd_name)).assert_called_once_with()
        for cmd_name in ["configure_beam", "configure_scan"]:
            cast(MagicMock, getattr(sc, cmd_name)).assert_called_once_with(**request_params)

    component_state_callback.assert_not_called()
    calls = [call(status=TaskStatus.COMPLETED, result="Completed")]
    task_callback.assert_has_calls(calls)


@pytest.mark.parametrize(
    "property_name, subcomponent_name, initial_value, update_value",
    [
        ("data_receive_rate", "recv", 0.0, 12.3),
        ("data_received", "recv", 0, 1138),
        ("data_drop_rate", "recv", 0.1, 0.3),
        ("data_dropped", "recv", 1, 11),
        ("misordered_packets", "recv", 0, 1),
        ("misordered_packet_rate", "recv", 0.1, 0.2),
        ("malformed_packets", "recv", 0, 3),
        ("malformed_packet_rate", "recv", 0.0, 0.3),
        ("misdirected_packets", "recv", 1, 4),
        ("misdirected_packet_rate", "recv", 0.2, 0.3),
        ("checksum_failure_packets", "recv", 0, 10),
        ("checksum_failure_packet_rate", "recv", 0.0, 0.5),
        ("timestamp_sync_error_packets", "recv", 4, 6),
        ("timestamp_sync_error_packet_rate", "recv", 0.1, 0.6),
        ("seq_number_sync_error_packets", "recv", 3, 8),
        ("seq_number_sync_error_packet_rate", "recv", 0.2, 0.0),
        ("no_valid_polarisation_correction_packets", "recv", 10, 0),
        ("no_valid_polarisation_correction_packet_rate", "recv", 0.0, 1.0),
        ("no_valid_station_beam_packets", "recv", 12, 0),
        ("no_valid_station_beam_packet_rate", "recv", 0.0, 0.246),
        ("no_valid_pst_beam_packets", "recv", 14, 0),
        ("no_valid_pst_beam_packet_rate", "recv", 0.0, 0.123),
        ("data_record_rate", "dsp", 0.2, 52.3),
        ("data_recorded", "dsp", 2, 42),
        ("disk_capacity", "dsp", sys.maxsize, 6543),
        ("disk_used_bytes", "dsp", 0, 7234),
        ("disk_used_percentage", "dsp", 0.0, 42.0),
        ("available_disk_space", "dsp", sys.maxsize, 1235),
        ("available_recording_time", "dsp", DEFAULT_RECORDING_TIME, 9876.0),
        ("ring_buffer_utilisation", "smrb", 0.0, 12.5),
        ("real_pol_a_mean_freq_avg", "stat", 0.0, 1.1),
        ("real_pol_a_variance_freq_avg", "stat", 0.0, 1.2),
        ("real_pol_a_num_clipped_samples", "stat", 0, 1),
        ("imag_pol_a_mean_freq_avg", "stat", 0.0, 2.1),
        ("imag_pol_a_variance_freq_avg", "stat", 0.0, 2.20),
        ("imag_pol_a_num_clipped_samples", "stat", 0, 2),
        ("real_pol_a_mean_freq_avg_rfi_excised", "stat", 0.0, 3.1),
        ("real_pol_a_variance_freq_avg_rfi_excised", "stat", 0.0, 3.2),
        ("real_pol_a_num_clipped_samples_rfi_excised", "stat", 0, 3),
        ("imag_pol_a_mean_freq_avg_rfi_excised", "stat", 0.0, 4.1),
        ("imag_pol_a_variance_freq_avg_rfi_excised", "stat", 0.0, 4.20),
        ("imag_pol_a_num_clipped_samples_rfi_excised", "stat", 0, 4),
        ("real_pol_b_mean_freq_avg", "stat", 0.0, 5.1),
        ("real_pol_b_variance_freq_avg", "stat", 0.0, 5.2),
        ("real_pol_b_num_clipped_samples", "stat", 0, 5),
        ("imag_pol_b_mean_freq_avg", "stat", 0.0, 6.1),
        ("imag_pol_b_variance_freq_avg", "stat", 0.0, 6.2),
        ("imag_pol_b_num_clipped_samples", "stat", 0, 6),
        ("real_pol_b_mean_freq_avg_rfi_excised", "stat", 0.0, 7.1),
        ("real_pol_b_variance_freq_avg_rfi_excised", "stat", 0.0, 7.2),
        ("real_pol_b_num_clipped_samples_rfi_excised", "stat", 0, 7),
        ("imag_pol_b_mean_freq_avg_rfi_excised", "stat", 0.0, 8.1),
        ("imag_pol_b_variance_freq_avg_rfi_excised", "stat", 0.0, 8.2),
        ("imag_pol_b_num_clipped_samples_rfi_excised", "stat", 0, 8),
    ],
)
def test_beam_cm_monitor_attributes(
    component_manager: PstBeamComponentManager,
    property_name: str,
    subcomponent_name: str,
    initial_value: Any,
    update_value: Any,
    property_callback: Callable,
) -> None:
    """Test that component manager subscribes to monitoring events."""
    # ensure subscriptions
    subcomponent: PstProcessApiSubcomponentManager
    if subcomponent_name == "dsp":
        subcomponent = component_manager._dsp_subcomponent._dsp_disk_component_manager
    else:
        subcomponent = getattr(component_manager, f"_{subcomponent_name}_subcomponent")

    monitor_data = subcomponent.monitor_data
    setattr(monitor_data, property_name, initial_value)

    init_beam_property_value = getattr(component_manager, property_name)
    assert init_beam_property_value == initial_value, (
        f"Expected {property_name} from {subcomponent_name} to be {initial_value} "
        f"but was {init_beam_property_value}"
    )

    setattr(monitor_data, property_name, update_value)

    # simulate an update of data
    subcomponent._monitor_data_handler._monitor_data_callback(monitor_data)

    curr_beam_property_value = getattr(component_manager, property_name)
    assert curr_beam_property_value == update_value, (
        f"Expected {property_name} from {subcomponent_name} to be {update_value} "
        f"but was {curr_beam_property_value}"
    )

    property_callback_calls = cast(MagicMock, property_callback).call_args_list
    assert (
        call(property_name, update_value) in property_callback_calls
    ), f'Expected a call handle_attribute_value_update("{property_name}", {update_value})'


def test_beam_cm_channel_block_configuration(
    component_manager: PstBeamComponentManager,
    property_callback: Callable,
) -> None:
    """Test that component manager handles channel block configuration from RECV subband configuration."""
    recv_subcomponent = component_manager._recv_subcomponent

    assert (
        component_manager.channel_block_configuration == {}
    ), "Expected channel block configuration to initially be empty"

    component_manager.channel_block_configuration = {"foo": "bar"}

    recv_subcomponent.subband_beam_configuration = {}
    assert component_manager.channel_block_configuration == {}

    # still need worry about the event callback to TANGO device
    cast(MagicMock, property_callback).assert_called_with("channel_block_configuration", "{}")

    recv_subband_config = {
        "common": {"nsubband": 2},
        "subbands": {
            1: {
                "data_host": "10.10.0.1",
                "data_port": 30000,
                "data_mac": "01:02:03:ab:cd:ef",
                "start_channel": 0,
                "end_channel": 10,
                "start_centre_freq_mhz": 1.234,
            },
            2: {
                "data_host": "10.10.0.1",
                "data_port": 30001,
                "data_mac": "01:02:03:ab:cd:ef",
                "start_channel": 10,
                "end_channel": 16,
                "start_centre_freq_mhz": 4.567,
            },
        },
    }

    recv_subcomponent.subband_beam_configuration = recv_subband_config
    expected_channel_block_configuration = calc_expected_beam_channel_block_configuration(recv_subband_config)

    assert component_manager.channel_block_configuration == expected_channel_block_configuration
    cast(MagicMock, property_callback).assert_called_with(
        "channel_block_configuration", json.dumps(expected_channel_block_configuration)
    )


@pytest.mark.parametrize(
    "patch_submit_job, processing_mode",
    [(True, PstProcessingMode.VOLTAGE_RECORDER), (True, PstProcessingMode.FLOW_THROUGH)],
)
def test_beam_cm_stores_configuration_properties(
    component_manager: PstBeamComponentManager,
    csp_configure_scan_request: dict,
    processing_mode: PstProcessingMode,
) -> None:
    """Test to see the BEAM component manager sets config id configure/deconfigure scan."""
    task_callback = _ThreadingCallback()

    assert component_manager.config_id == ""
    assert component_manager._curr_scan_config is None
    assert component_manager.processing_mode == PstProcessingMode.IDLE

    component_manager.configure_scan(task_callback=task_callback, **csp_configure_scan_request)

    task_callback.wait()

    # assert current scan config is configure_scan request
    assert component_manager.config_id == csp_configure_scan_request["common"]["config_id"]
    assert component_manager._curr_scan_config == csp_configure_scan_request
    assert component_manager.processing_mode == processing_mode

    task_callback.clear()
    component_manager.deconfigure_scan(task_callback=task_callback)
    task_callback.wait()

    assert component_manager.config_id == ""


@pytest.mark.parametrize("patch_submit_job", [True])
def test_beam_cm_configure_scan_sets_expected_data_record_rate(
    component_manager: PstBeamComponentManager,
    csp_configure_scan_request: dict,
    pst_configure_scan_request: dict,
) -> None:
    """Test to BEAM component manager updates expected data rate on configure/deconfigure scan."""
    task_callback = _ThreadingCallback()

    assert component_manager.expected_data_record_rate == 0.0

    component_manager.configure_scan(task_callback=task_callback, **csp_configure_scan_request)

    dsp_scan_request = generate_dsp_scan_request(**pst_configure_scan_request)
    assert (
        component_manager.expected_data_record_rate
        == dsp_scan_request["bytes_per_second"] * GIGABITS_PER_BYTE
    )

    component_manager.deconfigure_scan(task_callback=task_callback)

    assert component_manager.expected_data_record_rate == 0.0


@pytest.mark.parametrize(
    "err_subcomponent_name,err_msg",
    [
        (None, None),
        ("dsp", "Something went wrong"),
        ("recv", "That's not a valid request"),
        ("smrb", "Oops something went wrong with the validation"),
        ("stat", "Stat had a problem validating the request"),
    ],
)
def test_beam_cm_validate_configure_scan(
    component_manager: PstBeamComponentManager,
    csp_configure_scan_request: dict,
    pst_configure_scan_request: dict,
    err_subcomponent_name: str | None,
    err_msg: str | None,
    monkeypatch: pytest.MonkeyPatch,
) -> None:
    """Assert that validation actions are delegated to remote devices."""
    """Assert that actions that need to be delegated to remote devices."""
    component_manager._update_communication_state(CommunicationStatus.ESTABLISHED)

    _mock_subcomponent_command(component_manager._subcomponents, "configure_beam", monkeypatch)
    _mock_subcomponent_command(component_manager._subcomponents, "configure_scan", monkeypatch)
    for d in component_manager._subcomponents:
        if d.subcomponent_name == err_subcomponent_name:
            [error_mock, *_] = _mock_subcomponent_command([d], "validate_configure_scan", monkeypatch)
            error_mock.side_effect = RuntimeError(err_msg)

    if err_msg:
        with pytest.raises(RuntimeError) as excinfo:
            component_manager.validate_configure_scan(csp_configure_scan_request)

        assert str(excinfo.value) == err_msg
    else:
        component_manager.validate_configure_scan(csp_configure_scan_request)

    for d in component_manager._subcomponents:
        if d.subcomponent_name == err_subcomponent_name:
            error_mock.assert_called_once_with(configuration=pst_configure_scan_request)

        cast(MagicMock, getattr(d, "configure_beam")).assert_not_called()
        cast(MagicMock, getattr(d, "configure_scan")).assert_not_called()


@pytest.mark.parametrize("patch_submit_job", [True])
def test_beam_cm_updates_scan_id_on_start_scan_stop_scan(
    component_manager: PstBeamComponentManager, scan_request: dict
) -> None:
    """Test to BEAM component manager updates scan_id on start_scan/end_scan."""
    task_callback = MagicMock()

    assert component_manager.scan_id == 0

    component_manager.scan(task_callback=task_callback, **scan_request)

    assert component_manager.scan_id == scan_request["scan_id"]

    component_manager.end_scan(task_callback=task_callback)

    assert component_manager.scan_id == 0


def test_beam_cm_set_simulation_mode_on_child_devices(
    component_manager: PstBeamComponentManager,
    monkeypatch: pytest.MonkeyPatch,
) -> None:
    """Test component manager delegates setting simulation mode to sub-component devices."""
    _mock_subcomponent_command(component_manager._subcomponents, "_simulation_mode_changed", monkeypatch)

    component_manager.simulation_mode = SimulationMode.FALSE

    assert component_manager._smrb_subcomponent.simulation_mode == SimulationMode.FALSE
    assert component_manager._recv_subcomponent.simulation_mode == SimulationMode.FALSE
    assert component_manager._dsp_subcomponent.simulation_mode == SimulationMode.FALSE
    assert component_manager._stat_subcomponent.simulation_mode == SimulationMode.FALSE

    component_manager.simulation_mode = SimulationMode.TRUE

    assert component_manager._smrb_subcomponent.simulation_mode == SimulationMode.TRUE
    assert component_manager._recv_subcomponent.simulation_mode == SimulationMode.TRUE
    assert component_manager._dsp_subcomponent.simulation_mode == SimulationMode.TRUE
    assert component_manager._stat_subcomponent.simulation_mode == SimulationMode.TRUE


@pytest.mark.parametrize(
    "telescope_facility", [TelescopeFacilityEnum.Low, TelescopeFacilityEnum.Mid], indirect=True
)
def test_beam_cm_updates_frequency_band_to_low_for_ska_low(
    component_manager: PstBeamComponentManager,
    csp_configure_scan_request: dict,
    pst_configure_scan_request: dict,
    telescope_facility: TelescopeFacilityEnum,
    telescope_config: TelescopeConfig,
    monkeypatch: pytest.MonkeyPatch,
) -> None:
    """Test that component manager removes frequency band for Low but not High."""
    assert "frequency_band" in csp_configure_scan_request["common"]
    if telescope_facility == TelescopeFacilityEnum.Low:
        csp_configure_scan_request["common"]["frequency_band"] = "low"
        pst_configure_scan_request["cbf_pst_config"] = telescope_config.get_cbf_pst_config(
            frequency_band="low"
        )
    else:
        assert "frequency_band" in pst_configure_scan_request

    task_callback = _ThreadingCallback()

    component_manager._update_communication_state(CommunicationStatus.ESTABLISHED)

    _mock_subcomponent_command(component_manager._subcomponents, "configure_scan", monkeypatch)
    _mock_subcomponent_command(component_manager._subcomponents, "configure_beam", monkeypatch)

    component_manager.configure_scan(task_callback=task_callback, **csp_configure_scan_request)

    task_callback.wait()

    for m in ["configure_scan", "configure_beam"]:
        for sc in component_manager._subcomponents:
            mock = cast(MagicMock, getattr(sc, m))
            mock.assert_called_once_with(configuration=pst_configure_scan_request)

    calls = [call(status=TaskStatus.COMPLETED, result="Completed")]
    task_callback.assert_has_calls(calls)


def test_beam_cm_start_communicating(
    component_manager: PstBeamComponentManager,
    device_interface: PstBeamDeviceInterface,
    mocked_subcomponents: Dict[str, MagicMock],
) -> None:
    """Test BEAM component manager when start_communicating is called."""
    _initialise_subcomponents_health_state(mocked_subcomponents)

    component_manager._communication_state = CommunicationStatus.DISABLED
    component_manager.start_communicating()

    cast(MagicMock, device_interface.update_health_state).assert_called_once_with(health_state=HealthState.OK)
    assert component_manager._communication_state == CommunicationStatus.ESTABLISHED
    for subcomponent in mocked_subcomponents.values():
        cast(MagicMock, subcomponent.connect).assert_called_once()


def test_beam_cm_stop_communicating(
    component_manager: PstBeamComponentManager,
    device_interface: PstBeamDeviceInterface,
    monkeypatch: pytest.MonkeyPatch,
) -> None:
    """Test BEAM component manager when stop_communicating is called."""
    mocked_api_calls = _mock_subcomponent_command(component_manager._subcomponents, "disconnect", monkeypatch)

    component_manager._communication_state = CommunicationStatus.ESTABLISHED
    component_manager.stop_communicating()

    cast(MagicMock, device_interface.update_health_state).assert_called_once_with(
        health_state=HealthState.UNKNOWN
    )
    assert component_manager._communication_state == CommunicationStatus.DISABLED
    for mock_api_call in cast(List[MagicMock], mocked_api_calls):
        mock_api_call.assert_called_once()


@pytest.mark.parametrize(
    "subcomponent_name, beam_obs_state, subcomponent_obs_state",
    [
        ("smrb", ObsState.ABORTED, ObsState.SCANNING),
        ("recv", ObsState.ABORTED, ObsState.READY),
        ("dsp", ObsState.ABORTED, ObsState.IDLE),
        ("stat", ObsState.ABORTED, ObsState.IDLE),
        ("smrb", ObsState.FAULT, ObsState.IDLE),
        ("recv", ObsState.FAULT, ObsState.SCANNING),
        ("dsp", ObsState.FAULT, ObsState.READY),
        ("stat", ObsState.FAULT, ObsState.READY),
        ("smrb", ObsState.ABORTED, ObsState.EMPTY),
        ("recv", ObsState.ABORTED, ObsState.EMPTY),
        ("dsp", ObsState.ABORTED, ObsState.EMPTY),
        ("stat", ObsState.ABORTED, ObsState.EMPTY),
        ("smrb", ObsState.FAULT, ObsState.EMPTY),
        ("recv", ObsState.FAULT, ObsState.EMPTY),
        ("dsp", ObsState.FAULT, ObsState.EMPTY),
        ("stat", ObsState.FAULT, ObsState.EMPTY),
    ],
)
def test_beam_cm_puts_subordinate_devices_in_state_to_do_obsreset(
    component_manager: PstBeamComponentManager,
    subcomponent_name: str,
    beam_obs_state: ObsState,
    subcomponent_obs_state: ObsState,
    monkeypatch: pytest.MonkeyPatch,
) -> None:
    """Test that obsreset puts subordinate devices in right state before resetting."""
    subcomponent: PstSubcomponentManager
    for sc in component_manager._subcomponents:
        if sc.subcomponent_name == subcomponent_name:
            subcomponent = sc
            sc._obs_state = subcomponent_obs_state
        else:
            sc._obs_state = beam_obs_state
    assert subcomponent is not None, f"Expected to find a subcomponent with name '{subcomponent_name}'"

    _mock_subcomponent_command(component_manager._subcomponents, "abort", monkeypatch)
    _mock_subcomponent_command(component_manager._subcomponents, "obsreset", monkeypatch)

    task_callback = _ThreadingCallback()

    component_manager._update_communication_state(CommunicationStatus.ESTABLISHED)

    component_manager.obsreset(task_callback=task_callback)

    task_callback.wait()

    if subcomponent_obs_state != ObsState.EMPTY:
        cast(MagicMock, subcomponent.abort).assert_called_once()

    [
        cast(MagicMock, sc.abort).assert_not_called()  # type: ignore
        for sc in component_manager._subcomponents
        if sc != subcomponent
    ]

    [
        cast(MagicMock, sc.obsreset).assert_called_once()  # type: ignore
        for sc in component_manager._subcomponents
        if sc != subcomponent
    ]

    calls = [call(status=TaskStatus.COMPLETED, result="Completed")]
    task_callback.assert_has_calls(calls)


@pytest.mark.parametrize(
    "subcomponent_name, beam_obs_state, subcomponent_obs_state",
    [
        ("smrb", ObsState.ABORTED, ObsState.SCANNING),
        ("recv", ObsState.ABORTED, ObsState.READY),
        ("dsp", ObsState.ABORTED, ObsState.IDLE),
        ("stat", ObsState.ABORTED, ObsState.IDLE),
        ("smrb", ObsState.FAULT, ObsState.IDLE),
        ("recv", ObsState.FAULT, ObsState.SCANNING),
        ("dsp", ObsState.FAULT, ObsState.READY),
        ("stat", ObsState.FAULT, ObsState.READY),
        ("smrb", ObsState.ABORTED, ObsState.EMPTY),
        ("recv", ObsState.ABORTED, ObsState.EMPTY),
        ("dsp", ObsState.ABORTED, ObsState.EMPTY),
        ("stat", ObsState.ABORTED, ObsState.EMPTY),
        ("smrb", ObsState.FAULT, ObsState.EMPTY),
        ("recv", ObsState.FAULT, ObsState.EMPTY),
        ("dsp", ObsState.FAULT, ObsState.EMPTY),
        ("stat", ObsState.FAULT, ObsState.EMPTY),
    ],
)
def test_beam_cm_puts_subordinate_devices_in_state_to_do_reset(
    component_manager: PstBeamComponentManager,
    subcomponent_name: str,
    beam_obs_state: ObsState,
    subcomponent_obs_state: ObsState,
    monkeypatch: pytest.MonkeyPatch,
) -> None:
    """Test that reset puts subordinate devices in right state after resetting."""
    subcomponent: PstSubcomponentManager
    for sc in component_manager._subcomponents:
        if sc.subcomponent_name == subcomponent_name:
            subcomponent = sc
            sc._obs_state = subcomponent_obs_state
        else:
            sc._obs_state = beam_obs_state
    assert subcomponent is not None, f"Expected to find a subcomponent with name '{subcomponent_name}'"

    _mock_subcomponent_command(component_manager._subcomponents, "reset", monkeypatch)
    _mock_subcomponent_command(component_manager._subcomponents, "connect", monkeypatch)

    task_callback = _ThreadingCallback()

    component_manager._update_communication_state(CommunicationStatus.ESTABLISHED)

    component_manager.reset(task_callback=task_callback)

    task_callback.wait()

    [
        cast(MagicMock, sc.reset).assert_called_once()  # type: ignore
        for sc in component_manager._subcomponents
        if sc != subcomponent
    ]

    [
        cast(MagicMock, sc.connect).assert_called_once()  # type: ignore
        for sc in component_manager._subcomponents
        if sc != subcomponent
    ]

    calls = [call(status=TaskStatus.COMPLETED, result="Completed")]
    task_callback.assert_has_calls(calls)


@pytest.mark.parametrize(
    "log_level",
    [LoggingLevel.INFO, LoggingLevel.DEBUG, LoggingLevel.FATAL, LoggingLevel.WARNING, LoggingLevel.OFF],
)
def test_beam_cm_set_logging_level(
    component_manager: PstBeamComponentManager,
    log_level: LoggingLevel,
    monkeypatch: pytest.MonkeyPatch,
) -> None:
    """Test that updates the LogLevel of the PstBeamComponentManager."""
    _mock_subcomponent_command(component_manager._subcomponents, "set_logging_level", monkeypatch)

    component_manager.set_logging_level(log_level=log_level)
    for sc in component_manager._subcomponents:
        cast(MagicMock, getattr(sc, "set_logging_level")).assert_called_once_with(log_level=log_level)


def test_beam_cm_set_monitoring_polling_rate(
    component_manager: PstBeamComponentManager,
) -> None:
    """Test updating monitoring polling rate updates subordinated devices."""
    monitoring_polling_rate_ms = random.randint(100, 1000)
    component_manager.monitoring_polling_rate_ms = monitoring_polling_rate_ms
    assert component_manager.monitoring_polling_rate_ms == monitoring_polling_rate_ms, (
        "Expected the monitoring polling of BEAM component manager "
        f"to have been set to {monitoring_polling_rate_ms}"
    )
    for sc in component_manager._subcomponents:
        assert (
            sc.monitoring_polling_rate_ms == monitoring_polling_rate_ms
        ), f"Expected the monitoring polling rate for {sc} to have been set to {monitoring_polling_rate_ms}"


@pytest.mark.parametrize(
    "telescope_facility", [TelescopeFacilityEnum.Low, TelescopeFacilityEnum.Mid], indirect=False
)
def test_beam_cm_start_scan_writes_scan_config_json_file(
    component_manager: PstBeamComponentManager,
    scan_id: int,
    csp_configure_scan_request: dict,
    subsystem_id: str,
    telescope_facility: TelescopeFacilityEnum,
    expected_output_path: pathlib.Path,
    monkeypatch: pytest.MonkeyPatch,
) -> None:
    """Assert that the scan configuration is written to the correct output location."""
    if telescope_facility == TelescopeFacilityEnum.Low:
        assert subsystem_id == "pst-low", "Expected subsystem_id facet to be 'pst-low'"
    else:
        assert subsystem_id == "pst-mid", "Expected subsystem_id facet to be 'pst-mid'"

    task_callback = _ThreadingCallback()

    _mock_subcomponent_command(component_manager._subcomponents, "scan", monkeypatch)

    component_manager._curr_scan_config = csp_configure_scan_request
    # need to stub out calls to start scan on devices - for now test method directly
    component_manager.scan(scan_id=scan_id, task_callback=task_callback)
    # assert that file is written to correct location

    task_callback.wait()

    assert expected_output_path.exists(), f"Expected that {expected_output_path} exists"

    json_path = expected_output_path / "scan_configuration.json"
    assert json_path.exists(), f"Expected that {json_path} exists"

    with open(json_path, "r") as f:
        stored_config = json.load(f)

    assert (
        stored_config == csp_configure_scan_request
    ), "Expected stored config file to be same as CSP scan request"

    json_path.unlink()


def test_beam_cm_restarts_health_check_when_interval_changes(
    component_manager: PstBeamComponentManager,
    monkeypatch: pytest.MonkeyPatch,
) -> None:
    """Assert that health check is restarted when health check interval changes."""
    subcomponent_mock = MagicMock()
    monkeypatch.setattr(component_manager, "_subcomponents", [subcomponent_mock])

    health_check_interval = random.randint(1001, 10000)

    # assert default health check interval set
    assert component_manager.health_check_interval == 1000
    subcomponent_mock.health_check_interval = 1000

    component_manager.health_check_interval = health_check_interval
    assert (
        subcomponent_mock.health_check_interval == health_check_interval
    ), f"expected subcomponent manager health check interval to have been set to {health_check_interval}"

    cast(MagicMock, subcomponent_mock.restart_health_check).assert_called_once()


def test_beam_cm_handle_health_state_all_ok(
    component_manager: PstBeamComponentManager,
    device_interface: MagicMock,
    mocked_subcomponents: Dict[str, MagicMock],
) -> None:
    """Assert handle health state aggregation when all subcomponents are in OK state."""
    _initialise_subcomponents_health_state(subcomponents=mocked_subcomponents)

    component_manager.handle_health_state_change()

    cast(MagicMock, device_interface.update_health_state).assert_called_once_with(health_state=HealthState.OK)


def test_beam_cm_handle_health_state_when_one_subcomponent_has_degraded(
    component_manager: PstBeamComponentManager,
    device_interface: MagicMock,
    mocked_subcomponents: Dict[str, MagicMock],
) -> None:
    """Assert handle health state aggregation when one subcomponent is degraded."""
    _initialise_subcomponents_health_state(subcomponents=mocked_subcomponents)

    (rand_scm, rand_mock) = random.choice(list(mocked_subcomponents.items()))
    _set_subcomponent_health_state(rand_scm, rand_mock, HealthState.DEGRADED)

    component_manager.handle_health_state_change()

    cast(MagicMock, device_interface.update_health_state).assert_called_once_with(
        health_state=HealthState.DEGRADED
    )


def test_beam_cm_handle_health_state_when_at_least_two_subcomponents_are_degraded(
    component_manager: PstBeamComponentManager,
    device_interface: MagicMock,
    mocked_subcomponents: Dict[str, MagicMock],
) -> None:
    """Assert handle health state aggregation when at least 2 subcomponents are degraded."""
    _initialise_subcomponents_health_state(subcomponents=mocked_subcomponents)

    for rand_scm, rand_mock in random.sample(list(mocked_subcomponents.items()), random.randint(2, 4)):
        _set_subcomponent_health_state(rand_scm, rand_mock, HealthState.DEGRADED)

    component_manager.handle_health_state_change()

    cast(MagicMock, device_interface.update_health_state).assert_called_once_with(
        health_state=HealthState.FAILED
    )


def test_beam_cm_handle_health_state_when_at_least_one_has_failed(
    component_manager: PstBeamComponentManager,
    device_interface: MagicMock,
    mocked_subcomponents: Dict[str, MagicMock],
) -> None:
    """Assert handle health state aggregation when at least 1 subcomponent has failed."""
    _initialise_subcomponents_health_state(subcomponents=mocked_subcomponents)

    for rand_scm, rand_mock in random.sample(list(mocked_subcomponents.items()), random.randint(1, 4)):
        _set_subcomponent_health_state(rand_scm, rand_mock, HealthState.FAILED)

    component_manager.handle_health_state_change()

    cast(MagicMock, device_interface.update_health_state).assert_called_once_with(
        health_state=HealthState.FAILED
    )


def test_beam_cm_handle_health_state_when_at_least_one_is_unknown_but_rest_are_ok(
    component_manager: PstBeamComponentManager,
    device_interface: MagicMock,
    mocked_subcomponents: Dict[str, MagicMock],
) -> None:
    """Assert handle health state aggregation when at least one is unknown but rest are OK."""
    _initialise_subcomponents_health_state(subcomponents=mocked_subcomponents)

    for rand_scm, rand_mock in random.sample(list(mocked_subcomponents.items()), random.randint(1, 4)):
        _set_subcomponent_health_state(rand_scm, rand_mock, HealthState.UNKNOWN)

    component_manager.handle_health_state_change()

    cast(MagicMock, device_interface.update_health_state).assert_called_once_with(
        health_state=HealthState.UNKNOWN
    )


@pytest.mark.parametrize("subcomponent_name", ["recv", "smrb", "stat", "dsp_ft", "dsp_flow_through"])
def test_beam_cm_handles_subcomponent_events(
    device_interface: MagicMock,
    logger: logging.Logger,
    subcomponent_name: str,
    subcomponent_event_queue: queue.Queue[SubcomponentEventMessage],
    monkeypatch: pytest.MonkeyPatch,
) -> None:
    """Test that component manager handles events from the the subcomponents."""
    handle_health_state_change_mock = MagicMock()
    attribute_value_updated_callback_mock = cast(MagicMock, device_interface.handle_attribute_value_update)
    background_task_processor = BackgroundTaskProcessor(default_logger=logger)
    pst_task_executor = MagicMock()
    monkeypatch.setattr(
        PstBeamComponentManager, "handle_health_state_change", handle_health_state_change_mock
    )

    component_manager = PstBeamComponentManager(
        device_interface=device_interface,
        logger=logger,
        background_task_processor=background_task_processor,
        pst_task_executor=pst_task_executor,
        event_queue=subcomponent_event_queue,
    )

    assert subcomponent_event_queue == component_manager._event_queue

    component_manager._handle_subcomponent_event_messages()

    try:
        # ensure that we can tests first event will send both ObsState and HealthCheck
        # updates events back to TANGO device.
        del component_manager._previous_event_msgs[subcomponent_name]
        attribute_value_updated_callback_mock.reset_mock()
        handle_health_state_change_mock.reset_mock()
    except Exception:
        pass

    # Test first time getting a message
    health_state = random.choice(list(HealthState))
    obs_state = random.choice(list(ObsState))

    subcomponent_event_queue.put_nowait(
        SubcomponentEventMessage(
            subcomponent_name=subcomponent_name, health_state=health_state, obs_state=obs_state
        )
    )

    while not subcomponent_event_queue.empty():
        time.sleep(0.01)

    calls = [
        call(f"{subcomponent_name}_health_state", health_state),
        call(f"{subcomponent_name}_obs_state", obs_state),
    ]
    attribute_value_updated_callback_mock.assert_has_calls(calls=calls, any_order=True)
    attribute_value_updated_callback_mock.reset_mock()

    handle_health_state_change_mock.assert_called()
    handle_health_state_change_mock.reset_mock()

    # This will test that only we send a health state value had updated as obs state hasn't changed
    prev_health_state = health_state
    while prev_health_state == health_state:
        health_state = random.choice(list(HealthState))

    subcomponent_event_queue.put_nowait(
        SubcomponentEventMessage(
            subcomponent_name=subcomponent_name, health_state=health_state, obs_state=obs_state
        )
    )

    while not subcomponent_event_queue.empty():
        time.sleep(0.01)

    attribute_value_updated_callback_mock.assert_called_once_with(
        f"{subcomponent_name}_health_state", health_state
    )
    attribute_value_updated_callback_mock.reset_mock()

    handle_health_state_change_mock.assert_called_once()
    handle_health_state_change_mock.reset_mock()

    # This will test that only we send a obs state value had updated as health state hasn't changed
    prev_obs_state = obs_state
    while prev_obs_state == obs_state:
        obs_state = random.choice(list(ObsState))

    subcomponent_event_queue.put_nowait(
        SubcomponentEventMessage(
            subcomponent_name=subcomponent_name, health_state=health_state, obs_state=obs_state
        )
    )

    while not subcomponent_event_queue.empty():
        time.sleep(0.01)

    attribute_value_updated_callback_mock.assert_called_once_with(f"{subcomponent_name}_obs_state", obs_state)
    attribute_value_updated_callback_mock.reset_mock()

    handle_health_state_change_mock.assert_not_called()
    handle_health_state_change_mock.reset_mock()


@pytest.mark.parametrize("subcomponent_name", ["recv", "smrb", "stat", "dsp_disk", "dsp_flow_through"])
def test_beam_cm_subcomponent_obs_state(
    component_manager: PstBeamComponentManager,
    subcomponent_name: str,
    mocked_subcomponents: Dict[str, MagicMock],
) -> None:
    """Test that subcomponent obs state attributes are delegated to correct subcomponent."""
    expected_obs_state = random.choice(list(ObsState))
    if subcomponent_name.startswith("dsp"):
        scm = mocked_subcomponents["dsp"]
        setattr(scm, f"{subcomponent_name}_obs_state", expected_obs_state)
    else:
        scm = mocked_subcomponents[subcomponent_name]
        setattr(scm, "obs_state", expected_obs_state)

    subcomponent_obs_state = getattr(component_manager, f"{subcomponent_name}_obs_state")

    assert subcomponent_obs_state == expected_obs_state


@pytest.mark.parametrize("subcomponent_name", ["recv", "smrb", "stat", "dsp_disk", "dsp_flow_through"])
def test_beam_cm_subcomponent_health_state(
    component_manager: PstBeamComponentManager,
    subcomponent_name: str,
    mocked_subcomponents: Dict[str, MagicMock],
) -> None:
    """Test that subcomponent health state attributes are delegated to correct subcomponent."""
    expected_health_state = random.choice(list(HealthState))
    if subcomponent_name.startswith("dsp"):
        scm = mocked_subcomponents["dsp"]
        setattr(scm, f"{subcomponent_name}_health_state", expected_health_state)
    else:
        scm = mocked_subcomponents[subcomponent_name]
        setattr(scm, "health_state", expected_health_state)

    subcomponent_health_state = getattr(component_manager, f"{subcomponent_name}_health_state")

    assert subcomponent_health_state == expected_health_state


def test_beam_cm_setting_health_check_interval_updates_subcomponents(
    component_manager: PstBeamComponentManager,
    mocked_subcomponents: Dict[str, MagicMock],
) -> None:
    """Assert that updating the health check interval will update subcomponents."""
    component_manager._health_check_interval = random.randint(500, 1000)
    health_check_interval = random.randint(1000, 2000)

    component_manager.health_check_interval = health_check_interval

    # assert internal value
    assert component_manager._health_check_interval == health_check_interval
    # assert property
    assert component_manager.health_check_interval == health_check_interval

    for subcomponent_name, scm in mocked_subcomponents.items():
        assert hasattr(
            scm, "health_check_interval"
        ), f"expected mocked subcomponent {subcomponent_name} to have a health_check_interval attribute"
        assert getattr(scm, "health_check_interval") == health_check_interval, (
            f"expected mocked subcomponent {subcomponent_name}.health_check_interval "
            f"to be {health_check_interval}"
        )
        cast(MagicMock, scm.restart_health_check).assert_called_once()


def test_beam_cm_setting_health_check_interval_to_current_value_does_not_update_subcomponents(
    component_manager: PstBeamComponentManager,
    mocked_subcomponents: Dict[str, MagicMock],
) -> None:
    """Assert that setting health_check_interval to current value does not update subcomponents."""
    health_check_interval = random.randint(1000, 2000)
    component_manager.health_check_interval = health_check_interval

    # setting the heath check interval above should have set the values on the subcomponents
    # and forced a restart of the health check.
    for subcomponent_name, scm in mocked_subcomponents.items():
        assert getattr(scm, "health_check_interval") == health_check_interval, (
            f"expected mocked subcomponent {subcomponent_name}.health_check_interval "
            f"to be {health_check_interval}"
        )
        # perform a reset so we can assert later that there has been no update.
        cast(MagicMock, scm.restart_health_check).reset_mock()

    component_manager.health_check_interval = health_check_interval

    for subcomponent_name, scm in mocked_subcomponents.items():
        # assert that we don't restart health check when the interval value doesn't change
        cast(MagicMock, scm.restart_health_check).assert_not_called()
