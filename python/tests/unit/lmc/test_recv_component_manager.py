# -*- coding: utf-8 -*-
#
# This file is part of the SKA PST project
#
# Distributed under the terms of the BSD 3-clause new license.
# See LICENSE for more info.
"""This module contains tests for the RECV component manager class."""

import dataclasses
import logging
import queue
from typing import Callable, List
from unittest.mock import MagicMock

import pytest
from pytest_mock import MockerFixture
from ska_control_model import LoggingLevel, SimulationMode
from ska_pst.lmc.component import MonitorDataHandler, SubcomponentEventMessage
from ska_pst.lmc.receive.receive_component_manager import PstReceiveComponentManager
from ska_pst.lmc.receive.receive_model import ReceiveData
from ska_pst.lmc.receive.receive_process_api import (
    PstReceiveProcessApi,
    PstReceiveProcessApiGrpc,
    PstReceiveProcessApiSimulator,
)
from ska_pst.lmc.receive.receive_util import calculate_receive_subband_resources
from ska_pst.lmc.validation import ValidationError


@pytest.fixture
def subband_resources_callback() -> MagicMock:
    """Get subband resources calculated callback."""
    return MagicMock()


@pytest.fixture
def component_manager(
    device_name: str,
    grpc_endpoint: str,
    beam_id: int,
    subband_resources_callback: MagicMock,
    simulation_mode: SimulationMode,
    logger: logging.Logger,
    api: PstReceiveProcessApi,
    monitor_data_updated_callback: MagicMock,
    subcomponent_event_queue: queue.Queue[SubcomponentEventMessage],
) -> PstReceiveComponentManager:
    """Create instance of a component manager."""
    return PstReceiveComponentManager(
        device_name=device_name,
        process_api_endpoint=grpc_endpoint,
        simulation_mode=simulation_mode,
        logger=logger,
        api=api,
        beam_id=beam_id,
        monitor_data_updated_callback=monitor_data_updated_callback,
        subband_resources_callback=subband_resources_callback,
        event_queue=subcomponent_event_queue,
    )


@pytest.fixture
def api(
    device_name: str,
    grpc_endpoint: str,
    simulation_mode: SimulationMode,
    logger: logging.Logger,
) -> PstReceiveProcessApi:
    """Create an API instance."""
    if simulation_mode == SimulationMode.TRUE:
        return PstReceiveProcessApiSimulator(
            logger=logger,
        )
    else:
        return PstReceiveProcessApiGrpc(
            client_id=device_name,
            grpc_endpoint=grpc_endpoint,
            logger=logger,
        )


@pytest.fixture
def monitor_data() -> ReceiveData:
    """Create an an instance of ReceiveData for monitor data."""
    from ska_pst.lmc.receive.receive_simulator import generate_random_update

    return generate_random_update()


@pytest.fixture
def calculated_receive_subband_resources(
    beam_id: int,
    pst_configure_scan_request: dict,
    recv_data_host: str,
    recv_data_mac: str,
    subband_udp_ports: List[int],
) -> dict:
    """Calculate expected subband resources."""
    return calculate_receive_subband_resources(
        beam_id=beam_id,
        data_host=recv_data_host,
        data_mac=recv_data_mac,
        subband_udp_ports=subband_udp_ports,
        **pst_configure_scan_request,
    )


def test_recv_cm_connect_to_api_calls_connect_on_api(
    component_manager: PstReceiveComponentManager,
) -> None:
    """Assert start/stop communicating calls API."""
    api = MagicMock()
    component_manager._api = api

    component_manager.connect()
    api.connect.assert_called_once()
    api.disconnect.assert_not_called()

    component_manager.disconnect()
    api.disconnect.assert_called_once()


@pytest.mark.parametrize(
    "property",
    [
        ("data_receive_rate"),
        ("data_received"),
        ("data_drop_rate"),
        ("data_dropped"),
        ("misordered_packets"),
        ("misordered_packet_rate"),
        ("malformed_packets"),
        ("malformed_packet_rate"),
        ("misdirected_packets"),
        ("misdirected_packet_rate"),
        ("checksum_failure_packets"),
        ("checksum_failure_packet_rate"),
        ("timestamp_sync_error_packets"),
        ("timestamp_sync_error_packet_rate"),
        ("seq_number_sync_error_packets"),
        ("seq_number_sync_error_packet_rate"),
        ("no_valid_polarisation_correction_packets"),
        ("no_valid_polarisation_correction_packet_rate"),
        ("no_valid_station_beam_packets"),
        ("no_valid_station_beam_packet_rate"),
        ("no_valid_pst_beam_packets"),
        ("no_valid_pst_beam_packet_rate"),
    ],
)
def test_recv_cm_properties_comes_from_monitor_data(
    component_manager: PstReceiveComponentManager,
    api: PstReceiveProcessApi,
    monitor_data: ReceiveData,
    property: str,
    monkeypatch: pytest.MonkeyPatch,
) -> None:
    """Test properties are coming from API monitor data."""
    monkeypatch.setattr(MonitorDataHandler, "monitor_data", monitor_data)

    actual = getattr(component_manager, property)
    expected = getattr(monitor_data, property)

    assert actual == expected


def test_recv_cm_validate_configure_scan(
    component_manager: PstReceiveComponentManager,
    pst_configure_scan_request: dict,
    calculated_receive_subband_resources: dict,
    recv_data_host: str,
    recv_data_mac: str,
    subband_udp_ports: List[int],
) -> None:
    """Test that validate configuration when valid."""
    api = MagicMock()
    component_manager._api = api
    component_manager._api.get_env.return_value = {
        "data_host": recv_data_host,
        "data_mac": recv_data_mac,
        "data_port": subband_udp_ports[0],
    }

    component_manager.validate_configure_scan(configuration=pst_configure_scan_request)

    expected_request = {
        "common": calculated_receive_subband_resources["common"],
        "subband": calculated_receive_subband_resources["subbands"][1],
    }

    api.validate_configure_beam.assert_called_once_with(configuration=expected_request)
    api.validate_configure_scan.assert_called_once_with(configuration=pst_configure_scan_request)


def test_recv_cm_validate_configure_scan_fails_beam_configuration(
    component_manager: PstReceiveComponentManager,
    pst_configure_scan_request: dict,
    calculated_receive_subband_resources: dict,
    recv_data_host: str,
    recv_data_mac: str,
    subband_udp_ports: List[int],
) -> None:
    """Test that validate configuration when invalid beam configuration."""
    api = MagicMock()
    component_manager._api = api
    component_manager._api.get_env.return_value = {
        "data_host": recv_data_host,
        "data_mac": recv_data_mac,
        "data_port": subband_udp_ports[0],
    }

    validation_exception = ValidationError("This is not the configuration you're looking for.")
    api.validate_configure_beam.side_effect = validation_exception

    with pytest.raises(ValidationError) as exc_info:
        component_manager.validate_configure_scan(configuration=pst_configure_scan_request)

    assert exc_info.value == validation_exception

    expected_request = {
        "common": calculated_receive_subband_resources["common"],
        "subband": calculated_receive_subband_resources["subbands"][1],
    }

    api.validate_configure_beam.assert_called_once_with(configuration=expected_request)
    api.validate_configure_scan.assert_not_called()


def test_recv_cm_validate_configure_scan_fails_scan_configuration(
    component_manager: PstReceiveComponentManager,
    pst_configure_scan_request: dict,
    calculated_receive_subband_resources: dict,
    recv_data_host: str,
    recv_data_mac: str,
    subband_udp_ports: List[int],
) -> None:
    """Test that validate configuration when invalid scan configuration."""
    api = MagicMock()
    component_manager._api = api
    component_manager._api.get_env.return_value = {
        "data_host": recv_data_host,
        "data_mac": recv_data_mac,
        "data_port": subband_udp_ports[0],
    }

    validation_exception = ValidationError("That's no scan configuration")
    api.validate_configure_scan.side_effect = validation_exception

    with pytest.raises(ValidationError) as exc_info:
        component_manager.validate_configure_scan(configuration=pst_configure_scan_request)

    assert exc_info.value == validation_exception

    expected_request = {
        "common": calculated_receive_subband_resources["common"],
        "subband": calculated_receive_subband_resources["subbands"][1],
    }

    api.validate_configure_beam.assert_called_once_with(configuration=expected_request)
    api.validate_configure_scan.assert_called_once_with(configuration=pst_configure_scan_request)


def test_recv_cm_configure_beam(
    component_manager: PstReceiveComponentManager,
    pst_configure_scan_request: dict,
    calculated_receive_subband_resources: dict,
    recv_data_host: str,
    recv_data_mac: str,
    subband_udp_ports: List[int],
    subband_resources_callback: MagicMock,
) -> None:
    """Test that configure beam calls the API correctly."""
    api = MagicMock()
    component_manager._api = api
    component_manager._api.get_env.return_value = {
        "data_host": recv_data_host,
        "data_mac": recv_data_mac,
        "data_port": subband_udp_ports[0],
    }

    component_manager.configure_beam(configuration=pst_configure_scan_request)

    expected_request = {
        "common": calculated_receive_subband_resources["common"],
        "subband": calculated_receive_subband_resources["subbands"][1],
    }

    api.configure_beam.assert_called_once_with(
        configuration=expected_request,
    )
    assert component_manager.subband_beam_configuration == calculated_receive_subband_resources

    subband_resources_callback.assert_called_once_with(calculated_receive_subband_resources)


def test_recv_cm_deconfigure_beam(
    component_manager: PstReceiveComponentManager,
    property_callback: Callable,
    subband_resources_callback: MagicMock,
) -> None:
    """Test that deconfigure beam calls the API correctly."""
    api = MagicMock()
    component_manager._api = api

    component_manager.deconfigure_beam()

    api.deconfigure_beam.assert_called_once()
    assert component_manager.subband_beam_configuration == {}
    subband_resources_callback.assert_called_once_with({})


def test_recv_cm_configure_scan(
    component_manager: PstReceiveComponentManager,
    pst_configure_scan_request: dict,
) -> None:
    """Test that the component manager calls the API for configure_scan service."""
    api = MagicMock()
    component_manager._api = api

    component_manager.configure_scan(configuration=pst_configure_scan_request)

    api.configure_scan.assert_called_once_with(
        configuration=pst_configure_scan_request,
    )


def test_recv_cm_deconfigure_scan(
    component_manager: PstReceiveComponentManager,
) -> None:
    """Test that the component manager calls the API to deconfigure_scan service."""
    api = MagicMock()
    component_manager._api = api

    component_manager.deconfigure_scan()

    api.deconfigure_scan.assert_called_once()


def test_recv_cm_scan(
    component_manager: PstReceiveComponentManager,
    scan_request: dict,
) -> None:
    """Test that the component manager calls the API to start scan."""
    api = MagicMock()
    component_manager._api = api

    component_manager.scan(**scan_request)

    api.start_scan.assert_called_once_with(
        **scan_request,
    )


def test_recv_cm_end_scan(
    component_manager: PstReceiveComponentManager,
    monitor_data_callback: MagicMock,
    mocker: MockerFixture,
) -> None:
    """Test that the component manager calls the API to end scan."""
    api = MagicMock()
    component_manager._api = api
    data_handler_spy = mocker.spy(component_manager._monitor_data_handler, "reset_monitor_data")

    component_manager.end_scan()

    api.stop_scan.assert_called_once()
    api.stop_monitoring.assert_called_once()
    data_handler_spy.assert_called_once()
    assert component_manager.monitor_data == ReceiveData()
    monitor_data_callback.assert_called_once_with(dataclasses.asdict(ReceiveData()))


def test_recv_cm_abort(
    component_manager: PstReceiveComponentManager,
    monitor_data_callback: MagicMock,
    mocker: MockerFixture,
) -> None:
    """Test that the component manager calls the API to abort processes."""
    api = MagicMock()
    component_manager._api = api
    data_handler_spy = mocker.spy(component_manager._monitor_data_handler, "reset_monitor_data")

    component_manager.abort()

    api.abort.assert_called_once()
    api.stop_monitoring.assert_called_once()
    data_handler_spy.assert_called_once()
    assert component_manager.monitor_data == ReceiveData()
    monitor_data_callback.assert_called_once_with(dataclasses.asdict(ReceiveData()))


def test_recv_cm_obsreset(
    component_manager: PstReceiveComponentManager,
) -> None:
    """Test that the component manager calls the API reset service in ABORTED or FAULT state."""
    api = MagicMock()
    component_manager._api = api

    component_manager.obsreset()

    api.reset.assert_called_once()


def test_recv_cm_reset(
    component_manager: PstReceiveComponentManager,
) -> None:
    """Test that the component manager calls the API to restart service."""
    api = MagicMock()
    component_manager._api = api

    component_manager.reset()

    api.restart.assert_called_once()


def test_recv_cm_api_instance_changes_depending_on_simulation_mode(
    component_manager: PstReceiveComponentManager,
    monkeypatch: pytest.MonkeyPatch,
) -> None:
    """Test to assert that the process API changes depending on simulation mode."""
    grpc_connect_mock = MagicMock()
    grpc_disconnect_mock = MagicMock()
    simulator_disconnect_mock = MagicMock()
    simulator_connect_mock = MagicMock()

    monkeypatch.setattr(PstReceiveProcessApiSimulator, "connect", simulator_connect_mock)
    monkeypatch.setattr(PstReceiveProcessApiSimulator, "disconnect", simulator_disconnect_mock)
    monkeypatch.setattr(PstReceiveProcessApiGrpc, "connect", grpc_connect_mock)
    monkeypatch.setattr(PstReceiveProcessApiGrpc, "disconnect", grpc_disconnect_mock)

    assert component_manager.simulation_mode == SimulationMode.TRUE
    assert isinstance(component_manager._api, PstReceiveProcessApiSimulator)

    component_manager.simulation_mode = SimulationMode.FALSE
    assert isinstance(component_manager._api, PstReceiveProcessApiGrpc)

    simulator_disconnect_mock.assert_called_once()
    grpc_connect_mock.assert_called_once()

    simulator_connect_mock.assert_not_called()
    grpc_disconnect_mock.assert_not_called()

    grpc_connect_mock.reset_mock()
    grpc_disconnect_mock.reset_mock()
    simulator_disconnect_mock.reset_mock()
    simulator_connect_mock.reset_mock()

    component_manager.simulation_mode = SimulationMode.TRUE
    assert isinstance(component_manager._api, PstReceiveProcessApiSimulator)

    simulator_connect_mock.assert_called_once()
    grpc_disconnect_mock.assert_called_once()

    simulator_disconnect_mock.assert_not_called()
    grpc_connect_mock.assert_not_called()


def test_recv_cm_go_to_fault(
    component_manager: PstReceiveComponentManager,
    monitor_data_callback: MagicMock,
    mocker: MockerFixture,
) -> None:
    """Test that the component manager calls the API on go to fault."""
    api = MagicMock()
    component_manager._api = api
    data_handler_spy = mocker.spy(component_manager._monitor_data_handler, "reset_monitor_data")

    component_manager.go_to_fault(fault_msg="this is a fault message")

    api.go_to_fault.assert_called_once()

    api.stop_monitoring.assert_called_once()
    data_handler_spy.assert_called_once()
    assert component_manager.monitor_data == ReceiveData()
    monitor_data_callback.assert_called_once_with(dataclasses.asdict(ReceiveData()))


@pytest.mark.parametrize(
    "log_level",
    [
        LoggingLevel.INFO,
        LoggingLevel.DEBUG,
        LoggingLevel.FATAL,
        LoggingLevel.WARNING,
        LoggingLevel.OFF,
    ],
)
def test_recv_cm_set_logging_level(
    component_manager: PstReceiveComponentManager,
    log_level: LoggingLevel,
) -> None:
    """Test RECV component manager when set_log_level is updated."""
    api = MagicMock()
    component_manager._api = api

    component_manager.set_logging_level(log_level=log_level)
    api.set_log_level.assert_called_once_with(log_level=log_level)
