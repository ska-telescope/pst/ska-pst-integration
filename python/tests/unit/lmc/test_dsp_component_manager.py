# -*- coding: utf-8 -*-
#
# This file is part of the SKA PST project
#
# Distributed under the terms of the BSD 3-clause new license.
# See LICENSE for more info.
"""This module contains tests for the DSP component manager class."""

import logging
import queue
from unittest.mock import MagicMock

import pytest
from ska_control_model import LoggingLevel, ObsState, PstProcessingMode
from ska_pst.lmc.component import SubcomponentEventMessage
from ska_pst.lmc.dsp import PstDspComponentManager


@pytest.fixture
def mock_dsp_disk_component_manager() -> MagicMock:
    """Get a mock for the DSP.DISK sub-component."""
    return MagicMock()


@pytest.fixture
def mock_dsp_flow_through_component_manager() -> MagicMock:
    """Get a mock for the DSP.FT sub-component."""
    return MagicMock()


@pytest.fixture
def dsp_component_manager(
    device_name: str,
    beam_id: str,
    mock_dsp_disk_component_manager: MagicMock,
    mock_dsp_flow_through_component_manager: MagicMock,
    dsp_disk_process_api_endpoint: str,
    dsp_flow_through_process_api_endpoint: str,
    logger: logging.Logger,
    subcomponent_event_queue: queue.Queue[SubcomponentEventMessage],
) -> PstDspComponentManager:
    """Get the DSP component manager fixture for testing."""
    return PstDspComponentManager(
        device_name=device_name,
        beam_id=beam_id,
        dsp_disk_process_api_endpoint=dsp_disk_process_api_endpoint,
        dsp_flow_through_process_api_endpoint=dsp_flow_through_process_api_endpoint,
        dsp_disk_component_manager=mock_dsp_disk_component_manager,
        dsp_flow_through_component_manager=mock_dsp_flow_through_component_manager,
        logger=logger,
        event_queue=subcomponent_event_queue,
    )


@pytest.fixture
def current_cm(
    mock_dsp_disk_component_manager: MagicMock,
    mock_dsp_flow_through_component_manager: MagicMock,
    processing_mode: PstProcessingMode,
) -> MagicMock:
    """Get currently active DSP sub-component manager."""
    if processing_mode == PstProcessingMode.VOLTAGE_RECORDER:
        return mock_dsp_disk_component_manager
    else:
        return mock_dsp_flow_through_component_manager


@pytest.fixture
def inactive_cm(
    mock_dsp_disk_component_manager: MagicMock,
    mock_dsp_flow_through_component_manager: MagicMock,
    processing_mode: PstProcessingMode,
) -> MagicMock:
    """Get inactive DSP sub-component manager."""
    if processing_mode == PstProcessingMode.VOLTAGE_RECORDER:
        return mock_dsp_flow_through_component_manager
    else:
        return mock_dsp_disk_component_manager


@pytest.mark.parametrize(
    "processing_mode", [PstProcessingMode.FLOW_THROUGH, PstProcessingMode.VOLTAGE_RECORDER]
)
def test_dsp_cm_scan_life_cycle(
    pst_configure_scan_request: dict,
    dsp_component_manager: PstDspComponentManager,
    current_cm: MagicMock,
    inactive_cm: MagicMock,
    scan_request: dict,
) -> None:
    """Test the happy path lifecycle of the DSP component manager."""
    assert dsp_component_manager.obs_state == ObsState.EMPTY

    # assert that getting the current CM throws an exception until after configure beam
    with pytest.raises(AssertionError) as exc_info:
        dsp_component_manager.current_dsp_subcomponent

    assert str(exc_info.value) == "expected that there is a current processing mode set"

    dsp_component_manager.validate_configure_scan(configuration=pst_configure_scan_request)
    current_cm.validate_configure_scan.assert_called_once_with(configuration=pst_configure_scan_request)
    inactive_cm.validate_configure_scan.assert_not_called()

    with pytest.raises(AssertionError) as exc_info:
        dsp_component_manager.current_dsp_subcomponent

    assert str(exc_info.value) == "expected that there is a current processing mode set"

    dsp_component_manager.configure_beam(configuration=pst_configure_scan_request)
    current_cm.configure_beam.assert_called_once_with(configuration=pst_configure_scan_request)
    inactive_cm.configure_beam.assert_not_called()
    assert dsp_component_manager.current_dsp_subcomponent == current_cm
    assert dsp_component_manager.obs_state == ObsState.IDLE

    dsp_component_manager.configure_scan(configuration=pst_configure_scan_request)
    current_cm.configure_scan.assert_called_once_with(configuration=pst_configure_scan_request)
    inactive_cm.configure_scan.assert_not_called()
    assert dsp_component_manager.current_dsp_subcomponent == current_cm
    assert dsp_component_manager.obs_state == ObsState.READY

    dsp_component_manager.scan(**scan_request)
    current_cm.scan.assert_called_once_with(**scan_request)
    inactive_cm.scan.assert_not_called()
    assert dsp_component_manager.current_dsp_subcomponent == current_cm
    assert dsp_component_manager.obs_state == ObsState.SCANNING

    dsp_component_manager.end_scan()
    current_cm.scan.assert_called_once()
    inactive_cm.end_scan.assert_not_called()
    assert dsp_component_manager.current_dsp_subcomponent == current_cm
    assert dsp_component_manager.obs_state == ObsState.READY

    dsp_component_manager.deconfigure_scan()
    current_cm.deconfigure_scan.assert_called_once()
    inactive_cm.deconfigure_scan.assert_not_called()
    assert dsp_component_manager.current_dsp_subcomponent == current_cm
    assert dsp_component_manager.obs_state == ObsState.IDLE

    dsp_component_manager.deconfigure_beam()
    current_cm.deconfigure_beam.assert_called_once()
    inactive_cm.deconfigure_beam.assert_not_called()
    assert dsp_component_manager.obs_state == ObsState.EMPTY

    with pytest.raises(AssertionError) as exc_info:
        dsp_component_manager.current_dsp_subcomponent

    assert str(exc_info.value) == "expected that there is a current processing mode set"


@pytest.mark.parametrize(
    "processing_mode", [PstProcessingMode.FLOW_THROUGH, PstProcessingMode.VOLTAGE_RECORDER]
)
def test_dsp_cm_configure_beam_throws_exception_does_not_set_processing_mode(
    pst_configure_scan_request: dict,
    dsp_component_manager: PstDspComponentManager,
    current_cm: MagicMock,
) -> None:
    """Test the the DSP component ensures it handles configure_beam correctly when an exception thrown."""
    assert dsp_component_manager.obs_state == ObsState.EMPTY

    error = AssertionError("Oops something went wrong.")
    current_cm.configure_beam.side_effect = error

    with pytest.raises(AssertionError) as exc_info:
        dsp_component_manager.configure_beam(configuration=pst_configure_scan_request)

    assert exc_info.value == error
    assert dsp_component_manager._processing_mode == PstProcessingMode.IDLE
    assert dsp_component_manager.obs_state == ObsState.EMPTY

    with pytest.raises(AssertionError) as exc_info:
        dsp_component_manager.current_dsp_subcomponent

    assert str(exc_info.value) == "expected that there is a current processing mode set"


@pytest.mark.parametrize(
    "processing_mode", [PstProcessingMode.FLOW_THROUGH, PstProcessingMode.VOLTAGE_RECORDER]
)
def test_dsp_cm_abort_obsreset(
    dsp_component_manager: PstDspComponentManager,
    current_cm: MagicMock,
    inactive_cm: MagicMock,
    processing_mode: PstProcessingMode,
) -> None:
    """Test handling of abort and obsreset in DSP component manager."""
    # handle abort and reset before being configured
    dsp_component_manager.abort()
    current_cm.abort.assert_called_once()
    inactive_cm.abort.assert_called_once()
    assert dsp_component_manager.obs_state == ObsState.ABORTED

    dsp_component_manager.obsreset()
    current_cm.obsreset.assert_called_once()
    inactive_cm.obsreset.assert_called_once()
    assert dsp_component_manager.obs_state == ObsState.EMPTY

    current_cm.reset_mock()
    inactive_cm.reset_mock()

    # handle abort and reset after being configure in an processing mode
    dsp_component_manager._processing_mode = processing_mode
    dsp_component_manager._obs_state = ObsState.SCANNING

    dsp_component_manager.abort()
    current_cm.abort.assert_called_once()
    inactive_cm.abort.assert_not_called()
    assert dsp_component_manager.obs_state == ObsState.ABORTED

    dsp_component_manager.obsreset()
    current_cm.obsreset.assert_called_once()
    inactive_cm.obsreset.assert_not_called()
    assert dsp_component_manager.obs_state == ObsState.EMPTY


@pytest.mark.parametrize(
    "processing_mode", [PstProcessingMode.FLOW_THROUGH, PstProcessingMode.VOLTAGE_RECORDER]
)
def test_dsp_cm_reset(
    dsp_component_manager: PstDspComponentManager,
    current_cm: MagicMock,
    inactive_cm: MagicMock,
    processing_mode: PstProcessingMode,
) -> None:
    """Test handling reset in DSP component manager."""
    # configure
    dsp_component_manager._processing_mode = processing_mode
    assert dsp_component_manager._obs_state != ObsState.SCANNING

    # handle reset
    dsp_component_manager.reset()
    current_cm.reset.assert_called_once()
    inactive_cm.reset.assert_called_once()
    assert dsp_component_manager.obs_state == ObsState.EMPTY

    # reconfigure
    dsp_component_manager._processing_mode = processing_mode
    dsp_component_manager._obs_state = ObsState.SCANNING

    # handle reset
    dsp_component_manager.reset()
    assert current_cm.reset.call_count == 2
    assert inactive_cm.reset.call_count == 2
    assert dsp_component_manager.obs_state == ObsState.EMPTY


@pytest.mark.parametrize(
    "processing_mode", [PstProcessingMode.FLOW_THROUGH, PstProcessingMode.VOLTAGE_RECORDER]
)
def test_dsp_cm_go_to_fault(
    dsp_component_manager: PstDspComponentManager,
    mock_dsp_disk_component_manager: MagicMock,
    mock_dsp_flow_through_component_manager: MagicMock,
    processing_mode: PstProcessingMode,
) -> None:
    """Test handling of abort and obsreset in DSP component manager."""
    dsp_component_manager._processing_mode = processing_mode
    dsp_component_manager._obs_state = ObsState.SCANNING

    dsp_component_manager.go_to_fault(fault_msg="Going to fault stage")
    mock_dsp_disk_component_manager.go_to_fault.assert_called_once_with(fault_msg="Going to fault stage")
    mock_dsp_flow_through_component_manager.go_to_fault.assert_called_once_with(
        fault_msg="Going to fault stage"
    )
    assert dsp_component_manager.obs_state == ObsState.FAULT

    dsp_component_manager.obsreset()
    mock_dsp_disk_component_manager.obsreset.assert_called_once()
    mock_dsp_flow_through_component_manager.obsreset.assert_called_once()
    assert dsp_component_manager.obs_state == ObsState.EMPTY


@pytest.mark.parametrize(
    "processing_mode", [PstProcessingMode.FLOW_THROUGH, PstProcessingMode.VOLTAGE_RECORDER]
)
def test_dsp_cm_set_logging_level(
    dsp_component_manager: PstDspComponentManager,
    mock_dsp_disk_component_manager: MagicMock,
    mock_dsp_flow_through_component_manager: MagicMock,
    processing_mode: PstProcessingMode,
) -> None:
    """Test that set_logging_level is applied to all DSP sub-components regardless of obs mode."""
    dsp_component_manager._processing_mode = processing_mode
    dsp_component_manager.set_logging_level(log_level=LoggingLevel.DEBUG)
    mock_dsp_disk_component_manager.set_logging_level.assert_called_once_with(log_level=LoggingLevel.DEBUG)
    mock_dsp_flow_through_component_manager.set_logging_level.assert_called_once_with(
        log_level=LoggingLevel.DEBUG
    )


@pytest.mark.parametrize(
    "processing_mode", [PstProcessingMode.FLOW_THROUGH, PstProcessingMode.VOLTAGE_RECORDER]
)
def test_dsp_cm_reset_monitoring(
    dsp_component_manager: PstDspComponentManager,
    mock_dsp_disk_component_manager: MagicMock,
    mock_dsp_flow_through_component_manager: MagicMock,
    processing_mode: PstProcessingMode,
) -> None:
    """Test that reset_monitoring is applied to all DSP sub-components regardless of obs mode."""
    dsp_component_manager._processing_mode = processing_mode
    dsp_component_manager.reset_monitoring()
    mock_dsp_disk_component_manager.reset_monitoring.assert_called_once()
    mock_dsp_flow_through_component_manager.reset_monitoring.assert_called_once()


@pytest.mark.parametrize(
    "processing_mode", [PstProcessingMode.FLOW_THROUGH, PstProcessingMode.VOLTAGE_RECORDER]
)
def test_dsp_cm_connect(
    dsp_component_manager: PstDspComponentManager,
    mock_dsp_disk_component_manager: MagicMock,
    mock_dsp_flow_through_component_manager: MagicMock,
    processing_mode: PstProcessingMode,
) -> None:
    """Test that connect is applied to all DSP sub-components regardless of obs mode."""
    dsp_component_manager._processing_mode = processing_mode
    dsp_component_manager.connect()
    mock_dsp_disk_component_manager.connect.assert_called_once()
    mock_dsp_flow_through_component_manager.connect.assert_called_once()


@pytest.mark.parametrize(
    "processing_mode", [PstProcessingMode.FLOW_THROUGH, PstProcessingMode.VOLTAGE_RECORDER]
)
def test_dsp_cm_disconnect(
    dsp_component_manager: PstDspComponentManager,
    mock_dsp_disk_component_manager: MagicMock,
    mock_dsp_flow_through_component_manager: MagicMock,
    processing_mode: PstProcessingMode,
) -> None:
    """Test that disconnect is applied to all DSP sub-components regardless of obs mode."""
    dsp_component_manager._processing_mode = processing_mode
    dsp_component_manager.disconnect()
    mock_dsp_disk_component_manager.disconnect.assert_called_once()
    mock_dsp_flow_through_component_manager.disconnect.assert_called_once()


def test_dsp_start_health_check(
    dsp_component_manager: PstDspComponentManager,
    mock_dsp_disk_component_manager: MagicMock,
    mock_dsp_flow_through_component_manager: MagicMock,
) -> None:
    """Test that start_health_check is applied to all DSP sub-components."""
    dsp_component_manager.start_health_check()
    mock_dsp_disk_component_manager.start_health_check.assert_called_once()
    mock_dsp_flow_through_component_manager.start_health_check.assert_called_once()


def test_dsp_stop_health_check(
    dsp_component_manager: PstDspComponentManager,
    mock_dsp_disk_component_manager: MagicMock,
    mock_dsp_flow_through_component_manager: MagicMock,
) -> None:
    """Test that stop_health_check is applied to all DSP sub-components."""
    dsp_component_manager.stop_health_check()
    mock_dsp_disk_component_manager.stop_health_check.assert_called_once()
    mock_dsp_flow_through_component_manager.stop_health_check.assert_called_once()
