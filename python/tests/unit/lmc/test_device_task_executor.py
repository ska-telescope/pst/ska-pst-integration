# -*- coding: utf-8 -*-
#
# This file is part of the SKA PST project
#
# Distributed under the terms of the BSD 3-clause new license.
# See LICENSE for more info.

"""This module contains the pytest tests for the DeviceCommandTaskExecutor class."""

from __future__ import annotations

import logging
import threading
from typing import Any
from unittest.mock import MagicMock

import pytest
from ska_control_model import ResultCode
from ska_pst.lmc.job import DeviceCommandTask, DeviceCommandTaskContext, DeviceCommandTaskExecutor
from ska_pst.lmc.util import LrcResult


@pytest.fixture
def mock_task_callback() -> MagicMock:
    """Create mock fixture to use for asserting task callbacks."""
    return MagicMock()


@pytest.fixture
def mock_pst_device_proxy(mock_device_proxy: MagicMock) -> MagicMock:
    """Create a mock device proxy."""
    return MagicMock(device=mock_device_proxy)


@pytest.fixture
def mock_long_running_command(monkeypatch: pytest.MonkeyPatch) -> MagicMock:
    """Create a mock fixture to be used a test double for invoke_lrc."""
    mock_lrc = MagicMock()
    monkeypatch.setattr("ska_pst.lmc.util.LongRunningCommand", mock_lrc)
    return mock_lrc


def test_device_task_executor_lrc_ends_successfully(
    mock_pst_device_proxy: MagicMock,
    mock_device_proxy: MagicMock,
    mock_long_running_command: MagicMock,
    device_command_task_executor: DeviceCommandTaskExecutor,
    logger: logging.Logger,
) -> None:
    """Test device task executor sets task as complete when long running command completes successfully."""
    evt = threading.Event()

    def _lrc_side_effect(*args: Any, **kwargs: Any) -> LrcResult:
        return LrcResult(
            command="foobar",
            command_id="command_id",
            result_code=ResultCode.OK,
        )

    lrc_call = MagicMock()
    lrc_call.side_effect = _lrc_side_effect

    mock_long_running_command.return_value = lrc_call

    context = DeviceCommandTaskContext(
        command="foobar",
        command_args=("cat dog",),
        timeout=123.0,
        task=DeviceCommandTask(
            devices=[mock_pst_device_proxy],
            command="foobar",
            command_args=("cat dog",),
            timeout=123.0,
        ),
        evt=evt,
        device=mock_pst_device_proxy,
    )
    device_command_task_executor._task_queue.put(context)

    evt.wait(timeout=1.0)

    mock_long_running_command.assert_called_once_with(command="foobar")
    lrc_call.assert_called_once_with(
        timeout=context.timeout, proxy=mock_device_proxy, command_args=context.command_args
    )

    assert context.completed, "expected context to have completed successfully"
    assert not context.failed, "expected context to not have failed"
    assert context.exception is None, "expected context to no have an exception"


def test_device_task_executor_lrc_ends_with_exception(
    mock_pst_device_proxy: MagicMock,
    mock_device_proxy: MagicMock,
    mock_long_running_command: MagicMock,
    device_command_task_executor: DeviceCommandTaskExecutor,
    logger: logging.Logger,
) -> None:
    """Test device task executor sets task as complete when long running command completes successfully."""
    exception = ValueError("oops, something went wrong")
    evt = threading.Event()

    def _lrc_side_effect(*args: Any, **kwargs: Any) -> LrcResult:
        return LrcResult(
            command="foobar",
            command_id="command_id",
            exception=exception,
            result_code=ResultCode.FAILED,
        )

    lrc_call = MagicMock()
    lrc_call.side_effect = _lrc_side_effect

    mock_long_running_command.return_value = lrc_call

    context = DeviceCommandTaskContext(
        command="foobar",
        command_args=("cat dog",),
        timeout=123.0,
        task=DeviceCommandTask(
            devices=[mock_pst_device_proxy],
            command="foobar",
            command_args=("cat dog",),
            timeout=123.0,
        ),
        evt=evt,
        device=mock_pst_device_proxy,
    )
    device_command_task_executor._task_queue.put(context)

    evt.wait(timeout=1.0)

    mock_long_running_command.assert_called_once_with(command="foobar")
    lrc_call.assert_called_once_with(
        timeout=context.timeout, proxy=mock_device_proxy, command_args=context.command_args
    )

    assert not context.completed, "expected context to not have completed successfully"
    assert context.failed, "expected context to have failed"
    assert context.exception == exception, "expected context to have an exception"


def test_device_task_executor_lrc_ends_failure_result(
    mock_pst_device_proxy: MagicMock,
    mock_device_proxy: MagicMock,
    mock_long_running_command: MagicMock,
    device_command_task_executor: DeviceCommandTaskExecutor,
) -> None:
    """Test device task executor sets task as complete when long running command completes successfully."""
    failure_msg = "error due to some unknown remote error"
    evt = threading.Event()

    def _lrc_side_effect(*args: Any, **kwargs: Any) -> LrcResult:
        return LrcResult(
            command="foobar",
            command_id="command_id",
            result="error due to some unknown remote error",
            result_code=ResultCode.FAILED,
        )

    lrc_call = MagicMock()
    lrc_call.side_effect = _lrc_side_effect

    mock_long_running_command.return_value = lrc_call

    context = DeviceCommandTaskContext(
        command="foobar",
        command_args=("cat dog",),
        timeout=123.0,
        task=DeviceCommandTask(
            devices=[mock_pst_device_proxy],
            command="foobar",
            command_args=("cat dog",),
            timeout=123.0,
        ),
        evt=evt,
        device=mock_pst_device_proxy,
    )
    device_command_task_executor._task_queue.put(context)

    evt.wait(timeout=1.0)

    mock_long_running_command.assert_called_once_with(command="foobar")
    lrc_call.assert_called_once_with(
        timeout=context.timeout, proxy=mock_device_proxy, command_args=context.command_args
    )

    assert not context.completed, "expected context to not have completed successfully"
    assert context.failed, "expected context to have failed"
    assert context.exception is not None, "expected an exception"
    assert isinstance(context.exception, RuntimeError), "expected exception to be a RuntimeError"
    assert context.exception.args[0] == failure_msg, f"expected exception message to be {failure_msg}"
