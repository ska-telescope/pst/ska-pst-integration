# -*- coding: utf-8 -*-
#
# This file is part of the SKA PST project
#
# Distributed under the terms of the BSD 3-clause new license.
# See LICENSE for more info.
"""This module contains tests for the STAT component manager class."""

import dataclasses
import logging
import queue
from unittest.mock import MagicMock

import pytest
from pytest_mock import MockerFixture
from ska_control_model import LoggingLevel, SimulationMode
from ska_pst.lmc.component import MonitorDataHandler, SubcomponentEventMessage
from ska_pst.lmc.stat.stat_component_manager import PstStatComponentManager
from ska_pst.lmc.stat.stat_model import StatMonitorData
from ska_pst.lmc.stat.stat_process_api import (
    PstStatProcessApi,
    PstStatProcessApiGrpc,
    PstStatProcessApiSimulator,
)
from ska_pst.lmc.stat.stat_util import calculate_stat_subband_resources
from ska_pst.lmc.validation import ValidationError


@pytest.fixture
def component_manager(
    device_name: str,
    grpc_endpoint: str,
    beam_id: int,
    simulation_mode: SimulationMode,
    logger: logging.Logger,
    api: PstStatProcessApi,
    monitor_data_updated_callback: MagicMock,
    subcomponent_event_queue: queue.Queue[SubcomponentEventMessage],
) -> PstStatComponentManager:
    """Create instance of a component manager."""
    return PstStatComponentManager(
        device_name=device_name,
        process_api_endpoint=grpc_endpoint,
        simulation_mode=simulation_mode,
        logger=logger,
        api=api,
        beam_id=beam_id,
        monitor_data_updated_callback=monitor_data_updated_callback,
        event_queue=subcomponent_event_queue,
    )


@pytest.fixture
def api(
    device_name: str,
    grpc_endpoint: str,
    simulation_mode: SimulationMode,
    logger: logging.Logger,
) -> PstStatProcessApi:
    """Create an API instance."""
    if simulation_mode == SimulationMode.TRUE:
        return PstStatProcessApiSimulator(
            logger=logger,
        )
    else:
        return PstStatProcessApiGrpc(
            client_id=device_name,
            grpc_endpoint=grpc_endpoint,
            logger=logger,
        )


@pytest.fixture
def monitor_data(
    scan_request: dict,
) -> StatMonitorData:
    """Create an an instance of StatMonitorData for monitor data."""
    from ska_pst.lmc.stat.stat_simulator import PstStatSimulator

    simulator = PstStatSimulator()
    simulator.start_scan(args=scan_request)

    return simulator.get_data()


@pytest.fixture
def calculated_stat_subband_resources(beam_id: int, pst_configure_scan_request: dict) -> dict:
    """Fixture to calculate expected stat subband resources."""
    resources = calculate_stat_subband_resources(
        beam_id=beam_id,
        **pst_configure_scan_request,
    )
    return resources[1]


def test_stat_cm_connect_to_api_calls_connect_on_api(
    component_manager: PstStatComponentManager,
) -> None:
    """Assert start/stop communicating calls API."""
    api = MagicMock()
    component_manager._api = api

    component_manager.connect()
    api.connect.assert_called_once()
    api.disconnect.assert_not_called()

    component_manager.disconnect()
    api.disconnect.assert_called_once()


@pytest.mark.parametrize(
    "property",
    [
        ("real_pol_a_mean_freq_avg"),
        ("real_pol_a_variance_freq_avg"),
        ("real_pol_a_num_clipped_samples"),
        ("imag_pol_a_mean_freq_avg"),
        ("imag_pol_a_variance_freq_avg"),
        ("imag_pol_a_num_clipped_samples"),
        ("real_pol_a_mean_freq_avg_rfi_excised"),
        ("real_pol_a_variance_freq_avg_rfi_excised"),
        ("real_pol_a_num_clipped_samples_rfi_excised"),
        ("imag_pol_a_mean_freq_avg_rfi_excised"),
        ("imag_pol_a_variance_freq_avg_rfi_excised"),
        ("imag_pol_a_num_clipped_samples_rfi_excised"),
        ("real_pol_b_mean_freq_avg"),
        ("real_pol_b_variance_freq_avg"),
        ("real_pol_b_num_clipped_samples"),
        ("imag_pol_b_mean_freq_avg"),
        ("imag_pol_b_variance_freq_avg"),
        ("imag_pol_b_num_clipped_samples"),
        ("real_pol_b_mean_freq_avg_rfi_excised"),
        ("real_pol_b_variance_freq_avg_rfi_excised"),
        ("real_pol_b_num_clipped_samples_rfi_excised"),
        ("imag_pol_b_mean_freq_avg_rfi_excised"),
        ("imag_pol_b_variance_freq_avg_rfi_excised"),
        ("imag_pol_b_num_clipped_samples_rfi_excised"),
    ],
)
def test_stat_cm_properties_come_from_simulator_api_monitor_data(
    component_manager: PstStatComponentManager,
    monitor_data: StatMonitorData,
    property: str,
    monkeypatch: pytest.MonkeyPatch,
) -> None:
    """Test properties are coming from API monitor data."""
    monkeypatch.setattr(MonitorDataHandler, "monitor_data", monitor_data)

    actual = getattr(component_manager, property)
    expected = getattr(monitor_data, property)

    assert actual == expected


def test_stat_cm_api_instance_changes_depending_on_simulation_mode(
    component_manager: PstStatComponentManager,
    monkeypatch: pytest.MonkeyPatch,
) -> None:
    """Test to assert that the process API changes depending on simulation mode."""
    grpc_connect_mock = MagicMock()
    grpc_disconnect_mock = MagicMock()
    simulator_disconnect_mock = MagicMock()
    simulator_connect_mock = MagicMock()

    monkeypatch.setattr(PstStatProcessApiSimulator, "connect", simulator_connect_mock)
    monkeypatch.setattr(PstStatProcessApiSimulator, "disconnect", simulator_disconnect_mock)
    monkeypatch.setattr(PstStatProcessApiGrpc, "connect", grpc_connect_mock)
    monkeypatch.setattr(PstStatProcessApiGrpc, "disconnect", grpc_disconnect_mock)

    assert component_manager.simulation_mode == SimulationMode.TRUE
    assert isinstance(component_manager._api, PstStatProcessApiSimulator)

    component_manager.simulation_mode = SimulationMode.FALSE
    assert isinstance(component_manager._api, PstStatProcessApiGrpc)

    simulator_disconnect_mock.assert_called_once()
    grpc_connect_mock.assert_called_once()

    simulator_connect_mock.assert_not_called()
    grpc_disconnect_mock.assert_not_called()

    grpc_connect_mock.reset_mock()
    grpc_disconnect_mock.reset_mock()
    simulator_disconnect_mock.reset_mock()
    simulator_connect_mock.reset_mock()

    component_manager.simulation_mode = SimulationMode.TRUE
    assert isinstance(component_manager._api, PstStatProcessApiSimulator)

    simulator_connect_mock.assert_called_once()
    grpc_disconnect_mock.assert_called_once()

    simulator_disconnect_mock.assert_not_called()
    grpc_connect_mock.assert_not_called()


def test_stat_cm_validate_configure_scan(
    component_manager: PstStatComponentManager,
    pst_configure_scan_request: dict,
    calculated_stat_subband_resources: dict,
) -> None:
    """Test that validate configuration when valid."""
    api = MagicMock()
    component_manager._api = api

    component_manager.validate_configure_scan(configuration=pst_configure_scan_request)

    api.validate_configure_beam.assert_called_once_with(configuration=calculated_stat_subband_resources)
    api.validate_configure_scan.assert_called_once_with(configuration=pst_configure_scan_request)


def test_stat_cm_validate_configure_scan_fails_beam_configuration(
    component_manager: PstStatComponentManager,
    pst_configure_scan_request: dict,
    calculated_stat_subband_resources: dict,
) -> None:
    """Test that validate configuration when invalid beam configuration."""
    api = MagicMock()
    validation_exception = ValidationError("This is not the configuration you're looking for.")
    api.validate_configure_beam.side_effect = validation_exception

    component_manager._api = api

    with pytest.raises(ValidationError) as exc_info:
        component_manager.validate_configure_scan(configuration=pst_configure_scan_request)

    assert exc_info.value == validation_exception

    api.validate_configure_beam.assert_called_once_with(configuration=calculated_stat_subband_resources)
    api.validate_configure_scan.assert_not_called()


def test_stat_cm_validate_configure_scan_fails_scan_configuration(
    component_manager: PstStatComponentManager,
    pst_configure_scan_request: dict,
    calculated_stat_subband_resources: dict,
) -> None:
    """Test that validate configuration when invalid scan configuration."""
    api = MagicMock()
    validation_exception = ValidationError("That's no scan configuration")
    api.validate_configure_scan.side_effect = validation_exception

    component_manager._api = api

    with pytest.raises(ValidationError) as exc_info:
        component_manager.validate_configure_scan(configuration=pst_configure_scan_request)

    assert exc_info.value == validation_exception

    api.validate_configure_beam.assert_called_once_with(configuration=calculated_stat_subband_resources)
    api.validate_configure_scan.assert_called_once_with(configuration=pst_configure_scan_request)


def test_stat_cm_configure_beam(
    component_manager: PstStatComponentManager,
    pst_configure_scan_request: dict,
    calculated_stat_subband_resources: dict,
) -> None:
    """Test that configure beam calls the API correctly."""
    api = MagicMock()
    component_manager._api = api

    component_manager.configure_beam(configuration=pst_configure_scan_request)

    api.configure_beam.assert_called_once_with(
        configuration=calculated_stat_subband_resources,
    )


def test_stat_cm_deconfigure_beam(
    component_manager: PstStatComponentManager,
) -> None:
    """Test that deconfigure beam calls the API correctly."""
    api = MagicMock()
    component_manager._api = api

    component_manager.deconfigure_beam()

    api.deconfigure_beam.assert_called_once()


def test_stat_cm_configure_scan(
    component_manager: PstStatComponentManager,
    pst_configure_scan_request: dict,
) -> None:
    """Test that the component manager calls the API for configure scan."""
    api = MagicMock()
    component_manager._api = api

    component_manager.configure_scan(configuration=pst_configure_scan_request)

    api.configure_scan.assert_called_once_with(
        configuration=pst_configure_scan_request,
    )


def test_stat_cm_deconfigure_scan(
    component_manager: PstStatComponentManager,
) -> None:
    """Test that the component manager calls the API for deconfigure scan."""
    api = MagicMock()
    component_manager._api = api

    component_manager.deconfigure_scan()

    api.deconfigure_scan.assert_called_once()


def test_stat_cm_scan(
    component_manager: PstStatComponentManager,
    scan_request: dict,
    monkeypatch: pytest.MonkeyPatch,
) -> None:
    """Test that the component manager calls the API to start scan."""
    api = MagicMock()
    component_manager._api = api

    component_manager.scan(**scan_request)

    api.start_scan.assert_called_once_with(
        **scan_request,
    )
    api.monitor.assert_called_once_with(
        subband_monitor_data_callback=component_manager._monitor_data_handler.handle_subband_data,
        polling_rate=component_manager.monitoring_polling_rate_ms,
    )


def test_stat_cm_end_scan(
    component_manager: PstStatComponentManager,
    monitor_data_callback: MagicMock,
    mocker: MockerFixture,
) -> None:
    """Test that the component manager calls the API to end scan."""
    api = MagicMock()
    component_manager._api = api
    data_handler_spy = mocker.spy(component_manager._monitor_data_handler, "reset_monitor_data")

    component_manager.end_scan()

    api.stop_scan.assert_called_once()
    api.stop_monitoring.assert_called_once()
    data_handler_spy.assert_called_once()
    assert component_manager.monitor_data == StatMonitorData()
    monitor_data_callback.assert_called_once_with(dataclasses.asdict(StatMonitorData()))


def test_stat_cm_abort(
    component_manager: PstStatComponentManager,
    monitor_data_callback: MagicMock,
    mocker: MockerFixture,
) -> None:
    """Test that the component manager calls the API to abort on service."""
    api = MagicMock()
    component_manager._api = api
    data_handler_spy = mocker.spy(component_manager._monitor_data_handler, "reset_monitor_data")

    component_manager.abort()

    api.abort.assert_called_once()
    api.stop_monitoring.assert_called_once()
    data_handler_spy.assert_called_once()
    assert component_manager.monitor_data == StatMonitorData()
    monitor_data_callback.assert_called_once_with(dataclasses.asdict(StatMonitorData()))


def test_stat_cm_obsreset(
    component_manager: PstStatComponentManager,
) -> None:
    """Test that the component manager calls the API to reset service in ABORTED or FAULT state."""
    api = MagicMock()
    component_manager._api = api

    component_manager.obsreset()

    api.reset.assert_called_once()


def test_stat_cm_reset(
    component_manager: PstStatComponentManager,
) -> None:
    """Test that the component manager calls the API to restart service."""
    api = MagicMock()
    component_manager._api = api

    component_manager.reset()

    api.restart.assert_called_once()


def test_stat_cm_go_to_fault(
    component_manager: PstStatComponentManager,
    monitor_data_callback: MagicMock,
    mocker: MockerFixture,
) -> None:
    """Test that the component manager calls the API on go to fault."""
    api = MagicMock()
    component_manager._api = api
    data_handler_spy = mocker.spy(component_manager._monitor_data_handler, "reset_monitor_data")

    component_manager.go_to_fault(fault_msg="sending STAT to fault")

    api.go_to_fault.assert_called_once()

    api.stop_monitoring.assert_called_once()
    data_handler_spy.assert_called_once()
    assert component_manager.monitor_data == StatMonitorData()
    monitor_data_callback.assert_called_once_with(dataclasses.asdict(StatMonitorData()))


@pytest.mark.parametrize(
    "log_level",
    [
        LoggingLevel.INFO,
        LoggingLevel.DEBUG,
        LoggingLevel.FATAL,
        LoggingLevel.WARNING,
        LoggingLevel.OFF,
    ],
)
def test_stat_cm_set_logging_level(
    component_manager: PstStatComponentManager,
    log_level: LoggingLevel,
) -> None:
    """Test STAT component manager when set_log_level is updated."""
    api = MagicMock()
    component_manager._api = api

    component_manager.set_logging_level(log_level=log_level)
    api.set_log_level.assert_called_once_with(log_level=log_level)
