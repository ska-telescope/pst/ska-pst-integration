# -*- coding: utf-8 -*-
#
# This file is part of the SKA PST project
#
# Distributed under the terms of the BSD 3-clause new license.
# See LICENSE for more info.
"""This module contains tests for the DSP utility methods."""

from typing import List

import pytest
from ska_control_model import PstProcessingMode
from ska_pst.common.constants import COMPLEX_NDIMS
from ska_pst.lmc.dsp.dsp_util import calculate_dsp_subband_resources, generate_dsp_scan_request
from ska_pst.lmc.smrb.smrb_util import generate_data_key, generate_weights_key


def test_calculate_receive_subband_resources(
    beam_id: int,
    pst_configure_scan_request: dict,
) -> None:
    """Test that the correct DSP subband resources request is created."""
    actual = calculate_dsp_subband_resources(
        beam_id=beam_id,
        **pst_configure_scan_request,
    )

    assert 1 in actual

    actual_subband_1 = actual[1]

    assert actual_subband_1["data_key"] == generate_data_key(beam_id=beam_id, subband_id=1)
    assert actual_subband_1["weights_key"] == generate_weights_key(beam_id=beam_id, subband_id=1)


def test_generate_dsp_scan_request(pst_configure_scan_request: dict) -> None:
    """Test that we generate the correct scan configuration."""
    actual = generate_dsp_scan_request(**pst_configure_scan_request)

    assert actual["scanlen_max"] == pst_configure_scan_request["max_scan_length"]
    assert actual["bytes_per_second"] > 0


def test_generate_dsp_scan_request_when_max_scan_length_not_set(pst_configure_scan_request: dict) -> None:
    """Test that we generate the correct scan configuration when max_scan_length not set."""
    del pst_configure_scan_request["max_scan_length"]
    actual = generate_dsp_scan_request(**pst_configure_scan_request)

    assert actual["scanlen_max"] == 0.0
    assert actual["bytes_per_second"] > 0


# expected_bytes_per_second independently calculated at
# https://docs.google.com/spreadsheets/d/1vCsuVJq3MkVhKraskzFGEV1gh_U-mPzq9OnP9BkIngw
@pytest.mark.parametrize(
    ("telescope,frequency_band,bandwidth_mhz,expected_bytes_per_second"),
    [
        ("SKALow", "low", 22.22222222, 237_037_037.013),  # LowPST
        ("SKAMid", "1", 59.6736, 545_587_200.000),  # MidPSTBand1
        ("SKAMid", "2", 198.912, 1_818_624_000.000),  # MidPSTBand2
        ("SKAMid", "3", 348.096, 3_182_592_000.000),  # MidPSTBand3
        ("SKAMid", "4", 596.736, 2_727_936_000.000),  # MidPSTBand4
        ("SKAMid", "5a", 626.5728, 2_864_332_800.000),  # MidPSTBand5
    ],
)
def test_dsp_util_calc_bytes_per_second_vr(
    telescope: str,
    frequency_band: str,
    pst_configure_scan_request: dict,
    bandwidth_mhz: float,
    expected_bytes_per_second: float,
) -> None:
    """
    Test calculations for bytes_per_second.

    Test input values are based on the different telescope bands.
    """
    pst_configure_scan_request["pst_processing_mode"] = PstProcessingMode.VOLTAGE_RECORDER
    pst_configure_scan_request["eb_id"] = "fake_id"
    pst_configure_scan_request["bandwidth_mhz"] = bandwidth_mhz

    calculated_resources = generate_dsp_scan_request(
        **pst_configure_scan_request,
    )

    assert "bytes_per_second" in calculated_resources
    assert (
        abs(calculated_resources["bytes_per_second"] - expected_bytes_per_second) / expected_bytes_per_second
        < 1e-6
    )


# expected_bytes_per_second independently calculated at
# https://docs.google.com/spreadsheets/d/1vCsuVJq3MkVhKraskzFGEV1gh_U-mPzq9OnP9BkIngw
@pytest.mark.parametrize(
    (
        "telescope,frequency_band,bandwidth_mhz,num_frequency_channels,npol,"
        "nbits,oversampling_ratio,nbit_out,polarizations,channels,expected_bytes_per_second"
    ),
    [
        ("SKALow", "low", 22.22222222, 6144, 2, 16, [4, 3], 8, "Both", [1024, 5555], 87422839.497),  # LowPST
        ("SKAMid", "1", 59.6736, 1110, 2, 16, [8, 7], 4, "Both", [128, 899], 94863360.000),  # MidPSTBand1
        ("SKAMid", "2", 198.912, 3700, 2, 16, [8, 7], 2, "Both", [1350, 2649], 79872000.000),  # MidPSTBand2
        ("SKAMid", "3", 348.096, 6475, 2, 16, [8, 7], 2, "A", [2048, 4095], 62914560.000),  # MidPSTBand3
        ("SKAMid", "4", 596.736, 11100, 2, 8, [8, 7], 1, "B", [5000, 9999], 76800000.000),  # MidPSTBand4
        ("SKAMid", "5a", 626.5728, 11655, 2, 8, [8, 7], 4, "A", [300, 399], 6144000.000),  # MidPSTBand5
    ],
)
def test_dsp_util_calc_bytes_per_second_ft(
    telescope: str,
    frequency_band: str,
    pst_configure_scan_request: dict,
    bandwidth_mhz: float,
    num_frequency_channels: int,
    npol: int,
    nbits: int,
    oversampling_ratio: List[int],
    nbit_out: int,
    polarizations: str,
    channels: List[int],
    expected_bytes_per_second: float,
) -> None:
    """
    Test calculations for bytes_per_second.

    Test input values are based on the different telescope bands.
    """
    pst_configure_scan_request["pst_processing_mode"] = PstProcessingMode.FLOW_THROUGH
    pst_configure_scan_request["eb_id"] = "fake_id"
    pst_configure_scan_request["bandwidth_mhz"] = bandwidth_mhz
    pst_configure_scan_request["num_frequency_channels"] = num_frequency_channels
    pst_configure_scan_request["oversampling_ratio"] = oversampling_ratio
    pst_configure_scan_request["num_of_polarizations"] = npol
    pst_configure_scan_request["bits_per_sample"] = COMPLEX_NDIMS * nbits
    pst_configure_scan_request["flow_through_params"] = {
        "num_bits_out": nbit_out,
        "polarizations": polarizations,
        "channels": channels,
        "requantisation_scale": 1.0,
        "requantisation_init_time": 0,
    }

    calculated_resources = generate_dsp_scan_request(
        **pst_configure_scan_request,
    )

    assert "bytes_per_second" in calculated_resources
    assert (
        abs(calculated_resources["bytes_per_second"] - expected_bytes_per_second) / expected_bytes_per_second
        < 1e-6
    )
