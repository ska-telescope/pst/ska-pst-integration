# -*- coding: utf-8 -*-
#
# This file is part of the SKA PST project
#
# Distributed under the terms of the BSD 3-clause new license.
# See LICENSE for more info.
"""
This module contains tests for the base API subcomponent manager class.

This file should now be used for dealing with common functionality
for all Process API subcomponent managers.  The individual test
files should only be used for any specific handling.
"""

from __future__ import annotations

import logging
import queue
import random
import threading
import time
from typing import Any, cast
from unittest.mock import MagicMock

import pytest
from grpc import StatusCode
from overrides import override
from pytest_mock import MockerFixture
from ska_control_model import HealthState, ObsState, SimulationMode
from ska_pst.lmc.component import PstProcessApiSubcomponentManager, SubcomponentEventMessage
from ska_pst.lmc.component.grpc_lmc_client import ServiceUnavailable
from ska_pst.lmc.health_check import HealthCheckState
from ska_pst.lmc.util import BackgroundTaskProcessor


class _TestApiSubcomponentManager(
    PstProcessApiSubcomponentManager[MagicMock, MagicMock, MagicMock, MagicMock]
):

    def __init__(
        self: _TestApiSubcomponentManager,
        **kwargs: Any,
    ):
        """Initialise instance of the subcomponent manager."""
        super().__init__(
            subcomponent_name="test_api",
            data_store=MagicMock(),
            **kwargs,
        )

    @override
    def _simulator_api(self: _TestApiSubcomponentManager) -> MagicMock:
        """Get instance of the simulator API."""
        return MagicMock()

    @override
    def _grpc_api(self: _TestApiSubcomponentManager) -> MagicMock:
        """Get instance of a gRPC API."""
        return MagicMock()

    @override
    def validate_configure_scan(
        self: _TestApiSubcomponentManager, configuration: dict, **kwargs: Any
    ) -> None:
        """Validate a ConfigureScan request sent from CSP.LMC."""

    @override
    def _configure_beam(self: _TestApiSubcomponentManager, configuration: dict, **kwargs: Any) -> None:
        """Configure the beam of the the component with the resources."""


@pytest.fixture
def subcomponent_manager(
    device_name: str,
    grpc_endpoint: str,
    beam_id: int,
    simulation_mode: SimulationMode,
    logger: logging.Logger,
    api: MagicMock,
    monitor_data_updated_callback: MagicMock,
    background_task_processor: BackgroundTaskProcessor,
    subcomponent_event_queue: queue.Queue[SubcomponentEventMessage],
) -> _TestApiSubcomponentManager:
    """Create instance of a component manager."""
    cm = _TestApiSubcomponentManager(
        device_name=device_name,
        process_api_endpoint=grpc_endpoint,
        simulation_mode=simulation_mode,
        logger=logger,
        api=api,
        beam_id=beam_id,
        monitor_data_updated_callback=monitor_data_updated_callback,
        background_task_processor=background_task_processor,
        event_queue=subcomponent_event_queue,
    )
    return cm


@pytest.fixture
def api() -> MagicMock:
    """Create an API instance."""
    return MagicMock()


def test_api_sub_cm_connect_starts_health_check(
    subcomponent_manager: _TestApiSubcomponentManager,
    api: MagicMock,
) -> None:
    """Test that calling connect will start health check."""
    health_check_interval = random.randint(500, 5000)
    subcomponent_manager.health_check_interval = health_check_interval

    assert subcomponent_manager.health_state == HealthState.UNKNOWN
    subcomponent_manager.connect()
    assert subcomponent_manager.health_state == HealthState.OK

    cast(MagicMock, api.perform_health_check).assert_called_once_with(
        health_check_handler=subcomponent_manager,
        health_check_interval=health_check_interval,
    )


def test_api_sub_cm_disconnect_stops_health_check(
    subcomponent_manager: _TestApiSubcomponentManager,
    api: MagicMock,
) -> None:
    """Test that calling disconnect will stop health check."""
    subcomponent_manager._health_state = HealthState.OK

    subcomponent_manager.disconnect()
    assert subcomponent_manager.health_state == HealthState.UNKNOWN
    cast(MagicMock, api.stop_health_check).assert_called_once()


@pytest.mark.parametrize(
    "obs_state",
    [
        ObsState.EMPTY,
        ObsState.IDLE,
        ObsState.READY,
        ObsState.SCANNING,
        ObsState.ABORTED,
        ObsState.FAULT,
    ],
)
def test_api_sub_cm_health_check_with_expected_state(
    subcomponent_manager: _TestApiSubcomponentManager,
    obs_state: ObsState,
    service_name: str,
    service_uuid: str,
    mocker: MockerFixture,
) -> None:
    """Test handle of expected health check state."""
    log_spy = mocker.spy(subcomponent_manager.logger, "info")

    health_state = HealthCheckState(
        service_name=service_name,
        service_uuid=service_uuid,
        actual_service_uuid=service_uuid,
        obs_state=obs_state,
    )
    subcomponent_manager._obs_state = obs_state

    # assert we reset this value
    subcomponent_manager.health_check_state_mismatch_count = 1

    subcomponent_manager.handle_health_check_state(health_state)

    assert (
        subcomponent_manager.health_check_state_mismatch_count == 0
    ), "expected health check state mismatch count to be reset"
    log_spy.assert_called_once_with(
        f"Health check for {service_name} returned to expected state {obs_state.name}"
    )


@pytest.mark.parametrize(
    "obs_state",
    [
        ObsState.EMPTY,
        ObsState.IDLE,
        ObsState.READY,
        ObsState.SCANNING,
        ObsState.ABORTED,
    ],
)
def test_api_sub_cm_health_check_handles_unexpected_states(
    subcomponent_manager: _TestApiSubcomponentManager,
    obs_state: ObsState,
    service_name: str,
    service_uuid: str,
    mocker: MockerFixture,
) -> None:
    """Test handling of unexpected obs state when handling health check."""
    log_info_spy = mocker.spy(subcomponent_manager.logger, "info")
    log_warning_spy = mocker.spy(subcomponent_manager.logger, "warning")
    log_error_spy = mocker.spy(subcomponent_manager.logger, "error")

    expected_obs_state = obs_state
    # find a random unexpected state that is also not the FAULT state
    while expected_obs_state == obs_state or expected_obs_state == ObsState.FAULT:
        expected_obs_state = random.choice(list(ObsState))

    health_state = HealthCheckState(
        service_name=service_name,
        service_uuid=service_uuid,
        actual_service_uuid=service_uuid,
        obs_state=obs_state,
    )
    subcomponent_manager._obs_state = expected_obs_state

    assert (
        subcomponent_manager.health_check_state_mismatch_count == 0
    ), "expected health check state mismatch count to be 0"

    # handle first time that there is a mismatch
    subcomponent_manager.handle_health_check_state(health_state)
    assert (
        subcomponent_manager.health_check_state_mismatch_count == 1
    ), "expected health check state mismatch count to be 1"
    log_info_spy.assert_called_once_with(
        f"{subcomponent_manager.subcomponent_name} initial health check state mismatch - "
        f"expected {expected_obs_state.name} got {obs_state.name}"
    )

    # handle second time that there is a mismatch
    subcomponent_manager.handle_health_check_state(health_state)
    assert (
        subcomponent_manager.health_check_state_mismatch_count == 2
    ), "expected health check state mismatch count to be 2"
    log_warning_spy.assert_called_once_with(
        f"{subcomponent_manager.subcomponent_name} health check state mismatch twice - currently "
        f"expecting {expected_obs_state.name} got {obs_state.name}"
    )

    # handle third time that there is a mismatch
    subcomponent_manager.handle_health_check_state(health_state)
    assert (
        subcomponent_manager.health_check_state_mismatch_count == 3
    ), "expected health check state mismatch count to be 3"
    log_error_spy.assert_called_once_with(
        f"{subcomponent_manager.subcomponent_name} health check state mismatch more than 2 times "
        f"in a row. currently expecting {expected_obs_state.name} got {obs_state.name}"
    )

    # handle 4th time that there is a mismatch
    log_error_spy.reset_mock()
    subcomponent_manager.handle_health_check_state(health_state)
    assert (
        subcomponent_manager.health_check_state_mismatch_count == 4
    ), "expected health check state mismatch count to be 4"
    log_error_spy.assert_called_once_with(
        f"{subcomponent_manager.subcomponent_name} health check state mismatch more than 2 times "
        f"in a row. currently expecting {expected_obs_state.name} got {obs_state.name}"
    )


@pytest.mark.parametrize(
    "expected_obs_state",
    [
        ObsState.EMPTY,
        ObsState.IDLE,
        ObsState.READY,
        ObsState.SCANNING,
        ObsState.ABORTED,
    ],
)
def test_api_sub_cm_health_check_handles_unexpected_fault_state(
    subcomponent_manager: _TestApiSubcomponentManager,
    expected_obs_state: ObsState,
    service_name: str,
    service_uuid: str,
    mocker: MockerFixture,
) -> None:
    """Test handling of unexpected FAULT state for the first time."""
    log_warning_spy = mocker.spy(subcomponent_manager.logger, "warning")

    health_state = HealthCheckState(
        service_name=service_name,
        service_uuid=service_uuid,
        actual_service_uuid=service_uuid,
        obs_state=ObsState.FAULT,
        fault_message="Oops there is a fault.",
    )
    subcomponent_manager._obs_state = expected_obs_state
    assert subcomponent_manager.fault_msg is None

    subcomponent_manager.health_check_state_mismatch_count = mismatch_count = random.randint(0, 5)

    # handle first time that there is a mismatch
    subcomponent_manager.handle_health_check_state(health_state)
    assert (
        subcomponent_manager.health_check_state_mismatch_count == mismatch_count + 1
    ), f"expected health check state mismatch count to be {mismatch_count + 1}"
    log_warning_spy.assert_called_once_with(
        f"{service_name} has gone into a FAULT stage. It is expected to have been in "
        f"{expected_obs_state.name}. Fault message = '{health_state.fault_message}'"
    )
    assert subcomponent_manager.obs_state == ObsState.FAULT
    assert subcomponent_manager.fault_msg == health_state.fault_message


def test_api_sub_cm_health_check_handles_unexpected_exception(
    subcomponent_manager: _TestApiSubcomponentManager,
    api: MagicMock,
    service_name: str,
    service_uuid: str,
    mocker: MockerFixture,
) -> None:
    """Test handling of unexpected exception when performing health check."""
    subcomponent_manager._health_state = HealthState.OK

    log_error_spy = mocker.spy(subcomponent_manager.logger, "error")
    exception = ValueError("Testing an exception")

    health_state = HealthCheckState(service_name=service_name, service_uuid=service_uuid, exception=exception)

    subcomponent_manager.handle_health_check_state(health_state)
    assert subcomponent_manager.health_state == HealthState.FAILED
    assert subcomponent_manager.health_check_state_mismatch_count == 0
    cast(MagicMock, api.perform_health_check).assert_called_once()
    log_error_spy.assert_called_once_with(
        f"Health check for {service_name} has raised an " f"exception: {exception}. Restarting health check"
    )


def test_api_sub_cm_health_check_handles_exception_when_reset_is_happening(
    subcomponent_manager: _TestApiSubcomponentManager,
    api: MagicMock,
    service_name: str,
    service_uuid: str,
    monkeypatch: pytest.MonkeyPatch,
) -> None:
    """Test the component manager handles a health check exception during reset correctly."""
    subcomponent_manager._health_state = HealthState.OK
    connect_mock = MagicMock()
    monkeypatch.setattr(subcomponent_manager, "connect", connect_mock)

    # need to perform this in a background thread
    def _action() -> None:
        time.sleep(0.05)
        exception = ServiceUnavailable(error_code=StatusCode.UNAVAILABLE, message="Service unavailable")
        health_state = HealthCheckState(
            service_name=service_name, service_uuid=service_uuid, exception=exception
        )

        subcomponent_manager.handle_health_check_state(health_state)

    bg_thread = threading.Thread(target=_action, daemon=True)
    bg_thread.start()
    subcomponent_manager.reset()
    bg_thread.join()

    cast(MagicMock, api.restart).assert_called_once()
    connect_mock.assert_not_called()
    assert subcomponent_manager.health_state == HealthState.UNKNOWN
    assert subcomponent_manager.obs_state == ObsState.EMPTY


@pytest.mark.parametrize(
    "obs_state",
    [
        ObsState.EMPTY,
        ObsState.IDLE,
        ObsState.READY,
        ObsState.SCANNING,
        ObsState.ABORTED,
        ObsState.FAULT,
    ],
)
def test_api_sub_cm_sends_event_when_obs_state_updated(
    subcomponent_manager: _TestApiSubcomponentManager,
    obs_state: ObsState,
    subcomponent_event_queue: queue.Queue[SubcomponentEventMessage],
) -> None:
    """Assert that event is sent when obs_state property is updated."""
    # Set the subcomponent's health_state to a random value.  This is
    # needed so we do assertions afterwards based on obs_state changing.
    health_state = random.choice(list(HealthState))
    subcomponent_manager.health_state = health_state

    while not subcomponent_event_queue.empty():
        subcomponent_event_queue.get_nowait()

    # update the subcomponent's obs_state to force an event to be fired.
    subcomponent_manager.obs_state = obs_state

    msg = subcomponent_event_queue.get_nowait()
    expected_msg = SubcomponentEventMessage(
        subcomponent_name="test_api", health_state=health_state, obs_state=obs_state
    )

    assert msg == expected_msg


@pytest.mark.parametrize(
    "health_state",
    [
        HealthState.OK,
        HealthState.DEGRADED,
        HealthState.FAILED,
        HealthState.UNKNOWN,
    ],
)
def test_api_sub_cm_sends_event_when_health_state_updated(
    subcomponent_manager: _TestApiSubcomponentManager,
    health_state: HealthState,
    subcomponent_event_queue: queue.Queue[SubcomponentEventMessage],
) -> None:
    """Assert that event is sent when health state property is updated."""
    # Set the subcomponent's obs_state to a random value.  This is
    # needed so we do assertions afterwards based on health_state changing.
    obs_state = random.choice(list(ObsState))
    subcomponent_manager.obs_state = obs_state

    while not subcomponent_event_queue.empty():
        subcomponent_event_queue.get_nowait()

    # update the subcomponent's health_state to force an event to be fired.
    subcomponent_manager.health_state = health_state

    msg = subcomponent_event_queue.get_nowait()
    expected_msg = SubcomponentEventMessage(
        subcomponent_name="test_api", health_state=health_state, obs_state=obs_state
    )

    assert msg == expected_msg


# add test about restart health check
