# -*- coding: utf-8 -*-
#
# This file is part of the SKA PST project
#
# Distributed under the terms of the BSD 3-clause new license.
# See LICENSE for more info.

"""This module contains the pytest tests for the TaskExecutor class."""

from __future__ import annotations

from typing import List, cast
from unittest.mock import MagicMock

import numpy as np
import pytest
from pytest_mock import MockerFixture
from ska_pst.lmc import PstDeviceProxy
from ska_pst.lmc.job import (
    DeviceCommandTask,
    DeviceCommandTaskContext,
    LambdaTask,
    NoopTask,
    ParallelTask,
    SequentialTask,
    Task,
    TaskContext,
    TaskExecutor,
)


@pytest.fixture
def mock_task_callback() -> MagicMock:
    """Create mock fixture to use for asserting task callbacks."""
    return MagicMock()


def test_task_executor_with_noop_task(
    task_executor: TaskExecutor,
    mock_task_callback: MagicMock,
    mocker: MockerFixture,
) -> None:
    """Test task executor when a task is a no-op task."""
    # ensure we update routing map.
    noop_handler = mocker.spy(task_executor, "_handle_noop_task")
    task_executor.task_routing_map[NoopTask] = noop_handler

    task = NoopTask()

    task_executor.submit_job(task, mock_task_callback)

    mock_task_callback.assert_called_once_with(result=None)
    noop_handler.assert_called_once()


def test_task_executor_with_successful_lambda_task(
    task_executor: TaskExecutor,
    mock_task_callback: MagicMock,
    mocker: MockerFixture,
) -> None:
    """Test task executor when a task is a successful no-op task."""
    # ensure we update routing map.
    lambda_task_handler = mocker.spy(task_executor, "_handle_lambda_task")
    task_executor.task_routing_map[LambdaTask] = lambda_task_handler
    action = MagicMock()

    task = LambdaTask(action=action, name="test lambda task")

    task_executor.submit_job(task, mock_task_callback)

    mock_task_callback.assert_called_once_with(result=None)
    action.assert_called_once_with()
    lambda_task_handler.assert_called_once()


def test_task_executor_with_unsuccessful_lambda_task(
    task_executor: TaskExecutor,
    mock_task_callback: MagicMock,
    mocker: MockerFixture,
) -> None:
    """Test task executor when a task is an unsuccessful no-op task."""
    # ensure we update routing map.
    lambda_task_handler = mocker.spy(task_executor, "_handle_lambda_task")
    task_executor.task_routing_map[LambdaTask] = lambda_task_handler

    def _action() -> None:
        raise ValueError("oops something went wrong")

    task = LambdaTask(action=_action, name="test lambda task")

    with pytest.raises(ValueError) as excinfo:
        task_executor.submit_job(
            job=task,
            callback=mock_task_callback,
        )

    assert "oops something went wrong" in str(excinfo.value)
    mock_task_callback.assert_not_called()
    lambda_task_handler.assert_called_once()


@pytest.mark.parametrize("num_tasks", [1, 2, 3])
def test_task_executor_with_sequential_tasks(
    task_executor: TaskExecutor,
    mock_task_callback: MagicMock,
    mocker: MockerFixture,
    num_tasks: int,
) -> None:
    """Test task executor handles sequential tasks."""
    sequential_task_handler = mocker.spy(task_executor, "_handle_sequential_task")
    lambda_task_handler = mocker.spy(task_executor, "_handle_lambda_task")
    task_executor.task_routing_map[LambdaTask] = lambda_task_handler
    task_executor.task_routing_map[SequentialTask] = sequential_task_handler

    subtasks = [LambdaTask(action=MagicMock(), name=f"test lambda task {idx}") for idx in range(num_tasks)]
    job = SequentialTask(subtasks=cast(List[Task], subtasks))

    task_executor.submit_job(job, mock_task_callback)

    mock_task_callback.assert_called_once_with(result=None)
    sequential_task_handler.assert_called_once()
    for idx in range(num_tasks):
        cast(MagicMock, subtasks[idx].action).assert_called_once()

    assert lambda_task_handler.call_count == num_tasks
    calls = lambda_task_handler.call_args_list
    for idx, call in enumerate(calls):
        (context, *_), _ = call
        assert isinstance(context, TaskContext)
        assert context.completed, f"expected task {idx} to have completed successfully"
        assert not context.failed, f"expected task {idx} to not have failed"
        assert context.exception is None, f"expected task {idx} to not have an exception"
        assert context.task == subtasks[idx]


@pytest.mark.parametrize("num_tasks", [3, 4])
def test_task_executor_with_failed_sequential_tasks(
    task_executor: TaskExecutor,
    mock_task_callback: MagicMock,
    mocker: MockerFixture,
    num_tasks: int,
) -> None:
    """Test task executor handles sequential tasks when one fails."""
    failed_task = np.random.randint(1, num_tasks - 1)
    sequential_task_handler = mocker.spy(task_executor, "_handle_sequential_task")
    lambda_task_handler = mocker.spy(task_executor, "_handle_lambda_task")
    task_executor.task_routing_map[LambdaTask] = lambda_task_handler
    task_executor.task_routing_map[SequentialTask] = sequential_task_handler

    subtasks = [LambdaTask(action=MagicMock(), name=f"test lambda task {idx}") for idx in range(num_tasks)]
    job = SequentialTask(subtasks=cast(List[Task], subtasks))

    cast(MagicMock, subtasks[failed_task].action).side_effect = ValueError(f"oops task {failed_task} failed")

    with pytest.raises(ValueError) as exc_info:
        task_executor.submit_job(job, mock_task_callback)

    mock_task_callback.assert_not_called()
    sequential_task_handler.assert_called_once()

    for idx in range(num_tasks):
        if idx <= failed_task:
            cast(MagicMock, subtasks[idx].action).assert_called_once()
        else:
            cast(MagicMock, subtasks[idx].action).assert_not_called()

    assert lambda_task_handler.call_count == failed_task + 1
    calls = lambda_task_handler.call_args_list

    for idx, call in enumerate(calls):
        (context, *_), _ = call
        assert isinstance(context, TaskContext)
        if idx < failed_task:
            assert context.exception is None, f"expected task {idx} to not have an exception"
            assert context.completed, f"expected task {idx} to have completed successfully"
            assert not context.failed, f"expected task {idx} to not have failed"
        else:
            assert not context.completed, f"expected task {idx} to not have completed successfully"
            assert context.failed, f"expected task {idx} to have failed"
            assert context.exception == exc_info.value, f"expected task {idx} to have an exception"
        assert context.task == subtasks[idx]


@pytest.mark.parametrize("task_executor_max_parallel_workers", [1])
@pytest.mark.parametrize("num_tasks", [1, 2, 3])
def test_task_executor_with_parallel_tasks(
    task_executor: TaskExecutor,
    mock_task_callback: MagicMock,
    mocker: MockerFixture,
    num_tasks: int,
) -> None:
    """
    Test task executor handles parallel tasks.

    This test overrides the number of max_parallel_workers and sets it to 1 which
    results in the executor to perform the tasks in sequential order.
    """
    parallel_task_handler = mocker.spy(task_executor, "_handle_parallel_task")
    lambda_task_handler = mocker.spy(task_executor, "_handle_lambda_task")
    task_executor.task_routing_map[LambdaTask] = lambda_task_handler
    task_executor.task_routing_map[ParallelTask] = parallel_task_handler

    subtasks = [LambdaTask(action=MagicMock(), name=f"test lambda task {idx}") for idx in range(num_tasks)]
    job = ParallelTask(subtasks=cast(List[Task], subtasks))

    task_executor.submit_job(job, mock_task_callback)

    parallel_task_handler.assert_called_once()
    mock_task_callback.assert_called_once_with(result=None)
    for idx in range(num_tasks):
        cast(MagicMock, subtasks[idx].action).assert_called_once()

    assert lambda_task_handler.call_count == num_tasks
    calls = lambda_task_handler.call_args_list
    for idx, call in enumerate(calls):
        (context, *_), _ = call
        assert isinstance(context, TaskContext)
        assert context.completed, f"expected task {idx} to have completed successfully"
        assert not context.failed, f"expected task {idx} to not have failed"
        assert context.exception is None, f"expected task {idx} to not have an exception"
        assert context.task == subtasks[idx]


@pytest.mark.parametrize("task_executor_max_parallel_workers", [1])
@pytest.mark.parametrize("num_tasks", [3, 4])
def test_task_executor_with_failed_parallel_tasks(
    task_executor: TaskExecutor,
    mock_task_callback: MagicMock,
    mocker: MockerFixture,
    num_tasks: int,
) -> None:
    """
    Test task executor handles parallel tasks when one fails.

    This test overrides the number of max_parallel_workers and sets it to 1 which
    results in the executor to perform the tasks in sequential order.
    """
    failed_task = np.random.randint(1, num_tasks - 1)
    parallel_task_handler = mocker.spy(task_executor, "_handle_parallel_task")
    lambda_task_handler = mocker.spy(task_executor, "_handle_lambda_task")
    task_executor.task_routing_map[LambdaTask] = lambda_task_handler
    task_executor.task_routing_map[ParallelTask] = parallel_task_handler

    subtasks = [LambdaTask(action=MagicMock(), name=f"test lambda task {idx}") for idx in range(num_tasks)]
    job = ParallelTask(subtasks=cast(List[Task], subtasks))

    cast(MagicMock, subtasks[failed_task].action).side_effect = ValueError(f"oops task {failed_task} failed")

    with pytest.raises(ValueError) as exc_info:
        task_executor.submit_job(job, mock_task_callback)

    mock_task_callback.assert_not_called()

    for idx in range(num_tasks):
        if idx <= failed_task:
            cast(MagicMock, subtasks[idx].action).assert_called_once()
        else:
            cast(MagicMock, subtasks[idx].action).assert_not_called()

    parallel_task_handler.assert_called_once()
    assert lambda_task_handler.call_count == failed_task + 1
    calls = lambda_task_handler.call_args_list

    for idx, call in enumerate(calls):
        (context, *_), _ = call
        assert isinstance(context, TaskContext)
        if idx < failed_task:
            assert context.exception is None, f"expected task {idx} to not have an exception"
            assert context.completed, f"expected task {idx} to have completed successfully"
            assert not context.failed, f"expected task {idx} to not have failed"
        else:
            assert not context.completed, f"expected task {idx} to not have completed successfully"
            assert context.failed, f"expected task {idx} to have failed"
            assert context.exception == exc_info.value, f"expected task {idx} to have an exception"
        assert context.task == subtasks[idx]


@pytest.mark.parametrize("num_devices", [0, 1, 2, 3])
def test_task_executor_with_device_command_task(
    task_executor: TaskExecutor,
    mock_task_callback: MagicMock,
    mocker: MockerFixture,
    num_devices: int,
    monkeypatch: pytest.MonkeyPatch,
) -> None:
    """Test task executor when a task is a device command task."""

    def _device_command_task_queue_put_side_effect(task_context: DeviceCommandTaskContext) -> None:
        task_context.signal_complete()

    _device_command_task_queue_put = MagicMock()
    _device_command_task_queue_put.side_effect = _device_command_task_queue_put_side_effect

    monkeypatch.setattr(task_executor._device_command_task_queue, "put", _device_command_task_queue_put)

    device_command_task_handler = mocker.spy(task_executor, "_handle_device_command_task")
    parallel_task_handler = mocker.spy(task_executor, "_handle_parallel_task")
    task_executor.task_routing_map[DeviceCommandTask] = device_command_task_handler
    task_executor.task_routing_map[ParallelTask] = parallel_task_handler

    def _device_proxy(device_id: int) -> MagicMock:
        proxy = MagicMock()
        proxy.__repr__ = MagicMock(  # type: ignore
            return_value=f"PstDeviceProxy('test-pst/beam/{device_id:02}')"
        )
        return proxy

    device_proxies = [_device_proxy(idx) for idx in range(num_devices)]

    job = DeviceCommandTask(
        devices=cast(List[PstDeviceProxy], device_proxies),
        command="test_command",
    )

    task_executor.submit_job(job, mock_task_callback)

    calls = device_command_task_handler.call_args_list
    subtasks: List[Task] = list()
    if num_devices in {0, 1}:
        parallel_task_handler.assert_not_called()
        assert len(calls) == 1
        if num_devices == 1:
            subtasks = [job]
    else:
        # when number of devices > 1 it is split into a parallel job of individual device tasks
        assert len(calls) == num_devices + 1
        parallel_task_handler.assert_called_once()
        [call, *_] = parallel_task_handler.call_args_list
        (context, *_), _ = call
        assert isinstance(context, TaskContext)
        task = context.task
        assert isinstance(task, ParallelTask)
        assert context.completed, "expected parallel device command task have completed"
        assert not context.failed, "expected parallel device command task to have not failed"
        assert context.exception is None, "expected parallel device command task to not have an exception"
        subtasks = task.subtasks

        assert len(subtasks) == num_devices, f"expected {len(subtasks)=} to be {num_devices}"

    assert _device_command_task_queue_put.call_count == num_devices

    for idx, call in enumerate(calls):
        if idx == 0:
            continue

        (context, *_), _ = call
        assert isinstance(context, TaskContext)
        assert context.exception is None, f"expected device command task {idx-1} to not have an exception"
        assert context.completed, f"expected device command task {idx-1} to have completed successfully"
        assert not context.failed, f"expected device command task {idx-1} to not have failed"
        assert context.task == subtasks[idx - 1]


@pytest.mark.parametrize("task_executor_max_parallel_workers", [1])
@pytest.mark.parametrize("num_devices", [3, 4])
def test_task_executor_with_failing_device_command_task(
    task_executor: TaskExecutor,
    mock_task_callback: MagicMock,
    mocker: MockerFixture,
    num_devices: int,
    monkeypatch: pytest.MonkeyPatch,
) -> None:
    """Test task executor handles failing device command tasks."""
    failed_task = np.random.randint(1, num_devices - 1)
    message_count = 0

    def _device_command_task_queue_put_side_effect(task_context: DeviceCommandTaskContext) -> None:
        nonlocal message_count

        if message_count < failed_task:
            message_count += 1
            task_context.signal_complete()
        else:
            message_count += 1
            task_context.signal_failed(ValueError("failed device command task"))

    _device_command_task_queue_put = MagicMock()
    _device_command_task_queue_put.side_effect = _device_command_task_queue_put_side_effect

    monkeypatch.setattr(task_executor._device_command_task_queue, "put", _device_command_task_queue_put)

    device_command_task_handler = mocker.spy(task_executor, "_handle_device_command_task")
    parallel_task_handler = mocker.spy(task_executor, "_handle_parallel_task")
    task_executor.task_routing_map[DeviceCommandTask] = device_command_task_handler
    task_executor.task_routing_map[ParallelTask] = parallel_task_handler

    def _device_proxy(device_id: int) -> MagicMock:
        proxy = MagicMock()
        proxy.__repr__ = MagicMock(  # type: ignore
            return_value=f"PstDeviceProxy('test-pst/beam/{device_id:02}')"
        )
        return proxy

    device_proxies = [_device_proxy(idx) for idx in range(num_devices)]

    job = DeviceCommandTask(
        devices=cast(List[PstDeviceProxy], device_proxies),
        command="test_command",
    )

    with pytest.raises(ValueError) as exc_info:
        task_executor.submit_job(job, mock_task_callback)

    calls = device_command_task_handler.call_args_list
    # when number of devices > 1 it is split into a parallel job of individual device tasks
    assert len(calls) == failed_task + 2
    parallel_task_handler.assert_called_once()
    [call, *_] = parallel_task_handler.call_args_list
    (context, *_), _ = call
    assert isinstance(context, TaskContext)
    task = context.task
    assert isinstance(task, ParallelTask)
    assert context.failed, "expected parallel device command task to have failed"
    assert not context.completed, "expected parallel device command task to have not completed successfully"
    assert context.exception == exc_info.value, "expected parallel device command task to have an exception"

    subtasks = task.subtasks
    assert len(subtasks) == num_devices, f"expected {len(subtasks)=} to be {num_devices}"

    assert _device_command_task_queue_put.call_count == failed_task + 1

    for idx, call in enumerate(calls):
        if idx == 0:
            continue

        (context, *_), _ = call
        assert isinstance(context, TaskContext)
        if idx - 1 < failed_task:
            assert context.exception is None, f"expected device command task {idx-1} to not have an exception"
            assert context.completed, f"expected device command task {idx-1} have completed successfully"
            assert not context.failed, f"expected device command task {idx-1} to not have failed"
            assert context.task == subtasks[idx - 1]
        else:
            assert (
                context.exception == exc_info.value
            ), f"expected device command task {idx-1} to have an exception"
            assert (
                not context.completed
            ), f"expected device command task {idx-1} to not have completed successfully"
            assert context.failed, f"expected device command task {idx-1} to have failed"
            assert context.task == subtasks[idx - 1]
