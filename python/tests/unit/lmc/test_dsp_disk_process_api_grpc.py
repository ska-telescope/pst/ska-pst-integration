# -*- coding: utf-8 -*-
#
# This file is part of the SKA PST project
#
# Distributed under the terms of the BSD 3-clause new license.
# See LICENSE for more info.
"""This module contains tests for the DSP API."""

from __future__ import annotations

import logging
import random
import threading
import time
import unittest
from typing import Generator, cast
from unittest.mock import ANY, MagicMock, call

import grpc
import pytest
from ska_control_model import LoggingLevel, ObsState
from ska_pst.grpc.lmc.ska_pst_lmc_pb2 import (
    AbortRequest,
    AbortResponse,
    BeamConfiguration,
    ConfigureBeamRequest,
    ConfigureBeamResponse,
    ConfigureScanRequest,
    ConfigureScanResponse,
    ConnectionRequest,
    ConnectionResponse,
    DeconfigureBeamRequest,
    DeconfigureBeamResponse,
    DeconfigureScanRequest,
    DeconfigureScanResponse,
    DspDiskBeamConfiguration,
    DspDiskMonitorData,
    DspDiskScanConfiguration,
    ErrorCode,
    GetEnvironmentResponse,
    GoToFaultRequest,
    GoToFaultResponse,
    HealthCheckResponse,
    LogLevel,
    MonitorData,
    MonitorResponse,
)
from ska_pst.grpc.lmc.ska_pst_lmc_pb2 import ObsState as ObsStateGrpc
from ska_pst.grpc.lmc.ska_pst_lmc_pb2 import (
    ResetRequest,
    ResetResponse,
    ScanConfiguration,
    SetLogLevelRequest,
    SetLogLevelResponse,
    StartScanRequest,
    StartScanResponse,
    StopScanRequest,
    StopScanResponse,
)
from ska_pst.lmc.component.grpc_lmc_client import (
    BaseGrpcException,
    ResourcesAlreadyAssignedException,
    ScanConfiguredAlreadyException,
)
from ska_pst.lmc.dsp.dsp_disk_model import DspDiskSubbandMonitorData
from ska_pst.lmc.dsp.dsp_disk_process_api import PstDspDiskProcessApiGrpc
from ska_pst.lmc.dsp.dsp_util import calculate_dsp_subband_resources, generate_dsp_scan_request
from ska_pst.lmc.health_check import HealthCheckHandler, HealthCheckState
from ska_pst.lmc.test.test_grpc_server import TestMockException, TestPstLmcService
from ska_pst.lmc.validation import ValidationError


@pytest.fixture
def grpc_api(
    client_id: str,
    grpc_endpoint: str,
    logger: logging.Logger,
    pst_lmc_service: TestPstLmcService,
    service_name: str,
    service_uuid: str,
    mock_servicer_context: MagicMock,
) -> PstDspDiskProcessApiGrpc:
    """Fixture to create instance of a gRPC API with client."""
    # ensure we reset the mock before the API is going to be called.
    mock_servicer_context.reset_mock()
    api = PstDspDiskProcessApiGrpc(
        client_id=client_id,
        grpc_endpoint=grpc_endpoint,
        logger=logger,
    )
    api._grpc_client._service_name = service_name
    api._grpc_client._service_uuid = service_uuid
    return api


def test_dsp_disk_grpc_sends_connect_request(
    grpc_api: PstDspDiskProcessApiGrpc,
    mock_servicer_context: MagicMock,
    client_id: str,
) -> None:
    """Test that  API connects to the server."""
    response = ConnectionResponse()
    mock_servicer_context.connect = MagicMock(return_value=response)

    grpc_api.connect()

    mock_servicer_context.connect.assert_called_once_with(ConnectionRequest(client_id=client_id))


def test_dsp_disk_grpc_validate_configure_beam(
    grpc_api: PstDspDiskProcessApiGrpc,
    mock_servicer_context: MagicMock,
    pst_configure_scan_request: dict,
) -> None:
    """Test that  validate_configure_beam is called."""
    response = ConfigureBeamResponse()
    mock_servicer_context.configure_beam = MagicMock(return_value=response)
    configuration = calculate_dsp_subband_resources(beam_id=1, **pst_configure_scan_request)[1]

    grpc_api.validate_configure_beam(configuration=configuration)

    expected_dsp_request = DspDiskBeamConfiguration(**configuration)
    expected_request = ConfigureBeamRequest(
        beam_configuration=BeamConfiguration(dsp_disk=expected_dsp_request),
        dry_run=True,
    )
    mock_servicer_context.configure_beam.assert_called_once_with(expected_request)


def test_dsp_disk_grpc_validate_configure_beam_throws_invalid_request(
    grpc_api: PstDspDiskProcessApiGrpc,
    mock_servicer_context: MagicMock,
    pst_configure_scan_request: dict,
) -> None:
    """Test that  validate_configure_beam raises exception when there are validation errors."""
    mock_servicer_context.configure_beam.side_effect = TestMockException(
        grpc_status_code=grpc.StatusCode.FAILED_PRECONDITION,
        error_code=ErrorCode.INVALID_REQUEST,
        message="Validate configure beam error.",
    )

    configuration = calculate_dsp_subband_resources(beam_id=1, **pst_configure_scan_request)[1]

    with pytest.raises(ValidationError) as e_info:
        grpc_api.validate_configure_beam(configuration=configuration)

    assert e_info.value.message == "Validate configure beam error."

    expected_dsp_request = DspDiskBeamConfiguration(**configuration)
    expected_request = ConfigureBeamRequest(
        beam_configuration=BeamConfiguration(dsp_disk=expected_dsp_request),
        dry_run=True,
    )
    mock_servicer_context.configure_beam.assert_called_once_with(expected_request)


def test_dsp_disk_grpc_validate_configure_beam_throws_beam_already_configured(
    grpc_api: PstDspDiskProcessApiGrpc,
    mock_servicer_context: MagicMock,
    pst_configure_scan_request: dict,
) -> None:
    """Test that  validate_configure_beam raises an exception when already beam configured."""
    mock_servicer_context.configure_beam.side_effect = TestMockException(
        grpc_status_code=grpc.StatusCode.FAILED_PRECONDITION,
        error_code=ErrorCode.CONFIGURED_FOR_BEAM_ALREADY,
        message="Beam configured already.",
    )

    configuration = calculate_dsp_subband_resources(beam_id=1, **pst_configure_scan_request)[1]

    with pytest.raises(ValidationError) as e_info:
        grpc_api.validate_configure_beam(configuration=configuration)

    assert e_info.value.message == "Beam configured already."

    expected_dsp_request = DspDiskBeamConfiguration(**configuration)
    expected_request = ConfigureBeamRequest(
        beam_configuration=BeamConfiguration(dsp_disk=expected_dsp_request),
        dry_run=True,
    )
    mock_servicer_context.configure_beam.assert_called_once_with(expected_request)


def test_dsp_disk_grpc_configure_beam(
    grpc_api: PstDspDiskProcessApiGrpc,
    mock_servicer_context: MagicMock,
    pst_configure_scan_request: dict,
) -> None:
    """Test that  configure beam."""
    response = ConfigureBeamResponse()
    mock_servicer_context.configure_beam = MagicMock(return_value=response)
    configuration = calculate_dsp_subband_resources(beam_id=1, **pst_configure_scan_request)[1]

    grpc_api.configure_beam(configuration)

    expected_dsp_request = DspDiskBeamConfiguration(**configuration)
    expected_request = ConfigureBeamRequest(
        beam_configuration=BeamConfiguration(dsp_disk=expected_dsp_request),
        dry_run=False,
    )
    mock_servicer_context.configure_beam.assert_called_once_with(expected_request)


def test_dsp_disk_grpc_configure_beam_when_beam_configured(
    grpc_api: PstDspDiskProcessApiGrpc,
    mock_servicer_context: MagicMock,
    pst_configure_scan_request: dict,
) -> None:
    """Test that  configure beam when beam already configured."""
    mock_servicer_context.configure_beam.side_effect = TestMockException(
        grpc_status_code=grpc.StatusCode.FAILED_PRECONDITION,
        error_code=ErrorCode.CONFIGURED_FOR_BEAM_ALREADY,
        message="Beam has already been configured",
    )
    configuration = calculate_dsp_subband_resources(beam_id=1, **pst_configure_scan_request)[1]

    with pytest.raises(ResourcesAlreadyAssignedException) as e_info:
        grpc_api.configure_beam(configuration)

    assert e_info.value.message == "Beam has already been configured"

    expected_dsp_request = DspDiskBeamConfiguration(**configuration)
    expected_request = ConfigureBeamRequest(
        beam_configuration=BeamConfiguration(dsp_disk=expected_dsp_request),
        dry_run=False,
    )
    mock_servicer_context.configure_beam.assert_called_once_with(expected_request)


def test_dsp_disk_grpc_configure_beam_when_throws_exception(
    grpc_api: PstDspDiskProcessApiGrpc,
    mock_servicer_context: MagicMock,
    pst_configure_scan_request: dict,
) -> None:
    """Test that  configure beam throws an exception."""
    mock_servicer_context.go_to_fault = MagicMock(return_value=GoToFaultResponse())
    mock_servicer_context.configure_beam.side_effect = TestMockException(
        grpc_status_code=grpc.StatusCode.FAILED_PRECONDITION,
        error_code=ErrorCode.INTERNAL_ERROR,
        message="Internal server error occurred.",
    )
    configuration = calculate_dsp_subband_resources(beam_id=1, **pst_configure_scan_request)[1]

    with pytest.raises(BaseGrpcException) as e_info:
        grpc_api.configure_beam(configuration)

    assert e_info.value.message == "Internal server error occurred."

    expected_dsp_request = DspDiskBeamConfiguration(**configuration)
    expected_request = ConfigureBeamRequest(
        beam_configuration=BeamConfiguration(dsp_disk=expected_dsp_request),
        dry_run=False,
    )
    mock_servicer_context.configure_beam.assert_called_once_with(expected_request)
    mock_servicer_context.go_to_fault.assert_called_once_with(GoToFaultRequest())


def test_dsp_disk_grpc_deconfigure_beam(
    grpc_api: PstDspDiskProcessApiGrpc,
    mock_servicer_context: MagicMock,
) -> None:
    """Test that DSP gRPC calls deconfigure_scan on remote service."""
    response = DeconfigureBeamResponse()
    mock_servicer_context.deconfigure_beam = MagicMock(return_value=response)

    grpc_api.deconfigure_beam()

    mock_servicer_context.deconfigure_beam.assert_called_once_with(DeconfigureBeamRequest())


def test_dsp_disk_grpc_deconfigure_beam_when_no_resources_assigned(
    grpc_api: PstDspDiskProcessApiGrpc,
    mock_servicer_context: MagicMock,
) -> None:
    """Test that DSP deconfigure beam when there are not beam configured."""
    mock_servicer_context.deconfigure_beam.side_effect = TestMockException(
        grpc_status_code=grpc.StatusCode.FAILED_PRECONDITION,
        error_code=ErrorCode.NOT_CONFIGURED_FOR_BEAM,
        message="No resources have been assigned",
    )

    grpc_api.deconfigure_beam()

    mock_servicer_context.deconfigure_beam.assert_called_once_with(DeconfigureBeamRequest())


def test_dsp_disk_grpc_deconfigure_beam_when_throws_exception(
    grpc_api: PstDspDiskProcessApiGrpc,
    mock_servicer_context: MagicMock,
) -> None:
    """Test that DSP deconfigure beam when an exception is thrown."""
    mock_servicer_context.go_to_fault = MagicMock(return_value=GoToFaultResponse())
    mock_servicer_context.deconfigure_beam.side_effect = TestMockException(
        grpc_status_code=grpc.StatusCode.INTERNAL,
        message="Oops there was a problem",
    )

    with pytest.raises(BaseGrpcException) as e_info:
        grpc_api.deconfigure_beam()

    assert e_info.value.message == "Oops there was a problem"

    mock_servicer_context.deconfigure_beam.assert_called_once_with(DeconfigureBeamRequest())
    mock_servicer_context.go_to_fault.assert_called_once_with(GoToFaultRequest())


def test_dsp_disk_grpc_validate_configure_scan(
    grpc_api: PstDspDiskProcessApiGrpc,
    mock_servicer_context: MagicMock,
    pst_configure_scan_request: dict,
) -> None:
    """Test that  validate_configure_scan is called."""
    response = ConfigureScanResponse()
    mock_servicer_context.configure_scan = MagicMock(return_value=response)

    grpc_api.validate_configure_scan(pst_configure_scan_request)

    dsp_scan_request = generate_dsp_scan_request(**pst_configure_scan_request)
    assert "execution_block_id" in dsp_scan_request, "Expected key 'execution_block_id' in scan request"
    assert dsp_scan_request["execution_block_id"] == pst_configure_scan_request["eb_id"], (
        f"Expected execution_block_id to be {pst_configure_scan_request['eb_id']} "
        f"but value is {dsp_scan_request['execution_block_id']}"
    )
    expected_request = ConfigureScanRequest(
        scan_configuration=ScanConfiguration(dsp_disk=DspDiskScanConfiguration(**dsp_scan_request)),
        dry_run=True,
    )
    mock_servicer_context.configure_scan.assert_called_once_with(expected_request)


def test_dsp_disk_grpc_validate_configure_scan_throws_invalid_request(
    grpc_api: PstDspDiskProcessApiGrpc,
    mock_servicer_context: MagicMock,
    pst_configure_scan_request: dict,
) -> None:
    """Test that  validate_configure_beam raises exception when there are validation errors."""
    mock_servicer_context.configure_scan.side_effect = TestMockException(
        grpc_status_code=grpc.StatusCode.FAILED_PRECONDITION,
        error_code=ErrorCode.INVALID_REQUEST,
        message="Validate configure scan error.",
    )

    with pytest.raises(ValidationError) as e_info:
        grpc_api.validate_configure_scan(configuration=pst_configure_scan_request)

    assert e_info.value.message == "Validate configure scan error."

    expected_scan_configuration = generate_dsp_scan_request(**pst_configure_scan_request)
    expected_request = ConfigureScanRequest(
        scan_configuration=ScanConfiguration(
            dsp_disk=DspDiskScanConfiguration(**expected_scan_configuration)
        ),
        dry_run=True,
    )
    mock_servicer_context.configure_scan.assert_called_once_with(expected_request)


def test_dsp_disk_grpc_validate_configure_scan_throws_scan_already_configured(
    grpc_api: PstDspDiskProcessApiGrpc,
    mock_servicer_context: MagicMock,
    pst_configure_scan_request: dict,
) -> None:
    """Test that  validate_configure_scan raises exception when already configured for scanning."""
    mock_servicer_context.configure_scan.side_effect = TestMockException(
        grpc_status_code=grpc.StatusCode.FAILED_PRECONDITION,
        error_code=ErrorCode.CONFIGURED_FOR_SCAN_ALREADY,
        message="Already configured for scanning.",
    )

    with pytest.raises(ValidationError) as e_info:
        grpc_api.validate_configure_scan(configuration=pst_configure_scan_request)

    assert e_info.value.message == "Already configured for scanning."

    expected_scan_configuration = generate_dsp_scan_request(**pst_configure_scan_request)
    expected_request = ConfigureScanRequest(
        scan_configuration=ScanConfiguration(
            dsp_disk=DspDiskScanConfiguration(**expected_scan_configuration)
        ),
        dry_run=True,
    )
    mock_servicer_context.configure_scan.assert_called_once_with(expected_request)


def test_dsp_disk_grpc_configure_scan(
    grpc_api: PstDspDiskProcessApiGrpc,
    mock_servicer_context: MagicMock,
    pst_configure_scan_request: dict,
) -> None:
    """Test that  calls configure_scan on remote service."""
    response = ConfigureScanResponse()
    mock_servicer_context.configure_scan = MagicMock(return_value=response)

    grpc_api.configure_scan(pst_configure_scan_request)

    expected_scan_configuration = generate_dsp_scan_request(**pst_configure_scan_request)
    expected_request = ConfigureScanRequest(
        scan_configuration=ScanConfiguration(
            dsp_disk=DspDiskScanConfiguration(**expected_scan_configuration)
        ),
        dry_run=False,
    )
    mock_servicer_context.configure_scan.assert_called_once_with(expected_request)


def test_dsp_disk_grpc_configure_when_already_configured(
    grpc_api: PstDspDiskProcessApiGrpc,
    mock_servicer_context: MagicMock,
    pst_configure_scan_request: dict,
) -> None:
    """Test that  configure scan and already configured."""
    mock_servicer_context.configure_scan.side_effect = TestMockException(
        grpc_status_code=grpc.StatusCode.FAILED_PRECONDITION,
        error_code=ErrorCode.CONFIGURED_FOR_SCAN_ALREADY,
        message="Scan has already been configured.",
    )

    with pytest.raises(ScanConfiguredAlreadyException) as e_info:
        grpc_api.configure_scan(pst_configure_scan_request)

    assert e_info.value.message == "Scan has already been configured."

    expected_scan_configuration = generate_dsp_scan_request(**pst_configure_scan_request)
    expected_request = ConfigureScanRequest(
        scan_configuration=ScanConfiguration(
            dsp_disk=DspDiskScanConfiguration(**expected_scan_configuration)
        ),
        dry_run=False,
    )
    mock_servicer_context.configure_scan.assert_called_once_with(expected_request)


def test_dsp_disk_grpc_configure_when_throws_exception(
    grpc_api: PstDspDiskProcessApiGrpc,
    mock_servicer_context: MagicMock,
    pst_configure_scan_request: dict,
) -> None:
    """Test that  configure scan throws an exception."""
    mock_servicer_context.go_to_fault = MagicMock(return_value=GoToFaultResponse())
    mock_servicer_context.configure_scan.side_effect = TestMockException(
        grpc_status_code=grpc.StatusCode.FAILED_PRECONDITION,
        error_code=ErrorCode.INTERNAL_ERROR,
        message="Internal server error occurred",
    )

    with pytest.raises(BaseGrpcException) as e_info:
        grpc_api.configure_scan(pst_configure_scan_request)

    assert e_info.value.message == "Internal server error occurred"

    expected_scan_configuration = generate_dsp_scan_request(**pst_configure_scan_request)
    expected_request = ConfigureScanRequest(
        scan_configuration=ScanConfiguration(
            dsp_disk=DspDiskScanConfiguration(**expected_scan_configuration)
        ),
        dry_run=False,
    )
    mock_servicer_context.configure_scan.assert_called_once_with(expected_request)
    mock_servicer_context.go_to_fault.assert_called_once_with(GoToFaultRequest())


def test_dsp_disk_grpc_deconfigure_scan(
    grpc_api: PstDspDiskProcessApiGrpc,
    mock_servicer_context: MagicMock,
) -> None:
    """Test that  calls deconfigure_scan on remote service."""
    response = DeconfigureScanResponse()
    mock_servicer_context.deconfigure_scan = MagicMock(return_value=response)

    grpc_api.deconfigure_scan()

    mock_servicer_context.deconfigure_scan.assert_called_once_with(DeconfigureScanRequest())


def test_dsp_disk_grpc_deconfigure_when_not_configured_for_scan(
    grpc_api: PstDspDiskProcessApiGrpc,
    mock_servicer_context: MagicMock,
) -> None:
    """Test that  deconfigure scan and currently not configured."""
    mock_servicer_context.deconfigure_scan.side_effect = TestMockException(
        grpc_status_code=grpc.StatusCode.FAILED_PRECONDITION,
        error_code=ErrorCode.NOT_CONFIGURED_FOR_SCAN,
        message="Not configured for scan.",
    )
    grpc_api.deconfigure_scan()

    mock_servicer_context.deconfigure_scan.assert_called_once_with(DeconfigureScanRequest())


def test_dsp_disk_grpc_deconfigure_when_throws_exception(
    grpc_api: PstDspDiskProcessApiGrpc,
    mock_servicer_context: MagicMock,
) -> None:
    """Test that  deconfigure scan throws an exception."""
    mock_servicer_context.go_to_fault = MagicMock(return_value=GoToFaultResponse())
    mock_servicer_context.deconfigure_scan.side_effect = TestMockException(
        grpc_status_code=grpc.StatusCode.FAILED_PRECONDITION,
        error_code=ErrorCode.INTERNAL_ERROR,
        message="Internal server error occurred",
    )

    with pytest.raises(BaseGrpcException) as e_info:
        grpc_api.deconfigure_scan()

    assert e_info.value.message == "Internal server error occurred"

    mock_servicer_context.deconfigure_scan.assert_called_once_with(DeconfigureScanRequest())
    mock_servicer_context.go_to_fault.assert_called_once_with(GoToFaultRequest())


def test_dsp_disk_grpc_scan(
    grpc_api: PstDspDiskProcessApiGrpc,
    mock_servicer_context: MagicMock,
    scan_request: dict,
    expected_scan_request_protobuf: StartScanRequest,
) -> None:
    """Test that  scan."""
    response = StartScanResponse()
    mock_servicer_context.start_scan = MagicMock(return_value=response)

    grpc_api.start_scan(**scan_request)

    mock_servicer_context.start_scan.assert_called_once_with(expected_scan_request_protobuf)


def test_dsp_disk_grpc_scan_when_already_scanning(
    grpc_api: PstDspDiskProcessApiGrpc,
    mock_servicer_context: MagicMock,
    scan_request: dict,
    expected_scan_request_protobuf: StartScanRequest,
) -> None:
    """Test that  scan when already scanning."""
    mock_servicer_context.start_scan.side_effect = TestMockException(
        grpc_status_code=grpc.StatusCode.FAILED_PRECONDITION,
        error_code=ErrorCode.ALREADY_SCANNING,
        message="We are already scanning",
    )

    grpc_api.start_scan(**scan_request)

    mock_servicer_context.start_scan.assert_called_once_with(expected_scan_request_protobuf)


def test_dsp_disk_grpc_scan_when_throws_exception(
    grpc_api: PstDspDiskProcessApiGrpc,
    mock_servicer_context: MagicMock,
    scan_request: dict,
    expected_scan_request_protobuf: StartScanRequest,
) -> None:
    """Test that  scan when an exception is thrown."""
    mock_servicer_context.go_to_fault = MagicMock(return_value=GoToFaultResponse())
    mock_servicer_context.start_scan.side_effect = TestMockException(
        grpc_status_code=grpc.StatusCode.INTERNAL,
        message="Oops there was a problem",
    )

    with pytest.raises(BaseGrpcException) as e_info:
        grpc_api.start_scan(**scan_request)

    assert e_info.value.message == "Oops there was a problem"

    mock_servicer_context.start_scan.assert_called_once_with(expected_scan_request_protobuf)
    mock_servicer_context.go_to_fault.assert_called_once_with(GoToFaultRequest())


def test_dsp_disk_grpc_stop_scan(
    grpc_api: PstDspDiskProcessApiGrpc,
    mock_servicer_context: MagicMock,
) -> None:
    """Test that  end scan."""
    response = StopScanResponse()
    mock_servicer_context.stop_scan = MagicMock(return_value=response)

    with unittest.mock.patch.object(
        grpc_api, "stop_monitoring", wraps=grpc_api.stop_monitoring
    ) as stop_monitoring:
        grpc_api.stop_scan()
        stop_monitoring.assert_called_once()

    mock_servicer_context.stop_scan.assert_called_once_with(StopScanRequest())


def test_dsp_disk_grpc_stop_scan_when_not_scanning(
    grpc_api: PstDspDiskProcessApiGrpc,
    mock_servicer_context: MagicMock,
) -> None:
    """Test that  end scan when not scanning."""
    mock_servicer_context.stop_scan.side_effect = TestMockException(
        grpc_status_code=grpc.StatusCode.FAILED_PRECONDITION,
        error_code=ErrorCode.NOT_SCANNING,
        message="We're not scanning. End Scan doesn't need to do anything",
    )

    with unittest.mock.patch.object(
        grpc_api, "stop_monitoring", wraps=grpc_api.stop_monitoring
    ) as stop_monitoring:
        grpc_api.stop_scan()
        stop_monitoring.assert_called_once()

    mock_servicer_context.stop_scan.assert_called_once_with(StopScanRequest())


def test_dsp_disk_grpc_stop_scan_when_exception_thrown(
    grpc_api: PstDspDiskProcessApiGrpc,
    mock_servicer_context: MagicMock,
) -> None:
    """Test that  end scan when an exception is thrown."""
    mock_servicer_context.go_to_fault = MagicMock(return_value=GoToFaultResponse())
    mock_servicer_context.stop_scan.side_effect = TestMockException(
        grpc_status_code=grpc.StatusCode.INTERNAL,
        message="Something is wrong!",
    )

    with pytest.raises(BaseGrpcException) as e_info:
        with unittest.mock.patch.object(
            grpc_api, "stop_monitoring", wraps=grpc_api.stop_monitoring
        ) as stop_monitoring:
            grpc_api.stop_scan()
            stop_monitoring.assert_called_once()

    assert e_info.value.message == "Something is wrong!"

    mock_servicer_context.stop_scan.assert_called_once_with(StopScanRequest())
    mock_servicer_context.go_to_fault.assert_called_once_with(GoToFaultRequest())


def test_dsp_disk_grpc_abort(
    grpc_api: PstDspDiskProcessApiGrpc,
    mock_servicer_context: MagicMock,
) -> None:
    """Test that  abort."""
    response = AbortResponse()
    mock_servicer_context.abort = MagicMock(return_value=response)

    with unittest.mock.patch.object(
        grpc_api, "stop_monitoring", wraps=grpc_api.stop_monitoring
    ) as stop_monitoring:
        grpc_api.abort()
        stop_monitoring.assert_called_once()

    mock_servicer_context.abort.assert_called_once_with(AbortRequest())


def test_dsp_disk_grpc_abort_throws_exception(
    grpc_api: PstDspDiskProcessApiGrpc,
    mock_servicer_context: MagicMock,
) -> None:
    """Test that  abort when an exception is thrown."""
    mock_servicer_context.go_to_fault = MagicMock(return_value=GoToFaultResponse())
    mock_servicer_context.abort.side_effect = TestMockException(
        grpc_status_code=grpc.StatusCode.INTERNAL,
        message="We have an issue!",
    )

    with pytest.raises(BaseGrpcException) as e_info:
        with unittest.mock.patch.object(
            grpc_api, "stop_monitoring", wraps=grpc_api.stop_monitoring
        ) as stop_monitoring:
            grpc_api.abort()
            stop_monitoring.assert_called_once()

    assert e_info.value.message == "We have an issue!"

    mock_servicer_context.abort.assert_called_once_with(AbortRequest())
    mock_servicer_context.go_to_fault.assert_called_once_with(GoToFaultRequest())


def test_dsp_disk_grpc_reset(
    grpc_api: PstDspDiskProcessApiGrpc,
    mock_servicer_context: MagicMock,
) -> None:
    """Test that  reset."""
    response = ResetResponse()
    mock_servicer_context.reset = MagicMock(return_value=response)

    with unittest.mock.patch.object(
        grpc_api, "stop_monitoring", wraps=grpc_api.stop_monitoring
    ) as stop_monitoring:
        grpc_api.reset()
        stop_monitoring.assert_called_once()

    mock_servicer_context.reset.assert_called_once_with(ResetRequest())


def test_dsp_disk_grpc_reset_when_exception_thrown(
    grpc_api: PstDspDiskProcessApiGrpc,
    mock_servicer_context: MagicMock,
) -> None:
    """Test that  reset when exception is thrown."""
    mock_servicer_context.go_to_fault = MagicMock(return_value=GoToFaultResponse())
    mock_servicer_context.reset.side_effect = TestMockException(
        grpc_status_code=grpc.StatusCode.INTERNAL,
        message="Resetting error!",
    )

    with pytest.raises(BaseGrpcException) as e_info:
        with unittest.mock.patch.object(
            grpc_api, "stop_monitoring", wraps=grpc_api.stop_monitoring
        ) as stop_monitoring:
            grpc_api.reset()
            stop_monitoring.assert_called_once()

    assert e_info.value.message == "Resetting error!"

    mock_servicer_context.reset.assert_called_once_with(ResetRequest())
    mock_servicer_context.go_to_fault.assert_called_once_with(GoToFaultRequest())


def test_dsp_disk_grpc_simulated_monitor_calls_callback(
    grpc_api: PstDspDiskProcessApiGrpc,
    mock_servicer_context: MagicMock,
    subband_monitor_data_callback: MagicMock,
    abort_event: threading.Event,
    logger: logging.Logger,
) -> None:
    """Test simulated monitoring calls subband_monitor_data_callback."""
    disk_capacity = random.randint(50, 100)
    available_disk_space = random.randint(1, 50)
    data_recorded = disk_capacity - available_disk_space
    data_record_rate = 100.0 * random.random()

    def response_generator() -> Generator[MonitorResponse, None, None]:
        yield MonitorResponse(
            monitor_data=MonitorData(
                dsp_disk=DspDiskMonitorData(
                    disk_capacity=disk_capacity,
                    # Protobuf has disk_available_bytes not available_disk_space
                    disk_available_bytes=available_disk_space,
                    # Protobuf has bytes_written not data_recorded
                    bytes_written=data_recorded,
                    # Protobuf has write_rate not data_record_rate
                    write_rate=data_record_rate,
                )
            )
        )

    mock_servicer_context.monitor = MagicMock()
    mock_servicer_context.monitor.return_value = response_generator()

    grpc_api.monitor(
        subband_monitor_data_callback=subband_monitor_data_callback,
        polling_rate=50,
        monitor_abort_event=abort_event,
    )

    time.sleep(0.15)
    grpc_api.stop_monitoring()

    calls = [
        call(
            subband_id=1,
            subband_data=DspDiskSubbandMonitorData(
                disk_capacity=disk_capacity,
                available_disk_space=available_disk_space,
                data_recorded=data_recorded,
                data_record_rate=data_record_rate,
            ),
        )
    ]
    subband_monitor_data_callback.assert_has_calls(calls=calls)


def test_dsp_disk_grpc_go_to_fault(
    grpc_api: PstDspDiskProcessApiGrpc,
    mock_servicer_context: MagicMock,
) -> None:
    """Test that  go_to_fault."""
    mock_servicer_context.go_to_fault = MagicMock(return_value=GoToFaultResponse())

    with unittest.mock.patch.object(
        grpc_api, "stop_monitoring", wraps=grpc_api.stop_monitoring
    ) as stop_monitoring:
        grpc_api.go_to_fault()
        stop_monitoring.assert_called_once()

    mock_servicer_context.go_to_fault.assert_called_once_with(GoToFaultRequest())


def test_dsp_disk_grpc_api_get_env(
    grpc_api: PstDspDiskProcessApiGrpc,
    mock_servicer_context: MagicMock,
) -> None:
    """Test the get_env on gRPC API."""
    available_disk_space = random.randint(1, 30)
    disk_capacity = random.randint(30, 60)

    response = GetEnvironmentResponse()
    response.values["available_disk_space"].unsigned_int_value = available_disk_space
    response.values["disk_capacity"].unsigned_int_value = disk_capacity
    mock_servicer_context.get_env = MagicMock(return_value=response)

    actual = grpc_api.get_env()

    expected = {"available_disk_space": available_disk_space, "disk_capacity": disk_capacity}

    assert expected == actual


@pytest.mark.parametrize(
    "tango_log_level,grpc_log_level",
    [
        (LoggingLevel.INFO, LogLevel.INFO),
        (LoggingLevel.DEBUG, LogLevel.DEBUG),
        (LoggingLevel.FATAL, LogLevel.CRITICAL),
        (LoggingLevel.WARNING, LogLevel.WARNING),
        (LoggingLevel.OFF, LogLevel.INFO),
    ],
)
def test_dsp_disk_grpc_api_set_log_level(
    grpc_api: PstDspDiskProcessApiGrpc,
    mock_servicer_context: MagicMock,
    tango_log_level: LoggingLevel,
    grpc_log_level: LogLevel,
) -> None:
    """Test the set_logging_level on gRPC API."""
    response = SetLogLevelResponse()
    mock_servicer_context.set_log_level = MagicMock(return_value=response)
    log_level_request = SetLogLevelRequest(log_level=grpc_log_level)
    grpc_api.set_log_level(log_level=tango_log_level)
    mock_servicer_context.set_log_level.assert_called_once_with(log_level_request)
    mock_servicer_context.reset_mock()


def test_dsp_disk_grpc_api_perform_health_check(
    grpc_api: PstDspDiskProcessApiGrpc,
    mock_servicer_context: MagicMock,
    abort_event: threading.Event,
    service_name: str,
    service_uuid: str,
    health_check_handler: MagicMock,
) -> None:
    """Test normal response of DSP.DISK gRPC API client for health check."""
    obs_state = random.choice(
        [ObsStateGrpc.EMPTY, ObsStateGrpc.IDLE, ObsStateGrpc.READY, ObsStateGrpc.SCANNING]
    )
    expected_obs_state = ObsState(obs_state)

    health_check_response = HealthCheckResponse(
        service_name=service_name,
        uuid=service_uuid,
        obs_state=obs_state,
    )

    def response_generator() -> Generator[HealthCheckResponse, None, None]:
        while True:
            yield health_check_response

    mock_servicer_context.perform_health_check = MagicMock()
    mock_servicer_context.perform_health_check.return_value = response_generator()

    grpc_api.perform_health_check(
        health_check_handler=cast(HealthCheckHandler, health_check_handler),
        health_check_interval=50,
        health_check_abort_event=abort_event,
    )

    time.sleep(0.1)
    grpc_api.stop_health_check()

    expected_call = HealthCheckState(
        service_name=service_name,
        service_uuid=service_uuid,
        actual_service_uuid=service_uuid,
        obs_state=expected_obs_state,
    )

    cast(MagicMock, health_check_handler.handle_health_check_state).assert_called_with(expected_call)


def test_dsp_disk_grpc_api_perform_health_check_with_fault(
    grpc_api: PstDspDiskProcessApiGrpc,
    mock_servicer_context: MagicMock,
    abort_event: threading.Event,
    service_name: str,
    service_uuid: str,
    health_check_handler: MagicMock,
) -> None:
    """Test normal response of DSP.DISK gRPC API client when in FAULT state."""
    health_check_response = HealthCheckResponse(
        service_name=service_name,
        uuid=service_uuid,
        obs_state=ObsStateGrpc.FAULT,
        fault_message="This is a fault message",
    )

    def response_generator() -> Generator[HealthCheckResponse, None, None]:
        while True:
            yield health_check_response

    mock_servicer_context.perform_health_check = MagicMock()
    mock_servicer_context.perform_health_check.return_value = response_generator()

    grpc_api.perform_health_check(
        health_check_handler=cast(HealthCheckHandler, health_check_handler),
        health_check_interval=50,
        health_check_abort_event=abort_event,
    )

    time.sleep(0.1)
    grpc_api.stop_health_check()

    expected_call = HealthCheckState(
        service_name=service_name,
        service_uuid=service_uuid,
        actual_service_uuid=service_uuid,
        obs_state=ObsState.FAULT,
        fault_message="This is a fault message",
    )

    cast(MagicMock, health_check_handler.handle_health_check_state).assert_called_with(expected_call)


def test_dsp_disk_grpc_api_perform_health_check_throws_exception(
    grpc_api: PstDspDiskProcessApiGrpc,
    mock_servicer_context: MagicMock,
    abort_event: threading.Event,
    service_name: str,
    service_uuid: str,
    health_check_handler: MagicMock,
) -> None:
    """Test normal response of DSP.DISK gRPC API client when an exception is raised."""
    exception = TestMockException(
        grpc_status_code=grpc.StatusCode.CANCELLED,
        message="Service had cancelled response.",
    )

    mock_servicer_context.perform_health_check = MagicMock()
    mock_servicer_context.perform_health_check.side_effect = exception

    grpc_api.perform_health_check(
        health_check_handler=cast(HealthCheckHandler, health_check_handler),
        health_check_interval=10,
        health_check_abort_event=abort_event,
    )

    time.sleep(0.1)

    expected_call = HealthCheckState(
        service_name=service_name,
        service_uuid=service_uuid,
        exception=ANY,
    )

    cast(MagicMock, health_check_handler.handle_health_check_state).assert_called_with(expected_call)

    grpc_api.stop_health_check()
