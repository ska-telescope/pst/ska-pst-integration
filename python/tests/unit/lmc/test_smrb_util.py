# -*- coding: utf-8 -*-
#
# This file is part of the SKA PST project
#
# Distributed under the terms of the BSD 3-clause new license.
# See LICENSE for more info.
"""This module contains tests for the SMRB utility methods."""


import pytest
from ska_pst.lmc.smrb.smrb_util import (
    calculate_smrb_subband_resources,
    generate_data_key,
    generate_weights_key,
)

from ska_pst.common import TelescopeConfig


@pytest.mark.parametrize(
    "beam_id, subband_id, expected_key",
    [
        (1, 1, "0110"),
        (2, 1, "0210"),
        (3, 1, "0310"),
        (4, 1, "0410"),
        (5, 2, "0520"),
        (6, 2, "0620"),
        (7, 2, "0720"),
        (8, 2, "0820"),
        (9, 3, "0930"),
        (10, 3, "0a30"),
        (11, 3, "0b30"),
        (12, 3, "0c30"),
        (13, 4, "0d40"),
        (14, 4, "0e40"),
        (15, 4, "0f40"),
        (16, 4, "1040"),
    ],
)
def test_generate_data_key(beam_id: int, subband_id: int, expected_key: str) -> None:
    """Test generating SMRB data keys."""
    actual = generate_data_key(beam_id=beam_id, subband_id=subband_id)
    assert actual == expected_key


@pytest.mark.parametrize(
    "beam_id, subband_id, expected_key",
    [
        (1, 1, "0112"),
        (2, 1, "0212"),
        (3, 1, "0312"),
        (4, 1, "0412"),
        (5, 2, "0522"),
        (6, 2, "0622"),
        (7, 2, "0722"),
        (8, 2, "0822"),
        (9, 3, "0932"),
        (10, 3, "0a32"),
        (11, 3, "0b32"),
        (12, 3, "0c32"),
        (13, 4, "0d42"),
        (14, 4, "0e42"),
        (15, 4, "0f42"),
        (16, 4, "1042"),
    ],
)
def test_generate_weights_key(beam_id: int, subband_id: int, expected_key: str) -> None:
    """Test generating SMRB weights keys."""
    actual = generate_weights_key(beam_id=beam_id, subband_id=subband_id)
    assert actual == expected_key


@pytest.mark.parametrize(
    "telescope, frequency_band, nchan, db_bufsz, wb_bufsz, num_of_buffers",
    [
        ("SKALow", "low", 82944, 339738624, 2875392, 64),
        ("SKAMid", "1", 12950, 424345600, 26808320, 128),
        ("SKAMid", "2", 14985, 491028480, 31021056, 128),
        ("SKAMid", "3", 25900, 424345600, 26808320, 256),
        ("SKAMid", "4", 44215, 362209280, 45765632, 256),
        ("SKAMid", "5a", 46435, 380395520, 48063488, 256),
        ("SKAMid", "5b", 46435, 380395520, 48063488, 256),
    ],
)
def test_calculate_ring_buffer_sizes(
    telescope: str,
    frequency_band: str,
    nchan: int,
    db_bufsz: int,
    wb_bufsz: int,
    num_of_buffers: int,
    pst_configure_scan_request: dict,
    telescope_config: TelescopeConfig,
) -> None:
    """Test calculating of data and weight buffer sizes."""
    cbf_pst_config = telescope_config.get_cbf_pst_config(frequency_band=frequency_band)
    bandwidth_mhz = cbf_pst_config.bandwidth_mhz(nchan=nchan)

    request_params = {
        **pst_configure_scan_request,
        "cbf_pst_config": cbf_pst_config,
        "bandwidth_mhz": bandwidth_mhz,
    }

    output = calculate_smrb_subband_resources(beam_id=1, **request_params)

    assert output[1]["db_bufsz"] == db_bufsz
    assert output[1]["db_nbufs"] == num_of_buffers

    assert output[1]["wb_bufsz"] == wb_bufsz
    assert output[1]["wb_nbufs"] == num_of_buffers
