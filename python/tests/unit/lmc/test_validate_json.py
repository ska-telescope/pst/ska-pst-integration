# -*- coding: utf-8 -*-
#
# This file is part of the SKA PST project
#
# Distributed under the terms of the BSD 3-clause new license.
# See LICENSE for more info.
"""This module contains the pytest tests for the validate method."""

import json

import pytest
from jsonschema.exceptions import ValidationError
from ska_pst.lmc.validation import PstConfigValidator, PstScanValidator
from ska_telmodel._common import split_interface_version
from ska_telmodel.csp.examples import get_csp_config_example
from ska_telmodel.csp.version import CSP_CONFIG_VERSIONS

DEFAULT_MIN_VERSION = (2, 4)
FLOW_THROUGH_MODE_MIN_VERSION = (2, 5)


def is_valid_combination(version: str, scan: str | None) -> bool:
    """
    Check if version and scan paramters are valid combinations.

    :param version: version number for the CSP configuration.
    :type version: str
    :param scan: the type of CSP scan to validate.
    :type scan: Optional[str]
    :returns: true or false if not a valid version and scan combination.
    """
    if scan is None or scan not in [
        "pst_beam",
        "pst_scan_pt",
        "pst_scan_ds",
        "pst_scan_ft",
        "pst_scan_vr",
    ]:
        return False

    (major, minor) = split_interface_version(version)

    if scan == "pst_scan_ft":
        return (major, minor) >= FLOW_THROUGH_MODE_MIN_VERSION

    return (major, minor) >= DEFAULT_MIN_VERSION


@pytest.fixture
def config_validator() -> PstConfigValidator:
    """Get an instance of a config validator."""
    return PstConfigValidator()


@pytest.fixture
def scan_validator() -> PstScanValidator:
    """Get an instance of a scan validator."""
    return PstScanValidator()


@pytest.mark.parametrize(
    "version, valid, scan",
    [
        (v, is_valid_combination(v, scan), scan)
        for v in CSP_CONFIG_VERSIONS
        for scan in [
            None,
            "cal_a",
            "science_a",
            "pst_beam",
            "pst_scan_pt",
            "pst_scan_ds",
            "pst_scan_ft",
            "pst_scan_vr",
        ]
    ],
)
def test_only_version_2_4_or_above_accepted_for_configure_scan_validation(
    version: str, valid: bool, scan: str | None, config_validator: PstConfigValidator
) -> None:
    """
    Test that only version 2.4 of CSP schema is valid.

    Parameterised test that checks different combinations of version and scan valid to assert that only
    version 2.4 above and PST scans are valid.

    :param version: version number for the CSP configuration.
    :type version: str
    :param valid: the expected value of if this a valid version and scan combination.
    :type valid: bool
    :param scan: the type of CSP scan to validate.
    :type scan: str
    """
    try:
        (major, minor) = split_interface_version(version=version)
        config = get_csp_config_example(version=version, scan=scan)

        # remove cbf, pss, and subarray values
        for key in ["cbf", "subarray", "pss"]:
            if key in config:
                del config[key]

        request = json.dumps(config)

        if valid:
            config_validator.validate(request)
        else:
            has_pst_scan = "pst" in config and "scan" in config["pst"]

            if "interface" not in config:
                with pytest.raises(ValueError, match="'interface' field not in request"):
                    config_validator.validate(request)
            elif (major, minor) >= DEFAULT_MIN_VERSION and scan and scan.endswith("_ft"):
                with pytest.raises(ValueError, match=r"should be >= 2.5"):
                    config_validator.validate(request)
            elif (major, minor) >= DEFAULT_MIN_VERSION and not has_pst_scan:
                with pytest.raises(ValueError, match="Validation 'scan' field not in request"):
                    config_validator.validate(request)
            else:
                with pytest.raises(ValueError, match=r"should be >= 2.4"):
                    config_validator.validate(request)
    except ValueError:
        pass


def test_scan_validator_accepts_scan_json_dict(scan_validator: PstScanValidator, scan_id: int) -> None:
    """Test that the scan validator handles full json requests."""
    scan_request = {"scan_id": scan_id}
    request = json.dumps(scan_request)

    scan_validator.validate(request)


def test_scan_validator_rejects_invalid_scan_request(scan_validator: PstScanValidator) -> None:
    """Test that the scan validator rejects request that has no scan_id."""
    scan_request = {"foo": "bar"}
    request = json.dumps(scan_request)

    with pytest.raises(ValidationError, match="'scan_id' is a required property"):
        scan_validator.validate(request)


def test_scan_validator_accepts_scan_id_as_string(scan_validator: PstScanValidator, scan_id: int) -> None:
    """Test that the scan validator handles that request where the scan_id is a string."""
    scan_validator.validate(str(scan_id))


def test_scan_validator_accepts_scan_id_int_as_json_string(
    scan_validator: PstScanValidator, scan_id: int
) -> None:
    """Test that the scan validator handles that request where the scan_id int but json stringified."""
    scan_validator.validate(json.dumps(scan_id))
