# -*- coding: utf-8 -*-
#
# This file is part of the SKA PST project
#
# Distributed under the terms of the BSD 3-clause new license.
# See LICENSE for more info.
"""This module contains tests for the RECV API."""

from __future__ import annotations

import copy
import logging
import random
import threading
import time
import unittest
from typing import Generator, List, cast
from unittest.mock import ANY, MagicMock, call

import grpc
import numpy as np
import pytest
from ska_control_model import LoggingLevel, ObsState
from ska_pst.grpc.lmc.ska_pst_lmc_pb2 import (
    AbortRequest,
    AbortResponse,
    BeamConfiguration,
    ConfigureBeamRequest,
    ConfigureBeamResponse,
    ConfigureScanRequest,
    ConfigureScanResponse,
    ConnectionRequest,
    ConnectionResponse,
    DeconfigureBeamRequest,
    DeconfigureBeamResponse,
    DeconfigureScanRequest,
    DeconfigureScanResponse,
    ErrorCode,
    GetEnvironmentResponse,
    GoToFaultRequest,
    GoToFaultResponse,
    HealthCheckResponse,
    LogLevel,
    MonitorData,
    MonitorResponse,
)
from ska_pst.grpc.lmc.ska_pst_lmc_pb2 import ObsState as ObsStateGrpc
from ska_pst.grpc.lmc.ska_pst_lmc_pb2 import (
    ReceiveBeamConfiguration,
    ReceiveMonitorData,
    ReceiveScanConfiguration,
    ReceiveSubbandResources,
    ResetRequest,
    ResetResponse,
    ScanConfiguration,
    SetLogLevelRequest,
    SetLogLevelResponse,
    StartScanRequest,
    StartScanResponse,
    StopScanRequest,
    StopScanResponse,
)
from ska_pst.lmc.component.grpc_lmc_client import (
    BaseGrpcException,
    ResourcesAlreadyAssignedException,
    ScanConfiguredAlreadyException,
)
from ska_pst.lmc.health_check import HealthCheckHandler, HealthCheckState
from ska_pst.lmc.receive.receive_model import ReceiveData
from ska_pst.lmc.receive.receive_process_api import (
    GIGABITS_PER_BYTE,
    PstReceiveProcessApi,
    PstReceiveProcessApiGrpc,
)
from ska_pst.lmc.receive.receive_util import (
    calculate_receive_subband_resources,
    generate_recv_configure_scan_request,
)
from ska_pst.lmc.test.test_grpc_server import TestMockException, TestPstLmcService
from ska_pst.lmc.validation import ValidationError


@pytest.fixture
def grpc_api(
    client_id: str,
    grpc_endpoint: str,
    logger: logging.Logger,
    pst_lmc_service: TestPstLmcService,
    service_name: str,
    service_uuid: str,
    mock_servicer_context: MagicMock,
) -> PstReceiveProcessApi:
    """Fixture to create instance of a gRPC API with client."""
    # ensure we reset the mock before the API is going to be called.
    mock_servicer_context.reset_mock()
    api = PstReceiveProcessApiGrpc(
        client_id=client_id,
        grpc_endpoint=grpc_endpoint,
        logger=logger,
    )
    api._grpc_client._service_name = service_name
    api._grpc_client._service_uuid = service_uuid
    return api


@pytest.fixture
def subband_id() -> int:
    """Create subband."""
    return 1


@pytest.fixture
def calculated_receive_subband_resources(
    beam_id: int,
    pst_configure_scan_request: dict,
    recv_data_host: str,
    recv_data_mac: str,
    subband_udp_ports: List[int],
) -> dict:
    """Calculate RECV subband resources."""
    return calculate_receive_subband_resources(
        beam_id=beam_id,
        data_host=recv_data_host,
        data_mac=recv_data_mac,
        subband_udp_ports=subband_udp_ports,
        **pst_configure_scan_request,
    )


@pytest.fixture
def mapped_configure_request(
    pst_configure_scan_request: dict,
) -> dict:
    """Map configure scan request to RECV properties."""
    return generate_recv_configure_scan_request(**pst_configure_scan_request)


@pytest.fixture
def subband_configure_beam_request(
    subband_id: int,
    calculated_receive_subband_resources: dict,
) -> dict:
    """Create RECV subband request from calculated subband resources."""
    return {
        "common": calculated_receive_subband_resources["common"],
        "subband": calculated_receive_subband_resources["subbands"][subband_id],
    }


@pytest.fixture
def expected_beam_configuration_protobuf(
    subband_configure_beam_request: dict,
) -> ReceiveBeamConfiguration:
    """Create expected protobuf resources message for RECV."""
    subband_configure_beam_request = copy.deepcopy(subband_configure_beam_request)
    del subband_configure_beam_request["subband"]["start_centre_freq_mhz"]

    return ReceiveBeamConfiguration(
        **subband_configure_beam_request["common"],
        subband_resources=ReceiveSubbandResources(
            **subband_configure_beam_request["subband"],
        ),
    )


@pytest.fixture
def expected_receive_configure_protobuf(
    mapped_configure_request: dict,
) -> ReceiveScanConfiguration:
    """Fixture to build expected RECV scan configuration request."""
    return ReceiveScanConfiguration(**mapped_configure_request)


def test_receive_grpc_sends_connect_request(
    grpc_api: PstReceiveProcessApiGrpc,
    mock_servicer_context: MagicMock,
    client_id: str,
) -> None:
    """Test that RECV gRPC API connects to the server."""
    response = ConnectionResponse()
    mock_servicer_context.connect = MagicMock(return_value=response)

    grpc_api.connect()

    mock_servicer_context.connect.assert_called_once_with(ConnectionRequest(client_id=client_id))


def test_receive_grpc_validate_configure_beam(
    grpc_api: PstReceiveProcessApiGrpc,
    mock_servicer_context: MagicMock,
    subband_configure_beam_request: dict,
    expected_beam_configuration_protobuf: ReceiveBeamConfiguration,
) -> None:
    """Test that RECV gRPC validate_configure_beam is called."""
    response = ConfigureBeamResponse()
    mock_servicer_context.configure_beam = MagicMock(return_value=response)

    grpc_api.validate_configure_beam(configuration=subband_configure_beam_request)

    expected_request = ConfigureBeamRequest(
        beam_configuration=BeamConfiguration(receive=expected_beam_configuration_protobuf),
        dry_run=True,
    )
    mock_servicer_context.configure_beam.assert_called_once_with(expected_request)


def test_receive_grpc_validate_configure_beam_throws_invalid_request(
    grpc_api: PstReceiveProcessApiGrpc,
    mock_servicer_context: MagicMock,
    subband_configure_beam_request: dict,
    expected_beam_configuration_protobuf: ReceiveBeamConfiguration,
) -> None:
    """Test that RECV gRPC validate_configure_beam throws exception."""
    mock_servicer_context.configure_beam.side_effect = TestMockException(
        grpc_status_code=grpc.StatusCode.FAILED_PRECONDITION,
        error_code=ErrorCode.INVALID_REQUEST,
        message="Validate configure beam error.",
    )

    with pytest.raises(ValidationError) as e_info:
        grpc_api.validate_configure_beam(configuration=subband_configure_beam_request)

    assert e_info.value.message == "Validate configure beam error."

    expected_request = ConfigureBeamRequest(
        beam_configuration=BeamConfiguration(receive=expected_beam_configuration_protobuf),
        dry_run=True,
    )
    mock_servicer_context.configure_beam.assert_called_once_with(expected_request)


def test_receive_grpc_validate_configure_beam_throws_resources_already_assigned(
    grpc_api: PstReceiveProcessApiGrpc,
    mock_servicer_context: MagicMock,
    subband_configure_beam_request: dict,
    expected_beam_configuration_protobuf: ReceiveBeamConfiguration,
) -> None:
    """Test that validate_configure_beam throws exception when already beam configured."""
    mock_servicer_context.configure_beam.side_effect = TestMockException(
        grpc_status_code=grpc.StatusCode.FAILED_PRECONDITION,
        error_code=ErrorCode.CONFIGURED_FOR_BEAM_ALREADY,
        message="Beam configured already.",
    )

    with pytest.raises(ValidationError) as e_info:
        grpc_api.validate_configure_beam(configuration=subband_configure_beam_request)

    assert e_info.value.message == "Beam configured already."

    expected_request = ConfigureBeamRequest(
        beam_configuration=BeamConfiguration(receive=expected_beam_configuration_protobuf),
        dry_run=True,
    )
    mock_servicer_context.configure_beam.assert_called_once_with(expected_request)


def test_receive_grpc_configure_beam(
    grpc_api: PstReceiveProcessApiGrpc,
    mock_servicer_context: MagicMock,
    subband_configure_beam_request: dict,
    expected_beam_configuration_protobuf: ReceiveBeamConfiguration,
) -> None:
    """Test that RECV gRPC configure beam."""
    response = ConfigureBeamResponse()
    mock_servicer_context.configure_beam = MagicMock(return_value=response)

    grpc_api.configure_beam(
        subband_configure_beam_request,
    )

    expected_request = ConfigureBeamRequest(
        beam_configuration=BeamConfiguration(receive=expected_beam_configuration_protobuf)
    )
    mock_servicer_context.configure_beam.assert_called_once_with(expected_request)


def test_receive_grpc_configure_beam_when_beam_configured(
    grpc_api: PstReceiveProcessApiGrpc,
    mock_servicer_context: MagicMock,
    subband_configure_beam_request: dict,
    expected_beam_configuration_protobuf: ReceiveBeamConfiguration,
) -> None:
    """Test that RECV gRPC configure beam when beam already configured."""
    mock_servicer_context.configure_beam.side_effect = TestMockException(
        grpc_status_code=grpc.StatusCode.FAILED_PRECONDITION,
        error_code=ErrorCode.CONFIGURED_FOR_BEAM_ALREADY,
        message="Beam has already been configured",
    )

    with pytest.raises(ResourcesAlreadyAssignedException) as e_info:
        grpc_api.configure_beam(subband_configure_beam_request)

    assert e_info.value.message == "Beam has already been configured"

    expected_request = ConfigureBeamRequest(
        beam_configuration=BeamConfiguration(receive=expected_beam_configuration_protobuf)
    )
    mock_servicer_context.configure_beam.assert_called_once_with(expected_request)


def test_receive_grpc_configure_beam_when_throws_exception(
    grpc_api: PstReceiveProcessApiGrpc,
    mock_servicer_context: MagicMock,
    subband_configure_beam_request: dict,
    expected_beam_configuration_protobuf: ReceiveBeamConfiguration,
) -> None:
    """Test that RECV gRPC configure beam throws an exception."""
    mock_servicer_context.go_to_fault = MagicMock(return_value=GoToFaultResponse())
    mock_servicer_context.configure_beam.side_effect = TestMockException(
        grpc_status_code=grpc.StatusCode.FAILED_PRECONDITION,
        error_code=ErrorCode.INTERNAL_ERROR,
        message="Internal server error occurred.",
    )

    with pytest.raises(BaseGrpcException) as e_info:
        grpc_api.configure_beam(subband_configure_beam_request)

    assert e_info.value.message == "Internal server error occurred."

    expected_request = ConfigureBeamRequest(
        beam_configuration=BeamConfiguration(receive=expected_beam_configuration_protobuf)
    )
    mock_servicer_context.configure_beam.assert_called_once_with(expected_request)
    mock_servicer_context.go_to_fault.assert_called_once_with(GoToFaultRequest())


def test_receive_grpc_deconfigure_beam(
    grpc_api: PstReceiveProcessApiGrpc,
    mock_servicer_context: MagicMock,
) -> None:
    """Test that RECV gRPC deconfigure beam."""
    response = DeconfigureBeamResponse()
    mock_servicer_context.deconfigure_beam = MagicMock(return_value=response)

    grpc_api.deconfigure_beam()

    mock_servicer_context.deconfigure_beam.assert_called_once_with(DeconfigureBeamRequest())


def test_receive_grpc_deconfigure_beam_when_no_resources_assigned(
    grpc_api: PstReceiveProcessApiGrpc,
    mock_servicer_context: MagicMock,
) -> None:
    """Test that RECV deconfigure beam when there are not beam configured."""
    mock_servicer_context.deconfigure_beam.side_effect = TestMockException(
        grpc_status_code=grpc.StatusCode.FAILED_PRECONDITION,
        error_code=ErrorCode.NOT_CONFIGURED_FOR_BEAM,
        message="No resources have been assigned",
    )

    grpc_api.deconfigure_beam()

    mock_servicer_context.deconfigure_beam.assert_called_once_with(DeconfigureBeamRequest())


def test_receive_grpc_deconfigure_beam_when_throws_exception(
    grpc_api: PstReceiveProcessApiGrpc,
    mock_servicer_context: MagicMock,
) -> None:
    """Test that RECV deconfigure beam when an exception is thrown."""
    mock_servicer_context.go_to_fault = MagicMock(return_value=GoToFaultResponse())
    mock_servicer_context.deconfigure_beam.side_effect = TestMockException(
        grpc_status_code=grpc.StatusCode.INTERNAL,
        message="Oops there was a problem",
    )

    with pytest.raises(BaseGrpcException) as e_info:
        grpc_api.deconfigure_beam()

    assert e_info.value.message == "Oops there was a problem"

    mock_servicer_context.deconfigure_beam.assert_called_once_with(DeconfigureBeamRequest())
    mock_servicer_context.go_to_fault.assert_called_once_with(GoToFaultRequest())


def test_recv_grpc_validate_configure_scan(
    grpc_api: PstReceiveProcessApiGrpc,
    mock_servicer_context: MagicMock,
    pst_configure_scan_request: dict,
) -> None:
    """Test that RECV gRPC validate_configure_scan is called."""
    response = ConfigureScanResponse()
    mock_servicer_context.configure_scan = MagicMock(return_value=response)

    grpc_api.validate_configure_scan(pst_configure_scan_request)

    recv_scan_request = generate_recv_configure_scan_request(**pst_configure_scan_request)
    assert "execution_block_id" in recv_scan_request, "Expected key 'execution_block_id' in scan request"
    assert recv_scan_request["execution_block_id"] == pst_configure_scan_request["eb_id"], (
        f"Expected execution_block_id to be {pst_configure_scan_request['eb_id']} "
        f"but value is {recv_scan_request['execution_block_id']}"
    )

    expected_request = ConfigureScanRequest(
        scan_configuration=ScanConfiguration(receive=ReceiveScanConfiguration(**recv_scan_request)),
        dry_run=True,
    )
    mock_servicer_context.configure_scan.assert_called_once_with(expected_request)


def test_recv_grpc_validate_configure_scan_throws_invalid_request(
    grpc_api: PstReceiveProcessApiGrpc,
    mock_servicer_context: MagicMock,
    pst_configure_scan_request: dict,
    expected_receive_configure_protobuf: ReceiveScanConfiguration,
) -> None:
    """Test that validate_configure_scan throws exception when there are validation errors."""
    mock_servicer_context.configure_scan.side_effect = TestMockException(
        grpc_status_code=grpc.StatusCode.FAILED_PRECONDITION,
        error_code=ErrorCode.INVALID_REQUEST,
        message="Validate configure scan error.",
    )

    with pytest.raises(ValidationError) as e_info:
        grpc_api.validate_configure_scan(configuration=pst_configure_scan_request)

    assert e_info.value.message == "Validate configure scan error."

    expected_request = ConfigureScanRequest(
        scan_configuration=ScanConfiguration(receive=expected_receive_configure_protobuf),
        dry_run=True,
    )
    mock_servicer_context.configure_scan.assert_called_once_with(expected_request)


def test_recv_grpc_validate_configure_scan_throws_scan_already_configured(
    grpc_api: PstReceiveProcessApiGrpc,
    mock_servicer_context: MagicMock,
    pst_configure_scan_request: dict,
    expected_receive_configure_protobuf: ReceiveScanConfiguration,
) -> None:
    """Test that validate_configure_scan throws exception when already configured for scanning."""
    mock_servicer_context.configure_scan.side_effect = TestMockException(
        grpc_status_code=grpc.StatusCode.FAILED_PRECONDITION,
        error_code=ErrorCode.CONFIGURED_FOR_SCAN_ALREADY,
        message="Already configured for scanning.",
    )

    with pytest.raises(ValidationError) as e_info:
        grpc_api.validate_configure_scan(configuration=pst_configure_scan_request)

    assert e_info.value.message == "Already configured for scanning."

    expected_request = ConfigureScanRequest(
        scan_configuration=ScanConfiguration(receive=expected_receive_configure_protobuf),
        dry_run=True,
    )
    mock_servicer_context.configure_scan.assert_called_once_with(expected_request)


def test_recv_grpc_configure_scan(
    grpc_api: PstReceiveProcessApiGrpc,
    mock_servicer_context: MagicMock,
    pst_configure_scan_request: dict,
    expected_receive_configure_protobuf: ReceiveScanConfiguration,
) -> None:
    """Test that RECV gRPC calls configure_scan on remote service."""
    response = ConfigureScanResponse()
    mock_servicer_context.configure_scan = MagicMock(return_value=response)

    grpc_api.configure_scan(
        pst_configure_scan_request,
    )

    expected_request = ConfigureScanRequest(
        scan_configuration=ScanConfiguration(receive=expected_receive_configure_protobuf)
    )
    mock_servicer_context.configure_scan.assert_called_once_with(expected_request)


def test_recv_grpc_configure_when_already_configured(
    grpc_api: PstReceiveProcessApiGrpc,
    mock_servicer_context: MagicMock,
    pst_configure_scan_request: dict,
    expected_receive_configure_protobuf: ReceiveScanConfiguration,
) -> None:
    """Test that RECV gRPC configure scan and already configured."""
    mock_servicer_context.configure_scan.side_effect = TestMockException(
        grpc_status_code=grpc.StatusCode.FAILED_PRECONDITION,
        error_code=ErrorCode.CONFIGURED_FOR_SCAN_ALREADY,
        message="Scan has already been configured.",
    )

    with pytest.raises(ScanConfiguredAlreadyException) as e_info:
        grpc_api.configure_scan(pst_configure_scan_request)

    assert e_info.value.message == "Scan has already been configured."

    expected_request = ConfigureScanRequest(
        scan_configuration=ScanConfiguration(receive=expected_receive_configure_protobuf)
    )
    mock_servicer_context.configure_scan.assert_called_once_with(expected_request)


def test_recv_grpc_configure_when_throws_exception(
    grpc_api: PstReceiveProcessApiGrpc,
    mock_servicer_context: MagicMock,
    pst_configure_scan_request: dict,
    expected_receive_configure_protobuf: ReceiveScanConfiguration,
) -> None:
    """Test that RECV gRPC configure beam throws an exception."""
    mock_servicer_context.go_to_fault = MagicMock(return_value=GoToFaultResponse())
    mock_servicer_context.configure_scan.side_effect = TestMockException(
        grpc_status_code=grpc.StatusCode.FAILED_PRECONDITION,
        error_code=ErrorCode.INTERNAL_ERROR,
        message="Internal server error occurred",
    )

    with pytest.raises(BaseGrpcException) as e_info:
        grpc_api.configure_scan(pst_configure_scan_request)

    assert e_info.value.message == "Internal server error occurred"

    expected_request = ConfigureScanRequest(
        scan_configuration=ScanConfiguration(receive=expected_receive_configure_protobuf)
    )
    mock_servicer_context.configure_scan.assert_called_once_with(expected_request)
    mock_servicer_context.go_to_fault.assert_called_once_with(GoToFaultRequest())


def test_recv_grpc_deconfigure_scan(
    grpc_api: PstReceiveProcessApiGrpc,
    mock_servicer_context: MagicMock,
) -> None:
    """Test that RECV gRPC calls deconfigure_scan on remote service."""
    response = DeconfigureScanResponse()
    mock_servicer_context.deconfigure_scan = MagicMock(return_value=response)

    grpc_api.deconfigure_scan()

    mock_servicer_context.deconfigure_scan.assert_called_once_with(DeconfigureScanRequest())


def test_recv_grpc_deconfigure_when_not_configured_for_scan(
    grpc_api: PstReceiveProcessApiGrpc,
    mock_servicer_context: MagicMock,
) -> None:
    """Test that RECV gRPC deconfigure scan and currently not configured."""
    mock_servicer_context.deconfigure_scan.side_effect = TestMockException(
        grpc_status_code=grpc.StatusCode.FAILED_PRECONDITION,
        error_code=ErrorCode.NOT_CONFIGURED_FOR_SCAN,
        message="Not configured for scan.",
    )
    grpc_api.deconfigure_scan()

    mock_servicer_context.deconfigure_scan.assert_called_once_with(DeconfigureScanRequest())


def test_recv_grpc_deconfigure_when_throws_exception(
    grpc_api: PstReceiveProcessApiGrpc,
    mock_servicer_context: MagicMock,
) -> None:
    """Test that RECV gRPC deconfigure scan throws an exception."""
    mock_servicer_context.go_to_fault = MagicMock(return_value=GoToFaultResponse())
    mock_servicer_context.deconfigure_scan.side_effect = TestMockException(
        grpc_status_code=grpc.StatusCode.FAILED_PRECONDITION,
        error_code=ErrorCode.INTERNAL_ERROR,
        message="Internal server error occurred",
    )

    with pytest.raises(BaseGrpcException) as e_info:
        grpc_api.deconfigure_scan()

    assert e_info.value.message == "Internal server error occurred"

    mock_servicer_context.deconfigure_scan.assert_called_once_with(DeconfigureScanRequest())
    mock_servicer_context.go_to_fault.assert_called_once_with(GoToFaultRequest())


def test_recv_grpc_scan(
    grpc_api: PstReceiveProcessApiGrpc,
    mock_servicer_context: MagicMock,
    scan_request: dict,
    expected_scan_request_protobuf: StartScanRequest,
) -> None:
    """Test that RECV gRPC scan."""
    response = StartScanResponse()
    mock_servicer_context.start_scan = MagicMock(return_value=response)

    grpc_api.start_scan(**scan_request)

    mock_servicer_context.start_scan.assert_called_once_with(expected_scan_request_protobuf)


def test_recv_grpc_scan_when_already_scanning(
    grpc_api: PstReceiveProcessApiGrpc,
    mock_servicer_context: MagicMock,
    scan_request: dict,
    expected_scan_request_protobuf: StartScanRequest,
) -> None:
    """Test that RECV gRPC scan when already scanning."""
    mock_servicer_context.start_scan.side_effect = TestMockException(
        grpc_status_code=grpc.StatusCode.FAILED_PRECONDITION,
        error_code=ErrorCode.ALREADY_SCANNING,
        message="We are already scanning",
    )

    grpc_api.start_scan(**scan_request)

    mock_servicer_context.start_scan.assert_called_once_with(expected_scan_request_protobuf)


def test_recv_grpc_scan_when_throws_exception(
    grpc_api: PstReceiveProcessApiGrpc,
    mock_servicer_context: MagicMock,
    scan_request: dict,
    expected_scan_request_protobuf: StartScanRequest,
) -> None:
    """Test that RECV gRPC scan when an exception is thrown."""
    mock_servicer_context.go_to_fault = MagicMock(return_value=GoToFaultResponse())
    mock_servicer_context.start_scan.side_effect = TestMockException(
        grpc_status_code=grpc.StatusCode.INTERNAL,
        message="Oops there was a problem",
    )

    with pytest.raises(BaseGrpcException) as e_info:
        grpc_api.start_scan(**scan_request)

    assert e_info.value.message == "Oops there was a problem"

    mock_servicer_context.start_scan.assert_called_once_with(expected_scan_request_protobuf)
    mock_servicer_context.go_to_fault.assert_called_once_with(GoToFaultRequest())


def test_recv_grpc_stop_scan(
    grpc_api: PstReceiveProcessApiGrpc,
    mock_servicer_context: MagicMock,
) -> None:
    """Test that RECV gRPC end scan."""
    response = StopScanResponse()
    mock_servicer_context.stop_scan = MagicMock(return_value=response)

    with unittest.mock.patch.object(
        grpc_api, "stop_monitoring", wraps=grpc_api.stop_monitoring
    ) as stop_monitoring:
        grpc_api.stop_scan()
        stop_monitoring.assert_called_once()

    mock_servicer_context.stop_scan.assert_called_once_with(StopScanRequest())


def test_recv_grpc_stop_scan_when_not_scanning(
    grpc_api: PstReceiveProcessApiGrpc,
    mock_servicer_context: MagicMock,
) -> None:
    """Test that RECV gRPC end scan when not scanning."""
    mock_servicer_context.stop_scan.side_effect = TestMockException(
        grpc_status_code=grpc.StatusCode.FAILED_PRECONDITION,
        error_code=ErrorCode.NOT_SCANNING,
        message="We're not scanning. End Scan doesn't need to do anything",
    )

    with unittest.mock.patch.object(
        grpc_api, "stop_monitoring", wraps=grpc_api.stop_monitoring
    ) as stop_monitoring:
        grpc_api.stop_scan()
        stop_monitoring.assert_called_once()

    mock_servicer_context.stop_scan.assert_called_once_with(StopScanRequest())


def test_recv_grpc_stop_scan_when_exception_thrown(
    grpc_api: PstReceiveProcessApiGrpc,
    mock_servicer_context: MagicMock,
) -> None:
    """Test that RECV gRPC end scan when an exception is thrown."""
    mock_servicer_context.go_to_fault = MagicMock(return_value=GoToFaultResponse())
    mock_servicer_context.stop_scan.side_effect = TestMockException(
        grpc_status_code=grpc.StatusCode.INTERNAL,
        message="Something is wrong!",
    )

    with pytest.raises(BaseGrpcException) as e_info:
        with unittest.mock.patch.object(
            grpc_api, "stop_monitoring", wraps=grpc_api.stop_monitoring
        ) as stop_monitoring:
            grpc_api.stop_scan()
            stop_monitoring.assert_called_once()

    assert e_info.value.message == "Something is wrong!"

    mock_servicer_context.stop_scan.assert_called_once_with(StopScanRequest())
    mock_servicer_context.go_to_fault.assert_called_once_with(GoToFaultRequest())


def test_recv_grpc_handle_monitor_response(
    grpc_api: PstReceiveProcessApiGrpc,
    subband_monitor_data_callback: MagicMock,
) -> None:
    """Test the handling of monitor data."""
    receive_rate = float(
        np.float32(random.random() / GIGABITS_PER_BYTE)
    )  # hack so we use f32 precision use in protobuf
    receive_rate_gbs = receive_rate * GIGABITS_PER_BYTE  # python's default is f64
    data_received = random.randint(1, 100)
    data_drop_rate = random.random()
    data_dropped = random.randint(1, 100)
    misordered_packets = random.randint(1, 100)
    misordered_packet_rate = random.random()
    malformed_packets = random.randint(1, 100)
    malformed_packet_rate = random.random()
    misdirected_packets = random.randint(1, 100)
    misdirected_packet_rate = random.random()
    checksum_failure_packets = random.randint(1, 100)
    checksum_failure_packet_rate = random.random()
    timestamp_sync_error_packets = random.randint(1, 100)
    timestamp_sync_error_packet_rate = random.random()
    seq_number_sync_error_packets = random.randint(1, 100)
    seq_number_sync_error_packet_rate = random.random()
    no_valid_polarisation_correction_packets = random.randint(1, 100)
    no_valid_polarisation_correction_packet_rate = random.random()
    no_valid_station_beam_packets = random.randint(1, 100)
    no_valid_station_beam_packet_rate = random.random()
    no_valid_pst_beam_packets = random.randint(1, 100)
    no_valid_pst_beam_packet_rate = random.random()

    receive_monitor_data = ReceiveMonitorData(
        receive_rate=receive_rate,
        data_received=data_received,
        data_drop_rate=data_drop_rate,
        data_dropped=data_dropped,
        misordered_packets=misordered_packets,
        misordered_packet_rate=misordered_packet_rate,
        malformed_packets=malformed_packets,
        malformed_packet_rate=malformed_packet_rate,
        misdirected_packets=misdirected_packets,
        misdirected_packet_rate=misdirected_packet_rate,
        checksum_failure_packets=checksum_failure_packets,
        checksum_failure_packet_rate=checksum_failure_packet_rate,
        timestamp_sync_error_packets=timestamp_sync_error_packets,
        timestamp_sync_error_packet_rate=timestamp_sync_error_packet_rate,
        seq_number_sync_error_packets=seq_number_sync_error_packets,
        seq_number_sync_error_packet_rate=seq_number_sync_error_packet_rate,
        no_valid_polarisation_correction_packets=no_valid_polarisation_correction_packets,
        no_valid_polarisation_correction_packet_rate=no_valid_polarisation_correction_packet_rate,
        no_valid_station_beam_packets=no_valid_station_beam_packets,
        no_valid_station_beam_packet_rate=no_valid_station_beam_packet_rate,
        no_valid_pst_beam_packets=no_valid_pst_beam_packets,
        no_valid_pst_beam_packet_rate=no_valid_pst_beam_packet_rate,
    )

    response_message = MonitorResponse(monitor_data=MonitorData(receive=receive_monitor_data))

    grpc_api._handle_monitor_response(
        response_message.monitor_data, monitor_data_callback=subband_monitor_data_callback
    )

    subband_monitor_data_callback.assert_called_once_with(
        subband_id=1,
        subband_data=ReceiveData(
            # grab from the protobuf message given there can be rounding issues.
            data_received=receive_monitor_data.data_received,
            data_receive_rate=receive_rate_gbs,
            data_dropped=receive_monitor_data.data_dropped,
            data_drop_rate=receive_monitor_data.data_drop_rate,
            misordered_packets=receive_monitor_data.misordered_packets,
            misordered_packet_rate=receive_monitor_data.misordered_packet_rate,
            malformed_packets=receive_monitor_data.malformed_packets,
            malformed_packet_rate=receive_monitor_data.malformed_packet_rate,
            misdirected_packets=receive_monitor_data.misdirected_packets,
            misdirected_packet_rate=receive_monitor_data.misdirected_packet_rate,
            checksum_failure_packets=receive_monitor_data.checksum_failure_packets,
            checksum_failure_packet_rate=receive_monitor_data.checksum_failure_packet_rate,
            timestamp_sync_error_packets=receive_monitor_data.timestamp_sync_error_packets,
            timestamp_sync_error_packet_rate=receive_monitor_data.timestamp_sync_error_packet_rate,
            seq_number_sync_error_packets=receive_monitor_data.seq_number_sync_error_packets,
            seq_number_sync_error_packet_rate=receive_monitor_data.seq_number_sync_error_packet_rate,
            no_valid_polarisation_correction_packets=(
                receive_monitor_data.no_valid_polarisation_correction_packets
            ),
            no_valid_polarisation_correction_packet_rate=(
                receive_monitor_data.no_valid_polarisation_correction_packet_rate
            ),
            no_valid_station_beam_packets=receive_monitor_data.no_valid_station_beam_packets,
            no_valid_station_beam_packet_rate=receive_monitor_data.no_valid_station_beam_packet_rate,
            no_valid_pst_beam_packets=receive_monitor_data.no_valid_pst_beam_packets,
            no_valid_pst_beam_packet_rate=receive_monitor_data.no_valid_pst_beam_packet_rate,
        ),
    )


def test_recv_grpc_simulated_monitor_calls_callback(
    grpc_api: PstReceiveProcessApiGrpc,
    mock_servicer_context: MagicMock,
    subband_monitor_data_callback: MagicMock,
    abort_event: threading.Event,
    logger: logging.Logger,
) -> None:
    """Test simulated monitoring calls subband_monitor_data_callback."""
    receive_rate = float(
        np.float32(random.random() / GIGABITS_PER_BYTE)
    )  # hack so we use f32 precision use in protobuf
    receive_rate_gbs = receive_rate * GIGABITS_PER_BYTE  # python's default is f64
    data_received = random.randint(1, 100)
    data_drop_rate = random.random()
    data_dropped = random.randint(1, 100)
    misordered_packets = random.randint(1, 100)

    monitor_data = ReceiveMonitorData(
        receive_rate=receive_rate,
        data_received=data_received,
        data_drop_rate=data_drop_rate,
        data_dropped=data_dropped,
        misordered_packets=misordered_packets,
    )

    def response_generator() -> Generator[MonitorResponse, None, None]:
        while True:
            yield MonitorResponse(monitor_data=MonitorData(receive=monitor_data))

    mock_servicer_context.monitor = MagicMock()
    mock_servicer_context.monitor.return_value = response_generator()

    grpc_api.monitor(
        subband_monitor_data_callback=subband_monitor_data_callback,
        polling_rate=50,
        monitor_abort_event=abort_event,
    )

    time.sleep(0.15)
    grpc_api.stop_monitoring()

    calls = [
        call(
            subband_id=1,
            subband_data=ReceiveData(
                data_received=monitor_data.data_received,
                data_receive_rate=receive_rate_gbs,  # we get B/s not Gb/s,
                data_dropped=monitor_data.data_dropped,
                data_drop_rate=monitor_data.data_drop_rate,
                misordered_packets=monitor_data.misordered_packets,
            ),
        )
    ]
    subband_monitor_data_callback.assert_has_calls(calls=calls)


def test_recv_grpc_abort(
    grpc_api: PstReceiveProcessApiGrpc,
    mock_servicer_context: MagicMock,
) -> None:
    """Test that RECV gRPC abort."""
    response = AbortResponse()
    mock_servicer_context.abort = MagicMock(return_value=response)

    with unittest.mock.patch.object(
        grpc_api, "stop_monitoring", wraps=grpc_api.stop_monitoring
    ) as stop_monitoring:
        grpc_api.abort()
        stop_monitoring.assert_called_once()

    mock_servicer_context.abort.assert_called_once_with(AbortRequest())


def test_recv_grpc_abort_throws_exception(
    grpc_api: PstReceiveProcessApiGrpc,
    mock_servicer_context: MagicMock,
) -> None:
    """Test that RECV gRPC abort when an exception is thrown."""
    mock_servicer_context.go_to_fault = MagicMock(return_value=GoToFaultResponse())
    mock_servicer_context.abort.side_effect = TestMockException(
        grpc_status_code=grpc.StatusCode.INTERNAL,
        message="We have an issue!",
    )

    with pytest.raises(BaseGrpcException) as e_info:
        with unittest.mock.patch.object(
            grpc_api, "stop_monitoring", wraps=grpc_api.stop_monitoring
        ) as stop_monitoring:
            grpc_api.abort()
            stop_monitoring.assert_called_once()

    assert e_info.value.message == "We have an issue!"

    mock_servicer_context.abort.assert_called_once_with(AbortRequest())
    mock_servicer_context.go_to_fault.assert_called_once_with(GoToFaultRequest())


def test_recv_grpc_reset(
    grpc_api: PstReceiveProcessApiGrpc,
    mock_servicer_context: MagicMock,
) -> None:
    """Test that RECV gRPC reset."""
    response = ResetResponse()
    mock_servicer_context.reset = MagicMock(return_value=response)

    with unittest.mock.patch.object(
        grpc_api, "stop_monitoring", wraps=grpc_api.stop_monitoring
    ) as stop_monitoring:
        grpc_api.reset()
        stop_monitoring.assert_called_once()

    mock_servicer_context.reset.assert_called_once_with(ResetRequest())


def test_recv_grpc_reset_when_exception_thrown(
    grpc_api: PstReceiveProcessApiGrpc,
    mock_servicer_context: MagicMock,
) -> None:
    """Test that RECV gRPC reset when exception is thrown."""
    mock_servicer_context.go_to_fault = MagicMock(return_value=GoToFaultResponse())
    mock_servicer_context.reset.side_effect = TestMockException(
        grpc_status_code=grpc.StatusCode.INTERNAL,
        message="Resetting error!",
    )

    with pytest.raises(BaseGrpcException) as e_info:
        with unittest.mock.patch.object(
            grpc_api, "stop_monitoring", wraps=grpc_api.stop_monitoring
        ) as stop_monitoring:
            grpc_api.reset()
            stop_monitoring.assert_called_once()

    assert e_info.value.message == "Resetting error!"

    mock_servicer_context.reset.assert_called_once_with(ResetRequest())
    mock_servicer_context.go_to_fault.assert_called_once_with(GoToFaultRequest())


def test_recv_grpc_go_to_fault(
    grpc_api: PstReceiveProcessApiGrpc,
    mock_servicer_context: MagicMock,
) -> None:
    """Test that RECV gRPC go_to_fault."""
    mock_servicer_context.go_to_fault = MagicMock(return_value=GoToFaultResponse())

    with unittest.mock.patch.object(
        grpc_api, "stop_monitoring", wraps=grpc_api.stop_monitoring
    ) as stop_monitoring:
        grpc_api.go_to_fault()
        stop_monitoring.assert_called_once()

    mock_servicer_context.go_to_fault.assert_called_once_with(GoToFaultRequest())


def test_recv_grpc_get_env(
    grpc_api: PstReceiveProcessApiGrpc,
    mock_servicer_context: MagicMock,
) -> None:
    """Test get_env via gRPC."""
    response = GetEnvironmentResponse()
    response.values["data_host"].string_value = "10.10.0.5"
    response.values["data_port"].signed_int_value = 32080
    response.values["data_mac"].string_value = "00:01:02:03:04:05"
    mock_servicer_context.get_env = MagicMock(return_value=response)

    client_response = grpc_api.get_env()
    expected_response = {
        "data_host": "10.10.0.5",
        "data_port": 32080,
        "data_mac": "00:01:02:03:04:05",
    }

    assert expected_response == client_response


@pytest.mark.parametrize(
    "tango_log_level,grpc_log_level",
    [
        (LoggingLevel.INFO, LogLevel.INFO),
        (LoggingLevel.DEBUG, LogLevel.DEBUG),
        (LoggingLevel.FATAL, LogLevel.CRITICAL),
        (LoggingLevel.WARNING, LogLevel.WARNING),
        (LoggingLevel.OFF, LogLevel.INFO),
    ],
)
def test_recv_grpc_api_set_log_level(
    grpc_api: PstReceiveProcessApiGrpc,
    mock_servicer_context: MagicMock,
    tango_log_level: LoggingLevel,
    grpc_log_level: LogLevel,
) -> None:
    """Test the set_logging_level on gRPC API."""
    response = SetLogLevelResponse()
    mock_servicer_context.set_log_level = MagicMock(return_value=response)
    log_level_request = SetLogLevelRequest(log_level=grpc_log_level)
    grpc_api.set_log_level(log_level=tango_log_level)
    mock_servicer_context.set_log_level.assert_called_once_with(log_level_request)
    mock_servicer_context.reset_mock()


def test_recv_grpc_api_perform_health_check(
    grpc_api: PstReceiveProcessApiGrpc,
    mock_servicer_context: MagicMock,
    abort_event: threading.Event,
    service_name: str,
    service_uuid: str,
    health_check_handler: MagicMock,
) -> None:
    """Test normal response of RECV gRPC API client for health check."""
    obs_state = random.choice(
        [ObsStateGrpc.EMPTY, ObsStateGrpc.IDLE, ObsStateGrpc.READY, ObsStateGrpc.SCANNING]
    )
    expected_obs_state = ObsState(obs_state)

    health_check_response = HealthCheckResponse(
        service_name=service_name,
        uuid=service_uuid,
        obs_state=obs_state,
    )

    def response_generator() -> Generator[HealthCheckResponse, None, None]:
        while True:
            yield health_check_response

    mock_servicer_context.perform_health_check = MagicMock()
    mock_servicer_context.perform_health_check.return_value = response_generator()

    grpc_api.perform_health_check(
        health_check_handler=cast(HealthCheckHandler, health_check_handler),
        health_check_interval=50,
        health_check_abort_event=abort_event,
    )

    time.sleep(0.1)
    grpc_api.stop_health_check()

    expected_call = HealthCheckState(
        service_name=service_name,
        service_uuid=service_uuid,
        actual_service_uuid=service_uuid,
        obs_state=expected_obs_state,
    )

    cast(MagicMock, health_check_handler.handle_health_check_state).assert_called_with(expected_call)


def test_recv_grpc_api_perform_health_check_with_fault(
    grpc_api: PstReceiveProcessApiGrpc,
    mock_servicer_context: MagicMock,
    abort_event: threading.Event,
    service_name: str,
    service_uuid: str,
    health_check_handler: MagicMock,
) -> None:
    """Test normal response of RECV gRPC API client when in FAULT state."""
    health_check_response = HealthCheckResponse(
        service_name=service_name,
        uuid=service_uuid,
        obs_state=ObsStateGrpc.FAULT,
        fault_message="This is a fault message",
    )

    def response_generator() -> Generator[HealthCheckResponse, None, None]:
        while True:
            yield health_check_response

    mock_servicer_context.perform_health_check = MagicMock()
    mock_servicer_context.perform_health_check.return_value = response_generator()

    grpc_api.perform_health_check(
        health_check_handler=cast(HealthCheckHandler, health_check_handler),
        health_check_interval=50,
        health_check_abort_event=abort_event,
    )

    time.sleep(0.1)
    grpc_api.stop_health_check()

    expected_call = HealthCheckState(
        service_name=service_name,
        service_uuid=service_uuid,
        actual_service_uuid=service_uuid,
        obs_state=ObsState.FAULT,
        fault_message="This is a fault message",
    )

    cast(MagicMock, health_check_handler.handle_health_check_state).assert_called_with(expected_call)


def test_recv_grpc_api_perform_health_check_throws_exception(
    grpc_api: PstReceiveProcessApiGrpc,
    mock_servicer_context: MagicMock,
    abort_event: threading.Event,
    service_name: str,
    service_uuid: str,
    health_check_handler: MagicMock,
) -> None:
    """Test normal response of RECV gRPC API client when an exception is raised."""
    exception = TestMockException(
        grpc_status_code=grpc.StatusCode.CANCELLED,
        message="Service had cancelled response.",
    )

    mock_servicer_context.perform_health_check = MagicMock()
    mock_servicer_context.perform_health_check.side_effect = exception

    grpc_api.perform_health_check(
        health_check_handler=cast(HealthCheckHandler, health_check_handler),
        health_check_interval=10,
        health_check_abort_event=abort_event,
    )

    time.sleep(0.1)

    expected_call = HealthCheckState(
        service_name=service_name,
        service_uuid=service_uuid,
        exception=ANY,
    )

    cast(MagicMock, health_check_handler.handle_health_check_state).assert_called_with(expected_call)

    grpc_api.stop_health_check()
