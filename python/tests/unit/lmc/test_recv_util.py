# -*- coding: utf-8 -*-
#
# This file is part of the SKA PST project
#
# Distributed under the terms of the BSD 3-clause new license.
# See LICENSE for more info.
"""This module contains tests for the RECV utility methods."""

import random
from typing import List

import pytest
from ska_pst.lmc.receive.receive_util import (
    calculate_receive_subband_resources,
    generate_recv_configure_scan_request,
)
from ska_pst.lmc.smrb.smrb_util import generate_data_key, generate_weights_key

from ska_pst.common import CbfPstConfig, TelescopeConfig


@pytest.mark.parametrize(
    "telescope,frequency_band,centre_freq_mhz,bandwidth_mhz,start_chan,"
    "end_chan,start_pst_channel_centre_freq_mhz,refined_centre_freq_mhz,"
    "refined_bandwidth_mhz",
    [
        ("SKALow", "low", 177.800000, 6.250000, 34560, 36288, 174.609375, 177.732567, 6.250000),
        ("SKALow", "low", 177.732567, 6.250000, 34560, 36288, 174.609375, 177.732567, 6.250000),
        ("SKALow", "low", 199.607567, 300.000000, 0, 82944, 49.609375, 199.607567, 300.000000),
        ("SKALow", "low", 299.607567, 1.562500, 68904, 69336, 298.828125, 299.607567, 1.562500),
        ("SKALow", "low", 94.981554, 73.784722, 2328, 22728, 58.029514, 94.920067, 73.784722),
        ("SKALow", "low", 199.607567, 150.000000, 20736, 62208, 124.609375, 199.607567, 150.000000),
        ("SKAMid", "1", 341.59104, 9.945600, 6262, 6447, 336.64512, 341.59104, 9.945600),
        ("SKAMid", "1", 396.29184, 198.912000, 5522, 9222, 296.862720, 396.29184, 198.912000),
        ("SKAMid", "2", 990.87744, 198.912000, 16582, 20282, 891.448320, 990.87744, 198.912000),
        ("SKAMid", "5a", 4694.457600, 9.945600, 87230, 87415, 4689.51168, 4694.457600, 9.945600),
    ],
)
def test_calculate_receive_subband_resources(
    beam_id: int,
    pst_configure_scan_request: dict,
    recv_data_host: str,
    recv_data_mac: str,
    subband_udp_ports: List[int],
    centre_freq_mhz: float,
    bandwidth_mhz: float,
    start_chan: int,
    end_chan: int,
    telescope: str,
    telescope_config: TelescopeConfig,
    frequency_band: str,
    start_pst_channel_centre_freq_mhz: float,
    refined_centre_freq_mhz: float,
    refined_bandwidth_mhz: float,
) -> None:
    """Test that the correct RECV subband resources request is created."""
    frequency_band_config = telescope_config.frequency_bands[frequency_band]
    cbf_pst_config = frequency_band_config.cbf_pst_config
    receiver_config = frequency_band_config.receiver_config

    pst_configure_scan_request["cbf_pst_config"] = cbf_pst_config
    pst_configure_scan_request["centre_freq_mhz"] = centre_freq_mhz
    pst_configure_scan_request["bandwidth_mhz"] = bandwidth_mhz
    pst_configure_scan_request["nchan"] = end_chan - start_chan

    actual = calculate_receive_subband_resources(
        beam_id=beam_id,
        data_host=recv_data_host,
        data_mac=recv_data_mac,
        subband_udp_ports=subband_udp_ports,
        **pst_configure_scan_request,
    )

    assert "common" in actual
    assert "subbands" in actual
    assert 1 in actual["subbands"]

    actual_common = actual["common"]

    assert actual_common["nsubband"] == 1

    assert actual_common["udp_nsamp"] == cbf_pst_config.udp_nsamp
    assert actual_common["wt_nsamp"] == cbf_pst_config.wt_nsamp
    assert actual_common["udp_nchan"] == cbf_pst_config.udp_nchan
    assert actual_common["npol"] == cbf_pst_config.npol
    assert actual_common["nbits"] == cbf_pst_config.nbit
    assert actual_common["ndim"] == cbf_pst_config.ndim
    assert actual_common["ovrsamp"] == "/".join(map(str, cbf_pst_config.oversampling_ratio))

    receiver_id = pst_configure_scan_request["receiver_id"]

    assert actual_common["frequency"] == refined_centre_freq_mhz
    assert actual_common["bandwidth"] == refined_bandwidth_mhz
    assert actual_common["nchan"] == pst_configure_scan_request["nchan"]

    # Assert receiver configuration
    assert actual_common["frontend"] == receiver_id
    assert actual_common["fd_poln"] == receiver_config.feed_polarization
    assert actual_common["fd_hand"] == receiver_config.feed_handedness
    assert actual_common["fd_sang"] == receiver_config.feed_angle
    assert actual_common["fd_mode"] == receiver_config.feed_tracking_mode
    assert actual_common["fa_req"] == receiver_config.feed_position_angle

    assert actual_common["nant"] == len(pst_configure_scan_request["receptors"])
    assert actual_common["antennas"] == ",".join(pst_configure_scan_request["receptors"])
    assert actual_common["ant_weights"] == ",".join(map(str, pst_configure_scan_request["receptor_weights"]))
    assert actual_common["beam_id"] == str(pst_configure_scan_request["timing_beam_id"])

    actual_subband_1 = actual["subbands"][1]

    assert actual_subband_1["data_key"] == generate_data_key(beam_id=beam_id, subband_id=1)
    assert actual_subband_1["weights_key"] == generate_weights_key(beam_id=beam_id, subband_id=1)
    assert actual_subband_1["bandwidth"] == actual_common["bandwidth"]
    assert actual_subband_1["nchan"] == actual_common["nchan"]
    assert actual_subband_1["nchan"] == actual_subband_1["end_channel"] - actual_subband_1["start_channel"]
    assert actual_subband_1["frequency"] == actual_common["frequency"]
    assert actual_subband_1["start_channel"] == start_chan
    assert actual_subband_1["end_channel"] == end_chan
    assert actual_subband_1["bandwidth_out"] == actual_common["bandwidth"]
    assert actual_subband_1["nchan_out"] == actual_common["nchan"]
    assert (
        actual_subband_1["nchan_out"]
        == actual_subband_1["end_channel_out"] - actual_subband_1["start_channel_out"]
    )
    assert actual_subband_1["frequency_out"] == actual_common["frequency"]
    assert actual_subband_1["start_channel_out"] == start_chan
    assert actual_subband_1["end_channel_out"] == end_chan
    assert actual_subband_1["start_centre_freq_mhz"] == start_pst_channel_centre_freq_mhz


def test_calculate_receive_subband_resources_uses_timing_beam_id_from_request(
    beam_id: int,
    pst_configure_scan_request: dict,
    recv_data_host: str,
    recv_data_mac: str,
    subband_udp_ports: List[int],
) -> None:
    """Test that if timing_beam_id is in request that it is used not beam_id."""
    timing_beam_id = str(random.randint(2, 10))
    pst_configure_scan_request["timing_beam_id"] = timing_beam_id
    actual = calculate_receive_subband_resources(
        beam_id=beam_id,
        data_host=recv_data_host,
        data_mac=recv_data_mac,
        subband_udp_ports=subband_udp_ports,
        **pst_configure_scan_request,
    )

    assert actual["common"]["beam_id"] == timing_beam_id


@pytest.mark.parametrize(
    "telescope, frequency_band, expected_udp_format",
    [
        ("SKALow", "low", "LowPST"),
        ("SKAMid", "1", "MidPSTBand1"),
        ("SKAMid", "2", "MidPSTBand2"),
        ("SKAMid", "3", "MidPSTBand3"),
        ("SKAMid", "4", "MidPSTBand4"),
        ("SKAMid", "5a", "MidPSTBand5"),
        ("SKAMid", "5b", "MidPSTBand5"),
    ],
)
def test_udp_format_set_in_calculated_resources(
    telescope: str,
    frequency_band: str,
    beam_id: int,
    pst_configure_scan_request: dict,
    expected_udp_format: str,
    recv_data_host: str,
    recv_data_mac: str,
    subband_udp_ports: List[int],
    cbf_pst_config: CbfPstConfig,
) -> None:
    """Test that we add the correct udp_format field."""
    pst_configure_scan_request["cbf_pst_config"] = cbf_pst_config

    calculated_resources = calculate_receive_subband_resources(
        beam_id=beam_id,
        data_host=recv_data_host,
        data_mac=recv_data_mac,
        subband_udp_ports=subband_udp_ports,
        **pst_configure_scan_request,
    )

    assert "udp_format" in calculated_resources["common"]
    assert calculated_resources["common"]["udp_format"] == expected_udp_format


@pytest.mark.parametrize(
    "telescope, frequency_band, bandwidth_mhz, expected_tsamp",
    [
        ("SKALow", "low", 0.08680555556, 207.36),  # Low PST narrowest possible bandwidth
        ("SKALow", "low", 0.78125, 207.36),  # LowPST single coarse channel
        ("SKALow", "low", 22.22222222, 207.36),  # LowPST infinitely-repeating decimal bandwidth
        ("SKALow", "low", 300, 207.36),  # LowPST maximum possible bandwidth
        ("SKAMid", "1", 59.6736, 16.276041667),  # MidPSTBand1
        ("SKAMid", "2", 198.912, 16.276041667),  # MidPSTBand2
        ("SKAMid", "3", 348.096, 16.276041667),  # MidPSTBand3
        ("SKAMid", "4", 596.736, 16.276041667),  # MidPSTBand4
        ("SKAMid", "5a", 626.5728, 16.276041667),  # MidPSTBand5
    ],
)
def test_recv_util_calc_tsamp(
    telescope: str,
    frequency_band: str,
    beam_id: int,
    pst_configure_scan_request: dict,
    bandwidth_mhz: float,
    expected_tsamp: float,
    recv_data_host: str,
    recv_data_mac: str,
    subband_udp_ports: List[int],
) -> None:
    """
    Test calculations for tsamp.

    Test input values are based on the different telescopes bandwidths. Values for these parameterised tests
    come from the example RECV config files in the ska-pst-recv repository.
    """
    pst_configure_scan_request["bandwidth_mhz"] = bandwidth_mhz

    calculated_resources = calculate_receive_subband_resources(
        beam_id=beam_id,
        data_host=recv_data_host,
        data_mac=recv_data_mac,
        subband_udp_ports=subband_udp_ports,
        **pst_configure_scan_request,
    )

    assert "tsamp" in calculated_resources["common"]
    assert abs(calculated_resources["common"]["tsamp"] - expected_tsamp) < 1e-6


def test_map_configure_request(
    pst_configure_scan_request: dict,
) -> None:
    """Test that the correct RECV subband resources request is created."""
    actual = generate_recv_configure_scan_request(**pst_configure_scan_request)

    assert actual["observer"] == pst_configure_scan_request["observer_id"]
    assert actual["projid"] == pst_configure_scan_request["project_id"]
    if "pointing_id" in pst_configure_scan_request:
        assert actual["pnt_id"] == pst_configure_scan_request["pointing_id"]
    assert actual["subarray_id"] == str(pst_configure_scan_request["subarray_id"])
    assert actual["source"] == pst_configure_scan_request["source"]
    assert actual["itrf"] == ",".join(map(str, pst_configure_scan_request["itrf"]))
    assert actual["coord_md"] == "J2000"
    assert actual["equinox"] == str(pst_configure_scan_request["coordinates"].get("equinox", "2000.0"))
    assert actual["stt_crd1"] == pst_configure_scan_request["coordinates"]["ra"]
    assert actual["stt_crd2"] == pst_configure_scan_request["coordinates"]["dec"]
    assert actual["trk_mode"] == "TRACK"
    assert actual["scanlen_max"] == int(pst_configure_scan_request["max_scan_length"])


def test_map_configure_request_test_equinox(
    pst_configure_scan_request: dict,
) -> None:
    """Test that the correct RECV subband resources request is created."""
    pst_configure_scan_request["coordinates"]["equinox"] = 2000.0
    actual = generate_recv_configure_scan_request(**pst_configure_scan_request)

    assert actual["equinox"] == "2000.0"
