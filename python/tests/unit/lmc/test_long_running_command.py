# -*- coding: utf-8 -*-
#
# This file is part of the SKA PST project
#
# Distributed under the terms of the BSD 3-clause new license.
# See LICENSE for more info.

"""This module contains the pytest tests for the LongRunningCommand class."""

from __future__ import annotations

import random
import time
import uuid
from typing import Any
from unittest.mock import ANY, MagicMock

import pytest
from ska_control_model import ResultCode, TaskStatus
from ska_pst.lmc.util.long_running_command import LongRunningCommand, _LrcTracker
from ska_tango_base.faults import CommandError, ResultCodeError


@pytest.fixture
def mock_invoke_lrc(monkeypatch: pytest.MonkeyPatch) -> MagicMock:
    """Get a mocked invoke_lrc."""
    mock_invoke_lrc = MagicMock()
    monkeypatch.setattr("ska_tango_base.long_running_commands_api.invoke_lrc", mock_invoke_lrc)
    return mock_invoke_lrc


def test_long_running_command_happy_path(
    mock_invoke_lrc: MagicMock,
    mock_device_proxy: MagicMock,
) -> None:
    """Test the long running command object for a happy path."""
    command_id = str(uuid.uuid4())

    def _call_side_effect(lrc_callback: _LrcTracker, **kwargs: Any) -> MagicMock:
        lrc_callback(status=TaskStatus.QUEUED)
        lrc_callback(status=TaskStatus.IN_PROGRESS)
        lrc_callback(status=TaskStatus.COMPLETED, result={"foo": "bar"})
        return MagicMock(command_id=command_id)

    mock_invoke_lrc.side_effect = _call_side_effect

    lrc = LongRunningCommand(command="test_command")
    result = lrc(proxy=mock_device_proxy, command_args=None)

    mock_invoke_lrc.assert_called_once_with(
        lrc_callback=ANY, proxy=mock_device_proxy, command="test_command", command_args=None, logger=ANY
    )
    assert result.result == {"foo": "bar"}
    assert result.result_code == ResultCode.OK
    assert result.command == "test_command"
    assert result.command_id == command_id  # todo we could mock this
    assert result.task_status_events == [TaskStatus.QUEUED, TaskStatus.IN_PROGRESS, TaskStatus.COMPLETED]


def test_long_running_command_with_timeout(
    mock_invoke_lrc: MagicMock,
    mock_device_proxy: MagicMock,
) -> None:
    """Test long running command when a timeout occurs."""

    def _call_side_effect(**kwargs: Any) -> None:
        time.sleep(0.02)

    mock_invoke_lrc.side_effect = _call_side_effect

    lrc = LongRunningCommand(command="test_command")
    result = lrc(proxy=mock_device_proxy, timeout=0.01)

    mock_invoke_lrc.assert_called_once_with(
        lrc_callback=ANY, proxy=mock_device_proxy, command="test_command", command_args=None, logger=ANY
    )
    assert result.result is None
    assert result.result_code == ResultCode.FAILED
    assert result.command == "test_command"
    assert result.exception is not None
    assert isinstance(result.exception, TimeoutError)
    assert result.exception.args[0] == "test_command failed to return within 0.010 seconds."


def test_long_running_command_with_tango_error(
    mock_invoke_lrc: MagicMock,
    mock_device_proxy: MagicMock,
) -> None:
    """Test long running command when a TANGO DevError occurs."""
    command_id = str(uuid.uuid4())
    mock_tango_error = MagicMock()

    def _call_side_effect(lrc_callback: _LrcTracker, **kwargs: Any) -> MagicMock:
        lrc_callback(error=(mock_tango_error,))
        return MagicMock(command_id=command_id)

    mock_invoke_lrc.side_effect = _call_side_effect

    lrc = LongRunningCommand(command="test_command")
    result = lrc(proxy=mock_device_proxy)

    mock_invoke_lrc.assert_called_once_with(
        lrc_callback=ANY, proxy=mock_device_proxy, command="test_command", command_args=None, logger=ANY
    )
    assert result.result is None
    assert result.result_code == ResultCode.FAILED
    assert result.command == "test_command"
    assert result.exception == mock_tango_error


def test_long_running_command_when_command_is_rejected(
    mock_invoke_lrc: MagicMock,
    mock_device_proxy: MagicMock,
) -> None:
    """Test long running command when the command is rejected."""
    command_id = str(uuid.uuid4())
    rejected_error = CommandError(f"test_command command rejected: {command_id}")

    def _call_side_effect(**kwargs: Any) -> MagicMock:
        raise rejected_error

    mock_invoke_lrc.side_effect = _call_side_effect

    lrc = LongRunningCommand(command="test_command")
    result = lrc(proxy=mock_device_proxy)

    mock_invoke_lrc.assert_called_once_with(
        lrc_callback=ANY, proxy=mock_device_proxy, command="test_command", command_args=None, logger=ANY
    )
    assert result.result is None
    assert result.result_code == ResultCode.REJECTED
    assert result.command == "test_command"
    assert result.exception == rejected_error


def test_long_running_command_when_command_raises_result_code_error(
    mock_invoke_lrc: MagicMock,
    mock_device_proxy: MagicMock,
) -> None:
    """Test long running command when command raises a result code error."""
    result_code = random.choice(list(ResultCode))
    result_code_error = ResultCodeError(f"Unexpected result code for test_command command: {result_code}")

    def _call_side_effect(**kwargs: Any) -> MagicMock:
        raise result_code_error

    mock_invoke_lrc.side_effect = _call_side_effect

    lrc = LongRunningCommand(command="test_command")
    result = lrc(proxy=mock_device_proxy)

    mock_invoke_lrc.assert_called_once_with(
        lrc_callback=ANY, proxy=mock_device_proxy, command="test_command", command_args=None, logger=ANY
    )
    assert result.result is None
    assert result.result_code == result_code
    assert result.command == "test_command"
    assert result.exception == result_code_error
