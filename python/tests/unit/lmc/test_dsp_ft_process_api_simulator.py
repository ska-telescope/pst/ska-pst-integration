# -*- coding: utf-8 -*-
#
# This file is part of the SKA PST project
#
# Distributed under the terms of the BSD 3-clause new license.
# See LICENSE for more info.
"""This module contains tests for the DSP API."""

from __future__ import annotations

import logging
import threading
import time
import unittest
from typing import Dict, Generator
from unittest.mock import MagicMock, call

import pytest
from ska_control_model import LoggingLevel
from ska_pst.lmc.dsp import DspFlowThroughSubbandMonitorData
from ska_pst.lmc.dsp.dsp_ft_process_api import PstDspFlowThroughProcessApiSimulator
from ska_pst.lmc.dsp.dsp_ft_simulator import PstDspFlowThroughSimulator
from ska_pst.lmc.validation import ValidationError


@pytest.fixture
def simulation_api(
    simulator: PstDspFlowThroughSimulator,
    logger: logging.Logger,
) -> PstDspFlowThroughProcessApiSimulator:
    """Create an instance of the Simluator API."""
    return PstDspFlowThroughProcessApiSimulator(simulator=simulator, logger=logger)


@pytest.fixture
def simulator() -> PstDspFlowThroughSimulator:
    """Create instance of a simulator to be used within the API."""
    return PstDspFlowThroughSimulator()


def test_dsp_ft_simulator_api_monitor_calls_callback(
    simulation_api: PstDspFlowThroughProcessApiSimulator,
    subband_monitor_data_callback: MagicMock,
    abort_event: threading.Event,
    logger: logging.Logger,
    monkeypatch: pytest.MonkeyPatch,
) -> None:
    """Test simulated monitoring calls subband_monitor_data_callback."""
    simulation_api._scanning = True
    num_subbands = simulation_api._simulator.num_subbands

    simulator = MagicMock()
    data = {subband_id: DspFlowThroughSubbandMonitorData() for subband_id in range(1, num_subbands + 1)}

    def mock_monitor_generator() -> Generator[Dict[int, DspFlowThroughSubbandMonitorData], None, None]:
        logger.info("Generator created")

        logger.info("Generator yielding data")
        while True:
            yield data
            time.sleep(0.005)
            abort_event.set()

    simulator.get_subband_data.side_effect = mock_monitor_generator()

    monkeypatch.setattr(simulation_api, "_simulator", simulator)

    simulation_api.monitor(
        subband_monitor_data_callback=subband_monitor_data_callback,
        polling_rate=10,
        monitor_abort_event=abort_event,
    )
    abort_event.wait()

    calls = [
        call(subband_id=subband_id, subband_data=subband_data) for (subband_id, subband_data) in data.items()
    ]
    subband_monitor_data_callback.assert_has_calls(calls=calls)


def test_dsp_ft_simulator_api_validate_configure_beam(
    simulation_api: PstDspFlowThroughProcessApiSimulator,
) -> None:
    """Tests that validate configure beam when valid does not throw exception."""
    simulation_api.fail_validate_configure_beam = False
    simulation_api.validate_configure_beam(configuration={})


def test_dsp_ft_simulator_api_validate_configure_beam_throws_validation_exception(
    simulation_api: PstDspFlowThroughProcessApiSimulator,
) -> None:
    """Tests that validate configure beam on simulator API throws validation exception."""
    simulation_api.fail_validate_configure_beam = True
    with pytest.raises(ValidationError):
        simulation_api.validate_configure_beam(configuration={})


def test_dsp_ft_simulator_api_validate_configure_scan(
    simulation_api: PstDspFlowThroughProcessApiSimulator,
) -> None:
    """Tests that validate configure scan on simulator API."""
    simulation_api.fail_validate_configure_scan = False
    simulation_api.validate_configure_scan(configuration={})


def test_dsp_ft_simulator_api_validate_configure_scan_throws_validation_exception(
    simulation_api: PstDspFlowThroughProcessApiSimulator,
) -> None:
    """Tests that validate configure scan on simulator API throws validation exception."""
    simulation_api.fail_validate_configure_scan = True
    with pytest.raises(ValidationError):
        simulation_api.validate_configure_scan(configuration={})


def test_dsp_ft_simulator_api_configure_scan(
    simulation_api: PstDspFlowThroughProcessApiSimulator,
    simulator: PstDspFlowThroughSimulator,
    pst_configure_scan_request: dict,
) -> None:
    """Test that configure_scan simulator calls task."""
    with unittest.mock.patch.object(simulator, "configure_scan", wraps=simulator.configure_scan) as configure:
        simulation_api.configure_scan(
            pst_configure_scan_request,
        )
        configure.assert_called_with(configuration=pst_configure_scan_request)


def test_dsp_ft_simulator_api_deconfigure_scan(
    simulation_api: PstDspFlowThroughProcessApiSimulator,
    simulator: PstDspFlowThroughSimulator,
) -> None:
    """Test that deconfigure_scan simulator calls task."""
    with unittest.mock.patch.object(
        simulator, "deconfigure_scan", wraps=simulator.deconfigure_scan
    ) as deconfigure_scan:
        simulation_api.deconfigure_scan()
        deconfigure_scan.assert_called_once()


def test_dsp_ft_simulator_api_start_scan(
    simulation_api: PstDspFlowThroughProcessApiSimulator,
    simulator: PstDspFlowThroughSimulator,
    scan_request: dict,
) -> None:
    """Test that deconfigure_beam simulator calls task."""
    simulation_api._scanning = False
    with unittest.mock.patch.object(simulator, "start_scan", wraps=simulator.start_scan) as start_scan:
        simulation_api.start_scan(**scan_request)
        start_scan.assert_called_with(**scan_request)

    assert simulation_api._scanning, "Start scan should put API into a scanning state."


def test_dsp_ft_simulator_api_stop_scan(
    simulation_api: PstDspFlowThroughProcessApiSimulator,
    simulator: PstDspFlowThroughSimulator,
) -> None:
    """Test that stop_scan simulator calls task."""
    simulation_api._scanning = True
    with unittest.mock.patch.object(simulator, "stop_scan", wraps=simulator.stop_scan) as stop_scan:
        with unittest.mock.patch.object(
            simulation_api, "stop_monitoring", wraps=simulation_api.stop_monitoring
        ) as stop_monitoring:
            simulation_api.stop_scan()
            stop_scan.assert_called_once()
            stop_monitoring.assert_called_once()

    assert not simulation_api._scanning, "Stop scan should put API into a not scanning state."


def test_dsp_ft_simulator_api_abort(
    simulation_api: PstDspFlowThroughProcessApiSimulator,
    simulator: PstDspFlowThroughSimulator,
) -> None:
    """Test that abort simulator calls task."""
    simulation_api._scanning = True
    with unittest.mock.patch.object(simulator, "abort", wraps=simulator.abort) as abort:
        with unittest.mock.patch.object(
            simulation_api, "stop_monitoring", wraps=simulation_api.stop_monitoring
        ) as stop_monitoring:
            simulation_api.abort()
            abort.assert_called_once()
            stop_monitoring.assert_called_once()

    assert not simulation_api._scanning, "Abort should put API into a not scanning state."


def test_dsp_ft_simulator_api_reset(
    simulation_api: PstDspFlowThroughProcessApiSimulator,
    simulator: PstDspFlowThroughSimulator,
    monkeypatch: pytest.MonkeyPatch,
) -> None:
    """Test that reset simulator calls task."""
    stop_monitoring_mock = MagicMock()
    monkeypatch.setattr(simulation_api, "stop_monitoring", stop_monitoring_mock)

    simulation_api.reset()

    assert not simulation_api._scanning, "API should not be in a scanning state."
    stop_monitoring_mock.assert_called_once()


def test_dsp_ft_simulator_api_go_to_fault(
    simulation_api: PstDspFlowThroughProcessApiSimulator,
    monkeypatch: pytest.MonkeyPatch,
) -> None:
    """Test that go_to_fault for simulator."""
    stop_monitoring_mock = MagicMock()
    monkeypatch.setattr(simulation_api, "stop_monitoring", stop_monitoring_mock)

    simulation_api.go_to_fault()
    assert not simulation_api._scanning, "Expected scanning to stop"
    stop_monitoring_mock.assert_called_once()


def test_dsp_ft_simulator_api_go_to_fault_if_monitoring_event_is_not_set(
    simulation_api: PstDspFlowThroughProcessApiSimulator,
) -> None:
    """Test that go_to_fault for simulator."""
    simulation_api._monitor_abort_event = threading.Event()

    simulation_api.go_to_fault()

    assert simulation_api._monitor_abort_event.is_set(), "Expected the monitoring event to be set"


@pytest.mark.parametrize(
    "log_level",
    [
        LoggingLevel.INFO,
        LoggingLevel.DEBUG,
        LoggingLevel.FATAL,
        LoggingLevel.WARNING,
        LoggingLevel.OFF,
    ],
)
def test_dsp_ft_simulator_api_set_log_level(
    simulation_api: PstDspFlowThroughProcessApiSimulator, log_level: LoggingLevel
) -> None:
    """Test the set_log_level on simulator API."""
    simulation_api.set_log_level(log_level=log_level)
    assert simulation_api.logging_level == log_level
