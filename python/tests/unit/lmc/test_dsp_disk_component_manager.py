# -*- coding: utf-8 -*-
#
# This file is part of the SKA PST project
#
# Distributed under the terms of the BSD 3-clause new license.
# See LICENSE for more info.
"""This module contains tests for the DSP component manager class."""

import dataclasses
import logging
import queue
from typing import Any, Callable, cast
from unittest.mock import MagicMock

import pytest
from pytest_mock import MockerFixture
from ska_control_model import LoggingLevel, SimulationMode
from ska_pst.lmc.component import MonitorDataHandler, SubcomponentEventMessage
from ska_pst.lmc.dsp.dsp_disk_component_manager import PstDspDiskComponentManager
from ska_pst.lmc.dsp.dsp_disk_model import DspDiskMonitorData
from ska_pst.lmc.dsp.dsp_disk_process_api import (
    PstDspDiskProcessApi,
    PstDspDiskProcessApiGrpc,
    PstDspDiskProcessApiSimulator,
)
from ska_pst.lmc.dsp.dsp_util import calculate_dsp_subband_resources
from ska_pst.lmc.validation import ValidationError


@pytest.fixture
def mock_disk_monitor_task() -> MagicMock:
    """Get a mock to use for testing against disk monitoring."""
    return MagicMock()


@pytest.fixture
def component_manager(
    device_name: str,
    grpc_endpoint: str,
    beam_id: int,
    simulation_mode: SimulationMode,
    logger: logging.Logger,
    api: PstDspDiskProcessApi,
    monitor_data_updated_callback: MagicMock,
    mock_disk_monitor_task: MagicMock,
    monkeypatch: pytest.MonkeyPatch,
    subcomponent_event_queue: queue.Queue[SubcomponentEventMessage],
) -> PstDspDiskComponentManager:
    """Create instance of a component manager."""
    cm = PstDspDiskComponentManager(
        device_name=device_name,
        process_api_endpoint=grpc_endpoint,
        simulation_mode=simulation_mode,
        logger=logger,
        api=api,
        beam_id=beam_id,
        monitor_data_updated_callback=monitor_data_updated_callback,
        event_queue=subcomponent_event_queue,
    )

    def _task_generator(*args: Any, **kwargs: Any) -> None:
        cm._disk_monitor_task = mock_disk_monitor_task

    monkeypatch.setattr(PstDspDiskComponentManager, "_create_disk_monitor_task", _task_generator)

    return cm


@pytest.fixture
def api(
    device_name: str,
    grpc_endpoint: str,
    simulation_mode: SimulationMode,
    logger: logging.Logger,
) -> PstDspDiskProcessApi:
    """Create an API instance."""
    if simulation_mode == SimulationMode.TRUE:
        return PstDspDiskProcessApiSimulator(
            logger=logger,
        )
    else:
        return PstDspDiskProcessApiGrpc(
            client_id=device_name,
            grpc_endpoint=grpc_endpoint,
            logger=logger,
        )


@pytest.fixture
def monitor_data(
    scan_request: dict,
) -> DspDiskMonitorData:
    """Create an an instance of DspDiskMonitorData for monitor data."""
    from ska_pst.lmc.dsp.dsp_disk_simulator import PstDspDiskSimulator

    simulator = PstDspDiskSimulator()
    simulator.start_scan(args=scan_request)

    return simulator.get_data()


@pytest.fixture
def calculated_dsp_subband_resources(beam_id: int, pst_configure_scan_request: dict) -> dict:
    """Fixture to calculate expected dsp subband resources."""
    resources = calculate_dsp_subband_resources(
        beam_id=beam_id,
        **pst_configure_scan_request,
    )
    return resources[1]


def test_dsp_disk_cm_connect_to_api_calls_connect_on_api(
    component_manager: PstDspDiskComponentManager,
    mock_disk_monitor_task: MagicMock,
) -> None:
    """Assert start/stop communicating calls API."""
    api = MagicMock()
    component_manager._api = api

    component_manager.connect()
    api.connect.assert_called_once()
    api.disconnect.assert_not_called()
    assert component_manager._disk_monitor_task is not None
    cast(MagicMock, mock_disk_monitor_task.start_monitoring).assert_called_once()

    component_manager.disconnect()
    api.disconnect.assert_called_once()
    cast(MagicMock, mock_disk_monitor_task.stop_monitoring).assert_called_once()


@pytest.mark.parametrize(
    "property",
    [
        ("disk_capacity"),
        ("available_disk_space"),
        ("disk_used_bytes"),
        ("disk_used_percentage"),
        ("data_recorded"),
        ("data_record_rate"),
        ("available_recording_time"),
        ("subband_data_recorded"),
        ("subband_data_record_rate"),
    ],
)
def test_dsp_disk_cm_properties_come_from_simulator_api_monitor_data(
    component_manager: PstDspDiskComponentManager,
    monitor_data: DspDiskMonitorData,
    property: str,
    monkeypatch: pytest.MonkeyPatch,
) -> None:
    """Test properties are coming from API monitor data."""
    monkeypatch.setattr(MonitorDataHandler, "monitor_data", monitor_data)

    actual = getattr(component_manager, property)
    expected = getattr(monitor_data, property)

    assert actual == expected


def test_dsp_disk_cm_api_instance_changes_depending_on_simulation_mode(
    component_manager: PstDspDiskComponentManager,
    monkeypatch: pytest.MonkeyPatch,
) -> None:
    """Test to assert that the process API changes depending on simulation mode."""
    grpc_connect_mock = MagicMock()
    grpc_disconnect_mock = MagicMock()
    simulator_disconnect_mock = MagicMock()
    simulator_connect_mock = MagicMock()

    monkeypatch.setattr(PstDspDiskProcessApiSimulator, "connect", simulator_connect_mock)
    monkeypatch.setattr(PstDspDiskProcessApiSimulator, "disconnect", simulator_disconnect_mock)
    monkeypatch.setattr(PstDspDiskProcessApiGrpc, "connect", grpc_connect_mock)
    monkeypatch.setattr(PstDspDiskProcessApiGrpc, "disconnect", grpc_disconnect_mock)

    assert component_manager.simulation_mode == SimulationMode.TRUE
    assert isinstance(component_manager._api, PstDspDiskProcessApiSimulator)

    component_manager.simulation_mode = SimulationMode.FALSE
    assert isinstance(component_manager._api, PstDspDiskProcessApiGrpc)

    simulator_disconnect_mock.assert_called_once()
    grpc_connect_mock.assert_called_once()

    simulator_connect_mock.assert_not_called()
    grpc_disconnect_mock.assert_not_called()

    grpc_connect_mock.reset_mock()
    grpc_disconnect_mock.reset_mock()
    simulator_disconnect_mock.reset_mock()
    simulator_connect_mock.reset_mock()

    component_manager.simulation_mode = SimulationMode.TRUE
    assert isinstance(component_manager._api, PstDspDiskProcessApiSimulator)

    simulator_connect_mock.assert_called_once()
    grpc_disconnect_mock.assert_called_once()

    simulator_disconnect_mock.assert_not_called()
    grpc_connect_mock.assert_not_called()


def test_dsp_disk_cm_validate_configure_scan(
    component_manager: PstDspDiskComponentManager,
    pst_configure_scan_request: dict,
    calculated_dsp_subband_resources: dict,
) -> None:
    """Test that validate configuration when valid."""
    api = MagicMock()
    component_manager._api = api
    component_manager._submit_background_task = lambda task, task_callback: task()  # type: ignore

    component_manager.validate_configure_scan(configuration=pst_configure_scan_request)

    api.validate_configure_beam.assert_called_once_with(configuration=calculated_dsp_subband_resources)
    api.validate_configure_scan.assert_called_once_with(configuration=pst_configure_scan_request)


def test_dsp_disk_cm_validate_configure_scan_fails_beam_configuration(
    component_manager: PstDspDiskComponentManager,
    pst_configure_scan_request: dict,
    calculated_dsp_subband_resources: dict,
) -> None:
    """Test that validate configuration when invalid beam configuration."""
    api = MagicMock()
    validation_exception = ValidationError("This is not the configuration you're looking for.")
    api.validate_configure_beam.side_effect = validation_exception

    component_manager._api = api
    component_manager._submit_background_task = lambda task, task_callback: task()  # type: ignore

    with pytest.raises(ValidationError) as exc_info:
        component_manager.validate_configure_scan(configuration=pst_configure_scan_request)

    assert exc_info.value == validation_exception

    api.validate_configure_beam.assert_called_once_with(configuration=calculated_dsp_subband_resources)
    api.validate_configure_scan.assert_not_called()


def test_dsp_disk_cm_validate_configure_scan_fails_scan_configuration(
    component_manager: PstDspDiskComponentManager,
    pst_configure_scan_request: dict,
    task_callback: Callable,
    calculated_dsp_subband_resources: dict,
) -> None:
    """Test that validate configuration when invalid scan configuration."""
    api = MagicMock()
    validation_exception = ValidationError("That's no scan configuration")
    api.validate_configure_scan.side_effect = validation_exception

    component_manager._api = api
    component_manager._submit_background_task = lambda task, task_callback: task()  # type: ignore

    with pytest.raises(ValidationError) as exc_info:
        component_manager.validate_configure_scan(configuration=pst_configure_scan_request)

    assert exc_info.value == validation_exception

    api.validate_configure_beam.assert_called_once_with(configuration=calculated_dsp_subband_resources)
    api.validate_configure_scan.assert_called_once_with(configuration=pst_configure_scan_request)


def test_dsp_disk_cm_configure_beam(
    component_manager: PstDspDiskComponentManager,
    pst_configure_scan_request: dict,
    calculated_dsp_subband_resources: dict,
    api: PstDspDiskProcessApi,
    monkeypatch: pytest.MonkeyPatch,
) -> None:
    """Test that configure beam calls the API correctly."""
    configure_beam = MagicMock()
    monkeypatch.setattr(api, "configure_beam", configure_beam)

    component_manager.configure_beam(configuration=pst_configure_scan_request)

    configure_beam.assert_called_once_with(configuration=calculated_dsp_subband_resources)


def test_dsp_disk_cm_deconfigure_beam(
    component_manager: PstDspDiskComponentManager,
) -> None:
    """Test that deconfigure beam calls the API correctly."""
    api = MagicMock()
    component_manager._api = api

    component_manager.deconfigure_beam()

    api.deconfigure_beam.assert_called_once()


def test_dsp_disk_cm_configure_scan(
    component_manager: PstDspDiskComponentManager,
    pst_configure_scan_request: dict,
) -> None:
    """Test that the component manager calls the API for configure scan."""
    api = MagicMock()
    component_manager._api = api

    component_manager.configure_scan(configuration=pst_configure_scan_request)

    api.configure_scan.assert_called_once_with(
        configuration=pst_configure_scan_request,
    )


def test_dsp_disk_cm_deconfigure_scan(
    component_manager: PstDspDiskComponentManager,
    task_callback: Callable,
) -> None:
    """Test that the component manager calls the API for deconfigure scan."""
    api = MagicMock()
    component_manager._api = api

    component_manager.deconfigure_scan()

    api.deconfigure_scan.assert_called_once()


def test_dsp_disk_cm_scan(
    component_manager: PstDspDiskComponentManager,
    scan_request: dict,
) -> None:
    """Test that the component manager calls the API to start scan."""
    api = MagicMock()
    component_manager._api = api

    component_manager.scan(**scan_request)

    api.start_scan.assert_called_once_with(
        **scan_request,
    )
    api.monitor.assert_called_once_with(
        subband_monitor_data_callback=component_manager._monitor_data_handler.handle_subband_data,
        polling_rate=component_manager.monitoring_polling_rate_ms,
    )


def test_dsp_disk_cm_end_scan(
    component_manager: PstDspDiskComponentManager,
    monitor_data_updated_callback: MagicMock,
    mocker: MockerFixture,
) -> None:
    """Test that the component manager calls the API to end scan."""
    api = MagicMock()
    component_manager._api = api
    data_handler_spy = mocker.spy(component_manager._monitor_data_handler, "reset_monitor_data")

    component_manager.end_scan()

    api.stop_scan.assert_called_once()
    api.stop_monitoring.assert_called_once()
    data_handler_spy.assert_called_once()
    assert component_manager.monitor_data == DspDiskMonitorData()
    monitor_data_updated_callback.assert_called_once_with(dataclasses.asdict(DspDiskMonitorData()))


def test_dsp_disk_cm_abort(
    component_manager: PstDspDiskComponentManager,
    monitor_data_updated_callback: MagicMock,
    mocker: MockerFixture,
) -> None:
    """Test that the component manager calls the API to abort on service."""
    api = MagicMock()
    component_manager._api = api
    data_handler_spy = mocker.spy(component_manager._monitor_data_handler, "reset_monitor_data")

    component_manager.abort()

    api.abort.assert_called_once()
    api.stop_monitoring.assert_called_once()
    data_handler_spy.assert_called_once()
    assert component_manager.monitor_data == DspDiskMonitorData()
    monitor_data_updated_callback.assert_called_once_with(dataclasses.asdict(DspDiskMonitorData()))


def test_dsp_disk_cm_obsreset(
    component_manager: PstDspDiskComponentManager,
) -> None:
    """Test that the component manager calls the API to reset service in ABORTED or FAULT state."""
    api = MagicMock()
    component_manager._api = api

    component_manager.obsreset()

    api.reset.assert_called_once()


def test_dsp_disk_cm_reset(
    component_manager: PstDspDiskComponentManager,
) -> None:
    """Test that the component manager calls the API to restart service."""
    api = MagicMock()
    component_manager._api = api

    component_manager.reset()

    api.restart.assert_called_once()


def test_dsp_disk_cm_go_to_fault(
    component_manager: PstDspDiskComponentManager,
    monitor_data_callback: MagicMock,
    mocker: MockerFixture,
) -> None:
    """Test that the component manager calls the API on go to fault."""
    api = MagicMock()
    component_manager._api = api
    data_handler_spy = mocker.spy(component_manager._monitor_data_handler, "reset_monitor_data")

    component_manager.go_to_fault(fault_msg="putting DSP into fault")

    api.go_to_fault.assert_called_once()
    api.stop_monitoring.assert_called_once()
    data_handler_spy.assert_called_once()
    assert component_manager.monitor_data == DspDiskMonitorData()
    monitor_data_callback.assert_called_once_with(dataclasses.asdict(DspDiskMonitorData()))


@pytest.mark.parametrize(
    "log_level",
    [
        LoggingLevel.INFO,
        LoggingLevel.DEBUG,
        LoggingLevel.FATAL,
        LoggingLevel.WARNING,
        LoggingLevel.OFF,
    ],
)
def test_dsp_disk_cm_set_logging_level(
    component_manager: PstDspDiskComponentManager,
    log_level: LoggingLevel,
) -> None:
    """Test DSP component manager when set_log_level is updated."""
    api = MagicMock()
    component_manager._api = api

    component_manager.set_logging_level(log_level=log_level)
    api.set_log_level.assert_called_once_with(log_level=log_level)
