# -*- coding: utf-8 -*-
#
# This file is part of the SKA PST project
#
# Distributed under the terms of the BSD 3-clause new license.
# See LICENSE for more info.
"""This module contains tests for the SMRB component manager class."""

import dataclasses
import logging
import queue
from unittest.mock import MagicMock

import pytest
from pytest_mock import MockerFixture
from ska_control_model import LoggingLevel, SimulationMode
from ska_pst.lmc.component import MonitorDataHandler, SubcomponentEventMessage
from ska_pst.lmc.smrb.smrb_component_manager import PstSmrbComponentManager
from ska_pst.lmc.smrb.smrb_model import SmrbMonitorData
from ska_pst.lmc.smrb.smrb_process_api import (
    PstSmrbProcessApi,
    PstSmrbProcessApiGrpc,
    PstSmrbProcessApiSimulator,
)
from ska_pst.lmc.smrb.smrb_util import calculate_smrb_subband_resources
from ska_pst.lmc.validation import ValidationError


@pytest.fixture
def component_manager(
    device_name: str,
    grpc_endpoint: str,
    beam_id: int,
    simulation_mode: SimulationMode,
    logger: logging.Logger,
    api: PstSmrbProcessApi,
    monitor_data_updated_callback: MagicMock,
    subcomponent_event_queue: queue.Queue[SubcomponentEventMessage],
) -> PstSmrbComponentManager:
    """Create instance of a component manager."""
    return PstSmrbComponentManager(
        device_name=device_name,
        process_api_endpoint=grpc_endpoint,
        simulation_mode=simulation_mode,
        logger=logger,
        api=api,
        beam_id=beam_id,
        monitor_data_updated_callback=monitor_data_updated_callback,
        event_queue=subcomponent_event_queue,
    )


@pytest.fixture
def api(
    device_name: str,
    grpc_endpoint: str,
    simulation_mode: SimulationMode,
    logger: logging.Logger,
) -> PstSmrbProcessApi:
    """Create an API instance."""
    if simulation_mode == SimulationMode.TRUE:
        return PstSmrbProcessApiSimulator(
            logger=logger,
        )
    else:
        return PstSmrbProcessApiGrpc(
            client_id=device_name,
            grpc_endpoint=grpc_endpoint,
            logger=logger,
        )


@pytest.fixture
def monitor_data(
    scan_request: dict,
) -> SmrbMonitorData:
    """Create an an instance of ReceiveData for monitor data."""
    from ska_pst.lmc.smrb.smrb_simulator import PstSmrbSimulator

    simulator = PstSmrbSimulator()
    simulator.start_scan(args=scan_request)

    return simulator.get_data()


@pytest.fixture
def calculated_smrb_subband_resources(beam_id: int, pst_configure_scan_request: dict) -> dict:
    """Fixture to calculate expected smrb subband resources."""
    resources = calculate_smrb_subband_resources(
        beam_id=beam_id,
        **pst_configure_scan_request,
    )
    return resources[1]


def test_smrb_cm_connect_to_api_calls_connect_on_api(
    component_manager: PstSmrbComponentManager,
) -> None:
    """Assert start/stop communicating calls API."""
    api = MagicMock()
    component_manager._api = api

    component_manager.connect()
    api.connect.assert_called_once()
    api.disconnect.assert_not_called()

    component_manager.disconnect()
    api.disconnect.assert_called_once()


@pytest.mark.parametrize(
    "property",
    [
        ("ring_buffer_utilisation"),
        ("ring_buffer_size"),
        ("number_subbands"),
        ("ring_buffer_read"),
        ("ring_buffer_written"),
        ("subband_ring_buffer_utilisations"),
        ("subband_ring_buffer_sizes"),
        ("subband_ring_buffer_read"),
        ("subband_ring_buffer_written"),
    ],
)
def test_smrb_cm_properties_come_from_simulator_api_monitor_data(
    component_manager: PstSmrbComponentManager,
    monitor_data: SmrbMonitorData,
    property: str,
    monkeypatch: pytest.MonkeyPatch,
) -> None:
    """Test properties are coming from API monitor data."""
    monkeypatch.setattr(MonitorDataHandler, "monitor_data", monitor_data)

    actual = getattr(component_manager, property)
    expected = getattr(monitor_data, property)

    assert actual == expected


def test_smrb_cm_api_instance_changes_depending_on_simulation_mode(
    component_manager: PstSmrbComponentManager,
    monkeypatch: pytest.MonkeyPatch,
) -> None:
    """Test to assert that the process API changes depending on simulation mode."""
    grpc_connect_mock = MagicMock()
    grpc_disconnect_mock = MagicMock()
    simulator_disconnect_mock = MagicMock()
    simulator_connect_mock = MagicMock()

    monkeypatch.setattr(PstSmrbProcessApiSimulator, "connect", simulator_connect_mock)
    monkeypatch.setattr(PstSmrbProcessApiSimulator, "disconnect", simulator_disconnect_mock)
    monkeypatch.setattr(PstSmrbProcessApiGrpc, "connect", grpc_connect_mock)
    monkeypatch.setattr(PstSmrbProcessApiGrpc, "disconnect", grpc_disconnect_mock)

    assert component_manager.simulation_mode == SimulationMode.TRUE
    assert isinstance(component_manager._api, PstSmrbProcessApiSimulator)

    component_manager.simulation_mode = SimulationMode.FALSE
    assert isinstance(component_manager._api, PstSmrbProcessApiGrpc)

    simulator_disconnect_mock.assert_called_once()
    grpc_connect_mock.assert_called_once()

    simulator_connect_mock.assert_not_called()
    grpc_disconnect_mock.assert_not_called()

    grpc_connect_mock.reset_mock()
    grpc_disconnect_mock.reset_mock()
    simulator_disconnect_mock.reset_mock()
    simulator_connect_mock.reset_mock()

    component_manager.simulation_mode = SimulationMode.TRUE
    assert isinstance(component_manager._api, PstSmrbProcessApiSimulator)

    simulator_connect_mock.assert_called_once()
    grpc_disconnect_mock.assert_called_once()

    simulator_disconnect_mock.assert_not_called()
    grpc_connect_mock.assert_not_called()


def test_smrb_cm_validate_configure_scan(
    component_manager: PstSmrbComponentManager,
    pst_configure_scan_request: dict,
    calculated_smrb_subband_resources: dict,
) -> None:
    """Test that validate configuration when valid."""
    api = MagicMock()
    component_manager._api = api

    component_manager.validate_configure_scan(configuration=pst_configure_scan_request)

    api.validate_configure_beam.assert_called_once_with(configuration=calculated_smrb_subband_resources)
    api.validate_configure_scan.assert_called_once_with(configuration=pst_configure_scan_request)


def test_smrb_cm_validate_configure_scan_fails_beam_configuration(
    component_manager: PstSmrbComponentManager,
    pst_configure_scan_request: dict,
    calculated_smrb_subband_resources: dict,
) -> None:
    """Test that validate configuration when invalid beam configuration."""
    api = MagicMock()
    validation_exception = ValidationError("This is not the configuration you're looking for.")
    api.validate_configure_beam.side_effect = validation_exception

    component_manager._api = api

    with pytest.raises(ValidationError) as exc_info:
        component_manager.validate_configure_scan(configuration=pst_configure_scan_request)

    assert exc_info.value == validation_exception

    api.validate_configure_beam.assert_called_once_with(configuration=calculated_smrb_subband_resources)
    api.validate_configure_scan.assert_not_called()


def test_smrb_cm_validate_configure_scan_fails_scan_configuration(
    component_manager: PstSmrbComponentManager,
    pst_configure_scan_request: dict,
    calculated_smrb_subband_resources: dict,
) -> None:
    """Test that validate configuration when invalid scan configuration."""
    api = MagicMock()
    validation_exception = ValidationError("That's no scan configuration")
    api.validate_configure_scan.side_effect = validation_exception

    component_manager._api = api

    with pytest.raises(ValidationError) as exc_info:
        component_manager.validate_configure_scan(configuration=pst_configure_scan_request)

    assert exc_info.value == validation_exception

    api.validate_configure_beam.assert_called_once_with(configuration=calculated_smrb_subband_resources)
    api.validate_configure_scan.assert_called_once_with(configuration=pst_configure_scan_request)


def test_smrb_cm_configure_beam(
    component_manager: PstSmrbComponentManager,
    pst_configure_scan_request: dict,
    calculated_smrb_subband_resources: dict,
) -> None:
    """Test that configure beam calls the API correctly."""
    api = MagicMock()
    component_manager._api = api

    component_manager.configure_beam(configuration=pst_configure_scan_request)

    api.configure_beam.assert_called_once_with(
        configuration=calculated_smrb_subband_resources,
    )


def test_smrb_cm_deconfigure_beam(
    component_manager: PstSmrbComponentManager,
) -> None:
    """Test that deconfigure beam calls the API correctly."""
    api = MagicMock()
    component_manager._api = api

    component_manager.deconfigure_beam()

    api.deconfigure_beam.assert_called_once()


def test_smrb_cm_configure_scan(
    component_manager: PstSmrbComponentManager,
    pst_configure_scan_request: dict,
) -> None:
    """Test that the component manager calls the API for configure scan."""
    api = MagicMock()
    component_manager._api = api

    component_manager.configure_scan(configuration=pst_configure_scan_request)

    api.configure_scan.assert_called_once_with(
        configuration=pst_configure_scan_request,
    )


def test_smrb_cm_deconfigure_scan(
    component_manager: PstSmrbComponentManager,
) -> None:
    """Test that the component manager calls the API for deconfigure scan."""
    api = MagicMock()
    component_manager._api = api

    component_manager.deconfigure_scan()

    api.deconfigure_scan.assert_called_once()


def test_smrb_cm_scan(
    component_manager: PstSmrbComponentManager,
    scan_request: dict,
    monkeypatch: pytest.MonkeyPatch,
) -> None:
    """Test that the component manager calls the API to start scan."""
    api = MagicMock()
    component_manager._api = api

    component_manager.scan(**scan_request)

    api.start_scan.assert_called_once_with(
        **scan_request,
    )
    api.monitor.assert_called_once_with(
        subband_monitor_data_callback=component_manager._monitor_data_handler.handle_subband_data,
        polling_rate=component_manager.monitoring_polling_rate_ms,
    )


def test_smrb_cm_end_scan(
    component_manager: PstSmrbComponentManager,
    monitor_data_callback: MagicMock,
    mocker: MockerFixture,
) -> None:
    """Test that the component manager calls the API to end scan."""
    api = MagicMock()
    component_manager._api = api
    data_handler_spy = mocker.spy(component_manager._monitor_data_handler, "reset_monitor_data")

    component_manager.end_scan()

    api.stop_scan.assert_called_once()
    api.stop_monitoring.assert_called_once()
    data_handler_spy.assert_called_once()
    assert component_manager.monitor_data == SmrbMonitorData()
    monitor_data_callback.assert_called_once_with(dataclasses.asdict(SmrbMonitorData()))


def test_smrb_cm_abort(
    component_manager: PstSmrbComponentManager,
    monitor_data_callback: MagicMock,
    mocker: MockerFixture,
) -> None:
    """Test that the component manager calls the API to abort on service."""
    api = MagicMock()
    component_manager._api = api
    data_handler_spy = mocker.spy(component_manager._monitor_data_handler, "reset_monitor_data")

    component_manager.abort()

    api.abort.assert_called_once()
    api.stop_monitoring.assert_called_once()
    data_handler_spy.assert_called_once()
    assert component_manager.monitor_data == SmrbMonitorData()
    monitor_data_callback.assert_called_once_with(dataclasses.asdict(SmrbMonitorData()))


def test_smrb_cm_obsreset(
    component_manager: PstSmrbComponentManager,
) -> None:
    """Test that the component manager calls the API reset service in ABORTED or FAULT state."""
    api = MagicMock()
    component_manager._api = api

    component_manager.obsreset()

    api.reset.assert_called_once()


def test_smrb_cm_reset(
    component_manager: PstSmrbComponentManager,
) -> None:
    """Test that the component manager calls the API to restart service."""
    api = MagicMock()
    component_manager._api = api

    component_manager.reset()

    api.restart.assert_called_once()


def test_smrb_cm_go_to_fault(
    component_manager: PstSmrbComponentManager,
    monitor_data_callback: MagicMock,
    mocker: MockerFixture,
) -> None:
    """Test that the component manager calls the API on go to fault."""
    api = MagicMock()
    component_manager._api = api
    data_handler_spy = mocker.spy(component_manager._monitor_data_handler, "reset_monitor_data")

    component_manager.go_to_fault(fault_msg="sending SMRB to fault")

    api.go_to_fault.assert_called_once()

    api.stop_monitoring.assert_called_once()
    data_handler_spy.assert_called_once()
    assert component_manager.monitor_data == SmrbMonitorData()
    monitor_data_callback.assert_called_once_with(dataclasses.asdict(SmrbMonitorData()))


@pytest.mark.parametrize(
    "log_level",
    [
        LoggingLevel.INFO,
        LoggingLevel.DEBUG,
        LoggingLevel.FATAL,
        LoggingLevel.WARNING,
        LoggingLevel.OFF,
    ],
)
def test_smrb_cm_set_logging_level(
    component_manager: PstSmrbComponentManager,
    log_level: LoggingLevel,
) -> None:
    """Test SMRB component manager when set_log_level is updated."""
    api = MagicMock()
    component_manager._api = api

    component_manager.set_logging_level(log_level=log_level)
    api.set_log_level.assert_called_once_with(log_level=log_level)
