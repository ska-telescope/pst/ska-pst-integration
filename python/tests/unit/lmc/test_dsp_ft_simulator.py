# -*- coding: utf-8 -*-
#
# This file is part of the SKA PST project
#
# Distributed under the terms of the BSD 3-clause new license.
# See LICENSE for more info.
"""This module contains tests for the DSP Flow Through simulator class."""


import pytest
from ska_pst.lmc.dsp.dsp_ft_model import DspFlowThroughMonitorData
from ska_pst.lmc.dsp.dsp_ft_simulator import PstDspFlowThroughSimulator


@pytest.fixture
def simulator() -> PstDspFlowThroughSimulator:
    """Create a DSP Simulator fixture."""
    return PstDspFlowThroughSimulator()


def test_dsp_ft_simulator_using_constructor() -> None:
    """Test constructor creates correct simulator config."""
    simulator = PstDspFlowThroughSimulator(
        num_subbands=2,
    )
    assert simulator.num_subbands == 2
    data = simulator.get_data()

    # NOTE - in the future when we have monitoring properties this will change
    assert data == DspFlowThroughMonitorData()


def test_dps_simulator_configure_scan(simulator: PstDspFlowThroughSimulator) -> None:
    """Test that configuration of simulator sets up data."""
    configuration: dict = {
        "num_subbands": 2,
    }

    simulator.configure_scan(configuration=configuration)

    assert simulator.num_subbands == 2

    data = simulator._data_store.monitor_data

    # NOTE - in the future when we have monitoring properties this will change
    assert data == DspFlowThroughMonitorData()
