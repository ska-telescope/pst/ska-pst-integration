# -*- coding: utf-8 -*-
#
# This file is part of the SKA PST project
#
# Distributed under the terms of the BSD 3-clause new license.
# See LICENSE for more info.
"""This module defines elements of the pytest test harness shared by all tests."""

from __future__ import annotations

import collections
import json
import logging
import queue
import random
import string
import tempfile
import threading
import uuid
from concurrent import futures
from random import randint
from typing import Any, Callable, Dict, Generator, List, Optional, Tuple, cast
from unittest.mock import MagicMock

import grpc
import pytest
import tango
from ska_control_model import PstProcessingMode, SimulationMode
from ska_pst.common.constants import MEGA_HERTZ
from ska_pst.grpc.lmc.ska_pst_lmc_pb2 import StartScanRequest
from ska_pst.grpc.lmc.ska_pst_lmc_pb2_grpc import PstLmcServiceServicer, add_PstLmcServiceServicer_to_server
from ska_pst.lmc.beam import PstBeam, PstBeamComponentManager
from ska_pst.lmc.component import SubcomponentEventMessage
from ska_pst.lmc.device_proxy import DeviceProxyFactory
from ska_pst.lmc.dsp.dsp_disk_model import DEFAULT_RECORDING_TIME
from ska_pst.lmc.job import DeviceCommandTaskExecutor, TaskExecutor
from ska_pst.lmc.test.test_grpc_server import TestMockServicer, TestPstLmcService
from ska_pst.lmc.util.background_task import BackgroundTaskProcessor
from ska_pst.lmc.util.callback import Callback
from ska_pst.testutils.scan_config import ScanConfigGenerator
from ska_pst.testutils.tango import TangoChangeEventHelper
from ska_tango_base.executor import TaskStatus
from ska_tango_testing.mock import MockCallable
from ska_tango_testing.mock.tango import MockTangoEventCallbackGroup
from tango import DeviceProxy
from tango.test_context import DeviceTestContext, MultiDeviceTestContext, get_host_ip

from ska_pst.common import CbfPstConfig


class _ThreadingCallback:
    mock_callback: MagicMock
    callback_event: threading.Event

    def __init__(self: _ThreadingCallback, is_complete: Optional[Callable[..., bool]] = None):
        self.mock_callback = MagicMock()
        self.callback_event = threading.Event()
        self._is_complete = is_complete or self.is_complete

    def __call__(self: _ThreadingCallback, *args: Any, **kwargs: Any) -> Any:
        self.mock_callback(*args, **kwargs)
        if self._is_complete(*args, **kwargs):
            self.callback_event.set()

    def is_complete(
        self: _ThreadingCallback, *args: Any, status: Optional[TaskStatus] = None, **kwargs: Any
    ) -> bool:
        return status is not None and status in [TaskStatus.COMPLETED, TaskStatus.FAILED]

    def wait(self: _ThreadingCallback, timeout: Optional[float] = None) -> None:
        self.callback_event.wait(timeout=timeout)

    def clear(self: _ThreadingCallback) -> None:
        self.callback_event.clear()

    def set(self: _ThreadingCallback) -> None:
        self.callback_event.set()

    def __getattr__(self: _ThreadingCallback, name: str) -> Any:
        return getattr(self.mock_callback, name)


class _AttributeEventValidator:
    """Class to validate attribute events between BEAM and subordinate devices."""

    def __init__(
        self: _AttributeEventValidator,
        device_under_test: DeviceProxy,
        source_device_fqdn: str,
        attribute_name: str,
        default_value: Any,
        tango_change_event_helper: TangoChangeEventHelper,
        change_event_callbacks: MockTangoEventCallbackGroup,
        logger: logging.Logger,
    ) -> None:
        """Initialise validator."""
        self.logger = logger
        self.device_under_test = device_under_test
        self.source_device = DeviceProxyFactory.get_device(source_device_fqdn)
        if attribute_name == "subbandBeamConfiguration":
            self.attribute_name = "channelBlockConfiguration"
        else:
            self.attribute_name = attribute_name
        self.default_value = default_value

        self.attribute_value_queue: queue.Queue[Any] = queue.Queue()
        self.change_event_callbacks = change_event_callbacks

        tango_change_event_helper.subscribe(self.attribute_name)
        self.source_device.subscribe_change_event(attribute_name, self._store_value)

    def _store_value(self: _AttributeEventValidator, value: Any, *args: Any, **kwargs: Any) -> None:
        if self.attribute_name == "channelBlockConfiguration":
            # we need to map from subbandBeamConfiguration
            recv_subband_config = cast(dict, json.loads(value))
            if len(recv_subband_config) == 0:
                value = json.dumps(recv_subband_config)
            else:
                value = json.dumps(calc_expected_beam_channel_block_configuration(recv_subband_config))

        self.attribute_value_queue.put(value)

    def assert_initial_values(self: _AttributeEventValidator) -> None:
        """Assert initial values of BEAM and subordinate device as the same."""

        def _get_values() -> Tuple[Any, Any]:
            beam_value = getattr(self.device_under_test, self.attribute_name)
            if self.attribute_name == "channelBlockConfiguration":
                source_value = getattr(self.source_device, "subbandBeamConfiguration")
            else:
                source_value = getattr(self.source_device, self.attribute_name)

            return beam_value, source_value

        initial_values = _get_values()

        if self.attribute_name != "availableDiskSpace":
            # availableDiskSpace actually changes from a default value a new value when the On command
            # happens
            assert (
                initial_values[0] == self.default_value
            ), f"{self.attribute_name} on {self.device_under_test} not {self.default_value} but {initial_values[0]}"  # noqa: E501
            assert (
                initial_values[1] == self.default_value
            ), f"{self.attribute_name} on {self.device_under_test} not {self.default_value} but {initial_values[1]}"  # noqa: E501

    def assert_values(self: _AttributeEventValidator) -> None:
        """Assert that the events on BEAM as those from subordinate device."""
        # use None as a sentinel value to break out of assertion loop
        try:
            value: Any
            for value in iter(self.attribute_value_queue.get_nowait, None):
                if value is None:
                    break

                self.change_event_callbacks[self.attribute_name].assert_change_event(value, lookahead=3)
        except queue.Empty:
            pass


def _generate_random_string() -> str:
    letters = string.ascii_lowercase
    return "".join(random.choice(letters) for i in range(10))


@pytest.fixture
def default_device_property_config() -> Dict[str, dict]:
    """Get map of default values for device properties."""
    return {
        "test/recv/1": {
            "dataReceiveRate": 0.0,
            "dataReceived": 0,
            "dataDropRate": 0.0,
            "dataDropped": 0,
            "misorderedPackets": 0,
            "misorderedPacketRate": 0.0,
            "malformedPackets": 0,
            "malformedPacketRate": 0.0,
            "misdirectedPackets": 0,
            "misdirectedPacketRate": 0.0,
            "checksumFailurePackets": 0,
            "checksumFailurePacketRate": 0.0,
            "timestampSyncErrorPackets": 0,
            "timestampSyncErrorPacketRate": 0.0,
            "seqNumberSyncErrorPackets": 0,
            "seqNumberSyncErrorPacketRate": 0.0,
            "subbandBeamConfiguration": json.dumps({}),
        },
        "test/dsp/1": {
            "dataRecordRate": 0.0,
            "dataRecorded": 0,
            "availableRecordingTime": DEFAULT_RECORDING_TIME,
        },
        "test/smrb/1": {
            "ringBufferUtilisation": 0.0,
        },
    }


@pytest.fixture
def task_executor_max_parallel_workers() -> int:
    """
    Get the maximum number of parallel workers running for task executor.

    The default is 4 but this can be overriden in tests
    """
    return 4


@pytest.fixture
def task_executor(task_executor_max_parallel_workers: int) -> Generator[TaskExecutor, None, None]:
    """Return a generator for job executor."""
    executor = TaskExecutor(max_parallel_workers=task_executor_max_parallel_workers)
    executor.start()
    yield executor
    executor.stop()


@pytest.fixture
def device_command_task_executor(task_executor: TaskExecutor) -> DeviceCommandTaskExecutor:
    """Return a generator for a device command job executor."""
    return task_executor._device_task_executor


@pytest.fixture
def csp_configure_scan_request(
    eb_id: str,
    scan_config_generator: ScanConfigGenerator,
) -> dict:
    """Return valid configure JSON object that CSP would send."""
    return scan_config_generator.generate(eb_id=eb_id)


@pytest.fixture
def csp_configure_scan_request_json(csp_configure_scan_request: dict) -> str:
    """Get the CSP Configure Scan request as a JSON string."""
    return json.dumps(csp_configure_scan_request)


@pytest.fixture
def pst_configure_scan_request(csp_configure_scan_request: dict, cbf_pst_config: CbfPstConfig) -> dict:
    """Return a valid PST configure scan object."""
    request = {
        **csp_configure_scan_request["common"],
        **csp_configure_scan_request["pst"]["scan"],
    }

    request["cbf_pst_config"] = cbf_pst_config
    if "observation_mode" in request:
        request["pst_processing_mode"] = processing_mode = PstProcessingMode[request.pop("observation_mode")]
    else:
        request["pst_processing_mode"] = processing_mode = PstProcessingMode[
            request.pop("pst_processing_mode")
        ]

    if processing_mode == PstProcessingMode.FLOW_THROUGH:
        request["flow_through_params"] = request.pop("ft")

    request["bandwidth_mhz"] = request.pop("total_bandwidth") / MEGA_HERTZ
    request["centre_freq_mhz"] = request.pop("centre_frequency") / MEGA_HERTZ
    request["nchan"] = request["num_frequency_channels"]

    return request


@pytest.fixture
def expected_scan_request_protobuf(
    scan_request: dict,
) -> StartScanRequest:
    """Fixture for build expected start_scan request."""
    return StartScanRequest(scan_id=scan_request["scan_id"])


@pytest.fixture
def device_properties(
    grpc_endpoint: str,
    monitoring_polling_rate_ms: int,
    subsystem_id: str,
) -> dict:
    """Fixture that returns device_properties to be provided to the device under test."""
    return {
        "process_api_endpoint": grpc_endpoint,
        "DefaultMonitoringPollingRate": monitoring_polling_rate_ms,
        "SubsystemId": subsystem_id,
    }


@pytest.fixture()
def tango_context(
    device_test_config: dict,
) -> Generator[DeviceTestContext, None, None]:
    """Return a TANGO test context object, in which the device under test is running."""
    component_manager_patch = device_test_config.pop("component_manager_patch", None)
    if component_manager_patch is not None:

        def _create_component_manager(self: PstBeam) -> PstBeamComponentManager:
            return component_manager_patch(
                device_interface=self,
                logger=self.logger,
            )

        device_test_config["device"].create_component_manager = _create_component_manager

    if "timeout" not in device_test_config:
        device_test_config["timeout"] = 1
    device_test_config["process"] = False

    tango_context = DeviceTestContext(**device_test_config)
    tango_context.start()
    DeviceProxyFactory._proxy_supplier = tango_context.get_device
    yield tango_context
    tango_context.stop()


@pytest.fixture(scope="class")
def server_configuration() -> dict:
    """Get server configuration for multi device test."""
    return {}


def _generate_port() -> int:
    """Generate a random socket port number."""
    import socket
    from contextlib import closing

    with closing(socket.socket(socket.AF_INET, socket.SOCK_STREAM)) as s:
        s.bind(("", 0))
        s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        return s.getsockname()[1]


@pytest.fixture(scope="module")
def grpc_port() -> int:
    """Fixture to generate port for gRPC."""
    return _generate_port()


@pytest.fixture
def smrb_grpc_port() -> int:
    """Fixture to generate port for gRPC."""
    return _generate_port()


@pytest.fixture
def recv_grpc_port() -> int:
    """Fixture to generate port for gRPC."""
    return _generate_port()


@pytest.fixture
def dsp_disk_grpc_port() -> int:
    """Fixture to generate port for gRPC."""
    return _generate_port()


@pytest.fixture
def dsp_flow_through_grpc_port() -> int:
    """Fixture to generate port for gRPC."""
    return _generate_port()


@pytest.fixture
def stat_grpc_port() -> int:
    """Fixture to generate port for gRPC."""
    return _generate_port()


@pytest.fixture
def client_id() -> str:
    """Generate a random client_id string."""
    return _generate_random_string()


@pytest.fixture
def device_name(client_id: str) -> str:
    """Generate a random device name."""
    import random

    return f"test/{client_id}/{random.randint(0,16)}"


@pytest.fixture(scope="module")
def grpc_endpoint(grpc_port: int) -> str:
    """Return the endpoint of the gRPC server."""
    return f"127.0.0.1:{grpc_port}"


@pytest.fixture
def smrb_process_api_endpoint(smrb_grpc_port: int) -> str:
    """Return the endpoint of the gRPC server."""
    return f"127.0.0.1:{smrb_grpc_port}"


@pytest.fixture
def recv_process_api_endpoint(recv_grpc_port: int) -> str:
    """Return the endpoint of the gRPC server."""
    return f"127.0.0.1:{recv_grpc_port}"


@pytest.fixture
def dsp_disk_process_api_endpoint(dsp_disk_grpc_port: int) -> str:
    """Return the endpoint of the gRPC server."""
    return f"127.0.0.1:{dsp_disk_grpc_port}"


@pytest.fixture
def dsp_flow_through_process_api_endpoint(dsp_flow_through_grpc_port: int) -> str:
    """Return the endpoint of the gRPC server."""
    return f"127.0.0.1:{dsp_flow_through_grpc_port}"


@pytest.fixture
def stat_process_api_endpoint(stat_grpc_port: int) -> str:
    """Return the endpoint of the gRPC server."""
    return f"127.0.0.1:{stat_grpc_port}"


@pytest.fixture(scope="module")
def grpc_servicer(mock_servicer_context: MagicMock, logger: logging.Logger) -> TestMockServicer:
    """Create a test mock servicer given mock context."""
    return TestMockServicer(
        context=mock_servicer_context,
        logger=logger,
    )


@pytest.fixture(scope="module")
def grpc_service(
    grpc_port: int,
    grpc_servicer: PstLmcServiceServicer,
) -> grpc.Server:
    """Create instance of gRPC server."""
    grpc_tpe = futures.ThreadPoolExecutor(max_workers=10)
    server = grpc.server(grpc_tpe)
    server.add_insecure_port(f"0.0.0.0:{grpc_port}")
    add_PstLmcServiceServicer_to_server(servicer=grpc_servicer, server=server)

    return server


@pytest.fixture
def service_uuid() -> str:
    """Get a UUID string for a service."""
    return str(uuid.uuid4())


@pytest.fixture
def service_name() -> str:
    """Get a random service name string."""
    return _generate_random_string()


@pytest.fixture(scope="module")
def pst_lmc_service(
    grpc_service: grpc.Server,
    logger: logging.Logger,
) -> Generator[TestPstLmcService, None, None]:
    """Yield an instance of a PstLmcServiceServicer for testing."""
    service = TestPstLmcService(
        grpc_server=grpc_service,
        logger=logger,
    )
    evt = threading.Event()
    service.serve(started_callback=evt.set)
    evt.wait()
    yield service
    service.stop()


@pytest.fixture(scope="module")
def mock_servicer_context() -> MagicMock:
    """Generate a mock gRPC servicer context to use with testing of gRPC calls."""
    return MagicMock()


@pytest.fixture
def multidevice_test_context(
    server_configuration: dict, logger: logging.Logger
) -> Generator[MultiDeviceTestContext, None, None]:
    """Get generator for MultiDeviceTestContext."""
    if "host" not in server_configuration:
        server_configuration["host"] = get_host_ip()
    if "port" not in server_configuration:
        server_configuration["port"] = _generate_port()
    if "timeout" not in server_configuration:
        server_configuration["timeout"] = 5
    if "daemon" not in server_configuration:
        server_configuration["daemon"] = True

    def device_proxy_supplier(fqdn: str, *args: Any, **kwargs: Any) -> tango.DeviceProxy:
        if not fqdn.startswith("tango://"):
            host = server_configuration["host"]
            port = server_configuration["port"]

            fqdn = f"tango://{host}:{port}/{fqdn}#dbase=no"

        return tango.DeviceProxy(fqdn)

    DeviceProxyFactory._proxy_supplier = device_proxy_supplier

    logger.debug(f"Creating multidevice_test_context {server_configuration}")
    with MultiDeviceTestContext(**server_configuration) as context:
        logger.debug("Created multidevice_test_context")
        yield context


@pytest.fixture()
def device_under_test(tango_context: DeviceTestContext, logger: logging.Logger) -> DeviceProxy:
    """
    Return a device proxy to the device under test.

    :param tango_context: a TANGO test context with the specified device
        running
    :type tango_context: :py:class:`tango.DeviceTestContext`

    :return: a proxy to the device under test
    :rtype: :py:class:`tango.DeviceProxy`
    """
    device: DeviceProxy = tango_context.device
    logger.info(f"{device}.healthState = {device.healthState}")
    return device


@pytest.fixture()
def callbacks() -> dict:
    """
    Return a dictionary of callbacks with asynchronous support.

    :return: a collections.defaultdict that returns callbacks by name.
    """
    return collections.defaultdict(MockCallable)


@pytest.fixture
def abort_event() -> threading.Event:
    """Get fixture to handle aborting threads."""
    return threading.Event()


@pytest.fixture
def stub_background_processing() -> bool:
    """Fixture used to make background processing synchronous."""
    return True


@pytest.fixture
def background_task_processor(
    logger: logging.Logger,
    stub_background_processing: bool,
    monkeypatch: pytest.MonkeyPatch,
) -> BackgroundTaskProcessor:
    """
    Fixture to create background task processor.

    This can be used in synchronous or background processing if a stub_background_processing returns True or
    False, the default is to always stub.
    """
    processor = BackgroundTaskProcessor(default_logger=logger)

    if stub_background_processing:
        # need to stub the submit_task and replace
        logger.debug("Stubbing background processing")

        def _submit_task(
            action_fn: Callable,
            *args: Any,
            **kwargs: Any,
        ) -> MagicMock:
            action_fn()
            return MagicMock()

        monkeypatch.setattr(processor, "submit_task", _submit_task)

    return processor

    return BackgroundTaskProcessor(default_logger=logger)


@pytest.fixture
def communication_state_callback() -> Callable:
    """Create a communication state callback."""
    return MagicMock()


@pytest.fixture
def component_state_callback() -> Callable:
    """Create a component state callback."""
    return MagicMock()


@pytest.fixture
def task_callback() -> Callback:
    """Create a mock component to validate task callbacks."""
    return MagicMock()


@pytest.fixture
def simulation_mode(request: pytest.FixtureRequest) -> SimulationMode:
    """Set simulation mode for test."""
    try:
        return request.param.get("simulation_mode", SimulationMode.TRUE)  # type: ignore
    except Exception:
        return SimulationMode.TRUE


@pytest.fixture
def recv_data_host() -> str:
    """Get network interface for RECV to listen on."""
    return "127.0.0.1"


@pytest.fixture
def recv_data_mac() -> str:
    """Get MAC Address for the network interface of RECV."""
    return "01:02:03:ab:cd:ef"


@pytest.fixture
def subband_udp_ports() -> List[int]:
    """Get UDP port for RECV to listen on."""
    return [randint(20000, 30000) for _ in range(4)]


@pytest.fixture
def subband_monitor_data_callback() -> MagicMock:
    """Create a callback that can be used for subband data monitoring."""
    return MagicMock()


@pytest.fixture
def monitor_data_callback() -> MagicMock:
    """Create fixture for monitor data callback testing."""
    return MagicMock()


@pytest.fixture
def property_callback() -> MagicMock:
    """Create fixture for testing property callbacks."""
    return MagicMock()


@pytest.fixture(scope="session")
def monitoring_polling_rate_ms() -> int:
    """Fixture to get monitoring polling rate for test in milliseconds."""
    return 100


@pytest.fixture
def monitor_data_updated_callback(monitor_data_callback: MagicMock) -> MagicMock:
    """Get monitor data updated callback mock."""
    return monitor_data_callback


@pytest.fixture(scope="session")
def health_check_interval() -> int:
    """Fixture to get the health check interval for test in milliseconds."""
    return 100


def calc_expected_beam_channel_block_configuration(recv_subband_config: dict) -> dict:
    """Calculate the expected channel block configuration JSON."""
    num_subband = recv_subband_config["common"]["nsubband"]
    subbands = recv_subband_config["subbands"]

    return {
        "num_channel_blocks": num_subband,
        "channel_blocks": [
            {
                "destination_host": subbands[subband_id]["data_host"],
                "destination_port": subbands[subband_id]["data_port"],
                "destination_mac": subbands[subband_id]["data_mac"],
                "start_pst_channel": subbands[subband_id]["start_channel"],
                "start_pst_frequency": subbands[subband_id]["start_centre_freq_mhz"] * MEGA_HERTZ,
                "num_pst_channels": subbands[subband_id]["end_channel"]
                - subbands[subband_id]["start_channel"],
            }
            for subband_id in range(1, num_subband + 1)
        ],
    }


@pytest.fixture
def fail_validate_configure_beam() -> bool:
    """Fixture used to override simulator to fail validation or not."""
    return False


@pytest.fixture
def fail_validate_configure_scan() -> bool:
    """Fixture used to override simulator to fail validation or not."""
    return False


@pytest.fixture(scope="session")
def scan_output_dir_pattern() -> str:
    """Get the pattern for the output directory used for scan files."""
    tmp_dir = tempfile.gettempdir()

    # for unit testing, use /tmp/product as top level dir
    return f"{tmp_dir}/product/<eb_id>/<subsystem_id>/<scan_id>"


@pytest.fixture
def health_check_handler() -> MagicMock:
    """Get a mock health check handler."""
    return MagicMock()


@pytest.fixture
def subcomponent_event_queue() -> queue.Queue[SubcomponentEventMessage]:
    """Get the subcomponent event queue."""
    return queue.Queue()


@pytest.fixture
def mock_device_proxy() -> MagicMock:
    """Create a mock device proxy."""
    return MagicMock()
