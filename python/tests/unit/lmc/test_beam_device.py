# -*- coding: utf-8 -*-
#
# This file is part of the SKA PST project
#
# Distributed under the terms of the BSD 3-clause new license.
# See LICENSE for more info.
"""Test to the BEAM TANGO device for PST.LMC."""

from __future__ import annotations

import dataclasses
import json
import logging
import random
from typing import Any, Callable, List, cast
from unittest.mock import MagicMock

import backoff
import pytest
import tango
from ska_control_model import AdminMode, HealthState, LoggingLevel, ObsMode, ObsState, PstProcessingMode
from ska_pst.lmc import PstBeam
from ska_pst.lmc.beam import PstBeamComponentManager, PstBeamDeviceInterface
from ska_pst.lmc.component.pst_device import as_device_attribute_name
from ska_pst.testutils.tango import TangoChangeEventHelper, TangoDeviceCommandChecker
from ska_tango_testing.mock.tango import MockTangoEventCallbackGroup
from tango import DevFailed, DeviceProxy, DevState

from ska_pst.common import TelescopeFacilityEnum


@pytest.fixture
def device_properties(
    monitoring_polling_rate_ms: int,
    health_check_interval: int,
    scan_output_dir_pattern: str,
    subsystem_id: str,
    telescope_facility: TelescopeFacilityEnum,
) -> dict:
    """Get the device properties to configure the BEAM TANGO device under test."""
    return {
        "DeviceID": 1,
        "Facility": telescope_facility.name,
        "RecvProcessApiEndpoint": "ska-pst-core:28080",
        "SmrbProcessApiEndpoint": "ska-pst-core:28081",
        "DspDiskProcessApiEndpoint": "ska-pst-core:28082",
        "StatProcessApiEndpoint": "ska-pst-core:28083",
        "ScanOutputDirPattern": scan_output_dir_pattern,
        "SubsystemId": subsystem_id,
        "DefaultMonitoringPollingRate": monitoring_polling_rate_ms,
        "DefaultHealthCheckInterval": health_check_interval,
    }


@pytest.fixture
def mock_recv_component_manager() -> MagicMock:
    """Get a mocked RECV component manager."""
    return MagicMock()


@pytest.fixture
def mock_smrb_component_manager() -> MagicMock:
    """Get a mocked SMRB component manager."""
    return MagicMock()


@pytest.fixture
def mock_stat_component_manager() -> MagicMock:
    """Get a mocked STAT component manager."""
    return MagicMock()


@pytest.fixture
def mock_dsp_component_manager() -> MagicMock:
    """Get a mocked DSP component manager."""
    return MagicMock()


@dataclasses.dataclass(kw_only=True, frozen=True)
class _TangoAttributeAlarmConfig:
    cm_attr: str
    tango_attr: str
    default: Any
    source: MagicMock = dataclasses.field(repr=False)
    warning: Any | None = None
    alarm: Any | None = None


@pytest.fixture
def tango_attribute_alarm_configs(
    mock_dsp_component_manager: MagicMock,
    mock_recv_component_manager: MagicMock,
    mock_smrb_component_manager: MagicMock,
    mock_stat_component_manager: MagicMock,
) -> List[_TangoAttributeAlarmConfig]:
    """Get fixture with all the TANGO attribute alarm configuration."""
    return [
        _TangoAttributeAlarmConfig(
            tango_attr="dataDropRate",
            cm_attr="data_drop_rate",
            default=0.0,
            warning=0.1,
            source=mock_recv_component_manager,
        ),
        _TangoAttributeAlarmConfig(
            tango_attr="misorderedPacketRate",
            cm_attr="misordered_packet_rate",
            default=0.0,
            warning=0.1,
            source=mock_recv_component_manager,
        ),
        _TangoAttributeAlarmConfig(
            tango_attr="malformedPacketRate",
            cm_attr="malformed_packet_rate",
            default=0.0,
            warning=0.1,
            source=mock_recv_component_manager,
        ),
        _TangoAttributeAlarmConfig(
            tango_attr="misdirectedPacketRate",
            cm_attr="misdirected_packet_rate",
            default=0.0,
            warning=0.1,
            source=mock_recv_component_manager,
        ),
        _TangoAttributeAlarmConfig(
            tango_attr="checksumFailurePacketRate",
            cm_attr="checksum_failure_packet_rate",
            default=0.0,
            warning=0.1,
            source=mock_recv_component_manager,
        ),
        _TangoAttributeAlarmConfig(
            tango_attr="timestampSyncErrorPacketRate",
            cm_attr="timestamp_sync_error_packet_rate",
            default=0.0,
            warning=0.1,
            source=mock_recv_component_manager,
        ),
        _TangoAttributeAlarmConfig(
            tango_attr="seqNumberSyncErrorPacketRate",
            cm_attr="seq_number_sync_error_packet_rate",
            default=0.0,
            warning=0.1,
            source=mock_recv_component_manager,
        ),
        _TangoAttributeAlarmConfig(
            tango_attr="noValidPolarisationCorrectionPacketRate",
            cm_attr="no_valid_polarisation_correction_packet_rate",
            default=0.0,
            warning=0.1,
            source=mock_recv_component_manager,
        ),
        _TangoAttributeAlarmConfig(
            tango_attr="noValidStationBeamPacketRate",
            cm_attr="no_valid_station_beam_packet_rate",
            default=0.0,
            warning=0.1,
            source=mock_recv_component_manager,
        ),
        _TangoAttributeAlarmConfig(
            tango_attr="noValidPstBeamPacketRate",
            cm_attr="no_valid_pst_beam_packet_rate",
            default=0.0,
            warning=0.1,
            source=mock_recv_component_manager,
        ),
        _TangoAttributeAlarmConfig(
            tango_attr="availableRecordingTime",
            cm_attr="available_recording_time",
            source=cast(MagicMock, mock_dsp_component_manager.dsp_disk_monitor_data),
            default=120.0,
            warning=60.0,
            alarm=10.0,
        ),
        _TangoAttributeAlarmConfig(
            tango_attr="ringBufferUtilisation",
            cm_attr="ring_buffer_utilisation",
            default=0.0,
            warning=50.0,
            alarm=90.0,
            source=mock_smrb_component_manager,
        ),
        _TangoAttributeAlarmConfig(
            tango_attr="realPolAVarianceFreqAvg",
            cm_attr="real_pol_a_variance_freq_avg",
            default=1.0,
            warning=0.0,
            source=mock_stat_component_manager,
        ),
        _TangoAttributeAlarmConfig(
            tango_attr="realPolANumClippedSamples",
            cm_attr="real_pol_a_num_clipped_samples",
            default=0,
            warning=1,
            source=mock_stat_component_manager,
        ),
        _TangoAttributeAlarmConfig(
            tango_attr="imagPolAVarianceFreqAvg",
            cm_attr="imag_pol_a_variance_freq_avg",
            default=1.0,
            warning=0.0,
            source=mock_stat_component_manager,
        ),
        _TangoAttributeAlarmConfig(
            tango_attr="imagPolANumClippedSamples",
            cm_attr="imag_pol_a_num_clipped_samples",
            default=0,
            warning=1,
            source=mock_stat_component_manager,
        ),
        _TangoAttributeAlarmConfig(
            tango_attr="realPolAVarianceFreqAvgRfiExcised",
            cm_attr="real_pol_a_variance_freq_avg_rfi_excised",
            default=1.0,
            warning=0.0,
            source=mock_stat_component_manager,
        ),
        _TangoAttributeAlarmConfig(
            tango_attr="realPolANumClippedSamplesRfiExcised",
            cm_attr="real_pol_a_num_clipped_samples_rfi_excised",
            default=0,
            warning=1,
            source=mock_stat_component_manager,
        ),
        _TangoAttributeAlarmConfig(
            tango_attr="imagPolAVarianceFreqAvgRfiExcised",
            cm_attr="imag_pol_a_variance_freq_avg_rfi_excised",
            default=1.0,
            warning=0.0,
            source=mock_stat_component_manager,
        ),
        _TangoAttributeAlarmConfig(
            tango_attr="imagPolANumClippedSamplesRfiExcised",
            cm_attr="imag_pol_a_num_clipped_samples_rfi_excised",
            default=0,
            warning=1,
            source=mock_stat_component_manager,
        ),
        _TangoAttributeAlarmConfig(
            tango_attr="realPolBVarianceFreqAvg",
            cm_attr="real_pol_b_variance_freq_avg",
            default=1.0,
            warning=0.0,
            source=mock_stat_component_manager,
        ),
        _TangoAttributeAlarmConfig(
            tango_attr="realPolBNumClippedSamples",
            cm_attr="real_pol_b_num_clipped_samples",
            default=0,
            warning=1,
            source=mock_stat_component_manager,
        ),
        _TangoAttributeAlarmConfig(
            tango_attr="imagPolBVarianceFreqAvg",
            cm_attr="imag_pol_b_variance_freq_avg",
            default=1.0,
            warning=0.0,
            source=mock_stat_component_manager,
        ),
        _TangoAttributeAlarmConfig(
            tango_attr="imagPolBNumClippedSamples",
            cm_attr="imag_pol_b_num_clipped_samples",
            default=0,
            warning=1,
            source=mock_stat_component_manager,
        ),
        _TangoAttributeAlarmConfig(
            tango_attr="realPolBVarianceFreqAvgRfiExcised",
            cm_attr="real_pol_b_variance_freq_avg_rfi_excised",
            default=1.0,
            warning=0.0,
            source=mock_stat_component_manager,
        ),
        _TangoAttributeAlarmConfig(
            tango_attr="realPolBNumClippedSamplesRfiExcised",
            cm_attr="real_pol_b_num_clipped_samples_rfi_excised",
            default=0,
            warning=1,
            source=mock_stat_component_manager,
        ),
        _TangoAttributeAlarmConfig(
            tango_attr="imagPolBVarianceFreqAvgRfiExcised",
            cm_attr="imag_pol_b_variance_freq_avg_rfi_excised",
            default=1.0,
            warning=0.0,
            source=mock_stat_component_manager,
        ),
        _TangoAttributeAlarmConfig(
            tango_attr="imagPolBNumClippedSamplesRfiExcised",
            cm_attr="imag_pol_b_num_clipped_samples_rfi_excised",
            default=0,
            warning=1,
            source=mock_stat_component_manager,
        ),
    ]


@pytest.mark.forked
class TestPstBeam:
    """Test class used for testing the PstBeam TANGO device."""

    @pytest.fixture
    def device_test_config(
        self: TestPstBeam,
        device_properties: dict,
        patchable_component_manager: bool,
        component_manager_provider: Callable[..., PstBeamComponentManager],
    ) -> dict:
        """Get the TANGO device under test config."""
        return {
            "device": PstBeam,
            "process": False,
            "properties": device_properties,
            "component_manager_patch": component_manager_provider if patchable_component_manager else None,
        }

    @pytest.fixture
    def patchable_component_manager(self: TestPstBeam) -> bool:
        """Get fixture value for whether to get a patchable component manager."""
        return False

    @pytest.fixture
    def component_manager_provider(
        self: TestPstBeam,
        patchable_component_manager: bool,
    ) -> Callable[..., PstBeamComponentManager]:
        """Get fixture that can provide instances of the PstBeamComponentManager."""
        if patchable_component_manager:
            component_manager: PstBeamComponentManager | None = None

            def _provider(
                *, device_interface: PstBeamDeviceInterface | None = None, **kwargs: Any
            ) -> PstBeamComponentManager:
                nonlocal component_manager

                if component_manager is None:
                    assert device_interface is not None, "expected device to not be None"
                    component_manager = PstBeamComponentManager(
                        device_interface=device_interface,
                        **kwargs,
                    )

                return component_manager

            return _provider
        else:
            return PstBeamComponentManager

    @pytest.fixture(autouse=True)
    def setup_test_class(
        self: TestPstBeam,
        device_under_test: DeviceProxy,
        change_event_callbacks: MockTangoEventCallbackGroup,
        beam_attribute_names: List[str],
        logger: logging.Logger,
    ) -> None:
        """Put test class with TANGO devices and event checker."""
        logger.info(f"healthState = {device_under_test.healthState}")

        self.beam_proxy = device_under_test
        self._beam_attribute_names = [*beam_attribute_names]

        self.tango_change_event_helper = TangoChangeEventHelper(
            device_under_test=self.beam_proxy,
            change_event_callbacks=change_event_callbacks,
            logger=logger,
        )

        self.tango_device_command_checker = TangoDeviceCommandChecker(
            tango_change_event_helper=self.tango_change_event_helper,
            change_event_callbacks=change_event_callbacks,
            logger=logger,
        )

    def current_attribute_values(self: TestPstBeam) -> dict:
        """Get current attribute values for BEAM device."""
        # ignore availableDiskSpace as this can change but other props
        # will should get reset when going to Off
        return {
            attr: getattr(self.beam_proxy, attr)
            for attr in self._beam_attribute_names
            if attr not in ["availableDiskSpace", "diskCapacity", "diskUsedPercentage", "diskUsedBytes"]
        }

    @backoff.on_exception(
        backoff.expo,
        AssertionError,
        factor=0.05,
        max_time=1.0,
    )
    def assert_admin_mode(self: TestPstBeam, admin_mode: AdminMode) -> None:
        """Assert admin mode of device."""
        assert self.beam_proxy.adminMode == admin_mode

    @backoff.on_exception(
        backoff.expo,
        AssertionError,
        factor=0.05,
        max_time=1.0,
    )
    def assert_state(self: TestPstBeam, state: DevState) -> None:
        """Assert device state of device."""
        assert self.beam_proxy.state() == state

    @backoff.on_exception(
        backoff.expo,
        AssertionError,
        factor=0.05,
        max_time=1.0,
    )
    def assert_health_state(self: TestPstBeam, health_state: HealthState) -> None:
        """Assert health state of device."""
        assert self.beam_proxy.healthState == health_state

    def assert_logging_level(self: TestPstBeam, logging_level: LoggingLevel) -> None:
        """Assert the logging level of device."""

        @backoff.on_exception(
            backoff.expo,
            AssertionError,
            factor=0.05,
            max_time=1.0,
        )
        def _assert(device: DeviceProxy) -> None:
            curr_logging_level = LoggingLevel(device.loggingLevel)
            assert (
                logging_level == curr_logging_level
            ), f"Expected {device}.loggingLevel to be {logging_level.name} was {curr_logging_level.name}"

        _assert(self.beam_proxy)

    @backoff.on_exception(
        backoff.expo,
        AssertionError,
        factor=0.05,
        max_time=1.0,
    )
    def assert_obs_state(self: TestPstBeam, obs_state: ObsState) -> None:
        """Assert the observation state of device."""
        assert self.beam_proxy.obsState == obs_state

    @backoff.on_exception(
        backoff.expo,
        AssertionError,
        factor=0.05,
        max_time=1.0,
    )
    def assert_obs_mode(self: TestPstBeam, obs_mode: ObsMode) -> None:
        """Assert the observation mode of device."""
        assert self.beam_proxy.obsMode == obs_mode

    @backoff.on_exception(
        backoff.expo,
        AssertionError,
        factor=0.05,
        max_time=1.0,
    )
    def assert_pst_processing_mode(self: TestPstBeam, pst_processing_mode: PstProcessingMode) -> None:
        """
        Assert that the PST processing mode of device.

        :param pst_processing_mode: expected PST processing mode value
        :type pst_processing_mode: PstProcessingMode
        """
        assert self.beam_proxy.pstProcessingMode == pst_processing_mode.name
        # this is due to field being deprecated. Check that it is the same as pstProcessingMode
        assert self.beam_proxy.processingMode == pst_processing_mode.name

    def configure_scan(self: TestPstBeam, configuration: dict) -> None:
        """Perform a configure scan."""
        self.tango_device_command_checker.assert_command(
            command="ConfigureScan",
            command_args=(json.dumps(configuration),),
            expected_obs_state_events=[
                ObsState.CONFIGURING,
                ObsState.READY,
            ],
        )
        self.assert_obs_state(ObsState.READY)

    def scan(self: TestPstBeam, scan_request: dict) -> None:
        """Perform a scan."""
        self.tango_device_command_checker.assert_command(
            command="Scan",
            command_args=(json.dumps(scan_request),),
            expected_obs_state_events=[
                ObsState.SCANNING,
            ],
        )
        self.assert_obs_state(ObsState.SCANNING)

    def scan_legacy(self: TestPstBeam, scan_id: int) -> None:
        """Perform a scan using legacy API.

        This method will stringify a scan_id and send it rather than
        using a JSON request.
        """
        self.tango_device_command_checker.assert_command(
            command="Scan",
            command_args=(str(scan_id),),
            expected_obs_state_events=[
                ObsState.SCANNING,
            ],
        )
        self.assert_obs_state(ObsState.SCANNING)

    def end_scan(self: TestPstBeam) -> None:
        """End current scan."""
        self.tango_device_command_checker.assert_command(
            command="EndScan",
            expected_obs_state_events=[
                ObsState.READY,
            ],
        )
        self.assert_obs_state(ObsState.READY)

    def goto_idle(self: TestPstBeam) -> None:
        """Put TANGO device into IDLE state."""
        self.tango_device_command_checker.assert_command(
            command="GoToIdle",
            expected_obs_state_events=[
                ObsState.IDLE,
            ],
        )
        self.assert_obs_state(ObsState.IDLE)

    def abort(self: TestPstBeam) -> None:
        """Abort long running command."""
        self.tango_device_command_checker.assert_command(
            command="Abort",
            expected_obs_state_events=[
                ObsState.ABORTING,
                ObsState.ABORTED,
            ],
        )

    def goto_fault(self: TestPstBeam, fault_msg: str) -> None:
        """Force device to go to fault."""
        self.tango_device_command_checker.assert_command(
            command="GoToFault",
            command_args=(fault_msg,),
            expected_obs_state_events=[
                ObsState.FAULT,
            ],
        )
        self.assert_obs_state(ObsState.FAULT)
        assert self.beam_proxy.healthFailureMessage == fault_msg
        assert self.beam_proxy.healthState == HealthState.FAILED

    def obs_reset(self: TestPstBeam) -> None:
        """Reset TANGO device."""
        self.tango_device_command_checker.assert_command(
            command="ObsReset",
            expected_obs_state_events=[
                ObsState.RESETTING,
                ObsState.IDLE,
            ],
        )
        self.assert_obs_state(ObsState.IDLE)

    def reset(self: TestPstBeam, expected_obs_state_events: List[ObsState] | None = None) -> None:
        """Restart TANGO device."""
        expected_obs_state_events = expected_obs_state_events or [
            ObsState.RESETTING,
            ObsState.IDLE,
        ]

        self.tango_device_command_checker.assert_command(
            command="Reset",
            expected_obs_state_events=expected_obs_state_events,
        )
        self.assert_obs_state(ObsState.IDLE)

    def on(self: TestPstBeam) -> None:
        """Turn on TANGO device."""
        self.tango_device_command_checker.assert_command(
            command="On",
            expected_obs_state_events=[ObsState.IDLE],
            # in PyTango this can take a while 9.4.x
            timeout=15.0,
        )
        self.assert_state(DevState.ON)

        # need to configure beam
        self.assert_obs_state(ObsState.IDLE)

    def off(self: TestPstBeam) -> None:
        """Turn off TANGO device."""
        self.tango_device_command_checker.assert_command(
            command="Off",
        )
        self.assert_state(DevState.OFF)
        self.assert_health_state(HealthState.OK)

    def online(self: TestPstBeam) -> None:
        """Put TANGO device into ONLINE mode."""
        self.beam_proxy.adminMode = AdminMode.ONLINE
        self.assert_admin_mode(admin_mode=AdminMode.ONLINE)
        self.assert_state(DevState.OFF)
        self.assert_health_state(HealthState.OK)

    def offline(self: TestPstBeam) -> None:
        """Put TANGO device into OFFLINE mode."""
        self.beam_proxy.adminMode = AdminMode.OFFLINE
        self.assert_admin_mode(admin_mode=AdminMode.OFFLINE)
        self.assert_state(DevState.DISABLE)
        self.assert_health_state(HealthState.UNKNOWN)

    def test_beam_mgmt_State(self: TestPstBeam, device_under_test: DeviceProxy) -> None:
        """
        Test for State.

        :param device_under_test: a proxy to the device under test
        """
        device_under_test.adminMode = AdminMode.ONLINE
        assert device_under_test.state() == DevState.OFF
        assert device_under_test.Status() == "The device is in OFF state."

    def test_beam_mgmt_GetVersionInfo(self: TestPstBeam, device_under_test: DeviceProxy) -> None:
        """
        Test for GetVersionInfo.

        :param device_under_test: a proxy to the device under test
        """
        import re

        version_pattern = (
            f"{device_under_test.info().dev_class}, ska_pst.lmc, "
            "[0-9]+.[0-9]+.[0-9]+, A set of PST LMC tango devices for the SKA Low and Mid Telescopes."
        )
        version_info = device_under_test.GetVersionInfo()
        assert len(version_info) == 1
        assert re.match(version_pattern, version_info[0])

    def test_beam_mgmt_configure_then_scan_then_stop(
        self: TestPstBeam,
        csp_configure_scan_request: dict,
        csp_configure_scan_request_json: str,
        scan_request: dict,
        processing_mode: PstProcessingMode,
    ) -> None:
        """Test state model of PstBeam can configure, scan and stop."""
        # need to go through state mode
        self.assert_health_state(HealthState.UNKNOWN)

        self.online()
        self.on()

        assert self.beam_proxy.processingMode == "IDLE"
        self.assert_obs_mode(ObsMode.IDLE)

        self.configure_scan(csp_configure_scan_request)
        self.assert_obs_mode(ObsMode.PULSAR_TIMING)

        assert self.beam_proxy.processingMode == processing_mode.name

        assert self.beam_proxy.lastScanConfiguration == csp_configure_scan_request_json

        self.scan(scan_request)
        self.assert_obs_mode(ObsMode.PULSAR_TIMING)

        self.end_scan()
        assert self.beam_proxy.processingMode == processing_mode.name
        self.assert_obs_mode(ObsMode.PULSAR_TIMING)

        self.goto_idle()
        assert self.beam_proxy.processingMode == "IDLE"
        self.assert_obs_mode(ObsMode.IDLE)

        self.off()
        self.offline()

    def test_beam_mgmt_configure_when_in_ready_state(
        self: TestPstBeam,
        csp_configure_scan_request: dict,
        csp_configure_scan_request_json: str,
        scan_request: dict,
        processing_mode: PstProcessingMode,
    ) -> None:
        """Test state model of PstBeam can configure, scan and stop."""
        # need to go through state mode
        self.assert_health_state(HealthState.UNKNOWN)

        self.online()
        self.on()

        assert self.beam_proxy.processingMode == "IDLE"
        self.assert_obs_mode(ObsMode.IDLE)

        self.configure_scan(csp_configure_scan_request)
        self.assert_obs_mode(ObsMode.PULSAR_TIMING)

        assert self.beam_proxy.processingMode == processing_mode.name

        assert self.beam_proxy.lastScanConfiguration == csp_configure_scan_request_json

        self.scan(scan_request)
        self.assert_obs_mode(ObsMode.PULSAR_TIMING)

        self.end_scan()
        assert self.beam_proxy.processingMode == processing_mode.name
        self.assert_obs_mode(ObsMode.PULSAR_TIMING)

        # Currently in ObsState.READY
        self.assert_obs_state(ObsState.READY)
        self.configure_scan(csp_configure_scan_request)

        self.off()
        self.offline()

    def test_beam_mgmt_handles_stringified_scan_id(
        self: TestPstBeam,
        csp_configure_scan_request: dict,
        scan_id: int,
    ) -> None:
        """Test state model of PstBeam can configure, scan and stop."""
        # need to go through state mode
        self.assert_health_state(HealthState.UNKNOWN)

        self.online()
        self.on()

        self.configure_scan(csp_configure_scan_request)

        self.scan_legacy(scan_id)
        self.end_scan()
        self.goto_idle()
        self.off()
        self.offline()

    def test_beam_mgmt_configure_when_validation_error_from_api(
        self: TestPstBeam,
        csp_configure_scan_request: dict,
    ) -> None:
        """Test state model of PstBeam can configure, scan and stop."""
        # need to go through state mode
        self.assert_health_state(HealthState.UNKNOWN)

        self.online()
        self.on()

        self.assert_obs_state(ObsState.IDLE)

        csp_configure_scan_request["pst"]["scan"]["source"] = "invalid source"
        request_str = json.dumps(csp_configure_scan_request)

        with pytest.raises(DevFailed) as exc:
            self.beam_proxy.ConfigureScan(request_str)

        # PyTango is weird and doesn't provide helpful methods to get this
        error: DevFailed = exc.value
        error_reason: str = error.args[0].desc

        assert "Simulated validation error due to invalid source" in error_reason

        self.assert_obs_state(ObsState.IDLE)

        self.off()
        self.offline()

    def test_beam_mgmt_abort_then_obs_reset(
        self: TestPstBeam,
        csp_configure_scan_request: dict,
        scan_request: dict,
    ) -> None:
        """Test PstBeam can abort and then obs reset."""
        self.assert_health_state(HealthState.UNKNOWN)
        self.online()
        self.on()

        self.configure_scan(csp_configure_scan_request)

        self.scan(scan_request)

        self.abort()
        self.assert_obs_state(ObsState.ABORTED)
        self.obs_reset()
        self.assert_obs_state(ObsState.IDLE)
        self.assert_obs_mode(ObsMode.IDLE)
        self.assert_health_state(HealthState.OK)
        self.assert_pst_processing_mode(PstProcessingMode.IDLE)

        self.off()
        self.offline()

    def test_beam_mgmt_abort_then_reset(
        self: TestPstBeam,
        csp_configure_scan_request: dict,
        scan_request: dict,
    ) -> None:
        """Test PstBeam can abort and then reset."""
        self.assert_health_state(HealthState.UNKNOWN)
        self.online()
        self.on()

        self.configure_scan(csp_configure_scan_request)
        self.assert_obs_mode(ObsMode.PULSAR_TIMING)
        self.assert_pst_processing_mode(PstProcessingMode.VOLTAGE_RECORDER)

        self.scan(scan_request)

        self.abort()
        self.assert_obs_state(ObsState.ABORTED)

        self.reset()
        self.assert_obs_state(ObsState.IDLE)
        self.assert_obs_mode(ObsMode.IDLE)
        self.assert_health_state(HealthState.OK)
        self.assert_pst_processing_mode(PstProcessingMode.IDLE)

        self.off()
        self.offline()

    def test_beam_mgmt_reset_from_scanning(
        self: TestPstBeam,
        csp_configure_scan_request: dict,
        scan_request: dict,
    ) -> None:
        """Test PstBeam can reset from scanning."""
        self.assert_health_state(HealthState.UNKNOWN)
        self.online()
        self.on()

        self.configure_scan(csp_configure_scan_request)
        self.assert_pst_processing_mode(PstProcessingMode.VOLTAGE_RECORDER)
        self.assert_obs_mode(ObsMode.PULSAR_TIMING)

        self.scan(scan_request)
        assert self.beam_proxy.scanid == int(scan_request["scan_id"])

        self.reset()
        self.assert_obs_state(ObsState.IDLE)
        self.assert_obs_mode(ObsMode.IDLE)
        self.assert_health_state(HealthState.OK)
        self.assert_pst_processing_mode(PstProcessingMode.IDLE)
        assert self.beam_proxy.scanid == 0

        self.off()
        self.offline()

    def test_beam_mgmt_reset_from_configure_scan(
        self: TestPstBeam,
        csp_configure_scan_request: dict,
        scan_request: dict,
    ) -> None:
        """Test PstBeam can reset from scan configured."""
        self.assert_health_state(HealthState.UNKNOWN)
        self.online()
        self.on()

        self.configure_scan(csp_configure_scan_request)
        self.assert_obs_mode(ObsMode.PULSAR_TIMING)

        self.reset()
        self.assert_obs_state(ObsState.IDLE)
        self.assert_obs_mode(ObsMode.IDLE)
        self.assert_health_state(HealthState.OK)
        self.assert_pst_processing_mode(PstProcessingMode.IDLE)

        self.off()
        self.offline()

    def test_beam_mgmt_go_to_fault_then_obs_reset(
        self: TestPstBeam,
        csp_configure_scan_request: dict,
    ) -> None:
        """Test state model of PstBeam and go to Fault state and then an ObsReset."""
        self.assert_health_state(HealthState.UNKNOWN)
        self.online()
        self.on()

        self.configure_scan(csp_configure_scan_request)
        self.assert_obs_mode(ObsMode.PULSAR_TIMING)

        self.goto_fault("this is a fault message")
        self.assert_obs_state(ObsState.FAULT)

        self.obs_reset()
        self.assert_obs_state(ObsState.IDLE)
        self.assert_obs_mode(ObsMode.IDLE)

        self.off()
        self.offline()

    def test_beam_mgmt_go_to_fault_then_reset(
        self: TestPstBeam,
        csp_configure_scan_request: dict,
    ) -> None:
        """Test state model of PstBeam and go to Fault then perform a Reset."""
        self.assert_health_state(HealthState.UNKNOWN)
        self.online()
        self.on()

        self.configure_scan(csp_configure_scan_request)
        self.assert_obs_mode(ObsMode.PULSAR_TIMING)

        self.goto_fault("this is a fault message")
        self.assert_obs_state(ObsState.FAULT)

        self.reset()
        self.assert_obs_state(ObsState.IDLE)
        self.assert_obs_mode(ObsMode.IDLE)

        self.off()
        self.offline()

    @pytest.mark.parametrize("patchable_component_manager", [True])
    def test_beam_mgmt_test_alarm(
        self: TestPstBeam,
        component_manager_provider: Callable[..., PstBeamComponentManager],
        tango_attribute_alarm_configs: List[_TangoAttributeAlarmConfig],
        monkeypatch: pytest.MonkeyPatch,
        mock_recv_component_manager: MagicMock,
        mock_smrb_component_manager: MagicMock,
        mock_stat_component_manager: MagicMock,
        mock_dsp_component_manager: MagicMock,
    ) -> None:
        """Test warning and alarm levels for TANGO attributes."""
        cm = component_manager_provider()

        monkeypatch.setattr(cm, "_recv_subcomponent", mock_recv_component_manager)
        monkeypatch.setattr(cm, "_smrb_subcomponent", mock_smrb_component_manager)
        monkeypatch.setattr(cm, "_stat_subcomponent", mock_stat_component_manager)
        monkeypatch.setattr(cm, "_dsp_subcomponent", mock_dsp_component_manager)

        for alarm_config in tango_attribute_alarm_configs:
            setattr(alarm_config.source, alarm_config.cm_attr, alarm_config.default)

        self.assert_health_state(HealthState.UNKNOWN)
        self.online()
        self.on()

        self.assert_state(DevState.ON)

        for alarm_config in tango_attribute_alarm_configs:
            source = alarm_config.source

            if alarm_config.warning is not None:
                setattr(source, alarm_config.cm_attr, alarm_config.warning)
                cm._update_device_attribute(alarm_config.cm_attr, alarm_config.warning)
                self.assert_state(DevState.ALARM)
                device_attr = self.beam_proxy.read_attribute(alarm_config.tango_attr)
                assert device_attr.quality == tango.AttrQuality.ATTR_WARNING

                setattr(source, alarm_config.cm_attr, alarm_config.default)
                cm._update_device_attribute(alarm_config.cm_attr, alarm_config.default)
                self.assert_state(DevState.ON)
                device_attr = self.beam_proxy.read_attribute(alarm_config.tango_attr)
                assert device_attr.quality == tango.AttrQuality.ATTR_VALID

            if alarm_config.alarm is not None:
                setattr(source, alarm_config.cm_attr, alarm_config.alarm)
                cm._update_device_attribute(alarm_config.cm_attr, alarm_config.alarm)
                self.assert_state(DevState.ALARM)
                device_attr = self.beam_proxy.read_attribute(alarm_config.tango_attr)
                assert device_attr.quality == tango.AttrQuality.ATTR_ALARM

                setattr(source, alarm_config.cm_attr, alarm_config.default)
                cm._update_device_attribute(alarm_config.cm_attr, alarm_config.default)
                self.assert_state(DevState.ON)
                device_attr = self.beam_proxy.read_attribute(alarm_config.tango_attr)
                assert device_attr.quality == tango.AttrQuality.ATTR_VALID

        self.off()

    @pytest.mark.parametrize("patchable_component_manager", [True])
    def test_beam_mgmt_subcomponent_state_attributes(
        self: TestPstBeam,
        component_manager_provider: Callable[..., PstBeamComponentManager],
        tango_attribute_alarm_configs: List[_TangoAttributeAlarmConfig],
        monkeypatch: pytest.MonkeyPatch,
        mock_recv_component_manager: MagicMock,
        mock_smrb_component_manager: MagicMock,
        mock_stat_component_manager: MagicMock,
        mock_dsp_component_manager: MagicMock,
    ) -> None:
        """Test getting values of the subcomponent attributes of obs_state and health_state."""
        cm = component_manager_provider()

        monkeypatch.setattr(cm, "_recv_subcomponent", mock_recv_component_manager)
        monkeypatch.setattr(cm, "_smrb_subcomponent", mock_smrb_component_manager)
        monkeypatch.setattr(cm, "_stat_subcomponent", mock_stat_component_manager)
        monkeypatch.setattr(cm, "_dsp_subcomponent", mock_dsp_component_manager)

        for alarm_config in tango_attribute_alarm_configs:
            setattr(alarm_config.source, alarm_config.cm_attr, alarm_config.default)

        self.assert_health_state(HealthState.UNKNOWN)
        self.online()
        self.on()

        self.assert_state(DevState.ON)
        value: Any

        def _set_scm_attribute(subcomponent_name: str, attribute_name: str, value: Any) -> None:
            if subcomponent_name.startswith("dsp"):
                scm = mock_dsp_component_manager
                setattr(scm, f"{subcomponent_name}_{attribute_name}", value)
            else:
                scm = getattr(cm, f"_{subcomponent_name}_subcomponent")
                setattr(scm, attribute_name, value)

        for subcomponent_name in ["recv", "smrb", "stat", "dsp_disk", "dsp_flow_through"]:
            for attribute_name in ["obs_state", "health_state"]:
                tango_attr_name = as_device_attribute_name(f"{subcomponent_name}_{attribute_name}")
                if attribute_name == "obs_state":
                    value = random.choice(list(ObsState))
                else:
                    value = random.choice(list(HealthState))
                _set_scm_attribute(subcomponent_name, attribute_name, value)

                device_attr = self.beam_proxy.read_attribute(tango_attr_name)
                assert (
                    device_attr.value == value
                ), f"expected {tango_attr_name} from {subcomponent_name}_{attribute_name} to be {value.name}"

    @pytest.mark.parametrize("patchable_component_manager", [True])
    def test_beam_mgmt_setting_getting_health_check_interval(
        self: TestPstBeam,
        component_manager_provider: Callable[..., PstBeamComponentManager],
        tango_attribute_alarm_configs: List[_TangoAttributeAlarmConfig],
        monkeypatch: pytest.MonkeyPatch,
        mock_recv_component_manager: MagicMock,
        mock_smrb_component_manager: MagicMock,
        mock_stat_component_manager: MagicMock,
        mock_dsp_component_manager: MagicMock,
    ) -> None:
        """Test the setting ang getting of the health check interval."""
        cm = component_manager_provider()

        monkeypatch.setattr(cm, "_recv_subcomponent", mock_recv_component_manager)
        monkeypatch.setattr(cm, "_smrb_subcomponent", mock_smrb_component_manager)
        monkeypatch.setattr(cm, "_stat_subcomponent", mock_stat_component_manager)
        monkeypatch.setattr(cm, "_dsp_subcomponent", mock_dsp_component_manager)
        monkeypatch.setattr(
            cm,
            "_subcomponents",
            [
                mock_recv_component_manager,
                mock_smrb_component_manager,
                mock_stat_component_manager,
                mock_dsp_component_manager,
            ],
        )

        for alarm_config in tango_attribute_alarm_configs:
            setattr(alarm_config.source, alarm_config.cm_attr, alarm_config.default)

        self.assert_health_state(HealthState.UNKNOWN)
        self.online()
        self.on()

        self.assert_state(DevState.ON)

        def _set_scm_attribute(subcomponent_name: str, attribute_name: str, value: Any) -> None:
            if subcomponent_name.startswith("dsp"):
                scm = mock_dsp_component_manager
                setattr(scm, f"{subcomponent_name}_{attribute_name}", value)
            else:
                scm = getattr(cm, f"_{subcomponent_name}_subcomponent")
                setattr(scm, attribute_name, value)

        # check this equals default value
        device_attr = self.beam_proxy.read_attribute("healthCheckInterval")
        assert device_attr.value == 1000, "expected healthCheckInterval to be 1000"
        assert (
            cm._health_check_interval == 1000
        ), "expected health_check_interval on component manager to be 1000"

        health_check_interval = random.randint(500, 5000)
        self.beam_proxy.write_attribute("healthCheckInterval", health_check_interval)

        device_attr = self.beam_proxy.read_attribute("healthCheckInterval")
        assert (
            device_attr.value == health_check_interval
        ), f"expected healthCheckInterval to have been updated to {health_check_interval}"
        assert cm._health_check_interval == health_check_interval, (
            f"expected health_check_interval on component manager to have been updated "
            f"to {health_check_interval}"
        )
        assert mock_recv_component_manager.health_check_interval == health_check_interval
        cast(MagicMock, mock_recv_component_manager.restart_health_check).assert_called_once()
        assert mock_smrb_component_manager.health_check_interval == health_check_interval
        cast(MagicMock, mock_smrb_component_manager.restart_health_check).assert_called_once()
        assert mock_stat_component_manager.health_check_interval == health_check_interval
        cast(MagicMock, mock_stat_component_manager.restart_health_check).assert_called_once()
        assert mock_dsp_component_manager.health_check_interval == health_check_interval
        cast(MagicMock, mock_dsp_component_manager.restart_health_check).assert_called_once()
