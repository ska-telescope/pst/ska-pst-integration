# -*- coding: utf-8 -*-
#
# This file is part of the SKA PST project
#
# Distributed under the terms of the BSD 3-clause new license.
# See LICENSE for more info.
"""This module tests the PST obs state model state transitions."""

import logging
from unittest.mock import MagicMock

import pytest
from ska_control_model import ObsState
from ska_pst.lmc.component import PstObsStateModel


@pytest.fixture
def mock_update_obs_state() -> MagicMock:
    """Get a mocked callback for testing obs state being updated."""
    return MagicMock()


@pytest.fixture
def obs_state_machine(
    initial_internal_state: str,
    mock_update_obs_state: MagicMock,
    logger: logging.Logger,
) -> PstObsStateModel:
    """Get a state machine with the given initial state."""
    return PstObsStateModel(
        logger=logger, initial_state=initial_internal_state, callback=mock_update_obs_state
    )


@pytest.mark.parametrize(
    "initial_obs_state,initial_internal_state,expected_internal_state,action,expected_obs_state",
    [
        # configure assertions
        (ObsState.IDLE, "IDLE", "CONFIGURING_IDLE", "configure_invoked", ObsState.CONFIGURING),
        (
            ObsState.CONFIGURING,
            "CONFIGURING_IDLE",
            "CONFIGURING_READY",
            "component_configured",
            ObsState.CONFIGURING,
        ),
        (ObsState.CONFIGURING, "CONFIGURING_IDLE", "IDLE", "configure_completed", ObsState.IDLE),
        (ObsState.CONFIGURING, "CONFIGURING_READY", "READY", "configure_completed", ObsState.READY),
        (ObsState.READY, "READY", "READY", "scan_invoked", ObsState.READY),
        (ObsState.READY, "READY", "SCANNING", "component_scanning", ObsState.SCANNING),
        (ObsState.READY, "READY", "CONFIGURING_READY", "configure_invoked", ObsState.CONFIGURING),
        (ObsState.SCANNING, "SCANNING", "SCANNING", "end_scan_invoked", ObsState.SCANNING),
        (ObsState.SCANNING, "SCANNING", "READY", "component_not_scanning", ObsState.READY),
        # assertions of abort/aborting
        (ObsState.IDLE, "IDLE", "ABORTING", "abort_invoked", ObsState.ABORTING),
        (ObsState.CONFIGURING, "CONFIGURING_IDLE", "ABORTING", "abort_invoked", ObsState.ABORTING),
        (ObsState.CONFIGURING, "CONFIGURING_READY", "ABORTING", "abort_invoked", ObsState.ABORTING),
        (ObsState.READY, "READY", "ABORTING", "abort_invoked", ObsState.ABORTING),
        (ObsState.RESETTING, "RESETTING", "ABORTING", "abort_invoked", ObsState.ABORTING),
        (ObsState.ABORTING, "ABORTING", "ABORTED", "abort_completed", ObsState.ABORTED),
        (ObsState.ABORTING, "ABORTING", "ABORTING", "component_configured", ObsState.ABORTING),
        (ObsState.ABORTING, "ABORTING", "ABORTING", "component_unconfigured", ObsState.ABORTING),
        (ObsState.ABORTING, "ABORTING", "ABORTING", "component_scanning", ObsState.ABORTING),
        (ObsState.ABORTING, "ABORTING", "ABORTING", "component_not_scanning", ObsState.ABORTING),
        # assertions of fault
        (ObsState.IDLE, "IDLE", "FAULT", "component_obsfault", ObsState.FAULT),
        (ObsState.CONFIGURING, "CONFIGURING_IDLE", "FAULT", "component_obsfault", ObsState.FAULT),
        (ObsState.CONFIGURING, "CONFIGURING_READY", "FAULT", "component_obsfault", ObsState.FAULT),
        (ObsState.READY, "READY", "FAULT", "component_obsfault", ObsState.FAULT),
        (ObsState.SCANNING, "SCANNING", "FAULT", "component_obsfault", ObsState.FAULT),
        (ObsState.ABORTING, "ABORTING", "FAULT", "component_obsfault", ObsState.FAULT),
        (ObsState.ABORTED, "ABORTED", "FAULT", "component_obsfault", ObsState.FAULT),
        (ObsState.RESETTING, "RESETTING", "FAULT", "component_obsfault", ObsState.FAULT),
        # handle of obs reset
        (ObsState.ABORTED, "ABORTED", "RESETTING", "obsreset_invoked", ObsState.RESETTING),
        (ObsState.FAULT, "FAULT", "RESETTING", "obsreset_invoked", ObsState.RESETTING),
        (ObsState.RESETTING, "RESETTING", "IDLE", "obsreset_completed", ObsState.IDLE),
        # assertions of reset/resetting
        (ObsState.IDLE, "IDLE", "RESETTING", "reset_invoked", ObsState.RESETTING),
        (ObsState.CONFIGURING, "CONFIGURING_IDLE", "RESETTING", "reset_invoked", ObsState.RESETTING),
        (ObsState.CONFIGURING, "CONFIGURING_READY", "RESETTING", "reset_invoked", ObsState.RESETTING),
        (ObsState.READY, "READY", "RESETTING", "reset_invoked", ObsState.RESETTING),
        (ObsState.SCANNING, "SCANNING", "RESETTING", "reset_invoked", ObsState.RESETTING),
        (ObsState.ABORTING, "ABORTING", "RESETTING", "reset_invoked", ObsState.RESETTING),
        (ObsState.ABORTED, "ABORTED", "RESETTING", "reset_invoked", ObsState.RESETTING),
        (ObsState.RESETTING, "RESETTING", "RESETTING", "reset_invoked", ObsState.RESETTING),
        (ObsState.RESETTING, "RESETTING", "RESETTING", "component_unconfigured", ObsState.RESETTING),
        (ObsState.RESETTING, "RESETTING", "RESETTING", "component_not_scanning", ObsState.RESETTING),
        (ObsState.RESETTING, "RESETTING", "IDLE", "reset_completed", ObsState.IDLE),
    ],
)
def test_valid_state_transition(
    obs_state_machine: PstObsStateModel,
    action: str,
    initial_obs_state: ObsState,
    expected_obs_state: ObsState,
    mock_update_obs_state: MagicMock,
    initial_internal_state: str,
    expected_internal_state: str,
) -> None:
    """Test valid state transitions of the PST obs state machine."""
    obs_state_machine_model = obs_state_machine._obs_state_machine.model

    # assert the state machine is in the correct initial state
    assert (
        obs_state_machine._obs_state_machine.get_model_state(obs_state_machine_model).name
        == initial_internal_state
    )
    mock_update_obs_state.assert_called_with(initial_obs_state)
    mock_update_obs_state.reset_mock()

    # perform an action on the state machine
    obs_state_machine.perform_action(action)

    # assert updates
    assert (
        obs_state_machine._obs_state_machine.get_model_state(obs_state_machine_model).name
        == expected_internal_state
    )
    if initial_obs_state == expected_obs_state:
        # the SKA base machinery means there would be no update called
        mock_update_obs_state.assert_not_called()
    else:
        # this is where the TANGO device obs state would be updated
        mock_update_obs_state.assert_called_once_with(expected_obs_state)
