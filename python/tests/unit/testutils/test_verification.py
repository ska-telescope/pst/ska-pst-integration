# -*- coding: utf-8 -*-
#
# This file is part of the SKA PST project
#
# Distributed under the terms of the BSD 3-clause new license.
# See LICENSE for more info.

"""This module contains the pytest tests for verification."""

from __future__ import annotations

import inspect
import json
import logging
import pathlib
from typing import Any

import pandas as pd
import pytest
import yaml
from ska_pst.testutils.dada import DadaFileReader
from ska_pst.testutils.scan_config import ScanIdFactory
from ska_pst.testutils.verification import DADA_HEADER_CONVERTER_MAPPING, MetadataVerifier, ValueMapping


@pytest.fixture(scope="session")
def verification_data_path() -> pathlib.Path:
    """Get the path to the verification data."""
    return pathlib.Path(__file__).parent / "verification_data"


@pytest.fixture
def dpd_metadata(verification_data_path: pathlib.Path) -> dict:
    """Get the metadata as dictionary."""
    metadata_file = verification_data_path / "ska-data-product.yaml"
    with open(metadata_file, "r") as f:
        return yaml.safe_load(f)


@pytest.fixture
def scan_id(dpd_metadata: dict) -> int:
    """Get the scan ID for testing."""
    return int(dpd_metadata["obscore"]["obs_id"])


@pytest.fixture
def scan_configuration_path(verification_data_path: pathlib.Path) -> pathlib.Path:
    """Get the path location to the scan configuration JSON file."""
    return verification_data_path / "low_scan_configuration.json"


@pytest.fixture
def scan_config_overrides() -> dict:
    """Get scan config overrides."""
    return {}


@pytest.fixture
def scan_configuration(scan_config_overrides: dict, scan_configuration_path: pathlib.Path) -> dict:
    """Get scan configuration."""
    with open(scan_configuration_path, "r") as f:
        config = json.load(f)

    return {
        **config,
        **scan_config_overrides,
    }


def _get_value(data: dict, key: str) -> Any | None:
    try:
        value = data
        for k in key.split("/"):
            value = value[k]
    except KeyError:
        value = None

    return value


def _assert_file_value_mapping(
    file: DadaFileReader, header_key: str, df_key: str, is_weights: bool, df: pd.DataFrame
) -> None:
    file_number = file.file_number
    if is_weights:
        df_col_name = f"Weights File {file_number}"
    else:
        df_col_name = f"Data File {file_number}"

    value = _get_value(file.header, header_key)
    value_mapper = DADA_HEADER_CONVERTER_MAPPING.get(header_key, str)
    if value is not None:
        value = value_mapper(value)
        assert value == df.loc[df_key][df_col_name]
    else:
        assert df.loc[df_key][df_col_name] == ""


def test_metadata_verifier_happy_path(
    scan_configuration: dict,
    dpd_metadata: dict,
    verification_data_path: pathlib.Path,
    scan_id: int,
    logger: logging.Logger,
) -> None:
    """Test the happy path for verification of data."""
    verifier = MetadataVerifier(
        scan_configuration=scan_configuration,
        scan_id=scan_id,
        scan_path=verification_data_path,
        logger=logger,
    )

    scan_configuration = {
        **scan_configuration["common"],
        **scan_configuration["pst"]["scan"],
        "scan_id": scan_id,
    }

    verifier.verify()

    df = verifier.dataframe
    df = df.set_index("Keys")

    mapping_file = pathlib.Path(inspect.getfile(MetadataVerifier)).parent / "mapping.yaml"

    with open(mapping_file, "r") as f:
        mappings = {k: ValueMapping(**v) for k, v in yaml.safe_load(f).items()}

    for key, mapping in mappings.items():
        if mapping.config_key:
            value = _get_value(scan_configuration, mapping.config_key)
            if value:
                assert value == df.loc[key]["Scan Configuration"]
            else:
                assert df.loc[key]["Scan Configuration"] == ""

        if mapping.metadata_key:
            value = _get_value(dpd_metadata, mapping.metadata_key)
            if value:
                assert value == df.loc[key]["SDP DPD Metadata"]
            else:
                assert df.loc[key]["SDP DPD Metadata"] == ""

        if mapping.file_key:
            for file in verifier._metadata.data_files:
                _assert_file_value_mapping(
                    file=file,
                    header_key=mapping.file_key,
                    df_key=key,
                    is_weights=False,
                    df=df,
                )

            for file in verifier._metadata.weights_files:
                _assert_file_value_mapping(
                    file=file,
                    header_key=mapping.file_key,
                    df_key=key,
                    is_weights=True,
                    df=df,
                )


def test_metadata_verifier_invalid_scan_id(
    scan_configuration: dict,
    verification_data_path: pathlib.Path,
    scan_id_factory: ScanIdFactory,
    scan_id: int,
    logger: logging.Logger,
) -> None:
    """Test the happy path for verification of data."""
    bad_scan_id = scan_id_factory.generate_scan_id()
    while bad_scan_id == scan_id:
        bad_scan_id = scan_id_factory.generate_scan_id()

    verifier = MetadataVerifier(
        scan_configuration=scan_configuration,
        scan_id=bad_scan_id,
        scan_path=verification_data_path,
        logger=logger,
    )

    with pytest.raises(AssertionError) as exc_info:
        verifier.verify()

    exc = exc_info.value
    expected_error_msg = f"expected value of SCAN_ID to be {bad_scan_id} but was {scan_id}"
    assert str(exc).startswith(expected_error_msg)


@pytest.mark.parametrize(
    "header_key, header_value",
    [
        ("UDP_FORMAT", "invalid_format"),
        ("NSUBBAND", "2"),
        ("COORD_MD", "J2024"),
        ("TRK_MODE", "NO_TRACK"),
        ("START_CHANNEL", "1"),
        ("END_CHANNEL", "2"),
        ("START_CHANNEL_OUT", "1"),
        ("END_CHANNEL_OUT", "2"),
        ("NDIM", "4"),
        ("NPOL", "3"),
        ("NBIT", "64"),
        ("TSAMP", "1234.56"),
        ("BYTES_PER_SECOND", "1138.0"),
        ("RESOLUTION", "42"),
        ("OBS_OFFSET", "1233456789"),
        ("FILE_NUMBER", "3"),
        ("NANT", "10"),
        ("SCAN_ID", "42"),
        ("UDP_NSAMP", "49"),
        ("WT_NSAMP", "51"),
        ("UDP_NCHAN", "99"),
        ("FD_HAND", "1234"),
        ("FD_SANG", "3.14"),
        ("FA_REQ", "123.456"),
        ("NANT", "96"),
        ("ANTENNAE", "first_wrong_receptor,second_wrong_receptor"),
        ("ANT_WEIGHTS", "0.2,0.3,0.5"),
        ("NPOL", "3"),
        ("NBIT", "11"),
        ("OS_FACTOR", "22/7"),
        ("ITRF", "1.2,3.4,5.6"),
        ("SCANLEN_MAX", "96"),
        ("BW", "98.7"),
        ("NCHAN", "0"),
        ("END_CHANNEL", "348"),
        ("FREQ", "54321.0"),
        ("END_CHANNEL_OUT", "543"),
        ("BW_OUT", "346.2"),
        ("TSAMP", "512.42"),
    ],
)
def test_metadata_when_invalid_value_in_header(
    scan_configuration: dict,
    verification_data_path: pathlib.Path,
    scan_id: int,
    header_key: str,
    header_value: str,
    logger: logging.Logger,
) -> None:
    """Assert that when an invalid is in a header that the verifier raises error."""

    def _patch_header(file: DadaFileReader) -> None:
        logger.debug(f"Patching {file}.header[{header_key}] to be {header_value}")
        file._read_header()
        file._header[header_key] = header_value

    verifier = MetadataVerifier(
        scan_configuration=scan_configuration,
        scan_id=scan_id,
        scan_path=verification_data_path,
        logger=logger,
    )

    for f in verifier._metadata.data_files:
        _patch_header(f)

    with pytest.raises(AssertionError):
        verifier.verify()
