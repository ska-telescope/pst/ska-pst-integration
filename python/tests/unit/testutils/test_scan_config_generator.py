# -*- coding: utf-8 -*-
#
# This file is part of the SKA PST project
#
# Distributed under the terms of the BSD 3-clause new license.
# See LICENSE for more info.

"""This module contains the pytest tests for the Scan Config Generator."""

from __future__ import annotations

import pytest
from ska_pst.testutils.scan_config import create_default_scan_config_generator, generate_rf_config

from ska_pst.common import CbfPstConfig, round_


@pytest.mark.parametrize(
    "telescope,frequency_band",
    [
        ("SKALow", "low"),
        ("SKAMid", "1"),
        ("SKAMid", "2"),
    ],
)
def test_constructing_default_scan_config_generator(
    frequency_band: str, cbf_pst_config: CbfPstConfig
) -> None:
    """Test the default scan config generator generates valid scan configuration."""
    beam_id = 1
    rf_minimum_freq_mhz = cbf_pst_config.rf_freq_minimum_mhz
    rf_maximum_freq_mhz = cbf_pst_config.rf_freq_maximum_mhz

    generator = create_default_scan_config_generator(beam_id, frequency_band)

    # the scan config generator randomizes the channel choice, so exercise a range of tests
    for i in range(100):
        scan_config = generator.generate()
        nchan = scan_config["pst"]["scan"]["num_frequency_channels"]
        cfreq_mhz = scan_config["pst"]["scan"]["centre_frequency"] / 1_000_000
        bw_mhz = scan_config["pst"]["scan"]["total_bandwidth"] / 1_000_000

        minimum_freq_mhz = round_(cfreq_mhz - bw_mhz / 2, 8)
        maximum_freq_mhz = round_(cfreq_mhz + bw_mhz / 2, 8)

        assert (
            nchan % cbf_pst_config.udp_nchan == 0
        ), f"{nchan=} % {cbf_pst_config.udp_nchan=} != 0, {scan_config=}"
        assert (
            minimum_freq_mhz >= rf_minimum_freq_mhz
        ), f"{minimum_freq_mhz} < {rf_minimum_freq_mhz}, {scan_config=}"
        assert (
            maximum_freq_mhz <= rf_maximum_freq_mhz
        ), f"{maximum_freq_mhz=} > {rf_maximum_freq_mhz=} {scan_config=}"


@pytest.mark.parametrize(
    "telescope, frequency_band, max_data_rate",
    [
        ("SKALow", "low", 2.0**30),
        ("SKAMid", "1", 2.0**30),
        ("SKAMid", "2", 2.0**30),
        ("SKALow", "low", None),
        ("SKAMid", "1", None),
        ("SKAMid", "2", None),
    ],
)
def test_generate_rf_config(
    telescope: str, frequency_band: str, cbf_pst_config: CbfPstConfig, max_data_rate: float | None
) -> None:
    """Test that the RfConfig generator produces valid configuration."""
    rf_minimum_freq_mhz = cbf_pst_config.rf_freq_minimum_mhz
    rf_maximum_freq_mhz = cbf_pst_config.rf_freq_maximum_mhz
    min_nchan = cbf_pst_config.udp_nchan

    # the RF generator randomizes the channel choice, so exercise a range of tests
    for i in range(100):
        rf_config = generate_rf_config(cbf_pst_config, max_data_rate, min_nchan)
        minimum_freq_mhz = round_(rf_config.centre_frequency_mhz - rf_config.bandwidth_mhz / 2, 8)
        maximum_freq_mhz = round_(rf_config.centre_frequency_mhz + rf_config.bandwidth_mhz / 2, 8)
        offset_start_chan = rf_config.start_chan - cbf_pst_config.min_valid_chan

        assert (
            minimum_freq_mhz >= rf_minimum_freq_mhz
        ), f"{minimum_freq_mhz=} < {rf_minimum_freq_mhz=}, {rf_config=} "
        assert (
            maximum_freq_mhz <= rf_maximum_freq_mhz
        ), f"{maximum_freq_mhz=} > {rf_maximum_freq_mhz=}, {rf_config=}"
        assert (
            rf_config.nchan % cbf_pst_config.udp_nchan == 0
        ), f"{rf_config.nchan=} % {cbf_pst_config.udp_nchan=} != 0, {rf_config=}"
        assert (
            rf_config.start_chan >= cbf_pst_config.min_valid_chan
        ), f"{rf_config.start_chan=} < {cbf_pst_config.min_valid_chan=}, {rf_config=}"
        assert (
            offset_start_chan % cbf_pst_config.udp_nchan == 0
        ), f"{offset_start_chan=} % {cbf_pst_config.udp_nchan=} != 0, {rf_config=}"
