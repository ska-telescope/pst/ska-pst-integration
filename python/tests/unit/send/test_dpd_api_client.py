# -*- coding: utf-8 -*-
#
# This file is part of the SKA PST SEND project
#
# Distributed under the terms of the BSD 3-clause new license.
# See LICENSE for more info.

"""This module contains the pytest tests for the DpdApiClient."""

import pathlib
from unittest.mock import MagicMock, patch

import pytest
import yaml
from ska_pst.send.dpd_api_client import DpdApiClient


@pytest.fixture
def dpd_api_endpoint() -> str:
    """Fixture for DPD endpoint."""
    return "http://example.com:8080"


@pytest.fixture
def dpd_api_client(dpd_api_endpoint: str) -> DpdApiClient:
    """Fixture for DpdApiClient."""
    return DpdApiClient(dpd_api_endpoint)


@patch("ska_pst.send.dpd_api_client.requests.post")
def test_data_product_metadata(
    mock_post: MagicMock, dpd_api_client: DpdApiClient, metadata_uuid: str
) -> None:
    """Test that API calls dataproductmetadata endpoint to add metadata."""
    metadata = {"cat": "dog"}

    mock_response = MagicMock(status_code=200)
    mock_response.json.return_value = metadata
    mock_post.return_value = mock_response

    response = dpd_api_client.data_product_metadata(uuid=metadata_uuid)

    assert response == metadata, "expected data_product_metadata to return a metadata dictionary"
    mock_post.assert_called_once_with(
        f"{dpd_api_client.endpoint}/dataproductmetadata",
        headers={"accept": "application/json"},
        json={"uuid": metadata_uuid},
    )


@patch("ska_pst.send.dpd_api_client.requests.post")
def test_data_product_metadata_raises_error(
    mock_post: MagicMock, dpd_api_client: DpdApiClient, metadata_uuid: str
) -> None:
    """Test error handling when error for dataproductmetadata occurs."""
    mock_post.return_value = MagicMock(status_code=404, ok=False)

    with pytest.raises(Exception) as exc_info:
        dpd_api_client.data_product_metadata(uuid=metadata_uuid)

    mock_post.assert_called_once_with(
        f"{dpd_api_client.endpoint}/dataproductmetadata",
        headers={"accept": "application/json"},
        json={"uuid": metadata_uuid},
    )
    assert (
        str(exc_info.value)
        == f"Failed to retrieve DPD associated with uuid='{metadata_uuid}'. Status code: 404"
    )


@patch("ska_pst.send.dpd_api_client.requests.post")
def test_ingest_new_metadata(
    mock_post: MagicMock, dpd_api_client: DpdApiClient, metadata_uuid: str, send_tempdir: pathlib.Path
) -> None:
    """Test that API calls ingestnewmetadata endpoint to get metadata by UUID."""
    mock_response = MagicMock(status_code=200)
    mock_response.json.return_value = [
        {
            "status": "success",
            "message": "New data product metadata received and search store index updated",
            "uuid": metadata_uuid,
        },
        201,
    ]
    mock_post.return_value = mock_response

    metadata = {"foo": "bar"}
    metadata_file = send_tempdir / f"{metadata_uuid}.yaml"
    with open(metadata_file, "w") as f:
        yaml.safe_dump(metadata, f)

    response = dpd_api_client.ingest_new_metadata(metadata_file=metadata_file)

    try:
        assert response == metadata_uuid, "expected ingest_new_metadata to return a UUID"
        mock_post.assert_called_once_with(
            f"{dpd_api_client.endpoint}/ingestnewmetadata",
            headers={"accept": "application/json"},
            json=metadata,
        )
    finally:
        metadata_file.unlink(missing_ok=True)


@patch("ska_pst.send.dpd_api_client.requests.post")
def test_ingest_new_metadata_raises_error(
    mock_post: MagicMock, dpd_api_client: DpdApiClient, metadata_uuid: str, send_tempdir: pathlib.Path
) -> None:
    """Test error handling when error for ingestnewmetadata occurs."""
    mock_response = MagicMock(status_code=500, ok=False)
    mock_response.json.return_value = {"oops": "Something went wrong"}
    mock_post.return_value = mock_response

    metadata = {"foo": "bar"}
    metadata_file = send_tempdir / f"{metadata_uuid}.yaml"
    with open(metadata_file, "w") as f:
        yaml.safe_dump(metadata, f)

    try:
        with pytest.raises(Exception) as exc_info:
            dpd_api_client.ingest_new_metadata(metadata_file=metadata_file)

        mock_post.assert_called_once_with(
            f"{dpd_api_client.endpoint}/ingestnewmetadata",
            headers={"accept": "application/json"},
            json=metadata,
        )

        assert str(exc_info.value) == (
            "DPD failed to add dataproduct metadata. Status code: 500, "
            "response_body={'oops': 'Something went wrong'}"
        )
    finally:
        metadata_file.unlink(missing_ok=True)


def test_set_endpoint(dpd_api_client: DpdApiClient) -> None:
    """Test the setter method for the endpoint property."""
    # Set a new endpoint
    dpd_api_client.endpoint = "http://new-example.com:8081"

    # Check that the endpoint has been updated correctly
    assert dpd_api_client.endpoint == "http://new-example.com:8081"
