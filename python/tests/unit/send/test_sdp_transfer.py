# -*- coding: utf-8 -*-
#
# This file is part of the SKA PST SEND project
#
# Distributed under the terms of the BSD 3-clause new license.
# See LICENSE for more info.

"""This module contains the pytest tests for the ScanManager."""
from __future__ import annotations

import logging
import os
import pathlib
import subprocess
import threading
import time
from typing import Any, List, Tuple, cast
from unittest.mock import MagicMock, patch  # Import the necessary modules

import pytest
from ska_pst.send import ScanProcess, ScanTransfer, SdpTransfer, VoltageRecorderScan
from ska_pst.send.sdp_transfer import check_path_permissions


@pytest.fixture
def mock_logger() -> MagicMock:
    """Fixture for mocking the logger."""
    return MagicMock(spec=logging.Logger)


@pytest.mark.timeout(30, method="signal")
def test_sdp_transfer_process(
    local_remote_scans: Tuple[VoltageRecorderScan, VoltageRecorderScan],
    subsystem_id: str,
    monkeypatch: pytest.MonkeyPatch,
) -> None:
    """Test the process method of SdpTransfer."""
    (local_scan, remote_scan) = local_remote_scans

    local_path = local_scan.data_product_path
    remote_path = remote_scan.data_product_path
    data_product_dashboard = "disabled"
    verbose = False

    sdp_transfer = SdpTransfer(local_path, remote_path, subsystem_id, data_product_dashboard, verbose)

    def _process_side_effect(*args: Any, **kwargs: Any) -> MagicMock:
        # ensure the file is created
        for sf in local_scan._stats_files:
            (local_scan.full_scan_path / sf.data_product_path).touch()

        completed = MagicMock()
        completed.returncode = 0
        return completed

    mocked_cmd = MagicMock(side_effect=_process_side_effect)
    monkeypatch.setattr(subprocess, "run", mocked_cmd)

    local_scan._scan_completed_file.touch()
    local_scan._data_product_file.touch()

    def _interrupt_processing() -> None:
        # """Sleep for a timeout, then interrupt SdpTransfer processing."""
        time.sleep(0.5)
        sdp_transfer.interrupt_processing()

    notify_thread = threading.Thread(target=_interrupt_processing, daemon=True)
    notify_thread.start()
    sdp_transfer.process()
    notify_thread.join()


@pytest.mark.timeout(30, method="signal")
def test_metadata_exists_called_with_correct_search_value(
    subsystem_id: str,
    monkeypatch: pytest.MonkeyPatch,
    local_remote_scans: Tuple[VoltageRecorderScan, VoltageRecorderScan],
    logger: logging.Logger,
    metadata_uuid: str,
) -> None:
    """Test metadata exists."""
    (local_scan, remote_scan) = local_remote_scans

    local_path = local_scan.data_product_path
    remote_path = remote_scan.data_product_path
    data_product_dashboard = "http://localhost:8888"
    verbose = False
    sdp_transfer = SdpTransfer(local_path, remote_path, subsystem_id, data_product_dashboard, verbose)
    api_client = MagicMock()
    sdp_transfer._dpd_api_client = api_client
    api_client.ingest_new_metadata.return_value = metadata_uuid
    api_client.data_product_metadata.return_value = {"foo": "bar"}

    assert sdp_transfer._dpd_api_client is not None
    monkeypatch.setattr(ScanTransfer, "run", lambda: None)
    monkeypatch.setattr(ScanProcess, "run", lambda: None)

    mock_scan_process = MagicMock()
    mock_scan_process.completed = True
    monkeypatch.setattr("ska_pst.send.sdp_transfer.ScanProcess", mock_scan_process)

    mock_scan_transfer = MagicMock()
    mock_scan_transfer.completed = True
    monkeypatch.setattr("ska_pst.send.sdp_transfer.ScanTransfer", mock_scan_transfer)

    local_scan.data_product_file.touch()
    remote_scan.data_product_file.touch()
    data_product_file = remote_scan.data_product_file

    logger.info(f"data_product_file={data_product_file}")
    mock_voltage_recorder_scan = MagicMock()
    mock_voltage_recorder_scan.data_product_file_exists.return_value = True
    mock_voltage_recorder_scan.data_product_file = data_product_file
    logger.info(
        f"mock_voltage_recorder_scan.data_product_file={mock_voltage_recorder_scan.data_product_file}"
    )
    monkeypatch.setattr(
        "ska_pst.send.sdp_transfer.VoltageRecorderScan", lambda *args, **kwargs: mock_voltage_recorder_scan
    )

    def _stop_process() -> None:
        time.sleep(0.5)
        sdp_transfer.interrupt_processing()

    t = threading.Thread(target=_stop_process)
    t.start()
    sdp_transfer.process()
    t.join()

    cast(MagicMock, api_client.ingest_new_metadata).assert_called_once_with(
        metadata_file=remote_scan.data_product_file
    )
    cast(MagicMock, api_client.data_product_metadata).assert_called_once_with(uuid=metadata_uuid)


@pytest.mark.parametrize(
    "readable, writable, should_raise, expected_logs",
    [
        (
            True,
            True,
            False,
            [("debug", "Read permission is granted"), ("debug", "Write permission is granted")],
        ),
        (
            True,
            False,
            True,
            [("debug", "Read permission is granted"), ("error", "Write permission is not granted")],
        ),
        (
            False,
            True,
            True,
            [("error", "Read permission is not granted"), ("debug", "Write permission is granted")],
        ),
        (
            False,
            False,
            True,
            [("error", "Read permission is not granted"), ("error", "Write permission is not granted")],
        ),
    ],
)
def test_check_path_permissions(
    mock_logger: MagicMock,
    readable: bool,
    writable: bool,
    should_raise: bool,
    expected_logs: List[Tuple[str, str]],
) -> None:
    """Test check_path_permissions with different permission scenarios."""
    mock_path = MagicMock(spec=pathlib.Path)
    mock_path.name = "test_file"

    with patch(
        "os.access",
        side_effect=lambda path, mode: mode == os.R_OK and readable or mode == os.W_OK and writable,
    ):
        if should_raise:
            with pytest.raises(PermissionError, match="Permission deviation detected"):
                check_path_permissions(mock_path, mock_logger)
        else:
            check_path_permissions(mock_path, mock_logger)

    for level, msg in expected_logs:
        log_method = getattr(mock_logger, level)
        log_method.assert_any_call(f"{msg} for file: {mock_path.name}")
