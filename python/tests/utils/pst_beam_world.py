# -*- coding: utf-8 -*-
#
# This file is part of the SKA PST project
#
# Distributed under the terms of the BSD 3-clause new license.
# See LICENSE for more info.

"""Class file that is specific for the BDD 'world'."""

from __future__ import annotations

import json
import logging
import pathlib
import pprint
import shutil
import time
from datetime import datetime, timedelta
from typing import Any, Callable, Dict, List, Optional, Sequence, Tuple, cast

import backoff
import numpy as np
import pandas as pd
import requests
import tango
from ska_control_model import AdminMode, LoggingLevel, ObsState, PstProcessingMode, SimulationMode
from ska_pst.lmc import DeviceProxyFactory
from ska_pst.lmc.component.grpc_lmc_client import PstGrpcLmcClient, ServiceUnavailable
from ska_pst.lmc.dsp import generate_dsp_scan_request
from ska_pst.stat.hdf5 import Dimension, Polarisation
from ska_pst.testutils.common import pst_processing_mode_from_str
from ska_pst.testutils.dada import DspDataAnalyser
from ska_pst.testutils.scan_config import ChannelBlockValidator
from ska_pst.testutils.stats import (
    SampleStatistics,
    ScanStatFileWatcher,
    assert_statistics,
    assert_statistics_for_channels,
    assert_statistics_for_digitised_data,
)
from ska_pst.testutils.tango import AttributesMonitor, CommandTracker
from ska_pst.testutils.udp_gen import (
    GaussianNoiseConfig,
    SineWaveConfig,
    SquareWaveConfig,
    UdpDataGenerator,
    create_udp_data_generator,
)
from ska_pydada import DadaFile, SkaUnpacker, UnpackOptions
from ska_tango_testing.mock.tango import MockTangoEventCallbackGroup
from tango import DevState
from typing_extensions import TypedDict

from ska_pst.stat import Statistics

BackoffDetailsType = TypedDict("BackoffDetailsType", {"args": list, "elapsed": float})

OUTPUT_PATH_PATTERN: str = "product/<eb_id>/<sub_id>/<scan_id>/"

FILE_TYPE_GLOB_MAP: Dict[str, str] = {
    "voltage data": "*.dada",
    "weights and scales": "*.dada",
    "data statistics": "*.h5",
    "scan configuration": "scan_configuration.json",
    "metadata": "ska-data-product.yaml",
}


def _get_stat_attribute_name(
    polarisation: Polarisation, dimension: Dimension, stat_field: str, rfi_excised: bool = False
) -> str:
    stat_field = stat_field.lower()
    stat_field = stat_field.replace("number", "num")
    stat_field = stat_field.replace("frequency", "freq")
    stat_field = stat_field.replace("average", "avg")
    split = stat_field.split(" ")
    base_field_name = split[0] + "".join(ele.title() for ele in split[1:])
    "RfiExcised" if rfi_excised else ""

    return f"{dimension.text.lower()}Pol{polarisation.text}{base_field_name}"


def _in_range(value: float, low: float, high: float, rtol: float = 0.1) -> np.bool_:
    range_average = (low + high) / 2
    # used later to assert within range
    atol = abs((high - low) / 2)
    return np.isclose(value, range_average, atol=atol, rtol=rtol)


class PstBeamWorld:
    """
    Test class used for testing the PstBeam TANGO device.

    Note that the default_scanlen should be >10s since there are tests that expect
    10s Voltage Recorder and Flow Through data files to be written to disk.
    """

    def __init__(
        self: PstBeamWorld,
        change_event_callbacks: MockTangoEventCallbackGroup,
        recv_grpc_client: PstGrpcLmcClient,
        smrb_grpc_client: PstGrpcLmcClient,
        dsp_disk_grpc_client: PstGrpcLmcClient,
        dsp_ft_grpc_client: PstGrpcLmcClient,
        stat_grpc_client: PstGrpcLmcClient,
        monitoring_polling_rate: float,
        beam_attribute_names: List[str],
        stat_attribute_names: List[str],
        output_path: pathlib.Path,
        lfs_mount: pathlib.Path,
        beam_fqdn: str,
        dpd_api_endpoint: str | None = None,
        logger: logging.Logger | None = None,
        default_command_timeout: float = 10.0,
        default_scanlen: int = 15,
        ska_logging_level: LoggingLevel = LoggingLevel.INFO,
    ) -> None:
        """Put test class with Tango devices and event checker."""
        self.beam_proxy = DeviceProxyFactory.get_device(beam_fqdn)
        self.recv_grpc_client = recv_grpc_client
        self.smrb_grpc_client = smrb_grpc_client
        self.dsp_disk_grpc_client = dsp_disk_grpc_client
        self.dsp_ft_grpc_client = dsp_ft_grpc_client
        self.stat_grpc_client = stat_grpc_client
        self._monitoring_polling_rate = monitoring_polling_rate
        self.beam_attribute_names = [*beam_attribute_names]
        self.stat_attribute_names = [*stat_attribute_names]
        self._scan_config_overrides: dict = {}
        self.output_path = output_path
        self.lfs_mount = lfs_mount
        self.dpd_api_endpoint = dpd_api_endpoint
        self.logger = logger or logging.getLogger(__name__)
        self.eb_id: str | None = None
        self.expected_num_samples: int | None = None
        self.default_scanlen = default_scanlen

        # Before trying to set anything up we need to ensure the device proxies
        # are initialised. Even though we get a PstDeviceProxy from the factory
        # the device might not be initialised.
        self.beam_proxy.wait_for_initialised()
        self.beam_proxy.loggingLevel = ska_logging_level

        self.udp_data_generator: Optional[UdpDataGenerator] = None
        self.command_tracker = CommandTracker(
            device_proxy=self.beam_proxy,
            change_event_callbacks=change_event_callbacks,
            logger=logger,
            default_timeout=default_command_timeout,
        )
        self.attributes_monitor = AttributesMonitor(
            device_proxy=self.beam_proxy,
            attribute_names=beam_attribute_names,
        )
        self.stats_attributes_monitor = AttributesMonitor(
            device_proxy=self.beam_proxy, attribute_names=stat_attribute_names
        )
        self.curr_scan_configuration: dict | None = None
        self.curr_scan_id: int | None = None
        self._stat_file_watcher: ScanStatFileWatcher | None = None

    def _cleanup_filesystem(self: PstBeamWorld) -> None:
        """Clean up filesystems ready for tests."""

        def _log_cleanup_error(_fn: Callable[..., Any], path: str, exc_info: Any) -> None:
            self.logger.warning(f"Exception occurred while removing {path}.", exc_info=exc_info)

        def _clean_mount(mount: pathlib.Path) -> None:
            if self.curr_eb_id is None:
                return

            eb_id_dir = mount / "product" / self.curr_eb_id

            if not eb_id_dir.exists():
                return

            self.logger.info(f"Removing {eb_id_dir}")
            shutil.rmtree(eb_id_dir, onerror=_log_cleanup_error)

        _clean_mount(self.lfs_mount)
        _clean_mount(self.output_path)

    def setup(self: PstBeamWorld) -> None:
        """Set up the PST.BEAM world context for tests."""
        self.setup_test_state()
        self.attributes_monitor.setup()
        self._cleanup_filesystem()

    def set_scan_config_override_value(
        self: PstBeamWorld,
        key: str,
        value: Any,
    ) -> None:
        """Set scan configuration overrides."""
        self.scan_config_overrides[key] = value

    @property
    def subsystem_grpc_clients(self: PstBeamWorld) -> List[PstGrpcLmcClient]:
        """Get a list gRPC client for all the subsystems."""
        curr_dsp_grpc_client = self.curr_dsp_grpc_client
        if curr_dsp_grpc_client is None:
            dsp_grpc_clients = [
                self.dsp_disk_grpc_client,
                self.dsp_ft_grpc_client,
            ]
        else:
            dsp_grpc_clients = [curr_dsp_grpc_client]

        return [
            self.smrb_grpc_client,
            self.recv_grpc_client,
            self.stat_grpc_client,
            *dsp_grpc_clients,
        ]

    @property
    def curr_processing_mode(self: PstBeamWorld) -> PstProcessingMode:
        """Get the current processing mode that PST Beam is configured for.

        If the beam is not configured then this will return a None.
        """
        if self.curr_scan_configuration is None:
            return PstProcessingMode.IDLE

        processing_mode = self.curr_scan_configuration["pst"]["scan"]["observation_mode"]
        return pst_processing_mode_from_str(processing_mode)

    @property
    def curr_dsp_grpc_client(self: PstBeamWorld) -> PstGrpcLmcClient | None:
        """Get the current DSP client base on processing mode."""
        curr_processing_mode = self.curr_processing_mode
        if curr_processing_mode == PstProcessingMode.IDLE:
            return None

        if curr_processing_mode == PstProcessingMode.VOLTAGE_RECORDER:
            return self.dsp_disk_grpc_client
        elif curr_processing_mode == PstProcessingMode.FLOW_THROUGH:
            return self.dsp_ft_grpc_client
        else:
            raise ValueError(f"processing mode {curr_processing_mode} is not supported")

    @property
    def scan_config_overrides(self: PstBeamWorld) -> dict:
        """Get scan configuration overrides."""
        return {**self._scan_config_overrides}

    @property
    def subsystem_id(self: PstBeamWorld) -> str:
        """
        Get the subsystem that the beam is deployed in.

        This gets the SubsystemId from the BEAM.MGMT Tango device.
        """
        return str(self.beam_proxy.get_property("SubsystemId")["SubsystemId"][0])

    @property
    def curr_eb_id(self: PstBeamWorld) -> str | None:
        """Get the current eb_id that the beam is configured for."""
        if self.curr_scan_configuration is None:
            return None

        return self.curr_scan_configuration["common"]["eb_id"]

    def ensure_beam_mgmt_online(self: PstBeamWorld) -> None:
        """Ensure that BEAM.MGMT is online and in On state."""
        if self.beam_proxy.adminMode in [AdminMode.NOT_FITTED, AdminMode.RESERVED]:
            self.beam_proxy.adminMode = AdminMode.OFFLINE

        self.online()
        self.check_lmc_alarm_state(raise_exc=True)
        self.on()

    def check_lmc_alarm_state(self: PstBeamWorld, raise_exc: bool = False) -> None:
        """Check the alarm state of the LMC.

        This will check the alarm state of the BEAM device under test. If
        it is in ALARM state it will dump all monitored attributes that
        are in an ALARM state.

        If ``raise_exc`` is True, then this will raise and exception after
        logging all the alarm state.

        :param raise_exc: whether to raise an exception of the device state is
            ALARM, defaults to False
        :type raise_exc: bool, optional
        """
        if self.beam_proxy.state() != DevState.ALARM:
            return

        self.logger.warning(
            f"{self.beam_proxy} is in ALARM state. Logging attributes that are in ALARM state."
        )

        for attr_info in cast(Sequence, self.beam_proxy.attribute_list_query()):
            attr = self.beam_proxy.read_attribute(attr_info.name)
            if attr.quality in [tango.AttrQuality.ATTR_WARNING, tango.AttrQuality.ATTR_ALARM]:
                self.logger.warning(f"{self.beam_proxy}.{attr_info.name} is not valid. Details: {attr}")

        if raise_exc:
            raise AssertionError(f"{self.beam_proxy} is in ALARM state.")

    def teardown(self: PstBeamWorld) -> None:
        """Teardown the world context at end of test."""
        try:
            if self._stat_file_watcher is not None:
                self._stat_file_watcher.stop()

            # ensure we reset aborted states
            if self.beam_proxy.obsState == ObsState.SCANNING:
                try:
                    self.end_scan()
                except Exception:
                    self.logger.warning(
                        "Error occurred while trying to end scan, attempting to abort scan.", exc_info=True
                    )
                    self.abort()

            if self.beam_proxy.obsState in [ObsState.ABORTED, ObsState.FAULT]:
                self.obs_reset()

            if self.beam_proxy.obsState == ObsState.READY:
                self.goto_idle()

        finally:
            self.attributes_monitor.teardown()
            self.command_tracker.teardown()
            self._cleanup_filesystem()

    @property
    def channel_block_configuration(self: PstBeamWorld) -> dict:
        """Get the current channel block configuration as a dictionary."""
        return json.loads(self.beam_proxy.channelBlockConfiguration)

    def ensure_initial_state(
        self: PstBeamWorld,
        state: ObsState,
        configure_scan_request: dict = {},
        scan_id: Optional[int] = None,
    ) -> None:
        """Ensure that PST is in a given initial state.

        This is used to fast forward tests rather than having
        a huge Given/And to get to something like the Scanning
        state.
        """
        self.ensure_monitoring_polling_rate()

        if state == ObsState.IDLE:
            return

        if state in [ObsState.READY, ObsState.SCANNING]:
            self.configure_scan(configure_scan_request, raise_exception=True)

        if state == ObsState.SCANNING:
            assert scan_id is not None
            self.start_scan(scan_id=scan_id, raise_exception=True)

        if state == ObsState.ABORTED:
            self.abort(raise_exception=True)

        if state == ObsState.FAULT:
            self.goto_fault(raise_exception=True)

    def configure_scan(self: PstBeamWorld, configure_scan_request: dict, **kwargs: Any) -> None:
        """Perform a configure scan."""
        if self.eb_id is not None:
            configure_scan_request["common"]["eb_id"] = self.eb_id

        assert (
            "eb_id" in configure_scan_request["common"]
        ), "Expected 'eb_id' to be in common part of scan request"
        self.command_tracker.perform_command(
            "ConfigureScan",
            scan_configuration=configure_scan_request,
            timeout=10.0,
            **kwargs,
        )
        self.curr_scan_configuration = configure_scan_request

    def _start_stat_file_watcher(self: PstBeamWorld) -> None:
        # don't attempt to start the STAT file watcher.
        # we have tests that are testing invalid command transitions and
        # put unnecessary exceptions in our logs because of this.
        if self.curr_scan_configuration is None:
            return

        try:
            scan_path = self.local_scan_output_path
            # ensure the output path exists so we can watch for files
            scan_path.mkdir(parents=True, exist_ok=True)
            self._stat_file_watcher = ScanStatFileWatcher(
                scan_path=scan_path,
                logger=self.logger,
            )
            self.logger.info(f"Starting STAT file watcher on {scan_path}")
            self._stat_file_watcher.watch()
        except Exception:
            self.logger.warning("Error while starting STAT file watcher", exc_info=True)

    def start_scan(
        self: PstBeamWorld,
        scan_id: int,
        **kwargs: Any,
    ) -> None:
        """Perform a scan."""
        # capture the monitoring values before starting a scan
        self.logger.debug(f"Starting scan: {scan_id}")
        self.curr_scan_id = scan_id
        self._start_stat_file_watcher()
        self.command_tracker.perform_command("Scan", scan_id=scan_id, **kwargs)
        self._start_scanning_time = datetime.now()

    def abort(self: PstBeamWorld, **kwargs: Any) -> None:
        """Abort long running command."""
        self.command_tracker.perform_command("Abort", **kwargs)
        # stop sending data if we're already sending data.
        self.stop_sending_udp_data()

    def end_scan(self: PstBeamWorld, **kwargs: Any) -> None:
        """End current scan."""
        self._end_scan_request_time = datetime.now()
        self.command_tracker.perform_command("EndScan", **kwargs)
        self.stop_sending_udp_data()
        self.attributes_monitor.capture_current_values()

    def goto_idle(self: PstBeamWorld, **kwargs: Any) -> None:
        """Put Tango device into IDLE state."""
        self.command_tracker.perform_command("GoToIdle", **kwargs)

    def goto_fault(self: PstBeamWorld, fault_message: Optional[str] = None, **kwargs: Any) -> None:
        """Put Tango device into IDLE state."""
        fault_message = fault_message or "Putting system into fault state for tests"
        self.command_tracker.perform_command("GoToFault", fault_message=fault_message, **kwargs)

    def obs_reset(self: PstBeamWorld, **kwargs: Any) -> None:
        """Perform an ObsReset on Tango device."""
        self.command_tracker.perform_command("ObsReset", **kwargs)

    def reset(self: PstBeamWorld, **kwargs: Any) -> None:
        """Perform a Reset on Tango device."""
        # note the timeout for this is 10 minutes. It should not happen under normal circumstances
        # but k8s crash look backoff can take 5 minutes between restarting and app and then there
        # is the time for it to be registered with a service, along with the timing with TANGO
        # infrastructure itself.
        self.command_tracker.perform_command(
            "Reset", wait_for_obs_state=ObsState.IDLE, timeout=600.0, **kwargs
        )

    def on(self: PstBeamWorld) -> None:
        """Turn on Tango device."""
        if self.beam_proxy.state() is not DevState.ON:
            self.command_tracker.perform_command("On", raise_exception=True)

        self.assert_state(DevState.ON)

        try:
            self.assert_obstate(ObsState.IDLE, sub_obs_state=ObsState.EMPTY)
        except Exception:
            # unknown state - force everything into a fault state and do obs reset
            self.logger.warning(
                "Error in asserting initial state. Forcing in to fault and resetting.",
                exc_info=True,
            )
            # Log the current state of the core apps

            self.command_tracker.perform_command(
                "GoToFault",
                fault_message="Ensuring in correct state for tests",
                raise_exception=True,
            )
            self.command_tracker.perform_command("ObsReset", raise_exception=True)
            self.assert_obstate(ObsState.IDLE, sub_obs_state=ObsState.EMPTY)

    def off(self: PstBeamWorld) -> None:
        """Turn off Tango device."""
        if self.beam_proxy.state() is not DevState.OFF:
            self.command_tracker.perform_command("Off", raise_exception=True)

        self.assert_state(DevState.OFF)

    def online(self: PstBeamWorld) -> None:
        """Put Tango device into ONLINE mode."""
        if self.beam_proxy.adminMode != AdminMode.ONLINE:
            self.beam_proxy.adminMode = AdminMode.ONLINE

        self.assert_admin_mode(admin_mode=AdminMode.ONLINE)

    def ensure_monitoring_polling_rate(self: PstBeamWorld) -> None:
        """Ensure the proxies monitoring polling rate needed for tests."""
        polling_interval = int(self._monitoring_polling_rate * 1000.0)
        self.beam_proxy.monitoringPollingRate = polling_interval

    @property
    def monitoring_polling_rate(self: PstBeamWorld) -> float:
        """Get the monitoring polling rate."""
        return self._monitoring_polling_rate

    @monitoring_polling_rate.setter
    def monitoring_polling_rate(self: PstBeamWorld, monitoring_polling_rate: float) -> None:
        """Set the monitoring polling rate."""
        self.logger.info(f"Setting monitoring polling rate to {monitoring_polling_rate:.3f} seconds")
        self._monitoring_polling_rate = monitoring_polling_rate
        self.ensure_monitoring_polling_rate()

    def offline(self: PstBeamWorld) -> None:
        """Put Tango device into OFFLINE mode."""
        if self.beam_proxy.adminMode != AdminMode.OFFLINE:
            self.beam_proxy.adminMode = AdminMode.OFFLINE

        self.assert_admin_mode(admin_mode=AdminMode.OFFLINE)
        self.assert_state(DevState.DISABLE)

    def setup_test_state(self: PstBeamWorld) -> None:
        """Ensure Tango device is in Offline mode before test executes."""
        try:
            self.ensure_beam_mgmt_online()

            if self.beam_proxy.obsState in [ObsState.ABORTED, ObsState.FAULT]:
                self.logger.info(
                    f"{self.beam_proxy} is in obsState {self.beam_proxy.obsState}. Calling ObsReset"
                )
                self.obs_reset()

            if self.beam_proxy.obsState == ObsState.SCANNING:
                self.logger.info(f"{self.beam_proxy} is SCANNING. Calling EndScan")
                self.end_scan()

            if self.beam_proxy.obsState == ObsState.READY:
                self.logger.info(f"{self.beam_proxy} is READY. Calling GoToIdle")
                self.goto_idle()
        except Exception:
            self.logger.exception("Error while trying to setup test state.", exc_info=True)
            self.reset()

        # ensure we're not using simulation mode
        if self.beam_proxy.simulationMode == SimulationMode.TRUE:
            self.beam_proxy.simulationMode = SimulationMode.FALSE

    @property
    def curr_scanlen(self: PstBeamWorld) -> int:
        """Get the current scan length.

        This will get the value from the UDP generator if it exists else
        returns the the `default_scanlen`.
        """
        if self.udp_data_generator is not None:
            return self.udp_data_generator.scanlen

        return self.default_scanlen

    def _dsp_data_analyser(self: PstBeamWorld) -> DspDataAnalyser:
        """Create instance of DspDataAnalyser based on current configuration."""
        curr_scan_configuration = self.curr_scan_configuration
        assert curr_scan_configuration is not None, "scan configuration should have been set"
        assert self.curr_scan_id is not None

        return DspDataAnalyser(
            scan_config=curr_scan_configuration,
            scan_id=self.curr_scan_id,
            eb_id=self.curr_eb_id,
            subsystem_id=self.subsystem_id,
            base_dir=self.output_path,
            logger=self.logger,
        )

    def send_data(
        self: PstBeamWorld,
        calculated_resources: dict,
        udpgen_extra_args: List[str] = [],
        scanlen: Optional[int] = None,
        data_generator: str | None = None,
        generator_params: GaussianNoiseConfig | SineWaveConfig | SquareWaveConfig | None = None,
    ) -> None:
        """Send UDPGEN data."""
        assert self.curr_scan_id is not None
        scanlen = scanlen or self.default_scanlen

        self.udp_data_generator = create_udp_data_generator(
            scan_resources=calculated_resources,
            scan_id=self.curr_scan_id,
            scanlen=scanlen,
            channel_block_configuration=self.channel_block_configuration,
            data_generator=data_generator,
            generator_params=generator_params,
            udpgen_extra_args=udpgen_extra_args,
            logger=self.logger,
        )
        self.generator_params = generator_params
        self.udp_data_generator.generate_udp_data()

    def stop_sending_udp_data(self: PstBeamWorld) -> None:
        """Stop sending UDP data."""
        if self.udp_data_generator is not None:
            self.udp_data_generator.abort()

        self.udp_data_generator = None

        self.wait_for_monitoring_data_to_stop()

    def wait_for_monitoring_data(self: PstBeamWorld) -> None:
        """Wait until there is monitoring updates."""

        @backoff.on_exception(
            backoff.expo,
            AssertionError,
            factor=0.1,
            max_time=2 * self._monitoring_polling_rate,
        )
        def _assert() -> None:
            self.attributes_monitor.assert_attribute_values_changed()

        _assert()

    def wait_for_scan_to_complete(self: PstBeamWorld) -> None:
        """Wait for all the UDP data to have been sent."""
        self.logger.debug("Waiting for scan to complete")
        if self.udp_data_generator is not None:
            self.logger.debug("UDP Data Generator is not none. Waiting for that to complete.")
            self.udp_data_generator.wait_for_end_of_data()
        else:
            self.logger.debug("There was no UDP Data Generator.")

        time.sleep(2 * self.monitoring_polling_rate)

        self.logger.info(
            f"Finished waiting for data scan to complete. dataReceived = {self.beam_proxy.dataReceived}"
        )

    @backoff.on_exception(
        backoff.expo,
        AssertionError,
        factor=0.1,
        max_value=5.0,  # wait at most 5 seconds before rechecking
        max_time=120.0,  # may need to be optimised too.
    )
    def wait_for_send_to_complete(self: PstBeamWorld) -> None:
        """Wait for SEND to have finished processing."""
        assert self.curr_eb_id is not None
        assert self.subsystem_id is not None
        assert self.curr_scan_id is not None
        data_product_file: pathlib.Path = self.remote_scan_output_path / "ska-data-product.yaml"

        assert data_product_file.exists(), f"Expected {data_product_file} to exist"
        today = datetime.today()
        day_offset = timedelta(days=1)
        if self.dpd_api_endpoint is not None:
            # have +- today to allow for weirdness around midnight, it should also make it more performant.
            # select based on EB_ID and SCAN_ID
            search_request = {
                "start_date": (today - day_offset).strftime("%Y-%m-%d"),
                "end_date": (today + day_offset).strftime("%Y-%m-%d"),
                "key_value_pairs": [
                    f"execution_block:{self.curr_eb_id}",
                    f"obscore.obs_id:{self.curr_scan_id}",
                ],
            }
            search_term = str(data_product_file.relative_to(self.output_path))

            endpoint = f"{self.dpd_api_endpoint}/dataproductsearch"
            response = requests.post(endpoint, headers={"accept": "application/json"}, json=search_request)
            if response.ok:
                for item in response.json():
                    if "metadata_file" in item and item["metadata_file"] == search_term:
                        return

                raise AssertionError(f"Could not find {search_term} in DPD index.")
            else:
                raise Exception(f"Failed to search for data products. Status code: {response.status_code}")

    @property
    def relative_scan_output_path(self: PstBeamWorld) -> pathlib.Path:
        """Get the relative path for the scan output files."""
        assert self.curr_scan_configuration is not None
        assert self.curr_scan_id is not None
        config = {
            **self.curr_scan_configuration["common"],
            **self.curr_scan_configuration["pst"]["scan"],
            "scan_id": self.curr_scan_id,
            "sub_id": self.subsystem_id,
        }

        relative_path = OUTPUT_PATH_PATTERN
        for k, v in config.items():
            relative_path = relative_path.replace(f"<{k}>", str(v))

        return pathlib.Path(relative_path)

    @property
    def remote_scan_output_path(self: PstBeamWorld) -> pathlib.Path:
        """Get the expected scan output path."""
        return self.output_path / self.relative_scan_output_path

    @property
    def local_scan_output_path(self: PstBeamWorld) -> pathlib.Path:
        """Get the expected scan output path for local file system."""
        return self.lfs_mount / self.relative_scan_output_path

    @property
    def local_scan_data_files(self: PstBeamWorld) -> List[DadaFile]:
        """Get a list of data files on the local file storage."""
        return [
            DadaFile.load_from_file(f, chunk_size=-1)
            for f in self.local_scan_output_path.glob("data/**/*.dada")
        ]

    @property
    def local_scan_weights_files(self: PstBeamWorld) -> List[DadaFile]:
        """Get a list of weights files on the local file storage."""
        return [DadaFile.load_from_file(f) for f in self.local_scan_output_path.glob("weights/**/*.dada")]

    #######################
    # Assertions
    #######################

    def assert_no_monitoring_attributes_updates(self: PstBeamWorld) -> None:
        """Assert there has been no updates to monitoring attributes."""

        @backoff.on_exception(
            backoff.expo,
            AssertionError,
            factor=0.1,
            max_time=2 * self._monitoring_polling_rate,
        )
        def _assert() -> None:
            self.attributes_monitor.assert_attribute_values_not_changed()

        _assert()

    @backoff.on_exception(
        backoff.expo,
        AssertionError,
        factor=0.05,
        max_time=1.0,
    )
    def assert_state(self: PstBeamWorld, state: DevState) -> None:
        """Assert Tango devices are in a given DevState."""
        assert (
            self.beam_proxy.state() == state
        ), f"Expecting {self.beam_proxy}.state()={self.beam_proxy.state()} to be {state}"

    @backoff.on_exception(
        backoff.expo,
        AssertionError,
        factor=0.05,
        max_time=1.0,
    )
    def assert_obstate(
        self: PstBeamWorld, obs_state: ObsState, sub_obs_state: Optional[ObsState] = None
    ) -> None:
        """Assert that the Tango devices are in a given ObsState."""
        curr_obs_state = ObsState(self.beam_proxy.obsState)
        assert (
            curr_obs_state == obs_state
        ), f"Expecting {self.beam_proxy}.obsState={curr_obs_state.name} to be {obs_state.name}"

        if sub_obs_state is None:
            sub_obs_state = obs_state

        for grpc_client in self.subsystem_grpc_clients:
            self.assert_subsystem_state(grpc_client, sub_obs_state)

    @backoff.on_exception(
        backoff.expo,
        AssertionError,
        factor=1,
        max_time=5.0,
    )
    def assert_admin_mode(self: PstBeamWorld, admin_mode: AdminMode) -> None:
        """Assert that the Tango devices are in a given AdminMode."""
        curr_admin_mode = AdminMode(self.beam_proxy.adminMode)
        assert (
            curr_admin_mode == admin_mode
        ), f"Expecting {self.beam_proxy}.adminMode={curr_admin_mode.name} to be {admin_mode.name}"

    def assert_invalid_packet(self: PstBeamWorld, monitored_property: str) -> None:
        """Assert that the invalid packet recorded given InducedErrorOption."""
        attribute_names_map = {
            "malformed": "malformedPackets",
            "dropped": "dataDropped",
            "misdirected": "misdirectedPackets",
            "noValidPolarisationCorrection": [
                "noValidPolarisationCorrectionPackets",
                "noValidPolarisationCorrectionPacketRate",
            ],
            "noValidStationBeam": ["noValidStationBeamPackets", "noValidStationBeamPacketRate"],
            "noValidPstBeam": ["noValidPstBeamPackets", "noValidPstBeamPacketRate"],
        }

        attribute_names = attribute_names_map[monitored_property]
        if isinstance(attribute_names, str):
            attribute_names = [attribute_names]

        @backoff.on_exception(backoff.expo, AssertionError, factor=0.1, max_time=self.curr_scanlen)
        def _assert() -> None:
            for attribute_name in attribute_names:
                self.logger.debug(
                    f"assert_invalid_packet {attribute_name}: {getattr(self.beam_proxy, attribute_name)}"
                )
                self.attributes_monitor.assert_attribute(attribute_name, lambda x: x > 0)

        _assert()

    def assert_received_data(self: PstBeamWorld, data_received: bool = True) -> None:
        """Assert monitored data by RECV."""
        if data_received:
            assert self.beam_proxy.dataReceived > 0
        else:
            assert self.beam_proxy.dataReceived == 0

    def assert_recorded_data(self: PstBeamWorld, data_recorded: bool = True) -> None:
        """Assert monitored data by DSP."""
        if data_recorded:
            assert self.beam_proxy.dataRecorded > 0
        else:
            assert self.beam_proxy.dataRecorded == 0

    @backoff.on_exception(
        backoff.expo,
        exception=ServiceUnavailable,
        factor=0.1,
        max_time=120.0,
    )
    def assert_subsystem_state(
        self: PstBeamWorld,
        grpc_client: PstGrpcLmcClient,
        obsState: ObsState,
    ) -> None:
        """Assert subsystem state.

        This asserts the core app's gRPC state.
        """
        assert (
            grpc_client.get_state() == obsState
        ), f"Expected {grpc_client}.get_state()={grpc_client.get_state()} to be {obsState}"

    def assert_previous_command_rejected(
        self: PstBeamWorld,
    ) -> None:
        """Assert that previous command was rejected."""
        self.command_tracker.assert_previous_command_rejected()

    def assert_previous_command_failed(
        self: PstBeamWorld,
    ) -> None:
        """Assert that previous command failed."""
        self.command_tracker.assert_previous_command_failed()

    def assert_previous_command_error_message_matches(
        self: PstBeamWorld,
        expected_message_regexp: str,
    ) -> None:
        """Assert that previous command error message is as expected."""
        self.command_tracker.assert_previous_command_error_message_matches(expected_message_regexp)

    def log_core_apps_state(self: PstBeamWorld) -> None:
        """
        Log out the state of the core apps.

        This allows debugging to know if a particular app is in the incorrect state in teardown of
        the BDD test.
        """
        for grpc_client in self.subsystem_grpc_clients:
            remote_state = grpc_client.get_state()
            self.logger.info(f"{grpc_client}.state = {remote_state.name}")

    def log_lmc_state(self: PstBeamWorld) -> None:
        """Log out properties and state of Tango devices."""
        for p in [
            "lrcQueue",
            "lrcExecuting",
            "lrcFinished",
            "obsState",
            "adminMode",
            "state()",
        ]:
            if p == "state()":
                self.logger.info(f"{self.beam_proxy}.{p} = {self.beam_proxy.state()}")
                self.check_lmc_alarm_state()
            else:
                if self.beam_proxy.state() != DevState.DISABLE:
                    self.logger.info(f"{self.beam_proxy}.{p} = {getattr(self.beam_proxy, p)}")

    def log_scan_configuration(self: PstBeamWorld) -> None:
        """Log out the scan configuration and scan id."""
        try:
            if self.curr_scan_configuration is not None:
                self.logger.info(f"scan_configuration={pprint.pformat(self.curr_scan_configuration)}")

            if self.curr_scan_id:
                self.logger.info(f"curr_scan_id={self.curr_scan_id}")
        except AttributeError:
            # okay to ignore exception here as we might not have got into correct state
            pass

    def assert_monitoring_data_updated(self: PstBeamWorld) -> None:
        """Assert monitoring is being updated."""

        @backoff.on_exception(
            backoff.expo,
            AssertionError,
            factor=0.1,
            max_time=2 * self._monitoring_polling_rate,
        )
        def _assert() -> None:
            self.attributes_monitor.assert_attribute_values_changed()

        _assert()

    def assert_has_received_monitoring_data(self: PstBeamWorld) -> None:
        """Assert that has been monitoring data received."""
        try:
            self.attributes_monitor.wait_for_attribute_update(
                "dataReceived", timeout=2.0 * self._monitoring_polling_rate
            )
        except TimeoutError:
            assert self.beam_proxy.dataReceived > 0

    def wait_for_monitoring_data_to_stop(self: PstBeamWorld) -> None:
        """Wait for monitoring data to stop updating.

        This is implemented by getting the current values, wait for some time
        and check if the values are the same.
        """
        # ensure we wait a monitoring polling rate first.
        try:
            # need to wait for the monitoring polling rate
            time.sleep(2.0 * self._monitoring_polling_rate)
            self.attributes_monitor.wait_for_attribute_update(
                "dataReceived", timeout=self._monitoring_polling_rate
            )
            if self.beam_proxy.dataReceived == 0:
                return
            raise AssertionError("Expected monitoring data to have stopped updating")
        except TimeoutError:
            pass

    def assert_data_not_received(self: PstBeamWorld) -> None:
        """Check no data has been received."""
        assert self.beam_proxy.obsState == ObsState.SCANNING, (
            f"Expected PST to be in a SCANNING state but was in "
            f"the {ObsState(self.beam_proxy.obsState).name} state"
        )
        time.sleep(2 * self._monitoring_polling_rate)
        assert self.beam_proxy.dataReceived == 0

    # need to wait for SEND to have done its job
    @backoff.on_exception(
        backoff.expo,
        AssertionError,
        factor=0.1,
        max_time=60.0,
    )
    def assert_data_files_exists(self: PstBeamWorld, data_types: str | List[str] | None = None) -> None:
        """Check data was written to disk."""
        self.wait_for_scan_to_complete()
        dsp_data_analyser = self._dsp_data_analyser()
        if data_types is None:
            data_types = ["data", "weights"]  # type: ignore
        elif isinstance(data_types, str):  # type: ignore
            data_types = [data_types]  # type: ignore

        for dt in data_types:  # type: ignore
            dsp_data_analyser.check_dada_files_exist(dsp_subpath=dt)

    def assert_weights_has_dropped_packets(self: PstBeamWorld, dropped_packets: List[int]) -> None:
        """Check weights files contain the specified dropped packet indexes."""
        self._dsp_data_analyser().check_weights_contain_dropped_packets(dropped_packets)

    def assert_weights_zeroed_for_invalid_cbf_flag(self: PstBeamWorld, invalid_packets: List[int]) -> None:
        """Check weights files has zeroed out weights for invalid CBF packets."""
        self._dsp_data_analyser().check_weights_zeroed(invalid_packets)

    def assert_sine_wave(self: PstBeamWorld, sinusoid_frequency: float) -> None:
        """Check sine wave in data file."""
        self._dsp_data_analyser().check_sinusoid_frequency(expected_frequency=sinusoid_frequency)

    def assert_contiguous_files(
        self: PstBeamWorld, scanlen: float, file_type: str, calculated_resources: dict
    ) -> None:
        """Assert that files from scan are contiguous."""
        calculated_resources.update(generate_dsp_scan_request(**calculated_resources))
        self._dsp_data_analyser().check_contiguous_files(
            scanlen=scanlen, file_type=file_type, calculated_resources=calculated_resources
        )

    def assert_recording_time_remaining_less_than(self: PstBeamWorld, expected_remaining_time: int) -> None:
        """Assert that the total recording time is less than expected."""

        @backoff.on_exception(
            backoff.constant,
            AssertionError,
            interval=self._monitoring_polling_rate,
            max_time=self.curr_scanlen,
        )
        def _assert() -> None:
            self.attributes_monitor.assert_attribute(
                "availableRecordingTime", lambda a: a <= expected_remaining_time
            )

        _assert()

    def assert_in_alarm_state(self: PstBeamWorld) -> None:
        """Assert that BEAM.MGMT is in an alarm state."""

        @backoff.on_exception(
            backoff.expo,
            AssertionError,
            factor=0.1,
            max_time=2 * self._monitoring_polling_rate,
        )
        def _assert() -> None:
            self.assert_state(DevState.ALARM)

        _assert()

    def assert_attribute_quality(
        self: PstBeamWorld,
        attribute: str,
        maximum: bool,
        attribute_quality: str,
    ) -> None:
        """Assert that the Tango attribute has a given attribute quality.

        This can be used to test if the attribute is in a warning or alarm
        state.
        """
        from tango import AttributeInfoEx, AttrQuality, DeviceAttribute

        if attribute_quality == "warning":
            expected_quality = AttrQuality.ATTR_WARNING
        elif attribute_quality == "alarm":
            expected_quality = AttrQuality.ATTR_ALARM

        tango_attribute_config: AttributeInfoEx = self.beam_proxy.get_attribute_config(attribute)

        @backoff.on_exception(
            backoff.expo,
            AssertionError,
            factor=self._monitoring_polling_rate,
            max_time=self.curr_scanlen,
        )
        def _assert() -> None:
            tango_attribute: DeviceAttribute = self.beam_proxy.read_attribute(attribute)
            self.logger.debug(f"Tango device attribute for {attribute} = {tango_attribute}")
            quality: AttrQuality = tango_attribute.quality

            assert (
                quality == expected_quality
            ), f"Expected attribute quality of {attribute} to be {expected_quality} but it was {quality}"

            if maximum:
                max_warning = float(tango_attribute_config.alarms.max_warning)
                max_alarm = float(tango_attribute_config.alarms.max_alarm)

                assert float(tango_attribute.value) >= max_warning, (
                    f"Expected warning/alarm for {attribute} to be because "
                    f"value is >= max_warning of {max_warning}"
                )
                if attribute_quality == "alarm":
                    assert (
                        float(tango_attribute.value) >= max_alarm
                    ), f"Expected alarm for {attribute} to be because value is >= max_alarm of {max_alarm}"
            else:
                min_warning = float(tango_attribute_config.alarms.min_warning)
                min_alarm = float(tango_attribute_config.alarms.min_alarm)

                assert tango_attribute.value <= min_warning, (
                    f"Expected warning/alarm for {attribute} to be because "
                    f"value is <= min_warning of {min_warning}"
                )
                if attribute_quality == "alarm":
                    assert (
                        float(tango_attribute.value) <= min_alarm
                    ), f"Expected alarm for {attribute} to be because value is <= max_alarm of {min_alarm}"

        _assert()

    def assert_channel_block_configuration_empty(self: PstBeamWorld) -> None:
        """Check that the channel block configuration JSON attribute is empty."""
        self.channel_block_validator = ChannelBlockValidator(
            encoded_json=self.beam_proxy.channelBlockConfiguration, logger=self.logger
        )
        assert (
            self.channel_block_validator.is_empty()
        ), "beam channelBlockConfiguration attribute was not empty"

    def assert_channel_block_configuration_valid(self: PstBeamWorld) -> None:
        """Check that the channel block configuration JSON attribute is valid."""
        self.channel_block_validator = ChannelBlockValidator(
            encoded_json=self.beam_proxy.channelBlockConfiguration, logger=self.logger
        )
        self.channel_block_validator.validate()

    def assert_output_path_exists(self: PstBeamWorld) -> None:
        """Check that the output path was created."""
        expected_path = self.remote_scan_output_path
        self.logger.debug(f"Expected output path = {expected_path}")
        assert expected_path.exists(), f"Expected {expected_path} to exist."
        assert expected_path.is_dir(), f"Expected {expected_path} to be a directory."

    def assert_files_written_to_folder(
        self: PstBeamWorld, file_type: str, subfolder_path: str | None = None
    ) -> None:
        """Check that the correct files are written to correct output files."""
        if subfolder_path is None:
            expected_output_path = self.remote_scan_output_path
        else:
            expected_output_path = self.remote_scan_output_path / subfolder_path

        assert (
            expected_output_path.exists() and expected_output_path.is_dir()
        ), f"Expected {expected_output_path} to exist for type {file_type}"

        self.logger.debug(f"Files in {expected_output_path}: \n{list(expected_output_path.iterdir())}")

        glob_pattern = FILE_TYPE_GLOB_MAP[file_type]
        self.logger.debug(
            f"Asserting {file_type} files with glob pattern {glob_pattern} exist in {expected_output_path}"
        )
        files = list(expected_output_path.glob(glob_pattern))
        assert len(files) > 0, (
            f"Expected {expected_output_path} to at least one file with pattern {glob_pattern}. "
            f"Files in dir: {list(expected_output_path.iterdir())}"
        )

    def assert_stat_scalar_attributes_updated(
        self: PstBeamWorld, period_range_sec: Tuple[float, float]
    ) -> None:
        """Check that the scalar statistics are updated within period."""
        # this will ensure that monitoring has st
        assert self.udp_data_generator is not None, "Expected UDP Data Generator to exist"
        self.udp_data_generator.wait_for_end_of_data()

        (low, high) = period_range_sec

        time.sleep(2 * self.monitoring_polling_rate)

        errors: List[AssertionError] = []

        def _assert_attribute(attr: str) -> None:
            # ignore first event as that is what we get when we subscribe to the events but that
            # is before the scan has even started
            attr_hist_events = self.attributes_monitor.get_attribute_history_events(attr)[1:]

            # should have had initial value
            # then at scan length / least period_range_sec[1] extra records
            expected_num_records = int(self.curr_scanlen / high)
            self.logger.debug(f"Expected number of history events = {expected_num_records}")
            self.logger.debug(f"Number of history events received = {len(attr_hist_events)}")
            assert (
                len(attr_hist_events) >= expected_num_records
            ), f"Expected at least {expected_num_records} of updates for {attr}"

            # first 2 events can be very close due to some funkiness in the LMC / gRPC
            # so we want to drop the first event and then calc diffs between other events
            # which requires a total of 3 or more events
            if len(attr_hist_events) > 2:
                diff_sum = 0.0
                diffs = [
                    (s.update_time - f.update_time).total_seconds()
                    for (f, s) in zip(attr_hist_events[1:-1], attr_hist_events[2:])
                ]
                for idx, diff in enumerate(diffs):
                    diff_sum += diff
                    assert _in_range(diff, low=low, high=high), (
                        f"Expected attribute update change {idx} to be within range "
                        f"of [{low}, {high}] seconds. Change occurred {diff:0.3f} seconds. "
                        f"Previous event occurred at {attr_hist_events[idx].update_time} "
                        f"and current event time {attr_hist_events[idx + 1].update_time}"
                    )

                average_diff = diff_sum / len(diffs)
                assert _in_range(average_diff, low=low, high=high, rtol=0.01), (
                    f"Expected average time between differences to be within range of [{low}, {high}] "
                    f"seconds but was {average_diff:0.3f}"
                )

        for attr in self.stat_attribute_names:
            # ignore clipped samples as they may not update
            if "NumClippedSamples" in attr:
                continue

            try:
                _assert_attribute(attr)
            except AssertionError as e:
                self.logger.exception(f"Assertion error raised for attribute {attr}", exc_info=True)
                errors.append(e)

        assert len(errors) == 0, (
            f"Expected all scalar attributes to be updated in range of {low} to {high} seconds. "
            "See logs for details."
        )

    def assert_stat_quality_statistics_written(self: PstBeamWorld, period_range: Tuple[float, float]) -> None:
        """Assert that STAT HDF5 is written periodically within a given time range."""
        (low, high) = period_range

        assert self.udp_data_generator is not None, "Expected UDP Data Generator to exist"
        assert (
            self._stat_file_watcher is not None
        ), f"Expected STAT file watcher to be watching {self.local_scan_output_path} for files"

        self.udp_data_generator.wait_for_end_of_data()

        # sleep until last period range
        time.sleep(high)

        event_time_diffs = self._stat_file_watcher.event_time_diffs()
        assert len(event_time_diffs) > 0, "Expected as least 1 event time difference."

        total_diff = 0.0
        for evt in event_time_diffs:
            diff = evt.creation_time_difference
            total_diff += diff
            assert _in_range(diff, low=low, high=high), (
                f"Expected time difference between files {evt.first_file_path.name} and "
                f"{evt.second_file_path.name} to between [{low}, {high}] "
                f"seconds, but was {evt.creation_time_difference:0.3f} seconds."
            )

        average_diff = total_diff / len(event_time_diffs)
        assert _in_range(average_diff, low=low, high=high, rtol=0.01), (
            f"Expected average time difference between files to between [{low}, {high}] "
            f"seconds, but was {average_diff:0.3f} seconds."
        )

    def assert_frequency_average_statistics_are_within_tolerance(
        self: PstBeamWorld,
        data_dimensions: pd.DataFrame,
        tolerance: float,
        num_samples_per_channel: int,
    ) -> None:
        """Assert that the frequency averaged mean and variance of all polarisations and dimensions.

        This method loops over all the given polarisations and dimensions and asserts the statistics
        are within a given tolerance.

        :param data_dimensions: a data frame defining which polarisation and voltage dimensions to validate.
        :type data_dimensions: pd.DataFrame
        :param tolerance: the number of stddev to accept from population statistics
        :type tolerance: float
        :param num_samples_per_channel: the expected number of samples per channel
        :type num_samples: int
        """
        assert self.udp_data_generator is not None
        assert self.generator_params is not None and isinstance(self.generator_params, GaussianNoiseConfig)
        assert self.curr_scan_configuration is not None, "Should have a scan configuration"

        self.udp_data_generator.wait_for_end_of_data()

        population_mean = self.generator_params.normal_dist_mean
        population_var = self.generator_params.normal_dist_stddev**2.0

        nchan = self.curr_scan_configuration["pst"]["scan"]["num_frequency_channels"]
        num_samples = nchan * num_samples_per_channel

        def _assert(pol: str, dim: str) -> None:
            mean_data_attr = f"{dim.lower()}Pol{pol}MeanFreqAvg"
            mean_data = [
                float(v.value)
                for v in self.attributes_monitor.get_attribute_history_events(attribute_name=mean_data_attr)[
                    1:
                ]
            ]

            var_data_attr = f"{dim.lower()}Pol{pol}VarianceFreqAvg"
            var_data = [
                float(v.value)
                for v in self.attributes_monitor.get_attribute_history_events(attribute_name=var_data_attr)[
                    1:
                ]
            ]

            self.logger.debug(
                f"Asserting history for {mean_data_attr} and {var_data_attr} to be in {tolerance} sigma"
            )

            ipol = 0 if pol == "A" else 1
            for mean, var in zip(mean_data, var_data):
                sample_stats = SampleStatistics(mean=mean, variance=var, num_samples=num_samples)
                assert_statistics(
                    population_mean=population_mean,
                    population_var=population_var,
                    sample_stats=sample_stats,
                    channel=0,
                    pol=ipol,
                    tolerance=tolerance,
                )

        for pol, dim in data_dimensions[["pol", "dimension"]].itertuples(index=False):
            _assert(pol=pol, dim=dim)

    def assert_channel_statistics_are_within_tolerance(
        self: PstBeamWorld,
        data_dimensions: pd.DataFrame,
        tolerance: float,
    ) -> None:
        """Assert the channel statistics are within a given tolerance.

        This method loops over all the given polarisations and dimensions and asserts the statistics
        for each channel is within tolerance.

        :param data_dimensions: a data frame defining which polarisation and voltage dimensions to validate.
        :type data_dimensions: pd.DataFrame
        :param tolerance: the number of stddev to accept from population statistics
        :type tolerance: float
        """
        assert self.udp_data_generator is not None
        assert self.generator_params is not None and isinstance(self.generator_params, GaussianNoiseConfig)
        assert self._stat_file_watcher is not None
        self.udp_data_generator.wait_for_end_of_data()

        population_mean = self.generator_params.normal_dist_mean
        population_var = self.generator_params.normal_dist_stddev**2.0

        for evt in self._stat_file_watcher.events:
            self.logger.info(f"Asserting channel stats for {evt.file_path}")
            stats = Statistics.load_from_file(evt.file_path)
            channel_stats = stats.get_channel_stats()
            channel_samples = stats.metadata.num_samples_spectrum

            for pol, dim in data_dimensions.itertuples(index=False):
                self.logger.info(f"Asserting channel stats for pol={pol}, dim={dim}")
                channel_data: pd.DataFrame = channel_stats.loc[:, pol, dim]  # type: ignore
                channel_data["Num. Samples"] = channel_samples

                assert_statistics_for_channels(
                    channel_data=channel_data,
                    population_mean=population_mean,
                    population_var=population_var,
                    pol=pol,
                    tolerance=tolerance,
                )

    def assert_pst_processing_mode(self: PstBeamWorld, processing_mode: str) -> None:
        """
        Assert that the PST beam is in a given processing mode.

        If the processing mode is None then this assumes that there is no current observation
        mode set for the PST beam.
        """
        current_processing_mode: str = self.beam_proxy.processingMode

        if processing_mode == "" or processing_mode == "undefined":
            assert current_processing_mode == "", (
                "expected PST not to be configured for observation. Current processing mode = ",
                processing_mode,
            )
        else:
            # format the processing mode to be screaming snake case
            processing_mode = processing_mode.upper().replace(" ", "_")
            assert current_processing_mode == processing_mode, (
                f"expected PST's processing mode to be {processing_mode} "
                f"but is currently in {current_processing_mode}"
            )

    def assert_mode(self: PstBeamWorld, mode: str, expected_value: Any) -> None:
        """
        Assert that the given mode has the expected value.

        This method can be used for any of the control modes:

            * Test Mode
            * Admin Mode

        :param mode: the name of the mode to test
        :type mode: str
        :param expected_value: the value that the mode should have
        :type expected_value: Any
        """
        mode = f"{mode}Mode"
        actual_value = getattr(self.beam_proxy, mode)
        mode_type = type(actual_value)

        # bit of a hack to convert to enum as a TANGO device proxy only returns the int value
        if type(actual_value) is not mode_type:
            actual_value = mode_type(actual_value)

        assert (
            actual_value == expected_value
        ), f"expected {mode} value to be {expected_value.name} but was {actual_value.name}"

    def assert_reconfigured_within(self: PstBeamWorld, expected_timediff: timedelta) -> None:
        """Assert the time between last end scan and back to a scanning state."""
        actual_timediff = self._start_scanning_time - self._end_scan_request_time
        assert actual_timediff <= expected_timediff, (
            f"expected reconfiguring to happen within {expected_timediff} "
            f"but it happened in {actual_timediff}"
        )

    def assert_operational_state(self: PstBeamWorld, expected_operation_state: str) -> None:
        """Assert that the PST BEAM is in a given operational state."""
        actual_operation_state = self.beam_proxy.state()
        if expected_operation_state == "on":
            assert (
                actual_operation_state == DevState.ON
            ), f"expected operational state to be ON but was {actual_operation_state}"
        if expected_operation_state == "fault":
            assert (
                actual_operation_state == DevState.FAULT
            ), f"expected operational state to be FAULT but was {actual_operation_state}"

    def assert_flow_through_files_written(self: PstBeamWorld) -> None:
        """Assert that the DSP.FT files have been written."""
        self.logger.warning("In assert_flow_through_files_written")
        self.wait_for_scan_to_complete()
        assert (
            len(self.local_scan_data_files) > 0
        ), "expected at least 1 flow through data file to be generated."
        assert (
            len(self.local_scan_weights_files) == 0
        ), "expected at no flow through weights files to be generated."

    def assert_flow_through_files(
        self: PstBeamWorld,
        nbit_out: int | None = None,
        polarizations_out: str | None = None,
        channels_out: Tuple[int, int] | None = None,
    ) -> None:
        """Assert the data and header of individual DSP.FT files.

        :param nbit_out: the expected NBIT out value, defaults to None
        :type nbit_out: int | None, optional
        :param polarizations_out: the expected polarizations out, defaults to None
        :type polarizations_out: str | None, optional
        :param channels_out: the expected output channels, defaults to None
        :type channels_out: Tuple[int, int] | None, optional
        """
        unpacker = SkaUnpacker()

        def _assert_file(file: DadaFile) -> None:
            self.logger.debug(f"Asserting file {file._file.name}")
            header = file.header

            def _assert_value(header_key: str, value: int | str) -> None:
                self.logger.info(f"Asserting file={file._file} has {header_key=} with {value=}")

                header_value: int | str
                if isinstance(value, int):
                    header_value = header.get_int(header_key)
                else:
                    header_value = header.get_value(header_key)

                assert (
                    header_value == value
                ), f"expected {file._file} header to have {header_key}={value} but was {header_value}"

            # assert we have complex data
            _assert_value("NDIM", 2)

            if nbit_out is not None:
                _assert_value("NBIT", nbit_out)

            if polarizations_out is not None:
                npol_out = 2 if polarizations_out == "Both" else 1
                _assert_value("NPOL", npol_out)
                _assert_value("POLN_FT", polarizations_out)
            else:
                npol_out = header.get_int("NPOL")

            if channels_out is not None:
                (start_channel, end_channel) = channels_out
                # as the range is inclusive we need the +1 (if the range was [1,1] we want 1 channel)
                nchan_out = end_channel - start_channel + 1
                _assert_value("NCHAN", nchan_out)
                expected_chan_ft = f"{start_channel},{end_channel}"
                _assert_value("CHAN_FT", expected_chan_ft)
            else:
                nchan_out = header.get_int("NCHAN")

            # nbit_out may be None and can't set it as it affects the call
            # just get it from the header
            nbit = header.get_int("NBIT")
            unpack_options = UnpackOptions(
                nbit=nbit,
                nchan=nchan_out,
                ndim=2,
                npol=npol_out,
            )
            self.logger.debug(f"unpacking file={file._file}, {nbit=}, {nchan_out=}, {npol_out=}")
            unpacked_data = file.unpack_tfp(unpacker=unpacker, options=unpack_options)
            (_, f_nchan, f_npol) = unpacked_data.shape
            assert f_nchan == nchan_out, f"expected unpacked data to have nchan={nchan_out} but was {f_nchan}"
            assert f_npol == npol_out, f"expected unpacked data to have nchan={npol_out} but was {f_npol}"
            assert unpacked_data.dtype == np.complex64, "expected unpacked data to have been complex data"

            assert_statistics_for_digitised_data(data=unpacked_data, nbit=nbit)

        for f in self.local_scan_data_files:
            _assert_file(f)
