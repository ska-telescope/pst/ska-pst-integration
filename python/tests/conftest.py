# -*- coding: utf-8 -*-
#
# This file is part of the SKA PST project
#
# Distributed under the terms of the BSD 3-clause new license.
# See LICENSE for more info.

"""Pytest conftest.py to set up fixtures."""

from __future__ import annotations

import json
import logging
import pathlib
import random
import shutil
import string
from datetime import datetime
from typing import Callable, Dict, Generator, List, cast

import pytest
from pytest_bdd.parser import Feature, Scenario, Step
from ska_control_model import LoggingLevel, PstProcessingMode
from ska_pst.lmc.component.grpc_lmc_client import PstGrpcLmcClient
from ska_pst.lmc.device_proxy import DeviceProxyFactory, PstDeviceProxy
from ska_pst.testutils.common import pst_processing_mode_from_str
from ska_pst.testutils.dsp import DiskSpaceUtil
from ska_pst.testutils.scan_config import ScanConfigGenerator, ScanIdFactory
from ska_pst.testutils.tango import TangoChangeEventHelper, TangoDeviceCommandChecker
from ska_tango_testing.mock.tango import MockTangoEventCallbackGroup
from ska_telmodel.csp.version import CSP_SCAN_PREFIX
from ska_telmodel.pst.version import PST_CONFIGURE_PREFIX
from tango import DeviceProxy

from ska_pst.common import (
    CbfPstConfig,
    FrequencyBandConfig,
    ReceiverConfig,
    TelescopeConfig,
    TelescopeFacilityEnum,
    get_telescope_config,
)

LOG_LEVEL_MAP = {
    logging.CRITICAL: LoggingLevel.FATAL,
    logging.ERROR: LoggingLevel.ERROR,
    logging.WARNING: LoggingLevel.WARNING,
    logging.INFO: LoggingLevel.INFO,
    logging.DEBUG: LoggingLevel.DEBUG,
}


@pytest.fixture
def dpd_api_endpoint(pytestconfig: pytest.Config) -> str | None:
    """Get the data product dashboard API endoint.

    If no endpoint is set then the tests don't attempt to check against DPD for scans.
    """
    return pytestconfig.getoption("dpd_api_endpoint", default=None)


@pytest.fixture(scope="session")
def logging_level(pytestconfig: pytest.Config) -> int:
    """Get the logging level to use for tests."""
    logging_level = pytestconfig.getoption("loglevel")
    return logging.getLevelName(logging_level)


@pytest.fixture(scope="session")
def ska_logging_level(logging_level: int) -> LoggingLevel:
    """Get SKA Logging Level based on logging_level fixture."""
    return LOG_LEVEL_MAP[logging_level]


@pytest.fixture(scope="session")
def logger(logging_level: int) -> logging.Logger:
    """Get logger used within tests."""
    logger = logging.getLogger("TESTLOGGER")
    logger.setLevel(logging_level)

    formatter = logging.Formatter(
        "%(asctime)s [%(levelname)8s] [%(threadName)s] [%(filename)s:%(lineno)s] %(message)s"
    )
    stream_handler = logging.StreamHandler()
    stream_handler.setLevel(logging_level)

    stream_handler.setFormatter(formatter)

    logger.addHandler(stream_handler)
    return logger


@pytest.fixture(scope="session")
def default_command_timeout(pytestconfig: pytest.Config) -> float:
    """Get the default command timeout."""
    return pytestconfig.getoption("command_timeout", 60.0)


@pytest.fixture(scope="module")
def beam_id() -> int:
    """Get beam id, default is 1."""
    return 1


@pytest.fixture
def frequency_band(telescope: str) -> str:
    """Get a random frequency band."""
    if telescope == "SKALow":
        return "low"
    else:
        return random.choice(["1", "2", "3", "4", "5a", "5b"])


@pytest.fixture
def telescope(pytestconfig: pytest.Config) -> str:
    """Get name of telescope, default is SKALow."""
    telescope = pytestconfig.getoption("--telescope") or "SKALow"
    if telescope == "ska_low":
        telescope = "SKALow"
    elif telescope == "ska_mid":
        telescope = "SKAMid"

    return telescope


@pytest.fixture
def processing_mode(pytestconfig: pytest.Config) -> PstProcessingMode:
    """
    Get processing mode fixture value.

    The default for tests is 'VOLTAGE_RECORDER' and this will then define
    the what extra configurations are needed.  Tests can override this by
    putting override the fixture in the Python file or using a mark paramterized.
    """
    processing_mode = pytestconfig.getoption("--processing-mode") or "voltage_recorder"
    return PstProcessingMode[processing_mode.upper()]


@pytest.fixture
def telescope_facility(telescope: str) -> TelescopeFacilityEnum:
    """Get fixture for simulating which telescope facility to test for."""
    return TelescopeFacilityEnum.from_telescope(telescope)


@pytest.fixture
def telescope_config(telescope: str) -> TelescopeConfig:
    """Get the configuration for the currently defined telescope facility."""
    return get_telescope_config(telescope)


@pytest.fixture
def frequency_band_config(telescope_config: TelescopeConfig, frequency_band: str) -> FrequencyBandConfig:
    """Get the configuration for the telescope frequency band config."""
    return telescope_config.frequency_bands[frequency_band]


@pytest.fixture
def receiver_config(frequency_band_config: FrequencyBandConfig) -> ReceiverConfig:
    """Get the receiver configuration for the current frequency band."""
    return frequency_band_config.receiver_config


@pytest.fixture
def cbf_pst_config(frequency_band_config: FrequencyBandConfig) -> CbfPstConfig:
    """Get the CBF/PST configuration for the current frequency band."""
    return frequency_band_config.cbf_pst_config


@pytest.fixture
def subsystem_id(telescope_facility: TelescopeFacilityEnum) -> str:
    """Get the subsystem_id based on the telescope facility the test is for."""
    if telescope_facility == TelescopeFacilityEnum.Low:
        return "pst-low"
    else:
        return "pst-mid"


@pytest.fixture
def eb_id() -> str:
    """Return a valid execution block id for test config."""
    rand_char = random.choice(string.ascii_lowercase)
    rand1 = random.randint(0, 999)
    rand2 = random.randint(0, 99999)
    today_str = datetime.today().strftime("%Y%m%d")

    return f"eb-{rand_char}{rand1:03d}-{today_str}-{rand2:05d}"


@pytest.fixture
def beam_fqdn(telescope: str) -> str:
    """Get BEAM.MGMT FQDN."""
    fqdn = "pst/beam/01"
    if telescope == "SKALow":
        fqdn = f"low-{fqdn}"
    elif telescope == "SKAMid":
        fqdn = f"mid-{fqdn}"
    return fqdn


@pytest.fixture
def recv_endpoint() -> str:
    """Get gRPC endpoint for RECV.CORE."""
    return "test-ska-pst-core:28080"


@pytest.fixture
def smrb_endpoint() -> str:
    """Get gRPC endpoint for SMRB.RB."""
    return "test-ska-pst-core:28081"


@pytest.fixture
def dsp_disk_endpoint() -> str:
    """Get gRPC endpoint for DSP.DISK."""
    return "test-ska-pst-core:28082"


@pytest.fixture
def stat_endpoint() -> str:
    """Get gRPC endpoint for STAT.CORE."""
    return "test-ska-pst-core:28083"


@pytest.fixture
def dsp_ft_endpoint() -> str:
    """Get gRPC endpoint for DSP.FT."""
    return "test-ska-pst-core:28084"


@pytest.fixture
def beam_proxy(beam_fqdn: str) -> PstDeviceProxy:
    """Get a device proxy for BEAM.MGMT."""
    return DeviceProxyFactory.get_device(beam_fqdn)


@pytest.fixture
def recv_grpc_client(recv_endpoint: str, logger: logging.Logger) -> PstGrpcLmcClient:
    """Get a gRPC Client to RECV.CORE."""
    return PstGrpcLmcClient(client_id="recv_test_client", endpoint=recv_endpoint, logger=logger)


@pytest.fixture
def dsp_disk_grpc_client(dsp_disk_endpoint: str, logger: logging.Logger) -> PstGrpcLmcClient:
    """Get a gRPC Client to DSP.DISK."""
    return PstGrpcLmcClient(client_id="dsp_disk_test_client", endpoint=dsp_disk_endpoint, logger=logger)


@pytest.fixture
def dsp_ft_grpc_client(dsp_ft_endpoint: str, logger: logging.Logger) -> PstGrpcLmcClient:
    """Get a gRPC Client to DSP.FT."""
    return PstGrpcLmcClient(client_id="dsp_ft_test_client", endpoint=dsp_ft_endpoint, logger=logger)


@pytest.fixture
def smrb_grpc_client(smrb_endpoint: str, logger: logging.Logger) -> PstGrpcLmcClient:
    """Get a gRPC Client to SMRB.RB."""
    return PstGrpcLmcClient(client_id="smrb_test_client", endpoint=smrb_endpoint, logger=logger)


@pytest.fixture
def stat_grpc_client(stat_endpoint: str, logger: logging.Logger) -> PstGrpcLmcClient:
    """Get a gRPC Client to STAT.CORE."""
    return PstGrpcLmcClient(client_id="stat_test_client", endpoint=stat_endpoint, logger=logger)


@pytest.fixture
def scan_type() -> str:
    """Get scan type."""
    return "pst_scan_vr"


@pytest.fixture
def invalid_packet_flags() -> Dict[str, List[str]]:
    """Get default invalid flags for UDP generator."""
    flags = {
        "malformed": ["-e 15:MagicWord"],
        "incorrect packet size": ["-e 20:PacketSize"],
        "dropped": ["-e 15:Transmit"],
        "wrong scan id": ["-e 15:ScanID"],
        "wrong channel number": ["-e 20:ChannelNumber"],
        "no valid polarisation correction": [
            *[f"-i {10*idx+1}:JonesMatrices" for idx in range(10)],
            "-I JonesMatrices=0.01",
        ],
        "no valid station beam": [
            *[f"-i {10*idx+2}:StationBeam" for idx in range(10)],
            "-I StationBeam=0.01",
        ],
        "no valid pst beam": [*[f"-i {10*idx+4}:PstBeam" for idx in range(10)], "-I PstBeam=0.01"],
    }

    return {key: [v for arg in args for v in arg.split()] for (key, args) in flags.items()}


@pytest.fixture(scope="session")
def scan_id_factory() -> ScanIdFactory:
    """Generate a random scan_id to be used for scan requests."""
    return ScanIdFactory()


@pytest.fixture
def scan_id(scan_id_factory: ScanIdFactory) -> int:
    """Generate a scan id from scan id factory."""
    return scan_id_factory.generate_scan_id()


@pytest.fixture
def csp_version() -> str:
    """Get the CSP version for testing."""
    return "2.4"


@pytest.fixture
def csp_scan_interface(csp_version: str) -> str:
    """Get the interface value for CSP Scan."""
    return CSP_SCAN_PREFIX + csp_version


@pytest.fixture
def csp_config_scan_interface(csp_version: str) -> str:
    """Get the interface value for CSP Configure Scan."""
    return PST_CONFIGURE_PREFIX + csp_version


@pytest.fixture
def scan_request(scan_id: int, csp_scan_interface: str) -> dict:
    """Fixture for scan requests."""
    return {"interface": csp_scan_interface, "scan_id": scan_id}


@pytest.fixture
def scan_request_json(scan_request: dict) -> str:
    """Get scan request as JSON string."""
    return json.dumps(scan_request)


@pytest.fixture
def beam_attribute_names(stat_attribute_names: List[str]) -> List[str]:
    """Get list of beam change event attributes."""
    return [
        "dataReceiveRate",
        "dataReceived",
        "dataDropRate",
        "dataDropped",
        "misorderedPackets",
        "misorderedPacketRate",
        "malformedPackets",
        "malformedPacketRate",
        "misdirectedPackets",
        "misdirectedPacketRate",
        "checksumFailurePackets",
        "checksumFailurePacketRate",
        "timestampSyncErrorPackets",
        "timestampSyncErrorPacketRate",
        "seqNumberSyncErrorPackets",
        "seqNumberSyncErrorPacketRate",
        "noValidPolarisationCorrectionPackets",
        "noValidPolarisationCorrectionPacketRate",
        "noValidStationBeamPackets",
        "noValidStationBeamPacketRate",
        "noValidPstBeamPackets",
        "noValidPstBeamPacketRate",
        "dataRecordRate",
        "dataRecorded",
        "diskCapacity",
        "diskUsedBytes",
        "diskUsedPercentage",
        "availableDiskSpace",
        "availableRecordingTime",
        "ringBufferUtilisation",
        "channelBlockConfiguration",
        *stat_attribute_names,
    ]


@pytest.fixture()
def stat_attribute_names() -> List[str]:
    """Get list of STAT.MGMT change event attributes."""
    return [
        "realPolAMeanFreqAvg",
        "realPolAVarianceFreqAvg",
        "realPolANumClippedSamples",
        "imagPolAMeanFreqAvg",
        "imagPolAVarianceFreqAvg",
        "imagPolANumClippedSamples",
        "realPolAMeanFreqAvgRfiExcised",
        "realPolAVarianceFreqAvgRfiExcised",
        "realPolANumClippedSamplesRfiExcised",
        "imagPolAMeanFreqAvgRfiExcised",
        "imagPolAVarianceFreqAvgRfiExcised",
        "imagPolANumClippedSamplesRfiExcised",
        "realPolBMeanFreqAvg",
        "realPolBVarianceFreqAvg",
        "realPolBNumClippedSamples",
        "imagPolBMeanFreqAvg",
        "imagPolBVarianceFreqAvg",
        "imagPolBNumClippedSamples",
        "realPolBMeanFreqAvgRfiExcised",
        "realPolBVarianceFreqAvgRfiExcised",
        "realPolBNumClippedSamplesRfiExcised",
        "imagPolBMeanFreqAvgRfiExcised",
        "imagPolBVarianceFreqAvgRfiExcised",
        "imagPolBNumClippedSamplesRfiExcised",
    ]


@pytest.fixture()
def additional_change_events_callbacks() -> List[str]:
    """Return additional change event callbacks."""
    return []


@pytest.fixture
def change_event_callback_time() -> float:
    """Get timeout used for change event callbacks."""
    return 1.0


@pytest.fixture
def change_event_callbacks_factory(
    beam_attribute_names: List[str],
    additional_change_events_callbacks: List[str],
    change_event_callback_time: float,
) -> Callable[..., MockTangoEventCallbackGroup]:
    """Get factory to create instances of a `MockTangoEventCallbackGroup`."""

    def _factory() -> MockTangoEventCallbackGroup:
        return MockTangoEventCallbackGroup(
            "obsState",
            "healthState",
            "healthFailureMessage",
            *beam_attribute_names,
            *additional_change_events_callbacks,
            timeout=change_event_callback_time,
        )

    return _factory


@pytest.fixture()
def change_event_callbacks(
    change_event_callbacks_factory: Callable[..., MockTangoEventCallbackGroup],
) -> MockTangoEventCallbackGroup:
    """
    Return a dictionary of TANGO device change event callbacks with asynchrony support.

    :return: a collections.defaultdict that returns change event callbacks by name.
    """
    return change_event_callbacks_factory()


@pytest.fixture()
def tango_change_event_helper(
    device_under_test: PstDeviceProxy | DeviceProxy,
    change_event_callbacks: MockTangoEventCallbackGroup,
    logger: logging.Logger,
) -> TangoChangeEventHelper:
    """
    Return a helper to simplify subscription to the device under test with a callback.

    :param device_under_test: a proxy to the device under test
    :param change_event_callbacks: dictionary of callbacks with
        asynchrony support, specifically for receiving Tango device
        change events.
    """
    if isinstance(device_under_test, PstDeviceProxy):
        device_under_test = device_under_test.device

    return TangoChangeEventHelper(
        device_under_test=cast(DeviceProxy, device_under_test),
        change_event_callbacks=change_event_callbacks,
        logger=logger,
    )


@pytest.fixture
def tango_device_command_checker(
    tango_change_event_helper: TangoChangeEventHelper,
    change_event_callbacks: MockTangoEventCallbackGroup,
    logger: logging.Logger,
) -> TangoDeviceCommandChecker:
    """Fixture that returns a TangoChangeEventHelper."""
    return TangoDeviceCommandChecker(
        tango_change_event_helper=tango_change_event_helper,
        change_event_callbacks=change_event_callbacks,
        logger=logger,
    )


@pytest.fixture
def max_scan_length() -> float:
    """Get maximum scan length."""
    return 10.0


@pytest.fixture(scope="session")
def monitoring_polling_rate_secs() -> float:
    """Monitor polling rate of BEAM.MGMT in seconds."""
    return 2.0


@pytest.fixture(scope="session")
def monitoring_polling_rate_ms(monitoring_polling_rate_secs: float) -> int:
    """Monitor polling rate of BEAM.MGMT."""
    return int(monitoring_polling_rate_secs * 1000.0)


@pytest.fixture
def udp_gen_data_rate() -> float:
    """Get the default UDP gen data rate for tests, in bytes per second."""
    return 16_666_666.7


@pytest.fixture
def max_data_generated_size(
    sdp_mount: pathlib.Path,
    lfs_mount: pathlib.Path,
) -> int | None:
    """Get the maximum amount of data generated during a scan output, in bytes."""
    if sdp_mount.exists() and lfs_mount.exists():
        lfs_usage = shutil.disk_usage(lfs_mount)
        sdp_usage = shutil.disk_usage(sdp_mount)

        return int(0.95 * min(lfs_usage.free, sdp_usage.free))

    return None


@pytest.fixture
def scan_config_generator(
    telescope_facility: TelescopeFacilityEnum,
    frequency_band: str | None,
    beam_id: int,
    max_scan_length: float,
    processing_mode: PstProcessingMode | str,
    max_data_generated_size: int | None,
    logger: logging.Logger,
) -> ScanConfigGenerator:
    """Get an instance of a ScanConfigGenerator."""
    if isinstance(processing_mode, str):
        processing_mode = pst_processing_mode_from_str(processing_mode)

    frequency_band = frequency_band or "low"
    if frequency_band == "low":
        telescope_facility = TelescopeFacilityEnum.Low
        telescope = "SKALow"
    else:
        telescope_facility = TelescopeFacilityEnum.Mid
        telescope = "SKAMid"

    return ScanConfigGenerator(
        telescope=telescope,
        facility=telescope_facility,
        frequency_band=frequency_band,
        beam_id=beam_id,
        max_scan_length=max_scan_length,
        processing_mode=processing_mode,
        max_data_generated_size=max_data_generated_size,
        logger=logger,
    )


@pytest.fixture
def disk_space_util(
    lfs_mount: str | pathlib.Path,
    logger: logging.Logger,
) -> Generator[DiskSpaceUtil, None, None]:
    """Get DiskSpaceUtil generator fixture."""
    with DiskSpaceUtil(lfs_mount=pathlib.Path(lfs_mount), logger=logger) as util:
        yield util


@pytest.fixture(scope="session")
def lfs_mount(pytestconfig: pytest.Config) -> pathlib.Path:
    """Get the local filesystem mount shared between all PST pods."""
    return pathlib.Path(pytestconfig.getoption("lfs_mount", default="/mnt/lfs"))


@pytest.fixture(scope="session")
def sdp_mount(pytestconfig: pytest.Config) -> pathlib.Path:
    """Get the SDP product dashboard filesystem mount."""
    return pathlib.Path(pytestconfig.getoption("sdp_mount", default="/mnt/sdp"))


@pytest.fixture
def invalid_scan_configurations() -> dict:
    """Get configuration for invalid scans."""
    return {
        "num_frequency_channels not divisible by udp_nchan": {
            "override": {"num_frequency_channels": 434},
            "message": "NCHAN with value 434 failed validation: not a multiple of UDP_NCHAN",
        },
        "udp_nsamp not divisible by wt_nsamp": {
            "override": {"udp_nsamp": 50},
            "message": "UDP_NSAMP with value 50 failed validation: not a multiple of WT_NSAMP",
        },
        "oversampling_ratio not consistent with Band": {
            "override": {"oversampling_ratio": [6, 5]},
            "message": "OS_FACTOR with value 6/5 failed validation",
        },
        "bits_per_sample not consistent with Band": {
            "override": {"bits_per_sample": 84},
            "message": "NBIT with value 42 failed validation",
        },
    }


def pytest_addoption(parser: pytest.Parser) -> None:
    """Add options for PyTest command line."""
    parser.addoption(
        "--loglevel",
        action="store",
        default="INFO",
        type=str,
        help="set internal logging level",
    )
    parser.addoption(
        "--command-timeout",
        action="store",
        default=60.0,
        type=float,
        help="default timeout for TANGO commands",
    )
    parser.addoption(
        "--lfs-mount",
        action="store",
        default="/mnt/lfs",
        type=str,
        help="directory where to find shared local filesystem data",
    )
    parser.addoption(
        "--sdp-mount",
        action="store",
        default="/mnt/sdp",
        type=str,
        help="directory where to find SDP shared storage data",
    )
    parser.addoption(
        "--dpd-api-endpoint",
        action="store",
        default=None,
        type=str,
        help="The DPD API endpoint to use.",
    )
    parser.addoption(
        "--telescope",
        action="store",
        default=None,
        type=str,
        help="Which telescope the tests are for.",
    )
    parser.addoption(
        "--processing-mode",
        action="store",
        default=None,
        type=str,
        help="Which PST processing mode to use.",
    )


"""PyTest Hooks"""


def pytest_bdd_step_error(
    request: pytest.FixtureRequest,
    feature: Feature,
    scenario: Scenario,
    step: Step,
    step_func: Callable,
    step_func_args: dict,
    exception: Exception,
) -> None:
    """Log step error, including world state."""
    from tests.utils.pst_beam_world import PstBeamWorld

    logger = cast(logging.Logger, request.getfixturevalue("logger"))

    log_params = {
        "feature": feature.name,
        "scenario": scenario.name,
        "step": step.name,
    }
    logger.exception(f"Exception occurred in: {log_params}. Exception = {exception}")
    world = cast(PstBeamWorld, request.getfixturevalue("world"))
    world.log_core_apps_state()
    world.log_lmc_state()
    world.log_scan_configuration()


def pytest_collection_modifyitems(config: pytest.Config, items: List[pytest.Item]) -> None:
    """Modify selected tests depending on request options."""
    telescope = config.getoption("--telescope")
    processing_mode = config.getoption("--processing-mode")

    deselected = []
    kept = []

    for item in items:
        # Check markers explicitly set for the test item
        explicit_markers = {marker.name for marker in item.own_markers}

        # Handle telescope filtering
        if telescope and telescope not in explicit_markers:
            deselected.append(item)
            continue

        # Handle processing mode filtering
        if processing_mode and not item.get_closest_marker(processing_mode):
            deselected.append(item)
            continue

        # If the item passed all filters, keep it
        kept.append(item)

    if deselected:
        config.hook.pytest_deselected(items=deselected)
        items[:] = kept
