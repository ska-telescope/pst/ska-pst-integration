# -*- coding: utf-8 -*-
#
# This file is part of the SKA PST project
#
# Distributed under the terms of the BSD 3-clause new license.
# See LICENSE for more info.

"""Module to test the integration between the Tango devices and PST pipeline."""

from __future__ import annotations

import datetime
import logging
import pathlib
from typing import Any, Dict, Generator, List, Set, Tuple, Type

import astropy.units as u
import numpy as np
import pandas as pd
import pytest
from pytest_bdd import given, parsers, scenarios, then, when
from ska_control_model import (
    AdminMode,
    ControlMode,
    LoggingLevel,
    ObsState,
    PstProcessingMode,
    SimulationMode,
    TestMode,
)
from ska_pst.lmc.component.grpc_lmc_client import PstGrpcLmcClient
from ska_pst.testutils.common import convert_value_to_quantity, pst_processing_mode_from_str
from ska_pst.testutils.dsp import DiskSpaceUtil
from ska_pst.testutils.scan_config import ScanConfigGenerator, ScanIdFactory, generate_eb_id
from ska_pst.testutils.udp_gen import GaussianNoiseConfig, SineWaveConfig
from ska_tango_testing.mock.tango import MockTangoEventCallbackGroup
from tests.utils.pst_beam_world import OUTPUT_PATH_PATTERN, PstBeamWorld

from ska_pst.common import TelescopeFacilityEnum

"""PST fixture state initialisations."""


@pytest.fixture
def world(
    change_event_callbacks: MockTangoEventCallbackGroup,
    recv_grpc_client: PstGrpcLmcClient,
    smrb_grpc_client: PstGrpcLmcClient,
    dsp_disk_grpc_client: PstGrpcLmcClient,
    dsp_ft_grpc_client: PstGrpcLmcClient,
    stat_grpc_client: PstGrpcLmcClient,
    monitoring_polling_rate_secs: float,
    beam_attribute_names: List[str],
    stat_attribute_names: List[str],
    sdp_mount: pathlib.Path,
    lfs_mount: pathlib.Path,
    disk_space_util: DiskSpaceUtil,
    dpd_api_endpoint: str | None,
    logger: logging.Logger,
    default_command_timeout: float,
    ska_logging_level: LoggingLevel,
    beam_fqdn: str,
) -> Generator[PstBeamWorld, None, None]:
    """Get the PstBeamWorld fixture.

    This is the main context for the BDD tests. It is built up
    from other fixtures. This allows test steps to just use
    the the world as the abstraction over all the fixtures that
    are used in the testing.
    """
    world = PstBeamWorld(
        change_event_callbacks=change_event_callbacks,
        recv_grpc_client=recv_grpc_client,
        smrb_grpc_client=smrb_grpc_client,
        dsp_disk_grpc_client=dsp_disk_grpc_client,
        dsp_ft_grpc_client=dsp_ft_grpc_client,
        stat_grpc_client=stat_grpc_client,
        monitoring_polling_rate=monitoring_polling_rate_secs,
        beam_attribute_names=beam_attribute_names,
        stat_attribute_names=stat_attribute_names,
        output_path=sdp_mount,
        lfs_mount=lfs_mount,
        dpd_api_endpoint=dpd_api_endpoint,
        logger=logger,
        default_command_timeout=default_command_timeout,
        ska_logging_level=ska_logging_level,
        beam_fqdn=beam_fqdn,
    )
    try:
        yield world
    finally:
        import time

        # need to call clean up before cleaning up the world else
        # commands in world can fail.
        disk_space_util.cleanup()
        # ensure monitoring polling rate
        time.sleep(2 * monitoring_polling_rate_secs)
        world.teardown()


"""PST scenarios."""

# test all features
scenarios("features")

# individual features to allow for specific feature testing. Uncomment out specific feature file

# scenarios("features/single-happy-path.feature")
# scenarios("features/pst-can-perform-multiple-scans.feature")
# scenarios("features/obs-state-model-command-sequence.feature")
# scenarios("features/pst-can-abort.feature")
# scenarios("features/scan-receives-invalid-packets.feature")
# scenarios("features/pst-writes-valid-output-data-products-to-configured-storage.feature")
# scenarios("features/pst-writes-output-data-to-correct-path.feature")
# scenarios("features/pst-reports-on-low-disk-space.feature")
# scenarios("features/pst-validates-scan-configuration.feature")
# scenarios("features/pst-generates-valid-channel-block-configuration.feature")
# scenarios("features/pst-computes-and-reports-data-quality-statistics.feature")
# scenarios("features/pst-correctly-reports-writeable-modes.feature")
# scenarios("features/pst-correctly-reports-mode-and-state-transitions.feature")
# scenarios("features/pst-flow-through-mode.feature")

"""PST givens."""


@given("PST is configured, deployed and running")
def pst_is_configured_deployed_and_running(world: PstBeamWorld) -> None:
    """Ensure that PST has been configured and deployed.

    This will ensure that `world.setup()` is called.
    """
    world.setup()


@given(parsers.parse("PST is configured to report on data quality every {rate_str}"))
def pst_is_configured_to_report_on_data_quality(world: PstBeamWorld, rate_str: str) -> None:
    """Ensure that PST reports on data quality rate."""
    rate = convert_value_to_quantity(rate_str)
    assert isinstance(rate, u.Quantity)
    assert rate.unit.is_equivalent(u.s)

    world.monitoring_polling_rate = float(rate.si.value)


@given('is configured with sub-system identifier "sub_id"')
def pst_is_configure_with_subsystem_identifier(world: PstBeamWorld) -> None:
    """Ensure the PST has a subsystem id."""
    assert world.subsystem_id is not None, "Expected PST to have a subsystem id"


# Note that this fixture is used in both a Given and When state
@given(parsers.parse("PST is configured to operate in {processing_mode} mode"))
@when(parsers.parse("PST is configured to operate in {processing_mode} mode"))
def pst_is_configured_to_operate_in_mode_str(
    scan_config_generator: ScanConfigGenerator, processing_mode: str
) -> None:
    """Ensure that PST is configured for a given processing mode."""
    scan_config_generator.processing_mode = pst_processing_mode_from_str(processing_mode)


@given(
    parsers.re(
        r"PST is configured to observe the (?P<facility>Low|Mid) Band(?:\s?(?P<frequency_band>[1-4]|5[ab]))?"
    ),
    target_fixture="frequency_band",
)
def pst_is_configured_to_observe_a_band(
    facility: str,
    frequency_band: str | None,
    scan_config_generator: ScanConfigGenerator,
) -> str:
    """Ensure that PST is configured for a given telescope facility and frequency band."""
    if facility.lower() == "low":
        frequency_band = "low"
    else:
        assert facility.lower() == "mid"
        assert frequency_band is not None and len(frequency_band) > 0

    scan_config_generator.frequency_band = frequency_band

    facility_enum = TelescopeFacilityEnum[facility]
    scan_config_generator.facility = facility_enum
    return frequency_band


@given(parsers.re(r"^(?!PST)(?P<configuration_name>.*) is configured to (?!operate\s+)(?P<value_str>.*?)?$"))
def pst_override_configuration_value(
    scan_config_generator: ScanConfigGenerator,
    configuration_name: str,
    value_str: Any,
    logger: logging.Logger,
) -> None:
    """Override a configuration value."""
    # need convert configuration name to scan config key
    # anything complex we could use a dictionary to map to the correct value
    # if the unit is not None we may need to change if we need to scale (i.e. GHz would
    # need to multiply by 1e9

    key = configuration_name.replace(" ", "_").lower()

    value = convert_value_to_quantity(value_str)
    logger.debug(f"Converted {value_str} to {value}")

    if isinstance(value, u.Quantity):
        scan_config_generator.override_config(key, value.si.value)
    else:
        scan_config_generator.override_config(key, value_str)


@given(parsers.parse("PST is in the {given_state} state"))
@given(parsers.parse('PST is in the {given_state} state with scan identifier "scan_id"'))
def pst_in_the_given_state(
    world: PstBeamWorld,
    given_state: str,
    scan_config_generator: ScanConfigGenerator,
    scan_id_factory: ScanIdFactory,
    udp_gen_data_rate: float,
) -> None:
    """Ensure that PST is in a given ObsState.

    This will call make sure that the PST.BEAM is in a given
    state and will fast forward with sensible defaults, such
    as if it should be in a "Scanning" state a default
    scan configuration will be used.
    """
    if given_state == "Idle":
        world.ensure_initial_state(state=ObsState.IDLE)
    elif given_state in ["Scan Configured", "Ready"]:
        overrides = world.scan_config_overrides
        configure_scan_request = scan_config_generator.generate(
            eb_id=world.eb_id,
            overrides=overrides,
            max_data_rate=udp_gen_data_rate,
        )
        world.ensure_initial_state(state=ObsState.READY, configure_scan_request=configure_scan_request)
    elif given_state == "Scanning":
        overrides = world.scan_config_overrides
        configure_scan_request = scan_config_generator.generate(
            eb_id=world.eb_id, overrides=overrides, max_data_rate=udp_gen_data_rate
        )
        world.ensure_initial_state(
            state=ObsState.SCANNING,
            configure_scan_request=configure_scan_request,
            scan_id=scan_id_factory.generate_scan_id(),
        )
    elif given_state == "Aborted":
        world.ensure_initial_state(state=ObsState.ABORTED)
    elif given_state == "Fault":
        world.ensure_initial_state(state=ObsState.FAULT)
    else:
        raise ValueError(f"'{given_state}' is not a valid initial state to test")


@given("PST is receiving valid data")
def pst_is_receiving_valid_data(
    world: PstBeamWorld,
    scan_config_generator: ScanConfigGenerator,
) -> None:
    """Ensure that PST is receiving data."""
    calculated_resources = scan_config_generator.calculate_udp_gen_resources()
    world.send_data(
        calculated_resources=calculated_resources,
        data_generator="GaussianNoise",
        generator_params=GaussianNoiseConfig(),
    )
    world.wait_for_monitoring_data()


@given("PST is recording monitoring data")
def pst_is_recording_monitoring_data(world: PstBeamWorld) -> None:
    """Ensure PST has received monitoring data."""
    world.assert_has_received_monitoring_data()


@given("PST has not received any data")
def pst_has_not_received_any_data(world: PstBeamWorld) -> None:
    """Do not send any data via UDP Generator.

    This is a fixture more for context of a scenario. We don't do anything
    to the system to NOT send data.
    """
    world.assert_data_not_received()


@given("PST is online")
def pst_is_online(world: PstBeamWorld) -> None:
    """Ensure that the PST is online."""
    world.online()


@given('PST is configured with execution block identifier "eb_id"')
def pst_is_configure_with_eb_id(world: PstBeamWorld) -> None:
    """Ensure that the scan will have an eb_id."""
    world.eb_id = generate_eb_id()


@given(
    parsers.parse("PST is configured to compute statistics from Nt time samples:"),
    target_fixture="num_samples_per_channel",
)
def pst_is_configured_to_compute_statistics_for_n_time_samples(
    world: PstBeamWorld, telescope: str, frequency_band: str, datatable: List[List[Any]]
) -> int:
    """Ensure number of samples per channel is defined by telescope and frequency band.

    This is also a fixture `num_samples_per_channel` which is driven by the data table.
    """
    nt_table = convert_to_dataframe(datatable)
    idx = (nt_table["Telescope"] == telescope) & (nt_table["Band"] == frequency_band)
    nt_samples = nt_table.loc[idx]["Nt"].values
    assert (
        len(nt_samples) == 1
    ), f"Expected a value Nt value for telescope={telescope}, frequency_band={frequency_band}"

    return int(nt_samples[0])


@given(
    parsers.parse("PST is commanded to set {mode_str} mode to {value_str}"),
)
@given(parsers.parse("PST {mode_str} mode is {value_str}"))
@when(parsers.parse("PST {mode_str} mode changes to {value_str}"))
def pst_is_commanded_to_set_to_be_in_mode_str(
    world: PstBeamWorld,
    mode_str: str,
    value_str: str,
) -> None:
    """Set the PST BEAM with a given mode and value."""
    value: Any = get_mode_value(mode_str, value_str)
    mode_str = f"{mode_str}Mode"
    if mode_str == "adminMode" and value == AdminMode.NOT_FITTED:
        # need to ensure we're not in ONLINE mode
        world.beam_proxy.adminMode = AdminMode.OFFLINE

    setattr(world.beam_proxy, mode_str, value)


@given(parsers.parse("PST operational state is {state_str}"))
def pst_is_in_initial_operational_state(
    world: PstBeamWorld,
    state_str: str,
) -> None:
    """Set the PST BEAM to a given initial operational state."""
    if state_str == "off":
        world.off()
    elif state_str == "on":
        world.on()


@given(parsers.parse("PST is configured to write {nbit_out_str} bits per sample"), target_fixture="nbit_out")
def pst_is_configured_to_write_nbit_out_per_sample(
    nbit_out_str: str, scan_config_generator: ScanConfigGenerator
) -> int:
    """Set the NBIT_OUT."""
    nbit_out = int(nbit_out_str)
    assert (
        scan_config_generator.processing_mode == PstProcessingMode.FLOW_THROUGH
    ), "currently only support setting nbit_out for FLOW_THROUGH mode"
    scan_config_generator.add_processing_mode_config_value("nbit_out", nbit_out)
    return nbit_out


@given(
    parsers.parse("PST is configured to write {polarizations_out} polarizations"),
    target_fixture="polarizations_out",
)
def pst_is_configured_to_write_polarizations_out(
    polarizations_out: str, scan_config_generator: ScanConfigGenerator
) -> str:
    """Set which polarizations to record during flow through mode."""
    assert (
        scan_config_generator.processing_mode == PstProcessingMode.FLOW_THROUGH
    ), "currently only support setting polarizations_out for FLOW_THROUGH mode"
    scan_config_generator.add_processing_mode_config_value("polarizations_out", polarizations_out)
    return polarizations_out


@given(
    parsers.parse("PST is configured to write {start_channels_out}-{end_channels_out} frequency channels"),
    target_fixture="channels_out",
)
def pst_is_configured_to_write_frequency_channels_out(
    start_channels_out: str, end_channels_out: str, scan_config_generator: ScanConfigGenerator
) -> Tuple[int, int]:
    """Set which frequency channels to record during flow through mode."""
    assert (
        scan_config_generator.processing_mode == PstProcessingMode.FLOW_THROUGH
    ), "currently only support setting channels_out for FLOW_THROUGH mode"

    channels_out = (int(start_channels_out), int(end_channels_out))
    scan_config_generator.add_processing_mode_config_value("channels_out", channels_out)
    return channels_out


"""PST whens."""


@when("PST receives a valid scan configuration request")
@when("PST receives a configure scan request")
def pst_receives_a_configure_scan_request(
    world: PstBeamWorld, scan_config_generator: ScanConfigGenerator, udp_gen_data_rate: float
) -> None:
    """Configure PST beam with a valid scan configuration."""
    world.configure_scan(
        configure_scan_request=scan_config_generator.generate(max_data_rate=udp_gen_data_rate)
    )


@when("PST receives a start scan request")
@when("PST receives a scan request")
def pst_receives_a_start_scan_request(
    world: PstBeamWorld,
    scan_id_factory: ScanIdFactory,
) -> None:
    """Update PST to start scanning."""
    world.start_scan(scan_id=scan_id_factory.generate_scan_id())


@when("PST receives a end scan request")
@when("PST receives a stop scan request")
@when("PST receives a stop scan request at time T")
def pst_receives_an_end_scan_request(
    world: PstBeamWorld,
) -> None:
    """Update PST to end its current scan request."""
    world.end_scan()


@when("PST receives a abort request")
def pst_receives_a_abort_request(
    world: PstBeamWorld,
) -> None:
    """Send an abort commands request to PST."""
    world.abort()


@when("PST receives a go to idle request")
@when("PST receives a deconfigure scan request")
def pst_receives_a_go_to_idle_request(
    world: PstBeamWorld,
) -> None:
    """Put PST into an Idle state."""
    world.goto_idle()


@when("PST receives a obs reset request")
def pst_receives_a_obsreset_request(
    world: PstBeamWorld,
) -> None:
    """Send an ObsReset command to PST."""
    world.obs_reset()


@when("PST receives a reset request")
def pst_receives_a_reset_request(
    world: PstBeamWorld,
) -> None:
    """Send an Reset command to PST."""
    world.reset()


@when("valid data is sent to PST")
@when("data are sent to PST")
def valid_data_is_sent_to_pst(
    world: PstBeamWorld,
    scan_config_generator: ScanConfigGenerator,
) -> None:
    """Start sending UDP data to PST."""
    nbit = scan_config_generator.frequency_band_config.cbf_pst_config.nbit
    stddev = 1106.4 if nbit == 16 else 10.1
    mean = 0.0

    calculated_resources = scan_config_generator.calculate_udp_gen_resources()
    world.send_data(
        calculated_resources=calculated_resources,
        data_generator="GaussianNoise",
        generator_params=GaussianNoiseConfig(
            normal_dist_mean=mean,
            normal_dist_stddev=stddev,
        ),
    )
    world.wait_for_monitoring_data()


@when(parsers.re(r"data are sent to PST (with known mean and variance )?for (?P<scan_length_str>.*)"))
def valid_data_is_sent_to_pst_for_length_of_time(
    world: PstBeamWorld,
    scan_config_generator: ScanConfigGenerator,
    scan_length_str: str,
) -> None:
    """Start sending UDP data to PST for a given length of time."""
    scan_length = convert_value_to_quantity(scan_length_str)
    assert isinstance(scan_length, u.Quantity)

    # generate random mean and stddev
    stddev = 9.0 + 2.0 * np.random.random()
    mean = np.random.random() - 0.5

    calculated_resources = scan_config_generator.calculate_udp_gen_resources()
    world.send_data(
        calculated_resources=calculated_resources,
        scanlen=scan_length.si.value,
        data_generator="GaussianNoise",
        generator_params=GaussianNoiseConfig(
            normal_dist_mean=mean,
            normal_dist_stddev=stddev,
        ),
    )
    world.wait_for_monitoring_data()


@when(parsers.parse("data containing a {frequency_str} sine wave is sent to PST"))
def data_containing_a_sine_wave_is_sent_to_pst(
    world: PstBeamWorld,
    frequency_str: str,
    scan_config_generator: ScanConfigGenerator,
) -> None:
    """Send sine wave data to PST."""
    payload = world.curr_scan_configuration
    assert payload is not None, "Beam should have been configured, but current scan configuration is None"

    frequency = convert_value_to_quantity(frequency_str)
    assert (
        isinstance(frequency, u.Quantity) and frequency.si.unit == u.Hz
    ), f"Expected {frequency_str} to be a frequency value"
    frequency = frequency.to(u.MHz)

    calculated_resources = scan_config_generator.calculate_udp_gen_resources(overrides=payload)
    world.send_data(
        calculated_resources=calculated_resources,
        data_generator="Sine",
        generator_params=SineWaveConfig(sinusoid_freq=frequency.value),
    )
    world.wait_for_monitoring_data()


@when("data is no longer sent to PST")
def data_is_no_longer_sent_to_pst(world: PstBeamWorld) -> None:
    """Wait until data is no longer sent to PST."""
    world.wait_for_monitoring_data_to_stop()


@when("PST receives a start scan request with a different scan ID")
def pst_receives_a_start_scan_request_with_a_different_scan_id(
    world: PstBeamWorld,
    scan_id_factory: ScanIdFactory,
) -> None:
    """Send a start scan request but with a different scan id than before."""
    # this is a hack. perhaps scan_id_factory has previous scan ids it knows about.
    scan_id = scan_id_factory.generate_scan_id()
    world.start_scan(scan_id=scan_id)


@when("PST receives a different valid scan configuration request")
def pst_receives_a_different_valid_scan_configuration_request(
    world: PstBeamWorld, scan_config_generator: ScanConfigGenerator, udp_gen_data_rate: float
) -> None:
    """Send a configure scan with a different scan configuration."""
    world.configure_scan(
        configure_scan_request=scan_config_generator.generate(max_data_rate=udp_gen_data_rate)
    )


@when("PST receives an abort request")
def pst_receives_an_abort_request(world: PstBeamWorld) -> None:
    """Send an abort request."""
    world.abort()


@when(parsers.parse("PST receives an {invalid_packet_type} packet"))
def pst_receives_an_invalid_packet(
    world: PstBeamWorld,
    invalid_packet_type: str,
    invalid_packet_flags: Dict[str, List[str]],
    scan_config_generator: ScanConfigGenerator,
) -> None:
    """Send UDP data with an invalid or dropped packet."""
    calculated_resources = scan_config_generator.calculate_udp_gen_resources()

    udpgen_extra_args = invalid_packet_flags[invalid_packet_type]
    world.send_data(
        calculated_resources=calculated_resources,
        udpgen_extra_args=udpgen_extra_args,
        data_generator="GaussianNoise",
        generator_params=GaussianNoiseConfig(),
    )


@when(parsers.re(r"PST performs a scan for (?P<scan_length_str>.*)"))
def pst_performs_a_scan_for_given_length(
    world: PstBeamWorld,
    scan_length_str: str,
    scan_id_factory: ScanIdFactory,
    scan_config_generator: ScanConfigGenerator,
) -> None:
    """Simulate a scan of a given length of time.

    This will start a scan, then send UDP data for the
    scan length, wait for the UDP data generator to
    end and then perform an end scan.
    """
    scan_length_value = convert_value_to_quantity(scan_length_str)
    assert isinstance(scan_length_value, u.Quantity)
    world.start_scan(scan_id=scan_id_factory.generate_scan_id())

    calculated_resources = scan_config_generator.calculate_udp_gen_resources()
    world.send_data(
        calculated_resources=calculated_resources,
        scanlen=scan_length_value.si.value,
        data_generator="GaussianNoise",
        generator_params=GaussianNoiseConfig(),
    )
    world.wait_for_scan_to_complete()
    world.end_scan()
    world.wait_for_send_to_complete()


@when("scan completes successfully")
def pst_scan_completes_successfully(world: PstBeamWorld) -> None:
    """Perform an end scan operation."""
    world.wait_for_scan_to_complete()
    world.end_scan()
    world.wait_for_send_to_complete()


@when(parsers.parse("PST receives an invalid scan configuration request with {invalid_parameter}"))
def pst_receives_an_invalid_configure_scan_request(
    world: PstBeamWorld,
    scan_config_generator: ScanConfigGenerator,
    invalid_parameter: str,
    invalid_scan_configurations: dict,
    udp_gen_data_rate: float,
) -> None:
    """Send an invalid CSP JSON in a configure scan request."""
    invalidation = invalid_scan_configurations[invalid_parameter]["override"]
    world.configure_scan(
        configure_scan_request=scan_config_generator.generate(
            overrides=invalidation, max_data_rate=udp_gen_data_rate
        )
    )


@when(parsers.parse("the time remaining until storage fills falls below {remaining_time_str}"))
def recording_time_on_storage_falls_below_time(
    disk_space_util: DiskSpaceUtil,
    remaining_time_str: str,
    scan_config_generator: ScanConfigGenerator,
    logger: logging.Logger,
) -> None:
    """Fill the recording volume with data to force a drop in remaining recording time."""
    import math

    remaining_time = convert_value_to_quantity(remaining_time_str)
    assert isinstance(remaining_time, u.Quantity)
    # expect that we have a time quantity
    assert remaining_time.unit.is_equivalent(u.s)

    logger.debug(f"Ensuring less than {remaining_time_str} of disk space is available.")

    remaining_time_secs = remaining_time.si.value
    bytes_per_second = float(scan_config_generator.bytes_per_second)
    # include a fudge factor - as UDP gen may not send at the correct speed and test becomes flaky
    max_bytes_free = int(math.floor(bytes_per_second * remaining_time_secs / 1.1))

    logger.debug(f"Expect no more than {max_bytes_free} bytes free on {disk_space_util.lfs_mount}")

    disk_usage = disk_space_util.curr_disk_space()
    if disk_usage.free >= max_bytes_free:
        fill_bytes = disk_usage.free - max_bytes_free
        logger.debug(f"Need to fill {fill_bytes} bytes for test.")
        disk_space_util.create_tmp_file(fill_bytes=fill_bytes)


@when(parsers.parse("the disk storage fills above {percentage} used"))
def pst_disk_storage_usage_above_percentage(
    percentage: float,
    disk_space_util: DiskSpaceUtil,
    logger: logging.Logger,
) -> None:
    """Fill the recording volume with data to force disk space usage above a percentage."""
    import math

    percentage = float(percentage) / 100.0
    logger.debug(f"Ensuring {(percentage * 100.0):.1f} of disk is full")
    disk_usage = disk_space_util.curr_disk_space()
    curr_percentage_used = disk_usage.used / disk_usage.total
    logger.debug(f"Current disk usage is {disk_usage}, which is {(curr_percentage_used * 100.0):.1f}%")

    fill_percentage = max(percentage - curr_percentage_used, 0.0)
    if fill_percentage > 0.0:
        logger.debug(f"Filling {(fill_percentage * 100.0):.1f}% of the disk")
        fill_bytes = int(math.ceil(fill_percentage * disk_usage.total))
        logger.debug(f"Need to fill {fill_bytes} bytes for test.")
        disk_space_util.create_tmp_file(fill_bytes=fill_bytes)


@when(parsers.parse("PST {mode_str} mode is queried"))
def pst_when_mode_is_queried(world: PstBeamWorld, mode_str: str) -> None:
    """
    Query the current value of the mode.

    This fixture step does nothing as the Given as has already done the
    setting up and the THEN step will do the assertion.
    """


@when(parsers.parse("PST operational state changes to {state_str}"))
def pst_operational_state_changes_to_new_state(
    world: PstBeamWorld,
    state_str: str,
) -> None:
    """Set the PST BEAM to a given initial operational state."""
    if state_str == "on":
        world.on()
    elif state_str == "fault":
        world.goto_fault("force beam into fault state")


"""PST thens."""


@then(parsers.parse("PST transitions to the {expected_state} state"))
def pst_transitions_to_state(world: PstBeamWorld, expected_state: str) -> None:
    """Assert that PST has moved to an expected state."""
    if expected_state in ["Idle"]:
        world.assert_obstate(obs_state=ObsState.IDLE, sub_obs_state=ObsState.EMPTY)

    if expected_state in ["Scan Configured", "Ready"]:
        world.assert_obstate(obs_state=ObsState.READY)

    if expected_state in ["Scanning"]:
        world.assert_obstate(obs_state=ObsState.SCANNING)

    if expected_state in ["Aborted"]:
        try:
            world.assert_obstate(obs_state=ObsState.ABORTED)
        except AssertionError:
            world.assert_obstate(obs_state=ObsState.ABORTED, sub_obs_state=ObsState.EMPTY)

    if expected_state in ["Fault"]:
        world.assert_obstate(obs_state=ObsState.FAULT)


@then("PST writes expected data products to configured storage")
def pst_writes_expected_data_products_to_configured_storage(
    world: PstBeamWorld,
) -> None:
    """Assert that PST will write data files to expected storage location."""
    world.assert_data_files_exists()


@then(parsers.parse("PST writes valid {output_data} to configured storage"))
def pst_writes_valid_output_data_to_configured_storage(
    world: PstBeamWorld,
    output_data: str,
) -> None:
    """Assert that PST writes the correct type of files to configured storage."""
    if output_data == "beam-formed voltage data":
        data_type = "data"
    elif output_data == "data weights":
        data_type = "weights"
    elif world.curr_processing_mode == PstProcessingMode.FLOW_THROUGH:
        # NOTE - we currently don't support output data types for flow through
        # so just return rather than doing any asserts
        return
    else:
        raise ValueError(f"Unknown data type {output_data}")

    world.assert_data_files_exists(data_types=data_type)


@then(parsers.parse("{output_data} files contiguously span {scan_length_str}"))
def pst_outputs_contiguous_data_to_file_system(
    world: PstBeamWorld,
    scan_config_generator: ScanConfigGenerator,
    output_data: str,
    scan_length_str: str,
) -> None:
    """Assert that output data files are contiguous over the whole scan."""
    if output_data == "beam-formed voltage data":
        file_type = "data"
    elif output_data == "data weights":
        file_type = "weights"

    scan_length_value = convert_value_to_quantity(scan_length_str)
    assert isinstance(scan_length_value, u.Quantity)

    world.assert_contiguous_files(
        scanlen=scan_length_value.si.value,
        file_type=file_type,
        calculated_resources=scan_config_generator.calculate_udp_gen_resources(
            overrides=world.scan_config_overrides
        ),
    )


@then("PST reports monitoring data")
def pst_reports_monitoring_data(world: PstBeamWorld) -> None:
    """Assert that PST is reporting monitoring data."""
    world.assert_monitoring_data_updated()


@then("PST transitions to the Scan Configured state")
def pst_transitions_to_the_scan_configured_state(world: PstBeamWorld) -> None:
    """Assert PST transitioned to the Scan Configured state."""
    world.assert_obstate(obs_state=ObsState.READY)


@then("PST stops writing data products to configured storage")
def pst_stops_writing_data_products_to_configured_storage(world: PstBeamWorld) -> None:
    """Assert that PST stops writing data.

    This is currently not implemented. Potential implementation
    could be: check that there is is no process holding reference
    to the files, and after a given time (maybe 1s) that no new
    files or processes holding on to the files and the total
    file sizes for the scan was the same before.
    """


@then("PST stops reporting monitoring data")
def pst_stops_reporting_monitoring_data(
    world: PstBeamWorld,
) -> None:
    """Assert that the update of monitoring attributes stops."""
    world.assert_no_monitoring_attributes_updates()


@then("the command is rejected")
def the_command_is_rejected(
    world: PstBeamWorld,
) -> None:
    """Assert that the previous command was rejected."""
    world.assert_previous_command_rejected()


@then("PST returns a FAILED status code")
def pst_returns_failed_status_code(
    world: PstBeamWorld,
) -> None:
    """Assert that the previous command failed."""
    world.assert_previous_command_failed()


@then(parsers.parse("PST remains in the {initial_state} state"))
def pst_remains_in_the_state(world: PstBeamWorld, initial_state: str) -> None:
    """Assert that PST does not transition state."""
    # need to map str to ObsState
    if initial_state == "Idle":
        world.assert_obstate(obs_state=ObsState.IDLE, sub_obs_state=ObsState.EMPTY)
    elif initial_state == "Ready":
        world.assert_obstate(obs_state=ObsState.READY)
    elif initial_state == "Scanning":
        world.assert_obstate(obs_state=ObsState.SCANNING)
    elif initial_state == "Aborted":
        try:
            world.assert_obstate(obs_state=ObsState.ABORTED)
        except AssertionError:
            world.assert_obstate(obs_state=ObsState.ABORTED, sub_obs_state=ObsState.EMPTY)
    elif initial_state == "Fault":
        world.assert_obstate(obs_state=ObsState.FAULT)
    else:
        raise ValueError(f"unexpected state supplied '{initial_state}'")


@then(parsers.parse("PST increments the {monitored_property} packet counter"))
def pst_increments_the_invalid_packet_counter(
    world: PstBeamWorld,
    monitored_property: str,
) -> None:
    """Assert that the invalid packet monitoring value has incremented."""
    world.assert_invalid_packet(monitored_property=monitored_property)


CBF_VALIDITY_FLAG_PACKET_TYPES: Set[str] = {
    "no valid polarisation correction",
    "no valid station beam",
    "no valid pst beam",
}


@then(parsers.parse("PST flags the {invalid_packet_type} packet in {output_data} files"))
def pst_flags_invalid_packets_in_output_data_files(
    world: PstBeamWorld,
    invalid_packet_type: str,
    invalid_packet_flags: Dict[str, List[str]],
    output_data: str,
) -> None:
    """Assert the output data files records invalid packets correctly."""
    assert output_data == "weights", f"Unknown data type {output_data}"

    # infer the invalid packet number from the udpgen packet flags
    invalid_packet_flags[invalid_packet_type]
    invalid_packets = []

    # the args are in form of ["-flag", "value", "-flag", "value"]
    # need to ignore flags that are neither -e nor -i
    args_iter = iter(invalid_packet_flags[invalid_packet_type])

    for flag in args_iter:
        value = next(args_iter)
        if flag not in {"-e", "-i"}:
            continue

        [index, *_] = value.split(":")
        invalid_packets.append(int(index))

    if invalid_packet_type in CBF_VALIDITY_FLAG_PACKET_TYPES:
        world.assert_weights_zeroed_for_invalid_cbf_flag(invalid_packets=invalid_packets)
    else:
        world.assert_weights_has_dropped_packets(dropped_packets=invalid_packets)


@then(parsers.parse("a {frequency} MHz sine wave is detected in the {output_data}"))
def sine_wave_is_detected(world: PstBeamWorld, frequency: float) -> None:
    """Assert that a sine wave signal of known frequency is recorded in output data."""
    world.assert_sine_wave(sinusoid_frequency=float(frequency))


@then(
    parsers.re(
        (
            r"PST reports a (?P<message_type>\w+) that (?P<remaining_time_str>\d+ \w+) "
            r"remain(?:s?) until storage space will run out"
        )
    )
)
def pst_reports_time_remaining(world: PstBeamWorld, message_type: str, remaining_time_str: str) -> None:
    """Assert that PST monitoring will report remaining time."""
    remaining_time = convert_value_to_quantity(remaining_time_str)
    assert isinstance(remaining_time, u.Quantity)
    # expect that we have a time quantity
    assert remaining_time.unit.is_equivalent(u.s)

    remaining_time_secs = remaining_time.si.value
    world.assert_recording_time_remaining_less_than(remaining_time_secs)


@then("PST enters an ALARM state")
def pst_enters_an_alarm_state(world: PstBeamWorld) -> None:
    """Assert that PST is put into an ALARM state."""
    world.assert_in_alarm_state()


@then(
    parsers.re(
        (
            r"PST reports the (?P<pst_attribute>.*) is (?P<above_below>(:?above maximum)|(:?below minimum))"
            r" (?P<attribute_quality>.*) threshold"
        )
    )
)
def pst_reports_attribute_threshold_warning_and_alarms(
    world: PstBeamWorld,
    pst_attribute: str,
    above_below: str,
    attribute_quality: str,
) -> None:
    """Assert that a given monitoring property is in a warning or alarm state."""
    # this converts 'disk usage percentage' to 'diskUsagePercentage'
    init, *rest = pst_attribute.split(" ")
    attribute = "".join([init.lower(), *map(str.title, rest)])

    maximum = above_below == "above maximum"

    world.assert_attribute_quality(attribute=attribute, maximum=maximum, attribute_quality=attribute_quality)


@then(parsers.parse("PST reports an error message that describes {invalid_parameter}"))
def pst_reports_an_error_message_that_describes_invalid_parameter(
    world: PstBeamWorld,
    invalid_parameter: str,
    invalid_scan_configurations: dict,
) -> None:
    """Assert that invalid scan configuration will return the correct error message."""
    expected_message_regexp = invalid_scan_configurations[invalid_parameter]["message"]
    world.assert_previous_command_error_message_matches(expected_message_regexp)


@then("PST returns valid channel block configuration")
def pst_returns_valid_channel_block_configuration(
    world: PstBeamWorld,
) -> None:
    """Assert that the channel block configuration is correct given scan configuration."""
    world.assert_channel_block_configuration_valid()


@then("PST returns empty channel block configuration")
def pst_returns_empty_channel_block_configuration(
    world: PstBeamWorld,
) -> None:
    """Assert that there is no channel block configuration."""
    world.assert_channel_block_configuration_empty()


@then(parsers.parse('PST creates a subfolder of the shared storage mount named "{output_path_pattern}"'))
def pst_creates_output_folder(
    world: PstBeamWorld,
    output_path_pattern: str,
) -> None:
    """Assert that the output path is created."""
    assert (
        output_path_pattern == OUTPUT_PATH_PATTERN
    ), f"Expected {output_path_pattern} to equal {OUTPUT_PATH_PATTERN}"
    world.assert_output_path_exists()


@then(parsers.parse("""PST writes {file_type} files in the "{subfolder_path}" sub-folder"""))
def pst_write_files_in_a_subfolder(
    world: PstBeamWorld,
    file_type: str,
    subfolder_path: str,
) -> None:
    """Assert that a given file type is available in a the correct sub folder."""
    world.assert_files_written_to_folder(file_type=file_type, subfolder_path=subfolder_path)


@then(parsers.re(r"PST writes (?P<file_type>.*?) (?:files? )?in the output dir"))
def pst_write_files_in_the_output_dir(
    world: PstBeamWorld,
    file_type: str,
) -> None:
    """Assert that a given file type is available in a the correct sub folder."""
    world.assert_files_written_to_folder(file_type=file_type)


@then(parsers.parse("PST publishes scalar statistical summaries every {period_str}"))
def pst_publishes_scalar_statistical_summaries_periodically(world: PstBeamWorld, period_str: str) -> None:
    """Assert that STAT.MGMT updates statistical values periodically."""
    period = convert_value_to_quantity(period_str)
    assert isinstance(period, tuple)
    period_range_secs: Tuple[float, float] = tuple(v.value for v in period)  # type: ignore

    world.assert_stat_scalar_attributes_updated(period_range_secs)


@then(parsers.parse("PST writes all data quality statistics to configured storage every {period_str}"))
def pst_writes_all_data_quality_statistics_to_configured_storage(
    world: PstBeamWorld, period_str: str
) -> None:
    """Assert that STAT.CORE creates a STAT HDF5 monitor file periodically to configured storage."""
    period = convert_value_to_quantity(period_str)
    assert isinstance(period, tuple)
    period_range_secs: Tuple[float, float] = tuple(v.value for v in period)  # type: ignore

    world.assert_stat_quality_statistics_written(period_range_secs)


@then(
    parsers.parse("PST periodically publishes the mean and variance of each:"),
    target_fixture="data_dimensions",
)
@then(
    parsers.parse("PST periodically writes the mean and variance spectra of each:"),
    target_fixture="data_dimensions",
)
def pst_periodically_publishes_the_mean_and_variance(datatable: List[List[Any]]) -> pd.DataFrame:
    """Get data dimensions to assert mean and variances against in later steps."""
    return convert_to_dataframe(datatable=datatable)


@then(parsers.parse("each mean and variance are within {tolerance:g} sigma of the known values"))
def mean_and_variance_are_within_tolerance_sigma_of_the_known_values(
    world: PstBeamWorld,
    tolerance: float,
    data_dimensions: pd.DataFrame,
    num_samples_per_channel: int,
) -> None:
    """Assert the frequency averages statistics is within a variance of a known values."""
    world.assert_frequency_average_statistics_are_within_tolerance(
        tolerance=tolerance, data_dimensions=data_dimensions, num_samples_per_channel=num_samples_per_channel
    )


@then(
    parsers.parse("the mean and variance in each channel are within {tolerance:g} sigma of the known values")
)
def mean_and_variance_in_each_channel_are_within_tolerance_sigma_of_the_known_values(
    world: PstBeamWorld,
    tolerance: float,
    data_dimensions: pd.DataFrame,
) -> None:
    """Assert that a given statistic is within a variance of a known value."""
    world.assert_channel_statistics_are_within_tolerance(tolerance=tolerance, data_dimensions=data_dimensions)


@then(parsers.parse("PST is in {processing_mode} mode"))
@then(parsers.parse("PST reports that processing mode is {processing_mode}"))
def pst_is_in_processing_mode(
    world: PstBeamWorld,
    processing_mode: str,
) -> None:
    """Assert that PST is in a given processing mode."""
    world.assert_pst_processing_mode(processing_mode=processing_mode)


@then(parsers.parse("PST reports that {mode_str} mode is {value_str}"))
@then(parsers.re(r"PST reports that (?P<mode_str>\w+) mode transitioned from (?:\w+) to (?P<value_str>\w+)"))
def pst_reports_that_the_mode_has_a_given_value(
    world: PstBeamWorld,
    mode_str: str,
    value_str: str,
) -> None:
    """Assert that PST BEAM reports the correct mode value."""
    if mode_str == "processing":
        world.assert_pst_processing_mode(processing_mode=value_str)
    else:
        expected_value = get_mode_value(mode_str, value_str)
        world.assert_mode(mode_str, expected_value)


@then("PST completes the transition in less than T + 24 seconds")
def pst_completes_reconfiguration_within_24_seconds(world: PstBeamWorld) -> None:
    """Assert that PST can be reconfigured within 24 seconds."""
    world.assert_reconfigured_within(datetime.timedelta(seconds=24))


@then(parsers.re(r"PST reports that operational state transitioned from (?:\w+) to (?P<state_str>\w+)"))
def pst_reports_that_its_operational_state_updated(
    world: PstBeamWorld,
    state_str: str,
) -> None:
    """Assert that the PST BEAM is in the correct operational state."""
    world.assert_operational_state(state_str)


@then("PST writes valid decimated voltage data to configured storage")
def pst_writes_valid_flow_through_decimated_voltaged_data() -> None:
    """Assert that the FLOW THROUGH data is written to configured storage."""


@then(parsers.parse("decimated voltage data contain only {nbit_out_str} bits per sample"))
def pst_writes_correct_nbits_out_in_decimate_voltage(world: PstBeamWorld, nbit_out: int) -> None:
    """Assert that the FLOW THROUGH mode data has NBIT header set."""
    world.wait_for_scan_to_complete()
    world.assert_flow_through_files_written()
    world.assert_flow_through_files(nbit_out=nbit_out)


@then(parsers.parse("decimated voltage data contain only {polarizations_out} polarizations"))
def pst_writes_correct_polarizations_out_in_decimate_voltage(
    world: PstBeamWorld, polarizations_out: str
) -> None:
    """Assert that the FLOW THROUGH mode data has the correct output polarization(s)."""
    world.wait_for_scan_to_complete()
    world.assert_flow_through_files_written()
    world.assert_flow_through_files(polarizations_out=polarizations_out)


@then(parsers.parse("decimated voltage data contain only {channels_out_str} frequency channels"))
def pst_writes_correct_frequency_channels_out_in_decimate_voltage(
    world: PstBeamWorld,
    channels_out: Tuple[int, int],
) -> None:
    """Assert that FLOW THROUGH mode data has the correct number of NCHAN and the START_CHANNEL headers."""
    world.wait_for_scan_to_complete()
    world.assert_flow_through_files_written()
    world.assert_flow_through_files(channels_out=channels_out)


"""Helper functions."""


def convert_to_dataframe(datatable: List[List[Any]]) -> pd.DataFrame:
    """Convert a BDD datatable to a data frame.

    :param datatable: list of rows of a table, first row is the header.
    :type datatable: List[List[Any]]
    :return: the table as a Pandas data frame
    :rtype: pd.DataFrame
    """
    [columns, *rows] = datatable

    data: Dict[str, List[Any]] = {c: [] for c in columns}
    for idx, row in enumerate(rows):
        assert len(row) == len(columns), f"Expected data row {idx + 1} to have same length as header row"
        for c, v in zip(columns, row):
            data[c].append(v)

    df = pd.DataFrame(data=data)
    df = df.apply(pd.to_numeric, errors="ignore")  # type: ignore

    return df


CONTROL_MODE_TYPES: Dict[str, Type] = {
    "test": TestMode,
    "admin": AdminMode,
    "control": ControlMode,
    "simulation": SimulationMode,
}


def get_mode_value(mode: str, value: str) -> Any:
    """Get the enum value for a given mode."""
    value = value.upper().replace("-", "_")
    return CONTROL_MODE_TYPES[mode][value]
