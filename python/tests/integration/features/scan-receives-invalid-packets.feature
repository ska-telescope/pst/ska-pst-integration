@XTP-22325
Feature: PST correctly handles bad/missing data from the beam-former

    Background:
        Given PST is configured, deployed and running

    @XTP-22326 @XTP-22325 @XTP-19671 @CSP @Entry_point @PST @Team_PST @ska_low @ska_mid
    Scenario Outline: PST detects invalid packets
        Given PST is in the Scanning state
        When PST receives an <invalid_packet_type> packet
        Then PST increments the <monitored_property> packet counter
        And PST remains in the Scanning state

        Examples:
            | invalid_packet_type              | monitored_property            |
            | malformed                        | malformed                     |
            | incorrect packet size            | malformed                     |
            | dropped                          | dropped                       |
            | wrong scan id                    | misdirected                   |
            | wrong channel number             | misdirected                   |
            | no valid polarisation correction | noValidPolarisationCorrection |
            | no valid station beam            | noValidStationBeam            |
            | no valid pst beam                | noValidPstBeam                |

    @XTP-24204 @XTP-22325 @XTP-19671 @CSP @Entry_point @PST @Team_PST @ska_low @ska_mid
    Scenario Outline: PST flags invalid packets in output data
        Given PST is configured to operate in <processing_mode> mode
        And PST is in the Scanning state
        When PST receives an <invalid_packet_type> packet
        And scan completes successfully
        Then PST flags the <invalid_packet_type> packet in <output_data> files

        Examples:
            | processing_mode  | invalid_packet_type              | output_data |
            | voltage recorder | malformed                        | weights     |
            | voltage recorder | incorrect packet size            | weights     |
            | voltage recorder | dropped                          | weights     |
            | voltage recorder | wrong scan id                    | weights     |
            | voltage recorder | wrong channel number             | weights     |
            | voltage recorder | no valid polarisation correction | weights     |
            | voltage recorder | no valid station beam            | weights     |
            | voltage recorder | no valid pst beam                | weights     |
