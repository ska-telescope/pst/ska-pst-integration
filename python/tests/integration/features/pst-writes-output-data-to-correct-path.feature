@XTP-27515
Feature: PST writes output data to the correct path

    Background:
        Given PST is configured, deployed and running
        And is configured with sub-system identifier "sub_id"

    @XTP-27516 @XTP-27515 @XTP-19671 @CSP @PST @Team_PST @ska_low @ska_mid
    Scenario: PST writes output data to the correct path
        Given PST is configured to operate in voltage recorder mode
        And PST is configured with execution block identifier "eb_id"
        And PST is in the Scanning state with scan identifier "scan_id"
        When valid data is sent to PST
        And scan completes successfully
        Then PST creates a subfolder of the shared storage mount named "product/<eb_id>/<sub_id>/<scan_id>/"
        And PST writes voltage data files in the "data/" sub-folder
        And PST writes weights and scales files in the "weights/" sub-folder
        And PST writes data statistics files in the "stat/" sub-folder
        And PST writes scan configuration in the output dir
        And PST writes metadata file in the output dir
