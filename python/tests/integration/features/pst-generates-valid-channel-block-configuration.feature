@XTP-24703
Feature: PST generates valid channel block configuration

  Background:
    Given PST is configured, deployed and running

  @XTP-24708 @XTP-24703 @XTP-22310 @CSP @Entry_point @PST @Team_PST @ska_low @ska_mid
  Scenario: PST initialises empty channel block configuration
    Given PST is in the Idle state
    Then PST returns empty channel block configuration

  @XTP-24709 @XTP-24703 @XTP-22310 @CSP @Entry_point @PST @Team_PST @ska_low @ska_mid
  Scenario: PST generates valid channel block configuration
    Given PST is in the Idle state
    When PST receives a valid scan configuration request
    Then PST transitions to the Scan Configured state
    And PST returns valid channel block configuration

  @XTP-24711 @XTP-24703 @XTP-22310 @CSP @Entry_point @PST @Team_PST @ska_low @ska_mid
  Scenario: PST clears channel block configuration after gotoIdle
    Given PST is in the Scan Configured state
    When PST receives a deconfigure scan request
    Then PST transitions to the Idle state
    And PST returns empty channel block configuration
