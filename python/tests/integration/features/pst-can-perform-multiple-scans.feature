@XTP-22318
Feature: PST can perform multiple scans

    Background:
        Given PST is configured, deployed and running

    @XTP-22314 @XTP-22318 @XTP-19671 @Team_PST @configuration @ska_low @ska_mid
    Scenario: PST can use a single scan configuration for multiple scans
        Given PST is in the Scanning state
        And PST is receiving valid data
        When PST receives a stop scan request
        And data is no longer sent to PST
        And PST receives a start scan request with a different scan ID
        And valid data is sent to PST
        Then PST transitions to the Scanning state
        And PST reports monitoring data
        And PST writes expected data products to configured storage

    @XTP-22315 @XTP-22318 @XTP-19671 @PST @Team_PST @configuration @ska_low @ska_mid
    Scenario: PST can be reconfigured after a scan
        Given PST is in the Scanning state
        And PST is receiving valid data
        When PST receives a stop scan request at time T
        And data is no longer sent to PST
        And PST receives a deconfigure scan request
        And PST receives a different valid scan configuration request
        And PST receives a start scan request with a different scan ID
        And valid data is sent to PST
        Then PST transitions to the Scanning state
        And PST completes the transition in less than T + 24 seconds
        And PST reports monitoring data
        And PST writes expected data products to configured storage
