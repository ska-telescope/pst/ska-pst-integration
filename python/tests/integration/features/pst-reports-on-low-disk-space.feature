@XTP-24499
Feature: PST storage space is low or about to run out

    Background:
        Given PST is configured, deployed and running

    @skip @XTP-24500 @XTP-24499 @XTP-19671 @CSP @Entry_point @PST @Team_PST @ska_low @ska_mid
    Scenario Outline: PST reports available recording time
      Given PST is in the Scanning state
      When the time remaining until storage fills falls below <time>
      And valid data is sent to PST
      Then PST reports a <warning> until storage space will run out
      And PST remains in the Scanning state

      Examples:
        | time   | warning                        |
        | 1 hour | message that 1 hour remains    |
        | 30 min | message that 30 minutes remain |
        | 5 min  | message that 5 minutes remain  |

    @skip @XTP-24501 @XTP-24499 @XTP-19671 @CSP @Entry_point @PST @Team_PST @ska_low @ska_mid
    Scenario Outline: PST raises alarms when recording time is too low during a scan
      Given PST is in the Scanning state
      When the time remaining until storage fills falls below <time>
      And valid data is sent to PST
      Then PST enters an ALARM state
      And PST reports the available recording time is below minimum <alarm_type> threshold

      Examples:
        | time   | alarm_type  |
        | 1 min  | warning     |
        | 10 sec | alarm       |

    @skip @XTP-24502 @XTP-24499 @XTP-19671 @CSP @Entry_point @PST @Team_PST @ska_low @ska_mid
    Scenario Outline: PST raises alarms before storage space runs out
      Given PST is online
      When the disk storage fills above <percentage> used
      Then PST enters an ALARM state
      And PST reports the disk used percentage is above maximum <alarm_type> threshold

      Examples:
        | percentage | alarm_type |
        | 95         | warning    |
        | 99         | alarm      |
