@XTP-28781
Feature: PST computes and reports data quality statistics

  Asserting that the statistical values are updated every 5 seconds
  uses fuzzy logic. A range of values is supplied to allow for variance
  in timing. Monitored attributes are calculated in STAT.CORE every
  5 seconds but are reported back to the STAT.LMC via gRPC and TANGO.

  Background:
    Given PST is configured, deployed and running
    And PST is configured to report on data quality every 5 seconds

  @XTP-28780 @XTP-28781 @XTP-19671 @CSP @PST @Team_PST @ska_low @ska_mid
  Scenario: PST periodically reports data quality statistics

    Given PST is in the Scanning state
    When data are sent to PST for 30 seconds
    Then PST publishes scalar statistical summaries every 4 to 6 seconds

  @XTP-28782 @XTP-28781 @XTP-19671 @CSP @PST @Team_PST @ska_low @ska_mid
  Scenario: PST periodically writes data quality statistics to file
    Given PST is in the Scanning state
    When data are sent to PST for 30 seconds
    Then PST writes all data quality statistics to configured storage every 4 to 6 seconds

  @XTP-28783 @XTP-28781 @XTP-19671 @CSP @PST @Team_PST @ska_low @ska_mid
  Scenario: PST correctly computes the mean and variance of data
    Given PST is configured to compute statistics from Nt time samples:

        | Telescope | Band | Nt   |
        | SKALow    | low  | 512  |
        | SKAMid    | 1    | 4096 |
        | SKAMid    | 2    | 4096 |
        | SKAMid    | 3    | 2048 |
        | SKAMid    | 4    | 2048 |
        | SKAMid    | 5a   | 2048 |
        | SKAMid    | 5b   | 2048 |

    And PST is in the Scanning state
    When data are sent to PST with known mean and variance for 30 seconds
    Then PST periodically publishes the mean and variance of each:

        | pol | dimension |
        | A   | Real      |
        | A   | Imag      |
        | B   | Real      |
        | B   | Imag      |

    And each mean and variance are within 6 sigma of the known values

  @XTP-28784 @XTP-28781 @XTP-19671 @CSP @PST @Team_PST @ska_low @ska_mid
  Scenario: PST correctly computes mean and variance spectra
    Given PST is configured to compute statistics from Nt time samples:

        | Telescope | Band | Nt   |
        | SKALow    | low  | 512  |
        | SKAMid    | 1    | 4096 |
        | SKAMid    | 2    | 4096 |
        | SKAMid    | 3    | 2048 |
        | SKAMid    | 4    | 2048 |
        | SKAMid    | 5a   | 2048 |
        | SKAMid    | 5b   | 2048 |

    And PST is in the Scanning state
    When data are sent to PST with known mean and variance for 30 seconds
    Then PST periodically writes the mean and variance spectra of each:

        | pol | dimension |
        | A   | Real      |
        | A   | Imag      |
        | B   | Real      |
        | B   | Imag      |

    And the mean and variance in each channel are within 6 sigma of the known values
