@L2-4078 @L2-4080 @L2-4084 @L2-4087 @L2-4089 @L2-4092
Feature: PST correctly reports mode and state transitions

    Background:
        Given PST is configured, deployed and running

    #Note that PST does not implement any test modes, and therefore there are no test mode transitions in the scenario outline examples.
    @XTP-34400 @XTP-19671 @PST @Team_PST @configuration @ska_low @ska_mid
    Scenario Outline: PST correctly reports mode and state transitions
        Given PST <property> is <initial>
        When PST <property> changes to <final>
        Then PST reports that <property> transitioned from <initial> to <final>

        Examples:
            | property          | initial | final    |
            | operational state | off     | on       |
            # | operational state | on      | fault |
            # | health state | ok       | degraded |
            # | health state | degraded | failed   |
            | admin mode        | offline | online   |
            | admin mode        | offline | reserved |
            | control mode      | remote  | local    |
            | control mode      | local   | remote   |
            | simulation mode   | true    | false    |
            | simulation mode   | false   | true     |
