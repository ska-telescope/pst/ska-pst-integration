@L2-4088 @L2-4090 @L2-4092 @L2-4094 @L2-4099
Feature: PST correctly reports writable modes

    #Note that PST does not implement any test modes, and therefore only test mode = none is included in the scenario outline examples.

    Background:
        Given PST is configured, deployed and running

    @XTP-34393 @XTP-19671 @PST @Team_PST @configuration @ska_low @ska_mid
    Scenario Outline: PST correctly reports writable modes
        Given PST is in the Idle state
        And PST is commanded to set <mode> mode to <value>
        When PST <mode> mode is queried
        Then PST reports that <mode> mode is <value>

        Examples:
            | mode       | value       |
            | test       | none        |
            | admin      | online      |
            | admin      | offline     |
            | admin      | engineering |
            | admin      | not-fitted  |
            | control    | remote      |
            | control    | local       |
            | simulation | true        |
            | simulation | false       |
