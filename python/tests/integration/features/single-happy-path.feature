@XTP-22307
Feature: PST can perform a single scan

    Each PST instance is deployed to process a single tied-array beam
    in a single mode of operation. Possible modes of operation include
    voltage recorder, flow through, detected filterbank, and timing.
    The expected data products for each mode are well-defined
    (content, format, metadata, etc.)

    Background:
        Given PST is configured, deployed and running

    @XTP-22309 @XTP-22307 @XTP-19671 @PST @Team_PST @configuration @ska_low @ska_mid
    Scenario Outline: PST can be configured to perform a scan
        Given PST is in the Idle state
        When PST is configured to operate in <processing_mode> mode
        And PST receives a valid scan configuration request
        Then PST transitions to the Scan Configured state
        And PST reports that processing mode is <processing_mode>

        Examples:
            | processing_mode  |
            | voltage recorder |
            | flow through     |

    @XTP-22311 @XTP-22307 @XTP-19671 @PST @Resource_Management @Team_PST @ska_low @ska_mid
    Scenario Outline: PST can perform a scan
        Given PST is configured to operate in <processing_mode> mode
        And PST is in the Scan Configured state
        When PST receives a start scan request
        And valid data is sent to PST
        Then PST transitions to the Scanning state
        And PST reports that processing mode is <processing_mode>
        And PST reports monitoring data
        And PST writes valid <output_data> to configured storage

        Examples:
            | processing_mode  | output_data              |
            | voltage recorder | beam-formed voltage data |
            | flow through     | decimated voltage data   |

    @XTP-22312 @XTP-22307 @XTP-19671 @Abort @Team_PST @ska_low @ska_mid
    Scenario Outline: PST can stop a scan
        Given PST is configured to operate in <processing_mode> mode
        And PST is in the Scanning state
        And PST is receiving valid data
        And PST is recording monitoring data
        When PST receives a stop scan request
        And data is no longer sent to PST
        Then PST transitions to the Scan Configured state
        And PST reports that processing mode is <processing_mode>
        And PST stops writing data products to configured storage
        And PST stops reporting monitoring data

        Examples:
            | processing_mode  |
            | voltage recorder |
            | flow through     |

    @XTP-22313 @XTP-22307 @XTP-19671 @PST @Power @Team_PST @configuration @ska_low @ska_mid
    Scenario Outline: PST can return to the Idle state
        Given PST is configured to operate in <processing_mode> mode
        And PST is in the Scan Configured state
        When PST receives a deconfigure scan request
        Then PST transitions to the Idle state
        And PST reports that processing mode is Idle

        Examples:
            | processing_mode  |
            | voltage recorder |
            | flow through     |

    @XTP-73843 @XTP-22307 @XTP-19671 @PST @Power @Team_PST @configuration @ska_low @ska_mid
    Scenario Outline: PST can handle a scan when no data is sent
        Given PST is configured to operate in <processing_mode> mode
        And PST is in the Scanning state
        When PST receives a stop scan request
        Then PST transitions to the Scan Configured state

        Examples:
            | processing_mode  |
            | voltage recorder |
            | flow through     |
