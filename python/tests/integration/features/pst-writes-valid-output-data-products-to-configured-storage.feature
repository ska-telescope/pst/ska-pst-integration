@XTP-22499
Feature: PST writes valid output data products to configured storage

    Background:
        Given PST is configured, deployed and running

    @XTP-23878 @XTP-22499 @XTP-19671 @CSP @PST @Team_PST @ska_low @ska_mid
    Scenario Outline: PST writes data in the correct format
        Given PST is configured to operate in <processing_mode> mode
        And PST is in the Scan Configured state
        When PST receives a start scan request
        And valid data is sent to PST
        And scan completes successfully
        Then PST writes valid <output_data> to configured storage

        Examples:
            | processing_mode  | output_data              |
            | voltage recorder | beam-formed voltage data |
            | voltage recorder | data weights             |


    @XTP-23879 @XTP-22499 @XTP-19671 @CSP @PST @Team_PST @ska_low @ska_mid
    Scenario Outline: PST output data span the scan length
        Given PST is configured to operate in <processing_mode> mode
        And max scan length is configured to <scan_length>
        And PST is in the Scan Configured state
        When PST performs a scan for <scan_length>
        Then <output_data> files contiguously span <scan_length>

        Examples:
            | processing_mode  | scan_length | output_data              |
            | voltage recorder | 10 seconds  | beam-formed voltage data |
            | voltage recorder | 10 seconds  | data weights             |
            | voltage recorder | 15 seconds  | beam-formed voltage data |
            | voltage recorder | 15 seconds  | data weights             |
            | voltage recorder | 27 seconds  | beam-formed voltage data |
            | voltage recorder | 27 seconds  | data weights             |

    @XTP-22500 @XTP-22499 @XTP-19671 @CSP @PST @Team_PST
    Scenario Outline: PST output data have the correct channel order
        Given PST is configured to operate in <processing_mode> mode
        And PST is configured to observe the <band>
        And centre frequency is configured to <centre_frequency> Hz
        And bandwidth is configured to <bandwidth> Hz
        And PST is in the Scanning state
        When data containing a <frequency> MHz sine wave is sent to PST
        And scan completes successfully
        Then PST writes expected data products to configured storage
        And a <frequency> MHz sine wave is detected in the <output_data>

        @ska_low
        Examples:
            | processing_mode  | band     | output_data              | frequency | centre_frequency | bandwidth |
            | voltage recorder | Low Band | beam-formed voltage data | 51.3      | 51000000         | 1562500   |

        @ska_mid
        Examples:
            | processing_mode  | band       | output_data              | frequency | centre_frequency | bandwidth |
            | voltage recorder | Mid Band1  | beam-formed voltage data | 341.59104 |        341591040 |   9945600 |
            | voltage recorder | Mid Band5a | beam-formed voltage data | 4694.4576 |       4694457600 |   9945600 |
