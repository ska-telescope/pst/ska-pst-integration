@XTP-22688
Feature: PST behaves as expected when a command is received

  Background:
    Given PST is configured, deployed and running

  @XTP-22689 @XTP-22688 @XTP-19671 @Team_PST @ska_low @ska_mid
  Scenario Outline: PST correctly handles incorrect command requests
    Given PST is in the <state> state
    When PST receives a <invalid_command> request
    Then the command is rejected
    And PST remains in the <state> state

  Examples:
    | state    | invalid_command |
    | Idle     | start scan      |
    | Idle     | end scan        |
    | Idle     | go to idle      |
    | Idle     | obs reset       |
    | Ready    | end scan        |
    | Ready    | obs reset       |
    | Scanning | configure scan  |
    | Scanning | start scan      |
    | Scanning | go to idle      |
    | Scanning | obs reset       |
    | Aborted  | configure scan  |
    | Aborted  | start scan      |
    | Aborted  | end scan        |
    | Aborted  | go to idle      |
    | Fault    | configure scan  |
    | Fault    | start scan      |
    | Fault    | end scan        |
    | Fault    | go to idle      |

  @XTP-22690 @XTP-22688 @XTP-19671 @Team_PST @ska_low @ska_mid
  Scenario Outline: PST correctly handles correct command requests
    Given PST is in the <initial_state> state
    When PST receives a <command> request
    Then PST transitions to the <final_state> state

  Examples:
    | initial_state    | command        | final_state |
    | Idle             | configure scan | Ready       |
    | Idle             | abort          | Aborted     |
    | Ready            | configure scan | Ready       |
    | Ready            | start scan     | Scanning    |
    | Ready            | go to idle     | Idle        |
    | Ready            | abort          | Aborted     |
    | Scanning         | end scan       | Ready       |
    | Scanning         | abort          | Aborted     |
    | Aborted          | obs reset      | Idle        |
    | Fault            | obs reset      | Idle        |
    | Aborted          | reset          | Idle        |
    | Fault            | reset          | Idle        |
