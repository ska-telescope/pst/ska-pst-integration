@XTP-50184
Feature: PST writes decimated voltage data

    Data = dual-polarisation, channelized, complex-valued time series
    Data for a single tied-array beam are received by PST on its CBF interface
    and the output data have fewer frequency channels; and/or fewer bits per sample

    Lost = packets that are either invalid or have zero antenna weight will be recorded
    in a lost packets file

    Weights = average packet weights integrated over a specified time interval

    Background:
        Given PST is configured, deployed and running

    @XTP-50185 @XTP-50184 @XTP-19671 @ska_low @ska_mid
    Scenario Outline: PST writes re-quantised voltages

        Given PST is configured to operate in flow through mode
        And PST is configured to write <nbit_out> bits per sample
        And PST is in the Scanning state
        When data are sent to PST
        Then PST writes valid decimated voltage data to configured storage
        And decimated voltage data contain only <nbit_out> bits per sample

        Examples:
            | nbit_out |
            | 1        |
            | 2        |
            | 4        |
            | 8        |
            | 16       |

    @XTP-50189 @XTP-50184 @XTP-19671 @ska_low @ska_mid
    Scenario Outline: PST writes voltages for only specified polarizations

        Given PST is configured to operate in flow through mode
        And PST is configured to write <polarizations> polarizations
        And PST is in the Scanning state
        When data are sent to PST
        Then PST writes valid decimated voltage data to configured storage
        And decimated voltage data contain only <polarizations> polarizations

        Examples:
            | polarizations |
            | A             |
            | B             |
            | Both          |

    @XTP-50190 @XTP-50184 @XTP-19671
    Scenario Outline: PST writes voltages for only specified frequency channels

        Given PST is configured to operate in flow through mode
        And PST is configured to observe the <band>
        And PST is configured to write <channels_out> frequency channels
        And PST is in the Scanning state
        When data are sent to PST
        Then PST writes valid decimated voltage data to configured storage
        And decimated voltage data contain only <channels_out> frequency channels

        @ska_low
        Examples:
            | band     | channels_out |
            | Low Band | 0-255        |

        @ska_mid
        Examples:
            | band         | channels_out |
            | Mid Band 1   | 100-290      |
            | Mid Band 5a  | 500-550      |
