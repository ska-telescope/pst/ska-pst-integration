@XTP-22319
Feature: PST can abort

    Background:
        Given PST is configured, deployed and running

    @XTP-22323 @XTP-22319 @XTP-19671 @CSP @Entry_point @PST @Team_PST @ska_low @ska_mid
    Scenario Outline: PST can abort scan while receiving data
        Given PST is configured to operate in <processing_mode> mode
        And PST is in the Scanning state
        And PST is receiving valid data
        When PST receives an abort request
        Then PST transitions to the Aborted state
        And PST stops writing data products to configured storage
        And PST stops reporting monitoring data

        Examples:
            | processing_mode  | output_data              |
            | voltage recorder | beam-formed voltage data |
            | flow through     | decimated voltage data   |

    @XTP-22322 @XTP-22319 @XTP-19671 @Abort @PST @Team_PST @flaky @ska_low @ska_mid
    Scenario Outline: PST can abort scan before receiving data
        Given PST is configured to operate in <processing_mode> mode
        And PST is in the Scanning state
        And PST has not received any data
        When PST receives an abort request
        Then PST transitions to the Aborted state

        Examples:
            | processing_mode  | output_data              |
            | voltage recorder | beam-formed voltage data |
            | flow through     | decimated voltage data   |
