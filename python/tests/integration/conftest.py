# -*- coding: utf-8 -*-
#
# This file is part of the SKA PST project
#
# Distributed under the terms of the BSD 3-clause new license.
# See LICENSE for more info.
"""Configuration for BDD tests."""

import pytest


@pytest.fixture
def change_event_callback_time() -> float:
    """Get timeout used for change event callbacks."""
    return 5.0
