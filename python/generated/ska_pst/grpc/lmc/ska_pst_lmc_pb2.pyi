from google.protobuf.internal import containers as _containers
from google.protobuf.internal import enum_type_wrapper as _enum_type_wrapper
from google.protobuf import descriptor as _descriptor
from google.protobuf import message as _message
from typing import ClassVar as _ClassVar, Iterable as _Iterable, Mapping as _Mapping, Optional as _Optional, Union as _Union

DESCRIPTOR: _descriptor.FileDescriptor

class ObsState(int, metaclass=_enum_type_wrapper.EnumTypeWrapper):
    __slots__ = ()
    EMPTY: _ClassVar[ObsState]
    RESOURCING: _ClassVar[ObsState]
    IDLE: _ClassVar[ObsState]
    CONFIGURING: _ClassVar[ObsState]
    READY: _ClassVar[ObsState]
    SCANNING: _ClassVar[ObsState]
    ABORTING: _ClassVar[ObsState]
    ABORTED: _ClassVar[ObsState]
    RESETTING: _ClassVar[ObsState]
    FAULT: _ClassVar[ObsState]
    RESTARTING: _ClassVar[ObsState]

class ErrorCode(int, metaclass=_enum_type_wrapper.EnumTypeWrapper):
    __slots__ = ()
    OK: _ClassVar[ErrorCode]
    INVALID_REQUEST: _ClassVar[ErrorCode]
    INTERNAL_ERROR: _ClassVar[ErrorCode]
    CONFIGURED_FOR_BEAM_ALREADY: _ClassVar[ErrorCode]
    NOT_CONFIGURED_FOR_BEAM: _ClassVar[ErrorCode]
    ALREADY_SCANNING: _ClassVar[ErrorCode]
    NOT_SCANNING: _ClassVar[ErrorCode]
    CONFIGURED_FOR_SCAN_ALREADY: _ClassVar[ErrorCode]
    NOT_CONFIGURED_FOR_SCAN: _ClassVar[ErrorCode]

class ProcessingMode(int, metaclass=_enum_type_wrapper.EnumTypeWrapper):
    __slots__ = ()
    PULSAR_TIMING: _ClassVar[ProcessingMode]
    DETECTED_FILTERBANK: _ClassVar[ProcessingMode]
    FLOW_THROUGH: _ClassVar[ProcessingMode]

class LogLevel(int, metaclass=_enum_type_wrapper.EnumTypeWrapper):
    __slots__ = ()
    INFO: _ClassVar[LogLevel]
    DEBUG: _ClassVar[LogLevel]
    WARNING: _ClassVar[LogLevel]
    CRITICAL: _ClassVar[LogLevel]
    ERROR: _ClassVar[LogLevel]
EMPTY: ObsState
RESOURCING: ObsState
IDLE: ObsState
CONFIGURING: ObsState
READY: ObsState
SCANNING: ObsState
ABORTING: ObsState
ABORTED: ObsState
RESETTING: ObsState
FAULT: ObsState
RESTARTING: ObsState
OK: ErrorCode
INVALID_REQUEST: ErrorCode
INTERNAL_ERROR: ErrorCode
CONFIGURED_FOR_BEAM_ALREADY: ErrorCode
NOT_CONFIGURED_FOR_BEAM: ErrorCode
ALREADY_SCANNING: ErrorCode
NOT_SCANNING: ErrorCode
CONFIGURED_FOR_SCAN_ALREADY: ErrorCode
NOT_CONFIGURED_FOR_SCAN: ErrorCode
PULSAR_TIMING: ProcessingMode
DETECTED_FILTERBANK: ProcessingMode
FLOW_THROUGH: ProcessingMode
INFO: LogLevel
DEBUG: LogLevel
WARNING: LogLevel
CRITICAL: LogLevel
ERROR: LogLevel

class Status(_message.Message):
    __slots__ = ("code", "message")
    CODE_FIELD_NUMBER: _ClassVar[int]
    MESSAGE_FIELD_NUMBER: _ClassVar[int]
    code: ErrorCode
    message: str
    def __init__(self, code: _Optional[_Union[ErrorCode, str]] = ..., message: _Optional[str] = ...) -> None: ...

class SmrbBeamConfiguration(_message.Message):
    __slots__ = ("data_key", "weights_key", "hb_nbufs", "hb_bufsz", "db_nbufs", "db_bufsz", "wb_nbufs", "wb_bufsz")
    DATA_KEY_FIELD_NUMBER: _ClassVar[int]
    WEIGHTS_KEY_FIELD_NUMBER: _ClassVar[int]
    HB_NBUFS_FIELD_NUMBER: _ClassVar[int]
    HB_BUFSZ_FIELD_NUMBER: _ClassVar[int]
    DB_NBUFS_FIELD_NUMBER: _ClassVar[int]
    DB_BUFSZ_FIELD_NUMBER: _ClassVar[int]
    WB_NBUFS_FIELD_NUMBER: _ClassVar[int]
    WB_BUFSZ_FIELD_NUMBER: _ClassVar[int]
    data_key: str
    weights_key: str
    hb_nbufs: int
    hb_bufsz: int
    db_nbufs: int
    db_bufsz: int
    wb_nbufs: int
    wb_bufsz: int
    def __init__(self, data_key: _Optional[str] = ..., weights_key: _Optional[str] = ..., hb_nbufs: _Optional[int] = ..., hb_bufsz: _Optional[int] = ..., db_nbufs: _Optional[int] = ..., db_bufsz: _Optional[int] = ..., wb_nbufs: _Optional[int] = ..., wb_bufsz: _Optional[int] = ...) -> None: ...

class ReceiveSubbandResources(_message.Message):
    __slots__ = ("data_key", "weights_key", "bandwidth", "nchan", "start_channel", "end_channel", "frequency", "start_channel_out", "end_channel_out", "nchan_out", "bandwidth_out", "frequency_out", "data_host", "data_port", "data_mac")
    DATA_KEY_FIELD_NUMBER: _ClassVar[int]
    WEIGHTS_KEY_FIELD_NUMBER: _ClassVar[int]
    BANDWIDTH_FIELD_NUMBER: _ClassVar[int]
    NCHAN_FIELD_NUMBER: _ClassVar[int]
    START_CHANNEL_FIELD_NUMBER: _ClassVar[int]
    END_CHANNEL_FIELD_NUMBER: _ClassVar[int]
    FREQUENCY_FIELD_NUMBER: _ClassVar[int]
    START_CHANNEL_OUT_FIELD_NUMBER: _ClassVar[int]
    END_CHANNEL_OUT_FIELD_NUMBER: _ClassVar[int]
    NCHAN_OUT_FIELD_NUMBER: _ClassVar[int]
    BANDWIDTH_OUT_FIELD_NUMBER: _ClassVar[int]
    FREQUENCY_OUT_FIELD_NUMBER: _ClassVar[int]
    DATA_HOST_FIELD_NUMBER: _ClassVar[int]
    DATA_PORT_FIELD_NUMBER: _ClassVar[int]
    DATA_MAC_FIELD_NUMBER: _ClassVar[int]
    data_key: str
    weights_key: str
    bandwidth: float
    nchan: int
    start_channel: int
    end_channel: int
    frequency: float
    start_channel_out: int
    end_channel_out: int
    nchan_out: int
    bandwidth_out: float
    frequency_out: float
    data_host: str
    data_port: int
    data_mac: str
    def __init__(self, data_key: _Optional[str] = ..., weights_key: _Optional[str] = ..., bandwidth: _Optional[float] = ..., nchan: _Optional[int] = ..., start_channel: _Optional[int] = ..., end_channel: _Optional[int] = ..., frequency: _Optional[float] = ..., start_channel_out: _Optional[int] = ..., end_channel_out: _Optional[int] = ..., nchan_out: _Optional[int] = ..., bandwidth_out: _Optional[float] = ..., frequency_out: _Optional[float] = ..., data_host: _Optional[str] = ..., data_port: _Optional[int] = ..., data_mac: _Optional[str] = ...) -> None: ...

class ReceiveBeamConfiguration(_message.Message):
    __slots__ = ("udp_nsamp", "wt_nsamp", "udp_nchan", "frequency", "bandwidth", "nchan", "frontend", "fd_poln", "fd_hand", "fd_sang", "fd_mode", "fa_req", "nant", "antennas", "ant_weights", "npol", "nbits", "ndim", "tsamp", "ovrsamp", "nsubband", "subband_resources", "udp_format", "beam_id")
    UDP_NSAMP_FIELD_NUMBER: _ClassVar[int]
    WT_NSAMP_FIELD_NUMBER: _ClassVar[int]
    UDP_NCHAN_FIELD_NUMBER: _ClassVar[int]
    FREQUENCY_FIELD_NUMBER: _ClassVar[int]
    BANDWIDTH_FIELD_NUMBER: _ClassVar[int]
    NCHAN_FIELD_NUMBER: _ClassVar[int]
    FRONTEND_FIELD_NUMBER: _ClassVar[int]
    FD_POLN_FIELD_NUMBER: _ClassVar[int]
    FD_HAND_FIELD_NUMBER: _ClassVar[int]
    FD_SANG_FIELD_NUMBER: _ClassVar[int]
    FD_MODE_FIELD_NUMBER: _ClassVar[int]
    FA_REQ_FIELD_NUMBER: _ClassVar[int]
    NANT_FIELD_NUMBER: _ClassVar[int]
    ANTENNAS_FIELD_NUMBER: _ClassVar[int]
    ANT_WEIGHTS_FIELD_NUMBER: _ClassVar[int]
    NPOL_FIELD_NUMBER: _ClassVar[int]
    NBITS_FIELD_NUMBER: _ClassVar[int]
    NDIM_FIELD_NUMBER: _ClassVar[int]
    TSAMP_FIELD_NUMBER: _ClassVar[int]
    OVRSAMP_FIELD_NUMBER: _ClassVar[int]
    NSUBBAND_FIELD_NUMBER: _ClassVar[int]
    SUBBAND_RESOURCES_FIELD_NUMBER: _ClassVar[int]
    UDP_FORMAT_FIELD_NUMBER: _ClassVar[int]
    BEAM_ID_FIELD_NUMBER: _ClassVar[int]
    udp_nsamp: int
    wt_nsamp: int
    udp_nchan: int
    frequency: float
    bandwidth: float
    nchan: int
    frontend: str
    fd_poln: str
    fd_hand: int
    fd_sang: float
    fd_mode: str
    fa_req: float
    nant: int
    antennas: str
    ant_weights: str
    npol: int
    nbits: int
    ndim: int
    tsamp: float
    ovrsamp: str
    nsubband: int
    subband_resources: ReceiveSubbandResources
    udp_format: str
    beam_id: str
    def __init__(self, udp_nsamp: _Optional[int] = ..., wt_nsamp: _Optional[int] = ..., udp_nchan: _Optional[int] = ..., frequency: _Optional[float] = ..., bandwidth: _Optional[float] = ..., nchan: _Optional[int] = ..., frontend: _Optional[str] = ..., fd_poln: _Optional[str] = ..., fd_hand: _Optional[int] = ..., fd_sang: _Optional[float] = ..., fd_mode: _Optional[str] = ..., fa_req: _Optional[float] = ..., nant: _Optional[int] = ..., antennas: _Optional[str] = ..., ant_weights: _Optional[str] = ..., npol: _Optional[int] = ..., nbits: _Optional[int] = ..., ndim: _Optional[int] = ..., tsamp: _Optional[float] = ..., ovrsamp: _Optional[str] = ..., nsubband: _Optional[int] = ..., subband_resources: _Optional[_Union[ReceiveSubbandResources, _Mapping]] = ..., udp_format: _Optional[str] = ..., beam_id: _Optional[str] = ...) -> None: ...

class DspDiskBeamConfiguration(_message.Message):
    __slots__ = ("data_key", "weights_key")
    DATA_KEY_FIELD_NUMBER: _ClassVar[int]
    WEIGHTS_KEY_FIELD_NUMBER: _ClassVar[int]
    data_key: str
    weights_key: str
    def __init__(self, data_key: _Optional[str] = ..., weights_key: _Optional[str] = ...) -> None: ...

class DspFlowThroughBeamConfiguration(_message.Message):
    __slots__ = ("data_key", "weights_key")
    DATA_KEY_FIELD_NUMBER: _ClassVar[int]
    WEIGHTS_KEY_FIELD_NUMBER: _ClassVar[int]
    data_key: str
    weights_key: str
    def __init__(self, data_key: _Optional[str] = ..., weights_key: _Optional[str] = ...) -> None: ...

class StatBeamConfiguration(_message.Message):
    __slots__ = ("data_key", "weights_key")
    DATA_KEY_FIELD_NUMBER: _ClassVar[int]
    WEIGHTS_KEY_FIELD_NUMBER: _ClassVar[int]
    data_key: str
    weights_key: str
    def __init__(self, data_key: _Optional[str] = ..., weights_key: _Optional[str] = ...) -> None: ...

class TestBeamConfiguration(_message.Message):
    __slots__ = ("resources",)
    class ResourcesEntry(_message.Message):
        __slots__ = ("key", "value")
        KEY_FIELD_NUMBER: _ClassVar[int]
        VALUE_FIELD_NUMBER: _ClassVar[int]
        key: str
        value: str
        def __init__(self, key: _Optional[str] = ..., value: _Optional[str] = ...) -> None: ...
    RESOURCES_FIELD_NUMBER: _ClassVar[int]
    resources: _containers.ScalarMap[str, str]
    def __init__(self, resources: _Optional[_Mapping[str, str]] = ...) -> None: ...

class BeamConfiguration(_message.Message):
    __slots__ = ("smrb", "receive", "dsp_disk", "stat", "dsp_flow_through", "test")
    SMRB_FIELD_NUMBER: _ClassVar[int]
    RECEIVE_FIELD_NUMBER: _ClassVar[int]
    DSP_DISK_FIELD_NUMBER: _ClassVar[int]
    STAT_FIELD_NUMBER: _ClassVar[int]
    DSP_FLOW_THROUGH_FIELD_NUMBER: _ClassVar[int]
    TEST_FIELD_NUMBER: _ClassVar[int]
    smrb: SmrbBeamConfiguration
    receive: ReceiveBeamConfiguration
    dsp_disk: DspDiskBeamConfiguration
    stat: StatBeamConfiguration
    dsp_flow_through: DspFlowThroughBeamConfiguration
    test: TestBeamConfiguration
    def __init__(self, smrb: _Optional[_Union[SmrbBeamConfiguration, _Mapping]] = ..., receive: _Optional[_Union[ReceiveBeamConfiguration, _Mapping]] = ..., dsp_disk: _Optional[_Union[DspDiskBeamConfiguration, _Mapping]] = ..., stat: _Optional[_Union[StatBeamConfiguration, _Mapping]] = ..., dsp_flow_through: _Optional[_Union[DspFlowThroughBeamConfiguration, _Mapping]] = ..., test: _Optional[_Union[TestBeamConfiguration, _Mapping]] = ...) -> None: ...

class ConfigureBeamRequest(_message.Message):
    __slots__ = ("beam_configuration", "dry_run")
    BEAM_CONFIGURATION_FIELD_NUMBER: _ClassVar[int]
    DRY_RUN_FIELD_NUMBER: _ClassVar[int]
    beam_configuration: BeamConfiguration
    dry_run: bool
    def __init__(self, beam_configuration: _Optional[_Union[BeamConfiguration, _Mapping]] = ..., dry_run: bool = ...) -> None: ...

class ConfigureBeamResponse(_message.Message):
    __slots__ = ()
    def __init__(self) -> None: ...

class DeconfigureBeamRequest(_message.Message):
    __slots__ = ()
    def __init__(self) -> None: ...

class DeconfigureBeamResponse(_message.Message):
    __slots__ = ()
    def __init__(self) -> None: ...

class GetBeamConfigurationRequest(_message.Message):
    __slots__ = ()
    def __init__(self) -> None: ...

class GetBeamConfigurationResponse(_message.Message):
    __slots__ = ("beam_configuration",)
    BEAM_CONFIGURATION_FIELD_NUMBER: _ClassVar[int]
    beam_configuration: BeamConfiguration
    def __init__(self, beam_configuration: _Optional[_Union[BeamConfiguration, _Mapping]] = ...) -> None: ...

class SmrbScanConfiguration(_message.Message):
    __slots__ = ()
    def __init__(self) -> None: ...

class ScanCalibration(_message.Message):
    __slots__ = ("cal_mode", "cal_freq", "cal_dcyc", "cal_phs", "cal_nphs")
    CAL_MODE_FIELD_NUMBER: _ClassVar[int]
    CAL_FREQ_FIELD_NUMBER: _ClassVar[int]
    CAL_DCYC_FIELD_NUMBER: _ClassVar[int]
    CAL_PHS_FIELD_NUMBER: _ClassVar[int]
    CAL_NPHS_FIELD_NUMBER: _ClassVar[int]
    cal_mode: str
    cal_freq: float
    cal_dcyc: float
    cal_phs: float
    cal_nphs: int
    def __init__(self, cal_mode: _Optional[str] = ..., cal_freq: _Optional[float] = ..., cal_dcyc: _Optional[float] = ..., cal_phs: _Optional[float] = ..., cal_nphs: _Optional[int] = ...) -> None: ...

class ReceiveScanConfiguration(_message.Message):
    __slots__ = ("activation_time", "observation_id", "scan_id", "observer", "projid", "pnt_id", "subarray_id", "source", "itrf", "bmaj", "bmin", "coord_md", "equinox", "stt_crd1", "stt_crd2", "trk_mode", "scanlen_max", "test_vector", "calibration", "execution_block_id")
    ACTIVATION_TIME_FIELD_NUMBER: _ClassVar[int]
    OBSERVATION_ID_FIELD_NUMBER: _ClassVar[int]
    SCAN_ID_FIELD_NUMBER: _ClassVar[int]
    OBSERVER_FIELD_NUMBER: _ClassVar[int]
    PROJID_FIELD_NUMBER: _ClassVar[int]
    PNT_ID_FIELD_NUMBER: _ClassVar[int]
    SUBARRAY_ID_FIELD_NUMBER: _ClassVar[int]
    SOURCE_FIELD_NUMBER: _ClassVar[int]
    ITRF_FIELD_NUMBER: _ClassVar[int]
    BMAJ_FIELD_NUMBER: _ClassVar[int]
    BMIN_FIELD_NUMBER: _ClassVar[int]
    COORD_MD_FIELD_NUMBER: _ClassVar[int]
    EQUINOX_FIELD_NUMBER: _ClassVar[int]
    STT_CRD1_FIELD_NUMBER: _ClassVar[int]
    STT_CRD2_FIELD_NUMBER: _ClassVar[int]
    TRK_MODE_FIELD_NUMBER: _ClassVar[int]
    SCANLEN_MAX_FIELD_NUMBER: _ClassVar[int]
    TEST_VECTOR_FIELD_NUMBER: _ClassVar[int]
    CALIBRATION_FIELD_NUMBER: _ClassVar[int]
    EXECUTION_BLOCK_ID_FIELD_NUMBER: _ClassVar[int]
    activation_time: str
    observation_id: str
    scan_id: int
    observer: str
    projid: str
    pnt_id: str
    subarray_id: str
    source: str
    itrf: str
    bmaj: float
    bmin: float
    coord_md: str
    equinox: str
    stt_crd1: str
    stt_crd2: str
    trk_mode: str
    scanlen_max: int
    test_vector: str
    calibration: ScanCalibration
    execution_block_id: str
    def __init__(self, activation_time: _Optional[str] = ..., observation_id: _Optional[str] = ..., scan_id: _Optional[int] = ..., observer: _Optional[str] = ..., projid: _Optional[str] = ..., pnt_id: _Optional[str] = ..., subarray_id: _Optional[str] = ..., source: _Optional[str] = ..., itrf: _Optional[str] = ..., bmaj: _Optional[float] = ..., bmin: _Optional[float] = ..., coord_md: _Optional[str] = ..., equinox: _Optional[str] = ..., stt_crd1: _Optional[str] = ..., stt_crd2: _Optional[str] = ..., trk_mode: _Optional[str] = ..., scanlen_max: _Optional[int] = ..., test_vector: _Optional[str] = ..., calibration: _Optional[_Union[ScanCalibration, _Mapping]] = ..., execution_block_id: _Optional[str] = ...) -> None: ...

class DspDiskScanConfiguration(_message.Message):
    __slots__ = ("scanlen_max", "bytes_per_second", "execution_block_id")
    SCANLEN_MAX_FIELD_NUMBER: _ClassVar[int]
    BYTES_PER_SECOND_FIELD_NUMBER: _ClassVar[int]
    EXECUTION_BLOCK_ID_FIELD_NUMBER: _ClassVar[int]
    scanlen_max: float
    bytes_per_second: float
    execution_block_id: str
    def __init__(self, scanlen_max: _Optional[float] = ..., bytes_per_second: _Optional[float] = ..., execution_block_id: _Optional[str] = ...) -> None: ...

class DspFlowThroughScanConfiguration(_message.Message):
    __slots__ = ("scanlen_max", "bytes_per_second", "execution_block_id", "num_bits_out", "polarisations", "channels", "requantisation_scale", "requantisation_init_time")
    SCANLEN_MAX_FIELD_NUMBER: _ClassVar[int]
    BYTES_PER_SECOND_FIELD_NUMBER: _ClassVar[int]
    EXECUTION_BLOCK_ID_FIELD_NUMBER: _ClassVar[int]
    NUM_BITS_OUT_FIELD_NUMBER: _ClassVar[int]
    POLARISATIONS_FIELD_NUMBER: _ClassVar[int]
    CHANNELS_FIELD_NUMBER: _ClassVar[int]
    REQUANTISATION_SCALE_FIELD_NUMBER: _ClassVar[int]
    REQUANTISATION_INIT_TIME_FIELD_NUMBER: _ClassVar[int]
    scanlen_max: float
    bytes_per_second: float
    execution_block_id: str
    num_bits_out: int
    polarisations: str
    channels: _containers.RepeatedScalarFieldContainer[int]
    requantisation_scale: float
    requantisation_init_time: int
    def __init__(self, scanlen_max: _Optional[float] = ..., bytes_per_second: _Optional[float] = ..., execution_block_id: _Optional[str] = ..., num_bits_out: _Optional[int] = ..., polarisations: _Optional[str] = ..., channels: _Optional[_Iterable[int]] = ..., requantisation_scale: _Optional[float] = ..., requantisation_init_time: _Optional[int] = ...) -> None: ...

class StatScanConfiguration(_message.Message):
    __slots__ = ("execution_block_id", "processing_delay_ms", "req_time_bins", "req_freq_bins", "num_rebin")
    EXECUTION_BLOCK_ID_FIELD_NUMBER: _ClassVar[int]
    PROCESSING_DELAY_MS_FIELD_NUMBER: _ClassVar[int]
    REQ_TIME_BINS_FIELD_NUMBER: _ClassVar[int]
    REQ_FREQ_BINS_FIELD_NUMBER: _ClassVar[int]
    NUM_REBIN_FIELD_NUMBER: _ClassVar[int]
    execution_block_id: str
    processing_delay_ms: int
    req_time_bins: int
    req_freq_bins: int
    num_rebin: int
    def __init__(self, execution_block_id: _Optional[str] = ..., processing_delay_ms: _Optional[int] = ..., req_time_bins: _Optional[int] = ..., req_freq_bins: _Optional[int] = ..., num_rebin: _Optional[int] = ...) -> None: ...

class TestScanConfiguration(_message.Message):
    __slots__ = ("configuration",)
    class ConfigurationEntry(_message.Message):
        __slots__ = ("key", "value")
        KEY_FIELD_NUMBER: _ClassVar[int]
        VALUE_FIELD_NUMBER: _ClassVar[int]
        key: str
        value: str
        def __init__(self, key: _Optional[str] = ..., value: _Optional[str] = ...) -> None: ...
    CONFIGURATION_FIELD_NUMBER: _ClassVar[int]
    configuration: _containers.ScalarMap[str, str]
    def __init__(self, configuration: _Optional[_Mapping[str, str]] = ...) -> None: ...

class ScanConfiguration(_message.Message):
    __slots__ = ("smrb", "receive", "dsp_disk", "stat", "dsp_flow_through", "test")
    SMRB_FIELD_NUMBER: _ClassVar[int]
    RECEIVE_FIELD_NUMBER: _ClassVar[int]
    DSP_DISK_FIELD_NUMBER: _ClassVar[int]
    STAT_FIELD_NUMBER: _ClassVar[int]
    DSP_FLOW_THROUGH_FIELD_NUMBER: _ClassVar[int]
    TEST_FIELD_NUMBER: _ClassVar[int]
    smrb: SmrbScanConfiguration
    receive: ReceiveScanConfiguration
    dsp_disk: DspDiskScanConfiguration
    stat: StatScanConfiguration
    dsp_flow_through: DspFlowThroughScanConfiguration
    test: TestScanConfiguration
    def __init__(self, smrb: _Optional[_Union[SmrbScanConfiguration, _Mapping]] = ..., receive: _Optional[_Union[ReceiveScanConfiguration, _Mapping]] = ..., dsp_disk: _Optional[_Union[DspDiskScanConfiguration, _Mapping]] = ..., stat: _Optional[_Union[StatScanConfiguration, _Mapping]] = ..., dsp_flow_through: _Optional[_Union[DspFlowThroughScanConfiguration, _Mapping]] = ..., test: _Optional[_Union[TestScanConfiguration, _Mapping]] = ...) -> None: ...

class ConfigureScanRequest(_message.Message):
    __slots__ = ("scan_configuration", "dry_run")
    SCAN_CONFIGURATION_FIELD_NUMBER: _ClassVar[int]
    DRY_RUN_FIELD_NUMBER: _ClassVar[int]
    scan_configuration: ScanConfiguration
    dry_run: bool
    def __init__(self, scan_configuration: _Optional[_Union[ScanConfiguration, _Mapping]] = ..., dry_run: bool = ...) -> None: ...

class ConfigureScanResponse(_message.Message):
    __slots__ = ()
    def __init__(self) -> None: ...

class GetScanConfigurationRequest(_message.Message):
    __slots__ = ()
    def __init__(self) -> None: ...

class GetScanConfigurationResponse(_message.Message):
    __slots__ = ("scan_configuration",)
    SCAN_CONFIGURATION_FIELD_NUMBER: _ClassVar[int]
    scan_configuration: ScanConfiguration
    def __init__(self, scan_configuration: _Optional[_Union[ScanConfiguration, _Mapping]] = ...) -> None: ...

class DeconfigureScanRequest(_message.Message):
    __slots__ = ()
    def __init__(self) -> None: ...

class DeconfigureScanResponse(_message.Message):
    __slots__ = ()
    def __init__(self) -> None: ...

class StartScanRequest(_message.Message):
    __slots__ = ("scan_id",)
    SCAN_ID_FIELD_NUMBER: _ClassVar[int]
    scan_id: int
    def __init__(self, scan_id: _Optional[int] = ...) -> None: ...

class StartScanResponse(_message.Message):
    __slots__ = ()
    def __init__(self) -> None: ...

class StopScanRequest(_message.Message):
    __slots__ = ("end_time",)
    END_TIME_FIELD_NUMBER: _ClassVar[int]
    end_time: int
    def __init__(self, end_time: _Optional[int] = ...) -> None: ...

class StopScanResponse(_message.Message):
    __slots__ = ()
    def __init__(self) -> None: ...

class GetStateRequest(_message.Message):
    __slots__ = ()
    def __init__(self) -> None: ...

class GetStateResponse(_message.Message):
    __slots__ = ("state",)
    STATE_FIELD_NUMBER: _ClassVar[int]
    state: ObsState
    def __init__(self, state: _Optional[_Union[ObsState, str]] = ...) -> None: ...

class SmrbStatistics(_message.Message):
    __slots__ = ("nbufs", "bufsz", "written", "read", "full", "clear", "available")
    NBUFS_FIELD_NUMBER: _ClassVar[int]
    BUFSZ_FIELD_NUMBER: _ClassVar[int]
    WRITTEN_FIELD_NUMBER: _ClassVar[int]
    READ_FIELD_NUMBER: _ClassVar[int]
    FULL_FIELD_NUMBER: _ClassVar[int]
    CLEAR_FIELD_NUMBER: _ClassVar[int]
    AVAILABLE_FIELD_NUMBER: _ClassVar[int]
    nbufs: int
    bufsz: int
    written: int
    read: int
    full: int
    clear: int
    available: int
    def __init__(self, nbufs: _Optional[int] = ..., bufsz: _Optional[int] = ..., written: _Optional[int] = ..., read: _Optional[int] = ..., full: _Optional[int] = ..., clear: _Optional[int] = ..., available: _Optional[int] = ...) -> None: ...

class SmrbMonitorData(_message.Message):
    __slots__ = ("data", "weights")
    DATA_FIELD_NUMBER: _ClassVar[int]
    WEIGHTS_FIELD_NUMBER: _ClassVar[int]
    data: SmrbStatistics
    weights: SmrbStatistics
    def __init__(self, data: _Optional[_Union[SmrbStatistics, _Mapping]] = ..., weights: _Optional[_Union[SmrbStatistics, _Mapping]] = ...) -> None: ...

class ReceiveMonitorData(_message.Message):
    __slots__ = ("receive_rate", "data_received", "data_drop_rate", "data_dropped", "misordered_packets", "misordered_packet_rate", "malformed_packets", "malformed_packet_rate", "misdirected_packets", "misdirected_packet_rate", "checksum_failure_packets", "checksum_failure_packet_rate", "timestamp_sync_error_packets", "timestamp_sync_error_packet_rate", "seq_number_sync_error_packets", "seq_number_sync_error_packet_rate", "no_valid_station_beam_packets", "no_valid_station_beam_packet_rate", "no_valid_pst_beam_packets", "no_valid_pst_beam_packet_rate", "no_valid_polarisation_correction_packets", "no_valid_polarisation_correction_packet_rate")
    RECEIVE_RATE_FIELD_NUMBER: _ClassVar[int]
    DATA_RECEIVED_FIELD_NUMBER: _ClassVar[int]
    DATA_DROP_RATE_FIELD_NUMBER: _ClassVar[int]
    DATA_DROPPED_FIELD_NUMBER: _ClassVar[int]
    MISORDERED_PACKETS_FIELD_NUMBER: _ClassVar[int]
    MISORDERED_PACKET_RATE_FIELD_NUMBER: _ClassVar[int]
    MALFORMED_PACKETS_FIELD_NUMBER: _ClassVar[int]
    MALFORMED_PACKET_RATE_FIELD_NUMBER: _ClassVar[int]
    MISDIRECTED_PACKETS_FIELD_NUMBER: _ClassVar[int]
    MISDIRECTED_PACKET_RATE_FIELD_NUMBER: _ClassVar[int]
    CHECKSUM_FAILURE_PACKETS_FIELD_NUMBER: _ClassVar[int]
    CHECKSUM_FAILURE_PACKET_RATE_FIELD_NUMBER: _ClassVar[int]
    TIMESTAMP_SYNC_ERROR_PACKETS_FIELD_NUMBER: _ClassVar[int]
    TIMESTAMP_SYNC_ERROR_PACKET_RATE_FIELD_NUMBER: _ClassVar[int]
    SEQ_NUMBER_SYNC_ERROR_PACKETS_FIELD_NUMBER: _ClassVar[int]
    SEQ_NUMBER_SYNC_ERROR_PACKET_RATE_FIELD_NUMBER: _ClassVar[int]
    NO_VALID_STATION_BEAM_PACKETS_FIELD_NUMBER: _ClassVar[int]
    NO_VALID_STATION_BEAM_PACKET_RATE_FIELD_NUMBER: _ClassVar[int]
    NO_VALID_PST_BEAM_PACKETS_FIELD_NUMBER: _ClassVar[int]
    NO_VALID_PST_BEAM_PACKET_RATE_FIELD_NUMBER: _ClassVar[int]
    NO_VALID_POLARISATION_CORRECTION_PACKETS_FIELD_NUMBER: _ClassVar[int]
    NO_VALID_POLARISATION_CORRECTION_PACKET_RATE_FIELD_NUMBER: _ClassVar[int]
    receive_rate: float
    data_received: int
    data_drop_rate: float
    data_dropped: int
    misordered_packets: int
    misordered_packet_rate: float
    malformed_packets: int
    malformed_packet_rate: float
    misdirected_packets: int
    misdirected_packet_rate: float
    checksum_failure_packets: int
    checksum_failure_packet_rate: float
    timestamp_sync_error_packets: int
    timestamp_sync_error_packet_rate: float
    seq_number_sync_error_packets: int
    seq_number_sync_error_packet_rate: float
    no_valid_station_beam_packets: int
    no_valid_station_beam_packet_rate: float
    no_valid_pst_beam_packets: int
    no_valid_pst_beam_packet_rate: float
    no_valid_polarisation_correction_packets: int
    no_valid_polarisation_correction_packet_rate: float
    def __init__(self, receive_rate: _Optional[float] = ..., data_received: _Optional[int] = ..., data_drop_rate: _Optional[float] = ..., data_dropped: _Optional[int] = ..., misordered_packets: _Optional[int] = ..., misordered_packet_rate: _Optional[float] = ..., malformed_packets: _Optional[int] = ..., malformed_packet_rate: _Optional[float] = ..., misdirected_packets: _Optional[int] = ..., misdirected_packet_rate: _Optional[float] = ..., checksum_failure_packets: _Optional[int] = ..., checksum_failure_packet_rate: _Optional[float] = ..., timestamp_sync_error_packets: _Optional[int] = ..., timestamp_sync_error_packet_rate: _Optional[float] = ..., seq_number_sync_error_packets: _Optional[int] = ..., seq_number_sync_error_packet_rate: _Optional[float] = ..., no_valid_station_beam_packets: _Optional[int] = ..., no_valid_station_beam_packet_rate: _Optional[float] = ..., no_valid_pst_beam_packets: _Optional[int] = ..., no_valid_pst_beam_packet_rate: _Optional[float] = ..., no_valid_polarisation_correction_packets: _Optional[int] = ..., no_valid_polarisation_correction_packet_rate: _Optional[float] = ...) -> None: ...

class DspDiskMonitorData(_message.Message):
    __slots__ = ("disk_capacity", "disk_available_bytes", "bytes_written", "write_rate")
    DISK_CAPACITY_FIELD_NUMBER: _ClassVar[int]
    DISK_AVAILABLE_BYTES_FIELD_NUMBER: _ClassVar[int]
    BYTES_WRITTEN_FIELD_NUMBER: _ClassVar[int]
    WRITE_RATE_FIELD_NUMBER: _ClassVar[int]
    disk_capacity: int
    disk_available_bytes: int
    bytes_written: int
    write_rate: float
    def __init__(self, disk_capacity: _Optional[int] = ..., disk_available_bytes: _Optional[int] = ..., bytes_written: _Optional[int] = ..., write_rate: _Optional[float] = ...) -> None: ...

class DspFlowThroughMonitorData(_message.Message):
    __slots__ = ()
    def __init__(self) -> None: ...

class StatMonitorData(_message.Message):
    __slots__ = ("mean_frequency_avg", "mean_frequency_avg_masked", "variance_frequency_avg", "variance_frequency_avg_masked", "mean_spectrum", "variance_spectrum", "mean_spectral_power", "max_spectral_power", "histogram_1d_freq_avg", "histogram_1d_freq_avg_masked", "histogram_rebinned_2d_freq_avg", "histogram_rebinned_2d_freq_avg_masked", "histogram_rebinned_1d_freq_avg", "histogram_rebinned_1d_freq_avg_masked", "num_clipped_samples_spectrum", "num_clipped_samples", "num_clipped_samples_masked", "spectrogram", "timeseries", "timeseries_masked")
    MEAN_FREQUENCY_AVG_FIELD_NUMBER: _ClassVar[int]
    MEAN_FREQUENCY_AVG_MASKED_FIELD_NUMBER: _ClassVar[int]
    VARIANCE_FREQUENCY_AVG_FIELD_NUMBER: _ClassVar[int]
    VARIANCE_FREQUENCY_AVG_MASKED_FIELD_NUMBER: _ClassVar[int]
    MEAN_SPECTRUM_FIELD_NUMBER: _ClassVar[int]
    VARIANCE_SPECTRUM_FIELD_NUMBER: _ClassVar[int]
    MEAN_SPECTRAL_POWER_FIELD_NUMBER: _ClassVar[int]
    MAX_SPECTRAL_POWER_FIELD_NUMBER: _ClassVar[int]
    HISTOGRAM_1D_FREQ_AVG_FIELD_NUMBER: _ClassVar[int]
    HISTOGRAM_1D_FREQ_AVG_MASKED_FIELD_NUMBER: _ClassVar[int]
    HISTOGRAM_REBINNED_2D_FREQ_AVG_FIELD_NUMBER: _ClassVar[int]
    HISTOGRAM_REBINNED_2D_FREQ_AVG_MASKED_FIELD_NUMBER: _ClassVar[int]
    HISTOGRAM_REBINNED_1D_FREQ_AVG_FIELD_NUMBER: _ClassVar[int]
    HISTOGRAM_REBINNED_1D_FREQ_AVG_MASKED_FIELD_NUMBER: _ClassVar[int]
    NUM_CLIPPED_SAMPLES_SPECTRUM_FIELD_NUMBER: _ClassVar[int]
    NUM_CLIPPED_SAMPLES_FIELD_NUMBER: _ClassVar[int]
    NUM_CLIPPED_SAMPLES_MASKED_FIELD_NUMBER: _ClassVar[int]
    SPECTROGRAM_FIELD_NUMBER: _ClassVar[int]
    TIMESERIES_FIELD_NUMBER: _ClassVar[int]
    TIMESERIES_MASKED_FIELD_NUMBER: _ClassVar[int]
    mean_frequency_avg: _containers.RepeatedScalarFieldContainer[float]
    mean_frequency_avg_masked: _containers.RepeatedScalarFieldContainer[float]
    variance_frequency_avg: _containers.RepeatedScalarFieldContainer[float]
    variance_frequency_avg_masked: _containers.RepeatedScalarFieldContainer[float]
    mean_spectrum: _containers.RepeatedScalarFieldContainer[float]
    variance_spectrum: _containers.RepeatedScalarFieldContainer[float]
    mean_spectral_power: _containers.RepeatedScalarFieldContainer[float]
    max_spectral_power: _containers.RepeatedScalarFieldContainer[float]
    histogram_1d_freq_avg: _containers.RepeatedScalarFieldContainer[int]
    histogram_1d_freq_avg_masked: _containers.RepeatedScalarFieldContainer[int]
    histogram_rebinned_2d_freq_avg: _containers.RepeatedScalarFieldContainer[int]
    histogram_rebinned_2d_freq_avg_masked: _containers.RepeatedScalarFieldContainer[int]
    histogram_rebinned_1d_freq_avg: _containers.RepeatedScalarFieldContainer[int]
    histogram_rebinned_1d_freq_avg_masked: _containers.RepeatedScalarFieldContainer[int]
    num_clipped_samples_spectrum: _containers.RepeatedScalarFieldContainer[int]
    num_clipped_samples: _containers.RepeatedScalarFieldContainer[int]
    num_clipped_samples_masked: _containers.RepeatedScalarFieldContainer[int]
    spectrogram: _containers.RepeatedScalarFieldContainer[float]
    timeseries: _containers.RepeatedScalarFieldContainer[float]
    timeseries_masked: _containers.RepeatedScalarFieldContainer[float]
    def __init__(self, mean_frequency_avg: _Optional[_Iterable[float]] = ..., mean_frequency_avg_masked: _Optional[_Iterable[float]] = ..., variance_frequency_avg: _Optional[_Iterable[float]] = ..., variance_frequency_avg_masked: _Optional[_Iterable[float]] = ..., mean_spectrum: _Optional[_Iterable[float]] = ..., variance_spectrum: _Optional[_Iterable[float]] = ..., mean_spectral_power: _Optional[_Iterable[float]] = ..., max_spectral_power: _Optional[_Iterable[float]] = ..., histogram_1d_freq_avg: _Optional[_Iterable[int]] = ..., histogram_1d_freq_avg_masked: _Optional[_Iterable[int]] = ..., histogram_rebinned_2d_freq_avg: _Optional[_Iterable[int]] = ..., histogram_rebinned_2d_freq_avg_masked: _Optional[_Iterable[int]] = ..., histogram_rebinned_1d_freq_avg: _Optional[_Iterable[int]] = ..., histogram_rebinned_1d_freq_avg_masked: _Optional[_Iterable[int]] = ..., num_clipped_samples_spectrum: _Optional[_Iterable[int]] = ..., num_clipped_samples: _Optional[_Iterable[int]] = ..., num_clipped_samples_masked: _Optional[_Iterable[int]] = ..., spectrogram: _Optional[_Iterable[float]] = ..., timeseries: _Optional[_Iterable[float]] = ..., timeseries_masked: _Optional[_Iterable[float]] = ...) -> None: ...

class TestMonitorData(_message.Message):
    __slots__ = ("data",)
    class DataEntry(_message.Message):
        __slots__ = ("key", "value")
        KEY_FIELD_NUMBER: _ClassVar[int]
        VALUE_FIELD_NUMBER: _ClassVar[int]
        key: str
        value: str
        def __init__(self, key: _Optional[str] = ..., value: _Optional[str] = ...) -> None: ...
    DATA_FIELD_NUMBER: _ClassVar[int]
    data: _containers.ScalarMap[str, str]
    def __init__(self, data: _Optional[_Mapping[str, str]] = ...) -> None: ...

class MonitorRequest(_message.Message):
    __slots__ = ("polling_rate",)
    POLLING_RATE_FIELD_NUMBER: _ClassVar[int]
    polling_rate: int
    def __init__(self, polling_rate: _Optional[int] = ...) -> None: ...

class MonitorData(_message.Message):
    __slots__ = ("smrb", "receive", "dsp_disk", "stat", "dsp_flow_through", "test")
    SMRB_FIELD_NUMBER: _ClassVar[int]
    RECEIVE_FIELD_NUMBER: _ClassVar[int]
    DSP_DISK_FIELD_NUMBER: _ClassVar[int]
    STAT_FIELD_NUMBER: _ClassVar[int]
    DSP_FLOW_THROUGH_FIELD_NUMBER: _ClassVar[int]
    TEST_FIELD_NUMBER: _ClassVar[int]
    smrb: SmrbMonitorData
    receive: ReceiveMonitorData
    dsp_disk: DspDiskMonitorData
    stat: StatMonitorData
    dsp_flow_through: DspFlowThroughMonitorData
    test: TestMonitorData
    def __init__(self, smrb: _Optional[_Union[SmrbMonitorData, _Mapping]] = ..., receive: _Optional[_Union[ReceiveMonitorData, _Mapping]] = ..., dsp_disk: _Optional[_Union[DspDiskMonitorData, _Mapping]] = ..., stat: _Optional[_Union[StatMonitorData, _Mapping]] = ..., dsp_flow_through: _Optional[_Union[DspFlowThroughMonitorData, _Mapping]] = ..., test: _Optional[_Union[TestMonitorData, _Mapping]] = ...) -> None: ...

class MonitorResponse(_message.Message):
    __slots__ = ("monitor_data",)
    MONITOR_DATA_FIELD_NUMBER: _ClassVar[int]
    monitor_data: MonitorData
    def __init__(self, monitor_data: _Optional[_Union[MonitorData, _Mapping]] = ...) -> None: ...

class AbortRequest(_message.Message):
    __slots__ = ()
    def __init__(self) -> None: ...

class AbortResponse(_message.Message):
    __slots__ = ()
    def __init__(self) -> None: ...

class ResetRequest(_message.Message):
    __slots__ = ()
    def __init__(self) -> None: ...

class ResetResponse(_message.Message):
    __slots__ = ()
    def __init__(self) -> None: ...

class RestartRequest(_message.Message):
    __slots__ = ()
    def __init__(self) -> None: ...

class RestartResponse(_message.Message):
    __slots__ = ()
    def __init__(self) -> None: ...

class GoToFaultRequest(_message.Message):
    __slots__ = ("error_message",)
    ERROR_MESSAGE_FIELD_NUMBER: _ClassVar[int]
    error_message: str
    def __init__(self, error_message: _Optional[str] = ...) -> None: ...

class GoToFaultResponse(_message.Message):
    __slots__ = ()
    def __init__(self) -> None: ...

class ConnectionRequest(_message.Message):
    __slots__ = ("client_id",)
    CLIENT_ID_FIELD_NUMBER: _ClassVar[int]
    client_id: str
    def __init__(self, client_id: _Optional[str] = ...) -> None: ...

class ConnectionResponse(_message.Message):
    __slots__ = ("service_name", "uuid")
    SERVICE_NAME_FIELD_NUMBER: _ClassVar[int]
    UUID_FIELD_NUMBER: _ClassVar[int]
    service_name: str
    uuid: str
    def __init__(self, service_name: _Optional[str] = ..., uuid: _Optional[str] = ...) -> None: ...

class GetEnvironmentRequest(_message.Message):
    __slots__ = ()
    def __init__(self) -> None: ...

class EnvValue(_message.Message):
    __slots__ = ("string_value", "float_value", "signed_int_value", "unsigned_int_value")
    STRING_VALUE_FIELD_NUMBER: _ClassVar[int]
    FLOAT_VALUE_FIELD_NUMBER: _ClassVar[int]
    SIGNED_INT_VALUE_FIELD_NUMBER: _ClassVar[int]
    UNSIGNED_INT_VALUE_FIELD_NUMBER: _ClassVar[int]
    string_value: str
    float_value: float
    signed_int_value: int
    unsigned_int_value: int
    def __init__(self, string_value: _Optional[str] = ..., float_value: _Optional[float] = ..., signed_int_value: _Optional[int] = ..., unsigned_int_value: _Optional[int] = ...) -> None: ...

class GetEnvironmentResponse(_message.Message):
    __slots__ = ("values",)
    class ValuesEntry(_message.Message):
        __slots__ = ("key", "value")
        KEY_FIELD_NUMBER: _ClassVar[int]
        VALUE_FIELD_NUMBER: _ClassVar[int]
        key: str
        value: EnvValue
        def __init__(self, key: _Optional[str] = ..., value: _Optional[_Union[EnvValue, _Mapping]] = ...) -> None: ...
    VALUES_FIELD_NUMBER: _ClassVar[int]
    values: _containers.MessageMap[str, EnvValue]
    def __init__(self, values: _Optional[_Mapping[str, EnvValue]] = ...) -> None: ...

class SetLogLevelRequest(_message.Message):
    __slots__ = ("log_level",)
    LOG_LEVEL_FIELD_NUMBER: _ClassVar[int]
    log_level: LogLevel
    def __init__(self, log_level: _Optional[_Union[LogLevel, str]] = ...) -> None: ...

class SetLogLevelResponse(_message.Message):
    __slots__ = ()
    def __init__(self) -> None: ...

class GetLogLevelRequest(_message.Message):
    __slots__ = ()
    def __init__(self) -> None: ...

class GetLogLevelResponse(_message.Message):
    __slots__ = ("log_level",)
    LOG_LEVEL_FIELD_NUMBER: _ClassVar[int]
    log_level: LogLevel
    def __init__(self, log_level: _Optional[_Union[LogLevel, str]] = ...) -> None: ...

class HealthCheckRequest(_message.Message):
    __slots__ = ("health_check_interval",)
    HEALTH_CHECK_INTERVAL_FIELD_NUMBER: _ClassVar[int]
    health_check_interval: int
    def __init__(self, health_check_interval: _Optional[int] = ...) -> None: ...

class HealthCheckResponse(_message.Message):
    __slots__ = ("service_name", "uuid", "obs_state", "fault_message")
    SERVICE_NAME_FIELD_NUMBER: _ClassVar[int]
    UUID_FIELD_NUMBER: _ClassVar[int]
    OBS_STATE_FIELD_NUMBER: _ClassVar[int]
    FAULT_MESSAGE_FIELD_NUMBER: _ClassVar[int]
    service_name: str
    uuid: str
    obs_state: ObsState
    fault_message: str
    def __init__(self, service_name: _Optional[str] = ..., uuid: _Optional[str] = ..., obs_state: _Optional[_Union[ObsState, str]] = ..., fault_message: _Optional[str] = ...) -> None: ...
