# -*- coding: utf-8 -*-
#
# This file is part of the SKA PST project
#
# Distributed under the terms of the BSD 3-clause new license.
# See LICENSE for more info.

"""Package for math utility functions."""

import numpy as np


def round_(value: float, precision: int = 16) -> float:
    """
    Round a value to a given precision.

    :param value: the value to round to a given precision
    :type value: float
    :param precision: the number of digits of precision, defaults to 16
    :type precision: int, optional
    :return: the value rounded to a given precision.
    :rtype: float
    """
    # 0.0 is always going to round to zero
    if value == 0.0:
        return value

    num_value_digits = int(np.ceil(np.log10(np.abs(value))))
    decimals = precision - num_value_digits

    # a negative number of decimals is allowed as this rounds to
    # 10^(-decimals) so for -1 we round to 10.
    return np.around(value, decimals=decimals)
