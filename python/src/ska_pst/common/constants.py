# -*- coding: utf-8 -*-
#
# This file is part of the SKA PST project
#
# Distributed under the terms of the BSD 3-clause new license.
# See LICENSE for more info.

"""Package used to define constants used within the PST Python code."""

SIZE_OF_FLOAT32_IN_BYTES = 4
"""Number of bytes used for a single precision float (i.e. f32)."""

BITS_PER_BYTE = 8
"""Number of bits in a byte"""

WEIGHTS_NBITS = 16
"""Number of bits per weight."""

WEIGHTS_NDIM = 1
"""Number of dimensions in the weights values (i.e. weights are real valued)."""

WEIGHTS_NPOL = 1
"""Number of polarisations used for weights."""

COMPLEX_NDIMS: int = 2
"""Number of dimensions in complex valued data."""

REAL_NDIMS: int = 1
"""Number of dimensions in real valued data."""

GIGABITS_PER_BYTE = BITS_PER_BYTE * 1e-9
"""Scale factor used to convert from bytes to gigabits."""

DEFAULT_HEALTH_CHECK_INTERVAL_MS: int = 1000
"""The default interval, in milliseconds, at which the CORE apps should report their health state."""

DEFAULT_MONITORING_INTERVAL_MS: int = 5000
"""The default interval, in milliseconds, at which the CORE apps should report monitoring data."""

MEGA_HERTZ = 1_000_000
"""CSP sends values in SI units, including frequencies as Hz."""

KILO_HERTZ_PER_MEGA_HERTZ = 1_000
"""A scale factor to convert between kHz and MHz."""

MICROSECS_PER_SEC = 1_000_000
"""Number of microseconds in a second."""
