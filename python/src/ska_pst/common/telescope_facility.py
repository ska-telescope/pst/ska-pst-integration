# -*- coding: utf-8 -*-
#
# This file is part of the SKA PST project.
#
# Distributed under the terms of the BSD 3-clause new license.
# See LICENSE for more info.
"""Module containing an enum that represents the different telescope facilities (i.e. Mid vs Low)."""

from __future__ import annotations

import enum


class TelescopeFacilityEnum(enum.IntEnum):
    """Enum representing the different telescope facilities within SKAO."""

    Low = 1
    """Used to present that the functionality is for the SKA-Low facility."""

    Mid = 2
    """Used to present that the functionality is for the SKA-Mid facility."""

    @property
    def telescope(self: TelescopeFacilityEnum) -> str:
        """
        Get the SKA telescope that the facility enum represents.

        :return: the SKA telescope that the facility enum represents.
        :rtype: str
        """
        return f"SKA{self.name}"

    @staticmethod
    def from_telescope(telescope: str) -> TelescopeFacilityEnum:
        """
        Get the enum value based on telescope string.

        The ``telescope`` parameter must be either "SKALow" or "SKAMid".

        :param telescope: the name of the telescope to get the enum for.
        :type telescope: str
        :return: the enum value based on telescope string.
        :rtype: TelescopeFacilityEnum
        """
        assert telescope.startswith("SKA")
        facility_str = telescope[3:]

        return TelescopeFacilityEnum[facility_str]
