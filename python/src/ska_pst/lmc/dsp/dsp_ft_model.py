# -*- coding: utf-8 -*-
#
# This file is part of the SKA PST project.
#
# Distributed under the terms of the BSD 3-clause new license.
# See LICENSE for more info.
"""Module for providing the model data classes for DSP Flow Through."""

from __future__ import annotations

__all__ = [
    "DspFlowThroughMonitorData",
    "DspFlowThroughMonitorDataStore",
    "DspFlowThroughSubbandMonitorData",
]

from dataclasses import dataclass

from ska_pst.lmc.component import MonitorDataStore


@dataclass
class DspFlowThroughSubbandMonitorData:
    """
    A data class to represent a subband monitoring data record.

    This class is used to report on a subband specific monitoring data.

    The monitoring attributes will added in a future PI. For now this is an empty class.
    """


@dataclass
class DspFlowThroughMonitorData:
    """
    A data class to represent the DSP.FT monitoring across all subbands.

    This class is used to model the combined subband data for the DSP Flow Through.

    The monitoring attributes will added in a future PI. For now this is an empty class.
    """


class DspFlowThroughMonitorDataStore(
    MonitorDataStore[DspFlowThroughSubbandMonitorData, DspFlowThroughMonitorData]
):
    """
    Data store used to aggregate the subband data for DSP Flow Through.

    This class is a stub at the moment to allow until the monitoring attributes
    are determined in a future PI.
    """

    @property
    def monitor_data(self: DspFlowThroughMonitorDataStore) -> DspFlowThroughMonitorData:
        """
        Get current monitoring data for DSP.

        This returns the latest monitoring data calculated from the current
        subband data. If no subband data is available then the response is
        a default :py:class:`DspFlowThroughMonitorData` object.
        """
        return DspFlowThroughMonitorData()
