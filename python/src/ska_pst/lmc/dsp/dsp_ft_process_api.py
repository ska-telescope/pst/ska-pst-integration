# -*- coding: utf-8 -*-
#
# This file is part of the SKA PST project.
#
# Distributed under the terms of the BSD 3-clause new license.
# See LICENSE for more info.
"""
Module for providing the API to be communicate with the DSP process.

The :py:class:`PstDspFlowThroughProcessApiSimulator` is used in testing or
simulation mode, while the :py:class:`PstDspFlowThroughProcessApiGrpc` is used
to connect to a remote application that exposes a gRPC API.
"""

from __future__ import annotations

import logging
from typing import Callable

from overrides import override
from ska_pst.grpc.lmc.ska_pst_lmc_pb2 import (
    BeamConfiguration,
    DspFlowThroughBeamConfiguration,
    DspFlowThroughScanConfiguration,
    MonitorData,
    ScanConfiguration,
)
from ska_pst.lmc.component import PstProcessApi, PstProcessApiGrpc, PstProcessApiSimulator
from ska_pst.lmc.dsp.dsp_ft_model import DspFlowThroughSubbandMonitorData
from ska_pst.lmc.dsp.dsp_ft_simulator import PstDspFlowThroughSimulator
from ska_pst.lmc.dsp.dsp_util import generate_dsp_scan_request

__all__ = [
    "PstDspFlowThroughProcessApi",
    "PstDspFlowThroughProcessApiSimulator",
]


class PstDspFlowThroughProcessApi(PstProcessApi):
    """
    Abstract class for the API of the DSP process.

    This extends from :py:class:`PstProcessApi` but
    provides the specific method of getting the monitoring
    data.
    """


class PstDspFlowThroughProcessApiSimulator(
    PstProcessApiSimulator[DspFlowThroughSubbandMonitorData, PstDspFlowThroughSimulator],
    PstDspFlowThroughProcessApi,
):
    """A simulator implementation version of the  API of `PstDspFlowThroughProcessApi`."""

    def __init__(
        self: PstDspFlowThroughProcessApiSimulator,
        simulator: PstDspFlowThroughSimulator | None = None,
        logger: logging.Logger | None = None,
    ) -> None:
        """
        Initialise the API.

        :param logger: the logger to use for the API.
        :param component_state_callback: this allows the API to call back to the component manager / TANGO
            device to deal with state model changes.
        :param simulator: the simulator instance to use in the API.
        """
        simulator = simulator or PstDspFlowThroughSimulator()
        super().__init__(logger=logger, simulator=simulator)


class PstDspFlowThroughProcessApiGrpc(PstProcessApiGrpc, PstDspFlowThroughProcessApi):
    """This is an gRPC implementation of the `PstDspFlowThroughProcessApi` API.

    This uses an instance of a `PstGrpcLmcClient` to send requests through
    to the DSP.FT application. Instances of this class should be per
    subband, rather than one for all of DSP.FT as a whole.
    """

    @override
    def _get_configure_beam_request(
        self: PstDspFlowThroughProcessApiGrpc, configuration: dict
    ) -> BeamConfiguration:
        return BeamConfiguration(dsp_flow_through=DspFlowThroughBeamConfiguration(**configuration))

    @override
    def _handle_monitor_response(
        self: PstDspFlowThroughProcessApiGrpc,
        data: MonitorData,
        *,
        monitor_data_callback: Callable[..., None],
    ) -> None:
        # NOTE - in the future when we build out the monitoring attributes then
        # we would need to get the Protobuf message for the DSP.FT mode via something like
        # the following:
        #
        # dsp_ft_data: DspFlowThroughMonitorDataProtobuf = data.dsp_flow_through
        #
        monitor_data_callback(
            subband_id=1,
            subband_data=DspFlowThroughSubbandMonitorData(),
        )

    @override
    def _get_configure_scan_request(
        self: PstDspFlowThroughProcessApiGrpc, configure_parameters: dict
    ) -> ScanConfiguration:
        return ScanConfiguration(
            dsp_flow_through=DspFlowThroughScanConfiguration(
                **generate_dsp_scan_request(**configure_parameters)
            )
        )
