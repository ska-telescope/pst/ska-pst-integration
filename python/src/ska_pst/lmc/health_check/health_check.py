# -*- coding: utf-8 -*-
#
# This file is part of the SKA PST project.
#
# Distributed under the terms of the BSD 3-clause new license.
# See LICENSE.txt for more info.
"""Module of health check specific classes and functionality."""

from __future__ import annotations

import dataclasses
from typing import Any, Protocol

from ska_control_model import ObsState


@dataclasses.dataclass(kw_only=True)
class HealthCheckState:
    """
    A data class used to pass around health check state of a service.

    Since the LMC manages all the services this data class includes fields
    such as ``service_name`` and ``service_uuid`` to allow identifying
    which instance of a service produced the state object.
    """

    service_name: str
    """The name of the service that the health check state is for."""

    service_uuid: str
    """
    The UUID of the instance of the service.

    This is the value obtained from calls to ``connect`` of the service.
    A difference between this and the ``actual_service_uuid``
    attribute may represent an issue, such as the service instance had
    restarted.
    """

    actual_service_uuid: str | None = None
    """
    The UUID received from the remote service on the recent health check.

    If the service has restarted an instance of this class may not
    have received a valid response and the ``exception`` field should
    be set.

    Under normal processing this value should be set and equal to
    the ``service_uuid`` value.
    """

    obs_state: ObsState | None = None
    """The current observation state of the remote service."""

    fault_message: str | None = None
    """The reason why the remote service is in ``ObsState.FAULT`` state."""

    exception: Exception | None = None
    """
    The exception that has been received during health check processing.

    A common reason for this is that the remote service has been restarted
    and as such the health check process will get an exception. By storing
    the error the health check processing in a component manager can
    determine how to handler the error.
    """


class HealthCheckHandler(Protocol):
    """A protocol classes can implement to be used as a health check state handler."""

    def handle_health_check_state(self: HealthCheckHandler, state: HealthCheckState, **kwargs: Any) -> None:
        """
        Handle a health check state of a remote service.

        :param state: the current health check state of the remote service.
        :type state: HealthCheckState
        """
