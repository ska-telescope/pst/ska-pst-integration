# -*- coding: utf-8 -*-
#
# This file is part of the SKA PST project.
#
# Distributed under the terms of the BSD 3-clause new license.
# See LICENSE for more info.
"""This module defines classes related to subcomponent events."""

from __future__ import annotations

import dataclasses

from ska_control_model import HealthState, ObsState


@dataclasses.dataclass(kw_only=True)
class SubcomponentEventMessage:
    """
    A data class used for passing events messages from subcomponents around.

    When an event, such as health state change or going into a FAULT
    state, a subcomponent can create a message of this type and place
    it on the event queue that the BEAM component manager can listen to.

    This class is designed to avoid the anti-pattern of
    callback hell.  Rather than the BEAM component manager having to pass
    multiple callable functions to subcomponents or providing a reference
    to itself in the subcomponent managers, only a :py:class:`queue.Queue` is
    needed to be shared and the BEAM component manger can handle the events
    with a background task.
    """

    subcomponent_name: str
    """The name of the subcomponent that created the event."""

    health_state: HealthState
    """The current health state of the subcomponent."""

    obs_state: ObsState
    """The current observing state of the subcomponent."""
