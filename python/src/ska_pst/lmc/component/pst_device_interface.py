# -*- coding: utf-8 -*-
#
# This file is part of the SKA PST project.
#
# Distributed under the terms of the BSD 3-clause new license.
# See LICENSE for more info.
"""Module provides an purely abstract view of a PST Device."""

from __future__ import annotations

from typing import Any

from ska_control_model import CommunicationStatus, HealthState, ObsState

from ska_pst.common import TelescopeFacilityEnum


class PstDeviceInterface:
    """
    A purely abstract class to be implemented by TANGO device classes.

    This class is used to abstract away any TANGO functionality that component managers need to callback to
    the TANGO device and in turn allow passing of the TANGO device itself to the component manager but is
    abstracted. This class itself can be extended for a particular device/component_manager combination where
    there is a need for more specific functionality but without the need of exposing a callback.

    By being an abstract class this can be mocked to be used in testing.
    """

    def handle_communication_state_change(
        self: PstDeviceInterface, communication_state: CommunicationStatus
    ) -> None:
        """Handle a change in device's communication state."""
        raise NotImplementedError("PstDeviceInterface is abstract")

    def handle_component_state_change(self: PstDeviceInterface, *args: Any, **kwargs: Any) -> None:
        """Handle a change in the component's state."""
        raise NotImplementedError("PstDeviceInterface is abstract")

    def handle_attribute_value_update(self: PstDeviceInterface, attribute_name: str, value: Any) -> None:
        """
        Handle update of a device attribute value.

        :param attribute_name: the name of the attribute to update.
        :type attribute_name: str
        :param value: the new value of the attribute to update to.
        :type value: Any
        """
        raise NotImplementedError("PstDeviceInterface is abstract")

    @property
    def beam_id(self: PstDeviceInterface) -> int:
        """Get the beam id for the current device."""
        raise NotImplementedError("PstDeviceInterface is abstract")

    @property
    def device_name(self: PstDeviceInterface) -> str:
        """Get the name of the device."""
        raise NotImplementedError("PstDeviceInterface is abstract")

    def handle_fault(self: PstDeviceInterface, fault_msg: str) -> None:
        """Handle device going into a fault state."""
        raise NotImplementedError("PstDeviceInterface is abstract")

    @property
    def facility(self: PstDeviceInterface) -> TelescopeFacilityEnum:
        """Get the facility that this device is being used for."""
        raise NotImplementedError("PstDeviceInterface is abstract")

    @property
    def subsystem_id(self: PstDeviceInterface) -> str:
        """Get the sub-system id where device is deployed."""
        raise NotImplementedError("PstDeviceInterface is abstract")

    @property
    def obs_state(self: PstDeviceInterface) -> ObsState:
        """
        Get the current observing state of the device.

        :return: the current observing state of the device.
        :rtype: ObsState
        """
        raise NotImplementedError("PstDeviceInterface is abstract")

    def update_health_state(self: PstDeviceInterface, health_state: HealthState) -> None:
        """
        Update the health state of device.

        :param health_state: the new health state of the TANGO device.
        :type health_state: HealthState
        """
        raise NotImplementedError("PstDeviceInterface is abstract")

    @property
    def monitoring_polling_rate_ms(self: PstDeviceInterface) -> int:
        """Get the monitoring polling rate, in milliseconds."""
        raise NotImplementedError("PstDeviceInterface is abstract")

    @property
    def health_check_interval(self: PstDeviceInterface) -> int:
        """Get the health check interval, in milliseconds."""
        raise NotImplementedError("PstDeviceInterface is abstract")
