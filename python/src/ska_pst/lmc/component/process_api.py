# -*- coding: utf-8 -*-
#
# This file is part of the SKA PST project.
#
# Distributed under the terms of the BSD 3-clause new license.
# See LICENSE for more info.
"""
Module for providing the base abstract API for the PST.LMC processes.

This API is not a part of the component manager, as the component manager is also concerned with callbacks to
the TANGO device and has state model management. This API is expected to be used to call to an external
process or be simulated.
"""

from __future__ import annotations

__all__ = ["PstProcessApi"]

import logging
import threading
from typing import Any, Callable, Optional

from overrides import EnforceOverrides
from ska_control_model import LoggingLevel
from ska_pst.common.constants import DEFAULT_HEALTH_CHECK_INTERVAL_MS, DEFAULT_MONITORING_INTERVAL_MS
from ska_pst.lmc.health_check import HealthCheckHandler
from ska_pst.lmc.util.background_task import background_task


class PstProcessApi(EnforceOverrides):
    """Abstract class for the API of the PST.LMC processes like RECV, SMRB, etc."""

    def __init__(
        self: PstProcessApi,
        logger: logging.Logger | None = None,
    ) -> None:
        """
        Initialise the API.

        :param simulator: the simulator instance to use in the API.
        :param logger: the logger to use for the API.
        :param component_state_callback: this allows the API to call back to the component manager / TANGO
            device to deal with state model changes.
        """
        self._logger = logger or logging.getLogger(__name__)

    def connect(self: PstProcessApi) -> None:
        """Connect to the external process."""
        raise NotImplementedError("PstProcessApi is abstract class")

    def disconnect(self: PstProcessApi) -> None:
        """Disconnect from the external process."""
        raise NotImplementedError("PstProcessApi is abstract class")

    def validate_configure_beam(self: PstProcessApi, configuration: dict) -> None:
        """
        Validate a configure beam for service.

        :param configuration: Dictionary of resources to allocate.
        :raises ValidationError: if there an issue validating the request. The error message contains the
            details.
        """
        raise NotImplementedError("PstProcessApi is abstract class")

    def configure_beam(self: PstProcessApi, configuration: dict) -> None:
        """
        Configure beam for service.

        :param configuration: Dictionary of resources to allocate.
        """
        raise NotImplementedError("PstProcessApi is abstract class")

    def deconfigure_beam(self: PstProcessApi) -> None:
        """Deconfigure beam to release all resources."""
        raise NotImplementedError("PstProcessApi is abstract class")

    def validate_configure_scan(self: PstProcessApi, configuration: dict) -> None:
        """
        Validate a configure_scan request.

        :param configuration: the scan configuration for the device.
        :raises ValidationError: if there an issue validating the request. The error message contains the
            details.
        """
        raise NotImplementedError("PstProcessApi is abstract class")

    def configure_scan(self: PstProcessApi, configuration: dict) -> None:
        """
        Configure a scan.

        :param configuration: the scan configuration for the device.
        """
        raise NotImplementedError("PstProcessApi is abstract class")

    def deconfigure_scan(self: PstProcessApi) -> None:
        """Deconfigure a scan."""
        raise NotImplementedError("PstProcessApi is abstract class")

    def start_scan(self: PstProcessApi, scan_id: int, **kwargs: Any) -> None:
        """
        Start a scan.

        :param scan_id: the ID for the scan.
        :type scan_id: int
        :param kwargs: additional arguments, needed to allow for future proofing
            of scan request coming from TM / CSP.
        :type kwargs: dict
        """
        raise NotImplementedError("PstProcessApi is abstract class")

    def stop_scan(self: PstProcessApi) -> None:
        """Stop a scan."""
        raise NotImplementedError("PstProcessApi is abstract class")

    def abort(self: PstProcessApi) -> None:
        """Abort a scan."""
        raise NotImplementedError("PstProcessApi is abstract class")

    def reset(self: PstProcessApi) -> None:
        """Reset the component."""
        raise NotImplementedError("PstProcessApi is abstract class")

    def restart(self: PstProcessApi) -> None:
        """Restart the component."""
        raise NotImplementedError("PstProcessApi is abstract class")

    def go_to_fault(self: PstProcessApi) -> None:
        """
        Set remote service in a FAULT state.

        This doesn't take a callback as we want a synchronous call.
        """
        raise NotImplementedError("PstProcessApi is abstract class")

    @background_task
    def monitor(
        self: PstProcessApi,
        subband_monitor_data_callback: Callable[..., None],
        polling_rate: int = DEFAULT_MONITORING_INTERVAL_MS,
        monitor_abort_event: Optional[threading.Event] = None,
    ) -> None:
        """
        Monitor data of remote service.

        This needs to be implemented as a background task

        :param subband_monitor_data_callback: callback to use when there is an
            update of the sub-band monitor data.
        :param polling_rate: the rate, in milliseconds, at which the monitoring
            should poll. The default value is 5000ms (i.e. 5 seconds).
        :param monitor_abort_event: a :py:class:`threading.Event` that can be
            used to signal to stop monitoring. If not set then the background task
            will create one.
        """
        raise NotImplementedError("PstProcessApi is abstract class")

    def stop_monitoring(self: PstProcessApi) -> None:
        """Stop background monitoring."""
        raise NotImplementedError("PstProcessApi is abstract class")

    def get_env(self: PstProcessApi) -> dict:
        """Get the environment properties for the service."""
        raise NotImplementedError("PstProcessApi is abstract class")

    def set_log_level(self: PstProcessApi, log_level: LoggingLevel) -> None:
        """
        Set the LogLevel of the service.

        :param log_level: The required TANGO LoggingLevel
        :returns: None.
        """
        raise NotImplementedError("PstProcessApi is abstract class")

    @background_task
    def perform_health_check(
        self: PstProcessApi,
        health_check_handler: HealthCheckHandler,
        health_check_interval: int = DEFAULT_HEALTH_CHECK_INTERVAL_MS,
        health_check_abort_event: Optional[threading.Event] = None,
    ) -> None:
        """
        Perform health check of a process in the background.

        :param health_check_handler: an object that implements the
            ``HealthCheckHandler`` protocol.  Any health check state
            object that is returned from the service is delegated to
            this handler to be handled.
        :type health_check_handler: HealthCheckHandler
        :param health_check_interval: the interval, in milliseconds, at which
            health check should be perform, defaults to 1000 (i.e. 1 second).
        :type health_check_interval: int, optional
        :param health_check_abort_event: a threading primitive to be used
            to stop the health check by an external mechanism, defaults to None
        :type health_check_abort_event: Optional[threading.Event], optional
        """
        raise NotImplementedError("PstProcessApi is abstract class")

    def stop_health_check(self: PstProcessApi) -> None:
        """Stop performing health check of service."""
        raise NotImplementedError("PstProcessApi is abstract class")
