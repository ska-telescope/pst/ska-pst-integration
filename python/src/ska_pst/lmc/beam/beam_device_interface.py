# -*- coding: utf-8 -*-
#
# This file is part of the SKA PST project.
#
# Distributed under the terms of the BSD 3-clause new license.
# See LICENSE for more info.
"""Module provides an purely abstract view the BEAM.MGMT Device."""

from __future__ import annotations

from ska_control_model import ObsMode
from ska_pst.lmc.component import PstDeviceInterface


class PstBeamDeviceInterface(PstDeviceInterface):
    """
    A purely abstract class that that represents a BEAM.MGMT Device.

    This is implemented by `PstBeam` and used by the `PstBeamComponentManager`
    which needs a limited view of the TANGO device but without having full
    access to it.
    """

    @property
    def smrb_process_api_endpoint(self: PstBeamDeviceInterface) -> str:
        """Get the gRPC process API endpoint for SMRB.CORE."""
        raise NotImplementedError("PstBeamDeviceInterface is abstract")

    @property
    def recv_process_api_endpoint(self: PstBeamDeviceInterface) -> str:
        """Get the gRPC process API endpoint for SMRB.CORE."""
        raise NotImplementedError("PstBeamDeviceInterface is abstract")

    @property
    def dsp_disk_process_api_endpoint(self: PstBeamDeviceInterface) -> str:
        """Get the gRPC process API endpoint for DSP.DISK."""
        raise NotImplementedError("PstBeamDeviceInterface is abstract")

    @property
    def dsp_flow_through_process_api_endpoint(self: PstBeamDeviceInterface) -> str:
        """Get the gRPC process API endpoint for DSP.DISK."""
        raise NotImplementedError("PstBeamDeviceInterface is abstract")

    @property
    def stat_process_api_endpoint(self: PstBeamDeviceInterface) -> str:
        """Get the gRPC process API endpoint for STAT.CORE."""
        raise NotImplementedError("PstBeamDeviceInterface is abstract")

    @property
    def scan_output_dir_pattern(self: PstBeamDeviceInterface) -> str:
        """Get the pattern for directory used for scan output files."""
        raise NotImplementedError("PstBeamDeviceInterface is abstract")

    def update_obs_mode(self: PstBeamDeviceInterface, obs_mode: ObsMode) -> None:
        """
        Update the observation mode of the device.

        When the device has been configured for a scan it should be put into
        PULSAR_TIMING.

        When the device is in an IDLE state the
        observation mode should be set to IDLE too.

        When PST supports VLBI and the beam has been configured for VLBI then
        the observation mode should be set to VLBI not PULSAR_TIMING

        :param obs_mode: the observation mode enum value.
        :type obs_mode: ObsMode
        """
        raise NotImplementedError("PstBeamDeviceInterface is abstract")
