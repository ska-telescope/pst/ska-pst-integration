# -*- coding: utf-8 -*-
#
# This file is part of the SKA PST project.
#
# Distributed under the terms of the BSD 3-clause new license.
# See LICENSE for more info.
"""Module for class that is used in background processing that receives streamed results."""

from __future__ import annotations

__all__ = ["StreamingTask"]

import logging
import threading
from typing import Any, Callable, Generator, Generic, TypeVar

T = TypeVar("T")


class StreamingTask(Generic[T]):
    """
    A class used to handle the background task of streamed items of data.

    This class is used by background monitoring and health check results
    for gRPC services.  This has common code refactored out to handle
    the starting/stopping of the task, streaming of the results and
    handling of the results.
    """

    def __init__(
        self: StreamingTask,
        task_name: str,
        item_generator: Callable[[threading.Event], Generator[T, None, None]],
        item_handler: Callable[[T], None],
        exception_handler: Callable[[Exception], None] | None = None,
        logger: logging.Logger | None = None,
    ) -> None:
        """
        Create instance of a streaming task.

        This task takes an instance of a callable/partial function that takes
        a ``threading.Event`` returns a Python ``Generator`` which
        can be looped over.  This generator will yield an item when ever
        an item is ready and the task will then pass the item to the
        item handler.

        An optional exception handler can be provided.  If not provided the
        default implementation will log the exception and stop processing. No
        matter what, when an exception has been raised that background task will
        stop processing.

        :param task_name: the name of the task to use in logging
        :type task_name: str
        :param item_generator: a callable / partial function that takes a ``threading.Event``
            and returns a generator of items that need to be handled.
        :type item_generator: Callable[[threading.Event], Generator[T, None, None]]
        :param item_handler: the callback to use when an item is available.  This should
            not throw an exception.
        :type item_handler: Callable[[T], None]
        :param exception_handler: a callback that should be used if an exception has been
            raise. Defaults to just logging the exception.
        :type exception_handler: Callable[[Exception], None] | None, optional
        :param logger: an optional logger to use for logging warnings, defaults to None
        :type logger: logging.Logger | None, optional
        """
        self.task_name = task_name
        self._item_generator = item_generator
        self._item_handler = item_handler
        self._exception_handler = exception_handler
        self._logger = logger or logging.getLogger(__name__)
        self._running = False
        self._condvar = threading.Condition()
        self._abort_event: threading.Event | None = None

    def start(self: StreamingTask, abort_event: threading.Event | None = None, **kwargs: Any) -> None:
        """
        Start the streaming of results from the generator.

        :param abort_event: a threading primitive to use to signal stopping of the task, defaults to None
        :type abort_event: threading.Event | None, optional
        """
        self.stop()

        try:
            # this locks condition var
            with self._condvar:
                self._abort_event = abort_event or threading.Event()
                self._running = True

                self._logger.debug(f"{self.task_name} has started")
                # if this generator ends successful due to the abort event
                # set, then the cond var will be released
                for v in self._item_generator(self._abort_event):
                    self._item_handler(v)

            self._logger.debug(f"{self.task_name} has completed successfully")
        except Exception as e:
            # the condvar has been released
            self._logger.warning(f"Error while handing {self.task_name}.", exc_info=True)
            if self._exception_handler is not None:
                self._exception_handler(e)
        finally:
            self._logger.debug(f"{self.task_name} has completed, cleaning up")
            with self._condvar:
                self._running = False
                self._condvar.notify_all()

    def stop(self: StreamingTask) -> None:
        """
        Stop processing of background items.

        This will signal the generator, via the abort event used in the ``start()``,
        to stop streaming of items.
        """
        if not self._running:
            return

        if self._abort_event is not None:
            self._abort_event.set()

            # wait on the condvar so we know that the process has actually stopped
            with self._condvar:
                self._condvar.wait(0.1)
                self._abort_event = None

        self._running = False

    def is_running(self: StreamingTask) -> bool:
        """Get whether the task is running or not."""
        return self._running
