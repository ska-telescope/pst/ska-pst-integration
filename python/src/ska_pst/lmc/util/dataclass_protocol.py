# -*- coding: utf-8 -*-
#
# This file is part of the SKA PST project.
#
# Distributed under the terms of the BSD 3-clause new license.
# See LICENSE for more info.
"""This module is to create a Python protocol for asserting a class is dataclass."""

import dataclasses
from typing import Protocol, runtime_checkable


@runtime_checkable
@dataclasses.dataclass
class DataClass(Protocol):
    """A dataclass that can be converted to a dictionary."""
