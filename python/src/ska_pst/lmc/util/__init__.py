# -*- coding: utf-8 -*-
#
# This file is part of the SKA PST project.
#
# Distributed under the terms of the BSD 3-clause new license.
# See LICENSE for more info.
"""
This module covers common utility classes and functions of PST.LMC.

Functionality provided within this module is:

    * Validation of JSON requests
    * Background Task processing
    * Job processing, including running jobs in parallel/sequential and on remote Device TANGO devices
    * Custom timeout iterator (see :py:class:`TimeoutIterator`)
"""

__all__ = [
    "background_task",
    "BackgroundTask",
    "BackgroundTaskProcessor",
    "Callback",
    "DataClass",
    "LongRunningCommand",
    "LrcResult",
    "RunState",
    "StreamingTask",
    "TimeoutIterator",
]

from .dataclass_protocol import DataClass
from .background_task import BackgroundTaskProcessor, BackgroundTask, RunState, background_task
from .timeout_iterator import TimeoutIterator
from .callback import Callback
from .streaming_task import StreamingTask
from .long_running_command import LongRunningCommand, LrcResult
