# -*- coding: utf-8 -*-
#
# This file is part of the SKA PST project.
#
# Distributed under the terms of the BSD 3-clause new license.
# See LICENSE for more info.
"""This module is used as a utility module for dealing with validation."""
from __future__ import annotations

__all__ = ["pst_scan_schema_json"]


from ska_telmodel.csp.schema import get_csp_scan_schema
from ska_telmodel.csp.version import CSP_CONFIG_VER2_4


def pst_scan_schema_json(version: str = CSP_CONFIG_VER2_4, strict: bool = False) -> dict:
    """Get a JSON schema from the CSP Scan schema definition."""
    schema = get_csp_scan_schema(version=version, strict=strict)
    return schema.json_schema(schema_id=version)
