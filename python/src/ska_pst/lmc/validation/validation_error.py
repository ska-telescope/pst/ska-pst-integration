# -*- coding: utf-8 -*-
#
# This file is part of the SKA PST project.
#
# Distributed under the terms of the BSD 3-clause new license.
# See LICENSE for more info.
"""This module is used as a utility module for dealing with validation."""
from __future__ import annotations

__all__ = ["ValidationError"]

from typing import Any


class ValidationError(Exception):
    """Exception representing a validation exception."""

    def __init__(self: ValidationError, message: str, *args: Any) -> None:
        """Create instance of ValidationError."""
        self.message = message
        super().__init__(*args)

    def __str__(self: ValidationError) -> str:
        """Get string representation of exception."""
        return f"{self.message}"
