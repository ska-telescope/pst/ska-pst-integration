# -*- coding: utf-8 -*-
#
# This file is part of the SKA PST project
#
# Distributed under the terms of the BSD 3-clause new license.
# See LICENSE for more info.

"""Module used to generate scan configurations."""

from __future__ import annotations

__all__ = [
    "create_default_scan_config_generator",
    "create_fixed_scan_config_generator",
    "generate_eb_id",
    "generate_rf_config",
]

import dataclasses
import json
import logging
import random
import string
from dataclasses import field
from datetime import datetime
from typing import Any, List, Set, Tuple

import astropy.units as u
import numpy as np
from coolname import generate as generate_coolname
from ska_control_model import PstProcessingMode
from ska_pst.lmc.dsp import generate_dsp_scan_request
from ska_pst.lmc.receive import calculate_receive_subband_resources
from ska_pst.lmc.receive.receive_util import generate_recv_configure_scan_request
from ska_pst.lmc.smrb import calculate_smrb_subband_resources
from ska_pst.testutils.common import csp_scan_example_str
from ska_pst.testutils.pulsar import random_pulsar
from ska_telmodel.pst import get_pst_config_example
from ska_telmodel.pst.version import PST_CONFIG_VER2_5

from ska_pst.common import (
    CbfPstConfig,
    FrequencyBandConfig,
    TelescopeConfig,
    TelescopeFacilityEnum,
    convert_csp_config_to_pst_config,
    get_telescope_config,
)

DEFAULT_MAX_SCANLEN: float = 30.0

RECEPTOR_NAMES: List[str] = [f"SKA{n:03d}" for n in range(1, 37)]
MAX_VR_DATA_RATE: float = 2.0**30

ALLOWED_NBIT_OUT: List[int] = [1, 2, 4, 8, 16]

_logger = logging.getLogger(__name__)


def generate_general_metadata() -> dict:
    """
    Generate a dictionary of general metadata.

    This currently generates strings for:
    * ``observer_id``
    * ``project_id``
    * ``pointing_id``
    """

    def _generate_str(prefix: str) -> str:
        suffix = random.randint(1, 10)
        name = generate_coolname()[0]

        return f"{prefix}_{name}{suffix}"

    return {
        "observer_id": _generate_str("observer"),
        "project_id": _generate_str("project"),
        "pointing_id": _generate_str("pointing"),
    }


def generate_receptor_data(max_receptors: int = 4) -> dict:
    """Generate random configuration for receptors.

    This returns a dictionary that includes ``receptors`` and ``receptor_weights``.
    The weights is an array of floats that sums up to 1.0.

    :param max_receptors: the maximum number of receptors to generate, defaults to 4
    :type max_receptors: int, optional
    :return: a dictionary of receptors and weights.
    :rtype: dict
    """
    num_receptors = random.randint(1, max_receptors)
    receptor_names = random.choices(RECEPTOR_NAMES, k=num_receptors)
    receptor_names.sort()

    # use a Dirichlet distribution. the 50.0/num_receptors gives a scale factor that
    # ensures not all values are the same but also that none of them have a value of 1.0
    dist_weights = np.ones(num_receptors) * 50.0 / num_receptors
    weights = np.random.default_rng().dirichlet(dist_weights, size=1).flatten()

    # round to 2 decimal places and fix last value
    weights = weights.round(decimals=2)
    diff = 1.0 - np.sum(weights)
    if sum != 0.0:
        weights[-1] = np.around(weights[-1] - diff, decimals=2)

    return {
        "receptors": receptor_names,
        "receptor_weights": weights.tolist(),
    }


def calculate_resolution(udp_format: str, nchan: int, ndim: int, npol: int, nbits: int, **kwargs: Any) -> int:
    """Calculate resolution of UDP data.

    :param udp_format: the UDP format to use, this will determine the number of samples per UDP packet.
    :type udp_format: str
    :param nchan: the number of channels to calculated the resolution for.
    :type nchan: int
    :param ndim: the number of dimensions. For voltage data this should be 2 (i.e. complex data) and for
        weights it should be 1 (i.e. real valued)
    :type ndim: int
    :param npol: the number of polarisation for the data.
    :type npol: int
    :param nbits: the number of bits the data type is for,
    :type nbits: int
    :return: the number of bytes required to store UDP_NSAMP time samples in each chan, pol, dim.
    :rtype: int
    """
    if udp_format == "LowPST":
        nsamp_per_packet = 32
    else:
        nsamp_per_packet = 4

    return nsamp_per_packet * nchan * ndim * npol * nbits // 8


def create_default_scan_config_generator(
    beam_id: int = 1,
    frequency_band: str = "low",
    max_scan_length: float = DEFAULT_MAX_SCANLEN,
    max_data_generated_size: int | None = None,
    logger: logging.Logger | None = None,
    **kwargs: Any,
) -> ScanConfigGenerator:
    """
    Create instance of a ScanConfigGenerator using default values.

    Use this method if wanting to test using a default configuration.
    The implementation of this uses the CSP-PST v2.5 JSON
    `<https://developer.skao.int/projects/ska-telmodel/en/latest/schemas/pst/ska-pst-configure-2.5.html>_`
    for the default values.


    :param beam_id: the PST Beam ID to use, defaults to 1
    :type beam_id: int, optional
    :param frequency_band: the frequency band, defaults to "low".
    :type frequency_band: str, optional
    :param max_scan_length: the maximum length, in seconds, that the scan will be for, defaults to 30.0.
    :type max_scan_length: float, optional
    :param max_data_generated_size: the maximum amount of data, in bytes, that the scan should generate,
        defaults to None.  This should be set when performing tests in an automated environment that
        has limited storage capacity.
    :type max_data_generated_size: int | None, optional
    :param logger: the logger to use when logging, defaults to None. The :py:class:`ScanConfigGenerator`
        only uses the logger for debug messages.
    :type logger: logging.Logger | None, optional
    :return: an instance of the scan config generator.
    :rtype: ScanConfigGenerator
    """
    if "telescope" in kwargs:
        del kwargs["telescope"]
    if "facility" in kwargs:
        del kwargs["facility"]

    if frequency_band == "low":
        telescope = "SKALow"
    else:
        telescope = "SKAMid"

    facility = TelescopeFacilityEnum.from_telescope(telescope)
    return ScanConfigGenerator(
        beam_id=beam_id,
        telescope=telescope,
        facility=facility,
        frequency_band=frequency_band,
        max_scan_length=max_scan_length,
        max_data_generated_size=max_data_generated_size,
        logger=logger,
        **kwargs,
    )


def create_fixed_scan_config_generator(scan_config: dict) -> ScanConfigGenerator:
    """Create instance of ScanConfigGenerator that replays provided scan configuration."""
    beam_id = int(scan_config["pst"]["scan"]["timing_beam_id"])
    frequency_band = scan_config["common"]["frequency_band"]
    if frequency_band == "low":
        facility = TelescopeFacilityEnum.Low
        telescope = "SKALow"
    else:
        facility = TelescopeFacilityEnum.Mid
        telescope = "SKAMid"
    telescope = facility.telescope
    max_scan_length = scan_config["pst"]["scan"]["max_scan_length"]

    scan_config_generator = ScanConfigGenerator(
        beam_id=beam_id,
        telescope=telescope,
        facility=facility,
        frequency_band=frequency_band,
        max_scan_length=max_scan_length,
    )
    scan_config_generator.replay_config = scan_config

    return scan_config_generator


class _ExecutionBlockIdFactory:
    """Utility class to generate random execution block ids that are unique upon each call.

    Instances of this class should be at a session level scope rather than
    being created for every scan.  This will allow it to keep track of previously
    generated scan ids.
    """

    def __init__(self: _ExecutionBlockIdFactory) -> None:
        """Initialise the factory."""
        self._previous_eb_ids: Set[str] = set()

    def __call__(self: _ExecutionBlockIdFactory, *args: Any, **kwargs: Any) -> str:
        """Generate next execution block id."""
        next_eb_id = self._generate_eb_id()
        while next_eb_id in self._previous_eb_ids:
            next_eb_id = self._generate_eb_id()

        self._previous_eb_ids.add(next_eb_id)
        return next_eb_id

    def _generate_eb_id(self: _ExecutionBlockIdFactory) -> str:
        r"""Generate a unique execution block id.

        The EB_ID is a string that must match against the following
        regex: ^eb\-[a-z0-9]+\-[0-9]{8}\-[a-z0-9]+$.

        An example of this is: eb-m001-20230712-56789

        This generator will select a random char, a random number between
        0 and 999 inclusive, todays date, and a random number from 0 to
        99999 inclusive.
        """
        prefix_char = random.choice(string.ascii_lowercase)
        prefix_num = random.randint(0, 999)
        suffix = random.randint(0, 99999)
        today_str = datetime.today().strftime("%Y%m%d")

        return f"eb-{prefix_char}{prefix_num:03d}-{today_str}-{suffix:05d}"


EB_ID_FACTORY_INSTANCE: _ExecutionBlockIdFactory = _ExecutionBlockIdFactory()


def generate_eb_id() -> str:
    """Generate a new execution block id."""
    return EB_ID_FACTORY_INSTANCE()


@dataclasses.dataclass(kw_only=True, frozen=True)
class RfConfig:
    nchan: int
    """The number of channels of the data."""

    centre_frequency_mhz: float
    """The centre frequency of the CBF data, in MHz."""

    bandwidth_mhz: float
    """The bandwidth of the CBF data, in MHz."""

    start_chan: int
    """The channel that the data from the CBF is starting from."""

    cbf_pst_config: CbfPstConfig = field(repr=False)
    """The CBF/PST configuration, based on the frequency band, that the RF configuration is for."""

    def __post_init__(self: RfConfig) -> None:
        """Assert generated values are correct."""
        assert (
            self.bandwidth_mhz <= self.cbf_pst_config.rf_bw_mhz
        ), f"expected {self.bandwidth_mhz=} to be <= {self.cbf_pst_config.rf_bw_mhz}"
        assert self.start_chan >= 0, f"expected {self.start_chan=} to be >= 0"
        assert self.centre_frequency_mhz >= 0.0, f"expected {self.centre_frequency_mhz=} to be >= 0"

    @property
    def centre_frequency_hz(self: RfConfig) -> float:
        """Get frequency in Hz."""
        return u.Quantity(self.centre_frequency_mhz, unit=u.MHz).to(u.Hz).value

    @property
    def bandwidth_hz(self: RfConfig) -> float:
        """Get frequency in Hz."""
        return u.Quantity(self.bandwidth_mhz, unit=u.MHz).to(u.Hz).value

    @property
    def start_frequency_mhz(self: RfConfig) -> float:
        """Get the starting frequency of that this config is for, in MHz."""
        return self.centre_frequency_mhz - self.bandwidth_mhz / 2

    @property
    def start_frequency_hz(self: RfConfig) -> float:
        """Get the starting frequency of that this config is for, in Hz."""
        return u.Quantity(self.start_frequency_mhz, unit=u.MHz).to(u.Hz).value

    @property
    def end_frequency_mhz(self: RfConfig) -> float:
        """Get the end frequency that this config is for, in MHz."""
        return self.centre_frequency_mhz + self.bandwidth_mhz / 2

    @property
    def end_frequency_hz(self: RfConfig) -> float:
        """Get the end frequency that this config is for, in Hz."""
        return u.Quantity(self.end_frequency_mhz, unit=u.MHz).to(u.Hz).value

    @property
    def end_chan(self: RfConfig) -> int:
        """
        Get the end channel that this config covers.

        This is equivalent to ``start_chan + nchan - 1``.
        """
        return self.start_chan + self.nchan - 1

    @property
    def data_rate(self: RfConfig) -> float:
        """Get the data rate for the given configuration, in bytes/sec."""
        return self.cbf_pst_config.data_rate(self.nchan)

    def as_scan_config(self: RfConfig) -> dict:
        return {
            "bits_per_sample": self.cbf_pst_config.nbit * self.cbf_pst_config.ndim,
            "udp_nsamp": self.cbf_pst_config.udp_nsamp,
            "wt_nsamp": self.cbf_pst_config.wt_nsamp,
            "udp_nchan": self.cbf_pst_config.udp_nchan,
            "num_frequency_channels": self.nchan,
            "centre_frequency": self.centre_frequency_hz,
            "total_bandwidth": self.bandwidth_hz,
            "oversampling_ratio": list(self.cbf_pst_config.oversampling_ratio),
        }


def generate_rf_config(
    cbf_pst_config: CbfPstConfig,
    max_data_rate: float | None = None,
    min_nchan: int | None = None,
) -> RfConfig:
    """
    Generate a random RF configuration that is valid for a test.

    This generates a random RF configuration that will ensure the
    number of channels, frequency and bandwidth are consistent with the
    TSAMP value for the given frequency band.

    :param cbf_pst_config: the CBF/PST configuration for a given frequency band
    :type cbf_pst_config: CbfPstConfig
    :param max_data_rate: the maximum number of bytes per second to generated config for,
        defaults to None.
    :type max_data_rate: float | None, optional
    :param min_nchan: the minimum number of input PST channels to use,
        defaults to None.
    :type min_nchan: int | None, optional
    :return: a random RF configuration
    :rtype: RfConfig
    """
    _logger.debug(f"Generating RF Config: {cbf_pst_config=}, {max_data_rate=}, {min_nchan=}")

    if max_data_rate is None:
        nchan = cbf_pst_config.max_nchan
    else:
        # bytes_per_second = nchan * npol * nbits * NUM_DIMENSIONS / 8 * 1_000_000 / tsamp
        nchan = cbf_pst_config.nchan_for_data_rate(data_rate=max_data_rate)

    bw_est = cbf_pst_config.bandwidth_mhz(nchan=nchan)
    _logger.debug(f"Generating RF config with estimated nchan of {nchan=} and {bw_est=}")

    # add some randomness for testing - however the NCHAN and START_CHAN will end up being correct
    bw_est = (1 + random.random()) * bw_est / 2

    _logger.debug(f"Generating RF config random bandwidth estimate of {bw_est=}")

    # apply some limits based on updated BW
    nchan = min(cbf_pst_config.nchan_for_bandwidth(bandwidth_mhz=bw_est), nchan)
    nchan = cbf_pst_config.apply_nchan_limits(nchan=nchan, min_nchan=min_nchan)

    assert (
        nchan % cbf_pst_config.udp_nchan == 0
    ), f"expected {nchan=} to be a multiple of {cbf_pst_config.udp_nchan=}"
    if min_nchan:
        assert nchan >= min_nchan, f"expected {nchan=} to be greater or equal to {min_nchan=}"

    bandwidth_mhz = cbf_pst_config.bandwidth_mhz(nchan=nchan)
    _logger.debug(f"Generating RF using {nchan=}, {bandwidth_mhz=}")

    # Get the maximum multiple of UDP_NCHAN given the CBF config and number of NCHAN selected
    # this allows us to select a random START_CHAN that when combined with NCHAN
    # to still be within a valid range for the frequency band
    max_num_udp_nchan = (
        cbf_pst_config.max_valid_chan - cbf_pst_config.min_valid_chan - nchan
    ) // cbf_pst_config.udp_nchan

    # note even if we have max_num_udp_nchan=0 we still are getting NCHAN but it means our
    # START_CHAN will be the minimum valid channel for the frequency band
    if max_num_udp_nchan < 0:
        max_num_udp_nchan = 0

    _logger.debug(f"Generating RF with {max_num_udp_nchan=}")

    start_chan_offset = random.randint(0, max_num_udp_nchan) * cbf_pst_config.udp_nchan
    start_chan = cbf_pst_config.min_valid_chan + start_chan_offset

    frequency_mhz = cbf_pst_config.centre_freq_mhz(start_chan=start_chan, nchan=nchan)

    end_chan = start_chan + nchan
    _logger.debug(
        f"Generated rf with {start_chan=}, {end_chan=}, {nchan=}, {frequency_mhz=}, {bandwidth_mhz=}"
    )

    return RfConfig(
        nchan=nchan,
        bandwidth_mhz=bandwidth_mhz,
        centre_frequency_mhz=frequency_mhz,
        start_chan=start_chan,
        cbf_pst_config=cbf_pst_config,
    )


def generate_rfi_frequency_mask_config(
    rf_config: RfConfig, max_rfi_masks: int = 4, percent_masked: float = 5.0, **kwargs: Any
) -> dict:
    """Generate a random RFI Mask configuration.

    Generates a RFI frequency mask that would cover ``percent_masked`` with a maximum of
    ``max_rfi_masks`` contiguous masks.

    :param rf_config: the current RF configuration to generate the mask range from.
    :type rf_config: RfConfig
    :param max_rfi_masks: the maximum number of masks ranges to use, defaults to 4
    :type max_rfi_masks: int, optional
    :param percent_masked: the percentage of the RF band to mask as RFI, defaults to 5.0
    :type percent_masked: float, optional
    :return: a dictionary with values of ``num_rfi_frequency_masks`` and an array of
        ``rfi_frequency_masks``.
    :rtype: dict
    """
    num_rfi_masks = random.randint(0, max_rfi_masks)
    if num_rfi_masks == 0:
        return {
            "num_rfi_frequency_masks": 0,
            "rfi_frequency_masks": [],
        }

    num_masked_chans = int(round(rf_config.nchan * percent_masked / 100.0))
    num_chans_per_mask = int(num_masked_chans / num_rfi_masks)
    channel_idx = list(range(0, rf_config.nchan, num_chans_per_mask))
    np.random.shuffle(channel_idx)
    channel_idx = channel_idx[:num_rfi_masks]

    def _mask_range(start_channel: int) -> List[float]:
        end_channel = start_channel + num_chans_per_mask
        start_freq_mhz = np.around(
            rf_config.start_frequency_mhz + (start_channel * rf_config.cbf_pst_config.chan_separation_mhz),
            decimals=1,
        )
        end_freq_mhz = np.around(
            rf_config.start_frequency_mhz + (end_channel * rf_config.cbf_pst_config.chan_separation_mhz),
            decimals=1,
        )

        return [np.around(start_freq_mhz * 1e6), np.around(end_freq_mhz * 1e6)]

    return {
        "num_rfi_frequency_masks": num_rfi_masks,
        "rfi_frequency_masks": [_mask_range(start_chan) for start_chan in sorted(channel_idx)],
    }


def generate_channelisation_config(rf_config: RfConfig) -> dict:
    """Generate a channelisation configuration.

    :param rf_config: the current RF configuration to generate the channelisation config for.
    :type rf_config: RfConfig
    :return: a dictionary of containing channelisation configuration.
    :rtype: dict
    """
    return {
        "num_channelization_stages": rf_config.cbf_pst_config.num_channelisation_stages,
        "channelization_stages": [
            {"num_filter_taps": 1, "filter_coefficients": [1.0], **dataclasses.asdict(stage)}
            for stage in rf_config.cbf_pst_config.channelisation_stages
        ],
    }


def generate_pulsar_source_config() -> dict:
    """Generate configuration for a random pulsar.

    This uses the :py:func:`ska_pst.testutils.pulsar.random_pulsar` to
    get a random pulsar.  Then it takes that pulsar and creates a configuration
    dictionary valid for the PST scan configuration.

    The output has the following fields:
    * ``source``
    * ``coordinates``, which includes ``equinox``, ``ra``, and ``dec``.

    :return: a random pulsar source config
    :rtype: dict
    """
    pulsar = random_pulsar()
    pulsar_coords = pulsar.coordinates
    return {
        "source": pulsar.name,
        "coordinates": {
            "equinox": 2000.0,
            "ra": pulsar_coords.ra.to_string(unit=u.hour, sep=":"),
            "dec": pulsar_coords.dec.to_string(unit=u.degree, decimal=True),
        },
    }


def generate_flow_through_mode_config(
    rf_config: RfConfig,
    ft_scale_factor: float = 0.1,
    nbit_out: int | None = None,
    polarizations_out: str | None = None,
    channels_out: Tuple[int, int] | None = None,
    **kwargs: Any,
) -> dict:
    """Generate configuration specific to the flow through mode.

    This uses the input ``rf_config`` to then create a valid output
    configuration that selects a subset of channels, polarisation, and
    output ``nbits``

    :param rf_config: the current RF configuration to generate the FT config from.
    :type rf_config: RfConfig
    :param ft_scale_factor: the amount of data decimate down to, defaults to 0.1.
        A value of ``0.1`` is equivalent to 10% of the input data.
    :type ft_scale_factor: float, optional
    :param nbit_out: the number of bits to use for the output, defaults to None.
    :type nbit_out: int | None, optional
    :param polarizations_out: the polarizations_out to record, defaults to None.
    :type polarizations_out: str | None, optional
    :param channels_out: the inclusive range of channels to record, defaults to None.
    :type channels_out: Tuple[int, int] | None, optional
    :return: a dictionary that has a valid Flow Through mode configuration given
        the input RF configuration.
    :rtype: dict
    """
    nbit_out = nbit_out or random.choice(ALLOWED_NBIT_OUT)
    assert nbit_out in ALLOWED_NBIT_OUT, f"{nbit_out=} not in {ALLOWED_NBIT_OUT}"

    polarizations_out = polarizations_out or random.choices(["A", "B", "Both"], weights=[0.2, 0.2, 0.6])[0]
    assert polarizations_out in {"A", "B", "Both"}
    npol_out = 2 if polarizations_out == "Both" else 1

    if channels_out is None:
        nchan_out = int(
            np.round(
                ft_scale_factor
                * rf_config.nchan
                * rf_config.cbf_pst_config.npol
                * rf_config.cbf_pst_config.nbit
                / npol_out
                / nbit_out
            )
        )
        # This is a work around for AT3-910
        if nchan_out * npol_out < 32:
            nchan_out = 32 // npol_out

        nchan_out = min(max(nchan_out, 1), rf_config.nchan)
        start_chan_out = random.randint(0, rf_config.nchan - nchan_out)
        channels_out = (start_chan_out, start_chan_out + nchan_out - 1)

    return {
        "num_bits_out": nbit_out,
        "channels": list(channels_out),
        "requantisation_scale": 1.0,
        "polarizations": polarizations_out,
        "requantisation_init_time": 1.0,
    }


def generate_processing_mode_config(
    rf_config: RfConfig, processing_mode: PstProcessingMode, **kwargs: Any
) -> dict:
    """Generate an processing mode configuration.

    This is a utility method to abstract over creating the processing mode's
    specific configuration.  It takes the ``processing_mode`` parameter and
    calls the correct configuration method and returns a dictionary that
    can be used in the scan configuration.

    :param rf_config: the current RF configuration to create the observation
        mode configuration for.
    :type rf_config: RfConfig
    :param processing_mode: the current processing mode to generate a
        configuration for.
    :type processing_mode: PstProcessingMode
    :raises ValueError: if the processing mode is not supported.
    :return: a dictionary with the generated processing mode sub-configuration.
    :rtype: dict
    """
    if processing_mode == PstProcessingMode.VOLTAGE_RECORDER:
        return {}
    elif processing_mode == PstProcessingMode.FLOW_THROUGH:
        return {"ft": generate_flow_through_mode_config(rf_config=rf_config, **kwargs)}
    else:
        raise ValueError(f"unsupported processing mode {processing_mode.name}")


class ScanConfigGenerator:
    """Utility class to generate Scan configuration."""

    def __init__(
        self: ScanConfigGenerator,
        beam_id: int,
        telescope: str,
        facility: TelescopeFacilityEnum,
        frequency_band: str | None = None,
        max_scan_length: float = DEFAULT_MAX_SCANLEN,
        csp_config_version: str = PST_CONFIG_VER2_5,
        processing_mode: PstProcessingMode = PstProcessingMode.VOLTAGE_RECORDER,
        max_data_generated_size: int | None = None,
        logger: logging.Logger | None = None,
        **kwargs: Any,
    ) -> None:
        """Create instance of ScanConfigGenerator.

        :param beam_id: the ID of the beam being used to generate config for.
        :type beam_id: int
        :param telescope: the Telescope for which the config is being generated for.
        :type telescope: int
        :param facility: the facility that config is being generated for.
        :type facility: TelescopeFacilityEnum.
        :param frequency_band: the frequency band that the configuration is for, optional.
        :type frequency_band: str | None, default is "low" if the ``facility`` is ``Low``
            else it is randomly selected from the available Mid frequency bands.
        :param max_scan_length: the maximum scan length, default is 30 seconds.
            If value is less than 30.0s then this is updated to 30.0 else there
            would be a validation issue.
        :type max_scan_length: float
        :param csp_config_version: the CSP-PST configuration version to use. Default is
            'https://schema.skao.int/ska-pst-configure/2.5' (i.e 2.5 of CSP-PST schema).
        :type csp_config_version: str
        :param processing_mode: the default processing mode to use when generating
            scan configurations. Default is PstProcessingMode.VOLTAGE_RECORDER.
        :type processing_mode: PstProcessingMode
        :param max_data_generated_size: the maximum amount of data to generate, in bytes.
            If not set this may produce a large amount of data that could fill the file
            system. Default is None
        :type max_data_generated_size: int, optional
        """
        self._logger = logger or logging.getLogger(__name__)

        max_scan_length = max(max_scan_length, DEFAULT_MAX_SCANLEN)

        self._beam_id = beam_id
        self._telescope = telescope
        self._facility = facility
        self._telescope_config = get_telescope_config(telescope)
        if facility == TelescopeFacilityEnum.Low:
            self._frequency_band = "low"
        else:
            if frequency_band:
                self._frequency_band = frequency_band
            else:
                self._frequency_band = random.choice(["1", "2", "3", "4", "5a", "5b"])
        self._previous_config_ids: List[str] = []
        self._current_config: dict = {}
        self._max_scan_length = max_scan_length
        self._processing_mode = processing_mode
        self._config_override: dict = {}
        self._replay_config: dict | None = None
        self._csp_config_version = csp_config_version
        self._previous_scan_ids: Set[int] = set()
        self._max_data_generated_size = max_data_generated_size
        self._processing_mode_config: dict = {}

    @property
    def facility(self: ScanConfigGenerator) -> TelescopeFacilityEnum:
        """Get the current facility."""
        return self._facility

    @facility.setter
    def facility(self: ScanConfigGenerator, facility: TelescopeFacilityEnum) -> None:
        """Set the facility."""
        self._facility = facility

    @property
    def telescope(self: ScanConfigGenerator) -> str:
        """
        Get the current telescope.

        There is no setter for this as it depends on the facility

        :return: the current telescope.
        :rtype: str
        """
        return self._facility.telescope

    @property
    def telescope_config(self: ScanConfigGenerator) -> TelescopeConfig:
        """
        Get the configuration of the current telescope.

        This is a utility property to avoid duplication of code and the
        need to get the configuration all the time just to get
        telescope config needed for generating a scan configuration.

        :return: the configuration of the current telescope.
        :rtype: TelescopeConfig
        """
        return get_telescope_config(self.telescope)

    @property
    def frequency_band_config(self: ScanConfigGenerator) -> FrequencyBandConfig:
        """
        Get the current frequency band configuration for the current telescope and frequency band.

        This is a utility property to avoid duplication of code and the
        need to get the configuration all the time just to get
        frequency band config needed for generating a scan configuration.

        :return: the current frequency band configuration for the current telescope and frequency band.
        :rtype: FrequencyBandConfig
        """
        return self.telescope_config.frequency_bands[self.frequency_band]

    @property
    def cbf_pst_config(self: ScanConfigGenerator) -> CbfPstConfig:
        """
        Get the CBF/PST config for the telescope and frequency band.

        This is a utility property to avoid duplication of code and the
        need to get the configuration all the time just to get
        ``cbf_pst_config`` needed for generating a scan configuration.

        :return: the CBF/PST config for the telescope and frequency band.
        :rtype: CbfPstConfig
        """
        return self.frequency_band_config.cbf_pst_config

    @property
    def processing_mode(self: ScanConfigGenerator) -> PstProcessingMode:
        """Get current configured processing mode."""
        return self._processing_mode

    @processing_mode.setter
    def processing_mode(self: ScanConfigGenerator, processing_mode: PstProcessingMode) -> None:
        """Set the processing mode for the current test."""
        self._processing_mode = processing_mode

    @property
    def frequency_band(self: ScanConfigGenerator) -> str:
        """Get current frequency band."""
        return self._frequency_band

    @frequency_band.setter
    def frequency_band(self: ScanConfigGenerator, frequency_band: str) -> None:
        """Set the frequency band."""
        self._frequency_band = frequency_band

    def __getattr__(self: ScanConfigGenerator, key: str) -> Any:
        """Get a config value using Python attributes."""
        if key in self._config_override:
            return self._config_override[key]

        if key in self._current_config:
            return self._current_config[key]

        return self.calculate_udp_gen_resources()[key]

    def override_config(self: ScanConfigGenerator, key: str, value: Any) -> None:
        """Override a specific config value."""
        self._config_override[key] = value
        if key == "max_scan_length":
            self._max_scan_length = value

    def add_processing_mode_config_value(self: ScanConfigGenerator, key: str, value: Any) -> None:
        """Set a configuration specific for the given processing mode."""
        self._processing_mode_config[key] = value

    def _generate_config_id(self: ScanConfigGenerator) -> str:
        """Generate a unique configuration id."""
        # create a random valid string
        characters = string.ascii_letters + string.digits + "-"
        config_id = "".join(random.choice(characters) for _ in range(20))
        while config_id in self._previous_config_ids:
            config_id = "".join(random.choice(characters) for _ in range(20))

        self._previous_config_ids.append(config_id)
        return config_id

    def _generate_csp_common_config(self: ScanConfigGenerator, eb_id: str | None = None) -> dict:
        """Generate a CSP common configuration.

        This will also generate a unique configuration id.
        """
        config_id = self._generate_config_id()
        if eb_id is None:
            eb_id = generate_eb_id()

        return {
            "config_id": config_id,
            "subarray_id": 1,
            "frequency_band": self.frequency_band,
            "eb_id": eb_id,
        }

    def _get_pst_scan_config_example(self: ScanConfigGenerator) -> dict:
        """Get CSP configure scan example.

        This is used as a base configuration that the generator
        will override.
        """
        example = get_pst_config_example(
            version=self._csp_config_version, scan_type=csp_scan_example_str(self.processing_mode)
        )
        return example["pst"]["scan"]

    def _generate_pst_scan_config(
        self: ScanConfigGenerator, overrides: dict = {}, max_data_rate: float | None = None
    ) -> dict:
        """Generate the PST Scan config with overriden values.

        If no overrides are given then this will generate a valid
        configuration.  Overrides can be used to change the configuration
        or provide an invalid configuration that can be tested.
        """
        pst_beam_id = str(random.randint(1, 16))

        base_request = self._get_pst_scan_config_example()

        if self.processing_mode == PstProcessingMode.VOLTAGE_RECORDER:
            max_data_rate = min(max_data_rate or MAX_VR_DATA_RATE, MAX_VR_DATA_RATE)

        min_nchan: int | None = None
        if "channels_out" in self._processing_mode_config:
            channels_out = self._processing_mode_config["channels_out"]
            min_nchan = channels_out[-1]
            self._logger.debug(f"{channels_out=} ensuring {min_nchan=}")

        rf_config = generate_rf_config(
            cbf_pst_config=self.cbf_pst_config,
            max_data_rate=max_data_rate,
            min_nchan=min_nchan,
        )
        activation_time = datetime.utcnow().isoformat(sep="T", timespec="milliseconds") + "Z"

        max_scan_length = self._max_scan_length
        if self._max_data_generated_size:
            max_scan_length = np.floor(
                min(max_scan_length, self._max_data_generated_size / rf_config.data_rate)
            )

        self._logger.debug(f"{rf_config=}, {max_scan_length=}, data_rate={rf_config.data_rate:0.3f}")

        base_request = {
            **base_request,
            "activation_time": activation_time,
            "timing_beam_id": pst_beam_id,
            "observation_mode": self.processing_mode.name,
            "max_scan_length": max_scan_length,
            "subint_duration": 30.0,
            **rf_config.as_scan_config(),
            **generate_general_metadata(),
            **generate_receptor_data(),
            **generate_pulsar_source_config(),
            **generate_rfi_frequency_mask_config(rf_config=rf_config),
            **generate_channelisation_config(rf_config=rf_config),
            **dataclasses.asdict(self.frequency_band_config.receiver_config),
            **generate_processing_mode_config(
                rf_config=rf_config, processing_mode=self.processing_mode, **self._processing_mode_config
            ),
        }

        return {**base_request, **overrides, **self._config_override}

    def generate(
        self: ScanConfigGenerator,
        eb_id: str | None = None,
        overrides: dict = {},
        max_data_rate: float | None = None,
    ) -> dict:
        """Generate a configuration.

        This is the public method that should be used by test fixtures to generate
        the configuration. The overrides parameter can be used to generate an
        invalid configuration.
        """
        if self._replay_config is not None:
            return self._replay_config

        csp_common_request = self._generate_csp_common_config(eb_id=eb_id)
        configure_scan_request = self._generate_pst_scan_config(
            overrides=overrides, max_data_rate=max_data_rate
        )

        config = {
            "interface": self._csp_config_version,
            "common": csp_common_request,
            "pst": {
                "scan": configure_scan_request,
            },
        }
        self._current_config = config

        self._logger.debug(f"{config=}")
        return config

    def generate_json(self: ScanConfigGenerator) -> str:
        """Generate a configuration and return it as a json string.

        This is equivalent of doing:

        .. code-block:: python

            scan_config = scan_config_generator.generate()
            scan_config_str = json.dumps(scan_config)

        """
        config = self.generate()
        return json.dumps(config)

    @property
    def replay_config(self: ScanConfigGenerator) -> dict | None:
        return self._replay_config

    @replay_config.setter
    def replay_config(self: ScanConfigGenerator, config: dict) -> None:
        self._replay_config = config
        self._current_config = config

    def reset_replay_config(self: ScanConfigGenerator) -> None:
        self._replay_config = None
        self._current_config = {}

    @property
    def curr_config_id(self: ScanConfigGenerator) -> str:
        return self._current_config["common"]["config_id"]

    @property
    def curr_config(self: ScanConfigGenerator) -> dict:
        """Get the current scan configuration."""
        return self._current_config

    @property
    def curr_config_json(self: ScanConfigGenerator) -> str:
        """Get the current scan configuration as JSON string."""
        return json.dumps(self.curr_config)

    def _get_local_host_ip(self: ScanConfigGenerator) -> str:
        """Get IP address of eth0.

        Based off https://gist.github.com/EONRaider/3b7a8ca433538dc52b09099c0ea92745.
        This is needed because the testutils container may be on a different host to
        the RECV.
        """
        import fcntl
        import socket
        import struct

        sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        packed_iface = struct.pack("256s", "eth0".encode("utf_8"))
        packed_addr = fcntl.ioctl(sock.fileno(), 0x8915, packed_iface)[20:24]
        return socket.inet_ntoa(packed_addr)

    def calculate_udp_gen_resources(self: ScanConfigGenerator, overrides: dict = {}) -> dict:
        """Calculate the resources for PST for current configuration.

        This is used to send requests to the UDP generator and it needs
        to know from the configuration the bandwidth, nbits, etc to
        provide a valid configuration file to be used by the UDP generator.
        """
        scan_request_params = convert_csp_config_to_pst_config(self._telescope_config, self._current_config)

        if self._facility == TelescopeFacilityEnum.Low:
            band = "Low"
        else:
            band = "High"

        cbf_pst_config: CbfPstConfig = scan_request_params["cbf_pst_config"]

        smrb_resources = calculate_smrb_subband_resources(beam_id=self._beam_id, **scan_request_params)

        recv_resources = calculate_receive_subband_resources(
            beam_id=self._beam_id,
            request_params=scan_request_params,
            # these params will be added later
            data_host="127.0.0.1",
            data_mac="01:23:45:ab:cd:ef",
            subband_udp_ports=[10000],
            **scan_request_params,
        )

        recv_scan = generate_recv_configure_scan_request(**scan_request_params)
        dsp_scan = generate_dsp_scan_request(**scan_request_params)

        # for now only dealing with 1 subband
        return {
            **scan_request_params,
            **smrb_resources[1],
            **recv_scan,
            **recv_resources["common"],
            **recv_resources["subbands"][1],
            **dsp_scan,
            "beam_id": scan_request_params.get("timing_beam_id", self._beam_id),
            "band": band,
            "resolution": calculate_resolution(**recv_resources["common"]),
            "telescope": self._telescope,
            "local_host": self._get_local_host_ip(),
            "packets_per_buffer": cbf_pst_config.ring_buffer_config.packets_per_buffer,
            **overrides,
        }
