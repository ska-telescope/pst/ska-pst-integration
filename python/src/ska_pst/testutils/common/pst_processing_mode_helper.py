# -*- coding: utf-8 -*-
#
# This file is part of the SKA PST project
#
# Distributed under the terms of the BSD 3-clause new license.
# See LICENSE for more info.

"""Module to provide helper method to convert strings to PstProcessingModes."""

__all__ = ["pst_processing_mode_from_str"]

from ska_control_model import PstProcessingMode


def pst_processing_mode_from_str(value: str) -> PstProcessingMode:
    """Get processing mode from a string.

    PST BDD tests may use 'voltage recorder' or 'pulsar timing'.
    This will capitalise the string and replace spaces with
    underscores before converting to an enum value.
    """
    return PstProcessingMode[value.upper().replace(" ", "_")]
