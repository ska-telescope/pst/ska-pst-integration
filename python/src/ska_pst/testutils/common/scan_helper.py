# -*- coding: utf-8 -*-
#
# This file is part of the SKA PST project
#
# Distributed under the terms of the BSD 3-clause new license.
# See LICENSE for more info.

"""Module provides helper method to convert PstProcessingMode to a CSP scan example string."""

__all__ = ["csp_scan_example_str"]

from ska_control_model import PstProcessingMode


def csp_scan_example_str(mode: PstProcessingMode) -> str:
    """Get CSP Scan example string.

    This is to be used when calling the
    :py:meth:`ska_telmodel.csp.get_csp_config_example` method
    to retrieve the correct example CSP JSON data.

    :rtype: str
    """
    if mode == PstProcessingMode.PULSAR_TIMING:
        return "pst_scan_pt"
    elif mode == PstProcessingMode.DETECTED_FILTERBANK:
        return "pst_scan_df"
    elif mode == PstProcessingMode.FLOW_THROUGH:
        return "pst_scan_ft"
    elif mode == PstProcessingMode.VOLTAGE_RECORDER:
        return "pst_scan_vr"
    else:
        raise ValueError(f"Invalid enum value of {mode}.")
