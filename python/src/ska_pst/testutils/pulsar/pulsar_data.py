# -*- coding: utf-8 -*-
#
# This file is part of the SKA PST project
#
# Distributed under the terms of the BSD 3-clause new license.
# See LICENSE for more info.

"""Submodule providing example pulsars that can be used during testing."""

from __future__ import annotations

import dataclasses
import pathlib
import random
from functools import lru_cache
from typing import List

import astropy.units as u
from astropy.coordinates import Angle, SkyCoord

PULSAR_FILE: str = "psr_ra_dec_dm.txt"


@dataclasses.dataclass(kw_only=True)
class Pulsar:
    """
    Data class providing name, RA/Dec and dispersion measure of a pulsar.

    All coordinate values are J2000.0
    """

    name: str
    """The name of the pulsar (e.g J0002+6216)"""

    ra: str
    """The right acession (RA) of the pulsar, in hour angle."""

    dec: str
    """The declination (Dec) of the pulsar, in degrees."""

    dm: float
    """The dispersion measure of the pulsar, in (cm^-3 pc)."""

    @property
    def coordinates(self: Pulsar) -> SkyCoord:
        """Get the coordinates of the pulsar.

        This returns an Astropy :py:class:`SkyCoord` object
        that parses the :py:attr:`ra` and :py:attr:`dec` values
        of the current pulsar object.

        :return: the RA/Dec coordinates of object.
        :rtype: SkyCoord
        """
        ra = Angle(self.ra, unit=u.hour)
        dec = Angle(self.dec, unit=u.deg)
        return SkyCoord(ra, dec, equinox="J2000")


# This function is cached so only needed one
@lru_cache
def pulsar_data() -> List[Pulsar]:
    """Get a list of pulsar objects.

    This function can be use to get a list of :py:class:`Pulsar` objects
    that can be use as source objects during testing.

    :return: a list of pulsar objects
    :rtype: List[Pulsar]
    """
    data_file = pathlib.Path(__file__).parent / PULSAR_FILE

    pulsars: List[Pulsar] = []

    with open(data_file, "r") as f:
        for line in f.readlines():
            # ignore comment files
            if line.startswith("#"):
                continue
            [name, ra, dec, dm, *_] = line.split()
            pulsar = Pulsar(name=name, ra=ra, dec=dec, dm=float(dm))

            pulsars.append(pulsar)

    return pulsars


def random_pulsar() -> Pulsar:
    """Get a random pulsar."""
    return random.choice(pulsar_data())
