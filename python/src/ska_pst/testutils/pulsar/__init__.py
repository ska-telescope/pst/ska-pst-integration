# -*- coding: utf-8 -*-
#
# This file is part of the SKA PST project
#
# Distributed under the terms of the BSD 3-clause new license.
# See LICENSE for more info.

"""Submodule providing data and utility functions for pulsars that can be used in testing."""

__all__ = ["Pulsar", "pulsar_data", "random_pulsar"]

from .pulsar_data import Pulsar, pulsar_data, random_pulsar
