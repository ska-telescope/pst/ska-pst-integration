# -*- coding: utf-8 -*-
#
# This file is part of the SKA PST project
#
# Distributed under the terms of the BSD 3-clause new license.
# See LICENSE for more info.

"""Module for verification of data."""

__all__ = [
    "Metadata",
    "MetadataVerifier",
    "ProcessingModeVerifier",
    "ValueMapping",
    "VoltageRecorderVerifier",
    "assert_bytes_per_second",
    "assert_const_value",
    "assert_equal_to",
    "assert_file_number",
    "assert_header_value",
    "assert_nant",
    "assert_nbit",
    "assert_ndim",
    "assert_npol",
    "assert_obs_offset",
    "assert_resolution",
    "assert_tsamp",
    "assert_udp_format",
    "calculate_bytes_per_second",
    "calculate_resolution",
    "get_expected_tsamp",
    "to_si",
    "split_values",
    "DADA_HEADER_CONVERTER_MAPPING",
    "DADA_VALUE_ASSERTIONS",
    "SECONDS_PER_FILE",
]

from .assertions import (
    assert_bytes_per_second,
    assert_const_value,
    assert_equal_to,
    assert_file_number,
    assert_header_value,
    assert_nant,
    assert_nbit,
    assert_ndim,
    assert_npol,
    assert_obs_offset,
    assert_resolution,
    assert_tsamp,
    assert_udp_format,
    calculate_bytes_per_second,
    calculate_resolution,
    get_expected_tsamp,
    DADA_VALUE_ASSERTIONS,
    SECONDS_PER_FILE,
)
from .mapping import ValueMapping, to_si, split_values, DADA_HEADER_CONVERTER_MAPPING
from .metadata_verifier import Metadata, MetadataVerifier, ProcessingModeVerifier, VoltageRecorderVerifier
