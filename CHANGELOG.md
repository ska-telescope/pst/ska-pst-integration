# Changelog

## Unreleased

### New

* AT3-912 enable unique baremetal paths for k8s-tests
* AT3-894 integrate Square Wave Generator with Flow Through Manager tests

### Change

* AT3-926 Refactor send for optional build and chown paths.

### Fix

* AT3-891 implement DSP.FT output weights buffer
* AT3-911 fix issue with resetting of scales in UDPFormat found in Mid tests
* AT3-911 fix issues found with in BDD test utils to allow Mid BDD tests to pass

## 1.0.2 (2024-02-17)

### Change

* AT3-859 update LMC to use SKA base long running commands API
* AT3-825 change C++ logging be less verbose but add better context
* AT3-871 update versions of PSRDADA, SPDLOG (1.15.0) and gRPC (1.68.2)
* AT3-918 unpinned `numpy` and `astropy` Python dependencies

### Fix

* AT3-905 fix broken C++ tests relating to race conditions
* AT3-843 fix DSP.FT to handle stopping of a scan before data has been received
* AT3-792 fix DSP.FT use of StreamWriter for output data and weights
* AT3-918/SKB-697 fix PST not being able to be reconfigured while in READY state
* AT3-900 fix the calculation of RECV start channel to be a multiple of UDP_NCHAN
* AT3-900 fix the FileReader's parsing of OBS_OFFSET

## 1.0.1 (2024-12-11)

### Change

* AT3-799 speed up C++ unit tests
* AT3-799 added intermediate states of StartingScan and StoppingScan in c++ state model
* AT3-878 update documentation about output data products

### Fix

* SKB-639 / AT3-874 fix issue of stop scan when there is slow processing in DSP.DISK

## 1.0.0 (2024-11-20)

### New

* AT3-820 add EDA integration instructions
* AT3-813 added the following Taranta dashboards: overview, scan, stat
* AT3-812 add health checking between LMC and the core applications
* AT3-819 expose subcomponent health state to PST.LMC TANGO device
* AT3-821 add static scan configuration to allow removal of values from PST schema

### Change

* AT3-821 update LMC to handle future PST schema changes
* AT3-862 update Data Product Dashboard from version 0.8.2 to 0.11.0
* AT3-829 added start channel centre frequency to channel block configuration

### Fix

* SKB-637 / AT3-855 fix issue where `Reset` command not resetting PST.BEAM device correctly
* AT3-844 fix rounding errors when computing or converting floating point values from binary to string forms
* AT3-844 fix bugs in the RfConfig generator
* AT3-865 fix GitLab X-Ray publishing job

## 0.13.1 (2024-10-22)

### New

* AT3-809 add PST reset regardless of state.
* AT3-810 added TANGO warning and alarm levels to PST, including having `ska-tango-alarmhandler` deployed in `test-parent`
* AT3-845 add mac address to channel block configuration

### Change

* AT3-540 implement ska-pst chart default resource request and limits
* AT3-824 migrate to PstProcessingMode enum from ska-control-model
* AT3-826 accept either "observation_mode" or "pst_processing_mode" in scan configuration
* AT3-838 improve documentation about RECV monitoring points
* AT3-805 update documentation about deployment requirements and array assembly releases

### Fix

* SKB-615 fix send teardown by replacing pod with deployment in chart template
* AT3-847 fix issue when forcing applications to restart
* AT3-838 improve RECV.CORE's BW, FREQ and TSAMP regex validation to permit integer values, fixing SKB-597
* AT3-845 fix many spelling issues in code and documentation
* AT3-838 fix reconfiguration bug in IBVQueue described by SKB-619

## 0.13.0 (2024-08-22)

### New

* AT3-755 cuda implementation, tests, configuration and documentation of dsp.ft
* AT3-735 add documentation page for TANGO monitoring attributes
* AT3-732 add gpu chart configuration, documentation, and runtime tests
* AT3-737 add support to zero relative weights if CBFPSR packets are flagged invalid as per ECP-240079
* AT3-749 add monitoring of CBF validity flags (see [ECP-240079](https://confluence.skatelescope.org/pages/viewpage.action?pageId=267873385) for details)
* AT3-766 implement BDD tests that assert correct handling of CBF UDP packet validity flags

### Change

* AT3-720 refactored ska-pst to be a single Helm chart
* AT3-738 observation mode renamed processing mode and dynamic spectrum renamed detected filterbank
* AT3-733 update RECV.CORE to handle updated CBF UDP packet definition; includes changes for ECP-240078 (timestamp definition) ECP-240079 (validity flags)
* AT3-740 update PST to allow flow through mode to use NBIT=1,2,4,8, and 16
* AT3-748 refactor DSP flow through bytes_per_second calculation
* AT3-751 update Helm chart configuration easily allow of deploying PST in either SKA-Low or SKA-Mid
* AT3-793 update tests for asserting against updated digitisation mean
* AT3-755 update BDD tests for flow through to assert statistics of output data
* AT3-762 document our interfaces and dependencies to better supper AIV teams

### Fix

* AT3-750 fix xray-publish configuration and trigger configurations
* AT3-777 fix issue in Helm chart not using the chart `appVersion` as the default image tag
* AT3-667 improve disk data output rate calculation in LMC for flow through mode
* AT3-803 fix test_cpp_memcheck CI job

## 0.12.1 (2024-07-02)

### Fix

* AT3-734 fix persistentvolume.name persistentvolumeclaim.volume.name pod volume and container volume name

## 0.12.0 (2024-06-27)

### New

* AT3-724 add Helm templates and documentation for optional Network Attachment Definition configuration
* AT3-727 add initial CHANGELOG.md
* AT3-696 add BDD tests for verifying metadata of output files in DSP flow through mode
* AT3-713 add file writer thread to write DSPSR pipeline data to disk in DSP flow through application manager
* AT3-700 integrate DSPSR LoadToQuantize pipeline with `ska_pst_dsp_ft` application
* AT3-677 integrate DSPSR library with `ska-pst`
* AT3-684 add PBF inversion temporal fidelity notebook
* AT3-699 add missing X-ray connections on BDD tests
* AT3-662 add calculation for BLOCK_HEADER_BYTES and BLOCK_DATA_BYTES to UDPFormat
* AT3-469 add ability to verify consistency of metadata

### Change

* AT3-726 update to latest SKA base Python and Helm dependencies
* AT3-725 improve documentation for integrated deployments using PST
* AT3-678 change DSP flow through manager to handling input and output ringbuffers for DSPSR pipeline
* AT3-665 update SEND to ignore DSP flow through data files
* AT3-660 update scan config generator to use random data
* AT3-592 rename a test scan config for clarity to be a SKA Low config
* AT3-529 update dependencies to the latest SKA base

### Fix

* AT3-723 fix default value for the shared storage used by dsp
* AT3-718 fix bad CBFPSR assumption in unpackers, data generators and tests
* AT3-712 fix issues found during Topic / Cream / Perentie integration
* AT3-701 fix issue of `ska-pst` being deployed in simulation mode
* AT3-664 fix channel block configuration generated by PST
* AT3-661 fix metadata that SEND is SDP data product dashboard metadata file

### Other

* AT3-697 re-enable C++ code coverage reporting
* AT3-594 prototype solution of k8s deploying containers under development
* AT3-515 integrate C++ linting into CI/CD pipeline

## 0.11.0 (2024-02-21)

### Other

* REL-1245 bump version to 0.11.0 to avoid issues with other PST products

## 0.9.0 (2024-02-20)

### New

* AT3-568 allow Helm deployments to target `pst-beam2`
* AT3-638 add BDD tests to verify state model for DSP flow through
* AT3-631 add DSP flow through support in the LMC code
* AT3-624 add a `ska_pst_dsp_ft` application
* AT3-630 add a DSP flow through simulator in the LMC
* AT3-623 add LMC service handler for DSP flow through
* AT3-626 add ability to deploy example CUDA application on CUDA hardware
* AT3-621 add an application manager for DSP flow through mode
* AT3-625 add CUDA support into `ska-pst-buildtools`
* AT3-538 add a testutils chart to allow for better testing

### Change

* AT3-651 update to the latest SDP data product dashboard version
* AT3-567 change `k8s-test-psi-low` job to use `pst-beam2`
* AT3-583 review and update of Read the Docs documentation
* AT3-632 update Helm chart to deploy DSP flow through
* AT3-634 migrate Helm charts to TANGO operator
* AT3-620 update Helm chart to allow `ska-pst` to be deployed in simulation mode
* AT3-622 refactor LMC code to remove internal TANGO sub-devices
* AT3-618 update protobuf definition for DSP flow through
* AT3-617 update LMC to use latest `ska-telmodel` schema
* AT3-606 expose STAT TANGO attributes to the `PST.BEAM` TANGO device
* AT3-605 update to latest `ska-tango-base` and `ska-csp-lmc-base`
* AT3-616 add PCAP file reader in UDP generator

### Fix

* AT3-635 fix issue to allow MidCBF processing of data
* AT3-581 fix issues with Read the Docs rendering
* AT3-650 fix issue of having multiple scans with same scan configuration
* AT3-648 fix CUDA + spdlog compilation issue

### Other

* REL-1241 release PST version 0.9.0
* AT3-640 prepare for release of 0.9.0
* AT3-627 develop an example CUDA based application
* AT3-598 convert repository into a monorepo of `ska-pst`

## 0.8.0 (2023-11-23)
### New

* AT3-569 add a Jupyter notebook to demonstrate the STAT data access library API
* AT3-560 add data quality statistics in a Jupyter notebook
* AT3-489 add BDD tests to validate STAT behaviour

### Fix

* AT3-600 fix Helm persistent volume claim templating issue

### Other

* AT3-572 prepare for a release of 0.8.0 of PST

## 0.6.3 (2023-11-15)
### New

* AT3-300 add a Taranta dashboard for displaying TANGO devices and attributes
* AT3-561 add a notebook demonstrating statistics metrics of PST during a scan
* AT3-564 add STAT TANGO interface to PST
* AT3-477 add BDD tests for validating SEND functionality

### Change

* AT3-559 update Helm chart to launch STAT container
* AT3-549 allow for persistent deployment of SDP data product dashboard in Low PSI
* AT3-558 upgrade to TANGO 9.4.2

### Fix

* AT3-503 fix BDD tests failing after STAT and SEND integration
* AT3-593 fix issue with STAT that incorrectly calculates some statistics
* AT3-573 fix issue of SEND not sending files
* AT3-536 fix the `force-clean` tasks, which currently are not running in `ska-pst` gitlab pipelines

### Other

* AT3-591 update `ska-cicd-makefile`
* AT3-535 migrate `ska-pst-lmc` chart to this repository

## 0.6.1 (2023-09-06)

### New

* AT3-537 add readiness and liveness probes for Kubernetes pods in `ska-pst-core` chart
* AT3-525 enable access to the PST Jupyter Lab in the STFC cluster
* AT3-476 add `ska-pst-send` as a separate pod within Helm chart
* AT3-467 add writing of scan configuration from `PST.BEAM` TANGO to shared volume
* AT3-475 add deploying of SDP data product dashboard in `test-parent` Helm chart

### Fix

* AT3-461 fix issues found when preparing for SP-3339 demo

### Other

* REL-731 release 0.6.1 of PST product
* AT3-495 prepare for release of version 0.6.1 of PST product

## 0.6.0 (2023-08-18)

### New

* AT3-487 add STAT plotting to happy path notebooks
* AT3-470 develop CEPHFS shared storage for BDD tests

### Fix

* AT3-461 fix documentation

## 0.5.5 (2023-07-26)

### Other

* REL-798 fix broken release

## 0.5.4 (2023-07-20)

### New

* REL-798 release 0.5.4
* AT3-466 add end scan sentinel marker for DSP.DISK

## 0.5.3 (2023-07-20)

### New

* AT3-463 add ska-pst-send to Helm chart

### Change

* AT3-501 update `k8s-test` job to capture logs to output file
* AT3-458 update happy path notebook to use `ska-pst-testutils`
* AT3-504 change Read The Docs to use configuration yaml file

### Fix

* AT3-505 fix bug in happy path notebook about EndScan command failing
* AT3-436 change Helm chart to use `appVersion` not `AppVersion`

### Other

* AT3-460 document the use of the single happy path notebook
* AT3-511 improve the Read the Docs documentation
* AT3-456 refactor testing infrastructure into ska-pst-testutils

## 0.5.1 (2023-06-30)

### New

* AT3-502 add release instructions to documentation
* AT3-431 add a single happy path Jupyter notebook

### Change

* AT3-491 update TANGO and ska-pst-lmc dependencies
* AT3-490 update Docker images to use Ubuntu 22.04

### Other

* AT3-455 develop best practices for management and deployment of notebooks

## 0.4.0 (2023-06-13)

### New

* AT3-440 add BDD tests that verify channel block configuration is generated
* AT3-392 add BDD tests that validates the data recorded on disk matches the scan duration
* AT3-387 add BDD Tests that validates handling of invalid JSON scan configuration
* AT3-437 add BDD tests that verifies bad/dropped packets are flagged in output data
* AT3-389 schedule periodic running of BDD tests via gitlab CI
* AT3-348 add functionality that allows CORE subcomponents to validate requests
* AT3-415 add BDD test that injects tone in specific channel of UDP generator and detects tone in recorded voltage data product
* AT3-384 add Xray integration of existing BDD tests
* AT3-386 add BDD test and Xray integration for improper Obs State Model command sequence from CSP-LMC

### Change

* AT3-383 convert existing integration tests to BDD
* AT3-425 update ska-pst-lmc obsReset to use LmcServiceHandler Reset

### Fix

* AT3-435 fix bug where ska-pst cannot recover from fault
* AT3-438 fix bug where RECV.CORE not allowing a timing_beam_id of 16
* AT3-439 fix shared memory is not released when beam is deconfigured

### Other

* REL-708 release 0.4.0 of ska-pst

## 0.3.1 (2023-04-18)

### New

* AT3-400 add a test that demonstrates the solution of writing to CEPH shared storage
* AT3-414 add simulation mode to ska-pst Helm chart

### Fix

* AT3-388 fix bug with the monitoring streams not resetting in multi scan test
* SKB-218 fix bug where low-pst/beam/01 cannot set adminMode to ONLINE

## 0.3.0 (2023-04-03)

### New

* AT3-341 add BDD tests for running multiple scans with the same scan configuration

### Other

* AT3-380 rename ska-pst-integration to ska-pst

## 0.2.0 (2023-03-10)

### New

* AT3-340 add Python analysis module for PST recorded voltage data files
* AT3-339 add BDD tests that cover CBF/PSR ICD behaviours (bad packets, out of order data, timeouts)
* AT3-353 add BDD test for LMC + CORE to test abort on long running commands
* AT3-344 add BDD test for LMC + CORE to test multiple scans with different scan configuration
* AT3-310 develop negative CI System Tests
* AT3-343 enable JSON validation in PST LMC

### Change

* AT3-329 update documentation
* AT3-332 update Helm chart to support overriding the persistent volume location

### Fix

* AT3-367 ensure that the PST BEAM ID is consistent between CSP_LMC, RECV and CBF

## 0.1.0 (2022-12-02)

### New

* AT3-292 Develop positive system tests for low/mid
* AT3-303 Testing and Integration of BEAM.MGMT with PST.CORE
* AT3-304 Develop Deployment of PST Beam (TANGO & C++)
* AT3-290 Integrate Trivial CBF Simulator into PST.CORE System Test
* AT3-301 Develop the deployment of BEAM, RECV, SMRB and DSP.DISK TANGO
* AT3-286 Configure pod deployment of PST.CORE (RECV.CORE, SMRB.CORE, DSP.DISK) in ska-pst-integration
* AT3-277 Establish ska-pst-integration repository and support infrastructure
