ARG SKA_TANGO_RUNTIME_IMAGE=""
ARG SKA_PST_PYTHON_BUILDER_IMAGE=""
ARG SKA_PST_CORE_BUILDER_IMAGE=""
ARG POETRY_VERSION=1.8.2

FROM $SKA_PST_PYTHON_BUILDER_IMAGE as buildenv
FROM $SKA_PST_CORE_BUILDER_IMAGE as cppbuilder

FROM $SKA_TANGO_RUNTIME_IMAGE as runtime
ARG POETRY_VERSION

ENV DEBIAN_FRONTEND noninteractive

USER root

# WORKDIR is already /app because of TANGO base image

# Base TANGO image as ONBUILD copy command that copies EVERYTHING

WORKDIR /usr/local
COPY dependencies/ ./share/

RUN --mount=type=bind,from=cppbuilder,target=/builder \
    stat ./share/runtime.apt.txt \
    && apt-get update -y \
    && apt-get install  --no-install-suggests --no-install-recommends -y $(cat ./share/runtime.apt.txt) \
    && apt-get autoremove --yes \
    && rm -rf /var/lib/apt/lists/* \
    && rm -fr ./share/ \
    && cp -d /builder/usr/local/bin/ska_pst* ./bin/ \
    && cp -d /builder/usr/local/lib/*.so* ./lib/ \
    && cp -rd /builder/usr/local/psrdada ./psrdada \
    && cp -rd /builder/usr/local/grpc ./grpc \
    && cp -rd /builder/usr/local/spdlog ./spdlog \
    && ldconfig

ENV \
  LD_LIBRARY_PATH=/usr/local/psrdada/lib:/usr/local/grpc/lib:/usr/local/spdlog/lib \
  PATH=${PATH}:/usr/local/psrdada/bin

WORKDIR /app

RUN rm -rf ./*
COPY --chown=tango:tango pyproject.toml .
COPY --chown=tango:tango poetry.lock .
COPY --chown=tango:tango resources/ska-pydada/ /app/resources/ska-pydada/
COPY --chown=tango:tango python/ /app/python/
COPY --chown=tango:tango pytest.ini /app/python/
COPY --chown=tango:tango notebooks/ /app/notebooks/
COPY --chown=tango:tango README.md .

RUN poetry self update ${POETRY_VERSION} \
    && poetry config virtualenvs.create false \
    && poetry install --without docs --without jupyterlab --without codegen \
    && ska_pst_dsp_disk_data_unpack -h \
    && ska_pst_dsp_disk_sine_analyse -h \
    && ska_pst_recv_udpgen -h \
    && ska_pst_stat_file_proc -h

# our base runtime is a TANGO image so the user is tango not pst
USER tango
