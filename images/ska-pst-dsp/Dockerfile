ARG SKA_PST_CORE_BUILDER_IMAGE=""
ARG SKA_PST_CORE_RUNTIME_IMAGE=""
ARG UNAME="pst"

FROM $SKA_PST_CORE_BUILDER_IMAGE as builder

FROM $SKA_PST_CORE_RUNTIME_IMAGE
ARG UNAME

ENV DEBIAN_FRONTEND noninteractive
WORKDIR /usr/local

# Copy binaries and libraries from builder
RUN --mount=type=bind,from=builder,target=/builder \
  cp -d /builder/usr/local/bin/ska_pst_dsp* ./bin/ \
  && cp -d /builder/usr/local/lib/*.so* ./lib/ \
  && cp -rd /builder/usr/local/grpc ./grpc \
  && cp -rd /builder/usr/local/spdlog ./spdlog \
  && cp -rd /builder/usr/local/tempo2 ./tempo2 \
  && cp -rd /builder/usr/local/psrdada ./psrdada \
  && cp -rd /builder/usr/local/psrchive ./psrchive \
  && cp -rd /builder/usr/local/dspsr ./dspsr \
  && cp -rd /builder/etc/ld.so.conf.d/ska-pst-*.conf /etc/ld.so.conf.d/ \
  && ldconfig

ENV LD_LIBRARY_PATH=${LD_LIBRARY_PATH}:/usr/local/psrdada/lib:/usr/local/grpc/lib:/usr/local/spdlog/lib:/usr/local/tempo2/lib:/usr/local/psrchive/lib:/usr/local/dspsr/lib
ENV PATH=${PATH}:/usr/local/psrdada/bin:/usr/local/tempo2/bin:/usr/local/psrchive/bin:/usr/local/dspsr/bin

USER $UNAME

RUN ska_pst_dsp_disk_data_unpack -h \
  && ska_pst_dsp_disk_monitor -h \
  && ska_pst_dsp_disk_perftest -h \
  && ska_pst_dsp_disk_rand_generate -h \
  && ska_pst_dsp_disk_rand_validate -h \
  && ska_pst_dsp_disk_sine_analyse -h \
  && ska_pst_dsp_demo -h \
  && ska_pst_dsp_info
