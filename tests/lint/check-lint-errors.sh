#!/bin/bash

# Count the number of linting failures
total_linting_issues=$(grep '^\s*<failure.*$' build/clang-tidy-junit.xml)

# Exclude and failures, leaving warnings, that occur with .cu files, due mostly to unsupported compile time flags
count=$(grep '^\s*<failure.*$' build/clang-tidy-junit.xml | grep -v '^\s*<failure type="failure".*.cu">$' | wc -l)

# Check if count is greater than 0
if [ $count -gt 0 ]; then
    echo "$count lint errors found. Exiting with -1"
    exit -1
else
    echo "No errors found."
fi
