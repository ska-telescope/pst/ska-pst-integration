-- Create the testdb database
CREATE DATABASE testdb;

-- Create the admin user with a password
CREATE USER admin;

-- Grant all privileges on the testdb database to admin
GRANT ALL PRIVILEGES ON DATABASE testdb TO admin;

-- Switch db connection
\c testdb;

-- Grant all privileges on all current tables to admin
GRANT ALL PRIVILEGES ON ALL TABLES IN SCHEMA public TO admin;

-- Grant CREATE permissions on the database to your user
GRANT CREATE ON DATABASE testdb TO admin;

-- Grant CREATE and USAGE on the public schema specifically
GRANT USAGE, CREATE ON SCHEMA public TO admin;
GRANT USAGE ON SCHEMA public TO admin;

-- Ensure the user has the necessary role to perform operations
ALTER SCHEMA public OWNER TO admin;
