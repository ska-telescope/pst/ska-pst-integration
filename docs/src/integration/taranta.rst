.. _integration:

.. raw:: html

    <style> .red {color:red} </style>

.. role:: red

Taranta
=======

This page describes how to enable the Taranta dashboard and use the PST pre-configured interface.

Deployment
----------

The Taranta dashboard can be deployed as part of the PST ``test-parent`` chart.
It is disabled by default, and can be enabled by adding ``--set ska-taranta.enabled=true``
to the command line when launching the chart; e.g.

.. code-block:: bash

  make k8s-install-chart K8S_CHART=test-parent K8S_CHART_PARAMS=" --set ska-taranta.enabled=true --values=tests/integration/k8srunner/gitlab.test-parent.yaml"

After deploying the ``test-parent`` chart, visit the Taranta Dashboard in your browser;
e.g. if the chart is deployed in the ``ska-pst`` namespace on the Low PSI, visit

http://psi-low.atnf.csiro.au/ska-pst/taranta/dashboard

Login (in the top-right corner) with the following credentials for development and testing:

* User: PST
* Password: PST_SKA

After successful login, under My Dashboards, click 'SKA PST' for the default PST Taranta Dashboard.

Configuration
-------------
The interface of the Taranta dashboard is configurable, and its configuration can be imported or exported using a JSON file.

Import a Taranta Dashboard
~~~~~~~~~~~~~~~~~~~~~~~~~~

Perform the following steps for loading the PST Taranta dashboard configuration.

1. Click the Dashboard tab of the right hand panel.

2. Click the Import dashboard button. Select the dashboard configuration desired to be loaded.

The SKA PST Taranta Dashboards configuration can be download from the `SKA PST repository <https://gitlab.com/ska-telescope/pst/ska-pst/-/blob/main/dashboards/taranta>`_
and then imported into the Taranta server. The following dashboards are available:

* PST_OVERVIEW.wj - a dashboard with a high level overview of the PST system.
* PST_SCAN.wj - a dashboard that can be used to monitor PST scan related attributes, such as data receive and record rates.
* PST_STAT.wj - a dashboard that can be used to track complex voltage data statistics during a scan and used to monitor the mean and variance of the data.
* SKA PST.wj - the legacy dashboard, the combination of the 3 other dashboards should be used instead.

Export a Taranta Dashboard
~~~~~~~~~~~~~~~~~~~~~~~~~~

Perform the following steps to export a PST Taranta dashboard configuration.

1. Click the Dashboard tab of the right hand panel.

2. Click the down arrow icon of the dashboard configuration to be exported.


