
# Software Requirements

## Launch

Deployment of PST requires [Kubernetes](https://kubernetes.io/docs/home) and the [Helm package manager](https://helm.sh/docs/intro/). You might also need to obtain cluster access credentials.

In order to better use TANGO-controls in Kubernetes, 
PST employs the [ska-tango-operator](https://gitlab.com/ska-telescope/ska-tango-operator)
to extend Kubernetes with a Custom Resource Definition and a Controller.
To install the ``ska-tango-operator``

```bash
git clone --recursive git@gitlab.com:ska-telescope/ska-tango-operator.git
cd ska-tango-operator
make k8s-install-chart
```

On bare-metal clusters (like minikube and Docker Desktop) it may also be necessary to install the 
MetalLB Load Balancer as described in the [Installation](https://metallb.universe.tf/installation/) instructions.

If using minikube or Docker Desktop, it may also be necessary to enable some storage class emulators (for ``nfs``, ``nfss1``, ``block``, and ``bds1``);
this can be done by downloading [sc.yaml](https://gitlab.com/ska-telescope/sdi/ska-cicd-deploy-minikube/-/raw/master/scripts/sc.yaml).

For minikube, run

```bash
kubectl apply -f sc.yaml
```

For Docker Desktop, run

```bash
sed -e 's|k8s.io/minikube-hostpath|docker.io/hostpath|g' sc.yaml > ddsc.yaml
kubectl apply -f ddsc.yaml
```

## External Access

To access the Data Product Dashboard or JupyterLab instances that are deployed with the PST `test-parent` chart, it may be necessary to install the Ingress NGINX Controller as described in the [Installation Guide](https://kubernetes.github.io/ingress-nginx/deploy/).

## Further Help

For more information about the required utilities, please use the following links:

- [helm](https://helm.sh/docs/helm/helm/)
- [kubectl](https://kubernetes.io/docs/reference/generated/kubectl/kubectl-commands)
- [ska-tango-operator](https://gitlab.com/ska-telescope/ska-tango-operator)
- [k9s](https://k9scli.io)
- [minikube installation and configuration](https://gitlab.com/ska-telescope/sdi/ska-cicd-deploy-minikube)
- [MetalLB](https://metallb.universe.tf)
- [ingress-nginx](https://kubernetes.github.io/ingress-nginx/)
