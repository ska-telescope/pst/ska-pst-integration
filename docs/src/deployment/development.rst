.. _development_deployment:

.. raw:: html

    <style> .red {color:red} </style>

.. role:: red

Self-Test Deployment
========================

This page describes how to deploy the PST
for development and testing purposes via the ``test-parent`` chart.
The ``test-parent`` chart configures and deploys various components
that simulate all of the required PST interfaces, including

- Tango infrastructure for monitoring and control
- JupyterLab for controlling and monitoring PST via notebooks
- signal generator that simulates the stream of beam-formed voltage data from CBF
- configured storage for PST output data products
- configured storage, as would be used to send PST output data to SDP
- Data Product Dashboard for registering PST output data products

Configure environment
---------------------

First clone the repo and submodules to your local file system

.. code-block:: bash

    git clone --recursive git@gitlab.com:ska-telescope/pst/ska-pst.git
    cd ska-pst

The **repository must be clean** (such that `git diff` returns an empty string) or Things Will Fail.
(The ``getVersion()`` function defined in ``make/.make-release-support`` will add ``-dirty``
to the end of the version string; consequently, images will not be found, resulting in ``ErrImagePull`` and
``ImagePullBackOff`` status messages.)

The following environment variables control the deployment and testing of the PST:

.. list-table::
  :widths: auto
  :header-rows: 1

  * - Environment variable
    - Default value
    - Description
  * - ``KUBE_NAMESPACE``
    - ``ska-pst``
    - Kubernetes namespace for deployment
  * - ``K8S_CHART``
    - ``ska-pst``
    - Name of the Helm chart to be installed; for simulation mode, use ska-pst


If running in a local kubernetes environment, either use the default namespace or create a namespace; e.g.

.. code-block:: bash

    kubectl create namespace ska-pst

Configure the namespace used by ``helm`` and ``kubectl``:

.. code-block:: bash

    export HELM_NAMESPACE=ska-pst
    export KUBE_NAMESPACE=ska-pst
    kubectl config set-context --current --namespace=ska-pst

In the remainder of the documentation, it is assumed that the namespace is either the default or configured appropriately.

Deploy PST
--------------

Ensure that Helm is configured to used the SKAO artefact repository.

.. code-block:: bash

    helm repo add ska https://artefact.skao.int/repository/helm-internal


Deploy PST into the default namespace by installing the ``test-parent`` chart with the values file that enables the PST JupyterLab.

.. code-block:: bash

    make k8s-install-chart K8S_CHART=test-parent \
        K8S_CHART_VALUES="tests/integration/k8srunner/gitlab.test-parent.yaml"

    # Wait for the LMC pod to complete initialisation
    make k8s-wait KUBE_APP=ska-pst

The ``gitlab.test-parent.yaml`` values file configures helm to pull PST images from ``registry.gitlab.com``.

Deploy PST In Simulation mode
-----------------------------

PST can be deployed in a simulation mode without the need for the data signal processing pipeline to be running. When deployed in
simulation mode only the ``ska-pst-lmc`` chart is used and the ``PST BEAM.MGMT`` TANGO device is put into simulation mode. This should
only be used in a testing environment where there is no need for the digital signal processing pipeline and only a need to interact with
the PST LMC (e.g. CSP.LMC testing).

.. code-block:: bash

    make k8s-install-chart K8S_CHART=test-parent \
        K8S_CHART_PARAMS=" --values=tests/integration/k8srunner/simulation.ska-pst.yaml \
        --set ska-pst.image.registry=registry.gitlab.com/ska-telescope/pst/ska-pst"

    # Wait for the LMC pod to complete initialisation
    make k8s-wait KUBE_APP=ska-pst

The ``simulation.ska-pst.yaml`` values file does not override the default ``artefact.skao.int`` registry for PST images.

Interact with PST
---------------------

See :ref:`Operation / Using Jupyter <operation_jupyter>` for instructions on how to interact with PST via Jupyter notebooks.

Shut down PST
-----------------

When finished with PST, clean up the deployment.

.. code-block:: bash

    make k8s-uninstall-chart K8S_CHART=test-parent

