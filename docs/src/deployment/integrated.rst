.. _release_deployment:

.. raw:: html

    <style> .red {color:red} </style>

.. role:: red

Integrated Deployment
=====================

This page describes how to deploy PST for integration and verification
purposes via the ``ska-pst`` chart.  The ``ska-pst`` chart is intended for
integration in a larger umbrella chart that configures and deploys

- Tango infrastructure for monitoring and control
- Central Signal Processor (CSP) Local Monitoring and Control (LMC)
- Low/Mid Correlator Beam Former (CBF)
- configured storage for PST output data products
- persistent storage, as used by the Science Data Processor (SDP)
- Data Product Dashboard (DPD)

In this case PST is not responsible for creating
the Helm chart and values but there are values that would need to be overriden
in a ``values.yaml`` file to allow PST to be deployed in different environments.

Example Chart.yaml
~~~~~~~~~~~~~~~~~~

The following is an example ``Chart.yaml`` that can be used for an integrated deployment
that includes PST.  This file is based off the
`Low CBF Integration Testing - Chart.original.yaml <https://gitlab.com/ska-telescope/low-cbf/ska-low-cbf-integration/-/blob/main/charts/ska-low-cbf-integration/Chart.original.yaml?ref_type=heads>`_

.. code-block:: yaml

    apiVersion: v2
    name: ska-low-cbf-integration
    description: Low CBF Integration Testing Environment
    type: application
    version: 0.0.1
    appVersion: 0.0.1
    icon: https://www.skatelescope.org/wp-content/uploads/2016/07/09545_NEW_LOGO_2014.png
    dependencies:
      # PST Dependencies - this will only be enabled if the values.yaml file has the ska-pst enabled.
      - name: ska-pst
        version: 1.0.2
        repository: https://artefact.skao.int/repository/helm-internal
        condition: ska-pst.enabled

      # PST Sends files to the SDP data product dashboard (DPD) and this will need to be enabled and
      # deployed alongside the PST to allow files to be send there.
      - name: ska-dataproduct-dashboard
        version: 0.11.0
        repository: https://artefact.skao.int/repository/helm-internal
        condition: ska-dataproduct-dashboard.enabled

      # Low CBF - see Low CBF documentation for details
      - name: ska-low-cbf
        version: ">=0.7.0"
        repository: https://artefact.skao.int/repository/helm-internal

      - name: ska-low-cbf-conn
        version: ">=0.5.1"
        repository: https://artefact.skao.int/repository/helm-internal
        condition: ska-low-cbf.ska-low-cbf-conn.enabled

      - name: ska-low-cbf-proc
        version: ">=0.11.0"
        repository: https://artefact.skao.int/repository/helm-internal
        condition: ska-low-cbf.ska-low-cbf-proc.enabled

      - name: ska-low-cbf-tango-cnic
        version: ">=0.2.1"
        repository: https://artefact.skao.int/repository/helm-internal
        condition: ska-low-cbf.ska-low-cbf-tango-cnic.enabled

      # Upstream SKA Tango dependencies
      # The ska-tango-util is needed to ensure the templating is done correctly
      - name: ska-tango-util
        version: 0.4.2
        repository: https://artefact.skao.int/repository/helm-internal
      # TANGO Database Device server
      - name: ska-tango-base
        version: 0.4.2
        repository: https://artefact.skao.int/repository/helm-internal
      - name: ska-tango-taranta
        version: 2.2.0
        repository: https://artefact.skao.int/repository/helm-internal
        condition: ska-taranta.enabled
      - name: ska-tango-tangogql
        version: 1.3.7
        repository: https://artefact.skao.int/repository/helm-internal
        condition: ska-taranta.enabled
      - name: ska-tango-taranta-auth
        version: 0.1.6
        repository: https://artefact.skao.int/repository/helm-internal
        condition: ska-taranta-auth.enabled
      - name: ska-tango-taranta-dashboard
        version: 1.4.0
        repository: https://artefact.skao.int/repository/helm-internal
        condition: ska-dashboard-repo.enabled
      - name: ska-tango-taranta-dashboard-pvc
        version: 1.4.0
        repository: https://artefact.skao.int/repository/helm-internal
        condition: ska-dashboard-repo.enabled
      - name: ska-tango-archiver
        version: 2.6.0
        repository: https://artefact.skao.int/repository/helm-internal
        condition: ska-tango-archiver.enabled

      # Low TMC
      - name: ska-tmc-low
        version: "0.14.1"
        repository: https://artefact.skao.int/repository/helm-internal
        condition: ska-tmc-low.enabled

      # CSP LMC Low
      - name: ska-csp-lmc-low
        version: "0.11.1"
        repository: https://artefact.skao.int/repository/helm-internal
        condition: ska-csp-lmc-low.enabled

Example values.yaml
~~~~~~~~~~~~~~~~~~~

The following is an example of values that would need to go in to the overall
values file in the integrated deployment to enable the PST

.. code-block:: yaml

    # Enable the SDP DPD
    ska-dataproduct-dashboard:
        enabled: true

    # Enable the PST Chart
    ska-pst:
        enabled: true
        telescope: SKA-Low

        # Setup the low-beam-01 instance
        deviceServers:
          # TANGO specific parameters for the PST BEAM.LMC
          pstBeam:
            instances: ["01"]
            # don't deploy PST in simulation mode
            simulationMode: 0
            # set up monitoring between the PST LMC and the PST CORE apps as 5secs
            monitor_polling_rate: 5000
            # set up the health check interval between the PST LMC and the PST CORE apps at 1sec
            health_check_interval: 1000

          # Set up the persistent volume claim of PST LMC, this needs to be readOnly
          # false for write permissions on the mounted path defined in
          # ska-pst.core.primaryVolumeMount.containerPath
          volume:
            readOnly: false

          # This is needed to allow it to deploy alongside ska-pst-core
          # Because the PST CORE has the toleration of running on a Nvidia GPU
          # enabled node the LMC pod needs to have the same toleration.
          tolerations:
          - effect: NoExecute
              key: nvidia.com/gpu
              value: "true"

        # Setup the PST CORE pod/deployment that performs the digital signal processing
        core:

          # Enable K8s host network as the RECV application needs to
          # listen to a physical network card so the CBFs P4 switch can
          # route traffic to RECV.
          hostNetwork: true

          # Enable nvidia capabilities to the ska-pst core containers using
          # nvidia's container runtime configuration.
          runtimeClassName: nvidia

          # Allow DSP flow through application to utilise CUDA capable GPU.
          applications:
            dsp_ft:
              image: ska-pst-dsp
              command: ["bash"]
              args:
                - "-c"
                - "ska_pst_dsp_ft -d ${PST_DSP_MOUNT} -g 0 -c ${DSP_FT_MGMT_PORT}"

          # Define which K8s node to have the CORE pod to be deployed to
          # In this case it is running on the pst-beam2 in the Low PSI
          nodeSelector:
            kubernetes.io/hostname: pst-beam2

          # PST CORE needs to run on a GPU server, also the pst-beam2
          # has a K8s taint of not allowing anything to run unless it
          # specifically tolerates being on a server with a Nvidia GPU.
          tolerations:
            - effect: NoExecute
              key: nvidia.com/gpu
              value: "true"

          # Define the size of a local file system mount.  In this
          # case it is enabled with 50GB of data.

          # Use use cephfs nfs instead of baremetal disk
          primaryVolumeMount:
            name: ska-pst-lfs
            type: nfs
            hostPath: ""
            containerPath: /mnt/lfs
            permission: 0764
            size: 50G

          # Specify the IP address of the Physical Network interface (NIC)
          # that the PST RECV application would listen on.
          recv:
            dataNic: 192.168.0.101

          # Enable the PST SEND application that would send the output
          # files to the SDP DPD
          send:
              enabled: true
              dpdApiIntegration: true

Network Attachment Definition
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

If a Network Attachment Definition (NAD) has been installed for use,
then the ``ska-pst-core`` chart can be configured to use it as
described in the :ref:`SKA PST Helm Chart Parameters`.
