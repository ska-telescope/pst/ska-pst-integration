# SKA PST Helm Chart Parameters

This is a summary of the Helm chart parameters that can be used to customise PST deployment, configuration, and integration.
The defaults values can be located in the chart's [values file](https://gitlab.com/ska-telescope/pst/ska-pst/-/blob/main/charts/ska-pst/values.yaml>).

## Parameter Descriptions

### Global Values

Global values are used to propagate configuration to the `ska-pst` chart.

| Parameter | Description | Default |
| --- | --- | --- |
|`global.data-product-pvc-name`| Static shared storage expected to be set in a parent chart. This value takes precedence over `core.send.dataProductPVC`. Involved PersistentVolumeClaim is expected to exist. | `test-pst-storage` |

### CORE

These parameters are used for configuring deployments of the core PST signal processing chain.

| Parameter | Description | Default |
| --- | --- | --- |
|`core.enabled`| Enable/Disable PST.CORE deployment | `true` |
|`core.affinity`| Affinity applied to the ska-pst deployment | `{}` |
|`core.applications`| Application specific configuration that overrides container level configuration ||
|`core.applications.recv`|Contains RECV configuration||
|`core.applications.recv.dataNic`|Overrides the PodIP with the provided IP. Used with `core.hostNetwork=true`|`""`|
|`core.applications.recv.image`|Container image|`ska-pst-recv`|
|`core.applications.recv.command`|Container commands|`["bash"]`|
|`core.applications.recv.args`|Container command arguments|`["-c","ska_pst_recv_udpdb ${POD_IP} -p ${RECV_DATASTREAM_PORT} -c ${RECV_MGMT_PORT}"]`|
|`core.applications.recv.ports`|RECV port configurations||
|`core.applications.recv.ports.mgmt`|RECV management port configurations. Used for communication with LMC.||
|`core.applications.recv.ports.mgmt.lmcPropertyName`|Device property name configured in beam device configuration|`RecvProcessApiEndpoint`|
|`core.applications.recv.ports.mgmt.port`|PST service port for RECV management port|`28080`|
|`core.applications.recv.ports.mgmt.protocol`|PST service port protocol for RECV management port|`TCP`|
|`core.applications.recv.ports.mgmt.targetPort`|Pod exposed port used by RECV|`28080`|
|`core.applications.recv.ports.mgmt.datastream`|RECV data stream port configurations. Used for receiving data sent by CBF||
|`core.applications.recv.ports.datastream.port`|PST service port for RECV datastream port|`28080`|
|`core.applications.recv.ports.datastream.protocol`|PST service port protocol for RECV datastream port|`UDP`|
|`core.applications.recv.ports.datastream.targetPort`|Pod exposed port used by RECV|`28080`|
|`core.applications.recv.resources`|Optional resources configuration specific to RECV container|`{}`|
|`core.applications.smrb`|Contains SMRB configuration||
|`core.applications.smrb.image`|Container image|`ska-pst-smrb`|
|`core.applications.smrb.command`|Container commands|`["bash"]`|
|`core.applications.smrb.args`|Container command arguments|`["-c","ska_pst_smrb_core -c ${SMRB_MGMT_PORT}"`|
|`core.applications.smrb.ports`|SMRB port configurations||
|`core.applications.smrb.ports.mgmt`|SMRB management port configurations. Used for communication with LMC.||
|`core.applications.smrb.ports.mgmt.lmcPropertyName`|Device property name configured in beam device configuration|`SmrbProcessApiEndpoint`|
|`core.applications.smrb.ports.mgmt.port`|PST service port for SMRB management port|`28081`|
|`core.applications.smrb.ports.mgmt.protocol`|PST service port protocol for SMRB management port|`TCP`|
|`core.applications.smrb.ports.mgmt.targetPort`|Pod exposed port used by SMRB|`28081`|
|`core.applications.smrb.resources`|Optional resources configuration specific to SMRB container|`{}`|
|`core.applications.stat`|Contains STAT configuration||
|`core.applications.stat.image`|Container image|`ska-pst-stat`|
|`core.applications.stat.command`|Container commands|`["bash"]`|
|`core.applications.stat.args`|Container command arguments|`["-c","ska_pst_stat_core -d ${PST_DSP_MOUNT} -c ${STAT_MGMT_PORT}`|
|`core.applications.stat.ports`|STAT port configurations||
|`core.applications.stat.ports.mgmt`|STAT management port configurations. Used for communication with LMC.||
|`core.applications.stat.ports.mgmt.lmcPropertyName`|Device property name configured in beam device configuration|`StatProcessApiEndpoint`|
|`core.applications.stat.ports.mgmt.port`|PST service port for STAT management port|`28083`|
|`core.applications.stat.ports.mgmt.protocol`|PST service port protocol for STAT management port|`TCP`|
|`core.applications.stat.ports.mgmt.targetPort`|Pod exposed port used by RECV|`28083`|
|`core.applications.stat.resources`|Optional resources configuration specific to STAT container|`{}`|
|`core.applications.dsp_disk`|Contains DSP Voltage recorder mode configuration||
|`core.applications.dsp_disk.image`|Container image|`ska-pst-dsp`|
|`core.applications.dsp_disk.command`|Container commands|`["bash"]`|
|`core.applications.dsp_disk.args`|Container command arguments|`["-c","ska_pst_dsp_disk -d ${PST_DSP_MOUNT} -c ${DSP_DISK_MGMT_PORT}"]`|
|`core.applications.dsp_disk.ports`|DSP Voltage recorder mode port configurations||
|`core.applications.dsp_disk.ports.mgmt`|DSP Voltage recorder mode management port configurations. Used for communication with LMC.|`DspDiskProcessApiEndpoint`|
|`core.applications.dsp_disk.ports.mgmt.lmcPropertyName`|Device property name configured in beam device configuration|``|
|`core.applications.dsp_disk.ports.mgmt.port`|PST service port for DSP Voltage recorder mode management port|`28082`|
|`core.applications.dsp_disk.ports.mgmt.protocol`|PST service port protocol for DSP Voltage recorder mode management port|`TCP`|
|`core.applications.dsp_disk.ports.mgmt.targetPort`|Pod exposed port used by DSP Voltage recorder|`28082`|
|`core.applications.dsp_disk.resources`|Optional resources configuration specific to DSP_DISK container|`{}`|
|`core.applications.dsp_ft`|Contains DSP Flow through mode configuration||
|`core.applications.dsp_ft.image`|Container image|`ska-pst-dsp`|
|`core.applications.dsp_ft.command`|Container commands|`["bash"]`|
|`core.applications.dsp_ft.args`|Container command arguments. Optional flag of `-g $gpu_id` can be used where $gpu_id is replaced with numerical value of a CUDA capable gpu device id|`["-c","ska_pst_dsp_ft -d ${PST_DSP_MOUNT} -c ${DSP_FT_MGMT_PORT}`|
|`core.applications.dsp_ft.ports`|DSP Flow Through mode port configurations||
|`core.applications.dsp_ft.ports.mgmt`|DSP Flow Through mode management port configurations. Used for communication with LMC.||
|`core.applications.dsp_ft.ports.mgmt.lmcPropertyName`|Device property name configured in beam device configuration|`DspFlowThroughProcessApiEndpoint`|
|`core.applications.dsp_ft.ports.mgmt.port`|PST service port for DSP Flow Through mode management port|`28084`|
|`core.applications.dsp_ft.ports.mgmt.protocol`|PST service port protocol for DSP Flow Through mode management port|`TCP`|
|`core.applications.dsp_ft.ports.mgmt.targetPort`|Pod exposed port used by DSP Flow Through mode|`28084`|
|`core.applications.dsp_ft.resources`|Optional resources configuration specific to DSP_FT container|`{}`|
|`core.affinity`|Affinity assigned to the ska-pst-core deployment|`{}`|
|`core.podAnnotations`|Annotations assigned to the `ska-pst-core` pod|`{}`|
|`core.podSecurityContext`|Security context assigned to the `ska-pst-core` pod|`{}`|
|`core.replicaCount`|Number of `ska-pst-core` pod replicas generated by the `ska-pst-core` deployment|`1`|
|`core.runtimeClassName`|RuntimeClass object used to run the core pod. The core pod will not run if the configured runtimeclass does not exist. Used concurrently with `core.applications.dsp_ft.resources.requests=nvidia.com/gpu: 1` with a configuration of `core.runtimeClassName=nvidia` to utilise an nvidia gpu resource|``|
|`core.nodeSelectorTerms`|Ensures `ska-pst-core` resources deployed on nodes with declared labels|`kubernetes.io/os=linux`|
|`core.nodeSelector`|Selector which must match a node's labels for the pod to be scheduled on  that node.|`{}`|
|`core.probes`|Configurations for `ska-pst-core` readiness and liveliness probes||
|`core.probes.legacyMode`|Enables readiness and liveliness probes using port listeners|`true`|
|`core.probes.readiness.initialDelaySeconds`|Number of seconds after the container has started before readiness probes are initiated. Minimum value is 1|`5`|
|`core.probes.readiness.periodSeconds`|How often (in seconds) to perform the probe. Minimum value is 1|`10`|
|`core.probes.liveliness.initialDelaySeconds`|Number of seconds after the container has started before liveliness probes are initiated. Minimum value is 1|`5`|
|`core.probes.liveliness.periodSeconds`|How often (in seconds) to perform the probe. Minimum value is 1|`10`|
|`core.primaryVolumeMount`|The required storage configuration used by DSP to write files||
|`core.primaryVolumeMount.name`|Name of the persistentVolumeClaim used across the `ska-pst` chart|`ska-pst-lfs`|
|`core.primaryVolumeMount.type`|The storage class used. Available options involve `local-storage` (hostPath), `cephfs` (nfss1), `block` (bds) |`local-storage`|
|`core.primaryVolumeMount.hostPath`|Baremetal host path. Used concurrently with `core.primaryVolumeMount.type-local-storage`|`/tmp`|
|`core.primaryVolumeMount.containerPath`|Container mount path used across the `ska-pst` chart|`/mnt/lfs`|
|`core.primaryVolumeMount.permission`|The permission applied to folders created in `core.primaryVolumeMount.containerPath`|`0764`|
|`core.primaryVolumeMount.readOnly`|Permission assigned when the volume is assigned to a pod|`false`|
|`core.primaryVolumeMount.size`|The size requested by the persistentVolumeClaim. Acts as a label when `core.primaryVolumeMount.type=local-storage`|`10Gi`|
|`core.extraVolumeMounts`|List of additional mounts used across the `ska-pst` chart. Fields are the same as `core.primaryVolumeMount`|`[]`|
|`core.send`|Configurations applied to the `ska-pst-send` pod||
|`core.send.enabled`|Feature flag that enables or disables the `ska-pst-core-send` pod|`true`|
|`core.send.initPaths.enableBuild`|Feature flag for creating the base source and destination paths|`false`|
|`core.send.initPaths.enableChown`|Feature flag for recursively changing the ownership of the base source and destination paths|`false`|
|`core.send.enabled`|Feature flag that enables or disables the `ska-pst-core-send` pod|`true`|
|`core.send.dpdApiIntegration`|Feature flag that enables or disables the integration concerning the SDP data product dashboard|`false`|
|`core.send.resources`|Resources allocated to the `ska-pst-core-send` pod|`{}`|
|`core.send.image`|Container image|`ska-pst-send`|
|`core.send.dataProductPVC`|Configurations concerning the the SDP data product shared persistentVolumeClaim||
|`core.send.dataProductPVC.name`|persistentVolumeClaim name. Gets overriden by `global.data-product-pvc-name`|`test-pst-storage`|
|`core.send.dataProductPVC.size`|persistentVolumeClaim size|`5Gi`|
|`core.send.podSecurityContext`|holds pod-level security attributes and common container settings. |`{}`|
|`core.service`|Contains the kubernetes service configurations for the `ska-pst-core` deployment||
|`core.service.enabled`|Feature flag that enables or disables the kubernetes service for the `ska-pst-core` deployment|`true`|
|`core.service.type`|The kubernetes service type. Available options are `ClusterIP` `LoadBalancer`, and `NodePort`|`ClusterIP`|

### LMC

These parameters are used for configuring deployments of the local monitoring and control (LMC) part of the PST deployment.

| Parameter | Description | Default |
| --- | --- | --- |
|`system`||`SW-infrastructure`|
|`subsystem`||`ska-pst-lmc`|
|`telescope`||`SKA-low`|
|`deviceServers.pstLmc`|Contains device service configuration for the PST.BEAM||
|`deviceServers.pstLmc.enabled`|Enable/Disable PST.BEAM LMC deployment | `true` |
|`deviceServers.pstLmc.serverName`|The server name assigned to the PST.BEAM|`low-pst-beam` for SKA-Low<br>`mid-pst-beam` for SKA-Mid|
|`deviceServers.pstLmc.image`|Container image|`ska-pst-lmc`|
|`deviceServers.pstLmc.instances`|Beam instances list|`["01"]`|
|`deviceServers.pstLmc.file`|File path containing the beam template configuration|`"data/beam.yaml"`|
|`deviceServers.pstLmc.simulationMode`|Run Beam logical device in simulation (`0`) or non simulation (`1`) mode|`0`|
|`deviceServers.pstLmc.monitor_polling_rate`|Monitor polling rate (in milliseconds) used by the beam server|`5000`|
|`deviceServers.pstLmc.health_check_interval`|The health check interval (in milliseconds) used by the beam server to the PST subcomponents|`1000`|
|`deviceServers.pstLmc.scanOutputDirPattern`|Relative path pattern concerning data files|`product/<eb_id>/<subsystem_id>/<scan_id>`|
|`deviceServers.pstLmc.volume.readOnly`|Pod permission assigned to volume mount concerning the shared pst volume|`false`|
|`loggingLevel`|The logging level assigned by the beam server to the PST.CORE applications|`4`|
|`labels`|Labels assigned to the beam server|`app: ska-pst-lmc`|
|`annotations`|Annotations assigned to the beam server||
|`annotations.app.gitlab.com/app`|The gitlab source code repository for the pst beam server|`CI_PROJECT_PATH_SLUG`|
|`annotations.app.gitlab.com/env`|The environment concerning the kubernetes cluster|`CI_ENVIRONMENT_SLUG`|
|`resources.requests.cpu`|Defines the minimum amount of compute resources required|`50m`|
|`resources.requests.memory`|Defines the minimum amount of memory resources required|`50Mi`|
|`resources.limits.cpu`|Defines the maximum amount of compute resources allowed|`100m`|
|`resources.limits.memory`|Defines the maximum amount of memory resources allowed|`150Mi`|
|`livenessProbe`|Probe applied to the beam server. Periodic probe of container liveness. Container will be restarted if the probe fails||
|`livenessProbe.initialDelaySeconds`|Number of seconds after the container has started before liveness probes are initiated|`0`|
|`livenessProbe.periodSeconds`|How often (in seconds) to perform the probe. Minimum value is 1|`10`|
|`livenessProbe.timeoutSeconds`|Number of seconds after which the probe times out. Minimum value is 1.|`3`|
|`livenessProbe.successThreshold`|Minimum consecutive successes for the probe to be considered successful after having failed. Must be 1 for liveness and startup. Minimum value is 1.|`1`|
|`livenessProbe.failureThreshold`|Minimum consecutive failures for the probe to be considered failed after having succeeded. Minimum value is 1|`3`|
|`readinessProbe`|Probe applied to the beam server. Periodic probe of container service readiness. Container will be removed from service endpoints if the probe fails||
|`readinessProbe.initialDelaySeconds`|Number of seconds after the container has started before liveness probes are initiated|`0`|
|`readinessProbe.periodSeconds`|How often (in seconds) to perform the probe. Minimum value is 1|`10`|
|`readinessProbe.timeoutSeconds`|Number of seconds after which the probe times out. Minimum value is 1.|`3`|
|`readinessProbe.successThreshold`|Minimum consecutive successes for the probe to be considered successful after having failed. Must be 1 for liveness and startup. Minimum value is 1.|`1`|
|`readinessProbe.failureThreshold`|Minimum consecutive failures for the probe to be considered failed after having succeeded. Minimum value is 1|`3`|
|`securityContext.runAsNonRoot`|Indicates that the beam server container must run as a non-root user. If true, the Kubelet will validate the image at runtime to ensure that it does not run as UID 0 (root) and fail to start the container if it does. If unset or false, no such validation will be performed.|`true`|
|`securityContext.runAsUser`|The UID to run the entrypoint of the beam server container process|`1000`|
|`securityContext.runAsGroup`|The GID to run the entrypoint of the beam server container process|`1000`|
|`tolerations`|The beam server pod this Toleration is attached to tolerates any taint that matches the triple <key,value,effect> using the matching operator <operator>.|`[]`|

### Jupyterlab

These parameters are used for configuring deployment of a JupyterLab pod within the PST deployment. This
should only be enabled in a testing environment.

| Parameter | Description | Default |
| --- | --- | --- |
|`jupyterlab`|Contains configuration concerning jupyterlab deployment||
|`jupyterlab.enabled`|Feature flag to enable or disable jupyterlab deployment|`false`|
|`jupyterlab.image`|Container image|`ska-pst-jupyterlab`|
|`jupyterlab`|Container commands|`["jupyter"]`|
|`jupyterlab`|Container command arguments|`["lab", "--no-browser", "--ip=0.0.0.0", "--notebook-dir=/app/notebooks", "--IdentityProvider.token=''", "--ServerApp.password=''"]`|
|`jupyterlab.service`|Contains kubernetes service configuration for the `ska-pst-jupyterlab` pod||
|`jupyterlab.service.enabled`|Feature flag that enables or disables the kubernetes service for the `ska-pst-jupyterlab` pod|`true`|
|`jupyterlab.service.type`|The kubernetes service type. Available options are `ClusterIP` `LoadBalancer`, and `NodePort`|`ClusterIP`|
|`jupyterlab.service.ports.jupyterlab`|Jupyterlab service port configurations||
|`jupyterlab.service.ports.jupyterlab.port`|The port that will be exposed by the jupyterlab service|`8888`|
|`jupyterlab.service.ports.jupyterlab.protocol`|The IP protocol for the exposed port. Supports `TCP`, `UDP`, and `SCTP`.|`TCP`|
|`jupyterlab.service.ports.jupyterlab.targetPort`|Number or name of the port to access on the pods targeted by the jupyterlab service|`8888`|
|`jupyterlab.resources`|Resources allocated to the `ska-pst-jupyterlab` pod||
|`jupyterlab.resources.requests.cpu`|Defines the minimum amount of compute resources required|`2000m`|
|`jupyterlab.resources.requests.memory`|Defines the minimum amount of memory resources required|`4Gi`|
|`jupyterlab.resources.limits.cpu`|Defines the maximum amount of compute resources allowed|`2000m`|
|`jupyterlab.resources.limits.memory`|Defines the maximum amount of memory resources allowed|`4Gi`|

### SDP

| Parameter | Description | Default |
| --- | --- | --- |
|`sdp`|Contains sdp api configurations used for integration with `ska-pst` chart||
|`sdp.namespace`|The namespace involving the data product dashboard. Used for the FQDN in the internal k8s network to interact with the SDP dataproduct dashboard API's|`""`|
|`sdp.containerPath`|Mount path of the shared sdp storage used across the `ska-pst` containers|`"/mnt/sdp"`|
|`sdp.ApiServiceName`|The service name of the dataproduct dashboard API|`ska-dataproduct-dashboard-api-service`|
|`sdp.ApiServicePort`|The service port of the dataproduct dashboard API|`8000`|
|`sdp.waitInterval`|Time, in seconds, to wait between checking if service is running. Only used in the initContainers of SEND|`5`|

## Override Examples

### Use gitlab registry and override storage configuration

```yaml
image:
  registry: &SkaPstRepository "registry.gitlab.com/ska-telescope/pst/ska-pst"

ska-pst:
  image:
    registry: *SkaPstRepository

  core:
    primaryVolumeMount:
      type: nfs
```

### Increase resource allocated to PST.CORE and PST.LMC pods

```yaml
ska-pst:

  telescope: SKA-Mid

  # NOTE:
  # The resources are allocated on a container level on a Pod.
  #   The number of containers needs to be considered to determine
  #   the total resources to be allocated.
  #
  # The ska-pst-core pod includes the following containers:
  #   recv, dsp-disk, dsp-ft, smrb, stat.
  #
  # Below is the equation for the configuration
  #   total_configuration = ncontainer * configuration_value
  #
  #  The below configuration results into the following
  #  resource allocations
  #     total cpu limit        8000m
  #     total cpu request      8000m
  #     total memory limit     128000 Mi
  #     total memory request   60000 Mi
  core:
    resources:
      limits:
        cpu: 1600m
        memory: 25600Mi
      requests:
        cpu: 1600m
        memory: 12000Mi

  deviceServers:
    pstLmc:
      resources:
        limits:
          cpu: 200m
          memory: 300Mi
        requests:
          cpu: 100m
          memory: 100Mi
```

### Enable jupyterlab for manual testing of PST

```yaml
ska-pst:
  jupyterlab:
    enabled: true
```

### Deploy ska-pst to pst-beam2 in PSI Low environment

```yaml
ska-pst:

  telescope: SKA-Low

  # Applied across pst pods
  tolerations:
    - effect: NoExecute
      key: nvidia.com/gpu
      value: "true"

  core:
    # Required for pod deployments
    nodeSelector:
      kubernetes.io/hostname: pst-beam2

    # Required for persistentvolume deployments
    nodeSelectorTerms:
      - matchExpressions:
        - key: kubernetes.io/hostname
          operator: In
          values:
          - "pst-beam2"
```

### Deploy ska-pst with cuda enabled dsp_ft

```yaml
image:
  registry: &SkaPstRepository "registry.gitlab.com/ska-telescope/pst/ska-pst"


ska-pst:
  image:
    registry: *SkaPstRepository

  tolerations:
    - effect: NoExecute
      key: nvidia.com/gpu
      value: "true"

  core:
    applications:
      dsp_ft:
        resources:
          requests:
            nvidia.com/gpu: 1
          limits:
            nvidia.com/gpu: 1
    nodeSelector:
      kubernetes.io/hostname: pst-beam2

    nodeSelectorTerms:
      - matchExpressions:
        - key: kubernetes.io/hostname
          operator: In
          values:
          - "pst-beam2"

    primaryVolumeMount:
      type: local-storage
      hostPath: /data


```
