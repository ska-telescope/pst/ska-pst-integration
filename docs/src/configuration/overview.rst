.. _configuration_overview:

Helm Chart Overview
===================

The ``ska-pst`` chart is a mono chart that integrates the functionalities of the ``PST.CORE`` and ``PST.LMC`` applications.

The ``PST.CORE``, consists of the following components.
For further configuration information, see the documentation for each component.

- `RECV.CORE <../components/recv/index.html>`_
- `SMRB.CORE <../components/smrb/index.html>`_
- `DSP.DISK and DSP.FT <../components/dsp/index.html>`_
- `SEND.CORE <../components/send/index.html>`_
- `STAT.CORE <../components/stat/index.html>`_

The ``PST.LMC`` refers to all of the Tango infrastructure required for monitoring and controlling the ``PST.CORE``
See the `SKA PST LMC Documentation <../components/lmc/index.html>`_ for more information.


Production Mode
---------------

By default, the ``ska-pst`` chart is configured to deploy in production mode, such that

- ``PST.CORE`` is deployed through ``core.enabled: true`` and is to be waiting for gRPC communication with ``ska-pst-lmc`` and
- ``PST.LMC`` is deployed through ``deviceServers.pstLmc.enabled: true`` with simulation mode turned off

.. code-block:: yaml

  ska-pst:
    # Enable the deployment of the ska-pst chart
    enabled: true

    core:
      # Enable the deployment of the PST.CORE applications
      enabled: true

    deviceServers:
      pstLmc:
        # Enable the deployment of the PST.LMC which controls PST.CORE through gRPC
        enabled: true


Probes are configured to to ping ``low-pst`` devices recognised through ``tango_admin``; for example

.. code-block:: bash

    tango_admin -ping --ping-device low-pst/recv/01

Simulation Mode
---------------

The ``ska-pst`` chart can be deployed in simulation. To run it in simulation mode,
the ``PST.CORE`` must be disabled and the ``simulationMode`` flags of
the ``PST.LMC`` must be enabled. The resulting configuration is as follows.

.. code-block:: yaml

  ska-pst:
    core:
      enabled: false

    deviceServers:
      pstLmc:
        simulationMode: 1

    affinity:
      podAffinity:
        requiredDuringSchedulingIgnoredDuringExecution: []
