.. _configuration_override:

Overriding Parameters
=========================

Render
----------

The following example shows how to download the default ``ska-pst`` Helm chart values from the SKAO nexus.

.. code-block:: bash

    # Add the SKAO helm repository
    helm repo add skao https://artefact.skao.int/repository/helm-internal

    # Search the repository for ska-pst chart
    helm search repo skao/ska-pst

    # Store default values to file
    helm show values skao/ska-pst > values.yaml

The ``values.yaml`` file can be modified as needed and then validated using one of the two following methods.
If using the SKA makefile machinery,

.. code-block:: bash

    make k8s-template-chart

Alternatively, using Helm,

.. code-block:: bash

    helm template skao/ska-pst


Deploy
----------

First, set the ``KUBE_NAMESPACE`` and ``KUBE_APP`` environment variables.

.. code-block:: bash

    # Declare k8s namespace
    export KUBE_NAMESPACE=pst
    # Declare the chart to deploy (default chart is `test-parent`)
    export K8S_CHART=ska-pst
    # Declare target application for wait
    export KUBE_APP=ska-pst

Then, if using the SKA makefile machinery,

.. code-block:: bash

    # Install using overrides
    make K8S_CHART_VALUES="value.yaml" k8s-install-chart

    # Wait for pods to be in a ready or completed state (optional)
    make k8s-wait

Alternatively, using Helm

.. code-block:: bash

    # Install using overrides
    helm install --values=value.yaml --generate-name skao/ska-pst

    # Wait for pods to be in a ready or completed state (optional)
    kubectl -n ${KUBE_NAMESPACE} wait pod --for=condition=ready -l app=${KUBE_APP} --timeout=360s

