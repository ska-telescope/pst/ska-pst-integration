.. _release:

.. raw:: html

    <style> .red {color:red} </style>

.. role:: red

Release Process
===============

Before embarking on a PST release, first consult with the Product Manager and read the guidance at
`Release Management Process <https://confluence.skatelescope.org/display/SE/Guidelines%3A+Release+Management+Process>`_.

This document describes only the technical steps of the release process;
it does not describe any of the required planning and associated
communication that should take place with the Product Manager,
as described in the above document.

Check for existing releases
---------------------------

Before starting a new Draft release, check to see if there are any
already planned or in progress by visiting

https://jira.skatelescope.org/issues/?filter=19129

Compute the next version of ska-pst
-----------------------------------

In the terminal, ensure that you are on the current version of the main branch

.. code-block:: bash

  cd ska-pst
  git checkout main
  git pull --rebase

Make note of the current version number

.. code-block:: bash

  cat .release

and decide if you will bump the major, minor or patch number. See
`Release Management REL project and Product Versions <https://confluence.skatelescope.org/display/SE/Jira+Usage+Guidelines#JiraUsageGuidelines-ReleaseManagementRELprojectandProductVersions>`__
for advice.

**Let the new version number be 0.4.0 in the remainder of this example.**

Create REL issue for ska-pst
----------------------------

Visit https://jira.skatelescope.org/projects/REL/issues/ and click the blue Create button. In the pop up, enter

- Summary: PST v0.4.0

- Type: Sub-system Version

- Component(s): COM PST Software

- Fix Version/s: <current PI>

- Delivers: <relevant features>

- Tests: add all of the PST tests marked as Done; e.g. using
  `this JQL filter <https://jira.skatelescope.org/issues/?filter=19131>`__

- Environments: tick all of the boxes up to the last system targeted for deployment;
  e.g. Development, Staging, Prototype System Integration (PSI)

After creating the issue in Jira, add an “Issue Link” and relate the REL issue to the AT3 story for the release.

Release ska-pst components as needed
------------------------------------

Depending on the versions of PST components that are currently required
by the ska-pst-core and ska-pst-lmc umbrella helm charts, it may be
necessary to release the following packages

- ska-pst-buildtools

- ska-pst-dsp-tools

- ska-pst

All three packages are released using the following Common Release Instructions,
and special considerations for each package are listed below.

Common Release Instructions
~~~~~~~~~~~~~~~~~~~~~~~~~~~

This section defines the generic pattern for releasing a package.
Start by ensuring that everything is up to date.

.. code-block:: bash

  cd ska-pst<-pkg>
  git checkout main
  git pull --rebase
  git submodule update --init

Create a release branch

.. code-block:: bash

  git checkout -b rel-708-release-pst<-pkg>-0-4-0

Update the infrastructure

.. code-block:: bash

  make make

Double check that everything is ok and commit any changes; e.g.

.. code-block:: bash

  git commit -m "REL-708 AT3-451 commit after make make" .

Note that both the REL and AT3 tickets are in the commit name, so that
the commit gets linked to the PST project and the SKA Release project in
Jira.

Check the current version number

.. code-block:: bash

  cat .release

And then bump either the major, minor or patch release number; i.e.

.. code-block:: bash

  make bump-major-release

or

.. code-block:: bash

  make bump-minor-release

or

.. code-block:: bash

  make bump-patch-release

Look for previous/old/outdated release numbers in other files; e.g.

.. code-block:: bash

  grep -r "0\.3\..*" | grep -v -f .grepignore

Commit changes

.. code-block:: bash

  git commit -m "REL-708 AT3-451 bump ska-pst version to 0.4.0" .

When ready, git push and wait for the gitlab pipeline to finish.

After the pipeline completes successfully, start the merge request.

After the merge request is approved and the release branch is merged
with main, and the pipeline completes successfully

.. code-block:: bash

  git checkout main
  git pull --rebase
  make git-create-tag
  make git-push-tag

This will launch a new pipeline in gitlab, and the **publish** stage in this
special pipeline contains the **oci-image-publish** task. Check that
this task completed successfully by visiting the relevant pipeline at

https://gitlab.com/ska-telescope/pst/ska-pst-pkg/-/pipelines

After the **oci-image-publish** step has finished, ensure that the
release is accepted into the production area of the CAR by searching for
it at

https://harbor.skao.int/

Also double-check that the release is published in gitlab at

https://gitlab.com/ska-telescope/pst/ska-pst-pkg/-/releases

Finally, optionally delete the branch on which the release was prepared; e.g.

.. code-block:: bash

  git branch -d rel-708-release-pst-0-4-0

Notes for each ska-pst project
------------------------------

Release ska-pst-buildtools
~~~~~~~~~~~~~~~~~~~~~~~~~~

Release this package only if there was a change. While it isn’t
production code, we may bump versions of packages used by common, SMRB,
RECV, STAT, and DSP.

Follow the Common Release Instructions.

Release ska-pst-dsp-tools
~~~~~~~~~~~~~~~~~~~~~~~~~

Release this package if there as a change within the package or if ``ska-pst-buildtools``
has been released.

Follow the Common Release Instructions, but also ensure that the

.. code-block:: bash

  DEV_TAG

variable in the top-level Makefile is correct.

Release ska-pst
---------------

Follow the Common Release Instructions, but also ensure that the ``ska-pst-dsp-tools`` release is correct
in the:

- Top Level Makefile: SKA_PST_DSP_TOOLS_IMAGE

- .gitlab-ci.yml: SKA_PST_DSP_TOOLS_IMAGE

Sign in to Read the Docs using Gitlab and visit
https://readthedocs.org/projects/ska-telescope-ska-pst/versions/

Under the **Activate a Version** heading, click the **Activate** button
for the new release, tick the **Active** checkbox and click **Save**.

Visit the REL ticket for the release and edit the **Gitlab URL(s)**,
**CAR URL(s)**, and **User Docs** fields; e.g.

- Gitlab URL(s): ``https://gitlab.com/ska-telescope/pst/ska-pst/-/releases/0.4.0``

- CAR URL(s): ``https://artefact.skao.int/repository/helm-internal/ska-pst-0.4.0.tgz``

- User Docs: ``https://developer.skao.int/projects/ska-pst/en/0.4.0/``

Finally, flag the REL ticket as **Ready for Release**.
