.. _components:

Components
~~~~~~~~~~

The PST is composed of the following subcomponents:

- `COMMON <common/index.html>`_ -- Common elements used by PST components
- `LMC <lmc/index.html>`_ -- Local Monitoring and Control
- `RECV <recv/index.html>`_ -- High-speed data acquisition
- `SMRB <smrb/index.html>`_ -- Shared memory ring buffer manager
- `DSP <dsp/index.html>`_ -- Digital signal processing pipelines
- `STAT <stat/index.html>`_ -- Incoming data quality statistics monitor
- `SEND <send/index.html>`_ -- Output data collation, curation and archival

.. toctree::
  :hidden:

  Common<common/index>
  LMC<lmc/index>
  RECV<recv/index>
  SMRB<smrb/index>
  DSP<dsp/index>
  STAT<stat/index>
  SEND<send/index>
