=========================
SKA PST SEND Applications
=========================

sdp_transfer
--------------------

.. code-block:: none

    usage: sdp_transfer [-h] [--data_product_dashboard DATA_PRODUCT_DASHBOARD] [-v] local_path remote_path ska_subsystem

    positional arguments:
      local_path            local/source filesystem path in which PST data products are found
      remote_path           remote/dest filesystem path to which PST data products should be written
      ska_subsystem         ska-subsystem

    options:
      -h, --help            show this help message and exit
      --data_product_dashboard DATA_PRODUCT_DASHBOARD
                            endpoint for the SDP Data Product Dashboard REST API [e.g. http://127.0.0.1:8888]
      -v, --verbose

