.. _components_send:

SKA PST SEND
==================================

The SKA PST SEND software component implements the interface to the Science Data Processor (SDP) of
the Pulsar Timing (PST) Product of the Square Kilometre Array (SKA) Low and Mid telescopes.

.. toctree::
  :maxdepth: 2

  Applications<apps/index>
