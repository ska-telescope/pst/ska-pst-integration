.. _components_smrb:

SKA PST SMRB
=============

The SKA PST SMRB software component implements the the Shared Memory Ring Buffer (SMRB) of
the Pulsar Timing (PST) Product of the Square Kilometre Array (SKA) Low and Mid telescopes.
Tied-array data from the Correlator Beam Former (CBF) are written to the SMRB by the PST RECV
component and are read from the SMRB by the PST STAT component and the active PST DSP component.

.. toctree::
  :maxdepth: 2

  Architecture<architecture/index>
  Applications<apps/index>
