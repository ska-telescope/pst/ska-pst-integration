
=========================
SKA PST SMRB Applications
=========================

ska-pst builds several utility applications and one primary executable, which are linked to a shared object library.

ska_pst_smrb_core
-----------------

The primary executable for the ska-pst-smrb package. This application provides the C++ implementation for the Shared Memory Ring Buffer (SMRB) component of the Pulsar Timing instrument for the SKA. The primary function of the application is to instantiate an instance of an SMRB, which consists of a data ring buffer and a weights ring buffer.

.. code-block:: none

  Usage: ska_pst_smrb_core [options]
  Create shared memory ring buffers from the configuration file or via control command.
  Quit with CTRL+C.

    -c port     port on which to accept control commands
    -n name     name of the service
    -f config   ascii file containing observation configuration
    -h          print this help text
    -s          single observation, destroy the data blocks once a single observation has been processed
    -t          test mode, destroy the data blocks shortly after creating them
    -v          verbose output

ska_pst_smrb_writer
-------------------

Test application to write data and weights streams to the respective ring buffers created by ska_pst_smrb_core. The options provide support for specifying the data rate, amount of data to be written and whether the writer should simulate a memory copy from a local data buffer. This latter option provides the ability to simulate extremely high data rates.

.. code-block:: none

  Usage: ska_pst_smrb_writer config data_header weights_header
  Writes zero's to the data and weights shared memory ring buffers in accordance with the data/weights headers
  Quit with CTRL+C.

    config           configuration file describing the ring buffers
    data_header      ascii header describing the data time series
    weights_header   ascii header describing the weights time series
    -h               print this help text
    -c               write random data sequence, seeded by UTC_START into ring buffers
    -b               number of blocks of data to write [default 2]
    -r rate          override the natural data rate in MB/s, value of -1 indicates no limit
    -z               do not simulate memory writing into the data blocks
    -v               verbose output

ska_pst_smrb_reader
-------------------

Test application to read data and weights streams from the respective ring buffers created by ska_pst_smrb_core. The options provide support for specifying the block I/O methods (open_block and close_block) in the DataBlockRead class and whether reader should simulate a memory copy to a local data buffer.

.. code-block:: none

  Usage: ska_pst_smrb_reader [options] config
  Reads from shared memory buffers for data and weights streams into local memory buffers.
  Quit with CTRL+C.

    config           configuration file describing the ring buffers
    -b               use block io methods (open_block/close_block)
    -c               check the received data matches the random sequence seeded by the UTC_START
    -h               print this help text
    -z               do not simulate memory reading from the data blocks
    -v               verbose output
