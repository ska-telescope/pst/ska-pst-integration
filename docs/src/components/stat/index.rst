.. _components_stat:

SKA PST STAT
=============

The SKA PST STAT software component computes various statistical quantities that can be used
to evaluate the quality of the data received by the Pulsar Timing (PST) Product of the
Square Kilometre Array (SKA) Low and Mid telescopes. The software includes a Python data
access library (DAL) that can be used to read the HDF5 files produced by STAT during a scan.

.. toctree::
  :maxdepth: 2

  Architecture<architecture/index>
  Applications<apps/index>
