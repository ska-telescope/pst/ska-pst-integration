.. _components_recv:

SKA PST RECV
=============

The SKA PST RECV software component implements the interface to the correlator beam-former (CBF) of
the Pulsar Timing (PST) Product of the Square Kilometre Array (SKA) Low and Mid telescopes.
Tied-array data from the Correlator Beam Former (CBF) are received as a stream of UDP packets
and written to a ring buffer in shared memory (implemented by PST SMRB).

.. toctree::
  :maxdepth: 2

  Architecture<architecture/index>
  Applications<apps/index>
  Interfaces<interfaces/index>
  Monitoring and Logging<monitoring/index>
