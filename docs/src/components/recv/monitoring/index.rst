
RECV Monitoring and Logging
===========================

********
Overview
********

RECV implements monitoring points related to the external interface between PST and the Correlator Beamformer (CBF) as described in the `CBF to PSR Interface Control Document <https://confluence.skatelescope.org/display/SWSI/CBF-PSR+ICD>`_.

*****************
Monitoring Points
*****************

Monitoring data that are computed by RECV are published by the :obj:`PST Beam <ska_pst.lmc.beam.PstBeam>` TANGO device. The attributes are described in detail in `PST TANGO Monitoring Attributes <../../../operation/monitoring.html>`_ and they span metrics covering valid and invalid data received from the CBF. Each of these metrics include a count (bytes or packets) and the current rate of that count in the previous monitoring period (1 second). Monitored attributes have the following definition with respect to CBF-PSR packets

* **Received** An expected packet was received in good order

* **Dropped** An expected packet was not received, the reason for the the loss is not known

* **Ignored** The packet was valid, but the packet precedes the scan start time and so is ignored

* **Malformed** The packet's magic word field did not equal ``0xBEADFEED``

* **Misordered** A packet was substantially out of order, meaning the sequence number field was more than 2*N different to the current PSN. The value of N is configured on a per band process where

    * Low: 16

    * Mid.Band[1|2]: 1024

    * Mid.Band[3|4|5]: 512

* **Misdirected** The packet is valid, but did not match the configuration of PST, violating

    * The CBF/PSR packet Beam Number does not match the :obj:`!timing_beam_id` in the PST Scan Configuration JSON; or

    * The CBF/PSR packet Scan ID does not match the :obj:`!scan_id` provided to PST in the Scan command; or

    * The CBF/PST packet channel number does not match the channels numbers computed by PST after the Configure Scan has been applied. These are computed by PST using the :obj:`!centre_frequency` and :obj:`!total_bandwidth` fields in the PST Scan Configuration JSON, and then reported back to CSP-LMC via the PST :obj:`channelBlockConfiguration <ska_pst.lmc.beam.PstBeam.channelBlockConfiguration>` TANGO attribute.

* **Checksum Failed** The number of packets that did not pass the hardware checksum, indicating a corruption in the transport layer or invalid encoding by the CBF. Note: not currently inspected.

* **Timestamp Sync** The number of packets with a timestamp that is inconsistent with the packet sequence number. Note: not currently inspected.

* **Sequence Number Sync** The packet sequence number is inconsistent with the configured data rate and elapsed time, reported as a Misordered (packet arrives too late) or Dropped (packet arrives too early) packet.


*******
Logging
*******

RECV generates real-time logging during a scan, where the data capture performance is printed, with a 1 second cadence, to standard output in the following format:

::

    RX      DR      Ignore   Misdir   Malform  Misor    Drop     Sleeps
    8.192   0.000   0        0        0        0        0        0
    8.191   0.000   0        0        0        0        0        0
    8.192   0.000   0        0        0        0        0        0

The columns in this output map to attributes reported above:

.. list-table::
  :widths: auto
  :header-rows: 1

  * - Column
    - Description
  * - RX
    - Current data receive rate in Gigabits per second
  * - DR
    - Current data drop rate in Gigabits per second
  * - Ignore
    - Number of ignored packets in the current scan
  * - Misdir
    - Number of misdirected packets in the current scan
  * - Malform
    - Number of malformed packets in the current scan
  * - Drop
    - Number of dropped packets in the current scan
  * - Sleeps
    - Number of busy-wait sleeps in the current scan, not currently used
