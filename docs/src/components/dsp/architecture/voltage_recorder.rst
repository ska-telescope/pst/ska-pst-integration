
**********************
AA0.5 Voltage Recorder
**********************

The AA0.5 release of DSP includes DSP.DISK, which reads tied-array voltage timeseries from 
the `Shared Memory Ring Buffer (SMRB) <../../smrb/index.html>`_ and writes them to a set of files for offline analysis. 
The meta-data that describe the polarisation vectors are read from the header block of the data and weights ring buffers. 
The Data and Weights streams are read from separate ring buffers and written to a sequence of separate files.

-------------
Decomposition
-------------

The DSP component includes a monitoring and control module (DSP.MGMT) that interacts with the MGMT component and controls the other sub-components of the DSP. 
DSP.MGMT controls instances of `DataBlockManager <../api/classska_1_1pst_1_1smrb_1_1DataBlockManager.html>`_, one for each sub-band. 
Each `DataBlockManager <../api/classska_1_1pst_1_1smrb_1_1DataBlockManager.html>`_, contains `DataBlock <../api/classska_1_1pst_1_1smrb_1_1DataBlock.html>`_ 
instances for the Data and Weights streams. 

The DataBlock contains ring buffers for the Header (meta-data) and Data (time-series). 
Each of these ring buffers have a configurable number of elements and element size.

TO DO: add voltage recorder specifics about the stream writers, etc.

----------------------
Data Product Structure
----------------------

See :ref:`voltage_recorder_data_products`.
