.. _components_dsp:

SKA PST DSP
=============

The SKA PST DSP software component implements the main digital signal processing tasks of
the Pulsar Timing (PST) Product of the Square Kilometre Array (SKA) Low and Mid telescopes.

.. toctree::
  :maxdepth: 2

  Architecture<architecture/index>
  Applications<apps/index>
