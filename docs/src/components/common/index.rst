.. _components_common:

SKA PST COMMON
==============

SKA PST COMMON provides common code used by the components of
the Pulsar Timing (PST) Product
of the Square Kilometre Array (SKA) Low and Mid telescopes.
This includes gRPC and Protobuf definitions that are used for integration
between LMC and other PST software components.

.. toctree::
  :maxdepth: 2

  Architecture<architecture/index>
  Applications<apps/index>