.. _components_lmc:

SKA PST LMC - Local Monitoring and Control
==========================================

The SKA PST LMC software component implements the TANGO-based monitoring and control interface to
the Pulsar Timing (PST) Product of the Square Kilometre Array (SKA) Low and Mid telescopes.

.. toctree::
  :maxdepth: 2

  Architecture<architecture/index>

.. toctree:: ::
  :maxdepth: 2

  Health Check<health_check/index>
