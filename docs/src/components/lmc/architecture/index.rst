.. _architecture_tango:

SKA PST LMC Architecture
========================

SKA PST.LMC implements a single TANGO device, BEAM, that is deployed in a single TANGO Device server.

Full PST control and monitoring is accessed via the BEAM device.
Following the SKA TANGO device naming convention, access to the Low PST Beam 1 would use the TANGO FQDN of ``low-pst/beam/01``.

The BEAM device extends from the SKA TANGO base
`CSP Subelement Obs Device <https://developer.skao.int/projects/ska-csp-lmc-base/en/latest/api/obs/obs_device.html>`_
(``ska_csp_lmc_base.CspSubElementObsDevice``)
which in turn extends from the
`Obs Device <https://developer.skao.int/projects/ska-tango-base/en/latest/api/obs/obs_device.html>`_
(``ska_tango_base.obs.ObsDevice``) and
`Base Device <https://developer.skao.int/projects/ska-tango-base/en/latest/api/base/base_device.html>`_
(``ska_tango_base.base.SKABaseDevice``).


BEAM
----

This logical device implements the single point of contact for monitor and control between PST and
external systems, such as CSP.LMC or an engineering interface.
It manages all of the other PST components, except for SEND (in AA0.5, components managed by LMC include SMRB, RECV, DSP.DISK, and STAT).


Component Managers
~~~~~~~~~~~~~~~~~~~~~

Within the BEAM device there are component managers that manage the RECV, SMRB, DSP, and STAT components.

The following UML diagram shows the internal software architecture of the BEAM and component managers.

.. uml:: class_diagram.puml
  :caption: Class diagram showing the BEAM TANGO device and the component managers


SMRB
----

This component manager controls and monitors the SMRB process that manages the ring buffers in shared memory.

RECV
----

This component manager controls and monitors the RECV process that is responsible for capturing UDP packet
streams from the Correlator Beam Former and writing the data to ring buffers in shared memory.

DSP
---

This component manager controls and monitors the appropriate digital signal processing component within
the PST component. During a configure scan request the DSP component manager will decide what specific DSP component
to use. Each component has their own component manager, as seen in the above UML diagram.

Each DSP component reads incoming data from the ring buffer in shared memory and processes it according to the configured
mode of operation.  New modes of operation will be made available with each array assembly:

* AA0.5 voltage recorder (DSP.DISK) writes raw data from the beam former to files on disk
* AA1.0 search mode - including the flow through mode (DSP.FT) which does channel and polarisation selection and requantisation
  the complex voltage data.
* AA2.0 timing mode

STAT
----

This component manager controls and monitors the STAT process that reads incoming data from the ring buffer
in shared memory, computes various statistical quantities that can be used to evaluate data
quality, and publishes these quantities to configured subscribers (currently an HDF5 file writer
and TANGO-based monitor).
