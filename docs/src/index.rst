SKA-PST
=======

This project implements the Pulsar Timing Signal Processor (PST) of the Square Kilometre Array (SKA).
It provides the umbrella helm chart that integrates the CORE components of PST
(SMRB, RECV, DSP, and STAT) with the Local Monitoring and Control (LMC) component
This helm chart also launches the SEND process that autonomously moves PST output data
to persistent storage and updates the Data Product Dashboard.

This project includes a number of system integration tests and provides documentation for integrating
and using PST.

Current Capability
------------------

The current release of PST (ska-pst |version|) supports the following capabilities:

- Data Acquisition -- capture the tied-array voltages from the correlator beam former (CBF)
  and write them to a ring buffer in shared memory
- Data Quality Statistics -- periodically output diagnostic statistical summaries of the
  tied-array voltages in the ring buffer
- Voltage Recorder -- record the tied-array voltages from the CBF to file
- File Transfer -- transfer Voltage Recorder files to a persistent volume shared with the
  Science Data Processor (SDP) and make them available in the Data Product Dashboard
- Flow Through -- use a Graphics Processing Unit (GPU) to decimate tied-array voltages
  from the CBF and write reduced output to local file system;
  currently, these files are not transferred to the SDP

A detailed history of the features added and bugs fixed with each release can be found at `PST Changelog <CHANGELOG.html>`_

.. Architecture ===========================================================

.. toctree::
  :caption: Architecture
  :maxdepth: 1

  architecture/index
  interfaces_dependencies/index
  components/index

.. Deployment =============================================================

.. toctree::
  :caption: Deployment
  :maxdepth: 1

  deployment/resource_requirements
  deployment/development
  deployment/integrated
  deployment/troubleshooting

.. Integration =============================================================

.. toctree::
  :caption: Integration
  :maxdepth: 1

  integration/eda
  integration/sdp
  integration/taranta
  operation/alarm-handler

.. Operation ==============================================================

.. toctree::
  :caption: Operation
  :maxdepth: 1

  operation/jupyter
  Monitoring<operation/monitoring>
  operation/notebooks
  operation/troubleshooting

.. Data Products ==============================================================

.. toctree::
  :caption: Output Data Products
  :maxdepth: 3

  Overview<data_products/index>
  Data Analysis Examples<data_products/analysis/index>
  Simulated Data Generator <data_products/data_generator/index>

.. Configuration ==========================================================
.. toctree::
  :maxdepth: 1
  :caption: Configuration

  configuration/overview
  Helm Chart Parameters<configuration/helm>
  configuration/override

.. API ==============================================================
.. toctree::
  :maxdepth: 1
  :caption: API

  Python API<api/python/index>
  C++ API<api/cpp/index>
  gRPC + Protobuf<api/protobuf>
  Metadata Mapping<api/metadata_mapping.md>
  Static Configuration<api/pst_static_config.md>

.. Development ============================================================

.. toctree::
  :maxdepth: 2
  :caption: Development

  testing/test-harness.rst
  development/release.rst
  Gitlab README<README.md>
  Changelog<CHANGELOG.md>
