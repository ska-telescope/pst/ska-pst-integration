Interfaces and Dependencies
===============================

This page documents the current versions of both the interfaces that PST supports
and the core libraries on which it depends.

Interfaces
~~~~~~~~~~

PST interfaces with the following SKA subsystems:

  * `CSP-LMC <https://developer.skao.int/projects/csp-lmc/en/latest/>`_
  * `Low CBF <https://developer.skao.int/projects/ska-low-cbf/en/latest>`_
  * `Mid CBF <https://developer.skao.int/projects/ska-mid-cbf-mcs/en/latest/>`_
  * `Data Product Dashboard (DPD) <https://developer.skao.int/projects/ska-dataproduct-dashboard/en/latest>`_

CSP-LMC to PST Interface
------------------------

The interface been the CSP-LMC and PST is defined by the Python API of the
`CspSubElementObsDevice <https://developer.skao.int/projects/ska-csp-lmc-base/en/latest/api/obs/obs_device.html#ska_csp_lmc_base.obs.obs_device.CspSubElementObsDevice>`_
and the PST JSON schema, as as defined in the `SKA Telemodel <https://developer.skao.int/projects/ska-telmodel/en/latest/>`_. The
currently supported minimum versions of the PST Configure Schema are as follows:

.. list-table:: PST Configure Scan minimum versions

  * - Processing Mode
    - Minimum Supported Version
  * - Voltage Recorder
    - `PST Configure schema 2.4 <https://developer.skao.int/projects/ska-telmodel/en/latest/schemas/pst/ska-pst-configure-2.4.html>`_
  * - Flow Through
    - `PST Configure schema 2.5 <https://developer.skao.int/projects/ska-telmodel/en/latest/schemas/pst/ska-pst-configure-2.5.html>`_

During the scan configuration process, CSP-LMC must provide the CBFs with the destination information for the PST data stream.  This information is provided by
the PST BEAM TANGO device via the `channelBlockConfiguration <../api/python/_autosummary/lmc/ska_pst.lmc.beam.html#ska_pst.lmc.beam.PstBeam.channelBlockConfiguration>`_ attribute.

Low CBF / Mid CBF to PST Interface
----------------------------------

The interface between the Low CBF and PST is defined in the "SKA1 MID and LOW CSP Correlator and
Beamformer (CBF) to Pulsar Search (PSS) and Pulsar Timing (PST) Interface Control Document
(`SKA-TEL-CSP-0000291 Rev. 02 <https://confluence.skatelescope.org/display/SWSI/CBF-PSR+ICD>`_)"

The following approved Engineering Change Proposals (ECPs) have been implemented by PST

* `ECP-240078 <https://confluence.skatelescope.org/pages/viewpage.action?pageId=267870882>`_ Update Timestamp definitions in the Low/Mid CBF to PSS and PST (PSR) ICD
* `ECP-240079 <https://confluence.skatelescope.org/pages/viewpage.action?pageId=267873385>`_ Update Low/Mid CBF to PSS/PST (PSR) ICD definitions to include validity flags

PST to DPD interface
--------------------

PST sends output files to the DPD by uploading files to a shared volume using the `PST SEND application <../components/send/index.html>`_
and calling the `DPD API <https://developer.skao.int/projects/ska-dataproduct-api/en/latest>`_. The currently supported
API version is ``0.11.x``.

The following endpoints are used by PST:

* ``/ingestnewmetadata`` - when a scan has completed and been sent to the DPD
* ``/dataproductmetadata`` - used to check that the scan has been added to the DPD
* ``/dataproductsearch`` - used in our BDD tests to search for the given scan

Dependencies
~~~~~~~~~~~~

.. list-table:: Core Python Dependencies
  :widths: 25 25 50
  :header-rows: 1

  * - Name
    - Current Version
    - Notes
  * - ``ska-control-model``
    - 1.2.0
    - Transitive from ``ska-csp-lmc-base``
  * - ``ska-csp-lmc-base``
    - 1.0.0
    - Poetry version defined as ``^0.3.0``
  * - ``ska-pydada``
    - 0.1.0
    - Used for handling of PST DADA files
  * - ``ska-ser-logging``
    - 0.4.3
    - Transitive from ``ska-tango-base``
  * - ``ska-tango-base``
    - 1.3.1
    - Transitive from ``ska-csp-lmc-base``
  * - ``ska-telmodel``
    - 1.20.0
    - Poetry version defined as ``^1.14``
  * - ``pytango``
    - 9.5.1
    - Poetry version defined as ``^9.4.2`` and transitive from ``ska-tango-base``
  * - ``protobuf``
    - 5.29.3
    - Poetry version defined as ``*``
  * - ``grpcio``
    - 1.70.0
    - Poetry version defined as ``*``
  * - ``numpy``
    - 1.26.4
    - Poetry version defined as ``*``
  * - ``pandas``
    - 2.2.3
    - Poetry version defined as ``^2``
  * - ``astropy``
    - 6.1.7
    - Poetry version defined as ``*``

