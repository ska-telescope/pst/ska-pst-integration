# PST Static Configuration

As part of the work for [AT3-821](https://jira.skatelescope.org/browse/AT3-821) and
[AT3-816](https://jira.skatelescope.org/browse/AT3-816) the PST schema was rationalised static
configuration codified within the PST code base.

This page documents the YAML file and the fields within it.

Future work will further improve on this and get these values from the SKA Telescope Model.
This will be needed to deal with the change in CBF/PST configuration values, such as the
Mid CBF channelisation will change in the future.  This structure can also be extended to
all for dynamically determining which polyphase filter bank (PFB) coefficients have been
used based on the `cbf_firmware_release` in the PST scan configuration.

## Top level structure

The YAML file, [telescope_config.yaml](https://gitlab.com/ska-telescope/pst/ska-pst/-/blob/main/python/src/ska_pst/common/telescope_config.yaml), has the following structure:

* A list of telescopes
* For each telescope a dictionary of `frequency_bands`.  For SKALow there is only 1 band but for SKAMid bands 1, 2, 3, 4, 5a, and 5b are defined.
* For each frequency band, there is static configuration for the receiver (`receiver_config`) and CBF/PST configuration (`cbf_pst_config`).

An example of the high level structure of the YAML file is as follows:
```yaml
- name: SKALow
  frequency_bands:
    low:
      receiver_config:
        ...
      cbf_pst_config:
        ...
- name: SKAMid
  frequency_bands:
    "1":
      receiver_config:
        ...
      cbf_pst_config:
        ...
```

## CBF/PST Configuration (`cbf_pst_config`)

This section of the YAML file provides configuration that defines how the correlator beam
former (CBF) channelises the data and the UDP packet structure that is sent to PST.

| Name                                           | Notes |
|------------------------------------------------|-------|
| `tsamp`                                        | The sampling interval, in microseconds. |
| `udp_nchan`                                    | The number of PST fine channels in a UDP packet. |
| `udp_nsamp`                                    | The number of time samples in a UDP packet. |
| `wt_nsamp`                                     | The number of samples per weight in a UDP packet. This value should be same as `udp_nsamp` |
| `nbit`                                         | The number of bits used to represent a dimension of a sample value (total bits is number of dimensions * `nbit`) |
| `wt_nbit`                                      | The number of bits per weight. |
| `oversampling_ratio`                           | The oversampling ratio for the frequency band. |
| `max_nchan`                                    | The maximum number of channels for the frequency band. |
| `chan_separation_khz`                          | The frequency separation between the start and end of a channel, in kHz. |
| `rf_freq_minimum_mhz`                          | The bottom end of the frequency band, in MHz. |
| `rf_freq_maximum_mhz`                          | The top end of the frequency band, in MHz. |
| `rf_bw_mhz`                                    | The bandwidth of the band, in MHz. 
| `channel_0_start_freq_mhz`                     | The effective start frequency of channel 0, in MHz. |
| `ring_buffer_config/num_buffers`               | The number of buffers withing a ring buffer. This is internal to PST |
| `ring_buffer_config/packets_per_buffer`        | The number of UDP packets per buffer that a channel needs be recorded for. |
| `channelisation_stages/num_frequency_channels` | The number of channels used in the channelisation stage. |
| `channelisation_stages/oversampling_ratio`     | The oversampling ratio for the channelisation stage. |

## Receiver Feed Configuration (`receiver_config`)

While the receiver feed configuration is not used with in the PST code the following
values are added as metadata to the output files of the PST processing.

| Name                  | Notes |
|-----------------------|-------|
| `receiver_id`         | The name/id of the receiver. This relates to the telescope and frequency band |
| `feed_polarization`   | The native polarisation of the feed for the receiver. |
| `feed_handedness`     | The handedness of the feed of the receiver. |
| `feed_angle`          | The feed angle of the receiver. |
| `feed_tracking_mode`  | The tracking mode for the feed of the receiver. |
| `feed_position_angle` | The requested angle of feed reference. |

## Example

```yaml
- name: SKALow
  frequency_bands:
    low:
      receiver_config:
        receiver_id: LOW
        feed_polarization: LIN
        feed_handedness: 1
        feed_angle: 45.0
        feed_tracking_mode: FA
        feed_position_angle: 0.0

      cbf_pst_config:
        udp_format: LowPST
        tsamp: 207.36
        udp_nchan: 24
        udp_nsamp: 32
        wt_nsamp: 32
        nbit: 16
        wt_nbit: 16
        oversampling_ratio: [4, 3]
        max_nchan: 82944
        chan_separation_khz: 3.616898148
        rf_freq_minimum_mhz: 49.6093750
        rf_freq_maximum_mhz: 349.609375
        rf_bw_mhz: 300.0
        channel_0_start_freq_mhz: 49.609375
        ring_buffer_config:
          num_buffers: 64
          packets_per_buffer: 16
        channelisation_stages:
        - num_frequency_channels: 1024
          oversampling_ratio: [32, 27]
        - num_frequency_channels: 256
          oversampling_ratio: [4, 3]
...
```
