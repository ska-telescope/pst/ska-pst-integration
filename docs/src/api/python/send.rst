.. doctest-skip-all
.. _package-guide:

*********************
SEND Python API
*********************

.. autosummary::
    :toctree: _autosummary/send

    ska_pst.send
    ska_pst.send.dataproduct_file_manager
    ska_pst.send.metadata
