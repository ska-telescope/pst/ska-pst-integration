.. doctest-skip-all
.. _package-guide:

*****************
SKA PST Testutils
*****************

The ska_pst.testutils package is a utility Python
module that is to be used only for testing purposes
(i.e. should only be installed as a dev dependency).

This module is mainly used by the SKA PST Testutils project to
implement behavioural driven development (BDD) tests.
Most of the code in this package had been ported from the
SKA PST's tests directory so that the code could be
used within Python notebooks or shared with the
SKA PST LMC Python project.

.. autosummary::
    :toctree: _autosummary/testutils

    ska_pst.testutils.analysis
    ska_pst.testutils.common
    ska_pst.testutils.dada
    ska_pst.testutils.dsp
    ska_pst.testutils.pulsar
    ska_pst.testutils.scan_config
    ska_pst.testutils.stats
    ska_pst.testutils.tango
    ska_pst.testutils.udp_gen
    ska_pst.testutils.verification
