.. doctest-skip-all
.. _package-guide:

**********************************
Local Monitoring And Control (LMC)
**********************************

.. autosummary::
    :toctree: _autosummary/lmc

    ska_pst.lmc.beam
    ska_pst.lmc.component
    ska_pst.lmc.device_proxy
    ska_pst.lmc.dsp
    ska_pst.lmc.health_check
    ska_pst.lmc.job
    ska_pst.lmc.receive
    ska_pst.lmc.smrb
    ska_pst.lmc.stat
    ska_pst.lmc.test
    ska_pst.lmc.util
    ska_pst.lmc.validation
