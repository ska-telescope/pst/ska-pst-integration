.. doctest-skip-all
.. _stat_data_access_library:

************************
STAT Data Access Library
************************

The data files produced by SKA PST STAT are HDF5 files but as structured in a way
that is not easily used by a person that is unfamiliar with the file format.  The SKA PST
STAT project provides a Python library ``ska_pst.stat`` that can be used in a Jupyter notebook
and exposes the data with easy to use properties.

.. autosummary::
    :toctree: _autosummary/stat

    ska_pst.stat
    ska_pst.stat.hdf5
    ska_pst.stat.utility
