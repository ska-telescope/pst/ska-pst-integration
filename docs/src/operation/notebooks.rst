.. _operation_notebooks:

.. raw:: html

    <style> .red {color:red} </style>

.. role:: red

Available Notebooks
-------------------

The following notebooks provide examples of controlling and monitoring PST,
running a correlator beam-former (CBF) simulator, and plotting the statistical summaries
output by the PST Voltage Recorder.

Single Scan
===========

This notebook implements an end-to-end test of the PST using the CBF simulator to generate UDP packets
and the PST Stat Library to verify that the statistical summaries output by the PST Voltage Recorder
are consistent with the configured input signal.

Double-click on the ``tests`` sub-folder and then double-click on the ``pst-single-scan.ipynb`` notebook.
Run it either cell-by-cell or by clicking `Run All Cells` in the `Run` menu.

Modular Notebook Set
====================

This set of four more modular notebooks demonstrates how different components of the PST signal path can be configured and used independently.

The set consists of 

* cbf-simulator.ipynb - configures and runs the UDP packet generator for a single scan (emulates the CBF)
* perform-scan-external-cbf.ipynb - configures and runs the PST for a single scan (emulates the LMC)
* monitoring-notebook.ipynb - plots the statistical summary files that are output by the PST every five seconds
* dpd-stat-example.ipynb - downloads and plots the latest statistical summary file published at the data product dashboard
