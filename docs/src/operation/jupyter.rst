.. _operation_jupyter:

.. raw:: html

    <style> .red {color:red} </style>

.. role:: red

Using Jupyter
=============

Pipeline Deployment
-------------------

To interact with the PST JupyterLab on the STFC cluster, first launch a
`ska-pst pipeline <https://gitlab.com/ska-telescope/pst/ska-pst/-/pipelines/>`_
and start the `deploy-dev-environment` task.  
When it finishes, visit https://k8s.stfc.skao.int/ci-dev-ska-pst/jupyterlab
and follow the instructions for running one of the :ref:`Available Notebooks <operation_notebooks>`.

Direct Deployment
-----------------

To directly deploy the PST and interact with the PST JupyterLab, install the ``test-parent`` chart as described in
:ref:`Self-Test Deployment <development_deployment>`.

The ``test-parent`` chart deploys a pod that runs `JupyterLab <https://jupyterlab.readthedocs.io/en/latest/>`_
with copies of the notebooks that are found in the ``notebooks/`` sub-folder of the SKA PST source code.
The method of connecting to the PST JupyterLab depends on the environment of the Kubernetes cluster on
which the ``test-parent`` chart is deployed.

Local Deployment
++++++++++++++++

If using `minikube` on Linux or `Docker Desktop` on the Windows Subsystem for Linux (WSL), then 
connect to the deployed PST JupyterLab by navigating to http://localhost:8888/ska-pst/jupyterlab
and follow the instructions for running one of the :ref:`Available Notebooks <operation_notebooks>`.

If this does not work, create a port forward to the PST Jupyterlab using the following command,
then try navigating to the above URL again.

.. code-block:: bash

    make pst-forward-jupyterlab


Low PSI Deployment
++++++++++++++++++

To interact with the PST JupyterLab on the Low PSI requires

1. CSIRO login credentials, and

2. setting up a transparent proxy server that routes all Low PSI subnet traffic from your machine.

The latter can be done using ``sshuttle``, as described in :ref:`setting up a proxy server <proxy_server_setup>`.

After setting up the transparent proxy server, navigate to https://psi-low.atnf.csiro.au/ska-pst/jupyterlab
and follow the instructions for running one of the :ref:`Available Notebooks <operation_notebooks>`.

Clean up when done
++++++++++++++++++

Please remember to uninstall the ``test-parent`` chart when you are finished using the notebook.

.. code-block:: bash

    make k8s-uninstall-chart


.. _proxy_server_setup:

Setting up a transparent proxy server
+++++++++++++++++++++++++++++++++++++

**On Linux**, simply run the following command:

.. code-block:: bash

    sshuttle -r abc123@venice.atnf.csiro.au psi-head.atnf.csiro.au/27

**On WSL**, run the following three commands:

1. In a linux console, type ``hostname -I`` to get the IP address assigned to WSL
   (unfortunately, this address changes each time WSL is launched); for example,

.. code-block:: bash

    hostname -I
    172.22.113.143

2. In a Windows Powershell console (run as Administrator), route all PSI subnet traffic
   to the IP address assigned WSL, as determined in step 1; for example,

.. code-block:: bash

    route add 202.9.15.128 mask 255.255.255.224 172.22.113.143

3. In a linux console, run ``sshuttle`` to route all PSI subnet traffic and accept
   connections from other machines on your network

.. code-block:: bash

    sshuttle -r abc123@venice.atnf.csiro.au 202.9.15.131/27 -l 0.0.0.0
