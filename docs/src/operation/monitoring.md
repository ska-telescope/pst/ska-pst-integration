# TANGO monitoring attributes

The following table lists the TANGO attributes that can be monitored. For more information about the attributes
check the [ska_pst.lmc.beam API page](../api/python/_autosummary/lmc/ska_pst.lmc.beam.html) or click on
each attribute name to be deep-linked into the Python API that provides more information about the attribute.

The attributes except `diskCapacity`, `diskUsedBytes`, `diskUsedPercentage` and `availableDiskSpace` are
only valid during a PST scan.

The RECV subcomponent provides attributes that report counters (e.g. `malformedPackets`) and rates
(e.g. `malformedPacketRate`) for problems detected on the interface between the Correlator Beam Former (CBF)
and PST. If any of the rate values are > 0.0 a warning should be reported, noting this value could
return to 0.0 if the problem is intermittent. In particular, the `dataDropRate` could indicate network
issues between CBF and PST.

Any of the STAT subcomponent variance attributes (e.g. `realPolAVarianceFreqAvg`, `imagPolAVarianceFreqAvg`, etc)
that have a value of 0.0 during a scan means that all data received from the CBF are zero
which indicates a problem with the rescaling applied to data stream in the CBF.
For any of the clipped values (e.g. `imagPolANumClippedSamples`) a positive value could mean
strong RFI is present (valid and expected) or that there are rescaling problems in the CBF.

| Name | Type | Subcomponent | Notes |
|------|------|--------------|-------|
| [dataReceiveRate](../api/python/_autosummary/lmc/ska_pst.lmc.beam.html#ska_pst.lmc.beam.PstBeam.dataReceiveRate) | float | RECV | |
| [dataReceived](../api/python/_autosummary/lmc/ska_pst.lmc.beam.html#ska_pst.lmc.beam.PstBeam.dataReceived) | int | RECV | |
| [dataDropRate](../api/python/_autosummary/lmc/ska_pst.lmc.beam.html#ska_pst.lmc.beam.PstBeam.dataDropRate) | float | RECV | **Warn:** if > 0.0 |
| [dataDropped](../api/python/_autosummary/lmc/ska_pst.lmc.beam.html#ska_pst.lmc.beam.PstBeam.dataDropped) | int | RECV | |
| [misorderedPackets](../api/python/_autosummary/lmc/ska_pst.lmc.beam.html#ska_pst.lmc.beam.PstBeam.misorderedPackets) | int | RECV | |
| [misorderedPacketRate](../api/python/_autosummary/lmc/ska_pst.lmc.beam.html#ska_pst.lmc.beam.PstBeam.misorderedPacketRate) | float | RECV | **Warn:** if > 0.0 |
| [malformedPackets](../api/python/_autosummary/lmc/ska_pst.lmc.beam.html#ska_pst.lmc.beam.PstBeam.malformedPackets) | int | RECV | |
| [malformedPacketRate](../api/python/_autosummary/lmc/ska_pst.lmc.beam.html#ska_pst.lmc.beam.PstBeam.malformedPacketRate) | float | RECV | **Warn:** if > 0.0 |
| [misdirectedPackets](../api/python/_autosummary/lmc/ska_pst.lmc.beam.html#ska_pst.lmc.beam.PstBeam.misdirectedPackets) | int | RECV | |
| [misdirectedPacketRate](../api/python/_autosummary/lmc/ska_pst.lmc.beam.html#ska_pst.lmc.beam.PstBeam.misdirectedPacketRate) | float | RECV | **Warn:** if > 0.0 |
| [checksumFailurePackets](../api/python/_autosummary/lmc/ska_pst.lmc.beam.html#ska_pst.lmc.beam.PstBeam.checksumFailurePackets) | int | RECV | |
| [checksumFailurePacketRate](../api/python/_autosummary/lmc/ska_pst.lmc.beam.html#ska_pst.lmc.beam.PstBeam.checksumFailurePacketRate) | float | RECV | **Warn:** if > 0.0 |
| [timestampSyncErrorPackets](../api/python/_autosummary/lmc/ska_pst.lmc.beam.html#ska_pst.lmc.beam.PstBeam.timestampSyncErrorPackets) | int | RECV | |
| [timestampSyncErrorPacketRate](../api/python/_autosummary/lmc/ska_pst.lmc.beam.html#ska_pst.lmc.beam.PstBeam.timestampSyncErrorPacketRate) | float | RECV | **Warn:** if > 0.0 |
| [seqNumberSyncErrorPackets](../api/python/_autosummary/lmc/ska_pst.lmc.beam.html#ska_pst.lmc.beam.PstBeam.seqNumberSyncErrorPackets) | int | RECV | |
| [seqNumberSyncErrorPacketRate](../api/python/_autosummary/lmc/ska_pst.lmc.beam.html#ska_pst.lmc.beam.PstBeam.seqNumberSyncErrorPacketRate) | float | RECV | **Warn:** if > 0.0 |
| [noValidPolarisationCorrectionPackets](../api/python/_autosummary/lmc/ska_pst.lmc.beam.html#ska_pst.lmc.beam.PstBeam.noValidPolarisationCorrectionPackets) | int | RECV | |
| [noValidPolarisationCorrectionPacketRate](../api/python/_autosummary/lmc/ska_pst.lmc.beam.html#ska_pst.lmc.beam.PstBeam.noValidPolarisationCorrectionPacketRate) | float | RECV | **Warn:** if > 0.0 |
| [noValidStationBeamPackets](../api/python/_autosummary/lmc/ska_pst.lmc.beam.html#ska_pst.lmc.beam.PstBeam.noValidStationBeamPackets) | int | RECV | |
| [noValidStationBeamPacketRate](../api/python/_autosummary/lmc/ska_pst.lmc.beam.html#ska_pst.lmc.beam.PstBeam.noValidStationBeamPacketRate) | float | RECV | **Warn:** if > 0.0|
| [noValidPstBeamPackets](../api/python/_autosummary/lmc/ska_pst.lmc.beam.html#ska_pst.lmc.beam.PstBeam.noValidPstBeamPackets) | int | RECV | |
| [noValidPstBeamPacketRate](../api/python/_autosummary/lmc/ska_pst.lmc.beam.html#ska_pst.lmc.beam.PstBeam.noValidPstBeamPacketRate) | float | RECV | **Warn:** if > 0.0 |
| [dataRecordRate](../api/python/_autosummary/lmc/ska_pst.lmc.beam.html#ska_pst.lmc.beam.PstBeam.dataRecordRate) | float | DSP | |
| [dataRecorded](../api/python/_autosummary/lmc/ska_pst.lmc.beam.html#ska_pst.lmc.beam.PstBeam.dataRecorded) | int | DSP | |
| [diskCapacity](../api/python/_autosummary/lmc/ska_pst.lmc.beam.html#ska_pst.lmc.beam.PstBeam.diskCapacity) | int | DSP | |
| [diskUsedBytes](../api/python/_autosummary/lmc/ska_pst.lmc.beam.html#ska_pst.lmc.beam.PstBeam.diskUsedBytes) | int | DSP | |
| [diskUsedPercentage](../api/python/_autosummary/lmc/ska_pst.lmc.beam.html#ska_pst.lmc.beam.PstBeam.diskUsedPercentage) | int | DSP | |
| [availableDiskSpace](../api/python/_autosummary/lmc/ska_pst.lmc.beam.html#ska_pst.lmc.beam.PstBeam.availableDiskSpace) | float | DSP | |
| [expectedDataRecordRate](../api/python/_autosummary/lmc/ska_pst.lmc.beam.html#ska_pst.lmc.beam.PstBeam.expectedDataRecordRate) | float | DSP | |
| [availableRecordingTime](../api/python/_autosummary/lmc/ska_pst.lmc.beam.html#ska_pst.lmc.beam.PstBeam.availableRecordingTime) | float | DSP | **Warn:** if <= 60.0<br>**Alarm:** if <= 10.0 |
| [ringBufferUtilisation](../api/python/_autosummary/lmc/ska_pst.lmc.beam.html#ska_pst.lmc.beam.PstBeam.ringBufferUtilisation) | float | SMRB | **Warn:** if >= 50.0<br>**Alarm:** if >= 90.0|
| [realPolAMeanFreqAvg](../api/python/_autosummary/lmc/ska_pst.lmc.beam.html#ska_pst.lmc.beam.PstBeam.realPolAMeanFreqAvg) | float | STAT | |
| [realPolAVarianceFreqAvg](../api/python/_autosummary/lmc/ska_pst.lmc.beam.html#ska_pst.lmc.beam.PstBeam.realPolAVarianceFreqAvg) | float | STAT | **Warn:** if == 0.0 |
| [realPolANumClippedSamples](../api/python/_autosummary/lmc/ska_pst.lmc.beam.html#ska_pst.lmc.beam.PstBeam.realPolANumClippedSamples) | int | STAT | **Warn:** if > 0 |
| [imagPolAMeanFreqAvg](../api/python/_autosummary/lmc/ska_pst.lmc.beam.html#ska_pst.lmc.beam.PstBeam.imagPolAMeanFreqAvg) | float | STAT | |
| [imagPolAVarianceFreqAvg](../api/python/_autosummary/lmc/ska_pst.lmc.beam.html#ska_pst.lmc.beam.PstBeam.imagPolAVarianceFreqAvg) | float | STAT | **Warn:** if == 0.0 |
| [imagPolANumClippedSamples](../api/python/_autosummary/lmc/ska_pst.lmc.beam.html#ska_pst.lmc.beam.PstBeam.imagPolANumClippedSamples) | int | STAT | **Warn:** if > 0 |
| [realPolAMeanFreqAvgRfiExcised](../api/python/_autosummary/lmc/ska_pst.lmc.beam.html#ska_pst.lmc.beam.PstBeam.realPolAMeanFreqAvgRfiExcised) | float | STAT | |
| [realPolAVarianceFreqAvgRfiExcised](../api/python/_autosummary/lmc/ska_pst.lmc.beam.html#ska_pst.lmc.beam.PstBeam.realPolAVarianceFreqAvgRfiExcised) | float | STAT | **Warn:** if == 0.0 |
| [realPolANumClippedSamplesRfiExcised](../api/python/_autosummary/lmc/ska_pst.lmc.beam.html#ska_pst.lmc.beam.PstBeam.realPolANumClippedSamplesRfiExcised) | int | STAT | **Warn:** if > 0 |
| [imagPolAMeanFreqAvgRfiExcised](../api/python/_autosummary/lmc/ska_pst.lmc.beam.html#ska_pst.lmc.beam.PstBeam.imagPolAMeanFreqAvgRfiExcised) | float | STAT | |
| [imagPolAVarianceFreqAvgRfiExcised](../api/python/_autosummary/lmc/ska_pst.lmc.beam.html#ska_pst.lmc.beam.PstBeam.imagPolAVarianceFreqAvgRfiExcised) | float | STAT | **Warn:** if == 0.0 |
| [imagPolANumClippedSamplesRfiExcised](../api/python/_autosummary/lmc/ska_pst.lmc.beam.html#ska_pst.lmc.beam.PstBeam.imagPolANumClippedSamplesRfiExcised) | int | STAT | **Warn:** if > 0 |
| [realPolBMeanFreqAvg](../api/python/_autosummary/lmc/ska_pst.lmc.beam.html#ska_pst.lmc.beam.PstBeam.realPolBMeanFreqAvg) | float | STAT | |
| [realPolBVarianceFreqAvg](../api/python/_autosummary/lmc/ska_pst.lmc.beam.html#ska_pst.lmc.beam.PstBeam.realPolBVarianceFreqAvg) | float | STAT | **Warn:** if == 0.0 |
| [realPolBNumClippedSamples](../api/python/_autosummary/lmc/ska_pst.lmc.beam.html#ska_pst.lmc.beam.PstBeam.realPolBNumClippedSamples) | int | STAT | **Warn:** if > 0 |
| [imagPolBMeanFreqAvg](../api/python/_autosummary/lmc/ska_pst.lmc.beam.html#ska_pst.lmc.beam.PstBeam.imagPolBMeanFreqAvg) | float | STAT | |
| [imagPolBVarianceFreqAvg](../api/python/_autosummary/lmc/ska_pst.lmc.beam.html#ska_pst.lmc.beam.PstBeam.imagPolBVarianceFreqAvg) | float | STAT | **Warn:** if == 0.0 |
| [imagPolBNumClippedSamples](../api/python/_autosummary/lmc/ska_pst.lmc.beam.html#ska_pst.lmc.beam.PstBeam.imagPolBNumClippedSamples) | int | STAT | **Warn:** if > 0 |
| [realPolBMeanFreqAvgRfiExcised](../api/python/_autosummary/lmc/ska_pst.lmc.beam.html#ska_pst.lmc.beam.PstBeam.realPolBMeanFreqAvgRfiExcised) | float | STAT | |
| [realPolBVarianceFreqAvgRfiExcised](../api/python/_autosummary/lmc/ska_pst.lmc.beam.html#ska_pst.lmc.beam.PstBeam.realPolBVarianceFreqAvgRfiExcised) | float | STAT | **Warn:** if == 0.0 |
| [realPolBNumClippedSamplesRfiExcised](../api/python/_autosummary/lmc/ska_pst.lmc.beam.html#ska_pst.lmc.beam.PstBeam.realPolBNumClippedSamplesRfiExcised) | int | STAT | **Warn:** if > 0 |
| [imagPolBMeanFreqAvgRfiExcised](../api/python/_autosummary/lmc/ska_pst.lmc.beam.html#ska_pst.lmc.beam.PstBeam.imagPolBMeanFreqAvgRfiExcised) | float | STAT | |
| [imagPolBVarianceFreqAvgRfiExcised](../api/python/_autosummary/lmc/ska_pst.lmc.beam.html#ska_pst.lmc.beam.PstBeam.imagPolBVarianceFreqAvgRfiExcised) | float | STAT | **Warn:** if == 0.0 |
| [imagPolBNumClippedSamplesRfiExcised](../api/python/_autosummary/lmc/ska_pst.lmc.beam.html#ska_pst.lmc.beam.PstBeam.imagPolBNumClippedSamplesRfiExcised) | int | STAT | **Warn:** if > 0 |
