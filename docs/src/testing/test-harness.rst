.. _automated_tests:

Automated Tests
~~~~~~~~~~~~~~~

Deployment
**********

There are 2 charts present in the ``ska-pst`` repository. The  ``ska-pst`` and ``test-parent``.
Each of the charts undergoes a series of deployment tests using `GitLab's CICD pipelines <https://gitlab.com/ska-telescope/pst/ska-pst/-/pipelines>`_

Scan
****

Data Generator
--------------
The main tool for generating data is ``ska_pst_recv_udpgen`` which is an output artefact of the
`ska-pst-recv component <../components/recv/index.html>`_ and is included in the ``ska-pst-test-support`` OCI image.

Simulating a Scan
-----------------
Automated tests that simulated PST scans are executed through the SKA cicd makefile pipeline machinery target ``k8s-test``.
The underlying code involves the usage of pytest and tango to interact with ``ska-pst-lmc`` and ``ska-pst-core``.
The simulation can be executed through the command ``make k8s-test PYTHON_VARS_TEST_SUITE=$TESTSUITE``.
The default test suite is ``integration``.

Integration with Jira
----------------------
The results of ``integration`` test suite executions are reported in Jira via XRay,
where expected PST behaviour is described using Gherkin feature files.
For the current list of integration test scenarios, see one of the recent test executions listed at the
`PST Internal Integration Test Plan <https://jira.skatelescope.org/browse/XTP-19671>`_.

Sample Scan configuration
-------------------------
The base configuration as well as overrides used for the scan simulation tests can be found
`here <https://gitlab.com/ska-telescope/pst/ska-pst/-/blob/main/python/tests/conftest.py>`_.

Performance Tests
*****************
Performance related tests involving data transmission rely on the options available to ``ska_pst_recv_udpgen``.
The available options include the transmission rate (in Gb/s) and data transmission duration (in seconds).
