.. _flow_through_data_products:

Flow Through Data Products 
**************************

In the Flow Through processing mode, data are written to disk in a contiguous sequence of PSRDADA files as described at :ref:`dada_files`.

Flow Through Data Files
^^^^^^^^^^^^^^^^^^^^^^^

In each data file output in Flow Through mode, samples are organized in time, frequency, polarization (or TFP) order, such that time is the mostly slowly changing index and polarization
is the most quickly changing.  If data are encoded with fewer than eight bits per sample, then TFP order persists from the least significant bit (LSB) to the most signfificant bit (MSB).
For example, if NBIT=2, each byte will contain

    LSB Re[p0] Im[p0] Re[p1] Im[p1] MSB

where ``p0`` and ``p1`` are polarisation 0 and 1, and ``Re[z]`` and ``Re[z]`` are the real and imaginary parts of complex-valued ``z``.  Each value is encoded using twos complement.

Flow Through Weights Files
^^^^^^^^^^^^^^^^^^^^^^^^^^

The weights files contain a decimated form of the two vectors that are extracted from the meta-data in the CBF/PSR data stream:

* Block of relative weights: these describe the the per-antenna RFI mitigation that was performed by the beam-former during signal processing.
* Scale factors: these are floating point values by which that quantised data must be rescaled prior to subsequent signal processing.

Only the weights of the selected channels are written to file.
