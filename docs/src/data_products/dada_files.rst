.. _dada_files:

PSRDADA File Data Products
**************************

In PST Voltage Recorder and Flow Through processing modes, output data are written to files in the `PSRDADA <https://psrdada.sourceforge.net>`_ format,
which is compatible with existing Pulsar Timing software such as `DSPSR <https://dspsr.sourceforge.net>`_. 

----------------------
Data Product Structure
----------------------

Data are written to sequences of PSRDADA files divided into ``data/`` and ``weights/`` subdirectories.
The data and weights files are contiguous, each contains approximately 10 seconds of data, and they are named as follows::

    <TIMESTAMP>_<BYTE_OFFSET>.<FILE_NUMBER>.dada

Where

* TIMESTAMP is the UTC timestamp of the first integer second of the observation, written in the format: YYYY-MM-DD-HH:MM:SS.
* BYTE_OFFSET is the byte offset (zero padded to 16 digits) of the first sample in the file from the first byte in observation.
* FILE_NUMBER is the index (zero padded to 6 digits) of the file that has been written in the sequence.

--------------
File Structure
--------------

The PSRDADA file format does not define or assume any specific data structure; 
rather, each file is a bit container with an ASCII header block (typically of 4096 bytes) followed by a data block.
Any data reader must read the key/value pairs from the header block and, if it understands the meta-data, then interpret the data as required. 

^^^^^^^^^^^^
Header Block
^^^^^^^^^^^^

The header block is simply defined as a list of key/value stored in ASCII characters with each pair delimited by newline characters 
and separated by one or more whitespace characters. The ASCII header is padded with the null character from the final valid header character 
to the final byte in the header block.

^^^^^^^^^^^^^^^^^^^
Header Block Fields
^^^^^^^^^^^^^^^^^^^

================  ==========================================  ============  ===================
Key               Description                                 Units         Examples
================  ==========================================  ============  ===================
HDR_SIZE          Size of the header                          bytes         4096
HDR_VERSION       Version of the header                                     1.0
TELESCOPE         Name of the telescope observatory                         SKALow,SKAMid
RECEIVER          Name of the receiver used                                 LFAA,SPFRX
INSTRUMENT        TBD                                                       LowCBF
NBIT              Number of bits per real or imag sample                    8,16
NANT              Number of antenna present                                 1
NPOL              Number of polarisations present                           2
NDIM              Number of dimensions, 1=Real 2=Complex                    1,2
TSAMP             Sampling interval in                        microseconds  207.36
BAND              Name of the observing band                                Low,Band1,Band2
WT_NSAMP          Number of samples per relative weight                     32
UDP_NSAMP         Number of samples per UDP packet                          32
UDP_NCHAN         Number of channels per UDP packet                         24
UDP_FORMAT        Name of the UDP format                                    LowPST,MidBand1,...,MidBand5
OS_FACTOR         Over-sampling ratio of data                               4/3,8/7
NCHAN             Number of channels                                        432
FREQ              Centre frequency all channels               MHz           51.19357639
BW                Bandwidth of all channels                   MHz           1.562499999936
START_CHANNEL     First absolute channel number                             0
END_CHANNEL       Final absolute channel number                             432
RESOLUTION        Minimum coherent block of data              bytes         110592
BYTES_PER_SECOND  Data rate                                   bytes/sec     16666666
DATA_HOST         Network address at which data is recorded   IPv4          127.0.0.1
DATA_PORT         Network UDP port at which data is recorded                9510
LOCAL_HOST        Local address at which data is recorded     IPv4          127.0.0.1
CALFREQ           Frequency of pulsed noise diode - unused    Hz            11.1111111111
OBS_OFFSET        Offset of UTC_START of first sample         bytes         0
SOURCE            Name of astronomical source                               J0437-4715
DATA_KEY *        TBD                                                       a000
WEIGHTS_KEY *     TBD                                                       a010
NUMA_NODE         TBD                                                       0
HB_NBUFS *        TBD                                                       8
HB_BUFSZ *        TBD                                         bytes         4096
DB_NBUFS *        TBD                                                       8
DB_BUFSZ *        TBD                                         bytes         11059200
WB_NBUFS *        TBD                                                       8
WB_BUFSZ *        TBD                                         bytes         93600
SCAN_ID           Unique scan identifier provided by TM                     123234325345
BEAM_ID           Beam number                                               1-16
DATA_GENERATOR    Name of test vector that is present                       Sine,Random
SCANLEN_MAX       Length of the entire scan                   seconds       10
SINUSOID_FREQ     Frequency of Sine test vector, if present   MHz           51.3
UTC_START         Integer second timestamp of first sample    UTC           2023-03-15-03:41:29
PICOSECONDS       Offset from UTC_START of first sample       picoseconds   0
FILE_NUMBER       File number in the file sequence                          0
================  ==========================================  ============  ===================
