.. _detected_filterbank_data_products:

Detected Filterbank Data Products
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

In Detected Filterbank processing mode, channelized timeseries of polarized flux are periodically 
written to disk in the "search-mode" `PSRFITS <https://www.atnf.csiro.au/research/pulsar/psrfits_definition/Psrfits.html>`_ file format.
