.. _stat_data_products:

Diagnostic Statistics Data Products
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

The STAT sub-component produces diagnostic statistics that can be used to evaluate the quality of the voltage data
received from the beam former; these are periodically written to disk using the `High-performance Data Format 5 (HDF5) <https://www.hdfgroup.org/solutions/hdf5/>`_.
There is a Python library that can be used to explore the contents of the files output by STAT, and an application that can be used to plot them.

- :ref:`stat_data_access_library` -- a Python library that can be used to explore the contents of the HDF5 files output by PST STAT
- :ref:`plot_stat_file` -- a Python script that uses the STAT Data Access Library to plot PST STAT output data
