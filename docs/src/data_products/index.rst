.. _data_products:

Data Products Overview
~~~~~~~~~~~~~~~~~~~~~~

This page describes the data products output by the PST and provides examples of how to 
query metadata, perform further offline processing, and visualize either the data or 
the results derived from them.  Output data products are described for each of the following
processing modes.

1. Voltage Recorder (Array Assembly 0.5);
2. Flow Through (Array Assembly 1.0);
3. Detected Filterbank (Array Assembly 2.0);
4. Pulsar Timing (Array Assembly 3.0);
5. Diagnostic statistics (all modes)


.. Diagnostic Statistics ===========================================================

.. toctree::
  :caption: Diagnostic Statistics (All Modes)
  :maxdepth: 3

  Diagnostic Statistics <stat/index>

.. Voltage Recorder ===========================================================

.. toctree::
  :caption: Voltage Recorder (Array Assembly 0.5)
  :maxdepth: 3

  Voltage Recorder Data Products <voltage_recorder/index>

.. Flow Through ===========================================================

.. toctree::
  :caption: Flow Through (Array Assembly 1.0)
  :maxdepth: 3

  Flow Through Data Products <flow_through/index>

.. Detected Filterbank ===========================================================

.. toctree::
  :caption: Detected Filterbank (Array Assembly 2.0)
  :maxdepth: 3

  Detected Filterbank Data Products <detected_filterbank/index>

.. Pulsar Timing ===========================================================

.. toctree::
  :caption: Pulsar Timing (Array Assembly 3.0)
  :maxdepth: 3

  Pulsar Timing Data Products <pulsar_timing/index>

