.. _pulsar_timing_data_products:

Pulsar Timing Data Products
~~~~~~~~~~~~~~~~~~~~~~~~~~~

In Pulsar Timing processing mode, pulsar longitude-resolved averages of polarized flux (known as pulse profiles) are periodically 
written to disk in the "fold-mode" `PSRFITS <https://www.atnf.csiro.au/research/pulsar/psrfits_definition/Psrfits.html>`_ file format.
