.. _voltage_recorder_data_products:

Voltage Recorder Data Products
******************************

In the Voltage Recorder processing mode, data are written to disk in a contiguous sequence of PSRDADA files as described at :ref:`dada_files`.
In each file, voltage data are organised as contiguous heaps of packets as received from the CBF.

Voltage Recorder Data files
^^^^^^^^^^^^^^^^^^^^^^^^^^^

The data packing format used by the PST Voltage Recorder is defined by the UDP packet sequencing and structure in the CBF to PST data interface.

Common::

    npol = 2
    ndim = 2

Low CBF / PST::

    nchan_per_packet = 24
    nsamp_per_packet = 32
    nbit = 16

Mid CBF / PST::

    nchan_per_packet = 185
    nsamp_per_packet = 4
    nbit = 8 or 16

Interpretation Algorithm::

    # pointer to data block
    char * input

    nheap = data_bytes / (nchan * npol * ndim * nbit / 8)
    packets_per_heap = nchan / nchan_per_heap

    for heap in range(nheap):
        for packet in range(packets_per_heap):
            for ipol in range(npol):
                for ichan in range(nchan_per_packet):
                    channel = packet * nchan_per_packet + ichan
                    for isamp in range(nsamp_per_packet):
                        sample = heap * nsamp_per_packet + isamp
                        for idim in range(ndim):
                            value = float(input[idx]) / scale_factor
                            idx++

When unpacking the 8- or 16-bit data samples, the Weights Block is used to retrieve the scale_factor that must be used to properly denormalise the quantised samples in the Data Block.

Voltage Recorder Weights Files
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

The weights files contain two vectors that are extracted from the meta-data in the CBF/PSR data stream:

* Block of relative weights: these describe the the per-antenna RFI mitigation that was performed by the beam-former during signal processing.
* Scale factors: these are floating point values by which that quantised data must be rescaled prior to subsequent signal processing.

For the purposes of faithfully denormalising the samples in the data block, only the per-packet scale factor is required. 
For more information on the structure of the relative weights, refer to the CBF to PSR Interface Control Document. 
The summary is that each UDP packet consists of a 16-bit relative weight for each channel in the packet and a single 32-bit floating point scale factor. 
These are stored sequentially in the Weights Block for each packet.

Common::

    weights_nbit = 16
    packet_weights_size = nchan_per_packet * weights_nbit / 8
    packet_scales_size = 32 / 8
    combined_size = packet_weights_size + packet_scales_size

Iteration Algorithm::

    # pointer to weights data blockweights data blockweights data blockweights data block
    char * weights_ptr

    for heap in range(nheap):
        for packet in range(packets_per_heap):
            scales = weights_ptr
            weights = weights_ptr + packet_scales_size
            weights_ptr += combined_size
