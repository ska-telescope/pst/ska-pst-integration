# Find the PCAP library
#
# The following variables are optionally searched for defaults
#  PCAP_ROOT_DIR: Base directory where all libpcap components are found

# The following are set after configuration is done:
#  PCAP_FOUND
#  PCAP_INCLUDE_DIR
#  PCAP_CFLAGS
#  PCAP_LIBRARIES
#  PCAP_LDFLAGS

find_path(PCAP_INCLUDE_DIR
        NAMES pcap.h
        HINTS
        ${PCAP_ROOT_DIR}/include)

find_library(PCAP_LIBRARIES
        NAMES pcap
        HINTS
        ${PCAP_ROOT_DIR}/lib)

include(FindPackageHandleStandardArgs)
find_package_handle_standard_args(PCAP DEFAULT_MSG PCAP_INCLUDE_DIR PCAP_LIBRARIES)

if(PCAP_FOUND)

  get_filename_component(_PCAP_LIB_DIR ${PCAP_LIBRARIES} DIRECTORY)
  get_filename_component(_PCAP_LIB_NAME ${PCAP_LIBRARIES} NAME)

  string(REPLACE "lib" "" _PCAP_LIB_NAME ${_PCAP_LIB_NAME})
  string(REPLACE ".so" "" _PCAP_LIB_NAME ${_PCAP_LIB_NAME})
  string(REPLACE ".a"  "" _PCAP_LIB_NAME ${_PCAP_LIB_NAME})

  set(PCAP_LDFLAGS "-L${_PCAP_LIB_DIR} -l${_PCAP_LIB_NAME}")
  set(PCAP_CFLAGS "-I${PCAP_INCLUDE_DIR}")

  set(HAVE_PCAP on)
else()
  set(PCAP_LDFLAGS "")
  set(PCAP_CFLAGS "")
endif()

mark_as_advanced(PCAP_INCLUDE_DIR PCAP_CFLAGS PCAP_LIBRARIES PCAP_LDFLAGS)
