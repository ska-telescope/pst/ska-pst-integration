# Find the SKA-PST-LMC Protobuf Bindings and library
#

# The following are set after configuration is done:
#  SKAPSTCOMMON_FOUND
#  SKAPSTCOMMON_INCLUDE_DIR
#  SKAPSTCOMMON_LIBRARIES

find_path(SKAPSTCOMMON_INCLUDE_DIR NAMES ska/pst/common/config.h)

find_library(SKAPSTCOMMON_LIBRARIES NAMES ska_pst_common)

include(FindPackageHandleStandardArgs)
find_package_handle_standard_args(SkaPstCommon DEFAULT_MSG SKAPSTCOMMON_INCLUDE_DIR SKAPSTCOMMON_LIBRARIES)