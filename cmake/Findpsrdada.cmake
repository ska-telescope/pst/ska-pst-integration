# Find the PSRDADA library
#
# The following variables are optionally searched for defaults
#  PSRDADA_ROOT_DIR: Base directory where all psrdada components are found
#  PSRDADA_INCLUDE_DIR: Directory where psrdada headers are found
#  PSRDADA_LIB_DIR: Directory where psrdada libraries are found

# The following are set after configuration is done:
#  PSRDADA_FOUND
#  PSRDADA_INCLUDE_DIR
#  PSRDADA_LIBRARIES

find_package(PkgConfig QUIET)
PKG_CHECK_MODULES(PC_PSRDADA QUIET psrdada)

find_path(PSRDADA_INCLUDE_DIR
  NAMES ipcbuf.h
  HINTS
  ${PC_PSRDADA_INCLUDEDIR}
  ${PC_PSRDADA_INCLUDE_DIRS}
  ${PSRDADA_INCLUDE_DIR}
  $ENV{PSRDADA_ROOT_DIR}/include
  ${PSRDADA_ROOT_DIR}/include)

find_library(PSRDADA_LIBRARIES
  NAMES psrdada
  HINTS
  ${PC_PSRDADA_LIBDIR}
  ${PC_PSRDADA_LIBRARY_DIRS}
  ${PSRDADA_LIB_DIR}
  $ENV{PSRDADA_ROOT_DIR}/lib
  ${PSRDADA_ROOT_DIR}/lib)

include(FindPackageHandleStandardArgs)
find_package_handle_standard_args(psrdada DEFAULT_MSG PSRDADA_INCLUDE_DIR PSRDADA_LIBRARIES)
mark_as_advanced(PSRDADA_INCLUDE_DIR PSRDADA_LIBRARIES)
