# Find the UUID library
#
# The following variables are optionally searched for defaults
#  UUID_ROOT_DIR: Base directory where all uuid components are found
#  UUID_INCLUDE_DIR: Directory where uuid headers are found
#  UUID_LIB_DIR: Directory where uuid libraries are found

# The following are set after configuration is done:
#  UUID_FOUND
#  UUID_INCLUDE_DIR
#  UUID_LIBRARIES

find_package(PkgConfig QUIET)
PKG_CHECK_MODULES(PC_UUID REQUIRED QUIET uuid)

find_path(UUID_INCLUDE_DIR
  NAMES uuid.h
  HINTS
  ${PC_UUID_INCLUDEDIR}
  ${PC_UUID_INCLUDE_DIRS}
  ${UUID_INCLUDE_DIR}
  $ENV{UUID_ROOT_DIR}/include
  ${UUID_ROOT_DIR}/include)

find_library(UUID_LIBRARIES
  NAMES uuid
  HINTS
  ${PC_UUID_LIBDIR}
  ${PC_UUID_LIBRARY_DIRS}
  ${UUID_LIB_DIR}
  $ENV{UUID_ROOT_DIR}/lib
  ${UUID_ROOT_DIR}/lib)

include(FindPackageHandleStandardArgs)
find_package_handle_standard_args(UUID DEFAULT_MSG UUID_INCLUDE_DIR UUID_LIBRARIES)
mark_as_advanced(UUID_INCLUDE_DIR UUID_LIBRARIES)
