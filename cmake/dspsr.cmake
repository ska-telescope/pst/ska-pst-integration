
set(dspsr_compile_options
  ${PSRCHIVE_CFLAGS}
  ${DSPSR_CFLAGS}
)

set(dspsr_link_options
  ${DSPSR_LDFLAGS}
  ${PSRCHIVE_LDFLAGS}
)

set(dspsr_link_libraries
  dspsr dspdsp dspbase dspsrmore dspstats
  CUDA::cudart cufft
  psrutil
)

