{
 "cells": [
  {
   "attachments": {},
   "cell_type": "markdown",
   "id": "c13a5846-1721-4630-a7f9-89febebcee53",
   "metadata": {},
   "source": [
    "# Alarm Handler\n",
    "\n",
    "This notebook is designed to be used to demonstrate that SKA PST has warning and alarms added to the PST.BEAM TANGO device.\n",
    "\n",
    "This notebook should be used in conjunction with an Elettra Alarm Handler deployment, the CSP.LMC or a simulator/notebook like [perform-scan-external-cbf.ipynb](perform-scan-external-cbf.ipynb), a correlator beam former (CBF) or the [cbf-simulator.py](cbf-simulator.py) notebook.\n",
    "\n",
    "## Deploy the latest version of ska-pst\n",
    "\n",
    "Before running this notebook, launch the `test-parent` helm chart by running the following commands in a terminal with access to the same Kubernetes cluster as this notebook (e.g. `psi-head` in the Low PSI)\n",
    "\n",
    "```\n",
    "git clone --recursive git@gitlab.com:ska-telescope/pst/ska-pst.git\n",
    "cd ska-pst\n",
    "make k8s-install-chart KUBE_NAMESPACE=pst K8S_CHART=test-parent\n",
    "make k8s-wait KUBE_NAMESPACE=pst KUBE_APP=ska-pst\n",
    "```\n",
    "\n",
    "When finished running this demo, please remember to \n",
    "\n",
    "```\n",
    "make k8s-uninstall-chart KUBE_NAMESPACE=pst K8S_CHART=ska-pst\n",
    "```\n"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "id": "ebca7104",
   "metadata": {},
   "source": [
    "## Setup imports for notebook"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "id": "9410847a",
   "metadata": {},
   "outputs": [],
   "source": [
    "import enum\n",
    "import logging\n",
    "import os\n",
    "import sys\n",
    "\n",
    "import tango\n",
    "from tango import DeviceProxy"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "id": "c989a4ad",
   "metadata": {},
   "source": [
    "## Set up logging\n",
    "\n",
    "This will ensure any of the utility classes will log to cell outputs. IPython defaults to logging to `stderr` but the cells need to\n",
    "`stdout`.  If we didn't do this we would need to put print statements in the utility classes which is not a good development practice."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "id": "95541b94",
   "metadata": {},
   "outputs": [],
   "source": [
    "# override format here for more or less logging information\n",
    "# also update the logging level for different level of logging verbosity\n",
    "logging.basicConfig(\n",
    "    format=\"%(asctime)s | %(levelname)s : %(message)s\",\n",
    "    level=logging.INFO,\n",
    "    stream=sys.stdout,\n",
    ")\n",
    "\n",
    "logger = logging.getLogger()"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "id": "a2a34b4b-d8fc-47a1-9a93-e092546aac9f",
   "metadata": {},
   "source": [
    "## Set the TANGO_HOST environment variable\n",
    "\n",
    "If the `TANGO_HOST` environment variable is already set to something other than the default, then the following code assumes that it has been set correctly (e.g. in the environment variables of the image running `notebook-test`) and the value is not modified.\n",
    "\n",
    "Otherwise, the following code sets `TANGO_HOST` to the Tango database server in the `pst` namespace.\n",
    "\n",
    "If a different namespace was used to deploy the `test-parent` chart, then set the `kube_namespace` variable accordingly.\n",
    "\n",
    "### Using notebook agasint a k8s cluster\n",
    "\n",
    "If using this Notebook against a k8s cluster, like minikube, that you have admin access\n",
    "\n",
    "```\n",
    "$ kubectl get -n <namespace>  svc\n",
    "```\n",
    "\n",
    "This should output something like, find the `EXTERNAL-IP` for the `databaseds-tango-base-test` service.\n",
    "\n",
    "```console\n",
    "NAME                         TYPE           CLUSTER-IP       EXTERNAL-IP     PORT(S)          AGE\n",
    "databaseds-tango-base-test   LoadBalancer   10.109.225.112   192.168.49.97   10000:30553/TCP  50m\n",
    "```\n",
    "\n",
    "Ensure that you can reach the external IP and port.  On a Linux environment you should be able to do:\n",
    "\n",
    "```\n",
    "nc -v <external-ip> 10000\n",
    "```\n",
    "\n",
    "### Using notebook in Docker Desktop on Windows\n",
    "If this notebook is run in a container using Docker Desktop on Windows, then uncomment the line:\n",
    "\n",
    "```python\n",
    "os.environ[\"TANGO_HOST\"] = \"host.docker.internal:10000\"\n",
    "```"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "id": "01e1a96b-43e8-428d-a286-116571221539",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "2024-10-07 22:01:59,567 | INFO : TANGO_HOST=databaseds-tango-base-test:10000\n"
     ]
    }
   ],
   "source": [
    "default_tango_host = \"tango-databaseds.staging:10000\"\n",
    "tango_host = os.environ.get(\"TANGO_HOST\", default_tango_host)\n",
    "kube_namespace = \"test-parent\"\n",
    "\n",
    "if tango_host in [default_tango_host, \"\"]:\n",
    "    tango_host = f\"databaseds-tango-base-test.{kube_namespace}:10000\"\n",
    "    os.environ[\"TANGO_HOST\"] = tango_host\n",
    "\n",
    "# If running the ska-pst-jupyterlab pod within the same k8s namespace then uncomment this line\n",
    "os.environ[\"TANGO_HOST\"] = \"databaseds-tango-base-test:10000\"\n",
    "\n",
    "# If using k8s and got the \"EXTERNAL-IP\" of databaseds-tango-base-test, uncomment this and set the IP\n",
    "# os.environ[\"TANGO_HOST\"] = \"192.168.49.98:10000\"\n",
    "\n",
    "# uncomment this line if the notebook is running in a container hosted by Docker Desktop\n",
    "# os.environ[\"TANGO_HOST\"] = \"host.docker.internal:10000\"\n",
    "\n",
    "logger.info(f\"TANGO_HOST={os.environ['TANGO_HOST']}\")"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "161b6383-27e8-4504-bc84-58e36ba834f0",
   "metadata": {},
   "source": [
    "Get a TANGO Device Proxyto the current PST Beam device."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 4,
   "id": "2ea19345-7086-474e-b433-99b80ad24601",
   "metadata": {},
   "outputs": [],
   "source": [
    "beam = DeviceProxy(\"low-pst/beam/01\")"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "6e53b7be-af26-4cab-8969-9baecac46398",
   "metadata": {},
   "source": [
    "Get a TANGO Device Proxyto the Eletrra Alarm handler."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 5,
   "id": "13661519-8012-44e5-bd8c-effcad1b8c39",
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "()"
      ]
     },
     "execution_count": 5,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "ah = DeviceProxy(\"alarm/handler/01\")\n",
    "ah.eventSummary"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "e13496d1-4aa8-4f59-8cfa-207f42a4fedf",
   "metadata": {},
   "source": [
    "## Set up alarm handler rules.\n",
    "\n",
    "This bit of code will check the PST BEAM TANGO device attributes to find any that have any warning or alarm level that have been set.  It will then use that information to generate a list of alarm rules to be applied to the Elettra Alarm Handler"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 6,
   "id": "74cd17b7-fed8-4cb4-9c59-c43c7aae3697",
   "metadata": {},
   "outputs": [],
   "source": [
    "class _AlarmLevel(enum.IntEnum):\n",
    "    MIN_ALARM = 0\n",
    "    MIN_WARNING = 1\n",
    "    MAX_WARNING = 2\n",
    "    MAX_ALARM = 3\n",
    "\n",
    "\n",
    "def _create_rule(attr: tango.AttributeInfoEx, alarm_level: _AlarmLevel) -> None:\n",
    "    attr_alarms = attr.alarms\n",
    "    attr_fqdn = f\"{beam.name()}/{attr.name}\".lower()\n",
    "    alarm_attr_str = attr_fqdn.replace(\"/\", \"_\").replace(\"-\", \"\")\n",
    "\n",
    "    if alarm_level == _AlarmLevel.MIN_ALARM:\n",
    "        alarm_value = attr_alarms.min_alarm\n",
    "        prefix = \"alarm\"\n",
    "        suffix = \"min\"\n",
    "        rule = f\"{attr_fqdn} <= {alarm_value} || {attr_fqdn}.quality == ATTR_ALARM\"\n",
    "        message = f\"{attr.name} is too low\"\n",
    "    elif alarm_level == _AlarmLevel.MIN_WARNING:\n",
    "        alarm_value = attr_alarms.min_warning\n",
    "        prefix = \"warning\"\n",
    "        suffix = \"min\"\n",
    "        rule = f\"{attr_fqdn} <= {alarm_value} || {attr_fqdn}.quality == ATTR_WARNING\"\n",
    "        message = f\"{attr.name} is getting too low\"\n",
    "    elif alarm_level == _AlarmLevel.MAX_WARNING:\n",
    "        alarm_value = attr_alarms.max_warning\n",
    "        prefix = \"warning\"\n",
    "        suffix = \"max\"\n",
    "        rule = f\"{attr_fqdn} >= {alarm_value} || {attr_fqdn}.quality == ATTR_WARNING\"\n",
    "        message = f\"{attr.name} is getting too high\"\n",
    "    else:\n",
    "        assert alarm_level == _AlarmLevel.MAX_ALARM\n",
    "        alarm_value = attr_alarms.max_alarm\n",
    "        prefix = \"alarm\"\n",
    "        suffix = \"max\"\n",
    "        rule = f\"{attr_fqdn} >= {alarm_value} || {attr_fqdn}.quality == ATTR_WARNING\"\n",
    "        message = f\"{attr.name} is getting too high\"\n",
    "\n",
    "    if alarm_value == \"Not specified\":\n",
    "        return\n",
    "\n",
    "    rule_str = f\"tag={prefix}_{alarm_attr_str}_{suffix}; formula=({rule}); priority=log; message={message}\"\n",
    "\n",
    "    try:\n",
    "        ah.Load(rule_str)\n",
    "    except Exception:\n",
    "        ah.Modify(rule_str)\n",
    "\n",
    "\n",
    "for attr in beam.attribute_list_query_ex():\n",
    "    for alarm_level in _AlarmLevel:\n",
    "        _create_rule(attr, alarm_level)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "78644b4a-b793-4551-8cb0-d70127a21e32",
   "metadata": {},
   "source": [
    "Check current event summary on the Alarm Handler"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 7,
   "id": "d56aa52a-2290-45df-8178-255ef95a239b",
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "('event=low-pst/beam/01/availablerecordingtime;time=2024-10-07 22:02:02;values=[];exception={\"Reason\":\"NOT_connected\",\"Desc\":\"Attribute not subscribed\",\"Origin\":\"...\"};quality=ATTR_INVALID;',\n",
       " 'event=low-pst/beam/01/datadroprate;time=2024-10-07 22:02:02;values=[];exception={\"Reason\":\"NOT_connected\",\"Desc\":\"Attribute not subscribed\",\"Origin\":\"...\"};quality=ATTR_INVALID;',\n",
       " 'event=low-pst/beam/01/misorderedpacketrate;time=2024-10-07 22:02:02;values=[];exception={\"Reason\":\"NOT_connected\",\"Desc\":\"Attribute not subscribed\",\"Origin\":\"...\"};quality=ATTR_INVALID;',\n",
       " 'event=low-pst/beam/01/malformedpacketrate;time=2024-10-07 22:02:02;values=[];exception={\"Reason\":\"NOT_connected\",\"Desc\":\"Attribute not subscribed\",\"Origin\":\"...\"};quality=ATTR_INVALID;',\n",
       " 'event=low-pst/beam/01/misdirectedpacketrate;time=2024-10-07 22:02:02;values=[];exception={\"Reason\":\"NOT_connected\",\"Desc\":\"Attribute not subscribed\",\"Origin\":\"...\"};quality=ATTR_INVALID;',\n",
       " 'event=low-pst/beam/01/checksumfailurepacketrate;time=2024-10-07 22:02:02;values=[];exception={\"Reason\":\"NOT_connected\",\"Desc\":\"Attribute not subscribed\",\"Origin\":\"...\"};quality=ATTR_INVALID;',\n",
       " 'event=low-pst/beam/01/timestampsyncerrorpacketrate;time=2024-10-07 22:02:02;values=[];exception={\"Reason\":\"NOT_connected\",\"Desc\":\"Attribute not subscribed\",\"Origin\":\"...\"};quality=ATTR_INVALID;',\n",
       " 'event=low-pst/beam/01/seqnumbersyncerrorpacketrate;time=2024-10-07 22:02:02;values=[];exception={\"Reason\":\"NOT_connected\",\"Desc\":\"Attribute not subscribed\",\"Origin\":\"...\"};quality=ATTR_INVALID;',\n",
       " 'event=low-pst/beam/01/novalidpolarisationcorrectionpacketrate;time=2024-10-07 22:02:02;values=[];exception={\"Reason\":\"NOT_connected\",\"Desc\":\"Attribute not subscribed\",\"Origin\":\"...\"};quality=ATTR_INVALID;',\n",
       " 'event=low-pst/beam/01/novalidstationbeampacketrate;time=2024-10-07 22:02:02;values=[];exception={\"Reason\":\"NOT_connected\",\"Desc\":\"Attribute not subscribed\",\"Origin\":\"...\"};quality=ATTR_INVALID;',\n",
       " 'event=low-pst/beam/01/novalidpstbeampacketrate;time=2024-10-07 22:02:02;values=[];exception={\"Reason\":\"NOT_connected\",\"Desc\":\"Attribute not subscribed\",\"Origin\":\"...\"};quality=ATTR_INVALID;',\n",
       " 'event=low-pst/beam/01/ringbufferutilisation;time=2024-10-07 22:02:02;values=[];exception={\"Reason\":\"NOT_connected\",\"Desc\":\"Attribute not subscribed\",\"Origin\":\"...\"};quality=ATTR_INVALID;',\n",
       " 'event=low-pst/beam/01/realpolavariancefreqavg;time=2024-10-07 22:02:02;values=[];exception={\"Reason\":\"NOT_connected\",\"Desc\":\"Attribute not subscribed\",\"Origin\":\"...\"};quality=ATTR_INVALID;',\n",
       " 'event=low-pst/beam/01/realpolanumclippedsamples;time=2024-10-07 22:02:02;values=[];exception={\"Reason\":\"NOT_connected\",\"Desc\":\"Attribute not subscribed\",\"Origin\":\"...\"};quality=ATTR_INVALID;',\n",
       " 'event=low-pst/beam/01/imagpolavariancefreqavg;time=2024-10-07 22:02:02;values=[];exception={\"Reason\":\"NOT_connected\",\"Desc\":\"Attribute not subscribed\",\"Origin\":\"...\"};quality=ATTR_INVALID;',\n",
       " 'event=low-pst/beam/01/imagpolanumclippedsamples;time=2024-10-07 22:02:02;values=[];exception={\"Reason\":\"NOT_connected\",\"Desc\":\"Attribute not subscribed\",\"Origin\":\"...\"};quality=ATTR_INVALID;',\n",
       " 'event=low-pst/beam/01/realpolavariancefreqavgrfiexcised;time=2024-10-07 22:02:02;values=[];exception={\"Reason\":\"NOT_connected\",\"Desc\":\"Attribute not subscribed\",\"Origin\":\"...\"};quality=ATTR_INVALID;',\n",
       " 'event=low-pst/beam/01/realpolanumclippedsamplesrfiexcised;time=2024-10-07 22:02:02;values=[];exception={\"Reason\":\"NOT_connected\",\"Desc\":\"Attribute not subscribed\",\"Origin\":\"...\"};quality=ATTR_INVALID;',\n",
       " 'event=low-pst/beam/01/imagpolavariancefreqavgrfiexcised;time=2024-10-07 22:02:02;values=[];exception={\"Reason\":\"NOT_connected\",\"Desc\":\"Attribute not subscribed\",\"Origin\":\"...\"};quality=ATTR_INVALID;',\n",
       " 'event=low-pst/beam/01/imagpolanumclippedsamplesrfiexcised;time=2024-10-07 22:02:02;values=[];exception={\"Reason\":\"NOT_connected\",\"Desc\":\"Attribute not subscribed\",\"Origin\":\"...\"};quality=ATTR_INVALID;',\n",
       " 'event=low-pst/beam/01/realpolbvariancefreqavg;time=2024-10-07 22:02:02;values=[];exception={\"Reason\":\"NOT_connected\",\"Desc\":\"Attribute not subscribed\",\"Origin\":\"...\"};quality=ATTR_INVALID;',\n",
       " 'event=low-pst/beam/01/realpolbnumclippedsamples;time=2024-10-07 22:02:02;values=[];exception={\"Reason\":\"NOT_connected\",\"Desc\":\"Attribute not subscribed\",\"Origin\":\"...\"};quality=ATTR_INVALID;',\n",
       " 'event=low-pst/beam/01/imagpolbvariancefreqavg;time=2024-10-07 22:02:02;values=[];exception={\"Reason\":\"NOT_connected\",\"Desc\":\"Attribute not subscribed\",\"Origin\":\"...\"};quality=ATTR_INVALID;',\n",
       " 'event=low-pst/beam/01/imagpolbnumclippedsamples;time=2024-10-07 22:02:02;values=[];exception={\"Reason\":\"NOT_connected\",\"Desc\":\"Attribute not subscribed\",\"Origin\":\"...\"};quality=ATTR_INVALID;',\n",
       " 'event=low-pst/beam/01/realpolbvariancefreqavgrfiexcised;time=2024-10-07 22:02:02;values=[];exception={\"Reason\":\"NOT_connected\",\"Desc\":\"Attribute not subscribed\",\"Origin\":\"...\"};quality=ATTR_INVALID;',\n",
       " 'event=low-pst/beam/01/realpolbnumclippedsamplesrfiexcised;time=2024-10-07 22:02:02;values=[];exception={\"Reason\":\"NOT_connected\",\"Desc\":\"Attribute not subscribed\",\"Origin\":\"...\"};quality=ATTR_INVALID;',\n",
       " 'event=low-pst/beam/01/imagpolbvariancefreqavgrfiexcised;time=2024-10-07 22:02:02;values=[];exception={\"Reason\":\"NOT_connected\",\"Desc\":\"Attribute not subscribed\",\"Origin\":\"...\"};quality=ATTR_INVALID;',\n",
       " 'event=low-pst/beam/01/imagpolbnumclippedsamplesrfiexcised;time=2024-10-07 22:02:02;values=[];exception={\"Reason\":\"NOT_connected\",\"Desc\":\"Attribute not subscribed\",\"Origin\":\"...\"};quality=ATTR_INVALID;')"
      ]
     },
     "execution_count": 7,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "ah.eventSummary"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "id": "199ad34e-0fac-4497-bae2-2df510b8f023",
   "metadata": {},
   "source": [
    "The following image is the alarms that had been added in a testing environment after applying the above code.\n",
    "![image](alarm_handler_rules.png)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "4a1ad291-bb11-41cc-a82b-896b25df46e4",
   "metadata": {},
   "source": [
    "## Perform scans to generate alarms\n",
    "\n",
    "The following subsections explain how to test the different rules."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "eca19cc2-97a7-4261-b798-04128a97c4c3",
   "metadata": {},
   "source": [
    "### Signal Statistics Warnings\n",
    "\n",
    "The following warnings are statistical warnings on the input data coming from the CBF.\n",
    "\n",
    "* realPolAVarianceFreqAvg\n",
    "* realPolANumClippedSamples\n",
    "* imagPolAVarianceFreqAvg\n",
    "* imagPolANumClippedSamples\n",
    "* realPolAVarianceFreqAvgRfiExcised\n",
    "* realPolANumClippedSamplesRfiExcised\n",
    "* imagPolAVarianceFreqAvgRfiExcised\n",
    "* imagPolANumClippedSamplesRfiExcised\n",
    "* realPolBVarianceFreqAvg\n",
    "* realPolBNumClippedSamples\n",
    "* imagPolBVarianceFreqAvg\n",
    "* imagPolBNumClippedSamples\n",
    "* realPolBVarianceFreqAvgRfiExcised\n",
    "* realPolBNumClippedSamplesRfiExcised\n",
    "* imagPolBVarianceFreqAvgRfiExcised\n",
    "* imagPolBNumClippedSamplesRfiExcised\n",
    "\n",
    "For the `FreqAvg` attributes, the easiest way to cause the alarm to happen is to ensure that the data sent from the CBF is just zeros.  In the CBF Simulator notebook, this can be performed by using the Gaussian Noise generator with a standard deviation of 0.0.\n",
    "\n",
    "For the `ClippedSamples` attributes, the easiest way to cause the alarm to happen is to ensure that data has a large variance/standard deviation such that clipping occurs.  For `NBIT=16`, as standard deviation of ~11,000 would ensure that some clipping would occur, for systems where `NBIT=8` a value of ~45 would suffice (these calculations are from $2^{nbit-1} / 3$"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "067652b6-bb20-404e-bd0c-f7d90d38e393",
   "metadata": {},
   "source": [
    "### Available Recording Time Warning & Alarm\n",
    "\n",
    "These alarms occur when the available disk space on the local filesystem (LFS) in the digital signal processing (DSP) application is less that $t * bytes\\_per\\sec$ for a given scan, where $t$ is the time left for the scan.\n",
    "\n",
    "Simulating this requires filling up the LFS within the pod, this can be done by ensure the SEND pod stops cleaning up files.  The default test parent deployment of PST provides only 10GB of file storage each for the LFS and the shared disk between the SDP data product dashboard.  This can be filled up relatively quickyly by relatively long scan of around 10 minutes using the CBF simulator notebook.\n",
    "\n",
    "When performing such a simulation one should clean up the LFS snd SDP data mounts within in the SEND application\n",
    "\n",
    "* Use Kubectl get into the pod\n",
    "\n",
    "```\n",
    "kubectl exec -it ska-pst-core-send -- bash\n",
    "```\n",
    "\n",
    "* Remove files from the LFS and SDP mounts:\n",
    "\n",
    "```\n",
    "rm -fr /mnt/lfs/product/eb-*\n",
    "rm -fr /mnt/sdp/product/eb-*\n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "c24af68c-caca-4322-99db-69ce8d93cbec",
   "metadata": {},
   "source": [
    "### Ring Buffer Utilisation Warning and Alarm\n",
    "\n",
    "Testing of alarm is very hard to perform because if everything is going correctly it the DSP pipeline would be reading data from the ring buffer fast enough not to cause the ring buffer to get full.  There is no way to stop the DSP processing from performing the task."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "5a9eeca9-205f-4ce0-aaaf-19f389dba504",
   "metadata": {},
   "source": [
    "### UDP Packet errors and validity alarms\n",
    "\n",
    "The following attributes can be tested by using the CBF Simulator and enabling the `Induce Errors` and setting the \n",
    "percentage of packets that would have the errors.  These could be tested individually or in parallel.\n",
    "\n",
    "* dataDropRate\n",
    "* misorderedPacketRate\n",
    "* malformedPacketRate\n",
    "* misdirectedPacketRate\n",
    "* checksumFailurePacketRate\n",
    "* timestampSyncErrorPacketRate\n",
    "* seqNumberSyncErrorPacketRate\n",
    "* noValidPolarisationCorrectionPacketRate\n",
    "* noValidStationBeamPacketRate\n",
    "* noValidPstBeamPacketRate\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "cd0bdb94-8579-487f-bd0e-99dbba9faa2f",
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.10.12"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
