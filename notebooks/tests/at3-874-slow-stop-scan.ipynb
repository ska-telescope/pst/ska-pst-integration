{
 "cells": [
  {
   "cell_type": "code",
   "execution_count": 1,
   "id": "8da38d59-0505-4a8c-9521-d12edf293dbe",
   "metadata": {},
   "outputs": [],
   "source": [
    "%load_ext autoreload\n",
    "%autoreload 2"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "c13a5846-1721-4630-a7f9-89febebcee53",
   "metadata": {},
   "source": [
    "## Testing stopping of a slow DSP.DISK scan\n",
    "\n",
    "This notebook is used to test the bug [AT3-874](https://jira.skatelescope.org/browse/AT3-874) where slowness of the disk I/O of the local filesystem would cause DSP.DISK to take too long to complete before a timeout between the Python LMC code and the C++ gRPC service.  This resulted in the LMC going into a FAULT state and forcing DSP.DISK into a FAULT state but meanwhile the background scanning thread in DSP.DISK was still processing the data and the scan would eventually finish.\n",
    "\n",
    "**Note this script can be used for either manual or automated testing.**\n",
    "\n",
    "### Deploy the latest version of ska-pst\n",
    "\n",
    "Before running this notebook, launch the `test-parent` helm chart by running the following commands in a terminal with access to the same Kubernetes cluster as this notebook (e.g. `psi-head` in the Low PSI)\n",
    "\n",
    "```\n",
    "git clone --recursive git@gitlab.com:ska-telescope/pst/ska-pst.git\n",
    "cd ska-pst\n",
    "make k8s-install-chart K8S_CHART=test-parent K8S_CHART_PARAMS=\" --values=tests/integration/k8srunner/gitlab.test-parent.yaml\"\n",
    "make k8s-wait KUBE_APP=ska-pst\n",
    "```\n",
    "\n",
    "When finished running this demo, please remember to \n",
    "\n",
    "```\n",
    "make k8s-uninstall-chart K8S_CHART=test-parent\n",
    "```\n",
    "\n",
    "### After the chart is deployed, start this script"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "ebca7104",
   "metadata": {},
   "source": [
    "#### Setup imports for notebook"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "id": "9410847a",
   "metadata": {},
   "outputs": [],
   "source": [
    "import json\n",
    "import logging\n",
    "import os\n",
    "import pprint\n",
    "import sys\n",
    "import tempfile\n",
    "import threading\n",
    "import time\n",
    "from typing import Tuple\n",
    "\n",
    "import backoff\n",
    "import numpy as np\n",
    "from ska_control_model import AdminMode, LoggingLevel, ObsState\n",
    "from ska_pst.testutils.scan_config import (\n",
    "    ScanConfigGenerator,\n",
    "    ScanIdFactory,\n",
    "    create_default_scan_config_generator,\n",
    "    generate_eb_id,\n",
    ")\n",
    "from ska_pst.testutils.tango import PstTestDeviceProxy\n",
    "from ska_pst.testutils.udp_gen import create_udp_data_generator"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "75265e0b-2cc2-4234-b3df-33e830fe491a",
   "metadata": {},
   "source": [
    "Determine the write I/O rate for the LFS.\n",
    "\n",
    "This notebook should be deployed using NFS for the local filesystem as that would reduce the I/O write rate considerably compared to an NVME drive"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "id": "69bda814-62d5-492f-8764-feed6692aa00",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Avg. write rate: 0.15 GB/s\n",
      "Avg. write rate: 1.29 Gb/s\n"
     ]
    }
   ],
   "source": [
    "one_gigabyte = 2**30\n",
    "bits_per_gigabyte = 8 * one_gigabyte\n",
    "chunk = 1024  # 1kB\n",
    "num_chunks = one_gigabyte // chunk  # ~1million\n",
    "output_data = os.urandom(one_gigabyte)\n",
    "with tempfile.NamedTemporaryFile(dir=\"/mnt/lfs\") as f:\n",
    "    start_time = time.time()\n",
    "    for idx in range(num_chunks):\n",
    "        f.write(output_data[idx * chunk : (idx + 1) * chunk])\n",
    "        f.flush()\n",
    "    end_time = time.time()\n",
    "\n",
    "rate_gigabyte_sec = 1 / (end_time - start_time)\n",
    "rate_gigabits_sec = bits_per_gigabyte * rate_gigabyte_sec / 1_000_000_000\n",
    "rate_gigabits_sec = np.around(rate_gigabits_sec, 2)\n",
    "print(f\"Avg. write rate: {rate_gigabyte_sec:0.2f} GB/s\")\n",
    "print(f\"Avg. write rate: {rate_gigabits_sec:0.2f} Gb/s\")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 4,
   "id": "2063f3bc-7391-44cf-b789-1bcaf7d87a84",
   "metadata": {},
   "outputs": [],
   "source": [
    "# The following method is meant to run in the background\n",
    "def _background_io(scanlen: int, evt: threading.Event) -> None:\n",
    "    now = time.time()\n",
    "    endtime = now + scanlen\n",
    "    try:\n",
    "        logger.info(\"Staring background IO\")\n",
    "        while not evt.is_set() and time.time() < endtime:\n",
    "            start_time = time.time()\n",
    "            expected_endtime = start_time + 1 / (rate_gigabyte_sec)\n",
    "            os.urandom(chunk)\n",
    "            while time.time() < expected_endtime:\n",
    "                with tempfile.NamedTemporaryFile(dir=\"/mnt/lfs\") as f:\n",
    "                    f.write(output_data)\n",
    "                    f.flush()\n",
    "        logger.info(\"Background IO complete\")\n",
    "    except Exception:\n",
    "        logger.exception(\"Error in writing data\", exc_info=True)\n",
    "\n",
    "\n",
    "def start_background_io(scanlen: int) -> Tuple[threading.Thread, threading.Event]:\n",
    "    evt = threading.Event()\n",
    "    thread = threading.Thread(target=_background_io, args=[scanlen, evt], daemon=True)\n",
    "    thread.start()\n",
    "\n",
    "    return (thread, evt)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "c989a4ad",
   "metadata": {},
   "source": [
    "#### Set up logging\n",
    "\n",
    "This will ensure any of the utility classes will log to cell outputs. IPython defaults to logging to `stderr` but the cells need to\n",
    "`stdout`.  If we didn't do this we would need to put print statements in the utility classes which is not a good development practice."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 5,
   "id": "95541b94",
   "metadata": {},
   "outputs": [],
   "source": [
    "# override format here for more or less logging information\n",
    "# also update the logging level for different level of logging verbosity\n",
    "logging.basicConfig(\n",
    "    format=\"%(asctime)s | %(levelname)s : %(message)s\",\n",
    "    level=logging.INFO,\n",
    "    stream=sys.stdout,\n",
    ")\n",
    "\n",
    "logger = logging.getLogger()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 6,
   "id": "b9330fb2-089e-43f5-a28e-4b3cbbc104ec",
   "metadata": {},
   "outputs": [],
   "source": [
    "scan_id_factory: ScanIdFactory = ScanIdFactory()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 7,
   "id": "c025bc4a-69e7-4fa7-b22c-77df09eaa543",
   "metadata": {},
   "outputs": [],
   "source": [
    "@backoff.on_exception(backoff.expo, AssertionError, factor=0.1, max_time=1.0)\n",
    "def assert_obs_state(beam: PstTestDeviceProxy, obs_state: ObsState) -> None:\n",
    "    \"\"\"Assert that the current obsState of the BEAM.MGMT device is a given value.\n",
    "\n",
    "    This uses a backoff decorator to allow retesting the value over a period of 1 second.\n",
    "    \"\"\"\n",
    "    curr_obsState = ObsState(beam.obsState)\n",
    "    assert curr_obsState == obs_state, f\"current obsState = {curr_obsState}, expected {obs_state}\""
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "id": "a2a34b4b-d8fc-47a1-9a93-e092546aac9f",
   "metadata": {},
   "source": [
    "### Set the TANGO_HOST environment variable\n",
    "\n",
    "If the `TANGO_HOST` environment variable is already set to something other than the default, then the following code assumes that it has been set correctly (e.g. in the environment variables of the image running `notebook-test`) and the value is not modified.\n",
    "\n",
    "Otherwise, the following code sets `TANGO_HOST` to the Tango database server in the `pst` namespace.\n",
    "\n",
    "If a different namespace was used to deploy the `test-parent` chart, then set the `kube_namespace` variable accordingly.\n",
    "\n",
    "#### Using notebook agasint a k8s cluster\n",
    "\n",
    "If using this Notebook against a k8s cluster, like minikube, that you have admin access\n",
    "\n",
    "```\n",
    "$ kubectl get -n <namespace>  svc\n",
    "```\n",
    "\n",
    "This should output something like, find the `EXTERNAL-IP` for the `databaseds-tango-base-test` service.\n",
    "\n",
    "```console\n",
    "NAME                         TYPE           CLUSTER-IP       EXTERNAL-IP     PORT(S)          AGE\n",
    "databaseds-tango-base-test   LoadBalancer   10.109.225.112   192.168.49.97   10000:30553/TCP  50m\n",
    "```\n",
    "\n",
    "Ensure that you can reach the external IP and port.  On a Linux environment you should be able to do:\n",
    "\n",
    "```\n",
    "nc -v <external-ip> 10000\n",
    "```"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 8,
   "id": "01e1a96b-43e8-428d-a286-116571221539",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "2024-12-10 22:07:40,936 | INFO : TANGO_HOST=databaseds-tango-base-test:10000\n"
     ]
    }
   ],
   "source": [
    "default_tango_host = \"tango-databaseds.staging:10000\"\n",
    "tango_host = os.environ.get(\"TANGO_HOST\", default_tango_host)\n",
    "\n",
    "if tango_host in [default_tango_host, \"\"]:\n",
    "    os.environ[\"TANGO_HOST\"] = \"databaseds-tango-base-test:10000\"\n",
    "\n",
    "# If using k8s and got the \"EXTERNAL-IP\" of databaseds-tango-base-test, uncomment this and set the IP\n",
    "# os.environ[\"TANGO_HOST\"] = \"192.168.49.98:10000\"\n",
    "\n",
    "logger.info(f\"TANGO_HOST={os.environ['TANGO_HOST']}\")"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "6d4e781f-502b-4457-b921-53b42273f673",
   "metadata": {},
   "source": [
    "### Create a beam device object and turn it on.  \n",
    "\n",
    "Rather than using a Tango DeviceProxy directly, a thin test wrapper class `PstTestDeviceProxy` is used. This allows for waiting for\n",
    "long running commands to complete, which is needed when this notebook is run in an automated environment.\n",
    "\n",
    "The `beam_fqdn` in the next cell is the Tango fully qualified domain name (FQDN) of the PST Beam under test, it can be any\n",
    "FQDN for any valid PST Beam that is in the same Tango infrastructure as the `TANGO_HOST`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 9,
   "id": "cb33a5e0",
   "metadata": {},
   "outputs": [],
   "source": [
    "# override this FQDN when testing against a different PST BEAM instance.\n",
    "# In future, automated testing against different beams could use an per test environment value\n",
    "beam_fqdn = \"low-pst/beam/01\"\n",
    "\n",
    "beam = PstTestDeviceProxy(beam_fqdn)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 10,
   "id": "ec39136d",
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "'pst-low'"
      ]
     },
     "execution_count": 10,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "# get subsystem that the PST BEAM is for.  This is used in sending data to the SDP data product dashboard\n",
    "\n",
    "subsystem_id = beam.get_property(\"SubsystemId\")[\"SubsystemId\"][0]\n",
    "subsystem_id"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 11,
   "id": "00719729-46b3-4933-901d-676f2a8c0533",
   "metadata": {},
   "outputs": [],
   "source": [
    "# ensure that the BEAM is in `ONLINE` mode.\n",
    "beam.adminMode = AdminMode.ONLINE"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 12,
   "id": "e843e383",
   "metadata": {},
   "outputs": [],
   "source": [
    "# setting the monitoring polling rate, in millisecs. The default is 5000ms (5s) but for\n",
    "# this test set it to 1000ms (1s)\n",
    "beam.monitoringPollingRate = 1000"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 13,
   "id": "6f62b271-441c-4224-8f69-516277a35d68",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "2024-12-10 22:07:49,306 | INFO : Long running command result = QUEUED, command id = 1733868469.303337_13602700353879_On\n",
      "2024-12-10 22:07:49,386 | INFO : Current state of command 1733868469.303337_13602700353879_On = QUEUED\n",
      "2024-12-10 22:08:19,307 | INFO : Current state of command 1733868469.303337_13602700353879_On = COMPLETED\n"
     ]
    }
   ],
   "source": [
    "# this will 'turn on' the beam.\n",
    "beam.On()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 14,
   "id": "63bb1275-93ef-4e4a-8e97-be551bb9375b",
   "metadata": {},
   "outputs": [],
   "source": [
    "assert_obs_state(beam, ObsState.IDLE)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 15,
   "id": "90218e91-8689-4c2e-8eb1-6b0bca1a1e12",
   "metadata": {},
   "outputs": [],
   "source": [
    "beam.loggingLevel = LoggingLevel.INFO"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "id": "ad8d90a2-f069-40d8-9a73-2af2b6cdb541",
   "metadata": {},
   "source": [
    "### Configure a scan"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "d2899688",
   "metadata": {},
   "source": [
    "For automated testing this notebook uses a random Scan Configuration based upon the `ska_telmodel` CSP JSON schema.\n",
    "This can be overridden by using a manually crafted JSON.\n",
    "\n",
    "To use a specific CSP JSON request, then rather than using `create_defaul_scan_config_generator` use a\n",
    "`create_fixed_scan_config_generator` that will always replay the same scan configuation.\n",
    "\n",
    "PST expects at least version `2.4` of the CSP JSON schema."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 16,
   "id": "ab8cb282-13d6-4a09-b2f6-fe5c64fec19b",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Use this cell if in automated tests or using default schema from CSP JSON\n",
    "scan_config_generator: ScanConfigGenerator = create_default_scan_config_generator()"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "07efa50e",
   "metadata": {},
   "source": [
    "Setup an Execution Block. All runs of this notebook will be based on the same eb_id"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 17,
   "id": "a1fef2c1",
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "'eb-k102-20241210-68530'"
      ]
     },
     "execution_count": 17,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "eb_id: str = generate_eb_id()\n",
    "eb_id"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "c8cdc5e8",
   "metadata": {},
   "source": [
    "Generate a scan config and enable the data generator to send all zeros.\n",
    "\n",
    "This will use the `eb_id` generated from above."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 18,
   "id": "fc5c9fac",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "{   'interface': 'https://schema.skao.int/ska-pst-configure/2.5',\n",
      "    'common': {   'config_id': '6fZUMYlnhEHmx2qj7fYG',\n",
      "                  'subarray_id': 1,\n",
      "                  'frequency_band': 'low',\n",
      "                  'eb_id': 'eb-k102-20241210-68530'},\n",
      "    'pst': {   'scan': {   'activation_time': '2024-12-10T22:09:15.470Z',\n",
      "                           'bits_per_sample': 32,\n",
      "                           'timing_beam_id': '8',\n",
      "                           'num_of_polarizations': 2,\n",
      "                           'udp_nsamp': 32,\n",
      "                           'wt_nsamp': 32,\n",
      "                           'udp_nchan': 24,\n",
      "                           'num_frequency_channels': 41472,\n",
      "                           'centre_frequency': 150000000.0,\n",
      "                           'total_bandwidth': 7465277.777777,\n",
      "                           'observation_mode': 'VOLTAGE_RECORDER',\n",
      "                           'observer_id': 'observer_spiritual9',\n",
      "                           'project_id': 'project_classy8',\n",
      "                           'pointing_id': 'pointing_peculiar5',\n",
      "                           'source': 'J1538-5621',\n",
      "                           'itrf': [5109360.133, 2006852.586, -3238948.127],\n",
      "                           'receiver_id': 'LOW',\n",
      "                           'feed_polarization': 'LIN',\n",
      "                           'feed_handedness': 1,\n",
      "                           'feed_angle': 45.0,\n",
      "                           'feed_tracking_mode': 'FA',\n",
      "                           'feed_position_angle': 0.0,\n",
      "                           'oversampling_ratio': [4, 3],\n",
      "                           'coordinates': {   'equinox': 2000.0,\n",
      "                                              'ra': '15:38:43.2',\n",
      "                                              'dec': '-56.3654'},\n",
      "                           'max_scan_length': 30.0,\n",
      "                           'subint_duration': 30.0,\n",
      "                           'receptors': ['SKA016', 'SKA019', 'SKA020'],\n",
      "                           'receptor_weights': [0.4, 0.25, 0.35],\n",
      "                           'num_channelization_stages': 2,\n",
      "                           'channelization_stages': [   {   'num_filter_taps': 1,\n",
      "                                                            'filter_coefficients': [   1.0],\n",
      "                                                            'num_frequency_channels': 1024,\n",
      "                                                            'oversampling_ratio': [   32,\n",
      "                                                                                      27]},\n",
      "                                                        {   'num_filter_taps': 1,\n",
      "                                                            'filter_coefficients': [   1.0],\n",
      "                                                            'num_frequency_channels': 256,\n",
      "                                                            'oversampling_ratio': [   4,\n",
      "                                                                                      3]}],\n",
      "                           'num_rfi_frequency_masks': 4,\n",
      "                           'rfi_frequency_masks': [   [   278300000.0,\n",
      "                                                          279300000.0],\n",
      "                                                      [   290900000.0,\n",
      "                                                          291900000.0],\n",
      "                                                      [   299700000.0,\n",
      "                                                          300700000.0],\n",
      "                                                      [   323100000.0,\n",
      "                                                          324000000.0]]}}}\n"
     ]
    }
   ],
   "source": [
    "# limit the maximum data rate. This equates to a max of 432 channels in Low\n",
    "scan_config = scan_config_generator.generate(\n",
    "    eb_id=eb_id,\n",
    "    overrides={\n",
    "        \"num_frequency_channels\": 41472,\n",
    "        \"total_bandwidth\": 7_465_277.777777,\n",
    "        \"centre_frequency\": 150_000_000.0,\n",
    "    },\n",
    ")\n",
    "\n",
    "# display the scan configuration. Use pretty print\n",
    "pprint.pprint(scan_config, sort_dicts=False, indent=4)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 19,
   "id": "c1547715",
   "metadata": {},
   "outputs": [],
   "source": [
    "assert scan_config[\"common\"][\"eb_id\"] == eb_id"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 20,
   "id": "d38592fd",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "2024-12-10 22:09:20,455 | INFO : Long running command result = QUEUED, command id = 1733868560.4537354_190637587590196_ConfigureScan\n",
      "2024-12-10 22:09:20,486 | INFO : Current state of command 1733868560.4537354_190637587590196_ConfigureScan = QUEUED\n",
      "2024-12-10 22:09:20,986 | INFO : Current state of command 1733868560.4537354_190637587590196_ConfigureScan = COMPLETED\n"
     ]
    }
   ],
   "source": [
    "# Perform the ConfigureScan request to configure PST BEAM.\n",
    "scan_config_str = json.dumps(scan_config)\n",
    "beam.ConfigureScan(scan_config_str)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 21,
   "id": "5dcb7f57-1ac9-4baa-9c6a-14e99d4f7577",
   "metadata": {},
   "outputs": [],
   "source": [
    "assert_obs_state(beam, ObsState.READY)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 22,
   "id": "57451ebf-0f6d-427c-b30a-ed0acf9f767f",
   "metadata": {},
   "outputs": [],
   "source": [
    "assert beam.configurationId == scan_config_generator.curr_config_id\n",
    "assert beam.lastScanConfiguration == scan_config_str"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 23,
   "id": "6eac5e03",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "{   'num_channel_blocks': 1,\n",
      "    'channel_blocks': [   {   'destination_host': '10.10.211.40',\n",
      "                              'destination_port': 32080,\n",
      "                              'destination_mac': '82:c9:3b:c9:6d:fe',\n",
      "                              'start_pst_channel': 26724,\n",
      "                              'start_pst_frequency': 146267361.1111111,\n",
      "                              'num_pst_channels': 2064}]}\n"
     ]
    }
   ],
   "source": [
    "channel_block_configuration = json.loads(beam.channelBlockConfiguration)\n",
    "pprint.pprint(channel_block_configuration, sort_dicts=False, indent=4)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "6bcf9a99-d181-4482-bcff-5f5fac655de0",
   "metadata": {},
   "source": [
    "### Start and stop a scan"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 24,
   "id": "7345cb4d-f2b5-419f-ae06-452ded703648",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "2024-12-10 22:09:32,008 | INFO : Scan ID = 238\n"
     ]
    }
   ],
   "source": [
    "scan_id = scan_id_factory.generate_scan_id()\n",
    "logger.info(f\"Scan ID = {scan_id}\")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 25,
   "id": "b78e9af8",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "2024-12-10 22:09:33,859 | INFO : Long running command result = QUEUED, command id = 1733868573.8584054_127450744988173_Scan\n",
      "2024-12-10 22:09:33,886 | INFO : Current state of command 1733868573.8584054_127450744988173_Scan = QUEUED\n",
      "2024-12-10 22:10:03,860 | INFO : Current state of command 1733868573.8584054_127450744988173_Scan = COMPLETED\n"
     ]
    }
   ],
   "source": [
    "beam.Scan(str(scan_id))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 26,
   "id": "8ac279d2-2a36-4773-8663-420841fb060e",
   "metadata": {},
   "outputs": [],
   "source": [
    "assert_obs_state(beam, ObsState.SCANNING)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "98ea1e36",
   "metadata": {},
   "source": [
    "Ensure data is sent from the CBF"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 27,
   "id": "075d1733",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "2024-12-10 22:10:10,668 | INFO : Staring background IO\n",
      "2024-12-10 22:10:10,668 | WARNING : Deprecated field 'observation_mode' will be renamed to 'pst_processing_mode' in version 3.0 of PST schema\n",
      "2024-12-10 22:10:10,670 | WARNING : Deprecated field num_frequency_channels's value of 41472 is not the expected value 2064 for frequency band low. Field will be removed in version 3.0 of PST schema\n",
      "2024-12-10 22:10:10,670 | WARNING : Deprecated field 'channelization_stages' will be removed in version 3.0 of PST schema\n",
      "2024-12-10 22:10:10,672 | INFO : Starting to create UDP data.\n",
      "2024-12-10 22:10:10,673 | INFO : Creating config file: /tmp/config_scan_238_beam_8_1122.txt\n",
      "2024-12-10 22:10:10,704 | INFO : Generated Output file:\n",
      "HDR_SIZE                        4096\n",
      "HDR_VERSION                     1.0\n",
      "TELESCOPE                       SKALow\n",
      "RECEIVER                        LFAA\n",
      "INSTRUMENT                      LowCBF\n",
      "NBIT                            16\n",
      "NANT                            3\n",
      "NPOL                            2\n",
      "NDIM                            2\n",
      "TSAMP                           207.36\n",
      "BAND                            Low\n",
      "WT_NSAMP                        32\n",
      "UDP_NSAMP                       32\n",
      "UDP_NCHAN                       24\n",
      "NCHAN                           2064\n",
      "FREQ                            150.0\n",
      "BW                              7.4652777777770005\n",
      "START_CHANNEL                   26724\n",
      "END_CHANNEL                     28788\n",
      "RESOLUTION                      528384\n",
      "BYTES_PER_SECOND                79629629.62962963\n",
      "DATA_HOST                       10.10.211.40\n",
      "DATA_PORT                       32080\n",
      "LOCAL_HOST                      10.10.211.61\n",
      "OBS_OFFSET                      0\n",
      "SOURCE                          J1538-5621\n",
      "DATA_KEY                        0110\n",
      "WEIGHTS_KEY                     0112\n",
      "UDP_FORMAT                      LowPST\n",
      "NUMA_NODE                       0\n",
      "HB_NBUFS                        8\n",
      "HB_BUFSZ                        16384\n",
      "DB_NBUFS                        64\n",
      "DB_BUFSZ                        8454144\n",
      "WB_NBUFS                        64\n",
      "WB_BUFSZ                        71552\n",
      "SCANLEN_MAX                     30\n",
      "OS_FACTOR                       4/3\n",
      "SCAN_ID                         238\n",
      "BEAM_ID                         8\n",
      "2024-12-10 22:10:10,705 | INFO : Data written to /tmp/config_scan_238_beam_8_1122.txt\n",
      "2024-12-10 22:10:10,706 | INFO : generate_udp_data cmd=['ska_pst_recv_udpgen', '-t', '30', '/tmp/config_scan_238_beam_8_1122.txt', '-r', '1.29']\n",
      "2024-12-10 22:10:10,707 | INFO : Process 189 has started\n",
      "2024-12-10 22:10:42,090 | INFO : [UDPGEN] b'1|2024-12-10T22:10:10.741Z|INFO|Thread-189|configure_scan|UDPGenerator.cpp#163||Generated UTC_START=2024-12-10-22:10:10\\n'\n",
      "2024-12-10 22:10:42,091 | INFO : Process return code: 0\n",
      "2024-12-10 22:10:42,550 | INFO : Background IO complete\n"
     ]
    }
   ],
   "source": [
    "scanlen = 30\n",
    "(bg_thread, bg_evt) = start_background_io(scanlen)\n",
    "\n",
    "# Set up UDP Gen to send the data\n",
    "scan_resources = scan_config_generator.calculate_udp_gen_resources()\n",
    "udp_data_generator = create_udp_data_generator(\n",
    "    scan_resources=scan_resources,\n",
    "    scan_id=scan_id,\n",
    "    scanlen=scanlen,\n",
    "    channel_block_configuration=channel_block_configuration,\n",
    "    data_rate=np.round(rate_gigabits_sec, 2),\n",
    ")\n",
    "udp_data_generator.generate_udp_data()"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "f5d5534c",
   "metadata": {},
   "source": [
    "### Wait for all of scan data to be sent\n",
    "\n",
    "For tests using the PST UDP simulator this can be done by calling `wait_for_end_of_data` on the UDP Data Generator. However, for tests\n",
    "with external data sources it is up to the tester to wait for all that data to have been sent to PST."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 28,
   "id": "d4fd02cc-126b-4094-8979-a795a9227686",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "2024-12-10 22:10:11,750 | INFO : RB Util (%):  0.00\n",
      "2024-12-10 22:10:12,753 | INFO : RB Util (%): 21.88\n",
      "2024-12-10 22:10:13,755 | INFO : RB Util (%): 26.56\n",
      "2024-12-10 22:10:14,758 | INFO : RB Util (%): 26.56\n",
      "2024-12-10 22:10:15,762 | INFO : RB Util (%): 56.25\n",
      "2024-12-10 22:10:16,765 | INFO : RB Util (%): 82.81\n",
      "2024-12-10 22:10:17,767 | INFO : RB Util (%): 96.88\n",
      "2024-12-10 22:10:18,770 | INFO : RB Util (%): 96.88\n",
      "2024-12-10 22:10:19,774 | INFO : RB Util (%): 96.88\n",
      "2024-12-10 22:10:20,777 | INFO : RB Util (%): 95.31\n",
      "2024-12-10 22:10:21,780 | INFO : RB Util (%): 96.88\n",
      "2024-12-10 22:10:22,783 | INFO : RB Util (%): 96.88\n",
      "2024-12-10 22:10:23,785 | INFO : RB Util (%): 96.88\n",
      "2024-12-10 22:10:24,788 | INFO : RB Util (%): 96.88\n",
      "2024-12-10 22:10:25,791 | INFO : RB Util (%): 96.88\n",
      "2024-12-10 22:10:26,795 | INFO : RB Util (%): 96.88\n",
      "2024-12-10 22:10:26,796 | INFO : Ending Scan\n",
      "2024-12-10 22:10:26,802 | INFO : Long running command result = QUEUED, command id = 1733868626.800166_152046439435464_EndScan\n",
      "2024-12-10 22:10:26,887 | INFO : Current state of command 1733868626.800166_152046439435464_EndScan = QUEUED\n",
      "2024-12-10 22:10:40,187 | INFO : Current state of command 1733868626.800166_152046439435464_EndScan = COMPLETED\n",
      "13.4 s ± 0 ns per loop (mean ± std. dev. of 1 run, 1 loop each)\n"
     ]
    }
   ],
   "source": [
    "now = time.time()\n",
    "end = now + scanlen\n",
    "rb_utilitisation = beam.ringBufferUtilisation\n",
    "count = 0\n",
    "\n",
    "while time.time() < end:\n",
    "    logger.info(f\"RB Util (%): {rb_utilitisation:5.2f}\")\n",
    "    if rb_utilitisation >= 95.0:\n",
    "        # wait max of 10 seconds at rate alaram\n",
    "        count += 1\n",
    "        if count >= 10:\n",
    "            break\n",
    "    time.sleep(1)\n",
    "    rb_utilitisation = beam.ringBufferUtilisation\n",
    "\n",
    "logger.info(\"Ending Scan\")\n",
    "%timeit -r1 -n1 beam.EndScan()"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "e08fa7d5-7a6c-4eda-a3a8-ae228478e615",
   "metadata": {},
   "source": [
    "Note that the time above is more than 5 seconds, which was the cause of the time out.  The following assertions should be true if the EndScan command completed successfully."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 29,
   "id": "56190d0e-b275-470c-9ada-9d149b025b9d",
   "metadata": {},
   "outputs": [],
   "source": [
    "assert_obs_state(beam, ObsState.READY)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 30,
   "id": "424d6bbe-c500-41d1-89ef-d2afd2d3a446",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "2024-12-10 22:12:23,389 | INFO : Long running command result = QUEUED, command id = 1733868743.3868814_63710855015540_GoToIdle\n",
      "2024-12-10 22:12:23,485 | INFO : Current state of command 1733868743.3868814_63710855015540_GoToIdle = QUEUED\n",
      "2024-12-10 22:12:23,689 | INFO : Current state of command 1733868743.3868814_63710855015540_GoToIdle = COMPLETED\n"
     ]
    }
   ],
   "source": [
    "beam.GoToIdle()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 31,
   "id": "1792df40-c9c5-4817-b165-da8bd6d98de3",
   "metadata": {},
   "outputs": [],
   "source": [
    "assert_obs_state(beam, ObsState.IDLE)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 32,
   "id": "4074a14f-b904-4c8f-b01f-ea773f4b4b82",
   "metadata": {},
   "outputs": [],
   "source": [
    "bg_evt.set()\n",
    "bg_thread.join()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 33,
   "id": "5fc1cbd6",
   "metadata": {},
   "outputs": [],
   "source": [
    "udp_data_generator.abort()\n",
    "udp_data_generator.wait_for_end_of_data()"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.10.12"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
