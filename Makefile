CLUSTER_DOMAIN ?= cluster.local

# include common pst makefile library
-include .pst/base.mk

## The following should be standard includes
# include core makefile targets for release management
include .make/base.mk

# include oci makefile targets for oci management
include .make/cpp.mk

# include OCI support
include .make/oci.mk

# include helm make support
include .make/helm.mk

# include k8s make support
include .make/k8s.mk

# include xray upload
include .make/xray.mk

NOTEBOOK_IGNORE_FILES:=not debug.ipynb

# include python make support
-include .make/python.mk

# include your own private variables for custom deployment configuration
-include PrivateRules.mak

PROJECT=ska-pst
# Variables populated for local development and oci build.
#       Overriden by CI variables. See .gitlab-ci.yml#L19

POETRY_VERSION ?=1.8.2

SKA_RELEASE_REGISTRY=artefact.skao.int
PST_GITLAB_REGISTRY=registry.gitlab.com/ska-telescope/pst/ska-pst
PST_DEV_REGISTRY?=$(PST_GITLAB_REGISTRY)

# the dev image contains all ska-pst dependencies, psrchive and dspsr dependencies
DEV_IMAGE                       ?= registry.gitlab.com/ska-telescope/pst/ska-pst-dsp-tools/ska-pst-dspsr-builder
DEV_TAG                         ?= 0.0.13
SKA_PST_DSP_TOOLS_IMAGE         ?= $(DEV_IMAGE):$(DEV_TAG)

SKA_TANGO_BUILDER_IMAGE         ?=artefact.skao.int/ska-tango-images-pytango-builder
SKA_TANGO_BUILDER_TAG           ?=9.5.0

SKA_TANGO_RUNTIME_IMAGE         ?=artefact.skao.int/ska-tango-images-pytango-runtime
SKA_TANGO_RUNTIME_TAG           ?=9.5.0

NVIDIA_CUDA_RUNTIME_IMAGE       ?=nvidia/cuda
NVIDIA_CUDA_RUNTIME_TAG         ?=11.8.0-runtime-ubuntu22.04

SKA_PST_VERSION_TAG ?= $(VERSION)
SKA_PST_K8S_RELATIVE_MOUNT ?= /data/product/$(USER)
ifneq ($(CI_COMMIT_SHORT_SHA),)
SKA_PST_VERSION_TAG := $(SKA_PST_VERSION_TAG)-dev.c$(CI_COMMIT_SHORT_SHA)
SKA_PST_K8S_RELATIVE_MOUNT := /data/product/$(CI_COMMIT_SHORT_SHA)
endif

SKA_PST_K8S_MOUNT_FLAGS ?= --set ska-pst.core.primaryVolumeMount.hostPath=$(SKA_PST_K8S_RELATIVE_MOUNT)
ifneq ($(SKA_PST_K8S_MOUNT_FLAGS),)
SKA_PST_K8S_MOUNT_FLAGS := $(SKA_PST_K8S_MOUNT_FLAGS)
endif

K8S_CHART_VALUES ?= "tests/integration/k8srunner/gitlab.test-parent.yaml"
ifneq ($(K8S_CHART_VALUES),)
K8S_CHART_VALUES_FLAG := --values=$(K8S_CHART_VALUES)
endif

SKA_PST_CORE_BUILDER_IMAGE      ?=ska-pst-core-builder
SKA_PST_CORE_RUNTIME_IMAGE      ?=ska-pst-core-runtime
SKA_PST_PYTHON_BUILDER_IMAGE    ?=ska-pst-python-builder
SKA_PST_TEST_SUPPORT_IMAGE      ?=ska-pst-test-support

NOTEBOOK_LINT_TARGET=notebooks
PYTHON_TEST_FOLDER_NBMAKE=notebooks/tests
POETRY_CONFIG_VIRTUALENVS_CREATE=false

_VENV=.venv
_REQUIREMENTS=resources/k8s-bdd/requirements/k8s-bdd.python.txt

OCI_REGISTRY ?= $(PST_DEV_REGISTRY)
OCI_BUILDER=docker
OCI_IMAGE_BUILD_CONTEXT=$(PWD)
OCI_IMAGE=ska-pst-test-support
OCI_IMAGES_TO_PUBLISH=ska-pst-dsp ska-pst-jupyterlab ska-pst-lmc ska-pst-recv ska-pst-send ska-pst-smrb ska-pst-stat ska-pst-test-support

OCI_BUILD_ADDITIONAL_ARGS=--build-arg SKA_PST_DSP_TOOLS_IMAGE=$(SKA_PST_DSP_TOOLS_IMAGE) \
	--build-arg SKA_TANGO_BUILDER_IMAGE=$(SKA_TANGO_BUILDER_IMAGE):$(SKA_TANGO_BUILDER_TAG) \
	--build-arg SKA_TANGO_RUNTIME_IMAGE=$(SKA_TANGO_RUNTIME_IMAGE):$(SKA_TANGO_RUNTIME_TAG) \
	--build-arg SKA_PST_CORE_BUILDER_IMAGE=$(PST_DEV_REGISTRY)/$(SKA_PST_CORE_BUILDER_IMAGE):$(SKA_PST_VERSION_TAG) \
	--build-arg SKA_PST_CORE_RUNTIME_IMAGE=$(PST_DEV_REGISTRY)/$(SKA_PST_CORE_RUNTIME_IMAGE):$(SKA_PST_VERSION_TAG) \
	--build-arg SKA_PST_PYTHON_BUILDER_IMAGE=$(PST_DEV_REGISTRY)/$(SKA_PST_PYTHON_BUILDER_IMAGE):$(SKA_PST_VERSION_TAG) \
	--build-arg SKA_PST_TEST_SUPPORT_IMAGE=$(PST_DEV_REGISTRY)/$(SKA_PST_TEST_SUPPORT_IMAGE):$(SKA_PST_VERSION_TAG) \
	--build-arg NVIDIA_CUDA_RUNTIME_IMAGE=$(NVIDIA_CUDA_RUNTIME_IMAGE):$(NVIDIA_CUDA_RUNTIME_TAG) \
  --build-arg POETRY_VERSION=$(POETRY_VERSION)

DEV_IMAGE ?=$(PST_DEV_REGISTRY)/$(SKA_PST_CORE_BUILDER_IMAGE):$(VERSION)

K8S_TEST_IMAGE_TO_TEST_IMG=$(PST_GITLAB_REGISTRY)/$(SKA_PST_TEST_SUPPORT_IMAGE)
K8S_TEST_IMAGE_TO_TEST_TAG=$(SKA_PST_VERSION_TAG)

K8S_CHART_PARAMS := $(K8S_CHART_VALUES_FLAG) $(SKA_PST_K8S_MOUNT_FLAGS) \
  --set ska-pst.image.tag="$(K8S_TEST_IMAGE_TO_TEST_TAG)" \
  --set image.tag="$(K8S_TEST_IMAGE_TO_TEST_TAG)" \
  --set global.cluster_domain=$(CLUSTER_DOMAIN)

PYTEST_FLAKY_MAX_RUNS?=3
PYTEST_FLAKY_OPTIONS ?= --force-flaky --max-runs=$(PYTEST_FLAKY_MAX_RUNS)

PYTHON_SRC = python/src/
PYTHON_GENERATED_SRC = python/generated/
PYTHON_TEST_DIR ?= python/tests
PROTOBUF_DIR ?= $(shell pwd)/protobuf
K8S_PYTHON_TEST_DIR ?= /app/python/tests
PYTHON_TEST_FILE = $(PYTHON_TEST_DIR)/unit
PYTHON_BDD_TESTS_DIR = $(PYTHON_TEST_DIR)/integration

PYTHON_LINE_LENGTH = 110
PYTHON_LINT_TARGET = python/src/ python/tests/
PYTHON_SWITCHES_FOR_FLAKE8 := --extend-ignore=BLK,T --enable=DAR104 --ignore=E203,FS003,W503,N802 --max-complexity=10 \
    --rst-roles=py:attr,py:class,py:const,py:exc,py:func,py:meth,py:mod \
		--rst-directives=deprecated,uml


ifeq ($(findstring notebook, $(MAKECMDGOALS)),notebook)
	PYTHON_LINT_TARGET=$(NOTEBOOK_LINT_TARGET)
	PYTHON_SWITCHES_FOR_FLAKE8 := --extend-ignore=BLK,T --enable=DAR104 --ignore=E203,FS003,W503,N802,D100,D103 --max-complexity=10
endif

PYTHON_SWITCHES_FOR_ISORT := --skip-glob="*/__init__.py" --py=310
PYTHON_SWITCHES_FOR_PYLINT = --disable=W,C,R
PYTHON_SWITCHES_FOR_AUTOFLAKE ?= --in-place --remove-unused-variables --remove-all-unused-imports --recursive --ignore-init-module-imports

PYTHON_VARS_AFTER_PYTEST := $(PYTHON_VARS_AFTER_PYTEST) $(PYTEST_FLAKY_OPTIONS) --disable-pytest-warnings -rP
ifeq ($(strip $(firstword $(MAKECMDGOALS))),k8s-test)
# need to set the PYTHONPATH since the ska-cicd-makefile default definition
# of it is not OK for the alpine images
TANGO_HOST ?= databaseds-tango-base-test:10000

PYTHON_VARS_BEFORE_PYTEST = TANGO_HOST="$(TANGO_HOST)"
K8S_TEST_IMAGE_TO_TEST = $(K8S_TEST_IMAGE_TO_TEST_IMG):$(K8S_TEST_IMAGE_TO_TEST_TAG)
PYTHON_VARS_AFTER_PYTEST := $(PYTHON_VARS_AFTER_PYTEST)
else
# add code coverage config to allow for overrides
PYTHON_VARS_AFTER_PYTEST := $(PYTHON_VARS_AFTER_PYTEST) --cov-config=$(PWD)/.coveragerc
endif

python-pre-generate-code:
	@echo "Ensuring generated path $(PYTHON_GENERATED_SRC) exists"
	mkdir -p $(PYTHON_GENERATED_SRC)
	poetry install --with codegen

python-do-generate-code:
	@echo "Generating Python gRPC/Protobuf code."
	@echo "PROTOBUF_DIR=$(PROTOBUF_DIR)"
	@echo "PYTHON_GENERATED_SRC=$(PYTHON_GENERATED_SRC)"
	@echo
	@echo "List of protobuf files: $(shell find -L "$(PROTOBUF_DIR)" -iname "*.proto")"
	@echo
	$(PYTHON_RUNNER) python3 -m grpc_tools.protoc \
      -I"$(PROTOBUF_DIR)" \
      --python_out="$(PYTHON_GENERATED_SRC)" \
      --pyi_out="$(PYTHON_GENERATED_SRC)" \
      --grpc_python_out="$(PYTHON_GENERATED_SRC)" \
      $(shell find -L "$(PROTOBUF_DIR)" -iname "*.proto")
	@echo
	@echo "Files generated. $(shell find "$(PYTHON_GENERATED_SRC)" -iname "*.py")"

python-post-generate-code:

python-generate-code: python-pre-generate-code python-do-generate-code python-post-generate-code

.PHONY: python-generate-code python-pre-generate-code python-do-generate-code python-post-generate-code

mypy:
	$(PYTHON_RUNNER) mypy --config-file mypy.ini $(PYTHON_LINT_TARGET)

flake8:
	$(PYTHON_RUNNER) flake8 --show-source --statistics $(PYTHON_SWITCHES_FOR_FLAKE8) $(PYTHON_LINT_TARGET)

python-post-format:
	$(PYTHON_RUNNER) autoflake $(PYTHON_SWITCHES_FOR_AUTOFLAKE) $(PYTHON_LINT_TARGET)

notebook-post-format:
	$(PYTHON_RUNNER) nbqa autoflake $(PYTHON_SWITCHES_FOR_AUTOFLAKE) $(PYTHON_LINT_TARGET)

notebook-format: notebook-pre-format notebook-do-format notebook-post-format

python-post-lint: mypy

notebook-mypy:
	$(PYTHON_RUNNER) nbqa mypy --config-file=mypy.ini $(PYTHON_LINT_TARGET)

notebook-post-lint: notebook-mypy

.PHONY: python-post-format, notebook-format, notebook-post-format, python-post-lint, mypy, flake8, notebook-mypy

.PHONY: local-helm-build local-init-venv

local-helm-build:
	helm dependency build charts/$(PROJECT)
local-init-venv:
	$(call venv_exec,$(_VENV),pip install -r $(_REQUIREMENTS))

define venv_exec
	$(if [ ! -f "$($(1)/bin/activate)" ], python3 -m venv $(1))
	( \
    	source $(1)/bin/activate; \
    	$(2) \
	)
endef

# KUBE_NAMESPACE := pst
# Initialise k8stest workload
_K8S_TEST_DEPLOYMENT_NAME=pst-testutils
_K8S_TEST_OCI=$(K8S_TEST_IMAGE_TO_TEST_IMG):$(K8S_TEST_IMAGE_TO_TEST_TAG)
.PHONY: pst-k8stest-deploy pst-k8stest-patch pst-k8stest-wait pst-k8stest-show pst-k8stest-clean
pst-k8stest-deploy:
	@echo "Creating k8s deployment..."
	kubectl -n $(KUBE_NAMESPACE) create deployment \
		$(_K8S_TEST_DEPLOYMENT_NAME) \
		--image=$(_K8S_TEST_OCI)

_PST_TESTUTILS_PATCH={"spec":{"replicas":1,"template":{"spec":{"replicas":1,"strategy":{"type":"Recreate"},"containers":[{"name":"ska-pst-test-support","env":[{"name":"TANGO_HOST","value":"databaseds-tango-base-test:10000"}],"tty":true,"volumeMounts":[{"name":"ska-pst-lfs","mountPath":"/mnt/lfs"}, {"name":"sdp-data","mountPath":"/mnt/sdp"}]}],"volumes":[{"name":"ska-pst-lfs","persistentVolumeClaim":{"claimName":"ska-pst-lfs-pvc"}}, {"name":"sdp-data","persistentVolumeClaim":{"claimName":"ska-sdp-shared"}}]}}}}
_PST_TESTUTILS_LABEL=ska-pst-test-parent
pst-k8stest-patch:
	@echo "Delete pod, retain deployment"
	kubectl -n $(KUBE_NAMESPACE) scale deployment/$(_K8S_TEST_DEPLOYMENT_NAME) --replicas=0
	@echo "Wait for pod termination"
	kubectl -n $(KUBE_NAMESPACE) wait --for=delete pods -l app=$(_PST_TESTUTILS_LABEL) --timeout=300s
	@echo "kubectl patch deployment..."
	kubectl -n $(KUBE_NAMESPACE) patch deployment/$(_K8S_TEST_DEPLOYMENT_NAME) -p '$(_PST_TESTUTILS_PATCH)'
	@echo "Confirm patch rollout status"
	kubectl -n $(KUBE_NAMESPACE) rollout status deployment $(_K8S_TEST_DEPLOYMENT_NAME)

pst-k8stest-wait:
	kubectl -n $(KUBE_NAMESPACE) wait --for=condition=ready pods -l app=$(_PST_TESTUTILS_LABEL)

pst-k8stest-show:
	@echo "kubectl show deployment..."
	kubectl -n $(KUBE_NAMESPACE) get deployment/$(_K8S_TEST_DEPLOYMENT_NAME)
	kubectl -n $(KUBE_NAMESPACE) get pods -l app=$(_PST_TESTUTILS_LABEL)

pst-k8stest-clean:
	@echo "Deleting k8s deployment..."
	kubectl -n $(KUBE_NAMESPACE) delete deployment/$(_K8S_TEST_DEPLOYMENT_NAME) --ignore-not-found=true

pst-k8stest-clean-dsp-files:
	@echo "Deleting dsp data and weights files..."
	kubectl -n $(KUBE_NAMESPACE) exec deployment/$(_K8S_TEST_DEPLOYMENT_NAME) -- bash -c "sudo rm -rf /mnt/lfs/*"


pst-k8stest-update-test-payload:
	@echo "Updating k8s test payload..."
	kubectl -n $(KUBE_NAMESPACE) exec deployment/$(_K8S_TEST_DEPLOYMENT_NAME) -- bash -c "rm -rf $(K8S_PYTHON_TEST_DIR)"
	kubectl -n $(KUBE_NAMESPACE) cp $(PYTHON_TEST_DIR) $(shell kubectl -n $(KUBE_NAMESPACE) get pods --selector=app=$(_PST_TESTUTILS_LABEL) -o jsonpath='{.items[0].metadata.name}'):$(K8S_PYTHON_TEST_DIR)
	kubectl -n $(KUBE_NAMESPACE) cp pytest.ini $(shell kubectl -n $(KUBE_NAMESPACE) get pods --selector=app=$(_PST_TESTUTILS_LABEL) -o jsonpath='{.items[0].metadata.name}'):/app/python
	kubectl -n $(KUBE_NAMESPACE) exec deployment/$(_K8S_TEST_DEPLOYMENT_NAME) -- bash -c "ls -l $(K8S_PYTHON_TEST_DIR)"
	@echo "Updating k8s read/write permissions for test..."
	kubectl -n $(KUBE_NAMESPACE) exec deployment/$(_K8S_TEST_DEPLOYMENT_NAME) -- bash -c "sudo chmod -R 777 /mnt"
	kubectl -n $(KUBE_NAMESPACE) exec deployment/$(_K8S_TEST_DEPLOYMENT_NAME) -- bash -c "ls -l /mnt"

# pst-k8stest-clean pst-k8stest-deploy pst-k8stest-patch
k8s-pre-test: pst-k8stest-wait pst-k8stest-show pst-k8stest-update-test-payload

_TEST_OUTPUT_PATH=/tmp
_K8S_TEST_JUNIT_XML=unit-tests.xml
_K8S_TEST_CUCUMBER_JSON=cucumber.json
_K8S_TEST_REPORT_JSON=report.json
K8S_TEST_RESULTS_PATH ?= build/k8srunner/reports
.PHONY: pst-k8stest-pytest pst-k8stest-copy-test-results
k8s-do-test:
	@echo "Execute pytest"
	kubectl -n $(KUBE_NAMESPACE) exec deployment/$(_K8S_TEST_DEPLOYMENT_NAME) \
		-- bash -c "pytest $(PYTHON_VARS_AFTER_PYTEST) --junitxml=$(_TEST_OUTPUT_PATH)/$(_K8S_TEST_JUNIT_XML) --cucumber-json=$(_TEST_OUTPUT_PATH)/$(_K8S_TEST_CUCUMBER_JSON) --json-report --json-report-file=$(_TEST_OUTPUT_PATH)/$(_K8S_TEST_REPORT_JSON) $(PYTHON_BDD_TESTS_DIR)"

pst-k8stest-copy-test-results:
	@echo "Copy test results from pod to local filesystem"
	rm -rf $(K8S_TEST_RESULTS_PATH)
	mkdir -p $(K8S_TEST_RESULTS_PATH)
	kubectl -n $(KUBE_NAMESPACE) exec $(shell kubectl -n $(KUBE_NAMESPACE) get pods --selector=app=$(_PST_TESTUTILS_LABEL) -o jsonpath='{.items[0].metadata.name}') -- bash -c 'sudo chown -R 1000:1000 /tmp'
	kubectl -n $(KUBE_NAMESPACE) cp $(shell kubectl -n $(KUBE_NAMESPACE) get pods --selector=app=$(_PST_TESTUTILS_LABEL) -o jsonpath='{.items[0].metadata.name}'):$(_TEST_OUTPUT_PATH) $(K8S_TEST_RESULTS_PATH)
	ls -l $(K8S_TEST_RESULTS_PATH)

k8s-post-test: pst-k8stest-copy-test-results

k8s-test-shell:
	kubectl -n $(KUBE_NAMESPACE) exec -ti deployment/$(_K8S_TEST_DEPLOYMENT_NAME) -- bash

# Override for psi low. Remove poetry packages installation step
k8s_test_command = /bin/bash -o pipefail -c "\
	mkfifo results-pipe && tar zx --warning=all && \
		export PYTHONPATH=${PYTHONPATH}:/app/$(PYTHON_SRC):/app/$(PYTHON_GENERATED_SRC) && \
		mkdir -p build && \
	( \
	$(K8S_TEST_TEST_COMMAND); \
	); \
	echo \$$? > build/status; pip list > build/pip_list.txt; \
	echo \"k8s_test_command: test command exit is: \$$(cat build/status)\"; \
	tar zcf results-pipe build;"

k8s-pre-uninstall-chart:
	- kubectl exec -ti deployment.apps/ska-pst-core-send -- bash -c 'ls -l $${PST_DSP_MOUNT}/product/'
	- kubectl exec -ti deployment.apps/ska-pst-core-send -- bash -c 'rm -rf $${PST_DSP_MOUNT}/product/*'
	- kubectl exec -ti deployment.apps/ska-pst-core-send -- bash -c 'ls -l $${PST_DSP_MOUNT}/product/'

XRAY_TEST_RESULT_FILE ?= ./build/k8srunner/reports/cucumber.json

DPD_LOCAL_API_PORT?=8000
DPD_LOCAL_DASHBOARD_PORT?=8100
PST_LOCAL_JUPYTERLAB_PORT?=8888

dpd-forward-api:
	kubectl -n ${KUBE_NAMESPACE} port-forward service/ska-dataproduct-api $(DPD_LOCAL_API_PORT):8000

dpd-forward-dashboard:
	kubectl -n ${KUBE_NAMESPACE} port-forward service/ska-dataproduct-dashboard $(DPD_LOCAL_DASHBOARD_PORT):8100

pst-forward-jupyterlab:
	kubectl -n ${KUBE_NAMESPACE} port-forward service/test-ska-pst-jupyterlab $(PST_LOCAL_JUPYTERLAB_PORT):8888

NOTEBOOK_TEST_POD=test-ska-pst-jupyterlab

.PHONY: pst-notebook-patch pst-python-patch
pst-python-patch-jupyterlab:
	kubectl -n $(KUBE_NAMESPACE) exec pod/$(NOTEBOOK_TEST_POD) -- bash -c "sudo rm -rf /app/python"
	kubectl -n $(KUBE_NAMESPACE) cp python $(shell kubectl -n $(KUBE_NAMESPACE) get pods --selector=app=ska-pst-jupyterlab -o jsonpath='{.items[0].metadata.name}'):/tmp/python
	kubectl -n $(KUBE_NAMESPACE) exec pod/$(NOTEBOOK_TEST_POD) -- bash -c "sudo mv /tmp/python /app/python && chown -R tango:tango /app/python"

pst-python-patch-testutils:
	kubectl -n $(KUBE_NAMESPACE) exec deployment/$(_K8S_TEST_DEPLOYMENT_NAME) -- bash -c "sudo rm -rf /app/python"
	kubectl -n $(KUBE_NAMESPACE) cp python $(shell kubectl -n $(KUBE_NAMESPACE) get pods --selector=app=$(_PST_TESTUTILS_LABEL) -o jsonpath='{.items[0].metadata.name}'):/tmp/python
	kubectl -n $(KUBE_NAMESPACE) exec deployment/$(_K8S_TEST_DEPLOYMENT_NAME) -- bash -c "sudo mv /tmp/python /app/python && chown -R tango:tango /app/python"
	kubectl -n $(KUBE_NAMESPACE) exec deployment/$(_K8S_TEST_DEPLOYMENT_NAME) -- bash -c "sudo rm -rf /app/resources/ska-pydada"
	kubectl -n $(KUBE_NAMESPACE) cp resources/ska-pydada $(shell kubectl -n $(KUBE_NAMESPACE) get pods --selector=app=$(_PST_TESTUTILS_LABEL) -o jsonpath='{.items[0].metadata.name}'):/tmp/ska-pydada
	kubectl -n $(KUBE_NAMESPACE) exec deployment/$(_K8S_TEST_DEPLOYMENT_NAME) -- bash -c "sudo mv /tmp/ska-pydada /app/resources/ska-pydada && chown -R tango:tango /app/resources/ska-pydada"

pst-notebook-patch:
	kubectl -n $(KUBE_NAMESPACE) exec pod/$(NOTEBOOK_TEST_POD) -- bash -c "sudo rm -rf /app/notebooks"
	kubectl -n $(KUBE_NAMESPACE) cp notebooks $(shell kubectl -n $(KUBE_NAMESPACE) get pods --selector=app=ska-pst-jupyterlab -o jsonpath='{.items[0].metadata.name}'):/tmp/notebooks
	kubectl -n $(KUBE_NAMESPACE) exec pod/$(NOTEBOOK_TEST_POD) -- bash -c "sudo mv /tmp/notebooks /app/notebooks && chown -R tango:tango /app/notebooks"

notebook-do-test:
	@mkdir -p build
	kubectl -n $(KUBE_NAMESPACE) get pod/$(NOTEBOOK_TEST_POD) -o yaml
	kubectl -n $(KUBE_NAMESPACE) exec pod/$(NOTEBOOK_TEST_POD) \
		-- bash -c '$(PYTHON_RUNNER) pytest --version -c /dev/null && $(PYTHON_VARS_BEFORE_PYTEST) $(PYTHON_RUNNER) pytest --nbmake $(PYTHON_SWITCHES_FOR_NBMAKE) $(PYTHON_TEST_FOLDER_NBMAKE)'

protobuf-docs:
	@echo 'Generating protobuf docs'
	@protoc -I=$(PWD)/protobuf \
		--doc_out=$(PWD)/docs/src/api \
		--doc_opt=$(PWD)/protobuf/protobuf.md.mustache,protobuf.md \
		$(PWD)/protobuf/ska_pst/grpc/lmc/ska_pst_lmc.proto || echo "Unable to generate protobuf documentation"

docs-pre-build: protobuf-docs

.PHONY: protobuf-docs docs-pre-build

# Deploy private local k8s registry
.PHONY: pst-registry-apply pst-registry-status pst-registry-delete
pst-registry-apply:
	kubectl -n $(KUBE_NAMESPACE) apply -f tests/integration/k8srunner-psi-low/registry.yaml

pst-registry-status:
	kubectl -n $(KUBE_NAMESPACE) get -f tests/integration/k8srunner-psi-low/registry.yaml

pst-registry-delete:
	kubectl -n $(KUBE_NAMESPACE) delete -f tests/integration/k8srunner-psi-low/registry.yaml

# Push to k8s local Registry
.PHONY: pst-oci-tag pst-poci-ush
NODEPORT=$$(kubectl get svc/registry -o yaml | grep -oP 'nodePort: \K.*')
pst-oci-tag:
	@if [ -z $(PST_OCI_TARGET) ]; then \
	echo "PST_OCI_TARGET environment variable is not set in PrivateRules.mak"; \
	else \
	echo "PST_OCI_TARGET=$(PST_OCI_TARGET)"; \
	docker tag $(PST_OCI_TARGET):$(SKA_PST_VERSION_TAG) localhost:$(NODEPORT)/$(PST_OCI_TARGET):$(SKA_PST_VERSION_TAG); \
	docker image ls localhost:$(NODEPORT)/$(PST_OCI_TARGET):$(SKA_PST_VERSION_TAG); \
	fi;

pst-oci-push:
	@if [ -z $(PST_OCI_TARGET) ]; then \
	echo "PST_OCI_TARGET environment variable is not set in PrivateRules.mak"; \
	else \
	echo "PST_OCI_TARGET=$(PST_OCI_TARGET)"; \
	docker push localhost:$(NODEPORT)/$(PST_OCI_TARGET):$(SKA_PST_VERSION_TAG); \
	fi;

DSPSR_CONFIG=resources/dspsr/configure
DSPSR_MAKEFILE=build/dspsr/Makefile
# This allows for gitlab to override where to install the dspsr
DSPSR_INST ?= $(INST)/dspsr

.PHONY: dspsr dspsr-bootstrap
dspsr: $(DSPSR_MAKEFILE)
	cd build/dspsr && make -j $(PROCESSOR_COUNT) && make install

$(DSPSR_MAKEFILE): $(DSPSR_CONFIG)
	mkdir -p build/dspsr
	cp images/ska-pst-core-builder/backends.list build/dspsr
	cd build/dspsr && ../../$(DSPSR_CONFIG) \
		--with-cuda-include-dir=/usr/local/cuda/include \
		--with-cuda-lib-dir=/usr/local/cuda/lib64 \
		--enable-shared \
		--enable-existing=no \
		--disable-python \
		--prefix=$(DSPSR_INST)

dspsr-bootstrap $(DSPSR_CONFIG):
	cd resources/dspsr && ./bootstrap

dspsr-check:
	cd build/dspsr && make check

pst-sync-charts:
	@echo "sync ska-pst chart dependencies"
	helm dependency update charts/ska-pst/
	helm dependency build charts/ska-pst/
	@echo "sync test-parent chart dependencies"
	helm dependency update charts/test-parent/
	helm dependency build charts/test-parent/

# Directory where the chart files are located
CHARTS_DIR := $(shell pwd)/charts/test-parent/charts

# Find the ska-tango-archiver and ska-tango-archiver-timescaledb charts dynamically
EDA_ARCHIVER_CHART := $(shell find $(CHARTS_DIR) -name 'ska-tango-archiver-[0-9]*.tgz')
EDA_TIMESCALEDB_CHART := $(shell find $(CHARTS_DIR) -name 'ska-tango-archiver-timescaledb-*.tgz')

eda-timescaledb-install:
	@echo "install ska-tango-archiver-timscaledb"
	helm -n ${KUBE_NAMESPACE} install timescaledb -f tests/integration/eda/values-timescaledb.yaml \
		$(EDA_TIMESCALEDB_CHART)
	kubectl -n ${KUBE_NAMESPACE} wait --for=condition=ready pod timescaledb-0 --timeout=5m
	kubectl -n ${KUBE_NAMESPACE} wait --for=condition=ready pod timescaledb-1 --timeout=5m
	kubectl expose pod timescaledb-0

# admin password used for integration between timescaledb and the eda event subscriber
PGPASSWORD_ADMIN=$$(kubectl get secret --namespace ${KUBE_NAMESPACE} "timescaledb-credentials" -o jsonpath="{.data.PATRONI_admin_PASSWORD}" | base64 --decode)

eda-timescaledb-configure-initialise-dbschema:
	kubectl -n ${KUBE_NAMESPACE} exec -i pod/timescaledb-0 -- psql -U postgres -f - < tests/integration/eda/timescaledb.sql

eda-timescaledb-configure-update-credential:
	kubectl -n ${KUBE_NAMESPACE} exec -i pod/timescaledb-0 -- psql -U postgres -c "ALTER USER admin WITH PASSWORD '$(PGPASSWORD_ADMIN)';"

eda-timescaledb-configure: eda-timescaledb-configure-initialise-dbschema eda-timescaledb-configure-update-credential

eda-timescaledb-uninstall:
	@echo "uninstall ska-tango-archiver-timscaledb"
	helm -n ${KUBE_NAMESPACE} uninstall timescaledb
	$(MAKE) eda-timescaledb-cleanup-resources

eda-timescaledb-cleanup-resources:
	kubectl -n ${KUBE_NAMESPACE} delete --force \
		pod/timescaledb-0 \
		pod/timescaledb-1 \
		secret/timescaledb-certificate \
		secret/timescaledb-credentials \
		secret/timescaledb-pgbackrest \
		service/timescaledb-config \
		service/timescaledb-0 \
		pvc/storage-volume-timescaledb-0 \
		pvc/storage-volume-timescaledb-1 \
		pvc/wal-volume-timescaledb-0 \
		pvc/wal-volume-timescaledb-1

eda-archiver-install:
	@echo "install ska-tango-archiver"
	helm -n ${KUBE_NAMESPACE} upgrade --install archiver -f tests/integration/eda/values-archiver.yaml \
		--set archwizard_config="MyHDB=tango://databaseds-tango-base.$(KUBE_NAMESPACE).svc.cluster.local:10000/low-eda/cm/01" \
    --set dbpassword=$(PGPASSWORD_ADMIN) \
		$(EDA_ARCHIVER_CHART)

eda-archiver-uninstall:
	@echo "uninstall ska-tango-archiver"
	helm -n ${KUBE_NAMESPACE} uninstall archiver

eda-install-stack:
	$(MAKE) k8s-install-chart
	$(MAKE) eda-timescaledb-install
	$(MAKE) eda-timescaledb-configure
	$(MAKE) eda-archiver-install

eda-uninstall-chart:
	helm -n ${KUBE_NAMESPACE} uninstall timescaledb

eda-uninstall-stack:
	$(MAKE) k8s-uninstall-chart
	$(MAKE) eda-timescaledb-uninstall
	$(MAKE) eda-archiver-uninstall
