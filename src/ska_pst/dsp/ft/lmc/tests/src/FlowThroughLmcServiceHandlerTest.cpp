/*
 * Copyright 2024 Square Kilometre Array Observatory
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 * contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#include "ska_pst/dsp/ft/lmc/tests/FlowThroughLmcServiceHandlerTest.h"
#include "ska_pst/dsp/testutils/dsp_testutils.h"

#include "ska_pst/common/lmc/LmcServiceException.h"
#include "ska_pst/common/testutils/GtestMain.h"
#include "ska_pst/common/utils/Logging.h"
#include "ska_pst/common/definitions.h"

#include <iostream>
#include <thread>
#include <chrono>

using namespace std::chrono_literals;

auto main(int argc, char *argv[]) -> int
{
  return ska::pst::common::test::gtest_main(argc, argv);
}

namespace ska::pst::dsp::test
{

  FlowThroughLmcServiceHandlerTest::FlowThroughLmcServiceHandlerTest()
      : ::testing::Test()
  {
  }

  void FlowThroughLmcServiceHandlerTest::SetUp()
  {
    PSTLOG_TRACE(this, "creating flow through manager");
    _dsp = std::make_shared<ska::pst::dsp::FlowThroughManager>("/tmp", false);

    PSTLOG_TRACE(this, "creating handler");
    handler = std::make_shared<ska::pst::dsp::FlowThroughLmcServiceHandler>(_dsp);

    setup_data_block();
  }

  void FlowThroughLmcServiceHandlerTest::TearDown()
  {
    if (_dsp->is_scanning())
    {
      PSTLOG_TRACE(this, "_dsp->stop_scan()");
      _dsp->stop_scan();
    }

    if (_dsp->is_scan_configured())
    {
      PSTLOG_TRACE(this, "_dsp->deconfigure_scan()");
      _dsp->deconfigure_scan();
    }

    if (_dsp->is_beam_configured())
    {
      PSTLOG_TRACE(this, "_dsp->deconfigure_beam()");
      _dsp->deconfigure_beam();
    }

    tear_down_data_block();

    PSTLOG_TRACE(this, "complete");
  }

  void FlowThroughLmcServiceHandlerTest::configure_beam()
  {
    ska::pst::lmc::BeamConfiguration request;
    auto dsp_resources_request = request.mutable_dsp_flow_through();
    dsp_resources_request->set_data_key(data_key);
    dsp_resources_request->set_weights_key(weights_key);

    handler->configure_beam(request);
  }

  void FlowThroughLmcServiceHandlerTest::configure_scan(bool write_config)
  {
    ska::pst::lmc::ScanConfiguration request;

    auto *dsp_scan_configuration = request.mutable_dsp_flow_through();

    dsp_scan_configuration->set_bytes_per_second(data_scan_config.get<double>("BYTES_PER_SECOND"));
    dsp_scan_configuration->set_scanlen_max(data_scan_config.get<double>("SCANLEN_MAX"));
    dsp_scan_configuration->set_execution_block_id(data_scan_config.get_val("EB_ID"));
    dsp_scan_configuration->set_num_bits_out(data_scan_config.get<uint32_t>("NBIT_OUT"));
    dsp_scan_configuration->set_polarisations(data_scan_config.get_val("POLN_FT"));

    auto chan_ft = data_scan_config.get_val("CHAN_FT");
    std::istringstream iss(chan_ft);
    std::string first{"-1"};
    std::string second{"-1"};
    std::getline(iss, first, ',');
    std::getline(iss, second);
    auto channels = std::make_pair(std::stoi(first), std::stoi(second));
    dsp_scan_configuration->add_channels(channels.first);
    dsp_scan_configuration->add_channels(channels.second);

    dsp_scan_configuration->set_requantisation_scale(data_scan_config.get<float>("DIGITIZER_SCALE"));
    dsp_scan_configuration->set_requantisation_init_time(data_scan_config.get<uint32_t>("DIGITIZER_INIT_TIME"));

    if (write_config) {
      this->write_config();
    }
    handler->configure_scan(request);
  }

  void FlowThroughLmcServiceHandlerTest::start_scan(bool write_data)
  {
    auto request = ska::pst::lmc::StartScanRequest();
    request.set_scan_id(start_scan_config.get<uint32_t>("SCAN_ID"));
    if (write_data)
    {
      this->write_data();
    }
    handler->start_scan(request);
  }

  TEST_F(FlowThroughLmcServiceHandlerTest, configure_deconfigure_beam) // NOLINT
  {
    PSTLOG_TRACE(this, "configuring beam");
    EXPECT_FALSE(handler->is_beam_configured()); // NOLINT
    EXPECT_FALSE(_dsp->is_beam_configured());

    PSTLOG_TRACE(this, "configure_beam");
    configure_beam();

    EXPECT_TRUE(handler->is_beam_configured());
    EXPECT_TRUE(_dsp->is_beam_configured());
    PSTLOG_TRACE(this, "beam configured");

    PSTLOG_TRACE(this, "getting beam configuration");
    ska::pst::lmc::BeamConfiguration beam_configuration;
    handler->get_beam_configuration(&beam_configuration);

    EXPECT_TRUE(beam_configuration.has_dsp_flow_through());
    auto dsp_beam_configuration = beam_configuration.dsp_flow_through();

    EXPECT_EQ(data_key, dsp_beam_configuration.data_key());       // NOLINT
    EXPECT_EQ(weights_key, dsp_beam_configuration.weights_key()); // NOLINT

    ska::pst::common::AsciiHeader &dsp_resources = _dsp->get_beam_configuration();
    EXPECT_EQ(data_key, dsp_resources.get_val("DATA_KEY"));       // NOLINT
    EXPECT_EQ(weights_key, dsp_resources.get_val("WEIGHTS_KEY")); // NOLINT
    PSTLOG_TRACE(this, "checked beam configuration");

    PSTLOG_TRACE(this, "deconfiguring beam");
    handler->deconfigure_beam();
    EXPECT_FALSE(handler->is_beam_configured()); // NOLINT
    EXPECT_FALSE(_dsp->is_beam_configured());    // NOLINT
    PSTLOG_TRACE(this, "beam deconfigured");
  }

  TEST_F(FlowThroughLmcServiceHandlerTest, configure_beam_again_should_throw_exception) // NOLINT
  {
    PSTLOG_TRACE(this, "configuring beam");

    EXPECT_FALSE(handler->is_beam_configured()); // NOLINT

    configure_beam();

    EXPECT_TRUE(handler->is_beam_configured());
    EXPECT_TRUE(_dsp->is_beam_configured());
    PSTLOG_TRACE(this, "beam configured");

    try
    {
      configure_beam();
      FAIL() << " expected configure_beam to throw exception due to beam configured already.\n";
    }
    catch (ska::pst::common::LmcServiceException &ex)
    {
      EXPECT_EQ(std::string(ex.what()), "Beam already configured for DSP.FT.");
      EXPECT_EQ(ex.error_code(), ska::pst::lmc::ErrorCode::CONFIGURED_FOR_BEAM_ALREADY);
      EXPECT_EQ(ex.status_code(), grpc::StatusCode::FAILED_PRECONDITION);
    }
  }

  TEST_F(FlowThroughLmcServiceHandlerTest, configure_beam_with_invalid_configuration) // NOLINT
  {
    PSTLOG_TRACE(this, "configuring beam");
    data_key = "abcd";

    EXPECT_FALSE(handler->is_beam_configured());
    try
    {
      configure_beam();
      FAIL() << " expected configure_beam to throw exception due to invalid configuration.\n";
    }
    catch (std::exception &ex)
    {
      PSTLOG_DEBUG(this, "exception thrown as expected: {}", ex.what());
      EXPECT_EQ(_dsp->get_state(), ska::pst::common::State::RuntimeError);
      EXPECT_FALSE(handler->is_beam_configured());
    }
    PSTLOG_TRACE(this, "test done");
  }

  TEST_F(FlowThroughLmcServiceHandlerTest, configure_beam_should_have_dsp_object) // NOLINT
  {
    PSTLOG_TRACE(this, "configuring beam");
    EXPECT_FALSE(handler->is_beam_configured()); // NOLINT

    ska::pst::lmc::BeamConfiguration beam_configuration;
    beam_configuration.mutable_test();

    try
    {
      handler->configure_beam(beam_configuration);
      FAIL() << " expected configure_beam to throw exception due not having DSP.FT field.\n";
    }
    catch (ska::pst::common::LmcServiceException &ex)
    {
      EXPECT_EQ(std::string(ex.what()), "Expected a DSP.FT beam configuration object, but none were provided.");
      EXPECT_EQ(ex.error_code(), ska::pst::lmc::ErrorCode::INVALID_REQUEST);
      EXPECT_EQ(ex.status_code(), grpc::StatusCode::INVALID_ARGUMENT);
    }
  }

  TEST_F(FlowThroughLmcServiceHandlerTest, get_beam_configuration_when_not_beam_configured) // NOLINT
  {
    PSTLOG_TRACE(this, "getting beam configuration");
    EXPECT_FALSE(handler->is_beam_configured());

    ska::pst::lmc::BeamConfiguration response;
    try
    {
      handler->get_beam_configuration(&response);
    }
    catch (ska::pst::common::LmcServiceException &ex)
    {
      EXPECT_EQ(std::string(ex.what()), "DSP.FT not configured for beam.");
      EXPECT_EQ(ex.error_code(), ska::pst::lmc::ErrorCode::NOT_CONFIGURED_FOR_BEAM);
      EXPECT_EQ(ex.status_code(), grpc::StatusCode::FAILED_PRECONDITION);
    }
    PSTLOG_TRACE(this, "beam not configured");
  }

  TEST_F(FlowThroughLmcServiceHandlerTest, deconfigure_beam_when_not_beam_configured) // NOLINT
  {
    PSTLOG_TRACE(this, "deconfiguring beam");

    EXPECT_FALSE(handler->is_beam_configured()); // NOLINT

    try
    {
      handler->deconfigure_beam();
      FAIL() << " expected deconfigure_beam to throw exception due to beam not configured.\n";
    }
    catch (ska::pst::common::LmcServiceException &ex)
    {
      EXPECT_EQ(std::string(ex.what()), "DSP.FT not configured for beam.");
      EXPECT_EQ(ex.error_code(), ska::pst::lmc::ErrorCode::NOT_CONFIGURED_FOR_BEAM);
      EXPECT_EQ(ex.status_code(), grpc::StatusCode::FAILED_PRECONDITION);
    }
  }

  TEST_F(FlowThroughLmcServiceHandlerTest, deconfigure_beam_when_scan_configured) // NOLINT
  {
    PSTLOG_TRACE(this, "configuring beam");
    configure_beam();
    PSTLOG_TRACE(this, "beam configured");

    PSTLOG_TRACE(this, "configuring scan");
    EXPECT_FALSE(handler->is_scan_configured());
    configure_scan();
    EXPECT_TRUE(handler->is_scan_configured());
    PSTLOG_TRACE(this, "scan configured");

    try
    {
      handler->deconfigure_beam();
      FAIL() << " expected deconfigure_beam to throw exception due having scan configuration.\n";
    }
    catch (ska::pst::common::LmcServiceException &ex)
    {
      PSTLOG_TRACE(this, "exception occurred as expected");
      EXPECT_EQ(std::string(ex.what()), "DSP.FT is configured for scan but trying to deconfigure beam.");
      EXPECT_EQ(ex.error_code(), ska::pst::lmc::ErrorCode::INVALID_REQUEST);
      EXPECT_EQ(ex.status_code(), grpc::StatusCode::FAILED_PRECONDITION);
    }
    PSTLOG_TRACE(this, "done");
  }

  TEST_F(FlowThroughLmcServiceHandlerTest, configure_deconfigure_scan) // NOLINT
  {

    PSTLOG_TRACE(this, "configuring beam");
    configure_beam();
    PSTLOG_TRACE(this, "beam configured");

    PSTLOG_TRACE(this, "configuring scan");
    EXPECT_FALSE(handler->is_scan_configured());
    configure_scan();
    EXPECT_TRUE(handler->is_scan_configured());
    PSTLOG_TRACE(this, "scan configured");

    PSTLOG_TRACE(this, "getting configuration");
    ska::pst::lmc::ScanConfiguration get_response;
    handler->get_scan_configuration(&get_response);
    EXPECT_TRUE(get_response.has_dsp_flow_through());

    const auto &dsp_scan_configuration = get_response.dsp_flow_through();

    auto channels = dsp_scan_configuration.channels();
    std::ostringstream channels_ss;
    channels_ss << channels[0] << "," << channels[1];

    double tolerance = ska::pst::common::test::get_tolerance(dsp_scan_configuration.bytes_per_second(), data_scan_config.get<double>("BYTES_PER_SECOND"));
    EXPECT_NEAR(dsp_scan_configuration.bytes_per_second(), data_scan_config.get<double>("BYTES_PER_SECOND"), tolerance);
    EXPECT_EQ(data_scan_config.get<double>("SCANLEN_MAX"), dsp_scan_configuration.scanlen_max());
    EXPECT_EQ(data_scan_config.get_val("EB_ID"), dsp_scan_configuration.execution_block_id());
    EXPECT_EQ(data_scan_config.get<uint32_t>("NBIT_OUT"), dsp_scan_configuration.num_bits_out());
    EXPECT_EQ(data_scan_config.get_val("POLN_FT"), dsp_scan_configuration.polarisations());
    EXPECT_EQ(data_scan_config.get_val("CHAN_FT"), channels_ss.str());
    EXPECT_EQ(data_scan_config.get<float>("DIGITIZER_SCALE"), dsp_scan_configuration.requantisation_scale());
    EXPECT_EQ(data_scan_config.get<uint32_t>("DIGITIZER_INIT_TIME"), dsp_scan_configuration.requantisation_init_time());

    PSTLOG_TRACE(this, "checked configuration");

    PSTLOG_TRACE(this, "deconfiguring scan");
    handler->deconfigure_scan();
    EXPECT_FALSE(handler->is_scan_configured());
    PSTLOG_TRACE(this, "scan deconfigured");

    PSTLOG_TRACE(this, "deconfiguring beam");
    handler->deconfigure_beam();
    EXPECT_FALSE(handler->is_beam_configured()); // NOLINT
    PSTLOG_TRACE(this, "beam deconfigured");
  }

  TEST_F(FlowThroughLmcServiceHandlerTest, configure_scan_with_invalid_configuration) // NOLINT
  {
    PSTLOG_TRACE(this, "configuring beam");
    configure_beam();
    PSTLOG_TRACE(this, "beam configured");

    PSTLOG_TRACE(this, "configuring scan");
    EXPECT_FALSE(handler->is_scan_configured());
    static constexpr double bad_double = 1e24;
    data_scan_config.set("BYTES_PER_SECOND", bad_double);
    data_scan_config.set("SCANLEN_MAX", bad_double);
    try
    {
      configure_scan();
      FAIL() << " expected configure_scan to throw exception due to not having beam configured.\n";
    }
    catch (std::exception &ex)
    {
      EXPECT_EQ(_dsp->get_state(), ska::pst::common::State::BeamConfigured);
      EXPECT_FALSE(handler->is_scan_configured());
      EXPECT_TRUE(handler->is_beam_configured());

      PSTLOG_TRACE(this, "deconfiguring beam");
      handler->deconfigure_beam();
      EXPECT_EQ(_dsp->get_state(), ska::pst::common::State::Idle);
      PSTLOG_TRACE(this, "deconfiguring beam");
    }
  }

  TEST_F(FlowThroughLmcServiceHandlerTest, configure_scan_when_not_beam_configured) // NOLINT
  {
    PSTLOG_TRACE(this, "configuring scan");

    try
    {
      configure_scan();
      FAIL() << " expected configure_scan to throw exception due to not having beam configured.\n";
    }
    catch (ska::pst::common::LmcServiceException &ex)
    {
      EXPECT_EQ(std::string(ex.what()), "DSP.FT not configured for beam.");
      EXPECT_EQ(ex.error_code(), ska::pst::lmc::ErrorCode::NOT_CONFIGURED_FOR_BEAM);
      EXPECT_EQ(ex.status_code(), grpc::StatusCode::FAILED_PRECONDITION);
    }
  }

  TEST_F(FlowThroughLmcServiceHandlerTest, configure_scan_again_should_throw_exception) // NOLINT
  {
    PSTLOG_TRACE(this, "configuring beam");
    EXPECT_FALSE(handler->is_beam_configured()); // NOLINT
    configure_beam();
    EXPECT_TRUE(handler->is_beam_configured());
    PSTLOG_TRACE(this, "beam configured");

    PSTLOG_TRACE(this, "configuring scan");
    EXPECT_FALSE(handler->is_scan_configured());
    configure_scan();
    EXPECT_TRUE(handler->is_scan_configured());
    PSTLOG_TRACE(this, "scan configured");

    try
    {
      configure_scan(false);
      FAIL() << " expected configure_scan to throw exception due to scan already configured.\n";
    }
    catch (ska::pst::common::LmcServiceException &ex)
    {
      EXPECT_EQ(std::string(ex.what()), "Scan already configured for DSP.FT.");
      EXPECT_EQ(ex.error_code(), ska::pst::lmc::ErrorCode::CONFIGURED_FOR_SCAN_ALREADY);
      EXPECT_EQ(ex.status_code(), grpc::StatusCode::FAILED_PRECONDITION);
    }
  }

  TEST_F(FlowThroughLmcServiceHandlerTest, configure_scan_should_have_dsp_object) // NOLINT
  {
    PSTLOG_TRACE(this, "configuring beam");
    configure_beam();
    PSTLOG_TRACE(this, "beam configured");

    PSTLOG_TRACE(this, "configuring scan");
    try
    {
      ska::pst::lmc::ScanConfiguration configuration;
      configuration.mutable_test();
      handler->configure_scan(configuration);
      FAIL() << " expected configure_scan to throw exception due not having DSP.FT field.\n";
    }
    catch (ska::pst::common::LmcServiceException &ex)
    {
      EXPECT_EQ(std::string(ex.what()), "Expected a DSP.FT scan configuration object, but none were provided.");
      EXPECT_EQ(ex.error_code(), ska::pst::lmc::ErrorCode::INVALID_REQUEST);
      EXPECT_EQ(ex.status_code(), grpc::StatusCode::INVALID_ARGUMENT);
    }
  }

  TEST_F(FlowThroughLmcServiceHandlerTest, deconfigure_scan_when_not_configured) // NOLINT
  {
    PSTLOG_TRACE(this, "deconfiguring scan");

    try
    {
      handler->deconfigure_scan();
      FAIL() << " expected deconfigure_scan to throw exception due to beam not configured.\n";
    }
    catch (ska::pst::common::LmcServiceException &ex)
    {
      EXPECT_EQ(std::string(ex.what()), "Scan not currently configured for DSP.FT.");
      EXPECT_EQ(ex.error_code(), ska::pst::lmc::ErrorCode::NOT_CONFIGURED_FOR_SCAN);
      EXPECT_EQ(ex.status_code(), grpc::StatusCode::FAILED_PRECONDITION);
    }
  }

  TEST_F(FlowThroughLmcServiceHandlerTest, get_scan_configuration_when_not_configured) // NOLINT
  {
    PSTLOG_TRACE(this, "getting scan configuration");

    try
    {
      ska::pst::lmc::ScanConfiguration scan_configuration;
      handler->get_scan_configuration(&scan_configuration);
      FAIL() << " expected deconfigure_beam to throw exception due to beam not configured.\n";
    }
    catch (ska::pst::common::LmcServiceException &ex)
    {
      EXPECT_EQ(std::string(ex.what()), "Not currently configured for DSP.FT.");
      EXPECT_EQ(ex.error_code(), ska::pst::lmc::ErrorCode::NOT_CONFIGURED_FOR_SCAN);
      EXPECT_EQ(ex.status_code(), grpc::StatusCode::FAILED_PRECONDITION);
    }
  }

  TEST_F(FlowThroughLmcServiceHandlerTest, start_scan_stop_scan) // NOLINT
  {
    PSTLOG_TRACE(this, "configuring beam");
    configure_beam();
    PSTLOG_TRACE(this, "beam configured");

    PSTLOG_TRACE(this, "configuring scan");
    configure_scan();
    PSTLOG_TRACE(this, "scan configured");

    PSTLOG_TRACE(this, "starting scan");
    EXPECT_FALSE(handler->is_scanning());
    start_scan();
    EXPECT_TRUE(handler->is_scanning());

    PSTLOG_TRACE(this, "scanning");

    // See AT3-920 sleep to allow pipeline to do its job
    wait_for_pipeline_to_finish(_dsp);

    PSTLOG_TRACE(this, "ending scan");
    handler->stop_scan();
    EXPECT_FALSE(handler->is_scanning());
    PSTLOG_TRACE(this, "scan ended");

    PSTLOG_TRACE(this, "deconfiguring scan");
    handler->deconfigure_scan();
    PSTLOG_TRACE(this, "scan deconfigured");

    PSTLOG_TRACE(this, "deconfiguring beam");
    handler->deconfigure_beam();
    PSTLOG_TRACE(this, "beam deconfigured");
  }

  TEST_F(FlowThroughLmcServiceHandlerTest, start_scan_when_scanning) // NOLINT
  {

    PSTLOG_TRACE(this, "configuring beam");
    configure_beam();
    PSTLOG_TRACE(this, "beam configured");

    PSTLOG_TRACE(this, "configuring scan");
    configure_scan();
    PSTLOG_TRACE(this, "scan configured");

    PSTLOG_TRACE(this, "starting scan");
    EXPECT_FALSE(handler->is_scanning());
    start_scan();
    EXPECT_TRUE(handler->is_scanning());

    try
    {
      start_scan(false);
    }
    catch (ska::pst::common::LmcServiceException &ex)
    {
      EXPECT_EQ(std::string(ex.what()), "DSP.FT is already scanning.");
      EXPECT_EQ(ex.error_code(), ska::pst::lmc::ErrorCode::ALREADY_SCANNING);
      EXPECT_EQ(ex.status_code(), grpc::StatusCode::FAILED_PRECONDITION);
    }

    // See AT3-920 sleep to allow pipeline to do its job
    wait_for_pipeline_to_finish(_dsp);
  }

  TEST_F(FlowThroughLmcServiceHandlerTest, start_scan_when_not_configured_should_throw_exception) // NOLINT
  {
    PSTLOG_TRACE(this, "configuring beam");
    configure_beam();
    PSTLOG_TRACE(this, "beam configured");

    PSTLOG_TRACE(this, "start scan");
    EXPECT_FALSE(handler->is_scan_configured());

    try
    {
      start_scan(false);
      FAIL() << " expected start_scan to throw exception due to scan not being configured.\n";
    }
    catch (ska::pst::common::LmcServiceException &ex)
    {
      EXPECT_EQ(std::string(ex.what()), "Not currently configured for DSP.FT.");
      EXPECT_EQ(ex.error_code(), ska::pst::lmc::ErrorCode::NOT_CONFIGURED_FOR_SCAN);
      EXPECT_EQ(ex.status_code(), grpc::StatusCode::FAILED_PRECONDITION);
    }
  }

  TEST_F(FlowThroughLmcServiceHandlerTest, start_scan_again_should_throw_exception) // NOLINT
  {
    PSTLOG_TRACE(this, "configuring beam");
    configure_beam();
    PSTLOG_TRACE(this, "beam configured");

    PSTLOG_TRACE(this, "configuring scan");
    configure_scan();
    PSTLOG_TRACE(this, "scan configured");

    PSTLOG_TRACE(this, "start scan");
    EXPECT_FALSE(handler->is_scanning());
    start_scan();
    EXPECT_TRUE(handler->is_scanning());
    PSTLOG_TRACE(this, "scanning");

    try
    {
      start_scan();
      FAIL() << " expected start_scan to throw exception due to already scanning.\n";
    }
    catch (ska::pst::common::LmcServiceException &ex)
    {
      EXPECT_EQ(std::string(ex.what()), "DSP.FT is already scanning.");
      EXPECT_EQ(ex.error_code(), ska::pst::lmc::ErrorCode::ALREADY_SCANNING);
      EXPECT_EQ(ex.status_code(), grpc::StatusCode::FAILED_PRECONDITION);
    }

    // See AT3-920 sleep to allow pipeline to do its job
    wait_for_pipeline_to_finish(_dsp);
  }

  TEST_F(FlowThroughLmcServiceHandlerTest, stop_scan_when_not_scanning_should_throw_exception) // NOLINT
  {
    PSTLOG_TRACE(this, "end scan");
    EXPECT_FALSE(handler->is_scanning());

    try
    {
      handler->stop_scan();
      FAIL() << " expected stop_scan to throw exception due to not currently scanning.\n";
    }
    catch (ska::pst::common::LmcServiceException &ex)
    {
      EXPECT_EQ(std::string(ex.what()), "Received stop_scan request when DSP.FT is not scanning.");
      EXPECT_EQ(ex.error_code(), ska::pst::lmc::ErrorCode::NOT_SCANNING);
      EXPECT_EQ(ex.status_code(), grpc::StatusCode::FAILED_PRECONDITION);
    }
  }

  // test getting monitoring data
  TEST_F(FlowThroughLmcServiceHandlerTest, get_monitor_data) // NOLINT
  {
    // configure beam
    PSTLOG_TRACE(this, "configuring beam");
    configure_beam();
    PSTLOG_TRACE(this, "beam configured");

    // configure
    PSTLOG_TRACE(this, "configuring scan");
    configure_scan();
    PSTLOG_TRACE(this, "scan configured");

    // scan
    PSTLOG_TRACE(this, "starting scan");
    start_scan();
    PSTLOG_TRACE(this, "scanning");

    PSTLOG_TRACE(this, "monitoring");
    ska::pst::lmc::MonitorData monitor_data;
    handler->get_monitor_data(&monitor_data);

    EXPECT_TRUE(monitor_data.has_dsp_flow_through());

    // See AT3-920 sleep to allow pipeline to do its job
    wait_for_pipeline_to_finish(_dsp);

    // end scan
    PSTLOG_TRACE(this, "ending scan");
    handler->stop_scan();
    PSTLOG_TRACE(this, "scan ended");

    PSTLOG_TRACE(this, "deconfiguring scan");
    handler->deconfigure_scan();
    PSTLOG_TRACE(this, "scan deconfigured");

    PSTLOG_TRACE(this, "deconfiguring beam");
    handler->deconfigure_beam();
    PSTLOG_TRACE(this, "beam deconfigured");
  }

  TEST_F(FlowThroughLmcServiceHandlerTest, get_monitor_data_when_not_scanning_should_throw_exception) // NOLINT
  {
    PSTLOG_TRACE(this, "end scan");
    EXPECT_FALSE(handler->is_scanning());

    try
    {
      ska::pst::lmc::MonitorData monitor_data;
      handler->get_monitor_data(&monitor_data);
      FAIL() << " expected get_monitor_data to throw exception due to not currently scanning.\n";
    }
    catch (ska::pst::common::LmcServiceException &ex)
    {
      EXPECT_EQ(std::string(ex.what()), "Received get_monitor_data request when DSP.FT is not scanning.");
      EXPECT_EQ(ex.error_code(), ska::pst::lmc::ErrorCode::NOT_SCANNING);
      EXPECT_EQ(ex.status_code(), grpc::StatusCode::FAILED_PRECONDITION);
    }
  }

  TEST_F(FlowThroughLmcServiceHandlerTest, go_to_runtime_error) // NOLINT
  {
    PSTLOG_TRACE(this, "starting");

    try
    {
      throw std::runtime_error("this is a test error");
    }
    catch (...)
    {
      handler->go_to_runtime_error(std::current_exception());
    }

    ASSERT_EQ(ska::pst::common::RuntimeError, _dsp->get_state());
    ASSERT_EQ(ska::pst::common::RuntimeError, handler->get_application_manager_state());

    try
    {
      if (handler->get_application_manager_exception())
      {
        std::rethrow_exception(handler->get_application_manager_exception());
      }
      else
      {
        // the exception should be set and not null
        ASSERT_FALSE(true);
      }
    }
    catch (const std::exception &exc)
    {
      ASSERT_EQ("this is a test error", std::string(exc.what()));
    }
  }

  TEST_F(FlowThroughLmcServiceHandlerTest, reset_non_runtime_error_state) // NOLINT
  {
    PSTLOG_TRACE(this, "configuring beam");
    configure_beam();
    PSTLOG_TRACE(this, "beam configured");

    PSTLOG_TRACE(this, "configuring scan");
    EXPECT_FALSE(handler->is_scan_configured());
    configure_scan();
    EXPECT_TRUE(handler->is_scan_configured());
    PSTLOG_TRACE(this, "scan configured");

    PSTLOG_TRACE(this, "resetting");
    handler->reset();
    EXPECT_TRUE(handler->is_scan_configured());
    EXPECT_TRUE(handler->is_beam_configured());

    EXPECT_EQ(ska::pst::common::State::ScanConfigured, handler->get_application_manager_state());
    PSTLOG_TRACE(this, "reset");
  }

  TEST_F(FlowThroughLmcServiceHandlerTest, reset_from_runtime_error_state) // NOLINT
  {
    PSTLOG_TRACE(this, "configuring beam");
    configure_beam();
    PSTLOG_TRACE(this, "beam configured");

    PSTLOG_TRACE(this, "configuring scan");
    EXPECT_FALSE(handler->is_scan_configured());
    configure_scan();
    EXPECT_TRUE(handler->is_scan_configured());
    PSTLOG_TRACE(this, "scan configured");

    try
    {
      throw std::runtime_error("force fault state");
    }
    catch (...)
    {
      handler->go_to_runtime_error(std::current_exception());
    }

    PSTLOG_TRACE(this, "resetting");
    handler->reset();
    EXPECT_FALSE(handler->is_scan_configured());
    EXPECT_FALSE(handler->is_beam_configured());

    EXPECT_EQ(ska::pst::common::State::Idle, handler->get_application_manager_state());
    PSTLOG_TRACE(this, "reset");
  }

  TEST_F(FlowThroughLmcServiceHandlerTest, restart_from_beam_configured) // NOLINT
  {
    GTEST_FLAG_SET(death_test_style, "threadsafe");
    PSTLOG_INFO(this, "restart");
    // Assert that calling restart will eventually call exit(1)
    ASSERT_DEATH(
        {
            configure_beam();
            handler->restart();  // Call the restart method
        },".*"  // Optional: Regex to match any output from the process (can be empty)
    );
    PSTLOG_INFO(this, "restarted");
    EXPECT_FALSE(handler->is_beam_configured());
  }

  TEST_F(FlowThroughLmcServiceHandlerTest, restart_from_scan_configured) // NOLINT
  {
    GTEST_FLAG_SET(death_test_style, "threadsafe");
    PSTLOG_INFO(this, "restart");
    // Assert that calling restart will eventually call exit(1)
    ASSERT_DEATH(
        {
            configure_beam();
            configure_scan();
            handler->restart();  // Call the restart method
        },".*"  // Optional: Regex to match any output from the process (can be empty)
    );
    PSTLOG_INFO(this, "restarted");
    EXPECT_FALSE(handler->is_scan_configured());
    EXPECT_FALSE(handler->is_beam_configured());
  }

  TEST_F(FlowThroughLmcServiceHandlerTest, restart_from_scanning) // NOLINT
  {
    GTEST_FLAG_SET(death_test_style, "threadsafe");
    PSTLOG_INFO(this, "restart");
    // Assert that calling restart will eventually call exit(1)
    ASSERT_DEATH(
        {
            configure_beam();
            configure_scan();
            start_scan(true); //NOLINT
            handler->restart();  // Call the restart method
        },".*"  // Optional: Regex to match any output from the process (can be empty)
    );
    PSTLOG_INFO(this, "restarted");
    EXPECT_FALSE(handler->is_scanning());
    EXPECT_FALSE(handler->is_scan_configured());
    EXPECT_FALSE(handler->is_beam_configured());
  }

  TEST_F(FlowThroughLmcServiceHandlerTest, restart_from_runtime_error_state) // NOLINT
  {
    GTEST_FLAG_SET(death_test_style, "threadsafe");
    PSTLOG_INFO(this, "restart");
    // Assert that calling restart will eventually call exit(1)
    ASSERT_DEATH(
        {
            configure_beam();
            configure_scan();

            // induce error
            try {
                throw std::runtime_error("force fault state");
            } catch (...) {
                handler->go_to_runtime_error(std::current_exception());
            }
            handler->restart();  // Call the restart method
        },".*"  // Optional: Regex to match any output from the process (can be empty)
    );
    PSTLOG_INFO(this, "restarted");
    EXPECT_FALSE(handler->is_scan_configured());
    EXPECT_FALSE(handler->is_beam_configured());
  }

} // namespace ska::pst::dsp::test
