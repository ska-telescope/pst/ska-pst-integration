/*
 * Copyright 2024 Square Kilometre Array Observatory
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 * contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
#if HAVE_CONFIG_H
#include <config.h>
#endif

#include "ska_pst/dsp/ft/FlowThroughManager.h"
#include "ska_pst/dsp/disk/StreamWriter.h"
#include "ska_pst/common/definitions.h"
#include "ska_pst/common/utils/AsciiHeader.h"
#include "ska_pst/common/utils/Logging.h"

#include <dsp/ASCIIObservation.h>
#include <dsp/Operation.h>

#include <dsp/DualFile.h>
#include <dsp/SKAParallelUnpacker.h>
#include <dsp/ParallelIOManager.h>

#include <sstream>
#include <iomanip>

static constexpr int OUTPUT_RB_KEY_OFFSET = 4;

static const std::string only_pol0 = "A";
static const std::string only_pol1 = "B";
static const std::string both_pols = "Both";

auto pols_to_pol_range(const std::string &pols) -> std::string;
auto chans_to_chan_range(const std::pair<int32_t, int32_t> &chans) -> std::string;

ska::pst::dsp::FlowThroughManager::FlowThroughManager(const std::string& base_path, bool use_o_direct) :
  ska::pst::common::ApplicationManager("FlowThroughManager"),
  disk_monitor(base_path),
  recording_base_path(base_path),
  o_direct(use_o_direct)
{
  PSTLOG_TRACE(this, "ctor base_path={} o_direct={}", base_path, o_direct);
  initialise();
}

ska::pst::dsp::FlowThroughManager::~FlowThroughManager()
{
  PSTLOG_TRACE(this, "dtor quit()");
  quit();
}

void ska::pst::dsp::FlowThroughManager::perform_initialise()
{
  PSTLOG_TRACE(this, "performing initialise");
  output_data_stream = std::make_unique<ska::pst::dsp::StreamWriter>(disk_monitor, "output_data_stream");
  output_data_stream->set_skip_datablock_read_config();
  output_weights_stream = std::make_unique<ska::pst::dsp::StreamWriter>(disk_monitor, "output_weights_stream");
  output_weights_stream->set_skip_datablock_read_config();
}

void ska::pst::dsp::FlowThroughManager::configure_from_file(const std::string &config_file)
{
  PSTLOG_DEBUG(this, "loading configuration from config_file={}", config_file);
  ska::pst::common::AsciiHeader config;
  config.load_from_file(config_file);
  PSTLOG_TRACE(this, "config after loading from file: config={}", config.raw());

  // configure beam
  ska::pst::common::AsciiHeader beam_config;
  for (auto & key : required_beam_config_keys)
  {
    beam_config.set_val(key, config.get_val(key));
  }
  configure_beam(beam_config);

  // configure scan
  ska::pst::common::AsciiHeader scan_config;
  for (auto & key : required_scan_config_keys)
  {
    scan_config.set_val(key, config.get_val(key));
  }
  scan_config.set("BYTES_PER_SECOND", config.compute_bytes_per_second());
  configure_scan(scan_config);

  // configure from file
  ska::pst::common::AsciiHeader start_scan_config;
  for (auto & key : required_startscan_config_keys)
  {
    start_scan_config.set_val(key, config.get_val(key));
  }
  start_scan(start_scan_config);
}

void ska::pst::dsp::FlowThroughManager::validate_configure_beam(const ska::pst::common::AsciiHeader& config, ska::pst::common::ValidationContext *context)
{
  PSTLOG_TRACE(this, "validating beam configuration: config={}", config.raw());
  disk_monitor.validate_configure_beam(config, context);

  // Iterate through the vector and validate existence of required beam configuration keys
  for (const std::string& config_key : required_beam_config_keys)
  {
    if (config.has(config_key))
    {
      PSTLOG_DEBUG(this, "beam config: {}={}", config_key, config.get_val(config_key));
    } else {
      context->add_missing_field_error(config_key);
    }
  }

  // validate input data key
  try {
    input_data_key = config.get_val("DATA_KEY");
    ska::pst::smrb::DataBlock::parse_psrdada_key(input_data_key);
  }
  catch (std::runtime_error& exc)
  {
    PSTLOG_ERROR(this, "beam configuration input_data_key={} was invalid", input_data_key);
    context->add_validation_error("DATA_KEY", input_data_key, "invalid PSRDADA key");
  }

  // validate input weights key
  try {
    input_weights_key = config.get_val("WEIGHTS_KEY");
    ska::pst::smrb::DataBlock::parse_psrdada_key(input_weights_key);
  }
  catch (std::runtime_error& exc)
  {
    PSTLOG_ERROR(this, "beam configuration input_weights_key={} was invalid", input_weights_key);
    context->add_validation_error("WEIGHTS_KEY", input_weights_key, "invalid PSRDADA key");
  }

  // validate output data configuration
  try {
    output_data_key = generate_output_key(input_data_key);

    // don't assign to the instance variable - this helps having to avoid rolling back when validation errors occur
    ska::pst::common::AsciiHeader _output_data_beam_config;
    _output_data_beam_config.set_val("KEY", output_data_key);
    _output_data_beam_config.set_val("RECORDING_BASE_PATH", recording_base_path);
    _output_data_beam_config.set_val("RECORDING_SUFFIX", "data");
    output_data_stream->validate_configure_beam(_output_data_beam_config, context);
  }
  catch (std::runtime_error& exc)
  {
    PSTLOG_ERROR(this, "beam configuration output_data_key={} was invalid", output_data_key);
    output_data_beam_config.reset();
    context->add_validation_error("output_data_key", output_data_key, "invalid OUTPUT key");
  }

  // validate output weights configuration
  try {
    // auto input_weights_db_key = ska::pst::smrb::DataBlock::parse_psrdada_key(input_weights_key);
    // auto output_weights_db_key = input_weights_db_key + OUTPUT_RB_KEY_OFFSET;
    // need to convert this back to a string
    // std::ostringstream oss;
    // oss << std::hex << std::setw(4) << std::setfill('0') << output_weights_db_key;
    output_weights_key = generate_output_key(input_weights_key);

    // don't assign to the instance variable - this helps having to avoid rolling back when validation errors occur
    ska::pst::common::AsciiHeader _output_weights_beam_config;
    _output_weights_beam_config.set_val("KEY", output_weights_key);
    _output_weights_beam_config.set_val("RECORDING_BASE_PATH", recording_base_path);
    _output_weights_beam_config.set_val("RECORDING_SUFFIX", "weights");
    output_weights_stream->validate_configure_beam(_output_weights_beam_config, context);
  }
  catch (std::runtime_error& exc)
  {
    PSTLOG_ERROR(this, "beam configuration output_weights_key={} was invalid", output_weights_key);
    output_weights_beam_config.reset();
    context->add_validation_error("output_weights_key", output_weights_key, "invalid OUTPUT key");
  }

  PSTLOG_TRACE(this, "validation of beam configuration complete");
}

void ska::pst::dsp::FlowThroughManager::validate_configure_scan(const ska::pst::common::AsciiHeader& config, ska::pst::common::ValidationContext *context)
{
  disk_monitor.validate_configure_scan(config, context);
  output_data_stream->validate_configure_scan(config, context);
  output_weights_stream->validate_configure_scan(config, context);

  // Iterate through the vector and validate existence of required data header keys
  for (const std::string& config_key : required_scan_config_keys)
  {
    if (config.has(config_key))
    {
      PSTLOG_DEBUG(this, "scan config: {}={}", config_key, config.get_val(config_key));
    } else {
      context->add_missing_field_error(config_key);
    }
  }

  if (config.has("NBIT_OUT"))
  {
    num_bits_out = config.get<uint32_t>("NBIT_OUT");
    if (!context->test_value_list_presence(num_bits_out, valid_nbit_out))
    {
      context->add_value_list_error("NBIT_OUT", num_bits_out, valid_nbit_out);
    }
  }

  if (config.has("POLN_FT"))
  {
    polarisations = config.get_val("POLN_FT");
    if (!context->test_value_list_presence(polarisations, valid_polarisations))
    {
      context->add_value_list_error("POLN_FT", polarisations, valid_polarisations);
    }
  }

  if (config.has("CHAN_FT"))
  {
    std::string chan_ft = config.get_val("CHAN_FT");
    std::istringstream iss(chan_ft);
    std::string first{"-1"};
    std::string second{"-1"};
    try {
      std::getline(iss, first, ',');
      std::getline(iss, second);
      channels = std::make_pair(std::stoi(first), std::stoi(second));
    } catch (const std::exception& exc) {
      PSTLOG_WARN(this, "could not parse CHAN_FT={} as list of 2 integers", chan_ft, exc.what());
      context->add_validation_error("CHAN_FT", chan_ft, "not a comma separated list of 2 integers");
    }
    if (channels.first == -1)
    {
      context->add_validation_error("CHAN_FT", chan_ft, "first value was not a valid integer");
    }
    if (channels.second == -1)
    {
      context->add_validation_error("CHAN_FT", chan_ft, "second value was not a valid integer");
    }
  }

  if (config.has("RESCALE_ALGORITHM"))
  {
    std::string rescale_algorithm = config.get_val("RESCALE_ALGORITHM");
    if ((rescale_algorithm != "MeanStddev") && (rescale_algorithm != "MedianMad"))
    {
      context->add_validation_error("RESCALE_ALGORITHM", rescale_algorithm, "algorithm was not MeanStddev or MedianMad");
    }
  }

  PSTLOG_TRACE(this, "scan configuration validation complete");
}

void ska::pst::dsp::FlowThroughManager::validate_start_scan(const ska::pst::common::AsciiHeader& config)
{
  // Iterate through the vector and validate existence of required keys
  for (const std::string& config_key : required_startscan_config_keys)
  {
    if (config.has(config_key))
    {
      PSTLOG_DEBUG(this, "start scan: {}={}", config_key, config.get_val(config_key));
    } else {
      throw std::runtime_error("required field " + config_key + " missing in start scan configuration");
    }
  }
  PSTLOG_TRACE(this, "start scan validation complete");
}

void ska::pst::dsp::FlowThroughManager::perform_configure_beam()
{
  PSTLOG_TRACE(this, "beam_config={}", beam_config.raw());

  enforce(!is_beam_configured(), "FlowThroughManager perform_configure_beam cannot be called when beam already configured");

  // ska::pst::common::ApplicationManager has written beam configuration parameters to beam_config
  input_data_key = beam_config.get_val("DATA_KEY");
  input_weights_key = beam_config.get_val("WEIGHTS_KEY");

  // connection timeout is optional
  timeout = 0;
  if (beam_config.has("TIMEOUT"))
  {
    timeout = beam_config.get<int32_t>("TIMEOUT");
  }

  input_data_db = std::make_unique<ska::pst::smrb::DataBlockView>(input_data_key);
  input_weights_db = std::make_unique<ska::pst::smrb::DataBlockView>(input_weights_key);

  PSTLOG_DEBUG(this, "connecting to data db, timeout={}", timeout);
  input_data_db->connect(timeout);
  PSTLOG_DEBUG(this, "connecting to weights db, timeout={}", timeout);
  input_weights_db->connect(timeout);

  PSTLOG_DEBUG(this, "locking read access to data db");
  input_data_db->lock();
  PSTLOG_DEBUG(this, "locking read access to weights db");
  input_weights_db->lock();

  output_data_key = generate_output_key(input_data_key);
  PSTLOG_DEBUG(this, "output_data_key={}", output_data_key);

  output_weights_key = generate_output_key(input_weights_key);
  PSTLOG_DEBUG(this, "output_weights_key={}", output_weights_key);

  // TODO(future): update the size of the headers, data and number of buffers
  output_data_db_create = std::make_unique<ska::pst::smrb::DataBlockCreate>(output_data_key);
  output_data_db_create->create(input_data_db->get_header_nbufs(), input_data_db->get_header_bufsz());

  output_weights_db_create = std::make_unique<ska::pst::smrb::DataBlockCreate>(output_weights_key);
  output_weights_db_create->create(input_weights_db->get_header_nbufs(), input_weights_db->get_header_bufsz());

  try
  {
    PSTLOG_TRACE(this, "new DADABuffer with input_data_key={}", input_data_key);
    input_data_block = new ::dsp::DADABuffer; // NOLINT
    input_data_block->open_key(input_data_key);

    PSTLOG_TRACE(this, "new OutputDADABuffer with output_data_key={}", output_data_key);
    output_data_block = new ::dsp::OutputDADABuffer(output_data_key); // NOLINT

    PSTLOG_TRACE(this, "new DADABuffer with input_weights_key={}", input_weights_key);
    input_weights_block = new ::dsp::DADABuffer; // NOLINT
    input_weights_block->open_key(input_weights_key);

    PSTLOG_TRACE(this, "new OutputDADABuffer with output_weights_key={}", output_weights_key);
    output_weights_block = new ::dsp::OutputDADABuffer(output_weights_key); // NOLINT
  }
  catch (const Error &error)
  {
    PSTLOG_ERROR(this, "DSPSR raised an error: {}", error.get_message());
    handle_dspsr_error(error);
  }

  disk_monitor.configure_beam(beam_config);
  try
  {
    PSTLOG_TRACE(this, "output_data_beam_config={}", output_data_beam_config.raw());
    output_data_beam_config.set_val("KEY", output_data_key);
    output_data_beam_config.set_val("RECORDING_BASE_PATH", recording_base_path);
    output_data_beam_config.set_val("RECORDING_SUFFIX", "data");
    output_data_stream->configure_beam(output_data_beam_config);
  }
  catch (const std::exception &e)
  {
    PSTLOG_WARN(this, "exception raised during output_data_stream->configure_beam(output_data_beam_config): {}", e.what());
    output_data_beam_config.reset();
    disk_monitor.deconfigure_beam();
    throw(e);
  }

  try
  {
    PSTLOG_TRACE(this, "output_weights_beam_config={}", output_weights_beam_config.raw());
    output_weights_beam_config.set_val("KEY", output_weights_key);
    output_weights_beam_config.set_val("RECORDING_BASE_PATH", recording_base_path);
    output_weights_beam_config.set_val("RECORDING_SUFFIX", "weights");
    output_weights_stream->configure_beam(output_weights_beam_config);
  }
  catch (const std::exception &e)
  {
    PSTLOG_WARN(this, "exception raised during output_weights_beam_config->configure_beam(output_weights_beam_config): {}", e.what());
    output_weights_beam_config.reset();
    disk_monitor.deconfigure_beam();
    throw(e);
  }

  PSTLOG_TRACE(this, "configure beam complete");
}

void ska::pst::dsp::FlowThroughManager::construct_dspsr_pipeline() try
{
  input_data_block->reopen();
  input_weights_block->reopen();
  output_data_block->reopen();
  output_weights_block->reopen();

  PSTLOG_TRACE(this, "constructing a DSPSR DualFile for pipeline");
  Reference::To<::dsp::DualFile> dual_input = new ::dsp::DualFile;
  PSTLOG_TRACE(this, "setting input_data_block and input_weights_block as inputs to dual_input file");
  dual_input->set_inputs(input_data_block, input_weights_block);

  PSTLOG_TRACE(this, "creating a SKAParallelUnpacker for pipeline");
  Reference::To<::dsp::SKAParallelUnpacker> unpacker = new ::dsp::SKAParallelUnpacker;

  PSTLOG_TRACE(this, "creating a ParallelIOManager for pipeline");
  Reference::To<::dsp::ParallelIOManager> manager = new ::dsp::ParallelIOManager;
  PSTLOG_TRACE(this, "setting dual_input file as input on manager");
  manager->set_input(dual_input);
  PSTLOG_TRACE(this, "setting parallel unpacker as manager's unpacker");
  manager->set_unpacker(unpacker);

  auto dada_header = dynamic_cast<::dsp::ASCIIObservation*>(manager->get_info());
  if (dada_header)
  {
    unpacker->configure(dada_header);
    output_data_block->get_header()->set_header(dada_header->get_header());
    // required by DataBlockView in FlowThroughManagerTest.test_output_files_exists
    output_weights_block->get_header()->set_header(dada_header->get_header());
  }

  pipeline_config.pol_range = pols_to_pol_range(polarisations);
  pipeline_config.channel_range = chans_to_chan_range(channels);
  pipeline_config.output_nbit = num_bits_out;
  pipeline_config.use_median_mad = (rescale_algorithm == "MedianMad");
  pipeline_config.rescale_constant = true;

  if (device_id >= 0)
  {
    pipeline_config.set_cuda_device(std::to_string(device_id));
  }

  // set the maximum memory to use to be equivalent to 50% of the ring bufferelements
  // noting this size should be less than the total ring buffer size
  uint64_t bufsz = input_data_db->get_data_bufsz();
  uint64_t nbufsz = std::max(static_cast<uint64_t>(2), input_data_db->get_data_nbufs()/2);
  uint64_t max_ram_bytes = bufsz * nbufsz;
  PSTLOG_DEBUG(this, "configuring pipeline maximum RAM size={} bytes", max_ram_bytes);
  pipeline_config.set_maximum_RAM(max_ram_bytes);

  PSTLOG_TRACE(this, "constructing LoadToQuantize");
  pipeline = std::make_unique<::dsp::LoadToQuantize>(output_data_block, &pipeline_config);
  pipeline->set_output_weights(output_weights_block);
  pipeline->set_source(manager);

  if (spdlog::should_log(spdlog::level::trace))
  {
    ::dsp::set_verbosity (3);
  }

  // specifically catch the error here so we know what line it occurred on
  try {
    pipeline->construct();
  } catch (const Error &error) {
    PSTLOG_ERROR(this, "DSPSR raised an error: {}", error.get_message());
    throw std::runtime_error(error.get_message());
  }

  PSTLOG_INFO(this, "pipeline_construct return");
}
catch (const Error& error)
{
  PSTLOG_ERROR(this, "DSPSR raised an error: {}", error.get_message());
  throw std::runtime_error(error.get_message());
}

auto ska::pst::dsp::FlowThroughManager::get_dspsr_pipeline() const -> const ::dsp::LoadToQuantize*
{
  return pipeline.get();
}

void ska::pst::dsp::FlowThroughManager::perform_configure_scan()
{
  PSTLOG_TRACE(this, "scan_config=\n{}", scan_config.raw());

  enforce(!is_scan_configured(), "FlowThroughManager perform_configure_scan cannot be called when beam already configured");

  // get the configuration parameters for this scan
  num_bits_out = scan_config.get<uint32_t>("NBIT_OUT");
  polarisations = scan_config.get_val("POLN_FT");
  std::istringstream iss(scan_config.get_val("CHAN_FT"));
  std::string first, second;
  std::getline(iss, first, ',');
  std::getline(iss, second);
  channels = std::make_pair(std::stoi(first), std::stoi(second));
  requantisation_scale = scan_config.get<float>("DIGITIZER_SCALE");
  requantisation_init_time = scan_config.get<uint32_t>("DIGITIZER_INIT_TIME");

  // default rescaling algorithm is MeanStddev
  rescale_algorithm = "MeanStddev";
  if (scan_config.has("RESCALE_ALGORITHM"))
  {
    rescale_algorithm = scan_config.get_val("RESCALE_ALGORITHM");
  }

  PSTLOG_DEBUG(this, "read the config from the data DB");
  input_data_db->read_config();
  PSTLOG_DEBUG(this, "read the config from the weights DB");
  input_weights_db->read_config();

  try
  {
    input_data_block->load_header();
  }
  catch (const Error &error)
  {
    PSTLOG_ERROR(this, "DSPSR raised error while loading header of data block: {}", error.get_message());
    handle_dspsr_error(error);
  }

  try
  {
    input_weights_block->load_header();
  }
  catch (const Error &error)
  {
    PSTLOG_ERROR(this, "DSPSR raised error while loading header of weights block: {}", error.get_message());
    handle_dspsr_error(error);
  }

  // read the data and weights config into AsciiHeaders
  data_config.load_from_str(input_data_db->get_config());
  weights_config.load_from_str(input_weights_db->get_config());

  const uint32_t npol_out = (polarisations == both_pols) ? 2 : 1;
  int32_t nchan_out = (channels.second - channels.first) + 1;
  auto ndim = data_config.get<uint32_t>("NDIM");
  auto resolution = nchan_out * num_bits_out * ndim * npol_out / ska::pst::common::bits_per_byte;

  PSTLOG_DEBUG(this, "resolution={}", resolution);

  ska::pst::common::AsciiHeader _scan_config;
  _scan_config.clone(data_config);
  _scan_config.append_header(scan_config);
  _scan_config.set("NBIT", num_bits_out);
  _scan_config.set("NPOL", npol_out);
  _scan_config.set("NCHAN", nchan_out);
  _scan_config.set("RESOLUTION", resolution);
  _scan_config.set("POLN_FT", polarisations);
  _scan_config.set("CHAN_FT", scan_config.get_val("CHAN_FT"));
  _scan_config.set("RESCALE_ALGORITHM", rescale_algorithm);
  disk_monitor.configure_scan(_scan_config);

  output_data_stream->configure_scan(_scan_config);
  output_data_stream->set_extra_headers(_scan_config);

  output_weights_stream->configure_scan(_scan_config);
  output_weights_stream->set_extra_headers(_scan_config);

  PSTLOG_DEBUG(this, "configure scan complete");
}

/*!
  converts the polarisation string used to configure FlowThroughManager
  to the polarisation range string used to configure LoadToQuantize
*/
auto pols_to_pol_range (const std::string& pols) -> std::string
{
  if (pols == only_pol0) { return "0:0"; }
  if (pols == only_pol1) { return "1:1"; }
  if (pols == both_pols) { return "0:1"; }

  // in this case, LoadToQuantize defaults to both polarisations
  return "";
}

/*!
  converts the channel indeces used to configure FlowThroughManager
  to the channel range string used to configure LoadToQuantize
*/
auto chans_to_chan_range (const std::pair<int32_t, int32_t>& chans) -> std::string
{
  // in this case, LoadToQuantize defaults to the entire band
  if (chans.first == -1 && chans.second == -1) { return ""; }

  return std::to_string(chans.first) + ":" + std::to_string(chans.second);
}

void ska::pst::dsp::FlowThroughManager::perform_start_scan()
{
  PSTLOG_DEBUG(this, "starting scan");
  disk_monitor.start_scan(startscan_config);
}

void ska::pst::dsp::FlowThroughManager::perform_scan()
{
  PSTLOG_DEBUG(this, "performing scan");

  // wait for the header to be written to the db
  PSTLOG_DEBUG(this, "reading headers from data and weights DBs");
  input_data_db->read_header();
  input_weights_db->read_header();

  PSTLOG_DEBUG(this, "loading headers from data and weights DBs");
  data_header.load_from_str(input_data_db->get_header());
  weights_header.load_from_str(input_weights_db->get_header());

  // Acquire the pipeline mutex lock and perform the construct and preparation of the pipeline while holding onto the the mutex lock
  //
  // This helps avoid a race condition when an end scan is performed before the pipeline is fully constructed
  // which was occurring (see https://jira.skatelescope.org/browse/AT3-843). Basically the reading of the
  // ring buffer headers block until data is received from the CBF, if no data was received but an end scan
  // was performed, RECV.CORE would close the ring buffers and a perform_stop_scan within DSP.FT could happen
  // while the pipeline was being constructed.
  //
  // This mutex is locked until the pipeline is ready to be run, at which stage the perform_stop_scan will
  // be able to get the lock and then safely shut down the pipeline, if it was running.
  {
    PSTLOG_TRACE(this, "waiting for scan_mutex lock");
    std::unique_lock<std::mutex> control_lock(scan_mutex);
    PSTLOG_TRACE(this, "scan_mutex lock acquired");

    // if a stop scan has been called then exit
    PSTLOG_DEBUG(this, "checking if still in a scanning state");
    if (get_state() != ska::pst::common::Scanning) {
      PSTLOG_DEBUG(this, "Exiting perform_scan as no longer in a scanning state before setting up pipeline. state={}", ska::pst::common::state_names[get_state()]);
      return;
    }

    PSTLOG_DEBUG(this, "checking header consistency for SCAN_ID");
    // check that the SCAN_ID and EB_ID in the header's match the scan configuration
    if (data_header.get_val("SCAN_ID") != startscan_config.get_val("SCAN_ID"))
    {
      PSTLOG_ERROR(this, "SCAN_ID mismatch between data header [{}] and startscan_config=[{}]", data_header.get_val("SCAN_ID"), startscan_config.get_val("SCAN_ID"));
      throw std::runtime_error("SCAN_ID mismatch between data header and startscan_config");
    }
    PSTLOG_DEBUG(this, "checking header consistency for EB_ID");
    if (data_header.get_val("EB_ID") != scan_config.get_val("EB_ID"))
    {
      PSTLOG_ERROR(this, "EB_ID mismatch between data header={} and startscan_config={}", data_header.get_val("EB_ID"), scan_config.get_val("EB_ID"));
      throw std::runtime_error("EB_ID mismatch between data header and startscan_config");
    }

    // this is used to avoid a race condition in tests where the scan config is reset
    // before the background output stream has had a chance to use the header
    PSTLOG_DEBUG(this, "starting output stream");
    auto output_data_stream_startscan_config = ska::pst::common::AsciiHeader(startscan_config);
    output_data_stream->start_scan(output_data_stream_startscan_config);
    auto output_weights_stream_startscan_config = ska::pst::common::AsciiHeader(startscan_config);
    output_weights_stream->start_scan(output_weights_stream_startscan_config);

    PSTLOG_DEBUG(this, "Constructing a DSPSR pipeline");
    // DSPSR error handling performed in this method
    construct_dspsr_pipeline();

    try
    {
      PSTLOG_DEBUG(this, "Preparing the DSPSR pipeline");
      pipeline->prepare();
    }
    catch (const Error &error)
    {
      PSTLOG_ERROR(this, "Exception during pipeline prepare: {}", error.get_message());
      handle_dspsr_error(error);
    }
  }

  try
  {
    PSTLOG_DEBUG(this, "Running DSPSR pipeline");
    pipeline_running = true;
    pipeline->run();
  }
  catch (const Error &error)
  {
    PSTLOG_ERROR(this, "Exception during pipeline run: {}", error.get_message());
    pipeline_running = false;
    handle_dspsr_error(error);
  }
  PSTLOG_DEBUG(this, "DSPSR pipeline stopped");
  pipeline_running = false;

  PSTLOG_DEBUG(this, "output_data_block->close()");
  output_data_block->close();
  PSTLOG_DEBUG(this, "output_weights_block->close()");
  output_weights_block->close();

  PSTLOG_DEBUG(this, "input_data_block->close()");
  input_data_block->close();
  PSTLOG_DEBUG(this, "input_weights_block->close()");
  input_weights_block->close();

  PSTLOG_DEBUG(this, "scan exiting normally");
}

void ska::pst::dsp::FlowThroughManager::perform_stop_scan()
{
  // Need to acquire the pipeline mutex lock so we can perform the stop scan safely
  std::unique_lock<std::mutex> control_lock(scan_mutex);

  PSTLOG_DEBUG(this, "stopping scan on output data stream");
  if (output_data_stream->is_scanning())
  {
    output_data_stream->stop_scan();
  }

  PSTLOG_DEBUG(this, "stopping scan on output weights stream");
  if (output_weights_stream->is_scanning())
  {
    output_weights_stream->stop_scan();
  }

  PSTLOG_DEBUG(this, "stopping scan on disk monitor");
  disk_monitor.stop_scan();

  if (pipeline) {
    PSTLOG_DEBUG(this, "calling pipeline->finish()");
    // pipeline can be nullptr if the pipeline wasn't constructed
    // which is the case if scan is stopped before any data has been received
    pipeline->finish();
  }
  PSTLOG_INFO(this, "DSPSR pipeline finished");

  PSTLOG_DEBUG(this, "clear headers of data DB");
  input_data_db->clear_header();
  PSTLOG_DEBUG(this, "clear headers of weights DB");
  input_weights_db->clear_header();

  startscan_config.reset();

  PSTLOG_DEBUG(this, "resetting pipeline");
  pipeline.reset();

  PSTLOG_TRACE(this, "stop scan complete");
}

void ska::pst::dsp::FlowThroughManager::perform_deconfigure_scan()
{
  PSTLOG_DEBUG(this, "calling deconfigure scan on output data stream");
  output_data_stream->deconfigure_scan();
  PSTLOG_DEBUG(this, "calling deconfigure scan on output weights stream");
  output_weights_stream->deconfigure_scan();
  PSTLOG_DEBUG(this, "calling deconfigure scan on disk monitor");
  disk_monitor.deconfigure_scan();

  PSTLOG_DEBUG(this, "resetting scan configuration");
  scan_config.reset();
}

void ska::pst::dsp::FlowThroughManager::perform_deconfigure_beam()
{
  output_data_stream->deconfigure_beam();
  delete output_data_block;
  output_data_block = nullptr;
  output_weights_stream->deconfigure_beam();
  delete output_weights_block;
  output_weights_block = nullptr;

  output_data_db_create->destroy();
  output_data_db_create.reset();
  output_data_key.clear();

  output_weights_db_create->destroy();
  output_weights_db_create.reset();
  output_weights_key.clear();

  PSTLOG_DEBUG(this, "unlocking data and weights ring buffers");
  input_data_db->unlock();
  input_weights_db->unlock();

  PSTLOG_DEBUG(this, "disconnecting from data and weights ring buffers");
  input_data_db->disconnect();
  input_weights_db->disconnect();

  PSTLOG_DEBUG(this, "disk_monitor.deconfigure_beam()");
  disk_monitor.deconfigure_beam();

  PSTLOG_DEBUG(this, "resetting data db");
  input_data_db.reset();
  PSTLOG_DEBUG(this, "resetting weights db");
  input_weights_db.reset();

  PSTLOG_DEBUG(this, "resetting configs");
  beam_config.reset();
  output_data_beam_config.reset();
  output_weights_beam_config.reset();

  PSTLOG_DEBUG(this, "delete input_data_block");
  delete input_data_block;
  input_data_block = nullptr;
  PSTLOG_DEBUG(this, "input_data_block deleted");

  PSTLOG_DEBUG(this, "delete input_weights_block");
  delete input_weights_block;
  input_weights_block = nullptr;
  PSTLOG_DEBUG(this, "input_weights_block deleted");
}

auto ska::pst::dsp::FlowThroughManager::get_beam_configuration() -> ska::pst::common::AsciiHeader &
{
  PSTLOG_TRACE(this, "getting beam configuration");
  enforce(is_beam_configured(), "cannot get beam configuration as the beam has not been configured");
  return beam_config;
}

auto ska::pst::dsp::FlowThroughManager::get_scan_configuration() -> ska::pst::common::AsciiHeader &
{
  PSTLOG_TRACE(this, "getting scan configuration");
  enforce(is_scan_configured(), "cannot get scan configuration when scan has not be configured");
  return scan_config;
}

auto ska::pst::dsp::FlowThroughManager::get_flow_through_stats() -> ska::pst::dsp::FlowThroughManager::stats_t
{
  ska::pst::dsp::FlowThroughManager::stats_t stats;

  stats.capacity = disk_monitor.get_disk_capacity();
  stats.available = disk_monitor.get_disk_available();

  if (is_scan_configured())
  {
    stats.bytes_written = disk_monitor.get_bytes_written();
    stats.expected_data_write_rate = disk_monitor.get_expected_data_write_rate();
  }
  if (is_scanning())
  {
    stats.data_write_rate = disk_monitor.get_data_write_rate();
  }

  return stats;
}

void ska::pst::dsp::FlowThroughManager::handle_dspsr_error(const Error &error)
{
  std::ostringstream buffer;
  error.report(buffer);
  throw std::runtime_error(buffer.str());
}

auto ska::pst::dsp::FlowThroughManager::generate_output_key(const std::string& input_key_str) -> std::string
{
  auto input_key = ska::pst::smrb::DataBlock::parse_psrdada_key(input_key_str);
  auto output_key = input_key + OUTPUT_RB_KEY_OFFSET;
  std::ostringstream oss;
  oss << std::hex << std::setw(4) << std::setfill('0') << output_key;
  std::string output_key_str = oss.str();
  return output_key_str;
}
