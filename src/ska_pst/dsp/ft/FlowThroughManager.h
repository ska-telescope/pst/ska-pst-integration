/*
 * Copyright 2024 Square Kilometre Array Observatory
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 * contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#include "ska_pst/common/statemodel/ApplicationManager.h"
#include "ska_pst/common/utils/AsciiHeader.h"
#include "ska_pst/common/utils/ValidationContext.h"
#include "ska_pst/dsp/disk/DiskMonitor.h"
#include "ska_pst/dsp/disk/StreamWriter.h"
#include "ska_pst/smrb/DataBlockCreate.h"
#include "ska_pst/smrb/DataBlockView.h"

#include <dsp/LoadToQuantize.h>
#include <dsp/DADABuffer.h>
#include <dsp/OutputDADABuffer.h>

#include <string>
#include <mutex>

#ifndef SKA_PST_DSP_FT_FlowThroughManager_h
#define SKA_PST_DSP_FT_FlowThroughManager_h

namespace ska::pst::dsp
{

  /**
   * @brief The FlowThroughManager combines instances of the DataBlockRead,
   * FlowThroughProcessor (TBC) and FileStreamWriter (TBC) classes to provide the
   * monitoring and control functions of the PST Flow Through processing mode.
   *
   * This class extended the ska::pst::common::ApplicationManager, implementing the required
   * commands to step through state model common to PST C++ applications. The class also
   * utilises the DiskMonitor class, providing monitoring of the file system to which output
   * data products are being written.
   *
   */
  class FlowThroughManager : public ska::pst::common::ApplicationManager
  {

    public:

      /**
       * @brief Statistics structure of the Flow Through app manager.
       *
       */
      typedef struct stats
      {
        //! Size of the recording disk in bytes
        uint64_t capacity;

        //! Bytes recorded to disk
        uint64_t bytes_written;

        //! Write rate to disk in bytes per second
        double data_write_rate;

        //! Expected write rate to disk in bytes per second
        double expected_data_write_rate;

        //! Available space on the recording disk in bytes
        uint64_t available;

      } stats_t;

      /**
       * @brief Construct a new Flow Through Manager object.
       *
       * @param base_path absolute path to the base directory where data products should be written.
       * @param use_o_direct use the O_DIRECT option when writing data product files.
       */
      FlowThroughManager(const std::string& base_path, bool use_o_direct);

      /**
       * @brief Destroy the Flow Through Manager object
       *
       */
      ~FlowThroughManager();

      /**
       * @brief Configure beam and scan as described by the configuration file
       *
       * @param config_file configuration file containing beam and scan configuration parameters
       */
      void configure_from_file(const std::string &config_file);

      /**
       * @brief Validate the beam configuration parameters, reporting any errors found.
       *
       * @param config beam configuration parameters, that must contain the beam_config_keys.
       * @param context A validation context where errors should be added.
       */
      void validate_configure_beam(const ska::pst::common::AsciiHeader& config, ska::pst::common::ValidationContext *context) override;

      /**
       * @brief Validate the scan configuration parameters, reporting any errors found.
       *
       * @param config scan configuration parameters, that must contain the scan_config_keys.
       * @param context A validation context where errors should be added.
       */
      void validate_configure_scan(const ska::pst::common::AsciiHeader& config, ska::pst::common::ValidationContext *context) override;

      /**
       * @brief Validate the start_scan configuration parameters, reporting any errors found.
       *
       * @param config Start Scan configuration parameters, that must contain the start_scan_config_keys.
       * @throw std::runtime_error if the required parameters are not present in the start_scan_config_keys.
       */
      void validate_start_scan(const ska::pst::common::AsciiHeader& config) override;

      /**
       * @brief Return the statistics of the Flow Through Manager.
       *
       * @return stats_t struct of the flow through manager statistics.
       */
      stats_t get_flow_through_stats();

      /**
       * @brief Get the beam configuration
       *
       * @return const ska::pst::common::AsciiHeader& header containing beam configuration.
       * @throw std::runtime_exception if the beam is not configured.
       */
      ska::pst::common::AsciiHeader& get_beam_configuration() override;

      /**
       * @brief Get the scan configuration.
       *
       * @return const ska::pst::common::AsciiHeader& header containing scan configuration.
       * @throw std::runtime_exception if the scan is not configured.
       */
      ska::pst::common::AsciiHeader& get_scan_configuration() override;

      /**
       * @brief Set the timeout to use during connection to DataBlocks.
       *
       * @param timeout_secs timeout to wait in seconds during DataBlock connections.
       */
      void set_timeout(int timeout_secs) { timeout = timeout_secs; }

      //! List of valid NBIT_OUT values
      const static inline std::vector<uint32_t> valid_nbit_out = {1, 2, 4, 8, 16, 32};

      //! List of valid NBIT_OUT values
      const static inline std::vector<std::string> valid_polarisations = {"A", "B", "Both"};

      //! List of mandatory beam config keys
      const static inline std::vector<std::string> required_beam_config_keys = {"DATA_KEY", "WEIGHTS_KEY"};

      //! List of mandatory scan config keys
      const static inline std::vector<std::string> required_scan_config_keys = {"SCANLEN_MAX", "EB_ID", "NBIT_OUT", "POLN_FT", "CHAN_FT", "DIGITIZER_SCALE", "DIGITIZER_INIT_TIME"};

      //! List of mandatory start scan config keys
      const static inline std::vector<std::string> required_startscan_config_keys = {"SCAN_ID"};

      //! The indices of the first and last (inclusive) frequency channels that define the single contiguous range of frequency channels to be recorded
      std::pair<int32_t, int32_t> get_channels() {
        return channels;
      }

      //! Return the number of channels being selected
      uint32_t get_nchan() const {
        return (channels.second - channels.first) + 1;
      }

      //! Return the number of polarisations being selected
      uint32_t get_npol() const {
        return (polarisations == "Both") ? 2 : 1;
      }

      //! helper function for getting the output data buffer key
      const std::string get_output_data_key() { return output_data_key; }

      //! helper function for getting the output weights buffer key
      const std::string get_output_weights_key() { return output_weights_key; }

      //! pass the cuda device id to the LoadToQuantize pipeline
      void assign_cuda_device(int _device_id) { device_id = _device_id; };

      //! return mapped diagnostic context key/value pairs
      const std::map<std::string, std::string>& get_log_context() const { return log_context; }

      /**
       * @brief check if DSPSR pipeline is running or not
       *
       * Returns true if the DSPSR pipeline running. The FlowThroughManager is considered
       * scanning until this returns true.
       */
      bool is_pipeline_running() const { return pipeline_running; }

      /**
       * @brief Helper function that produces an output key based on an input key
       *
       * @param input_key_str input key in a string format
       *
       * @return output key in a string format
       */
      std::string generate_output_key(const std::string& input_key_str);

      //! Return the pointer to the LoadToQuantize pipeline
      const ::dsp::LoadToQuantize* get_dspsr_pipeline() const;

      //! Return the pointer to the LoadToQuantize pipeline configuration
      ::dsp::LoadToQuantize::Config* get_dspsr_pipeline_config() { return &pipeline_config; }

    private:

      //! the implementation of the signal processing pipeline
      std::unique_ptr<::dsp::LoadToQuantize> pipeline{nullptr};

      //! the signal processing pipeline configuration
      ::dsp::LoadToQuantize::Config pipeline_config;

      //! input data DADABuffer shared memory object
      Reference::To<::dsp::DADABuffer> input_data_block;

      //! input weights DADABuffer shared memory object
      Reference::To<::dsp::DADABuffer> input_weights_block;

      //! output data OutputDADABuffer shared memory object
      Reference::To<::dsp::OutputDADABuffer> output_data_block;

      //! output weights OutputDADABuffer shared memory object
      Reference::To<::dsp::OutputDADABuffer> output_weights_block;

      //! shared memory key for the input data DataBlock
      std::string input_data_key{""};

      //! shared memory key of the input weights DataBlock
      std::string input_weights_key{""};

      //! shared memory key of output data DataBlock
      std::string output_data_key{""};

      //! shared memory key of output data DataBlock
      std::string output_weights_key{""};

      //! input data block to read from
      std::unique_ptr<ska::pst::smrb::DataBlockView> input_data_db{nullptr};

      //! input weights block to read from
      std::unique_ptr<ska::pst::smrb::DataBlockView> input_weights_db{nullptr};

      //! the output data ring buffer Create
      std::unique_ptr<ska::pst::smrb::DataBlockCreate> output_data_db_create{nullptr};

      //! the output weights ring buffer Create
      std::unique_ptr<ska::pst::smrb::DataBlockCreate> output_weights_db_create{nullptr};

      //! the output data StreamWriter beam configuration
      ska::pst::common::AsciiHeader output_data_beam_config;

      //! the output weights StreamWriter beam configuration
      ska::pst::common::AsciiHeader output_weights_beam_config;

      //! the output data StreamWriter
      std::unique_ptr<StreamWriter> output_data_stream{nullptr};

      //! the output weights StreamWriter
      std::unique_ptr<StreamWriter> output_weights_stream{nullptr};

      //! timeout, in seconds, to wait when attempting to connect to the DataBlockView object.
      int timeout{120};

      void perform_initialise() override;
      void perform_configure_beam() override;
      void perform_configure_scan() override;
      void perform_start_scan() override;
      void perform_scan() override;
      void perform_stop_scan() override;
      void perform_deconfigure_scan() override;
      void perform_deconfigure_beam() override;
      void perform_terminate() override { ; };

      //! Constructs the LoadToQuantize pipeline
      void construct_dspsr_pipeline();

      //! config provided on the data ring buffer
      ska::pst::common::AsciiHeader data_config;

      //! config provided on the weights ring buffer
      ska::pst::common::AsciiHeader weights_config;

      //! header provided on the data ring buffer
      ska::pst::common::AsciiHeader data_header;

      //! header provided on the weights ring buffer
      ska::pst::common::AsciiHeader weights_header;

      //! disk monitor
      DiskMonitor disk_monitor;

      //! base directory to where files will be written
      std::string recording_base_path;

      //! use O_DIRECT I/O techniques when writing data to file systems
      bool o_direct{false};

      //! The number of bits per output sample
      uint32_t num_bits_out{0};

      //! The polarisations to be recorded
      std::string polarisations{"Both"};

      //! The indices of the first and last (inclusive) frequency channels that define the single contiguous range of frequency channels to be recorded
      std::pair<int32_t, int32_t> channels = {-1, -1};

      //! Scale factor applied during re-quantisation that modifies the dynamic range of the fixed precision output
      float requantisation_scale = 0;

      //! Time interval spanned by data used at the start of a scan to determine the scale factors applied before re-quantisation
      uint32_t requantisation_init_time{0};

      //! Name of the algorithm used to compute the offsets and scales, default MeanStddev
      std::string rescale_algorithm{"MeanStddev"};

      //! The cuda device id
      int device_id = -1;

      //! mapped diagnostic context key/value pairs
      std::map<std::string, std::string> log_context = {{"entity","FlowThroughManager"}};

      //! indicator of if pipeline is running or not.
      bool pipeline_running{false};

      //! mutex to handle synchronisation issues between start / stop scan
      std::mutex scan_mutex;

      /**
       * @brief handle a DSPSR/PSRCHIVE error in a standard way.
       *
       * This will convert the Error into a std::runtime_error
       *
       * @throws std::runtime_error the error message is the full error, including function stack, of the error
       */
      void handle_dspsr_error(const Error &error);
  };

} // namespace ska::pst::dsp

#endif // SKA_PST_DSP_FT_FlowThroughManager_h
