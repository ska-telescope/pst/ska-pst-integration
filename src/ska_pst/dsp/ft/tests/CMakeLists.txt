
include_directories($<BUILD_INTERFACE:${PROJECT_SOURCE_DIR}/src>)

add_executable(FlowThroughManagerTest src/FlowThroughManagerTest.cpp)

set(
  TEST_LINK_LIBS
  ska-pst-dsp-disk
  ska-pst-dsp-ft
  ska-pst-smrb
  ska-pst-common
  ska-pst-common-testutils
  ska-pst-dsp-testutils
)

target_link_libraries(FlowThroughManagerTest ${TEST_LINK_LIBS})

add_test(FlowThroughManagerTest FlowThroughManagerTest --test_data "${CMAKE_CURRENT_LIST_DIR}/data")

set_tests_properties(FlowThroughManagerTest PROPERTIES TIMEOUT 30)
