/*
 * Copyright 2024 Square Kilometre Array Observatory
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 * contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#include "ska_pst/dsp/ft/tests/FlowThroughManagerTest.h"

#include "ska_pst/smrb/DataBlockView.h"
#include "ska_pst/common/definitions.h"
#include "ska_pst/common/utils/AsciiHeader.h"
#include "ska_pst/common/utils/FilePaths.h"
#include "ska_pst/common/utils/Timer.h"
#include "ska_pst/common/utils/Logging.h"

#include "ska_pst/common/testutils/GtestMain.h"
#include "ska_pst/dsp/testutils/dsp_testutils.h"

#include "dsp/NormalSampleStats.h"

#include "Estimate.h" // from PSRCHIVE, helps with error first-order propagation

#include <cuda_runtime.h>
#include <iostream>
#include <thread>
#include <chrono>

using ska::pst::common::test::test_data_file;

auto main(int argc, char *argv[]) -> int try
{
  return ska::pst::common::test::gtest_main(argc, argv);
}
catch (Error& error)
{
  std::cerr << "FlowThroughManagerTest main caught Error exception" << error << std::endl;
}
catch (std::exception &exc)
{
  PSTLOG_ERROR(nullptr, "FlowThroughManagerTest main caught std::exception: {}", exc.what());
}

namespace ska::pst::dsp::test
{

  void FlowThroughManagerTest::SetUp()
  {
    PSTLOG_TRACE(this, "creating flow through manager");
    ftm = std::make_shared<ska::pst::dsp::FlowThroughManager>(base_path, use_o_direct);

    setup_data_block();
  }

  void FlowThroughManagerTest::TearDown()
  {
    tear_down_data_block();
  }

  TEST_F(FlowThroughManagerTest, test_configure_from_file) // NOLINT
  {
    ASSERT_FALSE(ftm->is_beam_configured());
    PSTLOG_TRACE(this, "ftm->is_beam_configured()={}", ftm->is_beam_configured());
    ASSERT_FALSE(ftm->is_scan_configured());
    PSTLOG_TRACE(this, "ftm->is_scan_configured()={}", ftm->is_scan_configured());

    // simulate that RECV has sent the scan config and scan header
    write_config();
    write_data();

    ftm->configure_from_file(test_data_file("FlowThroughManager_config.txt"));
    ASSERT_TRUE(ftm->is_beam_configured());
    ASSERT_TRUE(ftm->is_scan_configured());
    ASSERT_TRUE(ftm->is_scanning());

    // See AT3-920 sleep to allow pipeline to do its job
    wait_for_pipeline_to_finish(ftm);

    ftm->stop_scan();
    ftm->deconfigure_scan();
    ftm->deconfigure_beam();
  }

  TEST_F(FlowThroughManagerTest, test_configure_beam) // NOLINT
  {
    ASSERT_THROW(ftm->get_beam_configuration(), std::runtime_error); // NOLINT
    PSTLOG_TRACE(this, "ftm->is_beam_configured()={}", ftm->is_beam_configured());
    ASSERT_FALSE(ftm->is_beam_configured());
    ASSERT_EQ(ftm->get_output_data_key(), "");

    // test expected good values for the beam configuration
    PSTLOG_TRACE(this, "data_key={}", beam_config.get_val("DATA_KEY"));
    PSTLOG_TRACE(this, "weights_key={}", beam_config.get_val("WEIGHTS_KEY"));
    ftm->configure_beam(beam_config);
    ASSERT_TRUE(ftm->is_beam_configured());
    ASSERT_NO_THROW(ftm->get_beam_configuration()); // NOLINT
    ASSERT_EQ(ftm->get_output_data_key(), beam_config.get_val("OUTPUT_DATA_KEY"));
    ASSERT_EQ(ftm->get_output_weights_key(), beam_config.get_val("OUTPUT_WEIGHTS_KEY"));

    auto output_data_db_key = beam_config.get_val("OUTPUT_DATA_KEY");
    std::unique_ptr<ska::pst::smrb::DataBlockView> output_data_db_view = std::make_unique<ska::pst::smrb::DataBlockView>(output_data_db_key);
    ASSERT_NO_THROW(output_data_db_view->connect(1));

    auto output_weights_db_key = beam_config.get_val("OUTPUT_WEIGHTS_KEY");
    std::unique_ptr<ska::pst::smrb::DataBlockView> output_weights_db_view = std::make_unique<ska::pst::smrb::DataBlockView>(output_weights_db_key);
    ASSERT_NO_THROW(output_weights_db_view->connect(1));

    output_weights_db_view->disconnect();
    output_data_db_view->disconnect();
  }

  TEST_F(FlowThroughManagerTest, test_configure_beam_validation) // NOLINT
  {
    ASSERT_FALSE(ftm->is_beam_configured());

    // test bad beam configuration values
    beam_config.reset();
    ASSERT_THROW(ftm->configure_beam(beam_config), ska::pst::common::pst_validation_error); // NOLINT
    beam_config.set_val("DATA_KEY", "junk");
    ASSERT_THROW(ftm->configure_beam(beam_config), ska::pst::common::pst_validation_error); // NOLINT
    beam_config.set_val("DATA_KEY", "a000");
    ASSERT_THROW(ftm->configure_beam(beam_config), ska::pst::common::pst_validation_error); // NOLINT
    beam_config.set_val("WEIGHTS_KEY", "junk");
    ASSERT_THROW(ftm->configure_beam(beam_config), ska::pst::common::pst_validation_error); // NOLINT
    beam_config.set_val("WEIGHTS_KEY", "a002");
    ASSERT_NO_THROW(ftm->configure_beam(beam_config)); // NOLINT
  }

  TEST_F(FlowThroughManagerTest, test_configure_scan) // NOLINT
  {
    PSTLOG_TRACE(this, "ftm->is_beam_configured()={}", ftm->is_beam_configured());
    PSTLOG_TRACE(this, "ftm->is_scan_configured()={}", ftm->is_scan_configured());
    ASSERT_FALSE(ftm->is_beam_configured());
    ftm->configure_beam(beam_config);
    ASSERT_THROW(ftm->get_scan_configuration(), std::runtime_error); // NOLINT
    PSTLOG_TRACE(this, "ftm->is_beam_configured()={}", ftm->is_beam_configured());
    ASSERT_TRUE(ftm->is_beam_configured());

    ASSERT_FALSE(ftm->is_scan_configured());
    PSTLOG_TRACE(this, "ftm->configure_scan()");
    write_config();
    ftm->configure_scan(scan_config);
    PSTLOG_TRACE(this, "ftm->is_scan_configured()={}", ftm->is_scan_configured());
    ASSERT_TRUE(ftm->is_scan_configured());
    ASSERT_NO_THROW(ftm->get_scan_configuration()); // NOLINT
  }

  TEST_F(FlowThroughManagerTest, test_configure_scan_validation) // NOLINT
  {
    // prepare the testing state
    ftm->configure_beam(beam_config);
    PSTLOG_TRACE(this, "ftm->is_beam_configured()={}", ftm->is_beam_configured());
    PSTLOG_TRACE(this, "ftm->is_scan_configured()={}", ftm->is_scan_configured());

    ASSERT_THROW(ftm->get_scan_configuration(), std::runtime_error); // NOLINT
    ASSERT_FALSE(ftm->is_scan_configured());

    // test a bad value of NBIT_OUT
    {
      ska::pst::common::ValidationContext context;
      scan_config.set("NBIT_OUT", 3);
      ftm->validate_configure_scan(scan_config, &context);
      ASSERT_FALSE(context.is_empty());
    }

    // test all the good values of NBIT_OUT
    {
      ska::pst::common::ValidationContext context;
      for (auto &val : ftm->valid_nbit_out)
      {
        scan_config.set("NBIT_OUT", val);
        ftm->validate_configure_scan(scan_config, &context);
        ASSERT_TRUE(context.is_empty());
      }
    }

    // test a bad value of POLN_FT
    {
      ska::pst::common::ValidationContext context;
      scan_config.set("POLN_FT", "None");
      ftm->validate_configure_scan(scan_config, &context);
      ASSERT_FALSE(context.is_empty());
    }

    // test the good values of POLN_FT
    {
      ska::pst::common::ValidationContext context;
      for (auto &val : ftm->valid_polarisations)
      {
        scan_config.set("POLN_FT", val);
        ftm->validate_configure_scan(scan_config, &context);
        ASSERT_TRUE(context.is_empty());
      }
    }

    // test bad values of CHAN_FT
    std::vector<std::string> bad_chan_fts = {"Nonlist", "a,b", "1"};
    for (auto &val : bad_chan_fts)
    {
      ska::pst::common::ValidationContext context;
      scan_config.set("CHAN_FT", val);
      ftm->validate_configure_scan(scan_config, &context);
      ASSERT_FALSE(context.is_empty());
    }

    // test good value of CHAN_FT
    {
      ska::pst::common::ValidationContext context;
      scan_config.set("CHAN_FT", "0,127");
      ftm->validate_configure_scan(scan_config, &context);
      ASSERT_TRUE(context.is_empty());
    }

    // test good values of optional key: RESCALE_ALGORITHM
    {
      ska::pst::common::ValidationContext context;
      std::vector<std::string> valid_rescale_algorithms = {"MedianMad", "MeanStddev"};
      for (auto & rescale_algorithm : valid_rescale_algorithms)
      {
        scan_config.set("RESCALE_ALGORITHM", rescale_algorithm);
        ftm->validate_configure_scan(scan_config, &context);
        ASSERT_TRUE(context.is_empty());
      }
    }

    // test invalid value of optional key: RESCALE_ALGORITHM
    {
      ska::pst::common::ValidationContext context;
      scan_config.set("RESCALE_ALGORITHM", "MeanAndMad");
      ftm->validate_configure_scan(scan_config, &context);
      ASSERT_FALSE(context.is_empty());
    }

    // test for validation failures if any scan_config_key value is missing
    for (auto &key : ftm->required_scan_config_keys)
    {
      ska::pst::common::AsciiHeader test_scan_config;
      test_scan_config.clone(scan_config);
      test_scan_config.del(key);
      ska::pst::common::ValidationContext context;
      PSTLOG_TRACE(this, "testing scan_config without {}", key);
      ftm->validate_configure_scan(test_scan_config, &context);
      ASSERT_FALSE(context.is_empty());
    }
  }

  TEST_F(FlowThroughManagerTest, test_deconfigure_beam) // NOLINT
  {
    PSTLOG_TRACE(this, "ftm->is_beam_configured()={}", ftm->is_beam_configured());
    ASSERT_FALSE(ftm->is_beam_configured());

    ftm->configure_beam(beam_config);
    ASSERT_TRUE(ftm->is_beam_configured());

    auto output_data_db_key = beam_config.get_val("OUTPUT_DATA_KEY");
    std::unique_ptr<ska::pst::smrb::DataBlockView> output_data_db_view = std::make_unique<ska::pst::smrb::DataBlockView>(output_data_db_key);
    ASSERT_NO_THROW(output_data_db_view->connect(1));
    output_data_db_view->disconnect();

    auto output_weights_db_key = beam_config.get_val("OUTPUT_WEIGHTS_KEY");
    std::unique_ptr<ska::pst::smrb::DataBlockView> output_weights_db_view = std::make_unique<ska::pst::smrb::DataBlockView>(output_weights_db_key);
    ASSERT_NO_THROW(output_weights_db_view->connect(1));
    output_weights_db_view->disconnect();

    PSTLOG_TRACE(this, "ftm->is_beam_configured()={}", ftm->is_beam_configured());
    ftm->deconfigure_beam();
    ASSERT_FALSE(ftm->is_beam_configured());
    ASSERT_EQ(ftm->get_output_data_key(), "");

    ASSERT_ANY_THROW(output_data_db_view->connect(1));
    ASSERT_ANY_THROW(output_weights_db_view->connect(1));
  }

  TEST_F(FlowThroughManagerTest, test_deconfigure_scan) // NOLINT
  {
    PSTLOG_TRACE(this, "ftm->is_beam_configured()={}", ftm->is_beam_configured());
    ASSERT_FALSE(ftm->is_beam_configured());

    ftm->configure_beam(beam_config);
    ASSERT_TRUE(ftm->is_beam_configured());

    write_config();
    ftm->configure_scan(scan_config);
    ASSERT_TRUE(ftm->is_scan_configured());

    ftm->deconfigure_scan();
    PSTLOG_TRACE(this, "ftm->is_scan_configured()={}", ftm->is_scan_configured());
    ASSERT_FALSE(ftm->is_scan_configured());

    ftm->deconfigure_beam();
    ASSERT_FALSE(ftm->is_beam_configured());
  }

  TEST_F(FlowThroughManagerTest, test_start_scan_validation) // NOLINT
  {
    ftm->configure_beam(beam_config);
    write_config();
    ftm->configure_scan(scan_config);
    PSTLOG_TRACE(this, "ftm->is_beam_configured()={}", ftm->is_beam_configured());
    PSTLOG_TRACE(this, "ftm->is_scan_configured()={}", ftm->is_scan_configured());

    ASSERT_TRUE(ftm->is_beam_configured());
    ASSERT_TRUE(ftm->is_scan_configured());
    ASSERT_FALSE(ftm->is_scanning());

    // test for validation failures if any scan_config_key value is missing
    for (auto &key : ftm->required_startscan_config_keys)
    {
      ska::pst::common::AsciiHeader test_start_scan_config;
      test_start_scan_config.clone(start_scan_config);
      test_start_scan_config.del(key);
      PSTLOG_TRACE(this, "testing start_scan_config without {}", key);
      ASSERT_THROW(ftm->validate_start_scan(test_start_scan_config), std::runtime_error); // NOLINT
    }
  }

  TEST_F(FlowThroughManagerTest, test_happy_path) // NOLINT
  {
    PSTLOG_INFO(this, "Configuring beam");

    auto empty_header = ska::pst::common::AsciiHeader();
    auto expected_beam_config = ska::pst::common::AsciiHeader(beam_config);

    auto expected_scan_config = ska::pst::common::AsciiHeader(expected_beam_config);
    expected_scan_config.append_header(scan_config);

    ASSERT_FALSE(ftm->is_beam_configured());
    ASSERT_FALSE(ftm->is_scan_configured());
    ASSERT_FALSE(ftm->is_scanning());
    ASSERT_THROW(ftm->get_beam_configuration(), std::runtime_error);
    ASSERT_THROW(ftm->get_scan_configuration(), std::runtime_error);
    ASSERT_EQ(empty_header, ftm->get_startscan_configuration());

    ftm->configure_beam(beam_config);
    ASSERT_TRUE(ftm->is_beam_configured());
    ASSERT_FALSE(ftm->is_scan_configured());
    ASSERT_FALSE(ftm->is_scanning());
    ASSERT_EQ(expected_beam_config, ftm->get_beam_configuration());
    ASSERT_THROW(ftm->get_scan_configuration(), std::runtime_error);
    ASSERT_EQ(empty_header, ftm->get_startscan_configuration());

    PSTLOG_INFO(this, "Configuring scan");
    ASSERT_FALSE(ftm->is_scan_configured());
    write_config();
    ftm->configure_scan(scan_config);
    ASSERT_TRUE(ftm->is_beam_configured());
    ASSERT_TRUE(ftm->is_scan_configured());
    ASSERT_FALSE(ftm->is_scanning());
    ASSERT_EQ(expected_beam_config, ftm->get_beam_configuration());
    ASSERT_EQ(expected_scan_config, ftm->get_scan_configuration());
    ASSERT_EQ(empty_header, ftm->get_startscan_configuration());

    PSTLOG_INFO(this, "Starting scan");
    ftm->start_scan(start_scan_config);
    ASSERT_TRUE(ftm->is_beam_configured());
    ASSERT_TRUE(ftm->is_scan_configured());
    ASSERT_TRUE(ftm->is_scanning());
    ASSERT_EQ(expected_beam_config, ftm->get_beam_configuration());
    ASSERT_EQ(expected_scan_config, ftm->get_scan_configuration());
    ASSERT_EQ(start_scan_config, ftm->get_startscan_configuration());

    PSTLOG_INFO(this, "Fill up output SMRB");
    PSTLOG_INFO(this, "Writing data");
    write_data();

    // See AT3-920 sleep to allow pipeline to do its job
    wait_for_pipeline_to_finish(ftm);

    ftm->get_flow_through_stats();
    ftm->stop_scan();
    ASSERT_TRUE(ftm->is_beam_configured());
    ASSERT_TRUE(ftm->is_scan_configured());
    ASSERT_FALSE(ftm->is_scanning());
    ASSERT_EQ(expected_beam_config, ftm->get_beam_configuration());
    ASSERT_EQ(expected_scan_config, ftm->get_scan_configuration());
    ASSERT_EQ(empty_header, ftm->get_startscan_configuration());

    ftm->deconfigure_scan();
    ASSERT_TRUE(ftm->is_beam_configured());
    ASSERT_FALSE(ftm->is_scan_configured());
    ASSERT_FALSE(ftm->is_scanning());
    ASSERT_EQ(expected_beam_config, ftm->get_beam_configuration());
    ASSERT_THROW(ftm->get_scan_configuration(), std::runtime_error);
    ASSERT_EQ(empty_header, ftm->get_startscan_configuration());

    ftm->deconfigure_beam();
    ASSERT_FALSE(ftm->is_beam_configured());
    ASSERT_FALSE(ftm->is_scan_configured());
    ASSERT_FALSE(ftm->is_scanning());
    ASSERT_THROW(ftm->get_beam_configuration(), std::runtime_error);
    ASSERT_THROW(ftm->get_scan_configuration(), std::runtime_error);
    ASSERT_EQ(empty_header, ftm->get_startscan_configuration());
  }

  TEST_P(FlowThroughManagerTest, test_no_data_received) // NOLINT
  {
    auto on_gpu = GetParam();
    if (on_gpu)
    {
      cudaGetDeviceCount(&deviceCount);
      if (deviceCount > 0)
      {
        ftm->assign_cuda_device(0);
      }
      else
      {
        GTEST_SKIP();
      }
    }

    PSTLOG_INFO(this, "Configuring beam");

    auto empty_header = ska::pst::common::AsciiHeader();
    auto expected_beam_config = ska::pst::common::AsciiHeader(beam_config);

    auto expected_scan_config = ska::pst::common::AsciiHeader(expected_beam_config);
    expected_scan_config.append_header(scan_config);

    ASSERT_FALSE(ftm->is_beam_configured());
    ASSERT_FALSE(ftm->is_scan_configured());
    ASSERT_FALSE(ftm->is_scanning());
    ASSERT_THROW(ftm->get_beam_configuration(), std::runtime_error);
    ASSERT_THROW(ftm->get_scan_configuration(), std::runtime_error);
    ASSERT_EQ(empty_header, ftm->get_startscan_configuration());

    ftm->configure_beam(beam_config);
    ASSERT_TRUE(ftm->is_beam_configured());
    ASSERT_FALSE(ftm->is_scan_configured());
    ASSERT_FALSE(ftm->is_scanning());
    ASSERT_EQ(expected_beam_config, ftm->get_beam_configuration());
    ASSERT_THROW(ftm->get_scan_configuration(), std::runtime_error);
    ASSERT_EQ(empty_header, ftm->get_startscan_configuration());

    PSTLOG_INFO(this, "Configuring scan");
    ASSERT_FALSE(ftm->is_scan_configured());
    write_config();
    ftm->configure_scan(scan_config);
    ASSERT_TRUE(ftm->is_beam_configured());
    ASSERT_TRUE(ftm->is_scan_configured());
    ASSERT_FALSE(ftm->is_scanning());
    ASSERT_EQ(expected_beam_config, ftm->get_beam_configuration());
    ASSERT_EQ(expected_scan_config, ftm->get_scan_configuration());
    ASSERT_EQ(empty_header, ftm->get_startscan_configuration());

    PSTLOG_INFO(this, "Starting scan");
    ftm->start_scan(start_scan_config);
    ASSERT_TRUE(ftm->is_beam_configured());
    ASSERT_TRUE(ftm->is_scan_configured());
    ASSERT_TRUE(ftm->is_scanning());
    ASSERT_EQ(expected_beam_config, ftm->get_beam_configuration());
    ASSERT_EQ(expected_scan_config, ftm->get_scan_configuration());
    ASSERT_EQ(start_scan_config, ftm->get_startscan_configuration());

    // Perform no writing of data.
    data_to_write.resize(0);
    weights_to_write.resize(0);
    write_data();

    // See AT3-920 sleep to allow pipeline to do its job
    wait_for_pipeline_to_finish(ftm);

    ftm->stop_scan();

    ASSERT_TRUE(ftm->is_beam_configured());
    ASSERT_TRUE(ftm->is_scan_configured());
    ASSERT_FALSE(ftm->is_scanning());
    ASSERT_EQ(expected_beam_config, ftm->get_beam_configuration());
    ASSERT_EQ(expected_scan_config, ftm->get_scan_configuration());
    ASSERT_EQ(empty_header, ftm->get_startscan_configuration());

    ftm->deconfigure_scan();
    ASSERT_TRUE(ftm->is_beam_configured());
    ASSERT_FALSE(ftm->is_scan_configured());
    ASSERT_FALSE(ftm->is_scanning());
    ASSERT_EQ(expected_beam_config, ftm->get_beam_configuration());
    ASSERT_THROW(ftm->get_scan_configuration(), std::runtime_error);
    ASSERT_EQ(empty_header, ftm->get_startscan_configuration());

    ftm->deconfigure_beam();
    ASSERT_FALSE(ftm->is_beam_configured());
    ASSERT_FALSE(ftm->is_scan_configured());
    ASSERT_FALSE(ftm->is_scanning());
    ASSERT_THROW(ftm->get_beam_configuration(), std::runtime_error);
    ASSERT_THROW(ftm->get_scan_configuration(), std::runtime_error);
    ASSERT_EQ(empty_header, ftm->get_startscan_configuration());
  }

  TEST_F(FlowThroughManagerTest, test_multiple_scans_with_beam_and_scan_reconfiguration) // NOLINT
  {
    auto scan_id = start_scan_config.get<int32_t>("SCAN_ID");

    for (auto i = 0; i < 2; i++)
    {
      // update the scan ID in the configs
      if (i > 0)
      {
        // Note that setup_data_block is called by ::Setup and therefore already done for the first loop
        setup_data_block();
      }

      auto curr_scan_id = scan_id + i;
      data_scan_config.set("SCAN_ID", curr_scan_id);
      weights_scan_config.set("SCAN_ID", curr_scan_id);
      start_scan_config.set("SCAN_ID", curr_scan_id);
      data_header.set("SCAN_ID", curr_scan_id);
      weights_header.set("SCAN_ID", curr_scan_id);

      ASSERT_FALSE(ftm->is_beam_configured());
      ftm->configure_beam(beam_config);
      ASSERT_TRUE(ftm->is_beam_configured());

      ASSERT_FALSE(ftm->is_scan_configured());
      write_config();
      ftm->configure_scan(scan_config);
      ASSERT_TRUE(ftm->is_scan_configured());

      PSTLOG_TRACE(this, "ftm->start_scan()");
      PSTLOG_INFO(this, "Starting scan");
      write_data();
      ftm->start_scan(start_scan_config);

      // See AT3-920 sleep to allow pipeline to do its job
      wait_for_pipeline_to_finish(ftm);

      ftm->get_flow_through_stats();

      ftm->stop_scan();

      ftm->deconfigure_scan();
      ASSERT_FALSE(ftm->is_scan_configured());

      ftm->deconfigure_beam();
      ASSERT_FALSE(ftm->is_beam_configured());

      tear_down_data_block();
    }
  }

  TEST_F(FlowThroughManagerTest, test_multiple_scans_same_config) // NOLINT
  {
    auto scan_id = start_scan_config.get<int32_t>("SCAN_ID");

    ASSERT_FALSE(ftm->is_beam_configured());
    ftm->configure_beam(beam_config);
    ASSERT_TRUE(ftm->is_beam_configured());

    ASSERT_FALSE(ftm->is_scan_configured());
    write_config();
    ftm->configure_scan(scan_config);
    ASSERT_TRUE(ftm->is_scan_configured());

    for (auto i = 0; i < 3; i++)
    {
      ASSERT_FALSE(ftm->is_scanning());
      auto curr_scan_id = scan_id + i;
      PSTLOG_INFO(this, "========== Performing scan loop {}", curr_scan_id);
      // update the scan ID in the configs
      data_scan_config.set("SCAN_ID", curr_scan_id);
      weights_scan_config.set("SCAN_ID", curr_scan_id);
      start_scan_config.set("SCAN_ID", curr_scan_id);
      data_header.set("SCAN_ID", curr_scan_id);
      weights_header.set("SCAN_ID", curr_scan_id);

      PSTLOG_INFO(this, "Fill up output SMRB for scan {}", curr_scan_id);
      write_data();

      PSTLOG_TRACE(this, "ftm->start_scan()");
      PSTLOG_INFO(this, "Starting scan {}", curr_scan_id);
      ftm->start_scan(start_scan_config);
      ASSERT_TRUE(ftm->is_scanning());
      PSTLOG_INFO(this, "Scan started sleeping");

      // See AT3-920 sleep to allow pipeline to do its job
      wait_for_pipeline_to_finish(ftm);

      PSTLOG_INFO(this, "Stopping scan");
      ftm->stop_scan();
      ASSERT_FALSE(ftm->is_scanning());
    }

    ASSERT_TRUE(ftm->is_scan_configured());
    ftm->deconfigure_scan();
    ASSERT_FALSE(ftm->is_scan_configured());

    // deconfigure_beam destroys output smrb
    ASSERT_TRUE(ftm->is_beam_configured());
    ftm->deconfigure_beam();
    ASSERT_FALSE(ftm->is_beam_configured());

    tear_down_data_block();
  }

  TEST_F(FlowThroughManagerTest, test_get_configuration) // NOLINT
  {
    ftm->configure_beam(beam_config);

    ASSERT_EQ(beam_config.get_val("DATA_KEY"), ftm->get_beam_configuration().get_val("DATA_KEY"));
    ASSERT_EQ(beam_config.get_val("WEIGHTS_KEY"), ftm->get_beam_configuration().get_val("WEIGHTS_KEY"));

    write_config();
    ftm->configure_scan(scan_config);
    ASSERT_EQ(scan_config.get<double>("SCANLEN_MAX"), ftm->get_scan_configuration().get<double>("SCANLEN_MAX"));
  }

  void ska::pst::dsp::test::FlowThroughManagerTest::assert_dada_files(const std::string& dada_type, uint64_t total_size, int32_t expected_files, const ska::pst::common::AsciiHeader& config)
  {
    const double seconds_per_file = 10;

    PSTLOG_INFO(this, "config.raw:\n{}",config.raw());
    std::string eb_id = config.get_val("EB_ID");
    std::string telescope = config.get_val("TELESCOPE");
    std::string scan_id = config.get_val("SCAN_ID");
    std::string utc_start = config.get_val("UTC_START");

    ska::pst::common::FilePaths file_paths(base_path);
    std::filesystem::path output_path = file_paths.get_scan_product_path(eb_id, telescope, scan_id) / dada_type;

    auto tsamp = config.get<double>("TSAMP");
    auto num_bits_out = config.get<uint32_t>("NBIT");
    auto npol_out = config.get<uint32_t>("NPOL");
    auto nchan_out = config.get<uint32_t>("NCHAN");
    auto ndim = config.get<uint32_t>("NDIM");
    auto hdr_size = config.get<uint32_t>("HDR_SIZE");
    double bytes_per_sample = static_cast<double>(num_bits_out * npol_out * nchan_out * ndim) / (ska::pst::common::bits_per_byte);
    double samples_per_second = ska::pst::common::microseconds_per_second / tsamp;
    double bytes_per_second = samples_per_second * bytes_per_sample;
    auto resolution = config.get<uint64_t>("RESOLUTION"); // output resolution

    auto bytes_written_per_file = static_cast<uint64_t>(floor(bytes_per_second * seconds_per_file));
    uint64_t remainder = bytes_written_per_file % resolution;

    if (remainder > 0)
    {
      PSTLOG_INFO(this, "bytes_written_per_file={}", bytes_written_per_file);
      PSTLOG_INFO(this, "resolution={}", resolution);
      PSTLOG_INFO(this, "remainder={}", remainder);
      bytes_written_per_file += (resolution - remainder);
      PSTLOG_INFO(this, "bytes_written_per_file={}", bytes_written_per_file);
    }

    uint64_t total_expected_output_file_size = bytes_written_per_file + hdr_size;
    PSTLOG_INFO(this, "bytes_per_second={} bytes_written_per_file={} hdr_size={}",bytes_per_second, bytes_written_per_file, hdr_size);
    // PSTLOG_INFO(this, "smrb_data_out.get_data_bufsz()={}", smrb_data_out.get_data_bufsz());

    uint64_t obs_offset = 0;
    uint64_t file_number = 0;
    std::filesystem::path output_file;

    for (int i=1;i<=expected_files;i++)
    {
      output_file = output_path / ska::pst::common::FileWriter::get_filename(utc_start, obs_offset, file_number);
      ASSERT_TRUE(std::filesystem::exists(output_file));
      if(i==expected_files)
      {
        total_expected_output_file_size=(total_size-obs_offset)+hdr_size;
      }
      PSTLOG_INFO(this, "file{}={} size={} total_expected_output_file_size={}", file_number, output_file.generic_string(), std::filesystem::file_size(output_file), total_expected_output_file_size);
      ASSERT_EQ(total_expected_output_file_size, std::filesystem::file_size(output_file)) << dada_type;
      file_number += 1;
      obs_offset += bytes_written_per_file;
    }
  }

  TEST_P(FlowThroughManagerTest, test_output_files_exists) // NOLINT
  {
    auto on_gpu = GetParam();
    const double seconds_per_file = 10;

    // customise SMRB to enable multiple output files
    tear_down_data_block();
    const uint64_t bufsz_factor=128;
    setup_data_block(bufsz_factor);

    if (on_gpu)
    {
      ftm->assign_cuda_device(0);
    }

    PSTLOG_INFO(this, "Configuring beam");
    ASSERT_FALSE(ftm->is_beam_configured());
    ftm->configure_beam(beam_config);
    ASSERT_TRUE(ftm->is_beam_configured());

    PSTLOG_INFO(this, "Configuring scan");
    ASSERT_FALSE(ftm->is_scan_configured());
    write_config();
    ftm->configure_scan(scan_config);
    ASSERT_TRUE(ftm->is_scan_configured());

    PSTLOG_INFO(this, "Writing data");
    write_data();

    PSTLOG_INFO(this, "Starting scan");
    ftm->start_scan(start_scan_config);

    // See AT3-920 sleep to allow pipeline to do its job
    wait_for_pipeline_to_finish(ftm);

    // allow some time for FTM to process the data, is more needed? or a safer mechanism to know it is completed
    ska::pst::smrb::DataBlockView smrb_data_out("a004"); // NOLINT
    smrb_data_out.connect(0); // NOLINT
    smrb_data_out.lock(); // NOLINT
    smrb_data_out.read_config(); // NOLINT
    smrb_data_out.read_header(); // NOLINT

    ska::pst::common::AsciiHeader ft_data_config;
    ft_data_config.load_from_str(smrb_data_out.get_header());
    PSTLOG_INFO(this, "ft_data_config.raw:\n{}",ft_data_config.raw());

    ska::pst::smrb::DataBlockView smrb_weights_out("a006"); // NOLINT
    smrb_weights_out.connect(0); // NOLINT
    smrb_weights_out.lock(); // NOLINT
    smrb_weights_out.read_config(); // NOLINT
    smrb_weights_out.read_header(); // NOLINT

    ska::pst::common::AsciiHeader ft_weights_config;
    ft_weights_config.load_from_str(smrb_weights_out.get_header());
    PSTLOG_INFO(this, "ft_weights_config.raw:\n{}",ft_weights_config.raw());

    ska::pst::common::AsciiHeader input_data_header;
    input_data_header.load_from_file(test_data_file("data_header.txt"));

    auto data_nbit_out = ft_data_config.get<uint32_t>("NBIT_OUT");
    auto data_nbit = input_data_header.get<uint32_t>("NBIT");

    PSTLOG_INFO(this, "data_nbit_out={} data_nbit={}",data_nbit_out, data_nbit);
    uint64_t total_data_size = get_write_data_size() * data_nbit_out / data_nbit;
    PSTLOG_INFO(this, "total_data_size={}",total_data_size);
    uint64_t total_weights_size = get_write_weights_size();
    PSTLOG_INFO(this, "total_weights_size={}",total_weights_size);

    // generate file name for assertion
    std::string eb_id = ft_data_config.get_val("EB_ID");
    std::string telescope = ft_data_config.get_val("TELESCOPE");
    std::string scan_id = ft_data_config.get_val("SCAN_ID");
    std::string utc_start = ft_data_config.get_val("UTC_START");

    if (!on_gpu)
    {
      ASSERT_EQ(utc_start, ft_weights_config.get_val("UTC_START")); // on_gpu UTC_START=1858-11-17-00:00:00
      ASSERT_EQ(telescope, ft_weights_config.get_val("TELESCOPE")); // on_gpu TELESCOPE=unknown
    }
    ASSERT_EQ(eb_id, ft_weights_config.get_val("EB_ID"));
    ASSERT_EQ(scan_id, ft_weights_config.get_val("SCAN_ID"));

    smrb_data_out.unlock(); // NOLINT
    smrb_data_out.disconnect(); // NOLINT
    smrb_weights_out.unlock(); // NOLINT
    smrb_weights_out.disconnect(); // NOLINT

    ftm->get_flow_through_stats();
    ftm->stop_scan();

    ftm->deconfigure_scan();
    ASSERT_FALSE(ftm->is_scan_configured());

    ftm->deconfigure_beam();
    ASSERT_FALSE(ftm->is_beam_configured());

    const int32_t expected_files_data = 3;
    const int32_t expected_files_weights = 1;

    assert_dada_files("data", total_data_size, expected_files_data, ft_data_config);
    if (!on_gpu)
    {
      assert_dada_files("weights", total_weights_size, expected_files_weights, ft_weights_config);
    }
  }

  TEST_F(FlowThroughManagerTest, test_impulsive_interference_mean_stddev) // NOLINT
  {
    test_impulsive_interference(false);
  }

  TEST_F(FlowThroughManagerTest, test_impulsive_interference_median_mad) // NOLINT
  {
    test_impulsive_interference(true);
  }

  void FlowThroughManagerTest::test_impulsive_interference (bool use_robust_statistics)
  {
    auto backup_data_header = data_header;

    data_header.set("DATA_GENERATOR", "SquareWave");

    static constexpr double cal_frequency = 10.0; // Hz
    static constexpr double cal_off_intensity = 1e3;
    static constexpr double cal_on_intensity = 10.0 * cal_off_intensity;
    static constexpr double cal_duty_cycle = 0.1;

    data_header.set("CALFREQ", cal_frequency);
    data_header.set("CAL_DUTY_CYCLE", cal_duty_cycle);
    data_header.set("CAL_OFF_INTENSITY", cal_off_intensity);
    data_header.set("CAL_ON_INTENSITY", cal_on_intensity);

    _generator = std::make_unique<ska::pst::common::SegmentGenerator>();
    _generator->configure(data_header, weights_header);

    // customise SMRB to enable multiple output files
    tear_down_data_block();
    const uint64_t bufsz_factor=32;
    setup_data_block(bufsz_factor);

    PSTLOG_INFO(this, "RFI Configuring beam");
    ftm->configure_beam(beam_config);

    PSTLOG_INFO(this, "RFI Configure LoadToQuantize pipeline with use_median_mad={}", use_robust_statistics);
    if (use_robust_statistics)
    {
      scan_config.set_val("RESCALE_ALGORITHM", "MedianMad");
    }
    else
    {
      scan_config.set_val("RESCALE_ALGORITHM", "MeanStddev");
    }

    PSTLOG_INFO(this, "RFI Configuring scan");
    write_config();
    ftm->configure_scan(scan_config);

    PSTLOG_INFO(this, "RFI Starting scan");
    ftm->start_scan(start_scan_config);

    PSTLOG_INFO(this, "RFI Writing data ");
    write_data();

    // See AT3-920 sleep to allow pipeline to do its job
    wait_for_pipeline_to_finish(ftm);

    PSTLOG_INFO(this, "RFI Get Flow Through Stats");
    ftm->get_flow_through_stats();

    PSTLOG_INFO(this, "RFI Get Flow Through DSPSR Pipeline");
    auto pipeline = ftm->get_dspsr_pipeline();
    ASSERT_NE(pipeline, nullptr);

    // note that it may be better to extract the offset/scale from the output DADA files
    PSTLOG_INFO(this, "RFI Get Rescale operation");
    auto rescale = pipeline->get_rescale();
    ASSERT_NE(rescale, nullptr);

    unsigned nchan = ftm->get_nchan();
    unsigned npol = ftm->get_npol();

    double expected_variance = 0.0;
    double ndim = 2; // Re/Im

    // if robust statistics are used to compute the scale, then it should not be biased by cal_on_intensity
    if (use_robust_statistics)
    {
      expected_variance = cal_off_intensity / ndim;
    }
    else
    {
      double biased_variance = cal_duty_cycle*cal_on_intensity + (1-cal_duty_cycle)*cal_off_intensity;
      expected_variance = biased_variance / ndim;
    }

    double expected_scale = 1.0 / sqrt(expected_variance);
    double expected_offset = 0.0;

    double scale_tolerance = 0.0;
    double offset_tolerance = 0.0;

    if (use_robust_statistics)
    {
      /*
        The median and MAD of a discretely sampled (integer) distribution can have errors as large as 1 (one)
      */
      static constexpr double rms_error = 1.4826;  // k, where rms=k*MAD

      double offset_error = 1.0;

      double expected_rms = 1.0/expected_scale;
      Estimate<double> rms (expected_rms, rms_error*rms_error);
      Estimate<double> scale = 1.0 / rms;
      PSTLOG_INFO(this, "MAD scale error={}", scale.get_error());

      // two bins
      static constexpr double threshold = 2.0;

      scale_tolerance = threshold * scale.get_error();
      offset_tolerance = threshold * offset_error;
    }
    else
    {
      ::dsp::NormalSampleStats stats;
      stats.set_ndat (rescale->get_nsample());
      stats.set_variance (expected_variance);

      double offset_error = stats.get_sample_mean_stddev();
      double variance_error = stats.get_sample_variance_stddev();

      Estimate<double> var (expected_variance, variance_error*variance_error);
      Estimate<double> scale = 1.0 / sqrt(var);
      PSTLOG_INFO(this, "RMS scale error={}", scale.get_error());

      // six sigma
      static constexpr double threshold_sigma = 6.0;

      scale_tolerance = threshold_sigma * scale.get_error();
      offset_tolerance = threshold_sigma * offset_error;
    }

    unsigned offset_error_count = 0;
    unsigned scale_error_count = 0;
    static constexpr unsigned max_error_count = 5;

    for (unsigned ipol = 0; ipol < npol; ipol++)
    {
      auto offset = rescale->get_offset (ipol);
      auto scale = rescale->get_scale (ipol);

      for (unsigned ichan = 0; ichan < nchan; ichan++)
      {
        if (fabs(scale[ichan] - expected_scale) > scale_tolerance) // NOLINT
        {
          scale_error_count ++;
          if (scale_error_count < max_error_count)
          {
            std::cerr << "ipol=" << ipol << " ichan=" << ichan << " scale=" << scale[ichan] << " expected=" << expected_scale << std::endl; // NOLINT
          }
        }

        if (fabs(offset[ichan] - expected_offset) > offset_tolerance) // NOLINT
        {
          offset_error_count ++;
          if (offset_error_count < max_error_count)
          {
            std::cerr << "ipol=" << ipol << " ichan=" << ichan << " offset=" << offset[ichan] << " expected=" << expected_offset << std::endl; // NOLINT
          }
        }
      }
    }

    ftm->stop_scan();

    ftm->deconfigure_scan();
    ftm->deconfigure_beam();

    data_header = backup_data_header;

    ASSERT_EQ(scale_error_count, 0);
    ASSERT_EQ(offset_error_count, 0);
  }

  auto get_gpu_flags() -> std::vector<bool>
  {
    int device_count{0};
    cudaError_t cudaStatus = cudaGetDeviceCount(&device_count);
    if (cudaStatus == cudaSuccess && device_count > 0)
    {
      return { false, true };
    }

    std::cout << "No GPU detected, GPU tests of FlowThroughManager disabled" << std::endl;
    return { false };
  }

  INSTANTIATE_TEST_SUITE_P(
    FlowThroughManagerTestSuite,
    FlowThroughManagerTest,
    testing::ValuesIn(get_gpu_flags()),
    [](const testing::TestParamInfo<FlowThroughManagerTest::ParamType>& info)
    {
      bool on_gpu = info.param;
      std::string name;
      if (on_gpu)
        name = "on_gpu";
      else
        name = "on_cpu";

      return name;
    }
  );

} // namespace ska::pst::dsp::test
