/*
 * Copyright 2024 Square Kilometre Array Observatory
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 * contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#include <gtest/gtest.h>
#include "ska_pst/common/utils/AsciiHeader.h"
#include "ska_pst/smrb/DataBlockCreate.h"
#include "ska_pst/smrb/DataBlockWrite.h"
#include "ska_pst/dsp/ft/FlowThroughManager.h"
#include "ska_pst/dsp/testutils/DspTestHelper.h"

#ifndef SKA_PST_DSP_TESTS_FlowThroughManagerTest_h
#define SKA_PST_DSP_TESTS_FlowThroughManagerTest_h

namespace ska {
namespace pst {
namespace dsp {
namespace test {

/**
  * @brief Test the FlowThroughManager class. The parameters passed are as follows:
  *
  * - `bool` : A flag indicating whether to test with a CUDA capable gpu when available.
  *
  */
class FlowThroughManagerTest : public ::testing::TestWithParam<bool>, public DspTestHelper
{
    protected:
      void SetUp() override;

      void TearDown() override;

      //! run identical RFI test with or without using robust statistics
      void test_impulsive_interference (bool use_robust_statistics);

    public:
      FlowThroughManagerTest() = default;

      virtual ~FlowThroughManagerTest() = default;

      //! FlowThroughManager constructed in the SetUp fixture
      std::shared_ptr<ska::pst::dsp::FlowThroughManager> ftm{nullptr};

      //! Detected CUDA capable gpu devices
      int deviceCount = -1;

      //! Set true when test should be performed on GPU
      bool on_gpu = false;

      //! base path to which the FlowThroughManager will write data products
      std::filesystem::path base_path{"/tmp"};

      //! flag to configure FlowThroughManager to bypass kernel buffering on I/O
      bool use_o_direct{false};

      //! helper function for asserting dada file existence and file sizes
      void assert_dada_files(const std::string& dada_type, uint64_t total_size, int32_t expected_files, const ska::pst::common::AsciiHeader& config);

    private:

};

} // namespace test
} // namespace dsp
} // namespace pst
} // namespace ska

#endif // SKA_PST_DSP_TESTS_FlowThroughManagerTest_h

