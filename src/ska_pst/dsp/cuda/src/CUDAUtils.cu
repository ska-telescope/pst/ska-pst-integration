/*
 * Copyright 2024 Square Kilometre Array Observatory
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 * contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#include "ska_pst/common/definitions.h"
#include "ska_pst/common/utils/Logging.h"
#include "ska_pst/dsp/cuda/CUDAUtils.h"

#include <cstdlib>
#include <iostream>

__host__ __device__ unsigned fibonacci(unsigned n) // NOLINT
{
  if (n <= 1)
  {
    return n;
  }
   else
  {
    return fibonacci(n - 1) + fibonacci(n - 2);
  }
}

__global__ void fibonacci_kernel(int* result, unsigned n)
{
  unsigned idx = threadIdx.x;

  if (idx < n)
  {
    result[idx] = fibonacci(idx); // NOLINT
  }
}

ska::pst::dsp::CUDAUtils::CUDAUtils()
{
  PSTLOG_TRACE(this, "ctor");
}

ska::pst::dsp::CUDAUtils::~CUDAUtils()
{
  PSTLOG_TRACE(this, "dtor");
}

void ska::pst::dsp::CUDAUtils::check_cuda_error(cudaError_t err, const char* file, int line)
{
  if (err != cudaSuccess)
  {
    std::string cuda_error = cudaGetErrorString(err);
    PSTLOG_WARN(this, "{} at {}:{}", cuda_error, file, line);

    // Handle the error appropriately based on your application's needs.
    throw std::runtime_error("CUDA error: " + cuda_error); // NOLINT
  }
}

void ska::pst::dsp::CUDAUtils::checkGPUAvailability()
{
  // Initialize CUDA
  cudaError_t err = cudaFree(nullptr);
  check_cuda_error(err, __FILE__, __LINE__);

  // Get the number of available GPUs
  int device_count{0};
  err = cudaGetDeviceCount(&device_count);
  check_cuda_error(err, __FILE__, __LINE__);

  if (device_count == 0) {
    PSTLOG_INFO(this, "No GPU devices found");
  } else {
    PSTLOG_INFO(this, "Number of GPU devices: {}", device_count);
  }
}

void ska::pst::dsp::CUDAUtils::print_gpu_info()
{
  // Get the number of available GPUs
  int device_count = 0;
  cudaError_t err = cudaGetDeviceCount(&device_count);
  check_cuda_error(err, __FILE__, __LINE__);

  if (device_count == 0)
  {
    PSTLOG_INFO(this, "No GPU devices found");
  }
  else
  {
    if (device_count > 4)
    {
      PSTLOG_ERROR(this, "Unrealistic cudaDeviceCount: {}", device_count);
      throw std::runtime_error("Unrealistic number of devices returned by cudaGetDeviceCount");
    }
    PSTLOG_INFO(this, "Number of GPU devices: {}", device_count);

    for (int device = 0; device < device_count; ++device)
    {
      cudaDeviceProp deviceProp{};
      err = cudaGetDeviceProperties(&deviceProp, device);
      check_cuda_error(err, __FILE__, __LINE__);

      PSTLOG_INFO(this, "GPU Device {} Details:", device);
      PSTLOG_INFO(this, "Device Name: {}", deviceProp.name);
      PSTLOG_INFO(this, "Compute Capability: {}.{}", deviceProp.major, deviceProp.minor);
      PSTLOG_INFO(this, "Total Global Memory: {} MB", deviceProp.totalGlobalMem / ska::pst::common::bytes_per_megabyte);
      PSTLOG_INFO(this, "CUDA Cores: {}", deviceProp.multiProcessorCount);
      PSTLOG_INFO(this, "GPU Clock Rate: {} MHz", deviceProp.clockRate / ska::pst::common::kilohertz_per_megahertz);
      PSTLOG_INFO(this, "Memory Clock Rate: {} MHz", deviceProp.memoryClockRate / ska::pst::common::kilohertz_per_megahertz);
      PSTLOG_INFO(this, "Memory Bus Width: {} bits",  deviceProp.memoryBusWidth);
    }
  }
}

auto ska::pst::dsp::CUDAUtils::test_fibonacci(const size_t n) -> bool
{
  // Host variables
  unsigned* h_result{nullptr};

  // Device variables
  int* d_result{nullptr};

  // Allocate memory
  const size_t buffer_size = n * sizeof(unsigned);
  PSTLOG_TRACE(this, "test_fibonacci allocating {} bytes of host and gpu memory", buffer_size);
  cudaError_t err = cudaMallocHost(reinterpret_cast<void**>(&h_result), buffer_size);
  check_cuda_error(err, __FILE__, __LINE__);
  err = cudaMalloc(reinterpret_cast<void**>(&d_result), buffer_size);
  check_cuda_error(err, __FILE__, __LINE__);

  // Launch the kernel
  PSTLOG_TRACE(this, "test_fibonacci executing fibonacci kernel");
  fibonacci_kernel<<<1, n, 0>>>(d_result, n);
  cudaDeviceSynchronize();
  err = cudaGetLastError();
  check_cuda_error(err, __FILE__, __LINE__);

  // Copy the result back to the host
  PSTLOG_TRACE(this, "test_fibonacci copying results from device to host");
  err = cudaMemcpy(reinterpret_cast<void *>(h_result), reinterpret_cast<void *>(d_result), buffer_size, cudaMemcpyDeviceToHost);
  check_cuda_error(err, __FILE__, __LINE__);

  // Check the Fibonacci sequence
  PSTLOG_DEBUG(this, "test_fibonacci checking the Fibonacci Sequence for the first {} numbers.", n);
  bool sequence_valid = true;
  for (int i = 0; i < n; ++i)
  {
    unsigned fib = fibonacci(i);
    PSTLOG_TRACE(this, "test_fibonacci expected value={}", fib);
    PSTLOG_TRACE(this, "test_fibonacci expected h_result={}", reinterpret_cast<void *>(h_result));
    if (h_result[i] != fib) // NOLINT
    {
      PSTLOG_WARN(this, "test_fibonacci number={} GPU={} CPU={}", i, h_result[i], fib); // NOLINT
      sequence_valid = false;
    }
  }

  // Free device memory
  PSTLOG_TRACE(this, "test_fibonacci freeing memory");
  cudaFree(d_result);
  cudaFreeHost(h_result);

  return sequence_valid;
}
