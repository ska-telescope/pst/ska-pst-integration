/*
 * Copyright 2024 Square Kilometre Array Observatory
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 * contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
#include <iostream>
#include <cuda_runtime.h>

#ifndef SKA_PST_DSP_CUDAUtils_h
#define SKA_PST_DSP_CUDAUtils_h

namespace ska {
namespace pst {
namespace dsp {
  /**
   * @brief The CUDAUtils provides basic functionality for querying baremetal CUDA GPU's
   *
   */
   class CUDAUtils
  {
    public:

      /**
       * @brief Default Construct a new CUDAUtils object
       *
       */
      CUDAUtils();

      /**
       * @brief Destroy the CUDAUtils object
       *
       */
      ~CUDAUtils();

      /**
       * @brief Generic cuda error handler
       *
       * @param err This parameter holds the CUDA runtime API error code that you want to check. The CUDA runtime API functions return an error code of type
       * @param file This parameter represents the name of the source code file where the check_cuda_error function is called. It is typically filled in automatically using the __FILE__ macro when the macro is used in the code.
       * @param line This parameter represents the line number in the source code where the check_cuda_error function is called. It is typically filled in automatically using the __LINE__ macro when the macro is used in the code.
       *
       */
      void check_cuda_error(cudaError_t err, const char* file, int line);

      /**
       * @brief Prints details regarding GPU detected by CUDA libraries
       *
       */
      void print_gpu_info();

      /**
       * @brief Check the availability of GPUs and validate the CUDA driver and runtime versions.
       *
       */
      void checkGPUAvailability();

      /**
       * @brief Generate fibonacci sequence using a GPU and validate the result
       *
       * @param n Length of sequence
       * @return true GPU sequence matches a CPU computed sequence; false otherwise
       */
      bool test_fibonacci(const size_t n);

    private:
  };

} // dsp
} // pst
} // ska

#endif // SKA_PST_DSP_CUDAUtils_h
