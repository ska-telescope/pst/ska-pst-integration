/*
 * Copyright 2024 Square Kilometre Array Observatory
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 * contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#include "ska_pst/common/definitions.h"
#include "ska_pst/common/utils/AsciiHeader.h"
#include "ska_pst/common/utils/Timer.h"
#include "ska_pst/dsp/cuda/tests/CUDAUtilsTest.h"
#include "ska_pst/common/testutils/GtestMain.h"

#include <iostream>
#include <thread>

auto main(int argc, char* argv[]) -> int
{
  return ska::pst::common::test::gtest_main(argc, argv);
}

namespace ska::pst::dsp::test {

void CUDAUtilsTest::SetUp()
{
  try
  {
    cuda_utils = std::make_shared<CUDAUtils>();
    cuda_utils->checkGPUAvailability();
  }
  catch (std::exception& exc)
  {
    std::cerr << "No GPU was available for use" << std::endl;
    GTEST_SKIP();
  }
}

void CUDAUtilsTest::TearDown()
{
  cuda_utils = nullptr;
}

TEST_F(CUDAUtilsTest, test_cuda_gpu_info) // NOLINT
{
  ASSERT_NO_THROW(cuda_utils->print_gpu_info());
}

TEST_F(CUDAUtilsTest, test_cuda_malloc_mcopy) // NOLINT
{
  ASSERT_NO_THROW(cuda_utils->test_fibonacci(5)); // NOLINT
}

} // namespace ska::pst::dsp::test
