/*
 * Copyright 2024 Square Kilometre Array Observatory
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 * contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#include <gtest/gtest.h>
#include "ska_pst/smrb/DataBlockCreate.h"
#include "ska_pst/smrb/DataBlockWrite.h"
#include "ska_pst/dsp/cuda/CUDAUtils.h"

#ifndef SKA_PST_DSP_TESTS_CUDAUtilsTest_h
#define SKA_PST_DSP_TESTS_CUDAUtilsTest_h

namespace ska {
namespace pst {
namespace dsp {
namespace test {

/**
 * @brief Test the CUDAUtils class
 *
 * @details
 *
 */
class CUDAUtilsTest : public ::testing::Test
{
    protected:
      void SetUp() override;

      void TearDown() override;

    public:
      CUDAUtilsTest() = default;

      ~CUDAUtilsTest() = default;

      std::shared_ptr<CUDAUtils> cuda_utils{nullptr};

    private:

};

} // namespace test
} // namespace dsp
} // namespace pst
} // namespace ska

#endif // SKA_PST_SMRB_TESTS_CUDAUtilsTest_h

