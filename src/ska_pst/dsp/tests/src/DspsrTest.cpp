/*
 * Copyright 2024 Square Kilometre Array Observatory
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 * contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#include "ska_pst/dsp/tests/DspsrTest.h"
#include "ska_pst/common/testutils/GtestMain.h"

#include <dsp/DADABuffer.h>

#include <iostream>

auto main(int argc, char* argv[]) -> int
{
  return ska::pst::common::test::gtest_main(argc, argv);
}

namespace ska::pst::dsp::test {

DspsrTest::DspsrTest()
    : ::testing::Test()
{
}

void DspsrTest::SetUp()
{
}

void DspsrTest::TearDown()
{

}

/*
  The following test verifies that a dsp::DADABuffer can be constructed and deleted.

  This test was instrumental in tracking down a segmentation fault that arose because
  sizeof(DADABuffer) in dspsr != sizeof(DADABuffer) in ska_pst, which in turn arose because

  1) additional attributes in the DADABuffer class were defined when HAVE_CUDA == 1
  2) HAVE_CUDA is set to 1 in the config.h file internal to dspsr
  3) any package using dspsr as a third-party package cannot access config.h

  This bug was fixed by removing all instances of #include <config.h> and #if HAVE_anything
  from all dspsr header files.

  The first call to DADABuffer::open_key is expected to throw an exception.
  However, when the above bug was present, an exception was thrown by DADABuffer::open_file
  because the DADABuffer virtual method table (vtable) was defined differently in this
  compilation unit and the dspsr library.
*/
TEST_F(DspsrTest, test_construct_delete_dada_buffer) // NOLINT
{
  Reference::To<::dsp::DADABuffer> dada = new ::dsp::DADABuffer;

  // this call should fail
  try {
    dada->open_key("fail");
  }
  catch (const Error& error)
  {
    std::cerr << "expected exception caught " << error << std::endl;
  }

  // if the vtable is corrupted, then automatic deletion may lead to a segfault
  {
    Reference::To<::dsp::DADABuffer> tmp = new ::dsp::DADABuffer;
  }
}

} // namespace ska::pst::dsp::test
