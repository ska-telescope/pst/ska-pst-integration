/*
 * Copyright 2022 Square Kilometre Array Observatory
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 * contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#include "ska_pst/common/testutils/GtestMain.h"
#include "ska_pst/common/utils/Logging.h"
#include "ska_pst/dsp/testutils/DspTestHelper.h"

using ska::pst::common::test::test_data_file;

namespace ska::pst::dsp::test {

void DspTestHelper::setup_data_block(const uint64_t _bufsz_factor)
{
  bufsz_factor = _bufsz_factor;
  beam_config.load_from_file(test_data_file("beam_config.txt"));
  data_key = beam_config.get_val("DATA_KEY");
  weights_key = beam_config.get_val("WEIGHTS_KEY");
  PSTLOG_DEBUG(this, "data_key={} weights_key={}", data_key, weights_key);

  scan_config.load_from_file(test_data_file("scan_config.txt"));
  start_scan_config.load_from_file(test_data_file("start_scan_config.txt"));

  data_scan_config.load_from_file(test_data_file("data_scan_config.txt"));
  weights_scan_config.load_from_file(test_data_file("weights_scan_config.txt"));

  data_header.load_from_file(test_data_file("data_header.txt"));
  weights_header.load_from_file(test_data_file("weights_header.txt"));

  auto header_bufsz = data_header.get<uint64_t>("HDR_SIZE");
  // Ensure all the config use the right hdr_size
  beam_config.set("HDR_SIZE", header_bufsz);
  scan_config.set("HDR_SIZE", header_bufsz);
  start_scan_config.set("HDR_SIZE", header_bufsz);
  data_scan_config.set("HDR_SIZE", header_bufsz);
  weights_scan_config.set("HDR_SIZE", header_bufsz);
  weights_header.set("HDR_SIZE", header_bufsz);

  auto data_resolution = data_header.get<uint64_t>("RESOLUTION");
  auto weights_resolution = weights_header.get<uint64_t>("RESOLUTION");

  uint64_t data_bufsz = data_resolution * bufsz_factor;
  uint64_t weights_bufsz = weights_resolution * bufsz_factor;
  PSTLOG_DEBUG(this, "data_bufsz={} weights_bufsz={}", data_bufsz, weights_bufsz);

  _dbc_data = std::make_unique<ska::pst::smrb::DataBlockCreate>(data_key);
  _dbc_data->create(header_nbufs, header_bufsz, data_nbufs, data_bufsz, nreaders, device);

  _writer_data = std::make_unique<ska::pst::smrb::DataBlockWrite>(data_key);
  _writer_data->connect(0);
  _writer_data->lock();

  _dbc_weights = std::make_unique<ska::pst::smrb::DataBlockCreate>(weights_key);
  _dbc_weights->create(header_nbufs, header_bufsz, weights_nbufs, weights_bufsz, nreaders, device);

  _writer_weights = std::make_unique<ska::pst::smrb::DataBlockWrite>(weights_key);
  _writer_weights->connect(0);
  _writer_weights->lock();

  if (_generator)
  {
    // number of bytes to write
    auto total_data_nbyte = data_bufsz * (data_nbufs - 2);

    // number of bytes per heap
    auto heap_data_nbyte = _generator->get_layout().get_data_heap_stride();
    auto heap_weights_nbyte = _generator->get_layout().get_weights_heap_stride();
    PSTLOG_DEBUG(this, "data resolution={} heapsize={}", data_resolution, heap_data_nbyte);
    PSTLOG_DEBUG(this, "weights resolution={} heapsize={}", weights_resolution, heap_weights_nbyte);

    // number of heaps to write
    auto nheap = total_data_nbyte / heap_data_nbyte;
    _generator->resize(nheap);
  }
  else
  {
    data_to_write.resize(data_bufsz * (data_nbufs - 2));
    weights_to_write.resize(weights_bufsz * (weights_nbufs - 2));
  }
}

auto DspTestHelper::get_write_data_size() -> uint64_t
{
  data_header.load_from_file(test_data_file("data_header.txt"));
  uint64_t data_bufsz = data_header.get<uint64_t>("RESOLUTION") * bufsz_factor;
  return data_bufsz * (data_nbufs - 2);
}

auto DspTestHelper::get_write_weights_size() -> uint64_t
{
  weights_header.load_from_file(test_data_file("weights_header.txt"));
  uint64_t weights_bufsz = weights_header.get<uint64_t>("BLOCK_DATA_BYTES") * bufsz_factor;
  return weights_bufsz * (weights_nbufs - 2);
}

void DspTestHelper::tear_down_writer(ska::pst::smrb::DataBlockWrite &writer)
{
  if (writer.get_opened())
  {
    try
    {
      writer.close();
    }
    catch (const std::exception &e)
    {
      PSTLOG_WARN(this, "error in closing DataBlockWrite key={}. Details = {}", writer.get_data_block_key_str(), e.what());
    }
  }

  if (writer.get_locked())
  {
    try
    {
      writer.unlock();
    }
    catch (const std::exception &e)
    {
      PSTLOG_WARN(this, "error in unlocking DataBlockWrite key={}. Details = {}", writer.get_data_block_key_str(), e.what());
    }
  }

  try {
    writer.disconnect();
  }
  catch (const std::exception& e)
  {
    PSTLOG_WARN(this, "error in disconnecting DataBlockWrite key={}. Details = {}", writer.get_data_block_key_str(), e.what());
  }
}

void DspTestHelper::tear_down_data_block()
{
  if (_writer_data)
  {
    tear_down_writer(*_writer_data);
    _writer_data = nullptr;
  }

  if (_dbc_data)
  {
    _dbc_data->destroy();
    _dbc_data = nullptr;
  }

  if (_writer_weights)
  {
    tear_down_writer(*_writer_weights);
    _writer_weights = nullptr;
  }

  if (_dbc_weights)
  {
    _dbc_weights->destroy();
    _dbc_weights = nullptr;
  }
}

void DspTestHelper::write_config()
{
  PSTLOG_DEBUG(this, "data_scan_config.raw()=\n{}", data_scan_config.raw());
  _writer_data->write_config(data_scan_config.raw());
  PSTLOG_DEBUG(this, "weights_scan_config.raw()=\n{}", weights_scan_config.raw());
  _writer_weights->write_config(weights_scan_config.raw());
}

void DspTestHelper::write_data()
{
  _writer_data->write_header(data_header.raw());
  _writer_weights->write_header(weights_header.raw());

  _writer_data->open();

  // this is set only if a generator is in use
  ska::pst::common::SegmentProducer::Segment segment;

  if (_generator)
  {
    PSTLOG_DEBUG(this, "generator->next_segment()");
    segment = _generator->next_segment();

    PSTLOG_DEBUG(this, "segment.data.size={}", segment.data.size);
    PSTLOG_DEBUG(this, "get_write_data_size={}", get_write_data_size());

    _writer_data->write_data(segment.data.block, segment.data.size);
  }
  else
  {
    PSTLOG_DEBUG(this, "data_to_write.size()={}", data_to_write.size());
    _writer_data->write_data(&data_to_write[0], data_to_write.size());
  }

  _writer_data->close();

  _writer_weights->open();

  if (_generator)
  {
    PSTLOG_DEBUG(this, "segment.weights.size={}", segment.weights.size);
    _writer_weights->write_data(segment.weights.block, segment.weights.size);
  }
  else
  {
    PSTLOG_DEBUG(this, "weights_to_write.size()={}", weights_to_write.size());
    _writer_weights->write_data(&weights_to_write[0], weights_to_write.size());
  }

  _writer_weights->close();
}

} // namespace ska::pst::dsp::test
