/*
 * Copyright 2022 Square Kilometre Array Observatory
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 * contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#include "ska_pst/common/lmc/LmcServiceException.h"
#include "ska_pst/common/statemodel/StateModel.h"
#include "ska_pst/common/utils/AsciiHeader.h"
#include "ska_pst/common/utils/Logging.h"
#include "ska_pst/dsp/disk/lmc/DspDiskLmcServiceHandler.h"

#include <string>
#include <sstream>
#include <thread>

auto ska::pst::dsp::DspDiskLmcServiceHandler::beam_configuration_as_ascii_header(
    const ska::pst::lmc::BeamConfiguration &configuration) -> ska::pst::common::AsciiHeader
{
  PSTLOG_TRACE(this, "converting BeamConfiguration to AsciiHeader");
  // assert we only have an dsp resources request
  if (!configuration.has_dsp_disk())
  {
    PSTLOG_WARN(this, "BeamConfiguration protobuf message has no DSP.DISK details provided.");

    throw ska::pst::common::LmcServiceException(
        "Expected a DSP.DISK beam configuration object, but none were provided.",
        ska::pst::lmc::ErrorCode::INVALID_REQUEST,
        grpc::StatusCode::INVALID_ARGUMENT);
  }

  const auto &dsp_beam_configuration = configuration.dsp_disk();

  ska::pst::common::AsciiHeader config;
  config.set_val("DATA_KEY", dsp_beam_configuration.data_key());
  config.set_val("WEIGHTS_KEY", dsp_beam_configuration.weights_key());

  return config;
}

auto ska::pst::dsp::DspDiskLmcServiceHandler::scan_configuration_as_ascii_header(
    const ska::pst::lmc::ScanConfiguration &configuration) -> ska::pst::common::AsciiHeader
{
  PSTLOG_TRACE(this, "converting ScanConfiguration to AsciiHeader");
  if (!configuration.has_dsp_disk())
  {
    PSTLOG_WARN(this, "ScanConfiguration protobuf message has no DSP.DISK details provided.");

    throw ska::pst::common::LmcServiceException(
        "Expected a DSP.DISK scan configuration object, but none were provided.",
        ska::pst::lmc::ErrorCode::INVALID_REQUEST,
        grpc::StatusCode::INVALID_ARGUMENT);
  }

  const auto &request = configuration.dsp_disk();

  ska::pst::common::AsciiHeader scan_configuration;
  scan_configuration.set("BYTES_PER_SECOND", request.bytes_per_second());
  scan_configuration.set("SCANLEN_MAX", request.scanlen_max());
  scan_configuration.set("EB_ID", request.execution_block_id());

  return scan_configuration;
}

void ska::pst::dsp::DspDiskLmcServiceHandler::validate_beam_configuration(
    const ska::pst::lmc::BeamConfiguration &request)
{
  PSTLOG_TRACE(this, "validating BeamConfiguration");
  auto config = beam_configuration_as_ascii_header(request);
  ska::pst::common::ValidationContext context;
  dsp->validate_configure_beam(config, &context);
  context.throw_error_if_not_empty();
}

void ska::pst::dsp::DspDiskLmcServiceHandler::validate_scan_configuration(
    const ska::pst::lmc::ScanConfiguration &request)
{
  PSTLOG_TRACE(this, "validating ScanConfiguration");
  auto config = scan_configuration_as_ascii_header(request);
  ska::pst::common::ValidationContext context;
  dsp->validate_configure_scan(config, &context);
  context.throw_error_if_not_empty();
}

void ska::pst::dsp::DspDiskLmcServiceHandler::configure_beam(
    const ska::pst::lmc::BeamConfiguration &request)
{
  PSTLOG_TRACE(this, "configuring beam");

  if (dsp->get_state() == ska::pst::common::State::RuntimeError)
  {
    PSTLOG_WARN(this, "Received configure beam request when in Runtime Error.");

    throw ska::pst::common::LmcServiceException(
        "Received configure beam request when in Runtime Error.",
        ska::pst::lmc::ErrorCode::INVALID_REQUEST,
        grpc::StatusCode::FAILED_PRECONDITION);
  }

  if (is_beam_configured())
  {
    PSTLOG_WARN(this, "Received configure beam when beam configured already.");

    throw ska::pst::common::LmcServiceException(
        "Beam already configured for DSP.DISK.",
        ska::pst::lmc::ErrorCode::CONFIGURED_FOR_BEAM_ALREADY,
        grpc::StatusCode::FAILED_PRECONDITION);
  }

  auto config = beam_configuration_as_ascii_header(request);
  // validation will happens on the state model as first part of configure_beam
  // so no need to do validation here.
  dsp->configure_beam(config);
}

void ska::pst::dsp::DspDiskLmcServiceHandler::deconfigure_beam()
{
  PSTLOG_TRACE(this, "deconfiguring beam");

  if (dsp->get_state() == ska::pst::common::State::RuntimeError)
  {
    PSTLOG_WARN(this, "Received deconfigure beam when in Runtime Error.");
    throw ska::pst::common::LmcServiceException(
        "Received deconfigure beam when in Runtime Error.",
        ska::pst::lmc::ErrorCode::INVALID_REQUEST,
        grpc::StatusCode::FAILED_PRECONDITION);
  }

  if (!is_beam_configured())
  {
    PSTLOG_WARN(this, "Received deconfigure beam when beam not configured already.");
    throw ska::pst::common::LmcServiceException(
        "DSP.DISK not configured for beam.",
        ska::pst::lmc::ErrorCode::NOT_CONFIGURED_FOR_BEAM,
        grpc::StatusCode::FAILED_PRECONDITION);
  }

  if (is_scan_configured())
  {
    PSTLOG_WARN(this, "Received deconfigure beam when scan is already configured.");
    throw ska::pst::common::LmcServiceException(
        "DSP.DISK is configured for scan but trying to deconfigure beam.",
        ska::pst::lmc::ErrorCode::INVALID_REQUEST,
        grpc::StatusCode::FAILED_PRECONDITION);
  }

  dsp->deconfigure_beam();
}

void ska::pst::dsp::DspDiskLmcServiceHandler::get_beam_configuration(
    ska::pst::lmc::BeamConfiguration *response)
{
  PSTLOG_TRACE(this, "getting beam configuration");
  if (!is_beam_configured())
  {
    PSTLOG_WARN(this, "Received request to get beam configuration when beam not configured.");
    throw ska::pst::common::LmcServiceException(
        "DSP.DISK not configured for beam.",
        ska::pst::lmc::ErrorCode::NOT_CONFIGURED_FOR_BEAM,
        grpc::StatusCode::FAILED_PRECONDITION);
  }

  const auto &resources = dsp->get_beam_configuration();

  ska::pst::lmc::DspDiskBeamConfiguration *dsp_beam_configuration = response->mutable_dsp_disk();
  dsp_beam_configuration->set_data_key(resources.get_val("DATA_KEY"));
  dsp_beam_configuration->set_weights_key(resources.get_val("WEIGHTS_KEY"));
}

auto ska::pst::dsp::DspDiskLmcServiceHandler::is_beam_configured() const noexcept -> bool
{
  return (dsp->is_beam_configured());
}

void ska::pst::dsp::DspDiskLmcServiceHandler::configure_scan(
    const ska::pst::lmc::ScanConfiguration &configuration)
{
  PSTLOG_TRACE(this, "configure_scan");

  if (dsp->get_state() == ska::pst::common::State::RuntimeError)
  {
    PSTLOG_WARN(this, "Received configure scan when in Runtime Error.");
    throw ska::pst::common::LmcServiceException(
        "Received configure scan when in Runtime Error.",
        ska::pst::lmc::ErrorCode::INVALID_REQUEST,
        grpc::StatusCode::FAILED_PRECONDITION);
  }

  if (!is_beam_configured())
  {
    PSTLOG_WARN(this, "Received scan configuration request when beam not configured already.");

    throw ska::pst::common::LmcServiceException(
        "DSP.DISK not configured for beam.",
        ska::pst::lmc::ErrorCode::NOT_CONFIGURED_FOR_BEAM,
        grpc::StatusCode::FAILED_PRECONDITION);
  }

  if (is_scan_configured())
  {
    PSTLOG_WARN(this, "Received configure_scan when scan already configured.");

    throw ska::pst::common::LmcServiceException(
        "Scan already configured for DSP.DISK.",
        ska::pst::lmc::ErrorCode::CONFIGURED_FOR_SCAN_ALREADY,
        grpc::StatusCode::FAILED_PRECONDITION);
  }

  auto scan_configuration = scan_configuration_as_ascii_header(configuration);

  // validation will happens on the state model as first part of configure_scan
  // so no need to do validation here.
  dsp->configure_scan(scan_configuration);
}

void ska::pst::dsp::DspDiskLmcServiceHandler::deconfigure_scan()
{
  PSTLOG_TRACE(this, "deconfiguring scan");

  if (dsp->get_state() == ska::pst::common::State::RuntimeError)
  {
    PSTLOG_WARN(this, "Received deconfigure scan when in Runtime Error.");
    throw ska::pst::common::LmcServiceException(
        "Received deconfigure scan when in Runtime Error.",
        ska::pst::lmc::ErrorCode::INVALID_REQUEST,
        grpc::StatusCode::FAILED_PRECONDITION);
  }

  if (dsp->get_state() != ska::pst::common::State::ScanConfigured)
  {
    PSTLOG_WARN(this, "Received deconfigure_scan when scan not already configured.");

    throw ska::pst::common::LmcServiceException(
        "Scan not currently configured for DSP.DISK.",
        ska::pst::lmc::ErrorCode::NOT_CONFIGURED_FOR_SCAN,
        grpc::StatusCode::FAILED_PRECONDITION);
  }

  // Should not let a deconfigure request come through if in a scanning state.
  if (dsp->get_state() == ska::pst::common::State::Scanning)
  {
    PSTLOG_WARN(this, "Received deconfigure request when still scanning.");
    throw ska::pst::common::LmcServiceException(
        "DSP.DISK is scanning but trying to deconfigure scan.",
        ska::pst::lmc::ErrorCode::INVALID_REQUEST,
        grpc::StatusCode::FAILED_PRECONDITION);
  }

  dsp->deconfigure_scan();
}

void ska::pst::dsp::DspDiskLmcServiceHandler::get_scan_configuration(
    ska::pst::lmc::ScanConfiguration *configuration)
{
  PSTLOG_TRACE(this, "getting scan configuration");
  if (!is_scan_configured())
  {
    PSTLOG_WARN(this, "Received get_scan_configuration when scan not already configured.");

    throw ska::pst::common::LmcServiceException(
        "Not currently configured for DSP.DISK.",
        ska::pst::lmc::ErrorCode::NOT_CONFIGURED_FOR_SCAN,
        grpc::StatusCode::FAILED_PRECONDITION);
  }

  const auto &curr_scan_configuration = dsp->get_scan_configuration();
  auto *dsp_scan_configuration = configuration->mutable_dsp_disk();

  dsp_scan_configuration->set_scanlen_max(curr_scan_configuration.get<double>("SCANLEN_MAX"));
  dsp_scan_configuration->set_bytes_per_second(curr_scan_configuration.get<double>("BYTES_PER_SECOND"));
  dsp_scan_configuration->set_execution_block_id(curr_scan_configuration.get_val("EB_ID"));
}

auto ska::pst::dsp::DspDiskLmcServiceHandler::is_scan_configured() const noexcept -> bool
{
  return dsp->is_scan_configured();
}

void ska::pst::dsp::DspDiskLmcServiceHandler::start_scan(const ska::pst::lmc::StartScanRequest &request)
{
  PSTLOG_TRACE(this, "starting scan");

  if (dsp->get_state() == ska::pst::common::State::RuntimeError)
  {
    PSTLOG_WARN(this, "Received start scan when in Runtime Error.");
    throw ska::pst::common::LmcServiceException(
        "Received start scan when in Runtime Error.",
        ska::pst::lmc::ErrorCode::INVALID_REQUEST,
        grpc::StatusCode::FAILED_PRECONDITION);
  }

  if (!is_scan_configured())
  {
    PSTLOG_WARN(this, "Received scan request when scan not already configured.");

    throw ska::pst::common::LmcServiceException(
        "Not currently configured for DSP.DISK.",
        ska::pst::lmc::ErrorCode::NOT_CONFIGURED_FOR_SCAN,
        grpc::StatusCode::FAILED_PRECONDITION);
  }

  if (is_scanning())
  {
    PSTLOG_WARN(this, "Received scan request when already scanning.");
    throw ska::pst::common::LmcServiceException(
        "DSP.DISK is already scanning.",
        ska::pst::lmc::ErrorCode::ALREADY_SCANNING,
        grpc::StatusCode::FAILED_PRECONDITION);
  }
  ska::pst::common::AsciiHeader start_scan_config;
  start_scan_config.set("SCAN_ID", request.scan_id());

  dsp->start_scan(start_scan_config);
}

void ska::pst::dsp::DspDiskLmcServiceHandler::stop_scan()
{
  PSTLOG_TRACE(this, "stopping scan");
  if (dsp->get_state() == ska::pst::common::State::RuntimeError)
  {
    PSTLOG_WARN(this, "Received stop scan when in Runtime Error.");
    throw ska::pst::common::LmcServiceException(
        "Received stop scan when in Runtime Error.",
        ska::pst::lmc::ErrorCode::INVALID_REQUEST,
        grpc::StatusCode::FAILED_PRECONDITION);
  }

  if (!is_scanning())
  {
    PSTLOG_WARN(this, "Received stop_scan request when not scanning.");

    throw ska::pst::common::LmcServiceException(
        "Received stop_scan request when DSP.DISK is not scanning.",
        ska::pst::lmc::ErrorCode::NOT_SCANNING,
        grpc::StatusCode::FAILED_PRECONDITION);
  }

  dsp->stop_scan();
}

void ska::pst::dsp::DspDiskLmcServiceHandler::reset()
{
  PSTLOG_INFO(this, "resetting");
  if (dsp->get_state() == ska::pst::common::State::RuntimeError)
  {
    dsp->reset();
  }
}

void ska::pst::dsp::DspDiskLmcServiceHandler::restart()
{
  PSTLOG_INFO(this, "performing hard restart with exit_code={}", ska::pst::common::ApplicationManager::restart_exit_code);
  dsp->force_exit(ska::pst::common::ApplicationManager::restart_exit_code);
}

auto ska::pst::dsp::DspDiskLmcServiceHandler::is_scanning() const noexcept -> bool
{
  return (dsp->is_scanning());
}

void ska::pst::dsp::DspDiskLmcServiceHandler::get_monitor_data(
    ska::pst::lmc::MonitorData *data)
{
  PSTLOG_TRACE(this, "getting monitoring data");
  if (!is_scanning())
  {
    PSTLOG_WARN(this, "Received get_monitor_data request when not scanning.");

    throw ska::pst::common::LmcServiceException(
        "Received get_monitor_data request when DSP.DISK is not scanning.",
        ska::pst::lmc::ErrorCode::NOT_SCANNING,
        grpc::StatusCode::FAILED_PRECONDITION);
  }

  auto *dsp_monitor_data = data->mutable_dsp_disk();

  const auto &disk_stats = dsp->get_disk_stats();
  dsp_monitor_data->set_disk_capacity(disk_stats.capacity);
  dsp_monitor_data->set_disk_available_bytes(disk_stats.available);
  dsp_monitor_data->set_bytes_written(disk_stats.bytes_written);
  dsp_monitor_data->set_write_rate(disk_stats.data_write_rate);
}

void ska::pst::dsp::DspDiskLmcServiceHandler::get_env(
    ska::pst::lmc::GetEnvironmentResponse *response) noexcept
{
  auto values = response->mutable_values();

  auto stats = dsp->get_disk_stats();

  ska::pst::lmc::EnvValue disk_capacity;
  disk_capacity.set_unsigned_int_value(stats.capacity);
  (*values)["disk_capacity"] = disk_capacity;

  ska::pst::lmc::EnvValue disk_available_bytes;
  disk_available_bytes.set_unsigned_int_value(stats.available);
  (*values)["disk_available_bytes"] = disk_available_bytes;
}

void ska::pst::dsp::DspDiskLmcServiceHandler::go_to_runtime_error(
    std::exception_ptr exc)
{
  dsp->go_to_runtime_error(exc);
}
