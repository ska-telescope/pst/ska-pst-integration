/*
 * Copyright 2022 Square Kilometre Array Observatory
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 * contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#include "ska_pst/common/lmc/LmcServiceException.h"
#include "ska_pst/common/testutils/GtestMain.h"
#include "ska_pst/common/utils/Logging.h"
#include "ska_pst/common/definitions.h"
#include "ska_pst/dsp/disk/lmc/tests/DspDiskLmcServiceHandlerTest.h"

#include <iostream>
#include <grpc/grpc.h>
#include <grpc++/grpc++.h>

auto main(int argc, char *argv[]) -> int
{
  return ska::pst::common::test::gtest_main(argc, argv);
}

namespace ska::pst::dsp::test
{

  DspDiskLmcServiceHandlerTest::DspDiskLmcServiceHandlerTest()
      : ::testing::Test()
  {
  }

  void DspDiskLmcServiceHandlerTest::SetUp()
  {
    PSTLOG_TRACE(this, "creating shared data block manager");
    _dsp = std::make_shared<ska::pst::dsp::DiskManager>("/tmp", false);

    PSTLOG_TRACE(this, "creating handler");
    handler = std::make_shared<ska::pst::dsp::DspDiskLmcServiceHandler>(_dsp);

    setup_data_block();
  }

  void DspDiskLmcServiceHandlerTest::TearDown()
  {
    if (_dsp->is_scanning())
    {
      PSTLOG_TRACE(this, "_dsp->stop_scan()");
      _dsp->stop_scan();
    }

    if (_dsp->is_scan_configured())
    {
      PSTLOG_TRACE(this, "_dsp->deconfigure_scan()");
      _dsp->deconfigure_scan();
    }

    if (_dsp->is_beam_configured())
    {
      PSTLOG_TRACE(this, "_dsp->deconfigure_beam()");
      _dsp->deconfigure_beam();
    }

    tear_down_data_block();
    PSTLOG_TRACE(this, "complete");
  }

  auto DspDiskLmcServiceHandlerTest::generate_scan_id() -> uint64_t
  {
      _scan_id++; //NOLINT
      return _scan_id; //NOLINT
  }

  void DspDiskLmcServiceHandlerTest::configure_beam()
  {
    ska::pst::lmc::BeamConfiguration request;
    auto dsp_resources_request = request.mutable_dsp_disk();
    dsp_resources_request->set_data_key(data_key);
    dsp_resources_request->set_weights_key(weights_key);

    handler->configure_beam(request);
  }

  void DspDiskLmcServiceHandlerTest::configure_scan(bool write_config)
  {
    ska::pst::lmc::ScanConfiguration request;

    auto *dsp_scan_configuration_request = request.mutable_dsp_disk();
    dsp_scan_configuration_request->set_bytes_per_second(data_scan_config.get<double>("BYTES_PER_SECOND"));
    dsp_scan_configuration_request->set_scanlen_max(data_scan_config.get<double>("SCANLEN_MAX"));
    dsp_scan_configuration_request->set_execution_block_id(data_scan_config.get_val("EB_ID"));

    if (write_config)
    {
      this->write_config();
    }
    handler->configure_scan(request);
  }

  void DspDiskLmcServiceHandlerTest::start_scan(uint64_t scan_id, bool write_data) // NOLINT
  {
    ska::pst::lmc::StartScanRequest request; //NOLINT
    request.set_scan_id(scan_id); //NOLINT
    if (write_data)
    {
      this->write_data();
    }
    handler->start_scan(request); //NOLINT
  }

  TEST_F(DspDiskLmcServiceHandlerTest, configure_deconfigure_beam) // NOLINT
  {
    PSTLOG_TRACE(this, "configure_deconfigure_beam - configuring beam");
    EXPECT_FALSE(handler->is_beam_configured()); // NOLINT
    EXPECT_FALSE(_dsp->is_beam_configured());

    PSTLOG_TRACE(this, "configure_deconfigure_beam - configure_beam");
    configure_beam();

    EXPECT_TRUE(handler->is_beam_configured());
    EXPECT_TRUE(_dsp->is_beam_configured());
    PSTLOG_TRACE(this, "configure_deconfigure_beam - beam configured");

    PSTLOG_TRACE(this, "configure_deconfigure_beam - getting beam configuration");
    ska::pst::lmc::BeamConfiguration beam_configuration;
    handler->get_beam_configuration(&beam_configuration);

    EXPECT_TRUE(beam_configuration.has_dsp_disk());
    auto dsp_beam_configuration = beam_configuration.dsp_disk();

    EXPECT_EQ(data_key, dsp_beam_configuration.data_key());       // NOLINT
    EXPECT_EQ(weights_key, dsp_beam_configuration.weights_key()); // NOLINT

    ska::pst::common::AsciiHeader &dsp_resources = _dsp->get_beam_configuration();
    EXPECT_EQ(data_key, dsp_resources.get_val("DATA_KEY"));       // NOLINT
    EXPECT_EQ(weights_key, dsp_resources.get_val("WEIGHTS_KEY")); // NOLINT
    PSTLOG_TRACE(this, "configure_deconfigure_beam - checked beam configuration");

    PSTLOG_TRACE(this, "configure_deconfigure_beam - deconfiguring beam");
    handler->deconfigure_beam();
    EXPECT_FALSE(handler->is_beam_configured()); // NOLINT
    EXPECT_FALSE(_dsp->is_beam_configured());    // NOLINT
    PSTLOG_TRACE(this, "configure_deconfigure_beam - beam deconfigured");
  }

  TEST_F(DspDiskLmcServiceHandlerTest, configure_beam_again_should_throw_exception) // NOLINT
  {
    PSTLOG_TRACE(this, "configure_beam_again_should_throw_exception - configuring beam");

    EXPECT_FALSE(handler->is_beam_configured()); // NOLINT

    configure_beam();

    EXPECT_TRUE(handler->is_beam_configured());
    EXPECT_TRUE(_dsp->is_beam_configured());
    PSTLOG_TRACE(this, "configure_beam_again_should_throw_exception - beam configured");

    try
    {
      configure_beam();
      FAIL() << " expected configure_beam to throw exception due to beam configured already.\n";
    }
    catch (ska::pst::common::LmcServiceException &ex)
    {
      EXPECT_EQ(std::string(ex.what()), "Beam already configured for DSP.DISK.");
      EXPECT_EQ(ex.error_code(), ska::pst::lmc::ErrorCode::CONFIGURED_FOR_BEAM_ALREADY);
      EXPECT_EQ(ex.status_code(), grpc::StatusCode::FAILED_PRECONDITION);
    }
  }

  TEST_F(DspDiskLmcServiceHandlerTest, configure_beam_with_invalid_configuration) // NOLINT
  {
    PSTLOG_TRACE(this, "configure_beam_with_invalid_configuration - configuring beam");
    data_key = "abcd";

    EXPECT_FALSE(handler->is_beam_configured());
    try
    {
      configure_beam();
      FAIL() << " expected configure_beam to throw exception due to invalid configuration.\n";
    }
    catch (std::exception &ex)
    {
      PSTLOG_DEBUG(this, "configure_beam_with_invalid_configuration exception thrown as expected: {}", ex.what());
      EXPECT_EQ(_dsp->get_state(), ska::pst::common::State::RuntimeError);
      EXPECT_FALSE(handler->is_beam_configured());
    }
    PSTLOG_TRACE(this, "configure_beam_with_invalid_configuration test done");
  }

  TEST_F(DspDiskLmcServiceHandlerTest, configure_beam_should_have_dsp_object) // NOLINT
  {
    PSTLOG_TRACE(this, "configure_beam_should_have_dsp_object - configuring beam");
    EXPECT_FALSE(handler->is_beam_configured()); // NOLINT

    ska::pst::lmc::BeamConfiguration beam_configuration;
    beam_configuration.mutable_test();

    try
    {
      handler->configure_beam(beam_configuration);
      FAIL() << " expected configure_beam to throw exception due not having DSP.DISK field.\n";
    }
    catch (ska::pst::common::LmcServiceException &ex)
    {
      EXPECT_EQ(std::string(ex.what()), "Expected a DSP.DISK beam configuration object, but none were provided.");
      EXPECT_EQ(ex.error_code(), ska::pst::lmc::ErrorCode::INVALID_REQUEST);
      EXPECT_EQ(ex.status_code(), grpc::StatusCode::INVALID_ARGUMENT);
    }
  }

  TEST_F(DspDiskLmcServiceHandlerTest, get_beam_configuration_when_not_beam_configured) // NOLINT
  {
    PSTLOG_TRACE(this, "get_beam_configuration_when_not_beam_configured - getting beam configuration");
    EXPECT_FALSE(handler->is_beam_configured());

    ska::pst::lmc::BeamConfiguration response;
    try
    {
      handler->get_beam_configuration(&response);
    }
    catch (ska::pst::common::LmcServiceException &ex)
    {
      EXPECT_EQ(std::string(ex.what()), "DSP.DISK not configured for beam.");
      EXPECT_EQ(ex.error_code(), ska::pst::lmc::ErrorCode::NOT_CONFIGURED_FOR_BEAM);
      EXPECT_EQ(ex.status_code(), grpc::StatusCode::FAILED_PRECONDITION);
    }
    PSTLOG_TRACE(this, "get_beam_configuration_when_not_beam_configured - beam not configured");
  }

  TEST_F(DspDiskLmcServiceHandlerTest, deconfigure_beam_when_not_beam_configured) // NOLINT
  {
    PSTLOG_TRACE(this, "deconfigure_beam_when_not_beam_configured - deconfiguring beam");

    EXPECT_FALSE(handler->is_beam_configured()); // NOLINT

    try
    {
      handler->deconfigure_beam();
      FAIL() << " expected deconfigure_beam to throw exception due to beam not configured.\n";
    }
    catch (ska::pst::common::LmcServiceException &ex)
    {
      EXPECT_EQ(std::string(ex.what()), "DSP.DISK not configured for beam.");
      EXPECT_EQ(ex.error_code(), ska::pst::lmc::ErrorCode::NOT_CONFIGURED_FOR_BEAM);
      EXPECT_EQ(ex.status_code(), grpc::StatusCode::FAILED_PRECONDITION);
    }
  }

  TEST_F(DspDiskLmcServiceHandlerTest, deconfigure_beam_when_scan_configured) // NOLINT
  {
    PSTLOG_TRACE(this, "deconfigure_beam_when_scan_configured - configuring beam");
    configure_beam();
    PSTLOG_TRACE(this, "deconfigure_beam_when_scan_configured - beam configured");

    PSTLOG_TRACE(this, "deconfigure_beam_when_scan_configured - configuring scan");
    EXPECT_FALSE(handler->is_scan_configured());
    configure_scan();
    EXPECT_TRUE(handler->is_scan_configured());
    PSTLOG_TRACE(this, "deconfigure_beam_when_scan_configured - scan configured");

    try
    {
      handler->deconfigure_beam();
      FAIL() << " expected deconfigure_beam to throw exception due having scan configuration.\n";
    }
    catch (ska::pst::common::LmcServiceException &ex)
    {
      PSTLOG_TRACE(this, "deconfigure_beam_when_scan_configured exception occurred as expected");
      EXPECT_EQ(std::string(ex.what()), "DSP.DISK is configured for scan but trying to deconfigure beam.");
      EXPECT_EQ(ex.error_code(), ska::pst::lmc::ErrorCode::INVALID_REQUEST);
      EXPECT_EQ(ex.status_code(), grpc::StatusCode::FAILED_PRECONDITION);
    }
    PSTLOG_TRACE(this, "deconfigure_beam_when_scan_configured done");
  }

  TEST_F(DspDiskLmcServiceHandlerTest, configure_deconfigure_scan) // NOLINT
  {

    PSTLOG_TRACE(this, "configure_deconfigure_scan - configuring beam");
    configure_beam();
    PSTLOG_TRACE(this, "configure_deconfigure_scan - beam configured");

    PSTLOG_TRACE(this, "configure_deconfigure_scan - configuring scan");
    EXPECT_FALSE(handler->is_scan_configured());
    configure_scan();
    EXPECT_TRUE(handler->is_scan_configured());
    PSTLOG_TRACE(this, "configure_deconfigure_scan - scan configured");

    PSTLOG_TRACE(this, "configure_deconfigure_scan - getting configuration");
    ska::pst::lmc::ScanConfiguration get_response;
    handler->get_scan_configuration(&get_response);
    EXPECT_TRUE(get_response.has_dsp_disk());

    const auto &dsp_scan_configuration = get_response.dsp_disk();
    double tolerance = ska::pst::common::test::get_tolerance(dsp_scan_configuration.bytes_per_second(), data_scan_config.get<double>("BYTES_PER_SECOND"));
    EXPECT_NEAR(dsp_scan_configuration.bytes_per_second(), data_scan_config.get<double>("BYTES_PER_SECOND"), tolerance);
    EXPECT_EQ(data_scan_config.get<double>("SCANLEN_MAX"), dsp_scan_configuration.scanlen_max());
    EXPECT_EQ(data_scan_config.get_val("EB_ID"), dsp_scan_configuration.execution_block_id());

    PSTLOG_TRACE(this, "configure_deconfigure_scan - checked configuration");

    PSTLOG_TRACE(this, "configure_deconfigure_scan - deconfiguring scan");
    handler->deconfigure_scan();
    EXPECT_FALSE(handler->is_scan_configured());
    PSTLOG_TRACE(this, "configure_deconfigure_scan - scan deconfigured");

    PSTLOG_TRACE(this, "configure_deconfigure_scan - deconfiguring beam");
    handler->deconfigure_beam();
    EXPECT_FALSE(handler->is_beam_configured()); // NOLINT
    PSTLOG_TRACE(this, "configure_deconfigure_scan - beam deconfigured");
  }

  TEST_F(DspDiskLmcServiceHandlerTest, configure_scan_with_invalid_configuration) // NOLINT
  {
    PSTLOG_TRACE(this, "configure_scan_with_invalid_configuration - configuring beam");
    configure_beam();
    PSTLOG_TRACE(this, "configure_scan_with_invalid_configuration - beam configured");

    PSTLOG_TRACE(this, "configure_scan_with_invalid_configuration - configuring scan");
    EXPECT_FALSE(handler->is_scan_configured());
    static constexpr double bad_double = 1e24;
    data_scan_config.set("BYTES_PER_SECOND", bad_double);
    data_scan_config.set("SCANLEN_MAX", bad_double);
    try
    {
      configure_scan();
      FAIL() << " expected configure_scan to throw exception due to not having beam configured.\n";
    }
    catch (std::exception &ex)
    {
      EXPECT_EQ(_dsp->get_state(), ska::pst::common::State::BeamConfigured);
      EXPECT_FALSE(handler->is_scan_configured());
      EXPECT_TRUE(handler->is_beam_configured());

      PSTLOG_TRACE(this, "configure_scan_with_invalid_configuration deconfiguring beam");
      handler->deconfigure_beam();
      EXPECT_EQ(_dsp->get_state(), ska::pst::common::State::Idle);
      PSTLOG_TRACE(this, "configure_scan_with_invalid_configuration deconfiguring beam");
    }
  }

  TEST_F(DspDiskLmcServiceHandlerTest, configure_scan_when_not_beam_configured) // NOLINT
  {
    PSTLOG_TRACE(this, "configure_scan_when_not_beam_configured - configuring scan");

    try
    {
      configure_scan();
      FAIL() << " expected configure_scan to throw exception due to not having beam configured.\n";
    }
    catch (ska::pst::common::LmcServiceException &ex)
    {
      EXPECT_EQ(std::string(ex.what()), "DSP.DISK not configured for beam.");
      EXPECT_EQ(ex.error_code(), ska::pst::lmc::ErrorCode::NOT_CONFIGURED_FOR_BEAM);
      EXPECT_EQ(ex.status_code(), grpc::StatusCode::FAILED_PRECONDITION);
    }
  }

  TEST_F(DspDiskLmcServiceHandlerTest, configure_scan_again_should_throw_exception) // NOLINT
  {
    PSTLOG_TRACE(this, "configure_scan_again_should_throw_exception - configuring beam");
    EXPECT_FALSE(handler->is_beam_configured()); // NOLINT
    configure_beam();
    EXPECT_TRUE(handler->is_beam_configured());
    PSTLOG_TRACE(this, "configure_scan_again_should_throw_exception - beam configured");

    PSTLOG_TRACE(this, "configure_scan_again_should_throw_exception - configuring scan");
    EXPECT_FALSE(handler->is_scan_configured());
    configure_scan();
    EXPECT_TRUE(handler->is_scan_configured());
    PSTLOG_TRACE(this, "configure_scan_again_should_throw_exception - scan configured");

    try
    {
      configure_scan(false);
      FAIL() << " expected configure_scan to throw exception due to scan already configured.\n";
    }
    catch (ska::pst::common::LmcServiceException &ex)
    {
      EXPECT_EQ(std::string(ex.what()), "Scan already configured for DSP.DISK.");
      EXPECT_EQ(ex.error_code(), ska::pst::lmc::ErrorCode::CONFIGURED_FOR_SCAN_ALREADY);
      EXPECT_EQ(ex.status_code(), grpc::StatusCode::FAILED_PRECONDITION);
    }
  }

  TEST_F(DspDiskLmcServiceHandlerTest, configure_scan_should_have_dsp_object) // NOLINT
  {
    PSTLOG_TRACE(this, "configure_scan_should_have_dsp_object - configuring beam");
    configure_beam();
    PSTLOG_TRACE(this, "configure_scan_should_have_dsp_object - beam configured");

    PSTLOG_TRACE(this, "configure_scan_should_have_dsp_object - configuring scan");
    try
    {
      ska::pst::lmc::ScanConfiguration configuration;
      configuration.mutable_test();
      handler->configure_scan(configuration);
      FAIL() << " expected configure_scan to throw exception due not having DSP.DISK field.\n";
    }
    catch (ska::pst::common::LmcServiceException &ex)
    {
      EXPECT_EQ(std::string(ex.what()), "Expected a DSP.DISK scan configuration object, but none were provided.");
      EXPECT_EQ(ex.error_code(), ska::pst::lmc::ErrorCode::INVALID_REQUEST);
      EXPECT_EQ(ex.status_code(), grpc::StatusCode::INVALID_ARGUMENT);
    }
  }

  TEST_F(DspDiskLmcServiceHandlerTest, deconfigure_scan_when_not_configured) // NOLINT
  {
    PSTLOG_TRACE(this, "deconfigure_scan_when_not_configured - deconfiguring scan");

    try
    {
      handler->deconfigure_scan();
      FAIL() << " expected deconfigure_scan to throw exception due to beam not configured.\n";
    }
    catch (ska::pst::common::LmcServiceException &ex)
    {
      EXPECT_EQ(std::string(ex.what()), "Scan not currently configured for DSP.DISK.");
      EXPECT_EQ(ex.error_code(), ska::pst::lmc::ErrorCode::NOT_CONFIGURED_FOR_SCAN);
      EXPECT_EQ(ex.status_code(), grpc::StatusCode::FAILED_PRECONDITION);
    }
  }

  TEST_F(DspDiskLmcServiceHandlerTest, get_scan_configuration_when_not_configured) // NOLINT
  {
    PSTLOG_TRACE(this, "get_scan_configuration_when_not_configured - getting scan configuration");

    try
    {
      ska::pst::lmc::ScanConfiguration scan_configuration;
      handler->get_scan_configuration(&scan_configuration);
      FAIL() << " expected deconfigure_beam to throw exception due to beam not configured.\n";
    }
    catch (ska::pst::common::LmcServiceException &ex)
    {
      EXPECT_EQ(std::string(ex.what()), "Not currently configured for DSP.DISK.");
      EXPECT_EQ(ex.error_code(), ska::pst::lmc::ErrorCode::NOT_CONFIGURED_FOR_SCAN);
      EXPECT_EQ(ex.status_code(), grpc::StatusCode::FAILED_PRECONDITION);
    }
  }

  TEST_F(DspDiskLmcServiceHandlerTest, start_scan_stop_scan) // NOLINT
  {
    PSTLOG_TRACE(this, "start_scan_stop_scan - configuring beam");
    configure_beam();
    PSTLOG_TRACE(this, "start_scan_stop_scan - beam configured");

    PSTLOG_TRACE(this, "start_scan_stop_scan - configuring scan");
    configure_scan();
    PSTLOG_TRACE(this, "start_scan_stop_scan - scan configured");

    uint64_t scan_id = generate_scan_id();
    PSTLOG_TRACE(this, "start_scan_stop_scan - starting scan with SCAN_ID={}", scan_id); //NOLINT
    EXPECT_FALSE(handler->is_scanning());
    start_scan(scan_id);
    EXPECT_TRUE(handler->is_scanning());

    // sleep for bit, want to wait so we can stop.
    usleep(ska::pst::common::microseconds_per_decisecond);
    PSTLOG_TRACE(this, "start_scan_stop_scan - scanning");

    PSTLOG_TRACE(this, "start_scan_stop_scan - ending scan");
    handler->stop_scan();
    EXPECT_FALSE(handler->is_scanning());
    PSTLOG_TRACE(this, "start_scan_stop_scan - scan ended");

    PSTLOG_TRACE(this, "start_scan_stop_scan - deconfiguring scan");
    handler->deconfigure_scan();
    PSTLOG_TRACE(this, "start_scan_stop_scan - scan deconfigured");

    PSTLOG_TRACE(this, "start_scan_stop_scan - deconfiguring beam");
    handler->deconfigure_beam();
    PSTLOG_TRACE(this, "start_scan_stop_scan - beam deconfigured");
  }

  TEST_F(DspDiskLmcServiceHandlerTest, start_scan_when_scanning) // NOLINT
  {
    PSTLOG_TRACE(this, "start_scan_when_scanning - configuring beam");
    configure_beam();
    PSTLOG_TRACE(this, "start_scan_when_scanning - beam configured");

    PSTLOG_TRACE(this, "start_scan_when_scanning - configuring scan");
    configure_scan();
    PSTLOG_TRACE(this, "start_scan_when_scanning - scan configured");

    uint64_t scan_id = generate_scan_id();
    PSTLOG_TRACE(this, "start_scan_when_scanning - starting scan with SCAN_ID={}", scan_id); //NOLINT
    EXPECT_FALSE(handler->is_scanning());
    start_scan(scan_id);
    EXPECT_TRUE(handler->is_scanning());

    // sleep for bit, want to wait so we can stop.
    usleep(ska::pst::common::microseconds_per_decisecond);

    try
    {
      start_scan(scan_id);
    }
    catch (ska::pst::common::LmcServiceException &ex)
    {
      EXPECT_EQ(std::string(ex.what()), "DSP.DISK is already scanning.");
      EXPECT_EQ(ex.error_code(), ska::pst::lmc::ErrorCode::ALREADY_SCANNING);
      EXPECT_EQ(ex.status_code(), grpc::StatusCode::FAILED_PRECONDITION);
    }
  }

  TEST_F(DspDiskLmcServiceHandlerTest, start_scan_when_not_configured_should_throw_exception) // NOLINT
  {
    PSTLOG_TRACE(this, "start_scan_when_not_configured_should_throw_exception - configuring beam");
    configure_beam();
    PSTLOG_TRACE(this, "start_scan_when_not_configured_should_throw_exception - beam configured");

    PSTLOG_TRACE(this, "start_scan_when_not_configured_should_throw_exception - start scan");
    EXPECT_FALSE(handler->is_scan_configured());

    try
    {
      uint64_t scan_id = generate_scan_id();
      start_scan(scan_id, false);
      FAIL() << " expected start_scan to throw exception due to scan not being configured.\n";
    }
    catch (ska::pst::common::LmcServiceException &ex)
    {
      EXPECT_EQ(std::string(ex.what()), "Not currently configured for DSP.DISK.");
      EXPECT_EQ(ex.error_code(), ska::pst::lmc::ErrorCode::NOT_CONFIGURED_FOR_SCAN);
      EXPECT_EQ(ex.status_code(), grpc::StatusCode::FAILED_PRECONDITION);
    }
  }

  TEST_F(DspDiskLmcServiceHandlerTest, start_scan_again_should_throw_exception) // NOLINT
  {
    PSTLOG_TRACE(this, "start_scan_again_should_throw_exception - configuring beam");
    configure_beam();
    PSTLOG_TRACE(this, "start_scan_again_should_throw_exception - beam configured");

    PSTLOG_TRACE(this, "start_scan_again_should_throw_exception - configuring scan");
    configure_scan();
    PSTLOG_TRACE(this, "start_scan_again_should_throw_exception - scan configured");

    uint64_t scan_id = generate_scan_id();
    PSTLOG_TRACE(this, "start_scan_again_should_throw_exception - starting scan with SCAN_ID={}", scan_id); //NOLINT
    EXPECT_FALSE(handler->is_scanning());
    start_scan(scan_id);
    EXPECT_TRUE(handler->is_scanning());
    PSTLOG_TRACE(this, "start_scan_again_should_throw_exception - scanning");

    try
    {
      usleep(ska::pst::common::microseconds_per_decisecond);
      start_scan(scan_id, false);
      FAIL() << " expected start_scan to throw exception due to already scanning.\n";
    }
    catch (ska::pst::common::LmcServiceException &ex)
    {
      EXPECT_EQ(std::string(ex.what()), "DSP.DISK is already scanning.");
      EXPECT_EQ(ex.error_code(), ska::pst::lmc::ErrorCode::ALREADY_SCANNING);
      EXPECT_EQ(ex.status_code(), grpc::StatusCode::FAILED_PRECONDITION);
    }
  }

  TEST_F(DspDiskLmcServiceHandlerTest, stop_scan_when_not_scanning_should_throw_exception) // NOLINT
  {
    PSTLOG_TRACE(this, "stop_scan_when_not_scanning_should_throw_exception - end scan");
    EXPECT_FALSE(handler->is_scanning());

    try
    {
      handler->stop_scan();
      FAIL() << " expected stop_scan to throw exception due to not currently scanning.\n";
    }
    catch (ska::pst::common::LmcServiceException &ex)
    {
      EXPECT_EQ(std::string(ex.what()), "Received stop_scan request when DSP.DISK is not scanning.");
      EXPECT_EQ(ex.error_code(), ska::pst::lmc::ErrorCode::NOT_SCANNING);
      EXPECT_EQ(ex.status_code(), grpc::StatusCode::FAILED_PRECONDITION);
    }
  }

  // test getting monitoring data
  TEST_F(DspDiskLmcServiceHandlerTest, get_monitor_data) // NOLINT
  {
    // configure beam
    PSTLOG_TRACE(this, "get_monitor_data - configuring beam");
    configure_beam();
    PSTLOG_TRACE(this, "get_monitor_data - beam configured");

    // configure
    PSTLOG_TRACE(this, "get_monitor_data - configuring scan");
    configure_scan();
    PSTLOG_TRACE(this, "get_monitor_data - scan configured");

    uint64_t scan_id = generate_scan_id();
    PSTLOG_TRACE(this, "get_monitor_data - starting scan with SCAN_ID={}", scan_id); //NOLINT
    EXPECT_FALSE(handler->is_scanning());
    start_scan(scan_id);
    usleep(ska::pst::common::microseconds_per_decisecond);
    PSTLOG_TRACE(this, "get_monitor_data - scanning");

    PSTLOG_TRACE(this, "get_monitor_data - monitoring");
    ska::pst::lmc::MonitorData monitor_data;
    handler->get_monitor_data(&monitor_data);
    const auto &dsp_disk_stats = _dsp->get_disk_stats();

    EXPECT_TRUE(monitor_data.has_dsp_disk());
    const auto &dsp_monitor_data = monitor_data.dsp_disk();

    EXPECT_EQ(dsp_monitor_data.disk_capacity(), dsp_disk_stats.capacity);         // NOLINT
    EXPECT_EQ(dsp_monitor_data.disk_available_bytes(), dsp_disk_stats.available); // NOLINT
    EXPECT_EQ(dsp_monitor_data.bytes_written(), dsp_disk_stats.bytes_written);    // NOLINT
    EXPECT_EQ(dsp_monitor_data.write_rate(), dsp_disk_stats.data_write_rate);     // NOLINT

    // end scan
    PSTLOG_TRACE(this, "get_monitor_data - ending scan");
    handler->stop_scan();
    PSTLOG_TRACE(this, "get_monitor_data - scan ended");

    PSTLOG_TRACE(this, "get_monitor_data - deconfiguring scan");
    handler->deconfigure_scan();
    PSTLOG_TRACE(this, "get_monitor_data - scan deconfigured");

    PSTLOG_TRACE(this, "get_monitor_data - deconfiguring beam");
    handler->deconfigure_beam();
    PSTLOG_TRACE(this, "get_monitor_data - beam deconfigured");
  }

  TEST_F(DspDiskLmcServiceHandlerTest, get_monitor_data_when_not_scanning_should_throw_exception) // NOLINT
  {
    PSTLOG_TRACE(this, "get_monitor_data_when_not_scanning_should_throw_exception - end scan");
    EXPECT_FALSE(handler->is_scanning());

    try
    {
      ska::pst::lmc::MonitorData monitor_data;
      handler->get_monitor_data(&monitor_data);
      FAIL() << " expected get_monitor_data to throw exception due to not currently scanning.\n";
    }
    catch (ska::pst::common::LmcServiceException &ex)
    {
      EXPECT_EQ(std::string(ex.what()), "Received get_monitor_data request when DSP.DISK is not scanning.");
      EXPECT_EQ(ex.error_code(), ska::pst::lmc::ErrorCode::NOT_SCANNING);
      EXPECT_EQ(ex.status_code(), grpc::StatusCode::FAILED_PRECONDITION);
    }
  }

  TEST_F(DspDiskLmcServiceHandlerTest, get_env) // NOLINT
  {
    ska::pst::lmc::GetEnvironmentResponse response;
    handler->get_env(&response);
    auto stats = _dsp->get_disk_stats();

    EXPECT_EQ(response.values().size(), 2);
    auto values = response.values();

    EXPECT_EQ(values.count("disk_capacity"), 1);
    EXPECT_TRUE(values["disk_capacity"].has_unsigned_int_value());
    EXPECT_EQ(values["disk_capacity"].unsigned_int_value(), stats.capacity);

    EXPECT_EQ(values.count("disk_available_bytes"), 1);
    EXPECT_TRUE(values["disk_available_bytes"].has_unsigned_int_value());
    EXPECT_EQ(values["disk_available_bytes"].unsigned_int_value(), stats.available);
  }

  TEST_F(DspDiskLmcServiceHandlerTest, go_to_runtime_error) // NOLINT
  {
    PSTLOG_TRACE(this, "go_to_runtime_error");

    try
    {
      throw std::runtime_error("this is a test error");
    }
    catch (...)
    {
      handler->go_to_runtime_error(std::current_exception());
    }

    ASSERT_EQ(ska::pst::common::RuntimeError, _dsp->get_state());
    ASSERT_EQ(ska::pst::common::RuntimeError, handler->get_application_manager_state());

    try
    {
      if (handler->get_application_manager_exception())
      {
        std::rethrow_exception(handler->get_application_manager_exception());
      }
      else
      {
        // the exception should be set and not null
        ASSERT_FALSE(true);
      }
    }
    catch (const std::exception &exc)
    {
      ASSERT_EQ("this is a test error", std::string(exc.what()));
    }
  }

  TEST_F(DspDiskLmcServiceHandlerTest, reset_non_runtime_error_state) // NOLINT
  {
    PSTLOG_TRACE(this, "reset_non_runtime_error_state - configuring beam");
    configure_beam();
    PSTLOG_TRACE(this, "reset_non_runtime_error_state - beam configured");

    PSTLOG_TRACE(this, "reset_non_runtime_error_state - configuring scan");
    EXPECT_FALSE(handler->is_scan_configured());
    configure_scan();
    EXPECT_TRUE(handler->is_scan_configured());
    PSTLOG_TRACE(this, "reset_non_runtime_error_state - scan configured");

    PSTLOG_TRACE(this, "reset_non_runtime_error_state - resetting");
    handler->reset();
    EXPECT_TRUE(handler->is_scan_configured());
    EXPECT_TRUE(handler->is_beam_configured());

    EXPECT_EQ(ska::pst::common::State::ScanConfigured, handler->get_application_manager_state());
    PSTLOG_TRACE(this, "reset_non_runtime_error_state - reset");
  }

  TEST_F(DspDiskLmcServiceHandlerTest, reset_from_runtime_error_state) // NOLINT
  {
    PSTLOG_TRACE(this, "reset_from_runtime_error_state - configuring beam");
    configure_beam();
    PSTLOG_TRACE(this, "reset_from_runtime_error_state - beam configured");

    PSTLOG_TRACE(this, "reset_from_runtime_error_state - configuring scan");
    EXPECT_FALSE(handler->is_scan_configured());
    configure_scan();
    EXPECT_TRUE(handler->is_scan_configured());
    PSTLOG_TRACE(this, "reset_from_runtime_error_state - scan configured");

    try
    {
      throw std::runtime_error("force fault state");
    }
    catch (...)
    {
      handler->go_to_runtime_error(std::current_exception());
    }

    PSTLOG_TRACE(this, "reset_from_runtime_error_state - resetting");
    handler->reset();
    EXPECT_FALSE(handler->is_scan_configured());
    EXPECT_FALSE(handler->is_beam_configured());

    EXPECT_EQ(ska::pst::common::State::Idle, handler->get_application_manager_state());
    PSTLOG_TRACE(this, "reset_from_runtime_error_state - reset");
  }

  TEST_F(DspDiskLmcServiceHandlerTest, restart_from_beam_configured) // NOLINT
  {
    GTEST_FLAG_SET(death_test_style, "threadsafe");
    PSTLOG_INFO(this, "restart_from_beam_configured - restart");
    // Assert that calling restart will eventually call exit(1)
    ASSERT_DEATH(
        {
            configure_beam();
            handler->restart();  // Call the restart method
        },".*"  // Optional: Regex to match any output from the process (can be empty)
    );
    PSTLOG_INFO(this, "restart_from_beam_configured - restarted");
    EXPECT_FALSE(handler->is_beam_configured());
  }

  TEST_F(DspDiskLmcServiceHandlerTest, restart_from_scan_configured) // NOLINT
  {
    GTEST_FLAG_SET(death_test_style, "threadsafe");
    PSTLOG_INFO(this, "restart_from_scan_configured - restart");
    // Assert that calling restart will eventually call exit(1)
    ASSERT_DEATH(
        {
            configure_beam();
            configure_scan();
            handler->restart();  // Call the restart method
        },".*"  // Optional: Regex to match any output from the process (can be empty)
    );
    PSTLOG_INFO(this, "restart_from_scan_configured - restarted");
    EXPECT_FALSE(handler->is_scan_configured());
    EXPECT_FALSE(handler->is_beam_configured());
  }

  TEST_F(DspDiskLmcServiceHandlerTest, restart_from_scanning) // NOLINT
  {
    GTEST_FLAG_SET(death_test_style, "threadsafe");
    PSTLOG_INFO(this, "restart_from_scanning - restart");
    // Assert that calling restart will eventually call exit(1)
    ASSERT_DEATH(
        {
            configure_beam();
            configure_scan();
            uint64_t scan_id = generate_scan_id(); //NOLINT
            start_scan(scan_id); //NOLINT
            handler->restart();  // Call the restart method
        },".*"  // Optional: Regex to match any output from the process (can be empty)
    );
    PSTLOG_INFO(this, "restart_from_scanning - restarted");
    EXPECT_FALSE(handler->is_scanning());
    EXPECT_FALSE(handler->is_scan_configured());
    EXPECT_FALSE(handler->is_beam_configured());
  }

  TEST_F(DspDiskLmcServiceHandlerTest, restart_from_runtime_error_state) // NOLINT
  {
    GTEST_FLAG_SET(death_test_style, "threadsafe");
    PSTLOG_INFO(this, "restart_from_runtime_error_state - restart");
    // Assert that calling restart will eventually call exit(1)
    ASSERT_DEATH(
        {
            configure_beam();
            configure_scan();

            // induce error
            try {
                throw std::runtime_error("force fault state");
            } catch (...) {
                handler->go_to_runtime_error(std::current_exception());
            }
            handler->restart();  // Call the restart method
        },".*"  // Optional: Regex to match any output from the process (can be empty)
    );
    PSTLOG_INFO(this, "restart_from_runtime_error_state - restarted");
    EXPECT_FALSE(handler->is_scan_configured());
    EXPECT_FALSE(handler->is_beam_configured());
  }

} // namespace ska::pst::dsp::test
