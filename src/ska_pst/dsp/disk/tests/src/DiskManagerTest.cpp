/*
 * Copyright 2022 Square Kilometre Array Observatory
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 * contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#include "ska_pst/common/definitions.h"
#include "ska_pst/common/utils/AsciiHeader.h"
#include "ska_pst/common/utils/FileReader.h"
#include "ska_pst/common/utils/FilePaths.h"
#include "ska_pst/common/utils/Timer.h"
#include "ska_pst/dsp/disk/tests/DiskManagerTest.h"
#include "ska_pst/common/testutils/GtestMain.h"

#include <iostream>
#include <thread>

using ska::pst::common::test::test_data_file;

auto main(int argc, char* argv[]) -> int
{
  return ska::pst::common::test::gtest_main(argc, argv);
}

namespace ska::pst::dsp::test {

DiskManagerTest::DiskManagerTest()
    : ::testing::Test()
{
}

void DiskManagerTest::SetUp()
{
  beam_config.load_from_file(test_data_file("beam_config.txt"));
  scan_config.load_from_file(test_data_file("scan_config.txt"));
  start_scan_config.load_from_file(test_data_file("start_scan_config.txt"));

  data_scan_config.load_from_file(test_data_file("data_scan_config.txt"));
  weights_scan_config.load_from_file(test_data_file("weights_scan_config.txt"));

  data_header.load_from_file(test_data_file("data_header.txt"));
  weights_header.load_from_file(test_data_file("weights_header.txt"));

  setup_data_block();
}

void DiskManagerTest::setup_data_block()
{
  static constexpr uint64_t header_nbufs = 4;
  static constexpr uint64_t header_bufsz = 4096;
  static constexpr uint64_t bufsz_factor = 1024;
  static constexpr unsigned nreaders = 1;
  static constexpr int device = -1;
  uint64_t data_bufsz = data_header.get<uint64_t>("RESOLUTION") * bufsz_factor;
  uint64_t weights_bufsz = weights_header.get<uint64_t>("RESOLUTION") * bufsz_factor;
  PSTLOG_DEBUG(this, "data_bufsz={} weights_bufsz={}", data_bufsz, weights_bufsz);

  // write 11 seconds of data to ensure multiple files are written out
  double data_buf_duration_seconds = data_header.get<double>("TSAMP") * data_header.get<double>("UDP_NSAMP") * bufsz_factor / ska::pst::common::microseconds_per_second;
  static constexpr double target_duration = 11;
  auto nbufs = static_cast<uint64_t>(round(target_duration / data_buf_duration_seconds));
  uint64_t data_nbufs = nbufs * 2;
  uint64_t weights_nbufs = nbufs * 2;
  PSTLOG_DEBUG(this, "data_buf_duration_seconds={} nbufs={} data_nbufs={} weights_nbufs={}", data_buf_duration_seconds, nbufs, data_nbufs, weights_nbufs);

  uint64_t data_nbytes = nbufs * data_bufsz;
  uint64_t weights_nbytes = nbufs * weights_bufsz;

  _dbc_data = std::make_unique<ska::pst::smrb::DataBlockCreate>(beam_config.get_val("DATA_KEY"));
  _dbc_data->create(header_nbufs, header_bufsz, data_nbufs, data_bufsz, nreaders, device);

  _writer_data = std::make_unique<ska::pst::smrb::DataBlockWrite>(beam_config.get_val("DATA_KEY"));
  _writer_data->connect(0);
  _writer_data->lock();

  _dbc_weights = std::make_unique<ska::pst::smrb::DataBlockCreate>(beam_config.get_val("WEIGHTS_KEY"));
  _dbc_weights->create(header_nbufs, header_bufsz, weights_nbufs, weights_bufsz, nreaders, device);

  _writer_weights = std::make_unique<ska::pst::smrb::DataBlockWrite>(beam_config.get_val("WEIGHTS_KEY"));
  _writer_weights->connect(0);
  _writer_weights->lock();

  data_to_write.resize(data_nbytes);
  weights_to_write.resize(weights_nbytes);

  _writer_data->write_config(data_scan_config.raw());
  _writer_weights->write_config(weights_scan_config.raw());

  _writer_data->write_header(data_header.raw());
  _writer_weights->write_header(weights_header.raw());
}

void DiskManagerTest::tear_down_data_block()
{
  if (_writer_data)
  {
    if (_writer_data->get_opened())
    {
      _writer_data->close();
    }
    if (_writer_data->get_locked())
    {
      _writer_data->unlock();
    }
    _writer_data->disconnect();
  }
  _writer_data = nullptr;

  if (_dbc_data)
  {
    _dbc_data->destroy();
  }
  _dbc_data = nullptr;
  if (_writer_weights)
  {
    if (_writer_weights->get_opened())
    {
      _writer_weights->close();
    }
    if (_writer_weights->get_locked())
    {
      _writer_weights->unlock();
    }
    _writer_weights->disconnect();
  }
  _writer_weights = nullptr;

  if (_dbc_weights)
  {
    _dbc_weights->destroy();
  }
  _dbc_weights = nullptr;
}

void DiskManagerTest::TearDown()
{
  tear_down_data_block();
}

auto DiskManagerTest::get_dada_file_data_size(const std::filesystem::path& dada_file) -> uint64_t
{
  ska::pst::common::FileReader file_reader(dada_file.generic_string());
  file_reader.read_header();
  auto data_size = static_cast<uint64_t>(file_reader.get_data_size());
  file_reader.close_file();
  return data_size;
}

TEST_F(DiskManagerTest, test_construct_delete) // NOLINT
{
  std::shared_ptr<DiskManager> dm = std::make_shared<DiskManager>(recording_base_path, use_o_direct);
}

TEST_F(DiskManagerTest, test_configure_from_file) // NOLINT
{
  DiskManager dm(recording_base_path, use_o_direct);
  ASSERT_FALSE(dm.is_beam_configured());
  PSTLOG_TRACE(this, "test_configure_from_file dm.is_beam_configured()={}",dm.is_beam_configured());
  ASSERT_FALSE(dm.is_scan_configured());
  PSTLOG_TRACE(this, "test_configure_from_file dm.is_scan_configured()={}",dm.is_scan_configured());
  dm.configure_from_file(test_data_file("DiskManager_config.txt"));
  ASSERT_TRUE(dm.is_beam_configured());
  ASSERT_TRUE(dm.is_scan_configured());
  ASSERT_TRUE(dm.is_scanning());

  _writer_data->open();
  _writer_data->write_data(&data_to_write[0], data_to_write.size());
  _writer_data->close();
  _writer_weights->open();
  _writer_weights->write_data(&weights_to_write[0], weights_to_write.size());
  _writer_weights->close();
}

TEST_F(DiskManagerTest, test_happy_path) // NOLINT
{
  PSTLOG_TRACE(this, "test_happy_path");

  _writer_data->open();
  _writer_data->write_data(&data_to_write[0], data_to_write.size());
  _writer_data->close();
  _writer_weights->open();
  _writer_weights->write_data(&weights_to_write[0], weights_to_write.size());
  _writer_weights->close();

  auto empty_header = ska::pst::common::AsciiHeader();
  auto expected_beam_config = ska::pst::common::AsciiHeader(beam_config);
  auto expected_scan_config = ska::pst::common::AsciiHeader(expected_beam_config);
  expected_scan_config.append_header(scan_config);

  DiskManager dm(recording_base_path, use_o_direct);
  ASSERT_FALSE(dm.is_beam_configured());
  dm.configure_beam(beam_config);
  ASSERT_TRUE(dm.is_beam_configured());
  ASSERT_FALSE(dm.is_scan_configured());
  ASSERT_FALSE(dm.is_scanning());
  ASSERT_EQ(expected_beam_config, dm.get_beam_configuration());
  ASSERT_THROW(dm.get_scan_configuration(), std::runtime_error);
  ASSERT_EQ(empty_header, dm.get_startscan_configuration());

  ASSERT_FALSE(dm.is_scan_configured());
  dm.configure_scan(scan_config);
  ASSERT_TRUE(dm.is_beam_configured());
  ASSERT_TRUE(dm.is_scan_configured());
  ASSERT_FALSE(dm.is_scanning());
  ASSERT_EQ(expected_beam_config, dm.get_beam_configuration());
  ASSERT_EQ(expected_scan_config, dm.get_scan_configuration());
  ASSERT_EQ(empty_header, dm.get_startscan_configuration());

  PSTLOG_TRACE(this, "test_happy_path dm.start_scan()");
  dm.start_scan(start_scan_config);
  usleep(ska::pst::common::microseconds_per_decisecond);
  dm.get_disk_stats();
  ASSERT_TRUE(dm.is_beam_configured());
  ASSERT_TRUE(dm.is_scan_configured());
  ASSERT_TRUE(dm.is_scanning());
  ASSERT_EQ(expected_beam_config, dm.get_beam_configuration());
  ASSERT_EQ(expected_scan_config, dm.get_scan_configuration());
  ASSERT_EQ(start_scan_config, dm.get_startscan_configuration());

  dm.stop_scan();
  ASSERT_TRUE(dm.is_beam_configured());
  ASSERT_TRUE(dm.is_scan_configured());
  ASSERT_FALSE(dm.is_scanning());
  ASSERT_EQ(expected_beam_config, dm.get_beam_configuration());
  ASSERT_EQ(expected_scan_config, dm.get_scan_configuration());
  ASSERT_EQ(empty_header, dm.get_startscan_configuration());

  dm.deconfigure_scan();
  ASSERT_TRUE(dm.is_beam_configured());
  ASSERT_FALSE(dm.is_scan_configured());
  ASSERT_FALSE(dm.is_scanning());
  ASSERT_EQ(expected_beam_config, dm.get_beam_configuration());
  ASSERT_THROW(dm.get_scan_configuration(), std::runtime_error);
  ASSERT_EQ(empty_header, dm.get_startscan_configuration());

  dm.deconfigure_beam();
  ASSERT_FALSE(dm.is_beam_configured());
  ASSERT_FALSE(dm.is_scan_configured());
  ASSERT_FALSE(dm.is_scanning());
  ASSERT_THROW(dm.get_beam_configuration(), std::runtime_error);
  ASSERT_THROW(dm.get_scan_configuration(), std::runtime_error);
  ASSERT_EQ(empty_header, dm.get_startscan_configuration());

  // assert the are 2 data and weights files (~10s and ~1s) written to the output path
  std::string utc_start = data_header.get_val("UTC_START");
  std::string eb_id =  start_scan_config.get_val("EB_ID");
  std::string telescope = scan_config.get_val("TELESCOPE");
  std::string scan_id = start_scan_config.get_val("SCAN_ID");

  // prepare the paths for data and weights files
  ska::pst::common::FilePaths paths(recording_base_path);
  std::filesystem::path scan_path = paths.get_scan_product_path(eb_id, telescope, scan_id);
  std::filesystem::path data_path = scan_path / "data";
  std::filesystem::path weights_path = scan_path / "weights";

  uint64_t data_obs_offset{0}, weights_obs_offset{0}, file_number{0};

  // assert that the first data and weights files exist with the correct names
  std::filesystem::path data_file = data_path / ska::pst::common::FileWriter::get_filename(utc_start, data_obs_offset, file_number);
  std::filesystem::path weights_file = weights_path / ska::pst::common::FileWriter::get_filename(utc_start, weights_obs_offset, file_number);
  PSTLOG_TRACE(this, "test_happy_path data_file={} weights_file={}", data_file.generic_string(), weights_file.generic_string());
  ASSERT_TRUE(std::filesystem::exists(data_file));
  ASSERT_TRUE(std::filesystem::exists(weights_file));

  data_obs_offset += get_dada_file_data_size(data_file);
  weights_obs_offset += get_dada_file_data_size(weights_file);
  file_number++;

  // assert that the second data and weights files exist with the correct names
  data_file = data_path / ska::pst::common::FileWriter::get_filename(utc_start, data_obs_offset, file_number);
  weights_path = weights_path / ska::pst::common::FileWriter::get_filename(utc_start, weights_obs_offset, file_number);
  PSTLOG_TRACE(this, "test_happy_path data_file={} weights_file={}", data_file.generic_string(), weights_file.generic_string());
  ASSERT_TRUE(std::filesystem::exists(data_file));
  ASSERT_TRUE(std::filesystem::exists(weights_file));
}

TEST_F(DiskManagerTest, test_multiple_scans) // NOLINT
{
  PSTLOG_TRACE(this, "test_multiple_scans");

  auto scan_id = start_scan_config.get<int32_t>("SCAN_ID");
  DiskManager dm(recording_base_path, use_o_direct);
  for (auto i = 0; i < 2; i++) {
    // update the scan ID in the configs
    data_scan_config.set("SCAN_ID", scan_id + i);
    weights_scan_config.set("SCAN_ID", scan_id + i);
    start_scan_config.set("SCAN_ID", scan_id + i);
    data_header.set("SCAN_ID", scan_id + i);
    weights_header.set("SCAN_ID", scan_id + i);

    if (i > 0)
    {
      // Note that setup_data_block is called by ::Setup and therefore already done for the first loop
      setup_data_block();
    }

    _writer_data->open();
    _writer_data->write_data(&data_to_write[0], data_to_write.size());
    _writer_data->close();
    _writer_weights->open();
    _writer_weights->write_data(&weights_to_write[0], weights_to_write.size());
    _writer_weights->close();

    ASSERT_FALSE(dm.is_beam_configured());
    dm.configure_beam(beam_config);
    ASSERT_TRUE(dm.is_beam_configured());

    ASSERT_FALSE(dm.is_scan_configured());
    dm.configure_scan(scan_config);
    ASSERT_TRUE(dm.is_scan_configured());

    PSTLOG_TRACE(this, "test_multiple_scans dm.start_scan()");
    PSTLOG_INFO(this, "test_multiple_scans Starting scan");
    dm.start_scan(start_scan_config);
    PSTLOG_INFO(this, "test_multiple_scans Scan started sleeping");
    usleep(ska::pst::common::microseconds_per_decisecond);
    PSTLOG_INFO(this, "test_multiple_scans Getting disk stats");
    dm.get_disk_stats();
    dm.stop_scan();

    dm.deconfigure_scan();
    ASSERT_FALSE(dm.is_scan_configured());

    dm.deconfigure_beam();
    ASSERT_FALSE(dm.is_beam_configured());

    tear_down_data_block();
  }
}

TEST_F(DiskManagerTest, test_multiple_scans_one_config) // NOLINT
{
  PSTLOG_TRACE(this, "test_multiple_scans_one_config");

  auto scan_id = start_scan_config.get<int32_t>("SCAN_ID");
  DiskManager dm(recording_base_path, use_o_direct);

  ASSERT_FALSE(dm.is_beam_configured());
  dm.configure_beam(beam_config);
  ASSERT_TRUE(dm.is_beam_configured());

  ASSERT_FALSE(dm.is_scan_configured());
  dm.configure_scan(scan_config);
  ASSERT_TRUE(dm.is_scan_configured());

  for (auto i = 0; i < 3; i++)
  {
    // update the scan ID in the configs
    data_scan_config.set("SCAN_ID", scan_id + i);
    weights_scan_config.set("SCAN_ID", scan_id + i);
    start_scan_config.set("SCAN_ID", scan_id + i);
    data_header.set("SCAN_ID", scan_id + i);
    weights_header.set("SCAN_ID", scan_id + i);

    if (i > 0)
    {
      /* Note that these are teh last two lines of setup_data_block,
        which is called by ::Setup and is therefore already done for the first loop */
      _writer_data->write_header(data_header.raw());
      _writer_weights->write_header(weights_header.raw());
    }

    _writer_data->open();
    _writer_data->write_data(&data_to_write[0], data_to_write.size());
    _writer_data->close();
    _writer_weights->open();
    _writer_weights->write_data(&weights_to_write[0], weights_to_write.size());
    _writer_weights->close();

    PSTLOG_TRACE(this, "test_multiple_scans_one_config dm.start_scan()");
    PSTLOG_INFO(this, "test_multiple_scans Starting scan");
    dm.start_scan(start_scan_config);
    PSTLOG_INFO(this, "test_multiple_scans Scan started sleeping");
    usleep(ska::pst::common::microseconds_per_decisecond);
    PSTLOG_INFO(this, "test_multiple_scans Getting disk stats");
    dm.get_disk_stats();
    dm.stop_scan();
  }

  dm.deconfigure_scan();
  ASSERT_FALSE(dm.is_scan_configured());

  dm.deconfigure_beam();
  ASSERT_FALSE(dm.is_beam_configured());

  tear_down_data_block();
}

TEST_F(DiskManagerTest, test_get_configuration) // NOLINT
{
  PSTLOG_TRACE(this, "test_get_configuration");
  DiskManager dm(recording_base_path, use_o_direct);
  dm.configure_beam(beam_config);

  ASSERT_EQ(beam_config.get_val("DATA_KEY"), dm.get_beam_configuration().get_val("DATA_KEY"));
  ASSERT_EQ(beam_config.get_val("WEIGHTS_KEY"), dm.get_beam_configuration().get_val("WEIGHTS_KEY"));

  dm.configure_scan(scan_config);
  ASSERT_EQ(scan_config.get<double>("SCANLEN_MAX"), dm.get_scan_configuration().get<double>("SCANLEN_MAX"));
}

} // namespace ska::pst::dsp::test
