HDR_SIZE          4096
HDR_VERSION       1.0

DATA_KEY          a000
WEIGHTS_KEY       a002

NCHAN             24
NPOL              2
NDIM              2
NBIT              16
TSAMP             207.36

UDP_NSAMP         32
WT_NSAMP          32
UDP_NCHAN         24

# NCHAN * NPOL * NDIM * NBIT/8 * UDP_NSAMP
RESOLUTION        6144

SCANLEN_MAX       300
UTC_START         1970-01-01-00:00:00
OBS_OFFSET        0
SCAN_ID           12345
EB_ID             eb-m001-20230921-12345
TELESCOPE         SKALow
