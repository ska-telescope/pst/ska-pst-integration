/*
 * Copyright 2022 Square Kilometre Array Observatory
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 * contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#include "ska_pst/common/utils/FilePaths.h"
#include "ska_pst/common/utils/Logging.h"
#include "ska_pst/dsp/disk/StreamWriter.h"

#include <ctime>
#include <fcntl.h>
#include <iostream>
#include <ostream>
#include <stdexcept>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>

ska::pst::dsp::StreamWriter::StreamWriter(ska::pst::dsp::DiskMonitor& monitor, const std::string& _entity) :
  ska::pst::common::ApplicationManager(_entity),
  disk_monitor(monitor),
  file_writer(false)
{
  PSTLOG_TRACE(this, "ctor initialise()");
  initialise();
}

ska::pst::dsp::StreamWriter::StreamWriter(ska::pst::dsp::DiskMonitor& monitor, const std::string& _entity, bool use_o_direct) :
  ska::pst::common::ApplicationManager(_entity),
  disk_monitor(monitor),
  file_writer(use_o_direct)
{
  PSTLOG_TRACE(this, "initialise() use_o_direct={}", use_o_direct);
  initialise();
}

ska::pst::dsp::StreamWriter::~StreamWriter()
{
  PSTLOG_DEBUG(this, "dtor quit()");
  quit();
}

void ska::pst::dsp::StreamWriter::perform_initialise()
{
  PSTLOG_DEBUG(this, "done");
}

void ska::pst::dsp::StreamWriter::validate_configure_beam(const ska::pst::common::AsciiHeader &config, ska::pst::common::ValidationContext *context)
{
  PSTLOG_TRACE(this, "config=\n{}", config.raw());
  // Always have these keys as this is called from DiskManager and the key is set
  std::string key = config.get_val("KEY");
  auto recording_base_path_str = config.get_val("RECORDING_BASE_PATH");
  auto stream_path_str = config.get_val("RECORDING_SUFFIX");

  PSTLOG_TRACE(this, "key={}", key);
  try {
    auto recording_base_path = std::filesystem::path(recording_base_path_str);
    PSTLOG_TRACE(this, "recording_base_path={}", recording_base_path.generic_string());
  } catch (std::exception& exc) {
    context->add_validation_error<std::string>("RECORDING_BASE_PATH", recording_base_path_str, std::string(exc.what()));
  }

  try {
    auto stream_path = std::filesystem::path(stream_path_str);
    PSTLOG_TRACE(this, "stream_path={}", stream_path.generic_string());
  } catch (std::exception& exc) {
    context->add_validation_error<std::string>("RECORDING_SUFFIX", stream_path_str, std::string(exc.what()));
  }
}

void ska::pst::dsp::StreamWriter::validate_configure_scan(const ska::pst::common::AsciiHeader &config, ska::pst::common::ValidationContext *context)
{
  PSTLOG_TRACE(this, "validation scan configuration");

  if (!config.has("EB_ID")) {
    context->add_missing_field_error("EB_ID");
  }
}

void ska::pst::dsp::StreamWriter::validate_start_scan(const ska::pst::common::AsciiHeader& /*config*/)
{
  PSTLOG_TRACE(this, "done");
}

void ska::pst::dsp::StreamWriter::perform_configure_beam()
{
  PSTLOG_TRACE(this, "beam_config.raw()=\n{}", beam_config.raw());

  // connect to the data block as a reader
  std::string key = beam_config.get_val("KEY");
  recording_base_path = std::filesystem::path(beam_config.get_val("RECORDING_BASE_PATH"));
  stream_path = std::filesystem::path(beam_config.get_val("RECORDING_SUFFIX"));
  timeout = 0;
  if (beam_config.has("TIMEOUT"))
  {
    timeout = beam_config.get<int32_t>("TIMEOUT");
  }

  PSTLOG_DEBUG(this, "instantiating DataBlockRead({})", key);
  db = std::make_unique<ska::pst::smrb::DataBlockRead>(key);

  if (skip_datablock_read_config)
  {
    db->set_skip_read_config();
  }

  PSTLOG_DEBUG(this, "db->connect({})", timeout);
  db->connect(timeout);

  PSTLOG_DEBUG(this, "db->lock()");
  db->lock();

  header_bufsz = db->get_header_bufsz();
  data_bufsz = db->get_data_bufsz();

  // check that the header and data buffer sizes are compatible with o_direct flags
  file_writer.check_block_size(header_bufsz);
  file_writer.check_block_size(data_bufsz);
}

void ska::pst::dsp::StreamWriter::perform_configure_scan()
{
  PSTLOG_DEBUG(this, "db->read_config()");
  db->read_config();

  // Configure the AsciiHeader from the data block's header
  PSTLOG_DEBUG(this, "scan_config.load_from_str(db->get_config())");
  scan_config.load_from_str(db->get_config());
  PSTLOG_DEBUG(this, "scan_config=\n{}", scan_config.raw());

  // ensure required header parameters are set
  scan_config.set("HDR_VERSION", "1.0");
  scan_config.set("HDR_SIZE", db->get_header_bufsz());

  bytes_per_second = scan_config.compute_bytes_per_second();
  resolution = scan_config.get<uint64_t>("RESOLUTION");

  // initialise the first file_number in the output data stream
  file_number = 0;

  // compute the bytes to be written to each file in the data stream
  bytes_written_per_file = static_cast<uint64_t>(floor(bytes_per_second * seconds_per_file));

  // ensure the bytes written per file is a multiple of the resolution
  uint64_t remainder = bytes_written_per_file % resolution;
  if (remainder > 0)
  {
    bytes_written_per_file += (resolution - remainder);
  }
  PSTLOG_DEBUG(this, "bytes_per_second={} seconds_per_file={} bytes_written_per_file={}", bytes_per_second, seconds_per_file, bytes_written_per_file);
}

void ska::pst::dsp::StreamWriter::perform_start_scan()
{
  PSTLOG_TRACE(this, "done");
}

void ska::pst::dsp::StreamWriter::perform_scan()
{
  // wait for the header to be written to the db
  PSTLOG_DEBUG(this, "db->read_header()");
  db->read_header();

  header.load_from_str(db->get_header());
  PSTLOG_TRACE(this, "db->get_header()=\n{}", db->get_header());
  PSTLOG_TRACE(this, "startscan_config.raw()=\n{}", startscan_config.raw());
  auto eb_id = scan_config.get_val("EB_ID");
  auto telescope = scan_config.get_val("TELESCOPE");
  auto scan_id = startscan_config.get_val("SCAN_ID");

  // Load TELESCOPE from SMRB and convert to equivalent subsystem path
  ska::pst::common::FilePaths paths(recording_base_path);
  scan_path = paths.get_scan_product_path(eb_id, telescope, scan_id);
  PSTLOG_DEBUG(this, "scan_path={}", scan_path.generic_string());
  utc_start = header.get_val("UTC_START");
  obs_offset = header.get<uint64_t>("OBS_OFFSET");
  PSTLOG_DEBUG(this, "SCAN_ID={} UTC_START={} OBS_OFFSET={}", startscan_config.get_val("SCAN_ID"), utc_start, obs_offset);

  PSTLOG_DEBUG(this, "db->open()");
  db->open();

  output_path = scan_path / stream_path;
  PSTLOG_DEBUG(this, "create_directories({})", output_path.generic_string());
  create_directories(output_path);

  // ASSUME: bytes_written_per_file >> buffer_bytes_available
  char * block_buffer{nullptr};
  uint64_t buffer_bytes_available{0}, bytes_read_from_buffer{0};
  uint64_t bytes_written_to_file{0};

  bool eod = false;
  while (!eod)
  {
    // ensure there is a source of data
    if (block_buffer == nullptr)
    {
      PSTLOG_DEBUG(this, "opening block");
      block_buffer = db->open_block();
      buffer_bytes_available = db->get_buf_bytes();
      bytes_read_from_buffer = 0;
      PSTLOG_DEBUG(this, "opened block containing {} bytes", buffer_bytes_available);

      if (block_buffer == nullptr)
      {
        PSTLOG_DEBUG(this, "encountered end of data");
        eod = true;
      }
    }

    // ensure there is a sink for data
    if (!file_writer.is_file_open())
    {
      std::filesystem::path file_name = file_writer.get_filename(utc_start, obs_offset, file_number);
      file_writer.open_file(output_path / file_name);
      header.set("OBS_OFFSET", obs_offset);
      header.set("FILE_NUMBER", file_number);
      ssize_t bytes_written = file_writer.write_header(header);
      disk_monitor.increment_bytes(bytes_written);
      bytes_written_to_file = 0;
    }

    // compute the number of bytes to write as constrained by the max file size or the space left in the current block buffer
    uint64_t bytes_can_write = bytes_written_per_file - bytes_written_to_file;
    uint64_t bytes_can_read = buffer_bytes_available - bytes_read_from_buffer;
    uint64_t bytes_to_write = std::min(bytes_can_write, bytes_can_read);
    PSTLOG_TRACE(this, "bytes_can_write={} bytes_can_read={} bytes_to_write={}", bytes_can_write, bytes_can_read, bytes_to_write);

    // write the specified amount to the file
    size_t bytes_just_written = file_writer.write_data(block_buffer, bytes_to_write);
    disk_monitor.increment_bytes(bytes_just_written);
    PSTLOG_TRACE(this, "write_data bytes_just_written={} bytes_written={}", bytes_just_written, file_writer.get_data_bytes_written());

    // adjust in the block buffer pointer and counters
    block_buffer += bytes_just_written; // NOLINT
    bytes_written_to_file += bytes_just_written;
    bytes_read_from_buffer += bytes_just_written;

    // close the data buffer once the required number of bytes have been read
    if (block_buffer && bytes_read_from_buffer >= buffer_bytes_available)
    {
      PSTLOG_TRACE(this, "db->close_block({})", buffer_bytes_available);
      db->close_block(buffer_bytes_available);
      block_buffer = nullptr;
      buffer_bytes_available = 0;
      bytes_read_from_buffer = 0;
    }

    // close the output file once the required number of bytes have been written
    if (bytes_written_to_file >= bytes_written_per_file)
    {
      file_writer.close_file();
      obs_offset += file_writer.get_data_bytes_written();
      file_number++;
    }
  }

  PSTLOG_DEBUG(this, "EoD detected on input");
  if (file_writer.is_file_open())
  {
    if (bytes_written_to_file > 0)
    {
      file_writer.close_file();
    }
    else
    {
      file_writer.remove_file();
    }
  }

  PSTLOG_DEBUG(this, "db->close()");
  db->close();
  mark_scan("scan_completed");
  header.reset();

  PSTLOG_DEBUG(this, "done");
}

void ska::pst::dsp::StreamWriter::mark_scan(const std::string& state)
{
  PSTLOG_DEBUG(this, "state={}", state);
  ska::pst::common::FileWriter fw(false);

  // create file
  std::filesystem::path file_name = std::filesystem::path(state);
  fw.open_file(scan_path / file_name);
  // close file
  fw.close_file();
}

void ska::pst::dsp::StreamWriter::perform_stop_scan()
{
  PSTLOG_DEBUG(this, "startscan_config.reset()");
  startscan_config.reset();
  PSTLOG_DEBUG(this, "perform_stop_scan complete");
}

void ska::pst::dsp::StreamWriter::perform_deconfigure_scan()
{
  PSTLOG_DEBUG(this, "scan_config.reset()");
  scan_config.reset();
}

void ska::pst::dsp::StreamWriter::perform_deconfigure_beam()
{
  PSTLOG_DEBUG(this, "resetting bufsz");
  header_bufsz = 0;
  data_bufsz = 0;

  PSTLOG_DEBUG(this, "db->unlock()");
  db->unlock();

  PSTLOG_DEBUG(this, "db->disconnect()");
  db->disconnect();

  PSTLOG_DEBUG(this, "file_writer.deconfigure()");
  file_writer.deconfigure();

  PSTLOG_DEBUG(this, "db.reset()");
  db.reset();
  beam_config.reset();
}

void ska::pst::dsp::StreamWriter::set_extra_headers(const ska::pst::common::AsciiHeader &_header)
{
  header.append_header(_header);
}
