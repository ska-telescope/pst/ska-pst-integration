/*
 * Copyright 2022 Square Kilometre Array Observatory
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 * contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#include "ska_pst/common/definitions.h"
#include "ska_pst/common/utils/Logging.h"
#include "ska_pst/dsp/disk/DiskMonitor.h"

#include <cstdint>
#include <filesystem>
#include <iostream>
#include <stdexcept>
#include <sstream>
#include <regex>
#include <unistd.h>

ska::pst::dsp::DiskMonitor::DiskMonitor(std::string recording_path) : ska::pst::common::ApplicationManager("DiskMonitor"),
                                                                      file_system(std::move(recording_path))
{
  PSTLOG_DEBUG(this, "ctor initialise()");
  initialise();
}

ska::pst::dsp::DiskMonitor::~DiskMonitor()
{
  PSTLOG_DEBUG(this, "dtor quit()");
  quit();

  PSTLOG_DEBUG(this, "dtor monitor_thread->join()");
  monitor_thread->join();
}

void ska::pst::dsp::DiskMonitor::validate_configure_beam(const ska::pst::common::AsciiHeader & /*config*/, ska::pst::common::ValidationContext * /* context */)
{
  PSTLOG_DEBUG(this, "done");
}

void ska::pst::dsp::DiskMonitor::validate_configure_scan(const ska::pst::common::AsciiHeader &config, ska::pst::common::ValidationContext *context)
{
  PSTLOG_DEBUG(this, "validating scan configuration");

  double bytes_per_second{0.0};
  double scan_len_max{0.0};

  ska::pst::common::ValidationContext subcontext;
  if (config.has("BYTES_PER_SECOND"))
  {
    try
    {
      bytes_per_second = config.get<double>("BYTES_PER_SECOND");
    }
    catch (...)
    {
      subcontext.add_validation_error("BYTES_PER_SECOND", config.get_val("BYTES_PER_SECOND"), "not a numeric value");
    }
  }
  else
  {
    try
    {
      bytes_per_second = config.compute_bytes_per_second();
    }
    catch(std::exception& error)
    {
      PSTLOG_ERROR(this, "compute_bytes_per_second failed: {}", error.what());
      subcontext.add_missing_field_error("BYTES_PER_SECOND");
    }
  }

  if (config.has("SCANLEN_MAX"))
  {
    try
    {
      scan_len_max = config.get<double>("SCANLEN_MAX");
    }
    catch (...)
    {
      subcontext.add_validation_error("SCANLEN_MAX", config.get_val("SCANLEN_MAX"), "not a numeric value");
    }
  }
  else
  {
    subcontext.add_missing_field_error("SCANLEN_MAX");
  }

  if (config.has("EB_ID"))
  {
    // Regex comes from SKA Telemodel example eb-m001-20230921-12345.
    const std::string config_value_pattern = R"(^eb\-[a-z0-9]+\-[0-9]{8}\-[a-z0-9]+$)";
    std::regex regex_pattern(config_value_pattern);
    auto config_value = config.get_val("EB_ID");
    if (!std::regex_match(config_value, regex_pattern))
    {
      subcontext.add_value_regex_error("EB_ID", config_value, config_value_pattern);
    }
  }
  else
  {
    subcontext.add_missing_field_error("EB_ID");
  }

  if (!subcontext.is_empty())
  {
    context->copy_errors(subcontext);
    return;
  }

  // ensure the data rate can handle at least a Mid.CBF 16-bit frequency slice (2078 MB/s)
  static constexpr double bytes_per_gigabyte = 1073741824;
  static constexpr double max_bytes_per_second = 2.5 * bytes_per_gigabyte;

  if ((bytes_per_second <= 0) || (bytes_per_second > max_bytes_per_second))
  {
    PSTLOG_ERROR(this, "bytes_per_second={} which was not in the range [0 .. 2.5 GB/s]", max_bytes_per_second);
    std::ostringstream ss;
    ss << "value not between 0.0 and ";
    ss << bytes_per_gigabyte;
    ss << " bytes/sec";
    context->add_validation_error("BYTES_PER_SECOND", bytes_per_second, ss.str());
  }

  static constexpr double seconds_per_day = 86400;
  if (scan_len_max <= 0 or scan_len_max >= seconds_per_day)
  {
    PSTLOG_ERROR(this, "scan_len_max={} which is not in the range of [0 .. 86400s]", scan_len_max);
    std::ostringstream ss;
    ss << "value not between 0.0 and ";
    ss << seconds_per_day;
    ss << " seconds";
    context->add_validation_error("SCANLEN_MAX", scan_len_max, ss.str());
  }

  const double time_available = static_cast<double>(file_system_available) / bytes_per_second;
  PSTLOG_DEBUG(this, "bytes_per_second={}, file_system_available={}, time_available={}", bytes_per_second, file_system_available, time_available);
  if (static_cast<double>(scan_len_max) > time_available)
  {
    PSTLOG_WARN(this, "SCANLEN_MAX[{}] > time_available[{}], file_system_available={}", scan_len, time_available, file_system_available);
    std::ostringstream ss;
    ss << "value greater than ";
    ss << time_available;
    ss << " seconds available of recording time at a rate of ";
    ss << bytes_per_second;
    ss << " bytes/sec";
    context->add_validation_error("SCANLEN_MAX", scan_len_max, ss.str());
  }
}

void ska::pst::dsp::DiskMonitor::validate_start_scan(const ska::pst::common::AsciiHeader & /*config*/)
{
  PSTLOG_DEBUG(this, "done");
}

void ska::pst::dsp::DiskMonitor::perform_configure_beam()
{
  PSTLOG_DEBUG(this, "done");
}

void ska::pst::dsp::DiskMonitor::perform_configure_scan()
{
  PSTLOG_DEBUG(this, "performing configure scan");

  expected_bytes_per_second = scan_config.get<double>("BYTES_PER_SECOND");
  recording_time_available = static_cast<double>(file_system_available) / expected_bytes_per_second;
  PSTLOG_DEBUG(this, "expected_bytes_per_second={}, file_system_available={}, recording_time_available={}", expected_bytes_per_second, file_system_available, recording_time_available);

  scan_len = scan_config.get<double>("SCANLEN_MAX");
  PSTLOG_DEBUG(this, "scan_len={}", scan_len);
}

void ska::pst::dsp::DiskMonitor::perform_start_scan()
{
  file_writer_timer.reset();
  performing_scan = true;
}

void ska::pst::dsp::DiskMonitor::perform_stop_scan()
{
  PSTLOG_DEBUG(this, "performing stop scan");
  std::unique_lock<std::mutex> control_lock(mutex);
  performing_scan = false;
  control_lock.unlock();
  cond.notify_all();
  PSTLOG_DEBUG(this, "done");
}

void ska::pst::dsp::DiskMonitor::perform_initialise()
{
  PSTLOG_DEBUG(this, "checking filesystem validity");
  query_filesystem();

  PSTLOG_DEBUG(this, "starting monitor threads");
  monitoring = true;
  monitor_thread = std::make_unique<std::thread>(std::thread(&ska::pst::dsp::DiskMonitor::monitor_filesystem, this));
}

void ska::pst::dsp::DiskMonitor::perform_deconfigure_beam()
{
  PSTLOG_DEBUG(this, "done");
}

void ska::pst::dsp::DiskMonitor::perform_deconfigure_scan()
{
  recording_time_available = 0;
  expected_bytes_per_second = 0;
  PSTLOG_DEBUG(this, "done");
}

void ska::pst::dsp::DiskMonitor::perform_scan()
{
  static constexpr double bytes_per_megabyte = 1048576;

  while (performing_scan)
  {
    {
      using namespace std::chrono_literals;
      std::unique_lock<std::mutex> control_lock(mutex);
      cond.wait_for(control_lock, 1000ms, [&]
                    { return (!performing_scan); });
      control_lock.unlock();
    }

    double elapsed_seconds = file_writer_timer.get_elapsed_microseconds() * ska::pst::common::seconds_per_microsecond;
    PSTLOG_DEBUG(this, "elapsed_seconds={}", elapsed_seconds);

    // enforce a minimum sampling interval
    if (elapsed_seconds > minimum_elapsed)
    {
      // compute the per second data rate for data written to the recording path
      bytes_written_rate = static_cast<double>(bytes_written - prev_bytes_written) / elapsed_seconds;
      PSTLOG_DEBUG(this, "bytes_written_rate={} file_system_available_rate={}", bytes_written_rate, file_system_available_rate);
      PSTLOG_INFO(this, "Data Write Rate={} MB/s File System Available Rate={} MB/s Recording Time Available={} seconds", int(bytes_written_rate / bytes_per_megabyte), int(file_system_available_rate / bytes_per_megabyte), int(recording_time_available));
      file_writer_timer.reset();
      prev_bytes_written = bytes_written;
    }
  }
}

void ska::pst::dsp::DiskMonitor::perform_terminate()
{
  PSTLOG_DEBUG(this, "setting monitoring=false");
  std::unique_lock<std::mutex> control_lock(mutex);
  monitoring = false;
  control_lock.unlock();
  cond.notify_all();
  PSTLOG_DEBUG(this, "done");
}

void ska::pst::dsp::DiskMonitor::perform_reset()
{
  PSTLOG_DEBUG(this, "done");
}

void ska::pst::dsp::DiskMonitor::query_filesystem() try
{
  // read the available space on the file system
  PSTLOG_TRACE(this, "file_system={}", file_system);
  const std::filesystem::space_info si = std::filesystem::space(file_system);
  file_system_capacity = static_cast<std::intmax_t>(si.capacity);
  file_system_available = static_cast<std::intmax_t>(si.available);
  PSTLOG_DEBUG(this, "available={} capacity={}", file_system_available, file_system_capacity);
}
catch (const std::filesystem::filesystem_error &e)
{
  PSTLOG_WARN(this, "error in std::filesystem::space: {}", e.what());
  throw(e);
}

void ska::pst::dsp::DiskMonitor::monitor_filesystem()
{
  PSTLOG_TRACE(this, "query_filesystem()");
  query_filesystem();
  prev_file_system_available = file_system_available;

  while (monitoring)
  {
    // sleep, with conditional check (on the control_lock) for 1s
    {
      using namespace std::chrono_literals;
      std::unique_lock<std::mutex> control_lock(mutex);
      cond.wait_for(control_lock, 1000ms, [&]
                    { return (!monitoring); });
      control_lock.unlock();
    }

    if (monitoring)
    {
      PSTLOG_TRACE(this, "query_filesystem() monitoring={}", monitoring);
      query_filesystem();

      // compute the rate at which the available space on the file system is changing
      double elapsed_seconds = monitor_filesystem_timer.get_elapsed_microseconds() * ska::pst::common::seconds_per_microsecond;
      if (elapsed_seconds > minimum_elapsed)
      {
        file_system_available_rate = static_cast<double>(file_system_available - prev_file_system_available) / elapsed_seconds;
        monitor_filesystem_timer.reset();
        prev_file_system_available = file_system_available;
      }

      // if the scan is configured, compute the available recording time
      double bytes_per_second = expected_bytes_per_second;
      if (is_scan_configured() && bytes_per_second > 0)
      {
        recording_time_available = static_cast<double>(file_system_available) / bytes_per_second;
        PSTLOG_TRACE(this, "file_system_available={} bytes_per_second={} recording_time_available={} seconds", file_system_available, bytes_per_second, recording_time_available);
      }
    }
  }
}

void ska::pst::dsp::DiskMonitor::increment_bytes(uint64_t nbytes)
{
  PSTLOG_TRACE(this, "nbytes={}", nbytes);
  enforce(is_scanning(), "Disk monitor cannot increment bytes written if state not Scanning");
  std::unique_lock<std::mutex> control_lock(mutex);
  bytes_written += nbytes;
  control_lock.unlock();
}

auto ska::pst::dsp::DiskMonitor::get_disk_capacity() const -> size_t
{
  return static_cast<size_t>(file_system_capacity);
}

auto ska::pst::dsp::DiskMonitor::get_disk_available() const -> size_t
{
  return static_cast<size_t>(file_system_available);
}

auto ska::pst::dsp::DiskMonitor::get_recording_time_available() const -> double
{
  enforce(is_scan_configured(), "cannot get recording time available if scan not configured");
  return recording_time_available;
}

auto ska::pst::dsp::DiskMonitor::get_bytes_written() const -> uint64_t
{
  enforce(is_scan_configured(), "cannot get bytes written if scan not configured");
  return bytes_written;
}

auto ska::pst::dsp::DiskMonitor::get_data_write_rate() const -> double
{
  enforce(is_scanning(), "cannot get data write rate if scan not scanning");
  return bytes_written_rate;
}

auto ska::pst::dsp::DiskMonitor::get_expected_data_write_rate() const -> double
{
  enforce(is_scan_configured(), "cannot get expected data write rate if scan not configured");
  return expected_bytes_per_second;
}
