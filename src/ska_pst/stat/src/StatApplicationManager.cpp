/*
 * Copyright 2023 Square Kilometre Array Observatory
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 * contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#include "ska_pst/stat/StatApplicationManager.h"
#include "ska_pst/common/utils/AsciiHeader.h"
#include "ska_pst/common/utils/Logging.h"

#include <chrono>
#include <filesystem>
#include <stdexcept>

ska::pst::stat::StatApplicationManager::StatApplicationManager(std::string base_path) :
  ska::pst::common::ApplicationManager("stat"), stat_base_path(std::move(base_path))
{
  PSTLOG_DEBUG(this, "ctor stat_base_path={}", stat_base_path);
  initialise();
}

ska::pst::stat::StatApplicationManager::~StatApplicationManager()
{
  PSTLOG_DEBUG(this, "dtor quit");
  quit();
}

void ska::pst::stat::StatApplicationManager::configure_from_file(const std::string &config_file)
{
  PSTLOG_DEBUG(this, "config_file={}", config_file);
  ska::pst::common::AsciiHeader config;
  config.load_from_file(config_file);
  PSTLOG_TRACE(this, "config={}", config.raw());

  // configure beam
  configure_beam(config);

  // configure scan
  configure_scan(config);

  // configure from file
  start_scan(config);
}

void ska::pst::stat::StatApplicationManager::validate_configure_beam(const ska::pst::common::AsciiHeader& config, ska::pst::common::ValidationContext *context)
{
  PSTLOG_TRACE(this, "config={}", config.raw());
  // Iterate through the vector and validate existence of required data header keys
  for (const std::string& config_key : beam_config_keys)
  {
    if (config.has(config_key))
    {
      PSTLOG_DEBUG(this, "config_key={} value={}", config_key, config.get_val(config_key));
    } else {
      context->add_missing_field_error(config_key);
    }
  }
  PSTLOG_TRACE(this, "complete");
}

void ska::pst::stat::StatApplicationManager::validate_configure_scan(const ska::pst::common::AsciiHeader& config, ska::pst::common::ValidationContext* context)
{
  PSTLOG_TRACE(this, "config={}", config.raw());
  // Iterate through the vector and validate existence of required data header keys
  for (const std::string& config_key : scan_config_keys)
  {
    if (config.has(config_key))
    {
      PSTLOG_DEBUG(this, "config_key={} value={}", config_key, config.get_val(config_key));
    } else {
      context->add_missing_field_error(config_key);
    }
  }
  PSTLOG_TRACE(this, "complete");
}

void ska::pst::stat::StatApplicationManager::validate_start_scan(const ska::pst::common::AsciiHeader& config)
{
  PSTLOG_TRACE(this, "config={}", config.raw());
  // Iterate through the vector and validate existence of required keys
  for (const std::string& config_key : startscan_config_keys)
  {
    if (config.has(config_key))
    {
      PSTLOG_DEBUG(this, "config_key={} value={}", config_key, config.get_val(config_key));
    } else {
      throw std::runtime_error("required field " + config_key + " missing in start scan configuration");
    }
  }
  PSTLOG_TRACE(this, "complete");
}

void ska::pst::stat::StatApplicationManager::perform_initialise()
{
  // ensure the output directory exists on initialisation
  PSTLOG_DEBUG(this, "creating {}", stat_base_path);
  std::filesystem::path output_dir(stat_base_path);
  create_directories(output_dir);
}

void ska::pst::stat::StatApplicationManager::perform_configure_beam()
{
  PSTLOG_DEBUG(this, "start");

  // ska::pst::common::ApplicationManager has written beam configuration parameters to beam_config
  data_key = beam_config.get_val("DATA_KEY");
  weights_key = beam_config.get_val("WEIGHTS_KEY");

  // ensure the shared memory key is valid
  try {
    ska::pst::smrb::DataBlock::parse_psrdada_key(data_key);
  }
  catch (std::runtime_error& exc)
  {
    PSTLOG_ERROR(this, "DATA_KEY={} was invalid", data_key);
    throw std::runtime_error("DATA_KEY was invalid");
  }

  try {
    ska::pst::smrb::DataBlock::parse_psrdada_key(weights_key);
  }
  catch (std::runtime_error& exc)
  {
    PSTLOG_ERROR(this, "WEIGHTS_KEY={} was invalid", weights_key);
    throw std::runtime_error("WEIGHTS_KEY was invalid");
  }

  PSTLOG_DEBUG(this, "complete");
}

void ska::pst::stat::StatApplicationManager::perform_configure_scan()
{
  PSTLOG_DEBUG(this, "start");

  // Construct the SMRB segment producer
  PSTLOG_DEBUG(this, "SmrbSegmentProducer({}, {})", data_key, weights_key);
  producer = std::make_unique<ska::pst::smrb::SmrbSegmentProducer>(data_key, weights_key);

  processing_delay = scan_config.get<uint32_t>("STAT_PROC_DELAY_MS");
  PSTLOG_DEBUG(this, "setting processing_delay={} ms", processing_delay);

  // set configuration parameters for the StatProcessor
  req_time_bins = scan_config.get<uint32_t>("STAT_REQ_TIME_BINS");
  req_freq_bins = scan_config.get<uint32_t>("STAT_REQ_FREQ_BINS");
  num_rebin = scan_config.get<uint32_t>("STAT_NREBIN");

  // Connect to the SMRB segment producer, this triggers the read_config command on the SMRBs
  PSTLOG_DEBUG(this, "producer->connect({})", timeout);
  producer->connect(timeout);

  PSTLOG_DEBUG(this, "complete");
}

void ska::pst::stat::StatApplicationManager::perform_start_scan()
{
  PSTLOG_DEBUG(this, "stub");
}

void ska::pst::stat::StatApplicationManager::perform_scan() try
{
  PSTLOG_DEBUG(this, "start");
  // calling open on the producer will read the complete header from the SMRBs
  producer->open();

  // Acquire the data and weights beam configuration from the SMRB segment producer
  data_header.clone(producer->get_data_header());
  weights_header.clone(producer->get_weights_header());
  PSTLOG_TRACE(this, "data_header:\n{}", data_header.raw());
  PSTLOG_TRACE(this, "weights_header:\n{}", weights_header.raw());

  // check that the SCAN_ID and EB_ID in the header's match the scan configuration
  if (data_header.get_val("SCAN_ID") != startscan_config.get_val("SCAN_ID"))
  {
    PSTLOG_ERROR(this, "SCAN_ID mismatch between data header [{}] and startscan_config=[{}]", data_header.get_val("SCAN_ID"), startscan_config.get_val("SCAN_ID"));
    throw std::runtime_error("SCAN_ID mismatch between data header and startscan_config");
  }
  if (data_header.get_val("EB_ID") != scan_config.get_val("EB_ID"))
  {
    PSTLOG_ERROR(this, "EB_ID mismatch between data header [{}] and startscan_config=[{}]", data_header.get_val("EB_ID"), startscan_config.get_val("EB_ID"));
    throw std::runtime_error("EB_ID mismatch between data header and startscan_config");
  }

  PSTLOG_DEBUG(this, "SCAN_ID={} EB_ID={}", data_header.get_val("SCAN_ID"), data_header.get_val("EB_ID"));

  // set the base_path and suffix for statistics recording in the beam configuration
  data_header.set_val("STAT_BASE_PATH", stat_base_path);

  data_header.set("STAT_REQ_TIME_BINS", req_time_bins);
  data_header.set("STAT_REQ_FREQ_BINS", req_freq_bins);
  data_header.set("STAT_NREBIN", num_rebin);

  // ensure the STAT_OUTPUT_FILENAME is not present in the beam_config
  if (data_header.has("STAT_OUTPUT_FILENAME"))
  {
    data_header.del("STAT_OUTPUT_FILENAME");
  }

  processor = std::make_unique<ska::pst::stat::StatProcessor>(data_header, weights_header);

  PSTLOG_DEBUG(this, "add shared ScalarStatPublisher publisher to processor");
  scalar_publisher = std::make_shared<ska::pst::stat::ScalarStatPublisher>(data_header);
  processor->add_publisher(scalar_publisher);

  PSTLOG_DEBUG(this, "add shared StatHdf5FileWriter publisher to processor");
  hdf5_publisher = std::make_shared<StatHdf5FileWriter>(data_header);
  processor->add_publisher(hdf5_publisher);

  keep_processing = true;
  processing_state = Processing;

  bool eod = false;
  while (!eod && keep_processing)
  {
    PSTLOG_DEBUG(this, "producer->next_segment()");
    auto segment = producer->next_segment();
    auto obs_offset = segment.data.obs_offset;
    PSTLOG_DEBUG(this, "opened segment with offset {} and containing {} bytes", obs_offset, segment.data.size);

    if (segment.data.block == nullptr)
    {
      PSTLOG_DEBUG(this, "encountered end of data");
      eod = true;
    }
    else if (!processor->validate_segment(segment))
    {
      PSTLOG_DEBUG(this, "segment could not be processed");
      eod = true;
    }
    else
    {
      processing_state = Processing;
      PSTLOG_DEBUG(this, "processor->process");
      bool processing_complete = processor->process(segment);
      PSTLOG_DEBUG(this, "processor->process processing_complete={}", processing_complete);
    }

    processing_state = Waiting;

    // use processing_cond to wait on changes to the keep_processing boolean
    PSTLOG_DEBUG(this, "waiting for {} ms on processing_cond", processing_delay);
    {
      using namespace std::chrono_literals;
      std::chrono::milliseconds timeout = processing_delay * 1ms;
      std::unique_lock<std::mutex> lock(processing_mutex);
      processing_cond.wait_for(lock, timeout, [&]{return (keep_processing == false);});
      lock.unlock();
    }
    PSTLOG_DEBUG(this, "keep_processing={}", keep_processing);
  }

  PSTLOG_DEBUG(this, "closing producer connection");
  producer->close();

  // beam_config cleanup
  PSTLOG_DEBUG(this, "data_beam_config.reset()");
  data_header.reset();
  PSTLOG_DEBUG(this, "weights_beam_config.reset()");
  weights_header.reset();
  processing_state = Idle;

  PSTLOG_DEBUG(this, "processor.reset()");
  processor.reset(nullptr);

  PSTLOG_DEBUG(this, "complete");
}
catch (std::exception& exc)
{
  PSTLOG_WARN(this, "exception during perform_scan: {}", exc.what());
  producer->close();
  processor.reset(nullptr);
  go_to_runtime_error(std::current_exception());
}

void ska::pst::stat::StatApplicationManager::perform_stop_scan()
{
  PSTLOG_DEBUG(this, "start");

  // interrupt any of the statistics computation in the the StatProcessor.
  if (processor)
  {
    PSTLOG_DEBUG(this, "processor->interrupt()");
    processor->interrupt();
  }

  // signal the thread running perform_scan via the processing_cond
  {
    std::unique_lock<std::mutex> lock(processing_mutex);
    PSTLOG_DEBUG(this, "keep_processing = false");
    keep_processing = false;
    processing_cond.notify_all();
  }
}

void ska::pst::stat::StatApplicationManager::perform_deconfigure_scan()
{
  PSTLOG_DEBUG(this, "producer->disconnect()");
  producer->disconnect();
  PSTLOG_DEBUG(this, "done");
}

void ska::pst::stat::StatApplicationManager::perform_deconfigure_beam()
{
  PSTLOG_DEBUG(this, "stub");
}

void ska::pst::stat::StatApplicationManager::perform_terminate()
{
  PSTLOG_DEBUG(this, "stub");
}

auto ska::pst::stat::StatApplicationManager::get_scalar_stats() -> ska::pst::stat::StatStorage::scalar_stats_t
{
  if (scalar_publisher)
  {
    return scalar_publisher->get_scalar_stats();
  }
  else
  {
    StatStorage::scalar_stats_t empty_stats;
    return empty_stats;
  }
}
