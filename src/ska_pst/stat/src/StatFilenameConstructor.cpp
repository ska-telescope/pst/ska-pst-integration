/*
 * Copyright 2023 Square Kilometre Array Observatory
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 * contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#include "ska_pst/stat/StatFilenameConstructor.h"
#include "ska_pst/common/utils/FilePaths.h"
#include "ska_pst/common/utils/FileWriter.h"
#include "ska_pst/common/utils/Logging.h"

#include <map>
#include <stdexcept>
#include <string>

ska::pst::stat::StatFilenameConstructor::StatFilenameConstructor(const ska::pst::common::AsciiHeader& header)
{
  if (header.has("STAT_BASE_PATH"))
  {
    set_base_path(header.get_val("STAT_BASE_PATH"));
  }
  if (header.has("EB_ID"))
  {
    set_eb_id(header.get_val("EB_ID"));
  }
  if (header.has("SCAN_ID"))
  {
    set_scan_id(header.get_val("SCAN_ID"));
  }
  if (header.has("TELESCOPE"))
  {
    set_telescope(header.get_val("TELESCOPE"));
  }
}

void ska::pst::stat::StatFilenameConstructor::set_base_path(const std::string& stat_base)
{
  PSTLOG_TRACE(this, "stat_base={}", stat_base);
  stat_base_path = std::filesystem::path(stat_base);
}

void ska::pst::stat::StatFilenameConstructor::set_eb_id(const std::string& _eb_id)
{
  eb_id = _eb_id;
  PSTLOG_TRACE(this, "eb_id={}", _eb_id);
}

void ska::pst::stat::StatFilenameConstructor::set_scan_id(const std::string& _scan_id)
{
  scan_id = _scan_id;
  PSTLOG_TRACE(this, "scan_id={}", _scan_id);
}

void ska::pst::stat::StatFilenameConstructor::set_telescope(const std::string& _telescope)
{
  telescope = _telescope;
}

auto ska::pst::stat::StatFilenameConstructor::get_filename(
  const std::string& utc_start, uint64_t obs_offset, uint64_t file_number) -> std::filesystem::path
{
  if (stat_base_path.empty())
  {
    PSTLOG_ERROR(this, "stat_base_path is not set");
    throw std::runtime_error("stat_base_path is not set");
  }

  if (eb_id.empty())
  {
    PSTLOG_ERROR(this, "eb_id is not set");
    throw std::runtime_error("eb_id is not set");
  }

  if (telescope.empty())
  {
    PSTLOG_ERROR(this, "telescope is not set");
    throw std::runtime_error("telescope is not set");
  }

  if (scan_id.empty())
  {
    PSTLOG_ERROR(this, "scan_id is not set");
    throw std::runtime_error("scan_id is not set");
  }

  ska::pst::common::FilePaths file_paths(stat_base_path);
  std::filesystem::path stream_path{"monitoring_stats"};
  std::filesystem::path scan_path = file_paths.get_scan_product_path(eb_id, telescope, scan_id) / stream_path;

  // file name (with no directory prefix) using FileWriter for consistent naming
  std::filesystem::path filename = ska::pst::common::FileWriter::get_filename(utc_start, obs_offset, file_number);

  // full path to the STAT output file
  std::filesystem::path output_file = scan_path / filename.replace_extension("h5");

  return output_file;
}
