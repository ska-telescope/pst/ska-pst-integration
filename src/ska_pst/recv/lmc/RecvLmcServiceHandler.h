/*
 * Copyright 2022 Square Kilometre Array Observatory
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 * contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef __SKA_PST_RECV_RecvLmcServiceHandler_h
#define __SKA_PST_RECV_RecvLmcServiceHandler_h

#include <memory>

#include "ska_pst/recv/clients/Receiver.h"
#include "ska_pst/common/lmc/LmcServiceHandler.h"

namespace ska::pst::recv {

    /**
     * @brief Class to act as a bridge between the local monitoring and control
     *    service and a @see ska::pst::recv::Receiver that controls of PST RECV.CORE.
     *
     */
    class RecvLmcServiceHandler final : public ska::pst::common::LmcServiceHandler {
        private:
            /**
             * @brief A pointer to the Receiver used by this service.
             *
             * Commands that this service responds too will be converted to/from
             * the formate of the receiver.  All the actual logic of a RECV
             * is handled with instances of a Receiver.
             */
            std::shared_ptr<ska::pst::recv::Receiver> recv;

        public:
            /**
             * @brief Constructor for the LMC service handler.
             *
             * @param recv a Receiver that the service is acting as a bridge for.
             */
            RecvLmcServiceHandler(std::shared_ptr<ska::pst::recv::Receiver> recv) : recv(std::move(recv)) {}
            virtual ~RecvLmcServiceHandler() = default;

            /**
             * @brief Validate a beam configuration.
             *
             * Validate a beam configuration for correctness but does not apply the configuration.
             *
             * @throw std::exception if there is a problem with the beam configuration of the service.
             * @throw ska::pst::common::pst_validation_error if there are validation errors in the request.
             */
            void validate_beam_configuration(const ska::pst::lmc::BeamConfiguration &configuration) override;

            /**
             * @brief Validate a scan configuration.
             *
             * Validate a scan configuration for correctness but does not apply the configuration.
             *
             * @throw std::exception if there is a problem with the beam configuration of the service.
             * @throw ska::pst::common::pst_validation_error if there are validation errors in the request.
             */
            void validate_scan_configuration(const ska::pst::lmc::ScanConfiguration &configuration) override;

            // beam configuration methods
            /**
             * @brief Handle configuring the service to be a part of a beam.
             *
             * This implementation expects that there is an receive sub-field in the beam configuration
             * request. It takes the beam configuration and creates a @see ska::pst::common::AsciiHeader
             * and then calls configure_beam method on the Receiver with the config header.
             *
             * @param configuration the configuration for the beam. This message has one of field should
             *      be the receive sub-field message.
             */
            void configure_beam(const ska::pst::lmc::BeamConfiguration &configuration) override;

            /**
             * @brief Handle deconfiguring the service from a beam.
             *
             * This will attempt to release and destroy the beam resources that had already been
             * configured, such as disconnecting from the data and weights ring buffers.  This
             * implementation proxies the request through to the Receiver.
             */
            void deconfigure_beam() override;

            /**
             * @brief Handle getting the current beam configuration for the service.
             *
             * This will return the current beam configuration used for the RECV pipeline.
             */
            void get_beam_configuration(ska::pst::lmc::BeamConfiguration* resources) override;

            /**
             * @brief Check if beam configured to this service.
             *
             * Will return true if the @see Receiver has beam configuration.
             */
            bool is_beam_configured() const noexcept override {
                return recv->is_beam_configured();
            }

            // scan configuration methods
            /**
             * @brief Handle configuring the service for a scan.
             *
             * This will configure the Receiver for a scan. The implementation expects
             * that there is a receive sub-field and will map the scan configuration
             * data to an @see ska::pst::common::AsciiHeader config object and send
             * that to the Receiver to do the actual configuration.
             */
            void configure_scan(const ska::pst::lmc::ScanConfiguration &configuration) override;

            /**
             * @brief Handle deconfiguring service for a scan.
             *
             * This will proxy the request through to the Receiver to reset the
             * receiver's configuration. This Receiver will still have the allocated
             * resources but it won't have the scan specific configuration.
             */
            void deconfigure_scan() override;

            /**
             * @brief Handle getting the current scan configuration for the service.
             *
             * This will map the current scan configuration, of the Receiver, to the
             * receive sub-field message @see ska::pst::lmc::ReceiveScanConfiguration.
             */
            void get_scan_configuration(ska::pst::lmc::ScanConfiguration *configuration) override;

            /**
             * @brief Check if the service has been configured for a scan.
             *
             * @return This will return true if there had been a call to configure.
             */
            bool is_scan_configured() const noexcept override {
                return recv->is_scan_configured();
            }

            // scan methods
            /**
             * @brief Handle initiating a scan.
             *
             * This will initiate a scan on the Receiver, which in turns will
             * start processing the data coming on the configured UDP listener.
             */
            void start_scan(const ska::pst::lmc::StartScanRequest &request) override;

            /**
             * @brief Handle ending a scan.
             *
             * This will mark the application as not scanning. The implementation
             * will stop the Receiver from scanning.
             */
            void stop_scan() override;

            /**
             * @brief Handle resetting State into Idle
             *
             * This ties the states between LmcService ObsState::EMPTY with ApplicationManager State::Idle
             */
            void reset() override;

          /**
           * @brief Handle restarting the Service.
           *
           * Calls ApplicationManager quit() then force_exit(restart_exit_code).
           */
            void restart() override;

            /**
             * @brief Check if the service is currently performing a scan.
             *
             */
            bool is_scanning() const noexcept override {
                return recv->is_scanning();
            }

            // monitoring
            /**
             * @brief Handle getting the monitoring data for the service.
             *
             * This will get the current monitoring data for Receiver, including the
             * amount of data received, the instantaneous rate of data being received,
             * and the number of malformed packets. It is only valid to call this if
             * the handler is currently scanning.
             *
             * @param data Pointer to the protobuf message to return. Implementations should get
             *      mutable references to the sub-field they are responding to and update that message.
             * @throw std::exception if there is a validation issue or problem with configuring beam.
             */
            void get_monitor_data(ska::pst::lmc::MonitorData *data) override;

            /**
             * @brief Return environment variables back to the client.
             *
             * This implementation returns the data_host (as a string), data_mac (as a string) and
             * data_port (as an int), of the Receiver back to the client. The data_host is returned
             * because the client may not know the IP address of the network interface controller
             * that will receive data. In the case of SKA, the PST.LMC will expose this value back
             * to the CSP-LMC that can then send the data to CBF.
             *
             * @param response Pointer to a protobuf message message that includes the map to populate.
             */
            void get_env(ska::pst::lmc::GetEnvironmentResponse *response) noexcept override;

            /**
             * @brief Get the UDPReceiveDB state
             *
             * @return ska::pst::common::State returns current the enum State of the UDPReceiveDB
             */
            ska::pst::common::State get_application_manager_state() override { return recv->get_state(); }

            /**
             * @brief Get the UDPReceiveDB exception pointer
             *
             * @return std::exception_ptr returns the current captured exception caught by the UDPReceiveDB
             */
            std::exception_ptr get_application_manager_exception() override { return recv->get_exception(); }

            /**
              * @brief Put application into a runtime error state.
              *
              * @param exc an exception pointer to store on the application manager.
             */
            void go_to_runtime_error(std::exception_ptr exc) override;
    };

} // namespace ska::pst::recv

#endif // __SKA_PST_RECV_RecvLmcServiceHandler_h
