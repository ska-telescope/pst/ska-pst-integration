/*
 * Copyright 2022 Square Kilometre Array Observatory
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 * contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#include <string>
#include <sstream>
#include <thread>

#include "ska_pst/common/lmc/LmcServiceException.h"
#include "ska_pst/common/utils/Logging.h"

#include "ska_pst/recv/lmc/RecvLmcServiceHandler.h"

auto beam_configuration_as_ascii_header(
    const ska::pst::lmc::BeamConfiguration &configuration
) -> ska::pst::common::AsciiHeader
{
    // assert we only have an recv configure beam request
    if (!configuration.has_receive()) {
        PSTLOG_WARN(nullptr, "BeamConfiguration protobuf message has no RECV details provided.");
        throw ska::pst::common::LmcServiceException(
            "Expected a RECV beam configuration object, but none were provided.",
            ska::pst::lmc::ErrorCode::INVALID_REQUEST,
            grpc::StatusCode::INVALID_ARGUMENT
        );
    }

    const auto &recv_resources = configuration.receive();
    const auto &subband_resources = recv_resources.subband_resources();

    ska::pst::common::AsciiHeader config;
    int timeout = 0;

    config.set("UDP_NSAMP", recv_resources.udp_nsamp());
    config.set("WT_NSAMP", recv_resources.wt_nsamp());
    config.set("UDP_NCHAN", recv_resources.udp_nchan());
    config.set("FRONTEND", recv_resources.frontend());
    config.set("FD_POLN", recv_resources.fd_poln());
    config.set("FD_HAND", recv_resources.fd_hand());
    config.set("FD_SANG", recv_resources.fd_sang());
    config.set("FD_MODE", recv_resources.fd_mode());
    config.set("FA_REQ", recv_resources.fa_req());
    config.set("NANT", recv_resources.nant());
    config.set("ANTENNAE", recv_resources.antennas());
    config.set("ANT_WEIGHTS", recv_resources.ant_weights());
    config.set("NPOL", recv_resources.npol());
    config.set("NBIT", recv_resources.nbits());
    config.set("NDIM", recv_resources.ndim());
    config.set("TSAMP", recv_resources.tsamp());
    config.set("OS_FACTOR", recv_resources.ovrsamp());
    config.set("NSUBBAND", recv_resources.nsubband());
    config.set("UDP_FORMAT", recv_resources.udp_format());
    config.set("BEAM_ID", recv_resources.beam_id());

    config.set("DATA_KEY", subband_resources.data_key());
    config.set("WEIGHTS_KEY", subband_resources.weights_key());
    config.set("BW", subband_resources.bandwidth());
    config.set("NCHAN", subband_resources.nchan());
    config.set("START_CHANNEL", subband_resources.start_channel());
    config.set("END_CHANNEL", subband_resources.end_channel());
    config.set("FREQ", subband_resources.frequency());
    config.set("START_CHANNEL_OUT", subband_resources.start_channel_out());
    config.set("END_CHANNEL_OUT", subband_resources.end_channel_out());
    config.set("NCHAN_OUT", subband_resources.nchan_out());
    config.set("BW_OUT", subband_resources.bandwidth_out());
    config.set("FREQ_OUT", subband_resources.frequency_out());
    config.set("DATA_HOST", subband_resources.data_host());
    config.set("DATA_PORT", subband_resources.data_port());

    return config;
}

auto scan_configuration_as_ascii_header(
    const ska::pst::lmc::ScanConfiguration& scan_configuration
) -> ska::pst::common::AsciiHeader
{
    // ensure that this request was for RECV, even if it was an empty no-op
    if (!scan_configuration.has_receive()) {
        PSTLOG_WARN(nullptr, "ScanConfiguration protobuf message has no RECV details provided.");

        throw ska::pst::common::LmcServiceException(
            "Expected a RECV scan configuration object, but none were provided.",
            ska::pst::lmc::ErrorCode::INVALID_REQUEST,
            grpc::StatusCode::INVALID_ARGUMENT
        );
    }

    ska::pst::common::AsciiHeader header;
    const auto &configuration = scan_configuration.receive();
    // activation_time is deprecated in the PST schema and may not be set.
    if (configuration.activation_time().length() > 0)
    {
      header.set("ACTIVATION_TIME", configuration.activation_time());
    }

    header.set("OBSERVER", configuration.observer());
    header.set("PROJID", configuration.projid());
    // pointing_id is deprecated in the PST schema and may not be set.
    if (configuration.pnt_id().length() > 0)
    {
      header.set("PNT_ID", configuration.pnt_id());
    }
    header.set("SUBARRAY_ID", configuration.subarray_id());
    header.set("SOURCE", configuration.source());
    header.set("ITRF", configuration.itrf());
    // protobuf's default value for numbers is 0.
    // if both are zero then assume they were not set.
    if (configuration.bmaj() != 0.0 and configuration.bmin() != 0.0)
    {
      header.set("BMAJ", configuration.bmaj());
      header.set("BMIN", configuration.bmin());
    }
    header.set("COORD_MD", configuration.coord_md());
    header.set("EQUINOX", configuration.equinox());
    header.set("STT_CRD1", configuration.stt_crd1());
    header.set("STT_CRD2", configuration.stt_crd2());
    header.set("TRK_MODE", configuration.trk_mode());
    header.set("SCANLEN_MAX", configuration.scanlen_max());
    // set default OBS_OFFSET
    header.set("OBS_OFFSET", 0);
    // test_vector_id has been deprecated in the PST schema and may not be set
    if (configuration.test_vector().length() > 0)
    {
        header.set("TEST_VECTOR", configuration.test_vector());
    }
    header.set("EB_ID", configuration.execution_block_id());

    return header;
}

void ska::pst::recv::RecvLmcServiceHandler::validate_beam_configuration(
    const ska::pst::lmc::BeamConfiguration& configuration
)
{
    auto config = beam_configuration_as_ascii_header(configuration);
    ska::pst::common::ValidationContext context;
    recv->validate_configure_beam(config, &context);
    context.throw_error_if_not_empty();
}

void ska::pst::recv::RecvLmcServiceHandler::configure_beam(
    const ska::pst::lmc::BeamConfiguration& configuration
)
{
    PSTLOG_TRACE(this, "RecvLmcServiceHandler configure_beam()");

    // check if data manager has already have had beam configured
    if (recv->is_beam_configured()) {
        PSTLOG_WARN(this, "Received configure beam request but beam configured already.");
        throw ska::pst::common::LmcServiceException(
            "Resources already assigned for RECV.",
            ska::pst::lmc::ErrorCode::CONFIGURED_FOR_BEAM_ALREADY,
            grpc::StatusCode::FAILED_PRECONDITION
        );
    }

    auto config = beam_configuration_as_ascii_header(configuration);

    // validation will happens on the state model as first part of configure_beam
    // so no need to do validation here.
    recv->configure_beam(config);

    PSTLOG_TRACE(this, "Finish configuring beam");
}

void ska::pst::recv::RecvLmcServiceHandler::deconfigure_beam()
{
    PSTLOG_TRACE(this, "RecvLmcServiceHandler deconfigure_beam()");

    // check if receiver has already have had beam configured
    if (!recv->is_beam_configured()) {
        PSTLOG_WARN(this, "Received request to deconfigure beam when beam not configured.");
        throw ska::pst::common::LmcServiceException(
            "RECV not configured for beam.",
            ska::pst::lmc::ErrorCode::NOT_CONFIGURED_FOR_BEAM,
            grpc::StatusCode::FAILED_PRECONDITION
        );
    }

    recv->deconfigure_beam();
}

void ska::pst::recv::RecvLmcServiceHandler::get_beam_configuration(
    ska::pst::lmc::BeamConfiguration* response
)
{
    PSTLOG_TRACE(this, "RecvLmcServiceHandler get_beam_configuration()");
    if (!recv->is_beam_configured())
    {
        PSTLOG_WARN(this, "Received request to get beam configuration when beam not configured.");
        throw ska::pst::common::LmcServiceException(
            "RECV not configured for beam.",
            ska::pst::lmc::ErrorCode::NOT_CONFIGURED_FOR_BEAM,
            grpc::StatusCode::FAILED_PRECONDITION
        );
    }

    const auto& config = recv->get_beam_configuration();

    ska::pst::lmc::ReceiveBeamConfiguration *recv_resources = response->mutable_receive();
    ska::pst::lmc::ReceiveSubbandResources *subband_resources =
        recv_resources->mutable_subband_resources();

    recv_resources->set_udp_nsamp(config.get<uint32_t>("UDP_NSAMP"));
    recv_resources->set_wt_nsamp(config.get<uint32_t>("WT_NSAMP"));
    recv_resources->set_udp_nchan(config.get<uint32_t>("UDP_NCHAN"));
    recv_resources->set_frontend(config.get_val("FRONTEND"));
    recv_resources->set_fd_poln(config.get_val("FD_POLN"));
    recv_resources->set_fd_hand(config.get<int32_t>("FD_HAND"));
    recv_resources->set_fd_sang(config.get<float>("FD_SANG"));
    recv_resources->set_fd_mode(config.get_val("FD_MODE"));
    recv_resources->set_fa_req(config.get<float>("FA_REQ"));
    recv_resources->set_nant(config.get<uint32_t>("NANT"));
    recv_resources->set_antennas(config.get_val("ANTENNAE"));
    recv_resources->set_ant_weights(config.get_val("ANT_WEIGHTS"));
    recv_resources->set_npol(config.get<uint32_t>("NPOL"));
    recv_resources->set_nbits(config.get<uint32_t>("NBIT"));
    recv_resources->set_ndim(config.get<uint32_t>("NDIM"));
    recv_resources->set_tsamp(config.get<double>("TSAMP"));
    recv_resources->set_ovrsamp(config.get_val("OS_FACTOR"));
    recv_resources->set_nsubband(config.get<uint32_t>("NSUBBAND"));
    recv_resources->set_udp_format(config.get_val("UDP_FORMAT"));
    recv_resources->set_beam_id(config.get_val("BEAM_ID"));
    subband_resources->set_data_key(config.get_val("DATA_KEY"));
    subband_resources->set_weights_key(config.get_val("WEIGHTS_KEY"));
    subband_resources->set_bandwidth(config.get<double>("BW"));
    subband_resources->set_nchan(config.get<uint32_t>("NCHAN"));
    subband_resources->set_start_channel(config.get<uint32_t>("START_CHANNEL"));
    subband_resources->set_end_channel(config.get<uint32_t>("END_CHANNEL"));
    subband_resources->set_frequency(config.get<double>("FREQ"));
    subband_resources->set_start_channel_out(config.get<uint32_t>("START_CHANNEL_OUT"));
    subband_resources->set_end_channel_out(config.get<uint32_t>("END_CHANNEL_OUT"));
    subband_resources->set_nchan_out(config.get<uint32_t>("NCHAN_OUT"));
    subband_resources->set_bandwidth_out(config.get<double>("BW_OUT"));
    subband_resources->set_frequency_out(config.get<double>("FREQ_OUT"));
    subband_resources->set_data_host(config.get_val("DATA_HOST"));
    subband_resources->set_data_port(config.get<uint32_t>("DATA_PORT"));
}

void ska::pst::recv::RecvLmcServiceHandler::validate_scan_configuration(
    const ska::pst::lmc::ScanConfiguration& request
)
{
    auto config = scan_configuration_as_ascii_header(request);
    ska::pst::common::ValidationContext context;
    recv->validate_configure_scan(config, &context);
    context.throw_error_if_not_empty();
}


void ska::pst::recv::RecvLmcServiceHandler::configure_scan(
    const ska::pst::lmc::ScanConfiguration& request
)
{
    PSTLOG_TRACE(this, "RecvLmcServiceHandler configure_scan()");
    if (!is_beam_configured())
    {
        PSTLOG_WARN(this, "Received scan configuration request when beam not configured already.");

        throw ska::pst::common::LmcServiceException(
            "RECV not configured for beam.",
            ska::pst::lmc::ErrorCode::NOT_CONFIGURED_FOR_BEAM,
            grpc::StatusCode::FAILED_PRECONDITION
        );
    }

    if (recv->is_scan_configured()) {
        PSTLOG_WARN(this, "Received configure scan request but already configured.");

        throw ska::pst::common::LmcServiceException(
            "Scan already configured for RECV.",
            ska::pst::lmc::ErrorCode::CONFIGURED_FOR_SCAN_ALREADY,
            grpc::StatusCode::FAILED_PRECONDITION
        );
    }

    auto header = scan_configuration_as_ascii_header(request);
    // validation will happens on the state model as first part of configure_scan
    // so no need to do validation here.
    recv->configure_scan(header);
}

void ska::pst::recv::RecvLmcServiceHandler::deconfigure_scan()
{
    PSTLOG_TRACE(this, "RecvLmcServiceHandler deconfigure_scan()");
    if (!is_scan_configured())
    {
        PSTLOG_WARN(this, "Received deconfigure when not already configured.");

        throw ska::pst::common::LmcServiceException(
            "RECV not configured for scan.",
            ska::pst::lmc::ErrorCode::NOT_CONFIGURED_FOR_SCAN,
            grpc::StatusCode::FAILED_PRECONDITION
        );
    }

    recv->deconfigure_scan();
}

void ska::pst::recv::RecvLmcServiceHandler::get_scan_configuration(
    ska::pst::lmc::ScanConfiguration* response
)
{
    if (!is_scan_configured())
    {
        PSTLOG_WARN(this, "Received get_scan_configuration when not already configured.");

        throw ska::pst::common::LmcServiceException(
            "RECV not configured for scan.",
            ska::pst::lmc::ErrorCode::NOT_CONFIGURED_FOR_SCAN,
            grpc::StatusCode::FAILED_PRECONDITION
        );
    }

    auto receive_response = response->mutable_receive();
    auto &config = recv->get_scan_configuration();

    if (config.has("ACTIVATION_TIME"))
    {
      receive_response->set_activation_time(config.get_val("ACTIVATION_TIME"));
    }
    receive_response->set_observer(config.get_val("OBSERVER"));
    receive_response->set_projid(config.get_val("PROJID"));
    if (config.has("PNT_ID"))
    {
      receive_response->set_pnt_id(config.get_val("PNT_ID"));
    }
    receive_response->set_subarray_id(config.get_val("SUBARRAY_ID"));
    receive_response->set_source(config.get_val("SOURCE"));
    receive_response->set_itrf(config.get_val("ITRF"));
    if (config.has("BMAJ"))
    {
      receive_response->set_bmaj(config.get<float>("BMAJ"));
    }
    if (config.has("BMIN"))
    {
      receive_response->set_bmin(config.get<float>("BMIN"));
    }
    receive_response->set_coord_md(config.get_val("COORD_MD"));
    receive_response->set_equinox(config.get_val("EQUINOX"));
    receive_response->set_stt_crd1(config.get_val("STT_CRD1"));
    receive_response->set_stt_crd2(config.get_val("STT_CRD2"));
    receive_response->set_trk_mode(config.get_val("TRK_MODE"));
    receive_response->set_scanlen_max(config.get<int32_t>("SCANLEN_MAX"));
    if (config.has("TEST_VECTOR"))
    {
      receive_response->set_test_vector(config.get_val("TEST_VECTOR"));
    }
    receive_response->set_execution_block_id(config.get_val("EB_ID"));
}

void ska::pst::recv::RecvLmcServiceHandler::start_scan(
    const ska::pst::lmc::StartScanRequest& request
)
{
    PSTLOG_TRACE(this, "RecvLmcServiceHandler scan()");
    if (!is_scan_configured())
    {
        PSTLOG_WARN(this, "Received scan request when not already configured.");

        throw ska::pst::common::LmcServiceException(
            "RECV not configured for scan.",
            ska::pst::lmc::ErrorCode::NOT_CONFIGURED_FOR_SCAN,
            grpc::StatusCode::FAILED_PRECONDITION
        );
    }

    if (is_scanning())
    {
        PSTLOG_WARN(this, "Received scan request when already.");

        throw ska::pst::common::LmcServiceException(
            "RECV is already scanning.",
            ska::pst::lmc::ErrorCode::ALREADY_SCANNING,
            grpc::StatusCode::FAILED_PRECONDITION
        );
    }

    // add the scan_id here
    ska::pst::common::AsciiHeader start_scan_config;
    start_scan_config.set("SCAN_ID", request.scan_id());
    recv->start_scan(start_scan_config);
}

void ska::pst::recv::RecvLmcServiceHandler::stop_scan()
{
    PSTLOG_TRACE(this, "RecvLmcServiceHandler stop_scan()");
    if (!is_scanning())
    {
        PSTLOG_WARN(this, "Received stop_scan request when not scanning.");

        throw ska::pst::common::LmcServiceException(
            "Received stop_scan request when RECV is not scanning.",
            ska::pst::lmc::ErrorCode::NOT_SCANNING,
            grpc::StatusCode::FAILED_PRECONDITION
        );
    }

    recv->stop_scan();
}

void ska::pst::recv::RecvLmcServiceHandler::reset()
{
    PSTLOG_INFO(this, "RecvLmcServiceHandler reset()");
    if (recv->get_state() == ska::pst::common::State::RuntimeError) {
        recv->reset();
    }
}

void ska::pst::recv::RecvLmcServiceHandler::restart()
{
    PSTLOG_INFO(this, "RecvLmcServiceHandler restart received restart request.");
    recv->force_exit(ska::pst::common::ApplicationManager::restart_exit_code);
}

void ska::pst::recv::RecvLmcServiceHandler::get_monitor_data(
    ska::pst::lmc::MonitorData *response
)
{
    PSTLOG_TRACE(this, "RecvLmcServiceHandler get_monitor_data()");
    if (!is_scanning())
    {
        PSTLOG_WARN(this, "Received get_monitor_data request when not scanning.");

        throw ska::pst::common::LmcServiceException(
            "Received get_monitor_data request when RECV is not scanning.",
            ska::pst::lmc::ErrorCode::NOT_SCANNING,
            grpc::StatusCode::FAILED_PRECONDITION
        );
    }

    const auto &stats = recv->get_stats();

    auto *receive_monitor_data = response->mutable_receive();
    receive_monitor_data->set_receive_rate(static_cast<float>(stats.get_data_transmission_rate()));
    receive_monitor_data->set_data_received(stats.get_data_transmitted());
    receive_monitor_data->set_data_drop_rate(static_cast<float>(stats.get_data_drop_rate()));
    receive_monitor_data->set_data_dropped(stats.get_data_dropped());
    receive_monitor_data->set_malformed_packets(stats.get_malformed());
    receive_monitor_data->set_misdirected_packets(stats.get_misdirected());
    receive_monitor_data->set_misordered_packets(stats.get_misordered());

    // update packet validity flags
    receive_monitor_data->set_no_valid_polarisation_correction_packets(stats.get_no_valid_polarisation_correction());
    receive_monitor_data->set_no_valid_polarisation_correction_packet_rate(
      static_cast<float>(stats.get_no_valid_polarisation_correction_rate())
    );
    receive_monitor_data->set_no_valid_station_beam_packets(stats.get_no_valid_station_beam());
    receive_monitor_data->set_no_valid_station_beam_packet_rate(
      static_cast<float>(stats.get_no_valid_station_beam_rate())
    );
    receive_monitor_data->set_no_valid_pst_beam_packets(stats.get_no_valid_pst_beam());
    receive_monitor_data->set_no_valid_pst_beam_packet_rate(
      static_cast<float>(stats.get_no_valid_pst_beam_rate())
    );

    PSTLOG_TRACE(this, "RecvLmcServiceHandler get_monitor_data receive_rate={}", stats.get_data_transmission_rate());
    PSTLOG_TRACE(this, "RecvLmcServiceHandler get_monitor_data data_received={}", stats.get_data_transmitted());
    PSTLOG_TRACE(this, "RecvLmcServiceHandler get_monitor_data data_drop_rate={}", stats.get_data_drop_rate());
    PSTLOG_TRACE(this, "RecvLmcServiceHandler get_monitor_data data_dropped={}", stats.get_data_dropped());
    PSTLOG_TRACE(this, "RecvLmcServiceHandler get_monitor_data malformed_packets={}", stats.get_malformed());
    PSTLOG_TRACE(this, "RecvLmcServiceHandler get_monitor_data misdirected_packets={}", stats.get_misdirected());
    PSTLOG_TRACE(this, "RecvLmcServiceHandler get_monitor_data misordered_packets={}", stats.get_misordered());
    PSTLOG_TRACE(this, "RecvLmcServiceHandler get_monitor_data no_valid_polarisation_correction_packets={}", stats.get_no_valid_polarisation_correction());
    PSTLOG_TRACE(this, "RecvLmcServiceHandler get_monitor_data no_valid_polarisation_correction_packet_rate={}", stats.get_no_valid_polarisation_correction_rate());
    PSTLOG_TRACE(this, "RecvLmcServiceHandler get_monitor_data no_valid_station_beam_packets={}", stats.get_no_valid_station_beam());
    PSTLOG_TRACE(this, "RecvLmcServiceHandler get_monitor_data no_valid_station_beam_packet_rate={}", stats.get_no_valid_station_beam_rate());
    PSTLOG_TRACE(this, "RecvLmcServiceHandler get_monitor_data no_valid_pst_beam_packets={}", stats.get_no_valid_pst_beam());
    PSTLOG_TRACE(this, "RecvLmcServiceHandler get_monitor_data no_valid_pst_beam_packet_rate={}", stats.get_no_valid_pst_beam_rate());
}

void ska::pst::recv::RecvLmcServiceHandler::get_env(
    ska::pst::lmc::GetEnvironmentResponse *response
) noexcept
{
    PSTLOG_TRACE(this, "RecvLmcServiceHandler get_env()");
    auto values = response->mutable_values();

    ska::pst::lmc::EnvValue data_host;
    data_host.set_string_value(recv->get_data_host());
    (*values)["data_host"] = data_host;

    ska::pst::lmc::EnvValue data_port;
    data_port.set_signed_int_value(recv->get_data_port());
    (*values)["data_port"] = data_port;

    ska::pst::lmc::EnvValue data_mac;
    data_mac.set_string_value(recv->get_data_mac());
    (*values)["data_mac"] = data_mac;

    PSTLOG_TRACE(this, "RecvLmcServiceHandler get_env returning: {}", response->ShortDebugString());
}

void ska::pst::recv::RecvLmcServiceHandler::go_to_runtime_error(
    std::exception_ptr exc
)
{
    PSTLOG_TRACE(this, "RecvLmcServiceHandler go_to_runtime_error()");
    recv->go_to_runtime_error(exc);
}
