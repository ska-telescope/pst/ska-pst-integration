/*
 * Copyright 2022 Square Kilometre Array Observatory
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 * contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#include <unistd.h>
#include <cstring>
#include <stdexcept>

#include "ska_pst/common/definitions.h"
#include "ska_pst/common/utils/AsciiHeader.h"
#include "ska_pst/common/utils/Logging.h"
#include "ska_pst/common/utils/PacketGeneratorFactory.h"
#include "ska_pst/common/utils/ValidationRegex.h"

#include "ska_pst/recv/clients/UDPReceiver.h"
#include "ska_pst/recv/formats/UDPFormatFactory.h"

ska::pst::recv::UDPReceiver::UDPReceiver(std::shared_ptr<SocketReceive> sock, const std::string& host) :
  ska::pst::recv::Receiver(host),
  socket(std::move(sock))
{
  beam_config_value_patterns = {
      { "DATA_HOST", ska::pst::common::ipv4_regex },
      { "UDP_FORMAT", ska::pst::common::udp_format_regex },
      { "OS_FACTOR", ska::pst::common::os_ratio_regex },
      { "NCHAN", ska::pst::common::positive_int_regex },
      { "NBIT", ska::pst::common::positive_int_regex },
      { "NPOL", ska::pst::common::positive_int_regex },
      { "NDIM", ska::pst::common::positive_int_regex },
      { "TSAMP", ska::pst::common::positive_float_regex },
      { "BW", ska::pst::common::positive_float_regex },
      { "FREQ", ska::pst::common::positive_float_regex },
      { "START_CHANNEL", ska::pst::common::positive_int_regex },
      { "END_CHANNEL", ska::pst::common::positive_int_regex }
  };
  scan_config_value_patterns = {
      { "SOURCE", ska::pst::common::source_regex }
  };
  start_scan_config_value_patterns = {
      { "SCAN_ID", ska::pst::common::positive_int_regex }
  };
}

ska::pst::recv::UDPReceiver::UDPReceiver(std::shared_ptr<SocketReceive> sock, const std::string& host, int port) :
  ska::pst::recv::Receiver(host, port),
  socket(std::move(sock))
{
  beam_config_value_patterns = {
      { "DATA_HOST", ska::pst::common::ipv4_regex },
      { "UDP_FORMAT", ska::pst::common::udp_format_regex },
      { "OS_FACTOR", ska::pst::common::os_ratio_regex },
      { "NCHAN", ska::pst::common::positive_int_regex },
      { "NBIT", ska::pst::common::positive_int_regex },
      { "NPOL", ska::pst::common::positive_int_regex },
      { "NDIM", ska::pst::common::positive_int_regex },
      { "TSAMP", ska::pst::common::positive_float_regex },
      { "BW", ska::pst::common::positive_float_regex },
      { "FREQ", ska::pst::common::positive_float_regex },
      { "START_CHANNEL", ska::pst::common::positive_int_regex },
      { "END_CHANNEL", ska::pst::common::positive_int_regex }
  };
  scan_config_value_patterns = {
      { "SOURCE", ska::pst::common::source_regex },
  };
  start_scan_config_value_patterns = {
      { "SCAN_ID", ska::pst::common::positive_int_regex }
  };
}

ska::pst::recv::UDPReceiver::~UDPReceiver()
{
  PSTLOG_TRACE(this, "dtor - quit");
  quit();
  PSTLOG_TRACE(this, "dtor - quit done");
}

void ska::pst::recv::UDPReceiver::perform_configure_beam()
{
  ska::pst::recv::Receiver::parse_beam_config();

  if(beam_config.has("INITIAL_PACKET_TIMEOUT_THRESHOLD"))
  {
    stats.set_initial_packet_timeout_threshold(beam_config.get<uint64_t>("INITIAL_PACKET_TIMEOUT_THRESHOLD"));
  }
  if(beam_config.has("PACKET_TIMEOUT_THRESHOLD"))
  {
    stats.set_packet_timeout_threshold(beam_config.get<uint64_t>("PACKET_TIMEOUT_THRESHOLD"));
  }
  std::string format_name = beam_config.get_val("UDP_FORMAT");
  format = ska::pst::recv::UDPFormatFactory(format_name);

  // config has been prepared by Receiver::configure_beam
  format->configure_beam(beam_config);

  // configure the UDPStats counter
  stats.configure(format->get_packet_size(), format->get_packet_data_size());

  PSTLOG_DEBUG(this, "perform_configure_beam socket->allocate_resources({}, {})", ska::pst::common::ideal_kernel_socket_bufsz, format->get_packet_size());
  socket->allocate_resources(ska::pst::common::ideal_kernel_socket_bufsz, format->get_packet_size());

  // create and open a UDP receiving socket
  PSTLOG_DEBUG(this, "perform_configure_beam socket->configure({}, {})", data_host, data_port);
  socket->configure(data_host, data_port);

  // virtual block, make about 128 MB
  data.bufsz = format->get_resolution();
  weights.bufsz = format->get_weights_resolution();
  PSTLOG_DEBUG(this, "perform_configure_beam format data_resolution={} weights_resolution={}", data.bufsz, weights.bufsz);
  while (data.bufsz < ska::pst::common::receive_buffer_size)
  {
    data.bufsz *= 2;
    weights.bufsz *= 2;
  }

  PSTLOG_DEBUG(this, "perform_configure_beam data.bufsz={} weights.bufsz={}", data.bufsz, weights.bufsz);
  _data_buffer_a.resize(data.bufsz);
  _data_buffer_b.resize(data.bufsz);
  _weights_buffer_a.resize(weights.bufsz);
  _weights_buffer_b.resize(weights.bufsz);

  // page each of the buffers into RAM
  memset(&_data_buffer_a[0], 0, data.bufsz);
  memset(&_data_buffer_b[0], 0, data.bufsz);
  memset(&_weights_buffer_a[0], 0, weights.bufsz);
  memset(&_weights_buffer_b[0], 0, weights.bufsz);

  // set the curr and next pointers
  data.curr_buf = &_data_buffer_a[0];
  data.next_buf = &_data_buffer_b[0];
  weights.curr_buf = &_weights_buffer_a[0];
  weights.next_buf = &_weights_buffer_b[0];
}

void ska::pst::recv::UDPReceiver::perform_deconfigure_beam()
{
  PSTLOG_DEBUG(this, "perform_deconfigure_beam closing UDP socket");
  socket->deconfigure_beam();

  _data_buffer_a.resize(0);
  _data_buffer_b.resize(0);
  _weights_buffer_a.resize(0);
  _weights_buffer_b.resize(0);
  beam_config.reset();
}

void ska::pst::recv::UDPReceiver::perform_configure_scan()
{
  ska::pst::recv::Receiver::parse_scan_config();

  // prepare the format with the full header
  PSTLOG_DEBUG(this, "perform_configure_scan format->configure_scan(scan_config)");
  format->configure_scan(scan_config);

  // test for optional specification of packet data generator
  std::string dg_name = "none";
  try { dg_name = scan_config.get_val("DATA_GENERATOR"); }
  catch (std::exception& exc) { dg_name = "none"; }

  if (dg_name != "none" )
  {
    data_generator = ska::pst::common::PacketGeneratorFactory(dg_name, format);
  }
}

void ska::pst::recv::UDPReceiver::perform_deconfigure_scan()
{
  utc_start.set_time(static_cast<time_t>(0));

  PSTLOG_DEBUG(this, "perform_deconfigure format->conclude()");
  format->conclude();

  PSTLOG_DEBUG(this, "perform_deconfigure resetting header and stats");
  scan_config.reset();
  stats.reset();
  if (data_generator)
  {
    data_generator.reset();
  }
}

void ska::pst::recv::UDPReceiver::perform_start_scan()
{
  PSTLOG_DEBUG(this, "perform_start_scan");
  reset_accounting_meta(&data);
  reset_accounting_meta(&weights);
  PSTLOG_DEBUG(this, "perform_start_scan calling start_receiving");
  start_receiving();
  auto scan_id = get_scan_id();
  PSTLOG_DEBUG(this, "perform_start_scan scan_id={}",  scan_id);
  format->set_scan_id(scan_id);
  stats.reset();
  monitor_thread = std::make_unique<std::thread>(std::thread(&ska::pst::recv::Receiver::monitor_method, this));
}

void ska::pst::recv::UDPReceiver::perform_scan()
{
  PSTLOG_DEBUG(this, "perform_scan waiting for the start of the packet stream");

  // busy wait loop to acquire the first packet of the scan$a
  bool has_started = false;
  while (!has_started && socket->still_receiving())
  {
    int slot = socket->acquire_packet();
    if (slot >= 0)
    {
      char * packet_buffer = socket->get_buf_ptr(slot);
      ska::pst::recv::UDPFormat::PacketState packet_state = format->process_stream_start(packet_buffer);
      has_started = (packet_state == ska::pst::recv::UDPFormat::PacketState::Ok);
      if (has_started)
      {
        utc_start = format->get_start_timestamp();
      }
      else
      {
        socket->release_packet(slot);
      }
    }
    else
    {
      PSTLOG_TRACE(this, "perform_scan no packets available {}", slot);
    }
  }

  PSTLOG_DEBUG(this, "UDPReceiver perform_scan received packet stream");

  if (data_generator)
  {
    PSTLOG_TRACE(this, "UDPReceiver perform_scan configuring data_generator");
    ska::pst::common::AsciiHeader header;
    header.clone(scan_config);
    header.set_val("UTC_START", utc_start.get_gmtime());
    if (!header.has("OBS_OFFSET"))
    {
      header.set("OBS_OFFSET", 0);
    }
    data_generator->configure(header);
    PSTLOG_TRACE(this, "UDPReceiver perform_scan data_generator configured");
  }

  // initialise the weights scale factors in case of dropped packets
  format->clear_scales_buffer(weights.next_buf, weights.bufsz);

  PSTLOG_DEBUG(this, "perform_scan starting main loop");
  while (is_scanning() && socket->still_receiving())
  {
    receive_block();

    // rotate the data and weights
    rotate_accounting_meta(&data);
    rotate_accounting_meta(&weights);

    // swap curr/next buffers
    rotate_accounting_buffers(&data);
    rotate_accounting_buffers(&weights);

    // initialise the weights scale factors in case of dropped packets
    format->clear_scales_buffer(weights.next_buf, weights.bufsz);
  }
  PSTLOG_DEBUG(this, "perform_scan exited main loop, printing resulting stats");
}

void ska::pst::recv::UDPReceiver::receive_block()
{
  PSTLOG_TRACE(this, "receive_block data [{} - {} - {}]", data.curr_byte_offset, data.next_byte_offset, data.last_byte_offset);
  PSTLOG_TRACE(this, "receive_block weights [{} - {} - {}]", weights.curr_byte_offset, weights.next_byte_offset, weights.last_byte_offset);

  const int packet_size = static_cast<int>(format->get_packet_size());
  const auto packet_size_u64 = static_cast<uint64_t>(packet_size);
  bool filled_curr_buffer = false;
  ska::pst::recv::UDPFormat::PacketState packet_state{};
  ska::pst::recv::UDPFormat::data_and_weights_t offsets{}, received{};

  while (!filled_curr_buffer && socket->still_receiving())
  {
    // get a packet from the socket
    int slot = socket->acquire_packet();
    if (slot >= 0)
    {
      // decode the header so that the format knows what to do with the packet
      packet_state = format->decode_packet(socket->get_buf_ptr(slot), &offsets, &received);
      if (packet_state == ska::pst::recv::UDPFormat::PacketState::Ok)
      {
        // packet belongs in current buffer
        if ((offsets.data >= data.curr_byte_offset) && (offsets.data < data.next_byte_offset))
        {
          data.bytes_curr_buf += received.data;
          weights.bytes_curr_buf += received.weights;

          stats.increment_bytes(packet_size_u64);
          format->insert_last_packet(data.curr_buf + (offsets.data - data.curr_byte_offset), weights.curr_buf + (offsets.weights - weights.curr_byte_offset)); // NOLINT

          socket->release_packet(slot);
        }
        else if ((offsets.data >= data.next_byte_offset) && (offsets.data < data.last_byte_offset))
        {
          data.bytes_next_buf += received.data;
          weights.bytes_next_buf += received.weights;

          stats.increment_bytes(packet_size_u64);
          format->insert_last_packet(data.next_buf + (offsets.data - data.next_byte_offset), weights.next_buf + (offsets.weights - weights.next_byte_offset)); // NOLINT

          socket->release_packet(slot);
        }
        // ignore
        else if (offsets.data < data.curr_byte_offset)
        {
          #ifdef DEBUG
          PSTLOG_WARN(this, "byte_offset={} < data.curr_byte_offset={}", offsets.data, data.curr_byte_offset);
          #endif
          socket->release_packet(slot);
        }
        else
        {
          #ifdef DEBUG
          PSTLOG_WARN(this, "byte_offset >= data.last_byte_offset");
          #endif
          filled_curr_buffer = true;
        }
        format->update_statistics(stats);
      }
      else if (packet_state == ska::pst::recv::UDPFormat::PacketState::Ignored)
      {
        socket->release_packet(slot);
        stats.ignored();
      }
      else if (packet_state == ska::pst::recv::UDPFormat::PacketState::Malformed)
      {
        socket->release_packet(slot);
        stats.malformed();
      }
      else if (packet_state == ska::pst::recv::UDPFormat::PacketState::Misdirected)
      {
        socket->release_packet(slot);
        stats.misdirected();
      }

      if (data.bytes_curr_buf >= data.bufsz || data.bytes_next_buf >= data.bufsz/2 || filled_curr_buffer || !socket->still_receiving())
      {
        PSTLOG_DEBUG(this, "Curr {} / {} => {}", data.bytes_curr_buf, data.bufsz, ((float(data.bytes_curr_buf) / float(data.bufsz)) * ska::pst::common::percentiles_per_100));
        PSTLOG_DEBUG(this, "Next {} / {} => {}", data.bytes_next_buf, data.bufsz, ((float(data.bytes_next_buf) / float(data.bufsz)) * ska::pst::common::percentiles_per_100));
        PSTLOG_DEBUG(this, "receive_block bytes_curr_buf={} bytes_per_buf={} bytes_next_buf={}", data.bytes_curr_buf, data.bufsz, data.bytes_next_buf);
        if (data.bufsz > data.bytes_curr_buf)
        {
          PSTLOG_DEBUG(this, "Dropped {} bytes", (data.bufsz - data.bytes_curr_buf));
        }
        stats.dropped_bytes(data.bufsz - data.bytes_curr_buf);
        filled_curr_buffer = true;
      }
    }
    else if (slot == MALFORMED_PACKET)
    {
      stats.malformed();
    }
  }

  if (data_generator)
  {
    PSTLOG_INFO(this, "validating data buffer of {} bytes", data.bytes_curr_buf);
    if (!data_generator->test_data(data.curr_buf, data.bytes_curr_buf))
    {
      throw std::runtime_error("receive_block unexpected byte sequence in data buffer");
    }

    PSTLOG_INFO(this, "UDPReceive validating weights buffer of {} bytes", weights.bytes_curr_buf);
    if (!data_generator->test_weights(weights.curr_buf, weights.bytes_curr_buf))
    {
      throw std::runtime_error("receive_block unexpected byte sequence in weight buffer");
    }
  }

  stats.sleeps(socket->process_sleeps());
}

auto ska::pst::recv::UDPReceiver::get_data_bufsz() -> uint64_t
{
  if (!is_beam_configured())
  {
    PSTLOG_ERROR(this, "call to get_data_bufsz when not in a beam configured state");
    throw std::runtime_error("{} get_data_bufsz resources were not assigned");
  }
  return data.bufsz;
}

void ska::pst::recv::UDPReceiver::perform_terminate()
{
  PSTLOG_DEBUG(this, "perform_terminate");
}
