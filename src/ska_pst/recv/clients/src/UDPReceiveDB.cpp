/*
 * Copyright 2022 Square Kilometre Array Observatory
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 * contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#include <unistd.h>
#include <stdexcept>
#include <string>

#include "ska_pst/common/definitions.h"
#include "ska_pst/common/utils/Logging.h"
#include "ska_pst/common/utils/ValidationRegex.h"

#include "ska_pst/recv/clients/UDPReceiveDB.h"
#include "ska_pst/recv/formats/UDPFormatFactory.h"

ska::pst::recv::UDPReceiveDB::UDPReceiveDB(std::shared_ptr<ska::pst::recv::SocketReceive> sock, const std::string& host)
   : ska::pst::recv::Receiver(host),
  socket(std::move(sock))
{
  PSTLOG_TRACE(this, "ctor - host={}", host);
  beam_config_value_patterns = {
      { "DATA_HOST", ska::pst::common::ipv4_regex },
      { "DATA_KEY", ska::pst::common::dada_key_regex },
      { "WEIGHTS_KEY", ska::pst::common::dada_key_regex },
      { "UDP_FORMAT", ska::pst::common::udp_format_regex },
      { "OS_FACTOR", ska::pst::common::os_ratio_regex },
      { "NCHAN", ska::pst::common::positive_int_regex },
      { "NBIT", ska::pst::common::positive_int_regex },
      { "NPOL", ska::pst::common::positive_int_regex },
      { "NDIM", ska::pst::common::positive_int_regex },
      { "TSAMP",ska::pst::common::positive_float_regex },
      { "BW", ska::pst::common::positive_float_regex },
      { "FREQ", ska::pst::common::positive_float_regex },
      { "START_CHANNEL", ska::pst::common::positive_int_regex },
      { "END_CHANNEL", ska::pst::common::positive_int_regex },
      { "BEAM_ID",ska::pst::common::positive_int_regex }
  };
  scan_config_value_patterns = {
      { "SOURCE", ska::pst::common::source_regex },
      { "EB_ID", ska::pst::common::eb_id_regex }
  };
  start_scan_config_value_patterns = {
      { "SCAN_ID", ska::pst::common::positive_int_regex }
  };
}

ska::pst::recv::UDPReceiveDB::UDPReceiveDB(std::shared_ptr<ska::pst::recv::SocketReceive> sock, const std::string& host, int port)
   : ska::pst::recv::Receiver(host, port),
  socket(std::move(sock))
{
  PSTLOG_TRACE(this, "ctor - host={} port={}", host, port);
  beam_config_value_patterns = {
      { "DATA_HOST", ska::pst::common::ipv4_regex },
      { "DATA_KEY", ska::pst::common::dada_key_regex },
      { "WEIGHTS_KEY", ska::pst::common::dada_key_regex },
      { "UDP_FORMAT", ska::pst::common::udp_format_regex },
      { "OS_FACTOR", ska::pst::common::os_ratio_regex },
      { "NCHAN", ska::pst::common::positive_int_regex },
      { "NBIT", ska::pst::common::positive_int_regex },
      { "NPOL", ska::pst::common::positive_int_regex },
      { "NDIM", ska::pst::common::positive_int_regex },
      { "TSAMP", ska::pst::common::positive_float_regex },
      { "BW", ska::pst::common::positive_float_regex },
      { "FREQ", ska::pst::common::positive_float_regex },
      { "START_CHANNEL", ska::pst::common::positive_int_regex },
      { "END_CHANNEL", ska::pst::common::positive_int_regex },
      { "BEAM_ID", ska::pst::common::positive_int_regex }
  };
  scan_config_value_patterns = {
      { "SOURCE", ska::pst::common::source_regex },
      { "EB_ID", ska::pst::common::eb_id_regex },
  };
  start_scan_config_value_patterns = {
      { "SCAN_ID", ska::pst::common::positive_int_regex },
  };
}

ska::pst::recv::UDPReceiveDB::~UDPReceiveDB()
{
  PSTLOG_TRACE(this, "dtor - calling quit");
  quit();
}

void ska::pst::recv::UDPReceiveDB::perform_configure_beam()
{
  ska::pst::recv::Receiver::parse_beam_config();

  if(beam_config.has("INITIAL_PACKET_TIMEOUT_THRESHOLD"))
  {
    stats.set_initial_packet_timeout_threshold(beam_config.get<uint64_t>("INITIAL_PACKET_TIMEOUT_THRESHOLD"));
  }
  if(beam_config.has("PACKET_TIMEOUT_THRESHOLD"))
  {
    stats.set_packet_timeout_threshold(beam_config.get<uint64_t>("PACKET_TIMEOUT_THRESHOLD"));
  }

  // extract the data and header keys from the configuration file
  std::string data_key = beam_config.get_val("DATA_KEY");
  db = std::make_unique<ska::pst::smrb::DataBlockWrite>(data_key);

  std::string weights_key = beam_config.get_val("WEIGHTS_KEY");
  wb = std::make_unique<ska::pst::smrb::DataBlockWrite>(weights_key);

  std::string udp_format = beam_config.get_val("UDP_FORMAT");
  format = ska::pst::recv::UDPFormatFactory(udp_format);
  format->set_validity_flags_policy(validity_flags_policy);

  db->connect(timeout);
  db->lock();
  db->page();

  wb->connect(timeout);
  wb->lock();
  wb->page();

  PSTLOG_DEBUG(this, "configure_beam getting data sizes");
  data.bufsz = db->get_data_bufsz();
  weights.bufsz = wb->get_data_bufsz();

  PSTLOG_DEBUG(this, "configure_beam format->configure_beam()");
  format->configure_beam(beam_config);

  // now write new params to config
  uint64_t resolution = format->get_resolution();
  beam_config.set("RESOLUTION", resolution);
  PSTLOG_DEBUG(this, "configure_beam resolution={}", resolution);
  if (data.bufsz % resolution != 0)
  {
    PSTLOG_ERROR(this, "configure_beam DB buffer size[{}] must be a multiple of RESOLUTION[{}]",
      data.bufsz, resolution);
    throw std::runtime_error("configure_beam bad data buffer size");
  }

  if (weights.bufsz % format->get_weights_resolution() != 0)
  {
    PSTLOG_ERROR(this, "configure_beam weights DB buffer size[{}] must be a multiple of RESOLUTION[{}]",
      weights.bufsz, format->get_weights_resolution());
    throw std::runtime_error("configure_beam bad weights DB buffer size");
  }

  uint64_t data_nresolution = data.bufsz / resolution;
  uint64_t weights_nresolution = weights.bufsz / format->get_weights_resolution();
  if (data_nresolution != weights_nresolution)
  {
    PSTLOG_ERROR(this, "configure_beam data_nresolution[{}] != weights_nresolution[{}]", data_nresolution, weights_nresolution);
    throw std::runtime_error("configure_beam bad data / weights relative buffer size");
  }
  stats.configure(format->get_packet_size(), format->get_packet_data_size());

  // configure the kernel buffer size for the socket
  size_t bufsz = ska::pst::common::ideal_kernel_socket_bufsz;
  PSTLOG_DEBUG(this, "configure_beam socket->allocate_resources({}, {})", bufsz, format->get_packet_size());
  socket->allocate_resources(bufsz, format->get_packet_size());

  PSTLOG_DEBUG(this, "configure_beam configuring the socket");
  socket->configure(data_host, data_port);
}

void ska::pst::recv::UDPReceiveDB::perform_deconfigure_beam()
{
  PSTLOG_DEBUG(this, "perform_deconfigure_beam disconnecting from db");
  db->unlock();
  db->disconnect();

  PSTLOG_DEBUG(this, "perform_deconfigure_beam disconnecting from wb");
  wb->unlock();
  wb->disconnect();

  PSTLOG_DEBUG(this, "perform_deconfigure_beam socket->deconfigure_beam()");
  socket->deconfigure_beam();

  beam_config.reset();
}

void ska::pst::recv::UDPReceiveDB::perform_configure_scan()
{
  PSTLOG_DEBUG(this, "perform_configure_scan ska::pst::recv::Receiver::configure_scan()");
  ska::pst::recv::Receiver::parse_scan_config();

  PSTLOG_DEBUG(this, "perform_configure_scan set HDR_VERSION and HDR_SIZE");
  scan_config.set_val("HDR_VERSION", "1.0");
  scan_config.set("HDR_SIZE", db->get_header_bufsz());

  // determine the Telescope from the UDP Format
  std::string udp_format = scan_config.get_val("UDP_FORMAT");
  if (udp_format.find("LowPST") != std::string::npos)
  {
    scan_config.set_val("TELESCOPE", "SKALow");
    scan_config.set_val("INSTRUMENT", "LowCBF");
  }
  else if (udp_format.find("MidPST") != std::string::npos)
  {
    scan_config.set_val("TELESCOPE", "SKAMid");
    scan_config.set_val("INSTRUMENT", "MidCBF");
  }
  else
  {
    scan_config.set_val("TELESCOPE", "Unknown");
  }

  if (!scan_config.has("UTC_START"))
  {
    scan_config.set_val("UTC_START", "TBD");
  }

  for (auto& key: keys_to_remove)
  {
    if (scan_config.has(key))
    {
      scan_config.del(key);
    }
  }

  PSTLOG_DEBUG(this, "perform_configure_scan format->configure_scan(scan_config)");
  format->configure_scan(scan_config);

  // write the scan configuration to the dbs
  PSTLOG_DEBUG(this, "perform_configure_scan writing config to dbs");
  db->write_config(scan_config.raw());
  wb->write_config(format->get_weights_scan_config().raw());
}

void ska::pst::recv::UDPReceiveDB::perform_deconfigure_scan()
{
  utc_start.set_time(static_cast<time_t>(0));

  PSTLOG_DEBUG(this, "perform_deconfigure format->conclude()");
  format->conclude();

  // clear the header
  PSTLOG_DEBUG(this, "perform_deconfigure resetting header and stats");
  scan_config.reset();
  stats.reset();

  // clear the data block and weights block header config
  db->clear_config();
  wb->clear_config();
}

void ska::pst::recv::UDPReceiveDB::perform_start_scan()
{
  PSTLOG_DEBUG(this, "UDPReceiveDB perform_start_scan called");
  // block accounting
  reset_accounting_meta(&data);
  reset_accounting_meta(&weights);

  PSTLOG_DEBUG(this, "perform_start_scan calling start_receiving");
  start_receiving();
  auto scan_id = get_scan_id();
  PSTLOG_DEBUG(this, "perform_start_scan scan_id={}", scan_id);
  format->set_scan_id(scan_id);
  stats.reset();

  // start the monitoring thread
  monitor_thread = std::make_unique<std::thread>(std::thread(&ska::pst::recv::Receiver::monitor_method, this));
}

void ska::pst::recv::UDPReceiveDB::perform_scan()
{
  PSTLOG_DEBUG(this, "perform_scan()");
  uint64_t data_resolution = format->get_resolution();
  uint64_t weights_resolution = format->get_weights_resolution();

  PSTLOG_DEBUG(this, "perform_scan waiting for the start of the packet stream");
  // busy wait loop to acquire the first packet of the scan to extract the utc_start timestamp
  while (is_scanning() && socket->still_receiving())
  {
    int slot = socket->acquire_packet();

    if (slot >= 0)
    {
      char * packet_buffer = socket->get_buf_ptr(slot);
      ska::pst::recv::UDPFormat::PacketState packet_state = format->process_stream_start(packet_buffer);
      if (packet_state == ska::pst::recv::UDPFormat::PacketState::Ok)
      {
        utc_start = format->get_start_timestamp();
        PSTLOG_DEBUG(this, "perform_scan has started at utc_start={}", utc_start.get_gmtime());
        break;
      }
      else
      {
        socket->release_packet(slot);
      }
    }
    else
    {
      PSTLOG_TRACE(this, "perform_scan no packets available{}", slot);
    }
  }
  PSTLOG_DEBUG(this, "perform_scan exited loop waiting for the start of the packet stream");

  PSTLOG_DEBUG(this, "perform_scan UTC_START={} ATTOSECONDS={}", utc_start.get_gmtime(), utc_start.get_fractional_time_attoseconds());

  // generate the full header to be written to the data blocks
  ska::pst::common::AsciiHeader data_header, weights_header;

  data_header.clone(scan_config);
  data_header.set_val("UTC_START", utc_start.get_gmtime());
  data_header.set("PICOSECONDS", static_cast<uint64_t>(utc_start.get_fractional_time_attoseconds() / ska::pst::common::attoseconds_per_picosecond));
  data_header.set("SCAN_ID", get_scan_id());

  weights_header.clone(format->get_weights_scan_config());
  weights_header.set_val("UTC_START", utc_start.get_gmtime());
  weights_header.set("PICOSECONDS", static_cast<uint64_t>(utc_start.get_fractional_time_attoseconds() / ska::pst::common::attoseconds_per_picosecond));
  weights_header.set("SCAN_ID", get_scan_id());

  PSTLOG_TRACE(this, "UDPReceiveDB perform_scan data_header=\n{}", data_header.raw());
  PSTLOG_TRACE(this, "UDPReceiveDB perform_scan weights_header=\n{}", weights_header.raw());

  // write the full data and weights headers to the dbs
  db->write_header(data_header.raw());
  wb->write_header(weights_header.raw());

  // open the data block for writing
  PSTLOG_DEBUG(this, "perform_scan db->open() wb->open()");
  db->open();
  wb->open();

  data.curr_buf = db->open_block();
  weights.curr_buf = wb->open_block();

  // initialise the weights scale factors in case of dropped packets
  format->clear_scales_buffer(weights.curr_buf, weights.bufsz);

  uint64_t bytes_written{0};

  PSTLOG_DEBUG(this, "perform_scan starting main loop");
  // we have control command, so start the main loop
  while (is_scanning() && socket->still_receiving())
  {
    // open the next_bufs
    data.next_buf = db->open_block();
    weights.next_buf = wb->open_block();

    // initialise the weights scale factors in case of dropped packets
    format->clear_scales_buffer(weights.next_buf, weights.bufsz);

    receive_block();

    // close the data curr_buff
    bytes_written = (socket->still_receiving()) ? data.bufsz : (data.bytes_curr_buf - (data.bytes_curr_buf % data_resolution));
    PSTLOG_DEBUG(this, "perform_scan wrote {} bytes to db", bytes_written);
    db->close_block(bytes_written);

    // close the weights curr_buff
    bytes_written = (socket->still_receiving()) ? weights.bufsz : (weights.bytes_curr_buf - (weights.bytes_curr_buf % weights_resolution));
    PSTLOG_DEBUG(this, "perform_scan wrote {} bytes to wb", bytes_written);
    wb->close_block(bytes_written);

    // rotate the data and weights
    rotate_accounting_meta(&data);
    rotate_accounting_meta(&weights);

    // swap curr/next buffers
    advance_accounting_buffers(&data);
    advance_accounting_buffers(&weights);
  }

  PSTLOG_DEBUG(this, "perform_scan exited main loop");

  bytes_written = data.bytes_next_buf - (data.bytes_next_buf % data_resolution);
  PSTLOG_DEBUG(this, "perform_scan wrote {} bytes to db", bytes_written);
  db->close_block(bytes_written);

  bytes_written = weights.bytes_next_buf - (weights.bytes_next_buf % weights_resolution);
  PSTLOG_DEBUG(this, "perform_scan wrote {} bytes to wb", bytes_written);
  wb->close_block(bytes_written);

  if (db->is_block_open())
  {
    PSTLOG_DEBUG(this, "perform_scan db->close_block({})", data.bytes_curr_buf);
    db->close_block(data.bytes_curr_buf);
    data.bytes_curr_buf = 0;
  }

  if (wb->is_block_open())
  {
    PSTLOG_DEBUG(this, "perform_scan wb->close_block({})", weights.bytes_curr_buf);
    wb->close_block(weights.bytes_curr_buf);
    weights.bytes_curr_buf = 0;
  }

  // close the dbs, causing the EoD marker to be written
  PSTLOG_DEBUG(this, "perform_scan closing data blocks");
  db->close();
  wb->close();

  PSTLOG_DEBUG(this, "perform_scan wait for StoppingScan");
}

// receive a block of UDP data
void ska::pst::recv::UDPReceiveDB::receive_block()
{
  PSTLOG_DEBUG(this, "receive_block filling data [{} - {} - {}]", data.curr_byte_offset, data.next_byte_offset, data.last_byte_offset);
  PSTLOG_DEBUG(this, "receive_block filling weights [{} - {} - {}]", weights.curr_byte_offset, weights.next_byte_offset, weights.last_byte_offset);

  const auto packet_size = static_cast<int>(format->get_packet_size());
  const auto packet_size_u64 = static_cast<uint64_t>(packet_size);
  ska::pst::recv::UDPFormat::PacketState packet_state{};
  ska::pst::recv::UDPFormat::data_and_weights_t offsets{}, received{};
  bool filled_curr_buffer = false;

  // tighter loop to fill this buffer
  while (!filled_curr_buffer && socket->still_receiving())
  {
    // get a packet from the socket
    int slot = socket->acquire_packet();

    if (slot >= 0)
    {
      // decode the header so that the format knows what to do with the packet
      packet_state = format->decode_packet(socket->get_buf_ptr(slot), &offsets, &received);

      // packet that is part of this observation
      if (packet_state == ska::pst::recv::UDPFormat::PacketState::Ok)
      {
        // packet belongs in current buffer
        if ((offsets.data >= data.curr_byte_offset) && (offsets.data < data.next_byte_offset))
        {
          data.bytes_curr_buf += received.data;
          weights.bytes_curr_buf += received.weights;

          stats.increment_bytes(packet_size_u64);
          format->insert_last_packet(data.curr_buf + (offsets.data - data.curr_byte_offset), weights.curr_buf + (offsets.weights - weights.curr_byte_offset)); // NOLINT

          socket->release_packet(slot);
        }
        // packet belongs in next buffer
        else if ((offsets.data >= data.next_byte_offset) && (offsets.data < data.last_byte_offset))
        {
          data.bytes_next_buf += received.data;
          weights.bytes_next_buf += received.weights;

          stats.increment_bytes(packet_size_u64);
          format->insert_last_packet(data.next_buf + (offsets.data - data.next_byte_offset), weights.next_buf + (offsets.weights - weights.next_byte_offset)); // NOLINT

          socket->release_packet(slot);
        }
        // ignore packets the precede this buffer
        else if (offsets.data < data.curr_byte_offset)
        {
          PSTLOG_TRACE(this, "offsets.data={} data.curr_byte_offset={}", offsets.data, data.curr_byte_offset);
          socket->release_packet(slot);
          stats.misordered();
        }
        // packet is beyond next buffer
        else
        {
          PSTLOG_TRACE(this, "received data beyond current and next buffer, dropping data");
          filled_curr_buffer = true;
        }
        format->update_statistics(stats);
      }
      else if (packet_state == ska::pst::recv::UDPFormat::PacketState::Ignored)
      {
        socket->release_packet(slot);
        stats.ignored();
      }
      else if (packet_state == ska::pst::recv::UDPFormat::PacketState::Malformed)
      {
        socket->release_packet(slot);
        stats.malformed();
      }
      else if (packet_state == ska::pst::recv::UDPFormat::PacketState::Misdirected)
      {
        socket->release_packet(slot);
        stats.misdirected();
      }

      // close open data block buffer if is is now full
      if (data.bytes_curr_buf >= static_cast<int64_t>(data.bufsz) || filled_curr_buffer || !socket->still_receiving())
      {
        PSTLOG_DEBUG(this, "Curr {} / {} => {}", data.bytes_curr_buf, data.bufsz, ((float(data.bytes_curr_buf) / float(data.bufsz)) * ska::pst::common::percentiles_per_100));
        PSTLOG_DEBUG(this, "Next {} / {} => {}", data.bytes_next_buf, data.bufsz, ((float(data.bytes_next_buf) / float(data.bufsz)) * ska::pst::common::percentiles_per_100));
        PSTLOG_DEBUG(this, "receive_block bytes_curr_buf={} bytes_per_buf={} bytes_next_buf={} filled_curr_buffer={}",
          data.bytes_curr_buf, data.bufsz, data.bytes_next_buf, filled_curr_buffer);
        stats.dropped_bytes(data.bufsz - data.bytes_curr_buf);
        filled_curr_buffer = true;
      }
    }
    //
    else if (slot == MALFORMED_PACKET)
    {
      PSTLOG_WARN(this, "receive_block received malformed packet");
      stats.malformed();
    }
  }
  stats.sleeps(socket->process_sleeps());
}

void ska::pst::recv::UDPReceiveDB::close_curr_bufs()
{
  PSTLOG_DEBUG(this, "close_curr_bufs data.curr_buf={}", reinterpret_cast<void *>(data.curr_buf));
  if (data.curr_buf)
  {
    PSTLOG_DEBUG(this, "close_curr_bufs close_block({})", data.bytes_curr_buf);
    db->close_block(data.bytes_curr_buf);
  }
  data.curr_buf = nullptr;

  PSTLOG_DEBUG(this, "close_curr_bufs weights.curr_buf={}", reinterpret_cast<void *>(weights.curr_buf));
  if (weights.curr_buf)
  {
    PSTLOG_DEBUG(this, "close_curr_bufs close_block({})", weights.bytes_curr_buf);
    wb->close_block(weights.bytes_curr_buf);
  }
  weights.curr_buf = nullptr;
}

auto ska::pst::recv::UDPReceiveDB::get_data_bufsz() -> uint64_t
{
  if (!is_beam_configured())
  {
    PSTLOG_ERROR(this, "call to get_data_bufsz when not in a beam configured state");
    throw std::runtime_error("get_data_bufsz resources were not assigned");
  }
  return db->get_data_bufsz();
}

void ska::pst::recv::UDPReceiveDB::perform_terminate()
{
  PSTLOG_DEBUG(this, "perform_terminate");
}

void ska::pst::recv::UDPReceiveDB::set_validity_flags_policy(ska::pst::recv::PacketValidityFlagsPolicy _policy)
{
  PSTLOG_DEBUG(this, "set_validity_flags_policy - policy='{}'", ska::pst::recv::packet_validity_flags_policy_names[_policy]);
  validity_flags_policy = _policy;
}
