/*
 * Copyright 2022 Square Kilometre Array Observatory
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 * contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#include <unistd.h>
#include <ctime>
#include <regex>
#include <iostream>
#include <sstream>
#include <stdexcept>

#include "ska_pst/common/definitions.h"
#include "ska_pst/common/utils/Logging.h"
#include "ska_pst/common/utils/Time.h"
#include "ska_pst/common/utils/AsciiHeader.h"

#include "ska_pst/recv/network/SocketReceive.h"
#include "ska_pst/recv/clients/Receiver.h"
#include "ska_pst/recv/formats/UDPFormatFactory.h"


ska::pst::recv::Receiver::Receiver(std::string _data_host, int _data_port) : ska::pst::common::ApplicationManager("recv"), data_host(std::move(_data_host)), data_port(_data_port)
{
  read_mac_addr();
  PSTLOG_TRACE(this, "ctor - data_host={} data_port={} data_mac={}", data_host, data_port, ska::pst::common::mac_to_string(data_mac));
  initialise();
}

ska::pst::recv::Receiver::Receiver(std::string _data_host) : ska::pst::common::ApplicationManager("recv"), data_host(std::move(_data_host))
{
  read_mac_addr();
  PSTLOG_TRACE(this, "ctor - data_host={} data_mac={}", data_host, ska::pst::common::mac_to_string(data_mac));
  initialise();
}

ska::pst::recv::Receiver::~Receiver()
{
  PSTLOG_TRACE(this, "dtor");

  // if the monitoring thread was not joined in stop_receiving
  if (monitor_thread)
  {
    monitor_thread->join();
    PSTLOG_TRACE(this, "dtor - monitor_thread joined");
    monitor_thread = nullptr;
  }
}

auto ska::pst::recv::Receiver::get_scan_id() -> uint64_t
{
  return startscan_config.get_if_set<uint64_t>("SCAN_ID", UINT64_MAX);
}

void ska::pst::recv::Receiver::read_mac_addr()
{
  try
  {
    data_mac = ska::pst::common::interface_mac(data_host);
    PSTLOG_DEBUG(this, "read_mac_addr data_host={} has data_mac={}", data_host, ska::pst::common::mac_to_string(data_mac));
  }
  catch (std::exception& exc)
  {
    PSTLOG_ERROR(this, "read_mac_addr failed to determine MAC address from data_host={}, using {}", data_host, ska::pst::common::mac_to_string(data_mac));
  }
}

void ska::pst::recv::Receiver::perform_initialise()
{
  PSTLOG_DEBUG(this, "perform_initialise");
  data = INIT_DB_ACCOUNTING_T;
  weights = INIT_DB_ACCOUNTING_T;
}

void ska::pst::recv::Receiver::validate_configure_beam(const ska::pst::common::AsciiHeader& config, ska::pst::common::ValidationContext *context)
{
  PSTLOG_DEBUG(this, "validating beam configuration");
  check_config_value_patterns("beam_config", config, beam_config_value_patterns, context);

  // the check config value patterns check that UDP_FORMAT exists or not
  // this check is to avoid error being thrown if UDP_FORMAT does not
  // exist in the configuration.
  if (config.has("UDP_FORMAT")) {
    std::string format_name = config.get_val("UDP_FORMAT");
    if (!ska::pst::recv::is_format_supported(format_name))
    {
      std::stringstream ss;
      ss << "unknown format value. Supported values are ";
      ss << ska::pst::recv::get_supported_formats_list();
      context->add_validation_error("UDP_FORMAT", format_name, ss.str());
      return;
    }

    auto format = ska::pst::recv::UDPFormatFactory(format_name);
    format->validate_configure_beam(config, context);
  }
}

void ska::pst::recv::Receiver::validate_configure_scan(const ska::pst::common::AsciiHeader& config, ska::pst::common::ValidationContext *context)
{
  PSTLOG_DEBUG(this, "validating scan configuration");
  check_config_value_patterns("scan_config", config, scan_config_value_patterns, context);
}

void ska::pst::recv::Receiver::validate_start_scan(const ska::pst::common::AsciiHeader& config)
{
  PSTLOG_DEBUG(this, "validating start scan");
  ska::pst::common::ValidationContext context;
  check_config_value_patterns("start_scan_config", config, start_scan_config_value_patterns, &context);
  context.throw_error_if_not_empty();
}

void ska::pst::recv::Receiver::perform_stop_scan()
{
  PSTLOG_TRACE(this, "calling stop_receiving");
  stop_receiving();
  PSTLOG_TRACE(this, "calling print_stats");
  print_stats();
  PSTLOG_TRACE(this, "stop_receiving exited");
}

void ska::pst::recv::Receiver::start_receiving()
{
  PSTLOG_DEBUG(this, "setting keep_receiving=true");
  ska::pst::recv::SocketReceive::keep_receiving = true;
}

void ska::pst::recv::Receiver::stop_receiving()
{
  PSTLOG_DEBUG(this, "setting keep_receiving=false");
  ska::pst::recv::SocketReceive::keep_receiving = false;

  if (monitor_thread)
  {
    PSTLOG_DEBUG(this, "waiting for monitoring thread");
    monitor_thread->join();
    PSTLOG_TRACE(this, "monitor_thread joined");
    monitor_thread = nullptr;
  }
}

void ska::pst::recv::Receiver::monitor_method()
{
  // need to wait until scan has started
  //
  // Details:
  // This method is called within a thread that is launched when the state is StartingScan
  // and the Scanning state is set in a different background thread to the perform_start scan
  // and this thread.
  //
  // Unit tests are only seeing the state getting into scanning state and may set the state to
  // StoppingScan before this thread gets started.  The previous wait_for_state would block
  // indefinitely and result in the tests ultimately getting timed out.
  wait_for_not_state(ska::pst::common::State::StartingScan, ska::pst::common::milliseconds_per_second);

  uint64_t report_count = 0;
  static constexpr uint64_t report_header_period = 20;

  PSTLOG_DEBUG(this, "monitoring running");
  bool monitoring = true;
  while (monitoring)
  {
    // This is moved to start to allow at least 1sec of data for monitoring to come through
    // wait for the state to be anything other than scanning, if the timeout occurs
    monitoring = !wait_for_not_state(ska::pst::common::State::Scanning, ska::pst::common::milliseconds_per_second);
    PSTLOG_TRACE(this, "Receiver monitor_method !wait_for_not_state returned monitoring={} state={}", monitoring, get_name(state));

    monitoring &= ska::pst::recv::SocketReceive::keep_receiving;
    PSTLOG_TRACE(this, "Receiver monitor_method &=keep_receiving returned monitoring={} state={}", monitoring, get_name(state));

    if (!monitoring)
    {
      break;
    }

    // compute the instantaneous transmission and drop rates
    stats.compute_rates();

    uint64_t p_drop = stats.get_packets_dropped();
    uint64_t p_ignored = stats.get_ignored();
    uint64_t p_malformed = stats.get_malformed();
    uint64_t p_misdirected = stats.get_misdirected();
    uint64_t p_misordered = stats.get_misordered();
    double gb_recv_ps = stats.get_data_transmission_rate() * ska::pst::common::gigabits_per_byte;
    double gb_drop_ps = stats.get_data_drop_rate() * ska::pst::common::gigabits_per_byte;
    double sleeps_ps = stats.get_sleep_rate();

    // determine how much memory is free in the receivers
    if (report_count % report_header_period == 0)
    {
      PSTLOG_INFO(this, "RX\tDR\tIgnor\tMisdir\tMalfor\tMisor\tDrop\tSleeps");
    }

    std::ostringstream oss;
    oss << gb_recv_ps << "\t" << gb_drop_ps << "\t"
        << p_ignored << "\t" << p_misdirected << "\t"
        << p_malformed << "\t" << p_misordered << "\t"
        << p_drop << "\t" << sleeps_ps;
    PSTLOG_INFO(this, "{}", oss.str());

    report_count++;
  }
  PSTLOG_DEBUG(this, "monitor_thread exiting");
}

void ska::pst::recv::Receiver::parse_beam_config()
{
  PSTLOG_DEBUG(this, "parsing beam config");

  uint32_t nant{1}, nbeam{1};
  try { beam_config.get("NANT", &nant); }
  catch (std::exception& exc) { nant = 1; }

  try { beam_config.get("NBEAM", &nbeam); }
  catch (std::exception& exc){ nbeam = 1; }

  beam_config.get("NCHAN", &nchan);
  beam_config.get("NBIT", &nbit);
  beam_config.get("NPOL", &npol);
  beam_config.get("NDIM", &ndim);
  beam_config.get("TSAMP", &tsamp);
  beam_config.get("BW", &bw);
  beam_config.get("FREQ",&freq);
  beam_config.get("START_CHANNEL", &start_channel);
  beam_config.get("END_CHANNEL", &end_channel);
  PSTLOG_DEBUG(this, "nsig={} nchan={} nbit={} npol={} ndim={} tsamp={} bw={} freq={}",
                nant * nbeam, nchan, nbit, npol, ndim, tsamp, bw, freq);

  bytes_per_second = nant * nbeam * beam_config.compute_bytes_per_second();
  PSTLOG_DEBUG(this, "bytes_per_second={}", bytes_per_second);

  if (data_port <= 0)
  {
    beam_config.get("DATA_PORT", &data_port);
  }

  PSTLOG_DEBUG(this, "receiving {}:{}", data_host, data_port);
}

void ska::pst::recv::Receiver::parse_scan_config()
{
  if (!scan_config.has("SOURCE"))
  {
    throw std::runtime_error("SOURCE did not exist in header");
  }
}

void ska::pst::recv::Receiver::set_config_value_patterns(std::map<std::string,std::string> beam_config, std::map<std::string,std::string> scan_config, std::map<std::string,std::string> start_scan_config)
{
  beam_config_value_patterns = std::move(beam_config);
  scan_config_value_patterns = std::move(scan_config);
  start_scan_config_value_patterns = std::move(start_scan_config);
}

void ska::pst::recv::Receiver::check_config_value_patterns(const std::string& config_name, const ska::pst::common::AsciiHeader& config, const std::map<std::string,std::string>& config_value_patterns, ska::pst::common::ValidationContext *context)
{
  PSTLOG_DEBUG(this, "config_name={}", config_name);
  std::string config_key, config_value_pattern;

  // Loop through keys
  for (const auto &key_value_pair : config_value_patterns)
  {
    config_key = key_value_pair.first;
    config_value_pattern = key_value_pair.second;
    std::regex regex_pattern(key_value_pair.second);

    if (config.has(config_key))
    {
      auto config_value = config.get_val(config_key);
      if (!std::regex_match(config_value, regex_pattern))
      {
        context->add_value_regex_error(config_key, config_value, config_value_pattern);
      }
    }
    else
    {
      context->add_missing_field_error(config_key);
    }
  }
}

void ska::pst::recv::Receiver::print_stats()
{
  PSTLOG_DEBUG(this, "print_stats: stats.get_data_transmitted()={}", stats.get_data_transmitted());
  PSTLOG_DEBUG(this, "print_stats: stats.get_packets_transmitted()={}", stats.get_packets_transmitted());
  PSTLOG_DEBUG(this, "print_stats: stats.get_data_dropped()={}", stats.get_data_dropped());
  PSTLOG_DEBUG(this, "print_stats: stats.get_packets_dropped()={}", stats.get_packets_dropped());
  PSTLOG_DEBUG(this, "print_stats: stats.get_discarded()={}", stats.get_discarded());
  PSTLOG_DEBUG(this, "print_stats: stats.get_nsleeps()={}", stats.get_nsleeps());
  PSTLOG_DEBUG(this, "print_stats: stats.get_ignored()={}", stats.get_ignored());
  PSTLOG_DEBUG(this, "print_stats: stats.get_malformed()={}", stats.get_malformed());
  PSTLOG_DEBUG(this, "print_stats: stats.get_misdirected()={}", stats.get_misdirected());
  PSTLOG_DEBUG(this, "print_stats: stats.get_misordered()={}", stats.get_misordered());
  PSTLOG_DEBUG(this, "print_stats: stats.get_data_transmission_rate()={}", stats.get_data_transmission_rate());
  PSTLOG_DEBUG(this, "print_stats: stats.get_data_drop_rate()={}", stats.get_data_drop_rate());
  PSTLOG_DEBUG(this, "print_stats: stats.get_sleep_rate()={}", stats.get_sleep_rate());
  PSTLOG_DEBUG(this, "print_stats: stats.get_packet_receive_timeouts()={}", stats.get_packet_receive_timeouts());
  PSTLOG_DEBUG(this, "print_stats: stats.get_packet_timeout_threshold()={}", stats.get_packet_timeout_threshold());
  PSTLOG_DEBUG(this, "print_stats: stats.get_initial_packet_timeout_threshold()={}", stats.get_initial_packet_timeout_threshold());
}
