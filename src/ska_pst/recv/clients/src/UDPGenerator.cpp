/*
 * Copyright 2022 Square Kilometre Array Observatory
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 * contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#include <stdexcept>
#include <sys/time.h>
#include <unistd.h>
#include <algorithm>
#include <random>
#include <chrono>

#include "ska_pst/common/definitions.h"
#include "ska_pst/common/utils/Logging.h"
#include "ska_pst/common/utils/PacketGeneratorFactory.h"
#include "ska_pst/common/utils/Time.h"

#include "ska_pst/recv/clients/UDPGenerator.h"
#include "ska_pst/recv/formats/UDPFormatFactory.h"

ska::pst::recv::UDPGenerator::UDPGenerator(std::string host, int port) :
  data_host(std::move(host)), data_port(port)
{
}

ska::pst::recv::UDPGenerator::~UDPGenerator()
{
  sock.close_me();
  data_generator.reset();
}

void ska::pst::recv::UDPGenerator::add_induced_error(uint32_t packet_number, ska::pst::recv::FailureType failure_type)
{
  induced_errors.emplace_back(std::make_pair(packet_number, failure_type));
}

void ska::pst::recv::UDPGenerator::add_induced_error_rate(ska::pst::recv::FailureType failure_type, double rate)
{
  induced_error_rates.emplace_back(std::make_pair(failure_type, rate));
}

void ska::pst::recv::UDPGenerator::add_validity_error(uint32_t packet_number, ska::pst::recv::ValidityType validity_type)
{
  auto has_packet_number = [&packet_number](const std::pair<uint32_t, std::vector<ska::pst::recv::ValidityType>> &v)
  { return packet_number == v.first; };

  auto it = std::find_if(std::begin(validity_errors), std::end(validity_errors), has_packet_number);
  if (it == validity_errors.end())
  {
    validity_errors.emplace_back(std::make_pair(packet_number, std::vector<ska::pst::recv::ValidityType>{validity_type}));
  }
  else
  {
    it->second.emplace_back(validity_type);
  }
}

void ska::pst::recv::UDPGenerator::add_validity_error_rate(ska::pst::recv::ValidityType validity_type, double rate)
{
  validity_error_rates.emplace_back(std::make_pair(validity_type, rate));
}

void ska::pst::recv::UDPGenerator::configure_beam(const ska::pst::common::AsciiHeader &beam_config)
{
  beam_config.get("BEAM_ID", &beam_id);

  try { beam_config.get("SCAN_ID", &scan_id); }
  catch (std::exception& exc) { scan_id = 1; }

  PSTLOG_DEBUG(this, "configure_beam scan_id={}", scan_id);

  PSTLOG_DEBUG(this, "configure_beam parsing host info");
  if (data_host.length() == 0)
  {
    data_host = beam_config.get_val("DATA_HOST");
  }
  if (data_port <= 0)
  {
    beam_config.get("DATA_PORT", &data_port);
  }

  try { local_host = beam_config.get_val("LOCAL_HOST"); }
  catch (std::exception& exc){ local_host = "any"; }

  std::string udp_format = beam_config.get_val("UDP_FORMAT");
  format = ska::pst::recv::UDPFormatFactory(udp_format);

  PSTLOG_DEBUG(this, "configure_beam configuring beam for format");
  format->configure_beam(beam_config);
  PSTLOG_DEBUG(this, "configure_beam configuring header");
  udp_header.configure(beam_config, *(format.get()));

  PSTLOG_DEBUG(this, "configure_beam configuring stats");
  stats.configure(format->get_packet_size(), format->get_packet_data_size());
  PSTLOG_DEBUG(this, "UDP dest={}:{} <- {}", data_host, data_port, local_host);

  PSTLOG_DEBUG(this, "configure_beam sock.resize({})", format->get_packet_size());
  sock.resize(format->get_packet_size());
  PSTLOG_DEBUG(this, "configure_beam sock.open({}, {})", data_host, data_port);
  sock.open(data_host, data_port, local_host);

  bytes_per_second = beam_config.compute_bytes_per_second();
}

void ska::pst::recv::UDPGenerator::configure_scan(const ska::pst::common::AsciiHeader &scan_config)
{
  PSTLOG_DEBUG(this, "UDPGenerator prepare format->configure_scan(scan_config)");
  format->configure_scan(scan_config);

  ska::pst::common::Time start_timestamp;
  // ensure the complete header includes a UTC_START
  try
  {
    std::string config_utc_start = scan_config.get_val("UTC_START");
    PSTLOG_INFO(this, "Using configured UTC_START={}", config_utc_start);
    start_timestamp.set_time(config_utc_start.c_str());
    try
    {
      auto picoseconds = scan_config.get<uint64_t>("PICOSECONDS");
      start_timestamp.set_fractional_time_attoseconds(picoseconds * static_cast<u_int64_t>(ska::pst::common::attoseconds_per_picosecond));
    }
    catch(const std::exception& e)
    {
      PSTLOG_DEBUG(this, "configure_scan did not find PICOSECONDS in scan_config, assuming 0");
    }
  }
  catch(const std::exception& e)
  {
    using ska::pst::common::operator""_mega;

    start_timestamp.set_time(time(nullptr));
    uint64_t microseconds = std::chrono::duration_cast<std::chrono::microseconds>(
                              std::chrono::high_resolution_clock::now().time_since_epoch())
                              .count() %
                          1_mega;

    start_timestamp.set_fractional_time_attoseconds(microseconds * ska::pst::common::attoseconds_per_microsecond);
    PSTLOG_INFO(this, "Generated UTC_START={}", start_timestamp.get_gmtime());
  }

  PSTLOG_DEBUG(this, "configure_scan using UTC_START={}, PICOSECONDS={}", start_timestamp.get_gmtime(), static_cast<uint64_t>(start_timestamp.get_fractional_time_attoseconds() / ska::pst::common::attoseconds_per_picosecond));
  PSTLOG_DEBUG(this, "timestamp numerator={} denominator={}", format->get_sample_period_numerator(), format->get_sample_period_denominator());

  auto samples_since_ska_epoch = start_timestamp.adjust_to_nearest_sample(format->get_sample_period_numerator(), format->get_sample_period_denominator());
  PSTLOG_DEBUG(this, "configure_scan adjusted UTC_START={}, PICOSECONDS={}", start_timestamp.get_gmtime(), static_cast<uint64_t>(start_timestamp.get_fractional_time_attoseconds() / ska::pst::common::attoseconds_per_picosecond));

  udp_header.set_samples_since_ska_epoch(samples_since_ska_epoch);
  udp_header.set_scan_id(scan_id);

  if (!scan_config.has("SOURCE"))
  {
    PSTLOG_ERROR(this, "configure_scan SOURCE did not exist in scan configuration");
    throw std::runtime_error("SOURCE did not exist in scan configuration");
  }

  // test for optional specification of packet data generator
  std::string dg_name = "none";
  try { dg_name = scan_config.get_val("DATA_GENERATOR"); }
  catch (std::exception& exc) { dg_name = "none"; }

  if (dg_name != "none" )
  {
    ska::pst::common::AsciiHeader scan_header;
    scan_header.clone(scan_config);
    scan_header.set_val("UTC_START", start_timestamp.get_gmtime());
    scan_header.set("OBS_OFFSET", 0);

    PSTLOG_DEBUG(this, "configure_scan format->get_nchan_per_packet()={}", format->get_nchan_per_packet());
    data_generator = ska::pst::common::PacketGeneratorFactory(dg_name, format);
    PSTLOG_DEBUG(this, "configure_scan configuring data generator");
    data_generator->configure(scan_header);
    PSTLOG_DEBUG(this, "configure_scan data generator configured");
  }

  PSTLOG_DEBUG(this, "configure_scan complete");
}

void ska::pst::recv::UDPGenerator::set_start_packet_sequence_number(uint64_t psn)
{
  PSTLOG_DEBUG(this, "set_start_packet_sequence_number psn={}", psn);
  udp_header.set_packet_sequence_number(psn);
}

void ska::pst::recv::UDPGenerator::transmit(float tobs, float data_rate, bool second_boundary)
{
  PSTLOG_DEBUG(this, "transmit tobs={} data_rate={} bytes_per_second={} second_boundary={}",
    tobs, data_rate, bytes_per_second, second_boundary);

  // if no data rate has been specified, use the bytes per second
  if (data_rate <= 0)
  {
    data_rate = static_cast<float>(bytes_per_second);
  }

  auto bytes_to_send = static_cast<uint64_t>(rintf(data_rate * tobs));
  uint64_t packets_to_send = bytes_to_send / format->get_packet_size();
  PSTLOG_DEBUG(this, "transmit bytes_to_send={} packets_to_send={}", bytes_to_send, packets_to_send);

  std::mt19937_64 rng;
  // initialize the random number generator with time-dependent seed
  uint64_t timeSeed = std::chrono::high_resolution_clock::now().time_since_epoch().count();
  std::seed_seq ss{static_cast<uint32_t>(timeSeed & 0xffffffff), static_cast<uint32_t>(timeSeed >> 32)}; // NOLINT
  rng.seed(ss);
  // initialize a uniform distribution between 0 and 1
  std::uniform_real_distribution<float> unif(0, 1);

  for (auto induced_error_rate : induced_error_rates)
  {
    auto induced_error_type = induced_error_rate.first;
    auto rate = induced_error_rate.second;

    for (auto packet_idx = 0; packet_idx < packets_to_send; packet_idx++)
    {
      // generated a uniform number between 0.0 and 1.0
      auto curr_chance = unif(rng);
      if (rate > curr_chance)
      {
        add_induced_error(packet_idx, induced_error_type);
      }
    }
  }

  for (auto validity_error_rate : validity_error_rates)
  {
    auto validity_error_type = validity_error_rate.first;
    auto rate = validity_error_rate.second;

    for (auto packet_idx = 0; packet_idx < packets_to_send; packet_idx++)
    {
      // generated a uniform number between 0.0 and 1.0
      auto curr_chance = unif(rng);
      if (rate > curr_chance)
      {
        add_validity_error(packet_idx, validity_error_type);
      }
    }
  }

  // sort the induced errors by the packet number
  sort(induced_errors.begin(), induced_errors.end());
  PSTLOG_DEBUG(this, "transmit inducing {} errors", induced_errors.size());

  sort(validity_errors.begin(), validity_errors.end());
  PSTLOG_DEBUG(this, "transmit inducing {} errors", validity_errors.size());

  auto packets_per_second = static_cast<uint64_t>(ceilf(data_rate / static_cast<float>(format->get_packet_size())));
  auto sleep_time_us = static_cast<double>(ska::pst::common::microseconds_per_second) / static_cast<double>(packets_per_second);

  PSTLOG_DEBUG(this, "transmit packets_per_second={} sleep_time_us={}", packets_per_second, sleep_time_us);

  char * buf = sock.get_buf();
  uint64_t total_bytes_sent{0};

  struct timeval timestamp{};

  gettimeofday(&timestamp, nullptr);
  time_t start_second{timestamp.tv_sec};

  if (second_boundary)
  {
    start_second++;
  }

  double micro_seconds = 0;
  double micro_seconds_elapsed = 0;

  // busy sleep until next 1pps tick
  while (second_boundary && timestamp.tv_sec < start_second)
  {
    gettimeofday(&timestamp, nullptr);
  }

  char wait = 1;

  const uint64_t payload_size = format->get_packet_data_size();

  keep_transmitting = true;

  // start a transmitting thread
  start_monitor_thread();

  uint32_t pkt_idx = 0;
  uint32_t err_idx = 0;
  uint32_t val_idx = 0;
  size_t pkt_size{};

  while (total_bytes_sent < bytes_to_send && keep_transmitting)
  {
    if (val_idx < validity_errors.size() && pkt_idx == validity_errors[val_idx].first)
    {
      for (auto validity_type : validity_errors[val_idx].second)
      {
        auto validity_type_name = ska::pst::recv::validity_type_names[validity_type];
        PSTLOG_DEBUG(this, "transmit setting setting validity error for pkt_idx={} with value {}", pkt_idx, validity_type_name);
        udp_header.set_validity_flag(validity_type);
      }

      val_idx++;
    }
    else
    {
      udp_header.set_validity_flag(ska::pst::recv::ValidityType::AllValid);
    }
    if (err_idx < induced_errors.size() && pkt_idx == induced_errors[err_idx].first)
    {
      pkt_size = udp_header.gen_packet_failure(buf, induced_errors[err_idx].second);
      err_idx++;
    }
    else
    {
      pkt_size = udp_header.gen_packet(buf);
    }
    pkt_idx++;

    if (data_generator)
    {
      try
      {
        data_generator->fill_packet(buf);
      }
      catch(const std::runtime_error& e)
      {
        PSTLOG_ERROR(this, "failed to fill packet with data: {}", e.what());
        pkt_idx--;
        keep_transmitting = false;
      }
    }

    if (keep_transmitting)
    {
      sock.send(pkt_size);
      stats.increment();

      // determine the desired time to wait un
      micro_seconds += sleep_time_us;

      wait = 1;
    }

    while (wait && keep_transmitting)
    {
      gettimeofday (&timestamp, nullptr);
      micro_seconds_elapsed = (static_cast<double>(timestamp.tv_sec - start_second) * ska::pst::common::microseconds_per_second) + static_cast<double>(timestamp.tv_usec);

      if (micro_seconds_elapsed > micro_seconds)
      {
        wait = 0;
      }
    }

    total_bytes_sent += payload_size;
  }

  PSTLOG_INFO(this, "sent {} packets and {} payload bytes", pkt_idx, total_bytes_sent);

  // stop the transmitting thread
  stop_monitor_thread();
}

void ska::pst::recv::UDPGenerator::start_monitor_thread()
{
  keep_transmitting = true;
  monitor_thread = std::make_unique<std::thread>(std::thread(&ska::pst::recv::UDPGenerator::monitor_method, this));
}

void ska::pst::recv::UDPGenerator::stop_monitor_thread()
{
  keep_transmitting = false;
  if (monitor_thread)
  {
    monitor_thread->join();
    monitor_thread = nullptr;
  }
}

auto ska::pst::recv::UDPGenerator::get_stats() -> ska::pst::recv::UDPStats&
{
  return stats;
}

void ska::pst::recv::UDPGenerator::monitor_method()
{
  uint64_t b_sent_total = 0;
  uint64_t b_sent_1sec = 0;
  uint64_t b_sent_curr = 0;
  uint64_t report_count = 0;

  double gb_sent_ps = 0;
  static constexpr unsigned header_report_period = 20; // seconds


  while (keep_transmitting)
  {
    // get a snapshot of the data as quickly as possible
    b_sent_curr = stats.get_data_transmitted();

    // calc the values for the last second
    b_sent_1sec = b_sent_curr - b_sent_total;

    // update the totals
    b_sent_total = b_sent_curr;

    gb_sent_ps = static_cast<double>(b_sent_1sec) * ska::pst::common::gigabits_per_byte;

    if (report_count % header_report_period == 0)
    {
      PSTLOG_INFO(this, "sent [Gb/s]");
    }
    report_count++;

    PSTLOG_INFO(this, "{}", gb_sent_ps);

    // sleep for 1 second, with a 0.1 second check of keep_transmitting
    int iterations = ska::pst::common::deciseconds_per_second;
    while (keep_transmitting && iterations > 0)
    {
      usleep(ska::pst::common::microseconds_per_decisecond);
      iterations--;
    }
  }
}
