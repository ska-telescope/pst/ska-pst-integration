/*
 * Copyright 2022 Square Kilometre Array Observatory
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 * contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */


#include "ska_pst/common/utils/AsciiHeader.h"
#include "ska_pst/common/utils/PacketGenerator.h"

#include "ska_pst/recv/formats/UDPFormat.h"
#include "ska_pst/recv/formats/UDPHeader.h"
#include "ska_pst/recv/network/UDPSocketSend.h"
#include "ska_pst/recv/network/UDPStats.h"

#include <cstdlib>
#include <vector>
#include <thread>

#ifndef SKA_PST_RECV_NETWORK_UDPGenerator_h
#define SKA_PST_RECV_NETWORK_UDPGenerator_h

namespace ska::pst::recv {

  /**
   * @brief Generates a stream of UDP packets according to the UDPFormat.
   * The UDP packet stream is generated with zero'd data, but the metadata from
   * the specified UDPFormat is used.
   *
   */
  class UDPGenerator {

    public:

      /**
       * @brief Construct a new UDPGenerator object
       *
       */
      UDPGenerator() = default;

      /**
       * @brief Construct a new UDPGenerator object
       *
       * @param host IPv4 address for destination packets
       * @param port UDP port number for destination packets
       */
      UDPGenerator(std::string host, int port);

      /**
       * @brief Destroy the UDPGenerator object
       *
       */
      ~UDPGenerator();

      /**
       * @brief Induce an error into the generated UDP packet stream
       *
       * @param packet_number packet at which to induce the error
       * @param failure_type type of failure to induce
       */
      void add_induced_error(uint32_t packet_number, ska::pst::recv::FailureType failure_type);

      /**
       * @brief Induce a packet validity error at random with an average rate of packet loss
       *
       * @param failure_type type of failure to induce
       * @param rate the fractional number of packets that should have validity flag set.
       */
      void add_induced_error_rate(ska::pst::recv::FailureType failure_type, double rate);

      /**
       * @brief Induce a CBF validity flag into the generated UDP packet stream
       *
       * @param packet_number packet at which to induce the error
       * @param validity_type type of validity failure to induce
       */
      void add_validity_error(uint32_t packet_number, ska::pst::recv::ValidityType validity_type);

      /**
       * @brief Induce a CBF validity flag at random with an average rate of packet loss
       *
       * @param validity_type type of validity flag to induce
       * @param rate the fractional number of packets that should have validity flag set.
       */
      void add_validity_error_rate(ska::pst::recv::ValidityType validity_type, double rate);

      /**
       * @brief Process fixed configuration and perform resource allocation.
       * Configures the generator using the fixed configuration parameters that are
       * common to all observations and available at launch. Configures the UDPFormat
       * and UDPStats instances. Opens the UDP socket to transmit UDP packets.
       *
       * @param beam_config fixed configuration parameters
       */
      void configure_beam(const ska::pst::common::AsciiHeader &beam_config);

      /**
       * @brief Processes the runtime configuration parameters in the header.
       * Prepares the format with the runtime configuration parameters.
       *
       * @param scan_config dynamic configuration parameters
       */
      void configure_scan(const ska::pst::common::AsciiHeader &scan_config);

      /**
       * @brief Transmit the UDP data stream, returning once al data have been sent.
       * Will terminate early if keep_transmitting is set to false.
       *
       * @param tobs duration of the data transmission in seconds.
       * @param data_rate data rate for the data transmission in bytes per second
       * @param second_boundary wait for the next second boundary before starting transmitting, default is true
       */
      void transmit(float tobs, float data_rate, bool second_boundary = true);

      /**
       * @brief Stop transmitting data immediately
       *
       */
      void stop_transmit() { keep_transmitting = false; };

      /**
       * @brief Start the monitoring thread that reports data transmission statistics
       *
       */
      void start_monitor_thread();

      /**
       * @brief Stop the monitoring thread that reports data transmission statistics
       *
       */
      void stop_monitor_thread();

      /**
       * @brief Monitor the data transmission performance
       *
       */
      void monitor_method();

      /**
       * @brief Set the starting packet sequence number
       *
       * @param psn packet sequence number of the first packet generated
       */
      void set_start_packet_sequence_number(uint64_t psn);

      //! set the expected beam id for the packet stream
      void set_beam_id(uint64_t beam_id);

      //! set the expected scan id for the packet stream
      void set_scan_id(uint64_t scan_id);

      /**
       * @brief Get the bytes per second computed from the scan configuration
       *
       * @return double-precision bytes per second in the output data stream
       */
      double get_bytes_per_second() const { return bytes_per_second; }

      /**
       * @brief Return a reference to the UDP statistics management object.
       *
       * @return UDPStats& UDP statistics management object
       */
      UDPStats& get_stats();

      //! return mapped diagnostic context key/value pairs
      const std::map<std::string, std::string>& get_log_context() const
      {
        return log_context;
      }

    protected:

      //! Expected beam id for the packet stream
      uint64_t beam_id{0};

      //! Expected scan id for the packet stream
      uint64_t scan_id{0};

      //! UDP format provides the mapping from UDP metadata to memory addresses
      std::shared_ptr<UDPFormat> format{nullptr};

      //! UDP Header provides the encoding of UDP packets
      UDPHeader udp_header;

      //! Optional PacketGenerator sets the data and weights of each UDP packet
      std::shared_ptr<common::PacketGenerator> data_generator{nullptr};

      //! UDP sending socket
      UDPSocketSend sock;

      //! statistics of UDP transmission performance
      UDPStats stats;

      //! destination IPv4 address for the data stream
      std::string data_host;

      //! destination UDP port for the data stream
      int data_port{0};

      //! !local IPv4 address for the interface which will transmit the data stream
      std::string local_host;

      //! number of bytes per second in the data stream
      double bytes_per_second{0};

    private:

      //! errors to be induced in the data transmission for testing
      std::vector<std::pair<uint32_t, ska::pst::recv::FailureType>> induced_errors{};

      //! errors to be induced to be induced at a rate
      std::vector<std::pair<ska::pst::recv::FailureType, double>> induced_error_rates{};

      //! packet validity issues to be induced in the data transmission for testing
      std::vector<std::pair<uint32_t, std::vector<ska::pst::recv::ValidityType>>> validity_errors{};

      //! packet validity issues to be induced at a rate
      std::vector<std::pair<ska::pst::recv::ValidityType, double>> validity_error_rates{};

      //! flag to signal monitoring thread to persist
      bool keep_transmitting{true};

      //! delay to apply to the start of data transmission
      unsigned start_delay{2};

      /**
       * @brief thread for the monitoring method
       *
       */
      std::unique_ptr<std::thread> monitor_thread{nullptr};

      //! mapped diagnostic context key/value pairs
      std::map<std::string, std::string> log_context = {{"entity","UDPGenerator"}};
  };

} // namespace ska::pst::recv

#endif // SKA_PST_RECV_NETWORK_UDPGenerator_h
