/*
 * Copyright 2022 Square Kilometre Array Observatory
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 * contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#include <thread>
#include <chrono>

#include "ska_pst/common/testutils/GtestMain.h"
#include "ska_pst/common/utils/Logging.h"
#include "ska_pst/common/utils/PacketGeneratorFactory.h"

#include "ska_pst/recv/clients/tests/UDPReceiveDBTest.h"
#include "ska_pst/recv/clients/UDPGenerator.h"
#include "ska_pst/recv/formats/UDPFormatFactory.h"
#include "ska_pst/smrb/DataBlockRead.h"

using ska::pst::common::test::test_data_file;

auto main(int argc, char* argv[]) -> int
{
  return ska::pst::common::test::gtest_main(argc, argv);
}

namespace ska::pst::recv::test {

static constexpr float tobs = 0.1; // 100 ms
static constexpr float default_data_rate = 1250000.0; // 0.01 Gbps or 0.25 MiB/s
static constexpr float higher_data_rate  = 12500000.0; // 0.1 Gbps or 2.5 MiB/s

UDPReceiveDBTest::UDPReceiveDBTest()
    : ::testing::Test(),
    db_key("a000"),
    wb_key("a002"),
    db(db_key),
    wb(wb_key),
    _scan_id(static_cast<uint64_t>(time(nullptr)))
{
}

void UDPReceiveDBTest::set_config()
{
  beam_config.load_from_file(test_data_file("receiver_beam_config.txt"));
  scan_config.load_from_file(test_data_file("receiver_scan_config.txt"));
  startscan_config.load_from_file(test_data_file("receiver_startscan_config.txt"));
  format = UDPFormatFactory("LowTestVector");
  format->configure_beam(beam_config);
}

void UDPReceiveDBTest::SetUp()
{
  set_config();

  data_resolution = format->get_resolution();
  weights_resolution = format->get_weights_resolution();
  PSTLOG_DEBUG(this, "UDPReceiveDBTest SetUp data_resolution={} weights_resolution={}", data_resolution, weights_resolution);

  db.create(header_nbufs, header_bufsz, data_nbufs, data_resolution, 1, -1);
  wb.create(header_nbufs, header_bufsz, weights_nbufs, weights_resolution, 1, -1);

  sock = std::make_shared<UDPSocketReceive>();
  std::string data_host = beam_config.get_val("DATA_HOST");
  PSTLOG_TRACE(this, "UDPReceiveDBTest SetUp DATA_HOST={}", data_host);
  recv = std::make_shared<TestUDPReceiveDB>(sock, data_host);

  PSTLOG_TRACE(this, "UDPReceiveDBTest SetUp complete");
}

void UDPReceiveDBTest::TearDown()
{
  recv->quit();
  recv = nullptr;
  db.destroy();
  wb.destroy();
}

auto UDPReceiveDBTest::generate_scan_id() -> uint64_t
{
  _scan_id++;
  return _scan_id;
}

void UDPReceiveDBTest::reader(const std::string &key, uint64_t bufsz, bool validate_data, bool* valid_data_sequence, ska::pst::common::AsciiHeader *config, ska::pst::common::AsciiHeader *header)
{
  ska::pst::smrb::DataBlockRead dbr(key);
  dbr.connect(0);
  dbr.lock();
  dbr.read_config();
  config->load_from_str(dbr.get_config());

  for (unsigned i=0; i < number_of_scans; i++)
  {
    dbr.read_header();
    dbr.open();

    header->reset();
    header->load_from_str(dbr.get_header());

    // the format (which extends from ska::pst::common::PacketLayout)
    auto udp_format_str = header->get_val("UDP_FORMAT");
    PSTLOG_DEBUG(this, "UDPReceiveDBTest::reader input data stream has UDP_FORMAT={}", udp_format_str);
    auto format = ska::pst::recv::UDPFormatFactory(udp_format_str);

    // determine if the data stream contains a generated packet sequence
    std::shared_ptr<ska::pst::common::PacketGenerator> generator{nullptr};
    try {
      if (validate_data)
      {
        auto generator_name = header->get_val("DATA_GENERATOR");
        PSTLOG_DEBUG(this, "UDPReceiveDBTest::reader input data stream has DATA_GENERATOR={}", generator_name);
        generator = ska::pst::common::PacketGeneratorFactory(generator_name, format);
        PSTLOG_DEBUG(this, "UDPReceiveDBTest::reader configuring generator with header");
        generator->configure(*header);
        PSTLOG_DEBUG(this, "UDPReceiveDBTest::reader configuring generator configured");
      }
    } catch (std::runtime_error& exc) {
      PSTLOG_DEBUG(this, "No DATA_GENERATOR present in the data stream");
    }

    auto bytes = static_cast<size_t>(bufsz);
    std::vector<char> data(bytes);
    bool keep_reading = true;
    *valid_data_sequence = true;
    while (keep_reading)
    {
      size_t bytes_read = dbr.read_data(&data[0], bytes);
      keep_reading = ((bytes_read == bytes) && (!dbr.check_eod()));

      // validate the received data stream, if data generator present
      if (keep_reading && validate_data && generator)
      {
        *valid_data_sequence &= generator->test_data(&data[0], bytes);
      }
    }

    dbr.close();
    PSTLOG_DEBUG(this, "UDPReceiveDBTest::reader valid_data_sequence={}", *valid_data_sequence);
  }

  dbr.unlock();
  dbr.disconnect();
}

void TestUDPReceiveDB::compute_stat_rates()
{
  stats.compute_rates();
}

TEST_F(UDPReceiveDBTest, test_happy_path) // NOLINT
{
  PSTLOG_TRACE(this, "UDPReceiveDBTest test_happy_path"); // NOLINT

  auto empty_header = ska::pst::common::AsciiHeader();
  auto expected_beam_config = ska::pst::common::AsciiHeader(beam_config);
  expected_beam_config.set("RESOLUTION", data_resolution);

  auto expected_scan_config = ska::pst::common::AsciiHeader(expected_beam_config);
  expected_scan_config.append_header(scan_config);
  expected_scan_config.set_val("HDR_VERSION", "1.0");
  expected_scan_config.set("HDR_SIZE", 4096); // NOLINT
  expected_scan_config.set_val("TELESCOPE", "Unknown");
  expected_scan_config.set_val("UTC_START", "TBD");

  // build complete config
  ASSERT_EQ(ska::pst::common::State::Idle, recv->get_state()); // NOLINT
  ASSERT_FALSE(recv->is_beam_configured());
  ASSERT_FALSE(recv->is_scan_configured());
  ASSERT_FALSE(recv->is_scanning());
  ASSERT_EQ(empty_header, recv->get_beam_configuration());
  ASSERT_EQ(empty_header, recv->get_scan_configuration());
  ASSERT_EQ(empty_header, recv->get_startscan_configuration());

  // Configure Beam
  recv->configure_beam(beam_config);
  ASSERT_EQ(ska::pst::common::State::BeamConfigured, recv->get_state()); // NOLINT
  ASSERT_TRUE(recv->is_beam_configured());
  ASSERT_FALSE(recv->is_scan_configured());
  ASSERT_FALSE(recv->is_scanning());
  ASSERT_EQ(expected_beam_config, recv->get_beam_configuration());
  ASSERT_EQ(empty_header, recv->get_scan_configuration());
  ASSERT_EQ(empty_header, recv->get_startscan_configuration());

  recv->configure_scan(scan_config);
  ASSERT_EQ(ska::pst::common::State::ScanConfigured, recv->get_state()); // NOLINT
  ASSERT_TRUE(recv->is_beam_configured());
  ASSERT_TRUE(recv->is_scan_configured());
  ASSERT_FALSE(recv->is_scanning());
  ASSERT_EQ(expected_beam_config, recv->get_beam_configuration());
  ASSERT_EQ(expected_scan_config, recv->get_scan_configuration());
  ASSERT_EQ(empty_header, recv->get_startscan_configuration());

  recv->start_scan(startscan_config);
  ASSERT_EQ(ska::pst::common::State::Scanning, recv->get_state()); // NOLINT
  ASSERT_TRUE(recv->is_beam_configured());
  ASSERT_TRUE(recv->is_scan_configured());
  ASSERT_TRUE(recv->is_scanning());
  ASSERT_EQ(expected_beam_config, recv->get_beam_configuration());
  ASSERT_EQ(expected_scan_config, recv->get_scan_configuration());
  ASSERT_EQ(startscan_config, recv->get_startscan_configuration());

  recv->stop_scan();
  ASSERT_EQ(ska::pst::common::State::ScanConfigured, recv->get_state()); // NOLINT
  ASSERT_TRUE(recv->is_beam_configured());
  ASSERT_TRUE(recv->is_scan_configured());
  ASSERT_FALSE(recv->is_scanning());
  ASSERT_EQ(expected_beam_config, recv->get_beam_configuration());
  ASSERT_EQ(expected_scan_config, recv->get_scan_configuration());
  ASSERT_EQ(empty_header, recv->get_startscan_configuration());

  recv->deconfigure_scan();
  ASSERT_EQ(ska::pst::common::State::BeamConfigured, recv->get_state()); // NOLINT
  ASSERT_TRUE(recv->is_beam_configured());
  ASSERT_FALSE(recv->is_scan_configured());
  ASSERT_FALSE(recv->is_scanning());
  ASSERT_EQ(expected_beam_config, recv->get_beam_configuration());
  ASSERT_EQ(empty_header, recv->get_scan_configuration());
  ASSERT_EQ(empty_header, recv->get_startscan_configuration());

  recv->deconfigure_beam();
  ASSERT_EQ(ska::pst::common::State::Idle, recv->get_state()); // NOLINT
  ASSERT_FALSE(recv->is_beam_configured());
  ASSERT_FALSE(recv->is_scan_configured());
  ASSERT_FALSE(recv->is_scanning());
  ASSERT_EQ(empty_header, recv->get_beam_configuration());
  ASSERT_EQ(empty_header, recv->get_scan_configuration());
  ASSERT_EQ(empty_header, recv->get_startscan_configuration());

  recv->quit();
  ASSERT_EQ(ska::pst::common::State::Unknown, recv->get_state()); // NOLINT
  ASSERT_FALSE(recv->is_beam_configured());
  ASSERT_FALSE(recv->is_scan_configured());
  ASSERT_FALSE(recv->is_scanning());
}

TEST_F(UDPReceiveDBTest, test_receive_packet_timeout) // NOLINT
{
  uint64_t scan_id = generate_scan_id();

  static constexpr uint64_t timeout = 1;
  beam_config.set("INITIAL_PACKET_TIMEOUT_THRESHOLD", timeout);
  beam_config.set("PACKET_TIMEOUT_THRESHOLD", 1);

  startscan_config.set("SCAN_ID", scan_id);

  // RECV configuration
  ASSERT_EQ(ska::pst::common::State::Idle, recv->get_state()); // NOLINT
  // Configure Beam
  recv->configure_beam(beam_config);
  ASSERT_EQ(ska::pst::common::State::BeamConfigured, recv->get_state()); // NOLINT
  recv->configure_scan(scan_config);
  ASSERT_EQ(ska::pst::common::State::ScanConfigured, recv->get_state()); // NOLINT

  recv->start_scan(startscan_config);
  ASSERT_EQ(ska::pst::common::State::Scanning, recv->get_state()); // NOLINT
  sleep(timeout+1);
  ASSERT_EQ(1, recv->get_stats().get_packet_receive_timeouts()); // NOLINT
}

TEST_F(UDPReceiveDBTest, test_data_acquisition) // NOLINT
{
  // setup a UDP receiver
  uint64_t scan_id = generate_scan_id();
  // Required by UDP generator
  beam_config.set("SCAN_ID", scan_id);
  startscan_config.set("SCAN_ID", scan_id);

  static constexpr uint32_t factor = 32;
  uint64_t data_bufsz = data_resolution * factor;
  uint64_t weights_bufsz = weights_resolution * factor;

  bool data_seq_valid{false};
  bool weights_seq_valid{false};

  ska::pst::common::AsciiHeader data_config, weights_config;
  ska::pst::common::AsciiHeader data_header, weights_header;

  std::thread data_thread = std::thread(&UDPReceiveDBTest::reader, this, db_key, data_bufsz, true, &data_seq_valid, &data_config, &data_header);
  std::thread weights_thread = std::thread(&UDPReceiveDBTest::reader, this, wb_key, weights_bufsz, false, &weights_seq_valid, &weights_config, &weights_header);

  recv->configure_beam(beam_config);
  ASSERT_EQ(ska::pst::common::State::BeamConfigured, recv->get_state()); // NOLINT
  recv->configure_scan(scan_config);
  ASSERT_EQ(ska::pst::common::State::ScanConfigured, recv->get_state()); // NOLINT
  recv->start_scan(startscan_config);
  ASSERT_EQ(ska::pst::common::State::Scanning, recv->get_state()); // NOLINT

  UDPGenerator udpgen;
  udpgen.configure_beam(beam_config);
  udpgen.configure_scan(scan_config);
  udpgen.transmit(tobs, higher_data_rate, false);

  recv->compute_stat_rates();
  ASSERT_TRUE(recv->get_stats().get_data_transmitted() > 0);
  ASSERT_TRUE(recv->get_stats().get_data_transmission_rate() > 0);

  recv->stop_scan();

  recv->deconfigure_scan();
  recv->deconfigure_beam();
  recv->quit();

  PSTLOG_DEBUG(this, "UDPReceiveDBTest test_data_acquisition data_thread.join()");
  data_thread.join();
  PSTLOG_DEBUG(this, "UDPReceiveDBTest test_data_acquisition weights_thread.join()");
  weights_thread.join();

  ASSERT_TRUE(data_seq_valid);
  ASSERT_TRUE(weights_seq_valid);
}

TEST_F(UDPReceiveDBTest, test_multiple_scans_one_config) // NOLINT
{
  recv->configure_beam(beam_config);
  ASSERT_EQ(ska::pst::common::State::BeamConfigured, recv->get_state()); // NOLINT
  recv->configure_scan(scan_config);
  ASSERT_EQ(ska::pst::common::State::ScanConfigured, recv->get_state()); // NOLINT

  static constexpr uint32_t factor = 32;
  uint64_t data_bufsz = data_resolution * factor;
  uint64_t weights_bufsz = weights_resolution * factor;

  number_of_scans = 2;

  bool data_seq_valid{false};
  bool weights_seq_valid{false};

  ska::pst::common::AsciiHeader data_config, weights_config;
  ska::pst::common::AsciiHeader data_header, weights_header;

  std::thread data_thread = std::thread(&UDPReceiveDBTest::reader, this, db_key, data_bufsz, true, &data_seq_valid, &data_config, &data_header);
  std::thread weights_thread = std::thread(&UDPReceiveDBTest::reader, this, wb_key, weights_bufsz, false, &weights_seq_valid, &weights_config, &weights_header);

  for (unsigned i = 0; i < number_of_scans; i++)
  {
    uint64_t scan_id = generate_scan_id();
    // Required by UDP generator
    beam_config.set("SCAN_ID", scan_id);
    startscan_config.set("SCAN_ID", scan_id);

    usleep(ska::pst::common::microseconds_per_decisecond);
    recv->start_scan(startscan_config);
    ASSERT_EQ(ska::pst::common::State::Scanning, recv->get_state()); // NOLINT

    UDPGenerator udpgen;
    udpgen.configure_beam(beam_config);
    udpgen.configure_scan(scan_config);
    udpgen.transmit(tobs, higher_data_rate, false);

    recv->compute_stat_rates();
    ASSERT_TRUE(recv->get_stats().get_data_transmitted() > 0);
    ASSERT_TRUE(recv->get_stats().get_data_transmission_rate() > 0);

    recv->stop_scan();
  }

  recv->deconfigure_scan();
  recv->deconfigure_beam();
  recv->quit();

  PSTLOG_DEBUG(this, "UDPReceiveDBTest test_multiple_scans_one_config data_thread.join()");
  data_thread.join();
  PSTLOG_DEBUG(this, "UDPReceiveDBTest test_multiple_scans_one_config weights_thread.join()");
  weights_thread.join();

  ASSERT_TRUE(data_seq_valid);
  ASSERT_TRUE(weights_seq_valid);
}

TEST_F(UDPReceiveDBTest, test_persistent_monitor_thread) // NOLINT
{
  uint64_t scan_id = generate_scan_id();


  startscan_config.set("SCAN_ID", scan_id);

  // RECV configuration
  ASSERT_EQ(ska::pst::common::State::Idle, recv->get_state()); // NOLINT
  // Configure Beam
  recv->configure_beam(beam_config);
  ASSERT_EQ(ska::pst::common::State::BeamConfigured, recv->get_state()); // NOLINT
  recv->configure_scan(scan_config);
  ASSERT_EQ(ska::pst::common::State::ScanConfigured, recv->get_state()); // NOLINT

  recv->start_scan(startscan_config);
  ASSERT_EQ(ska::pst::common::State::Scanning, recv->get_state()); // NOLINT
  ASSERT_TRUE(recv->check_running_monitor_thread());
  recv->stop_scan();
  ASSERT_EQ(ska::pst::common::State::ScanConfigured, recv->get_state()); // NOLINT
}

TEST_F(UDPReceiveDBTest, test_receive_invalid_packets) // NOLINT
{
  // setup a UDP receiver
  recv->configure_beam(beam_config);
  recv->configure_scan(scan_config);
  recv->start_scan(startscan_config);

  // start at a high packet sequence number to support the MisorderedPacketSequenceNumber test
  static constexpr uint64_t start_psn = 1000;
  UDPGenerator udpgen;
  udpgen.configure_beam(beam_config);
  udpgen.configure_scan(scan_config);
  udpgen.set_start_packet_sequence_number(start_psn);

  uint64_t expected_malformed{0}, expected_dropped{0}, expected_misdirected{0};

  // beam configuration dictates 18 packets per frame
  udpgen.add_induced_error(1, ska::pst::recv::FailureType::BadMagicWord); // NOLINT
  expected_malformed++;
  expected_dropped++;

  udpgen.add_induced_error(19, ska::pst::recv::FailureType::BadPacketSize); // NOLINT
  expected_malformed++;
  expected_dropped++;

  udpgen.add_induced_error(37, ska::pst::recv::FailureType::BadTransmit); // NOLINT
  expected_dropped++;

  udpgen.add_induced_error(55, ska::pst::recv::FailureType::MisorderedPacketSequenceNumber); // NOLINT
  expected_dropped++;

  // these errors are not detectable yet, just check that they can be generated
  udpgen.add_induced_error(73, ska::pst::recv::FailureType::BadScanID); // NOLINT
  expected_misdirected++;
  expected_dropped++;

  udpgen.add_induced_error(91, ska::pst::recv::FailureType::BadChannelNumber); // NOLINT
  expected_misdirected++;
  expected_dropped++;

  udpgen.add_induced_error(109, ska::pst::recv::FailureType::BadTimestamp); // NOLINT
  udpgen.add_induced_error(127, ska::pst::recv::FailureType::BadDataRate);  // NOLINT

  // increase the default duration due to the slowness of AWS
  udpgen.transmit(tobs, higher_data_rate, false);

  recv->compute_stat_rates();
  ASSERT_EQ(recv->get_stats().get_malformed(), expected_malformed);
  ASSERT_TRUE(recv->get_stats().get_malformed() > 0);
  ASSERT_TRUE(recv->get_stats().get_misdirected() > 0);

  recv->stop_scan();
  recv->deconfigure_scan();
  recv->deconfigure_beam();
}

TEST_F(UDPReceiveDBTest, test_receive_validity_flags) // NOLINT
{
  // setup a UDP receiver
  uint64_t scan_id = generate_scan_id();
  // Required by UDP generator
  beam_config.set("SCAN_ID", scan_id);
  startscan_config.set("SCAN_ID", scan_id);

  static constexpr uint32_t factor = 32;
  uint64_t data_bufsz = data_resolution * factor;
  uint64_t weights_bufsz = weights_resolution * factor;

  bool data_seq_valid{false};
  bool weights_seq_valid{false};

  ska::pst::common::AsciiHeader data_config, weights_config;
  ska::pst::common::AsciiHeader data_header, weights_header;

  std::thread data_thread = std::thread(&UDPReceiveDBTest::reader, this, db_key, data_bufsz, true, &data_seq_valid, &data_config, &data_header);
  std::thread weights_thread = std::thread(&UDPReceiveDBTest::reader, this, wb_key, weights_bufsz, false, &weights_seq_valid, &weights_config, &weights_header);

  format->set_validity_flags_policy(ska::pst::recv::PacketValidityFlagsPolicy::All);
  recv->configure_beam(beam_config);
  recv->configure_scan(scan_config);
  recv->start_scan(startscan_config);


  UDPGenerator udpgen;
  udpgen.configure_beam(beam_config);
  udpgen.configure_scan(scan_config);

  // Use a validity error rate rather than specific values to allow for issues with network
  udpgen.add_validity_error_rate(ska::pst::recv::ValidityType::JonesMatricesFlagInvalid, 1.0); // NOLINT
  udpgen.add_validity_error_rate(ska::pst::recv::ValidityType::StationBeamFlagInvalid, 1.0);   // NOLINT
  udpgen.add_validity_error_rate(ska::pst::recv::ValidityType::PstBeamFlagInvalid, 1.0);       // NOLINT

  udpgen.transmit(tobs, default_data_rate, false);

  recv->compute_stat_rates();
  ASSERT_GT(recv->get_stats().get_no_valid_polarisation_correction(), 0);
  ASSERT_GT(recv->get_stats().get_no_valid_station_beam(), 0);
  ASSERT_GT(recv->get_stats().get_no_valid_pst_beam(), 0);

  ASSERT_GE(recv->get_stats().get_no_valid_polarisation_correction_rate(), 0.0);
  ASSERT_GE(recv->get_stats().get_no_valid_station_beam_rate(), 0.0);
  ASSERT_GE(recv->get_stats().get_no_valid_pst_beam_rate(), 0.0);

  recv->stop_scan();
  recv->deconfigure_scan();
  recv->deconfigure_beam();
  recv->quit();

  data_thread.join();
  weights_thread.join();
}

TEST_F(UDPReceiveDBTest, test_validate_beam_configuration_expects_beam_id) // NOLINT
{
  beam_config.del("BEAM_ID");
  ASSERT_THROW(recv->configure_beam(beam_config), ska::pst::common::pst_validation_error); // NOLINT
}

TEST_F(UDPReceiveDBTest, test_square_wave_generator) // NOLINT
{
  // disabling due to poor network performance on AWS
  GTEST_SKIP();

  // setup a UDP receiver
  uint64_t scan_id = generate_scan_id();
  // Required by UDP generator
  beam_config.set("SCAN_ID", scan_id);
  startscan_config.set("SCAN_ID", scan_id);

  // configuration required for UDP Generator to use the square wave generator
  scan_config.set("DATA_GENERATOR", "SquareWave");
  scan_config.set("CAL_OFF_INTENSITY", "10");
  scan_config.set("CAL_ON_POL_0_INTENSITY", "11");
  scan_config.set("CAL_ON_POL_1_CHAN_0_INTENSITY", "9");
  scan_config.set("CAL_ON_POL_1_CHAN_N_INTENSITY", "12");

  ska::pst::common::AsciiHeader complete_config;
  complete_config.append_header(beam_config);
  complete_config.append_header(scan_config);
  complete_config.append_header(startscan_config);

  recv->configure_beam(beam_config);
  ASSERT_EQ(ska::pst::common::State::BeamConfigured, recv->get_state()); // NOLINT
  recv->configure_scan(scan_config);
  ASSERT_EQ(ska::pst::common::State::ScanConfigured, recv->get_state()); // NOLINT

  usleep(ska::pst::common::microseconds_per_decisecond);
  recv->start_scan(startscan_config);
  ASSERT_EQ(ska::pst::common::State::Scanning, recv->get_state()); // NOLINT

  static constexpr uint32_t factor = 32;
  uint64_t data_bufsz = data_resolution * factor;
  uint64_t weights_bufsz = weights_resolution * factor;

  bool data_seq_valid{false};
  bool weights_seq_valid{false};

  ska::pst::common::AsciiHeader data_config, weights_config;
  ska::pst::common::AsciiHeader data_header, weights_header;

  std::thread data_thread = std::thread(&UDPReceiveDBTest::reader, this, db_key, data_bufsz, true, &data_seq_valid, &data_config, &data_header);
  std::thread weights_thread = std::thread(&UDPReceiveDBTest::reader, this, wb_key, weights_bufsz, false, &weights_seq_valid, &weights_config, &weights_header);

  usleep(ska::pst::common::microseconds_per_decisecond);

  PSTLOG_DEBUG(this, "UDPReceiveDBTest::test_square_wave_generator constructing UDPGenerator");
  UDPGenerator udpgen;

  ASSERT_NO_THROW(udpgen.configure_beam(complete_config));
  ASSERT_NO_THROW(udpgen.configure_scan(complete_config));

  udpgen.transmit(tobs, default_data_rate, false);

  recv->compute_stat_rates();
  ASSERT_TRUE(recv->get_stats().get_data_transmitted() > 0);
  ASSERT_TRUE(recv->get_stats().get_data_transmission_rate() > 0);

  recv->stop_scan();

  recv->deconfigure_scan();
  recv->deconfigure_beam();
  recv->quit();

  PSTLOG_DEBUG(this, "UDPReceiveDBTest::test_square_wave_generator data_thread.join()");
  data_thread.join();
  PSTLOG_DEBUG(this, "UDPReceiveDBTest::test_square_wave_generator weights_thread.join()");
  weights_thread.join();

  ASSERT_TRUE(data_seq_valid);
  ASSERT_TRUE(weights_seq_valid);
}

TEST_F(UDPReceiveDBTest, test_reader_configs_and_headers) // NOLINT
{
  // setup a UDP receiver
  uint64_t scan_id = generate_scan_id();
  // Required by UDP generator
  beam_config.set("SCAN_ID", scan_id);
  startscan_config.set("SCAN_ID", scan_id);

  recv->configure_beam(beam_config);
  ASSERT_EQ(ska::pst::common::State::BeamConfigured, recv->get_state()); // NOLINT
  recv->configure_scan(scan_config);
  ASSERT_EQ(ska::pst::common::State::ScanConfigured, recv->get_state()); // NOLINT

  usleep(ska::pst::common::microseconds_per_decisecond);
  recv->start_scan(startscan_config);
  ASSERT_EQ(ska::pst::common::State::Scanning, recv->get_state()); // NOLINT

  static constexpr uint32_t factor = 32;
  uint64_t data_bufsz = data_resolution * factor;
  uint64_t weights_bufsz = weights_resolution * factor;

  bool data_seq_valid{false};
  bool weights_seq_valid{false};

  ska::pst::common::AsciiHeader data_config, weights_config;
  ska::pst::common::AsciiHeader data_header, weights_header;

  std::thread data_thread = std::thread(&UDPReceiveDBTest::reader, this, db_key, data_bufsz, true, &data_seq_valid, &data_config, &data_header);
  std::thread weights_thread = std::thread(&UDPReceiveDBTest::reader, this, wb_key, weights_bufsz, false, &weights_seq_valid, &weights_config, &weights_header);

  usleep(ska::pst::common::microseconds_per_decisecond);

  UDPGenerator udpgen;
  udpgen.configure_beam(beam_config);
  udpgen.configure_scan(scan_config);
  udpgen.transmit(tobs, default_data_rate, false);

  recv->compute_stat_rates();
  ASSERT_TRUE(recv->get_stats().get_data_transmitted() > 0);
  ASSERT_TRUE(recv->get_stats().get_data_transmission_rate() > 0);

  recv->stop_scan();

  recv->deconfigure_scan();
  recv->deconfigure_beam();
  recv->quit();

  PSTLOG_DEBUG(this, "UDPReceiveDBTest test_reader_configs_and_headers data_thread.join()");
  data_thread.join();
  PSTLOG_DEBUG(this, "UDPReceiveDBTest test_reader_configs_and_headers weights_thread.join()");
  weights_thread.join();

  // check expected keys all exist in the config and headers from the data and weights readers.
  std::vector<std::string> expected_config_keys = {
    "BEAM_ID",
    "BW",
    "DATA_HOST",
    "DATA_KEY",
    "DATA_PORT",
    "EB_ID",
    "END_CHANNEL",
    "FREQ",
    "HDR_SIZE",
    "HDR_VERSION",
    "NBIT",
    "NCHAN",
    "NDIM",
    "NPOL",
    "OS_FACTOR",
    "RESOLUTION",
    "SCAN_ID",
    "SOURCE",
    "START_CHANNEL",
    "TELESCOPE",
    "TSAMP",
    "UDP_FORMAT",
    "WEIGHTS_KEY",
  };

  for (auto &key : expected_config_keys)
  {
    ASSERT_TRUE(data_config.has(key));
    ASSERT_TRUE(data_header.has(key));
    ASSERT_TRUE(weights_config.has(key));
    ASSERT_TRUE(weights_header.has(key));
  }

  std::vector<std::string> expected_header_keys = {
    "PICOSECONDS",
    "UTC_START",
  };

  for (auto &key : expected_header_keys)
  {
    ASSERT_TRUE(data_header.has(key));
    ASSERT_TRUE(weights_header.has(key));
  }

  std::vector<std::string> expected_weights_config_keys = {
    "BLOCK_DATA_BYTES",
    "BLOCK_HEADER_BYTES",
    "NSAMP_PER_WEIGHT",
    "ORDER",
    "PACKET_SCALES_SIZE",
    "PACKET_WEIGHTS_SIZE",
  };

  for (auto &key : expected_weights_config_keys)
  {
    ASSERT_TRUE(weights_config.has(key));
    ASSERT_TRUE(weights_header.has(key));
  }
}

#ifdef SUPPORT_UTC_STOP
TEST_F(UDPReceiveDBTest, test_data_acquisition_with_utc_stop) // NOLINT
{
  recv->configure_beam(beam_config, 0);

  scan_config.set_val("UTC_START", "2020-01-01-00:00:00");
  scan_config.set_val("UTC_STOP", "2020-01-01-00:00:01");

  recv->configure_scan(scan_config);
  usleep(ska::pst::common::microseconds_per_decisecond);
  uint64_t scan_id = generate_scan_id();
  recv->start_scan(scan_id);

  uint64_t data_bufsz = data_resolution * 32;
  uint64_t weights_bufsz = weights_resolution * 32;

  bool data_seq_valid{false};
  bool weights_seq_valid{false};

  ska::pst::common::AsciiHeader data_config, weights_config;
  ska::pst::common::AsciiHeader data_header, weights_header;

  std::thread data_thread = std::thread(&UDPReceiveDBTest::reader, this, db_key, data_bufsz, true, &data_seq_valid, &data_config, &data_header);
  std::thread weights_thread = std::thread(&UDPReceiveDBTest::reader, this, wb_key, weights_bufsz, false, &weights_seq_valid, &weights_config, &weights_header);

  usleep(ska::pst::common::microseconds_per_decisecond);

  recv->utc_stop_scan(stop_header);

  UDPGenerator udpgen(format);
  udpgen.configure_beam(beam_config);
  udpgen.configure_scan(scan_config);
  static constexpr int tobs = 1; // seconds
  static constexpr float data_rate = -1;
  udpgen.transmit(tobs, data_rate);

  recv->stop_scan();
  recv->deconfigure_scan();
  recv->deconfigure_beam();
  recv->quit();

  PSTLOG_DEBUG(this, "UDPReceiveDBTest test_data_acquisition data_thread.join()");
  data_thread.join();
  PSTLOG_DEBUG(this, "UDPReceiveDBTest test_data_acquisition weights_thread.join()");
  weights_thread.join();

  ASSERT_TRUE(data_seq_valid);
  ASSERT_TRUE(weights_seq_valid);

}
#endif

} // namespace ska::pst::recv::test
