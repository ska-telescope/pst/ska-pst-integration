/*
 * Copyright 2022 Square Kilometre Array Observatory
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 * contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#include <iostream>
#include <cstdlib>
#include <pthread.h>
#include <string>

#include "ska_pst/smrb/DataBlockWrite.h"
#include "ska_pst/recv/formats/UDPFormat.h"
#include "ska_pst/recv/network/UDPStats.h"
#include "ska_pst/recv/clients/Receiver.h"

#ifndef __SKA_PST_RECV_CLIENTS_UDPReceiveDB_h
#define __SKA_PST_RECV_CLIENTS_UDPReceiveDB_h

namespace ska::pst::recv {

  /**
   * @brief Receives CBF/PSR packets with a standard UDP socket, writing the data and
   * weights to ring buffers. Extending the ControlledReceiver, this class can be controlled
   * via the LmcService.
   *
   */
  class UDPReceiveDB : public Receiver {

    public:

      /**
       * @brief Construct a new UDPReceiveDB object using provided socket to receive data from the
       * UDP endpoint at host
       *
       * @param sock SocketReceive on which the data will be received
       * @param host IPv4 address on the sock will used
       */
      UDPReceiveDB(std::shared_ptr<SocketReceive> sock, const std::string& host);

      /**
       * @brief Construct a new UDPReceiveDB object using provided socket to receive data from the
       * UDP endpoint at host:port
       *
       * @param sock SocketReceive on which the data will be received
       * @param host IPv4 address on the sock will used
       * @param port UDP port on which the sock will receive data
       */
      UDPReceiveDB(std::shared_ptr<SocketReceive> sock, const std::string& host, int port);

      /**
       * @brief Destroy the UDPReceiveDB object.
       * Unlocks and disconnects from the data and weights ring buffers.
       */
      virtual ~UDPReceiveDB();

      /**
       * @brief Configure the client with fixed parameters.
       * Configures the client using the fixed configuration parameters that are
       * common to all observations and available at launch. Configures the UDPFormat
       * and UDPStats instances. Opens the UDP socket to receiveUDP packets.
       *
       * @param config fixed configuration parameters
       */
      void perform_configure_beam() override;

      /**
       * @brief Release beam resources of the Receiver, which deallocates the receive
       * socket and disconnects from the data and weights ring buffers.
       *
       */
      void perform_deconfigure_beam() override;

      /**
       * @brief Processes the runtime configuration parameters in the header.
       * Prepares the format with the runtime configuration parameters and opens
       * the data blocks for writing.
       *
       */
      void perform_configure_scan() override;

      /**
       * @brief Deconfigure the scan for the receiver, closing the transfer to the data and
       * weights ring buffers, and reset the format, header and weights
       *
       */
      void perform_deconfigure_scan() override;

      /**
       * @brief Scan callback that is called by \ref ska::pst::common::ApplicationManager::main.
       * Contains scanning instructions meant to be launched in a separate thread.
       *
       */
      void perform_scan() override;

      /**
       * @brief StartScan callback that is called by \ref ska::pst::common::ApplicationManager::main.
       * Contains the instructions required prior to transitioning the state from ScanConfigured to Scanning.
       * Launches perform_scan on a separate thread.
       *
       */
      void perform_start_scan() override;

      /**
       * @brief Terminate callback that is called by \ref ska::pst::common::ApplicationManager::main.
       * Contains the instructions required prior to transitioning the state from RuntimeError to Idle.
       *
       */
      void perform_terminate() override;

      /**
       * @brief Return the size of a data block element
       *
       * @return uint64_t size of data block buffer in bytes
       */
      uint64_t get_data_bufsz() override;

      /**
       * @brief Set the new validity flags policy for the UDPFormat.
       *
       * @param policy new validity flags policy to use when interpreting packet validity flags
       */
      void set_validity_flags_policy(PacketValidityFlagsPolicy policy);

      /**
       * @brief Get the validity flags policy for the UDPFormat.
       *
       * @return PacketValidityFlagsPolicy current validity flags policy that is used when interpreting packet validity flags
       */
      PacketValidityFlagsPolicy get_validity_flags_policy() const { return validity_flags_policy; };

      //! return mapped diagnostic context key/value pairs
      const std::map<std::string, std::string>& get_log_context() const
      {
        return log_context;
      }

    protected:

      //! Output data block that will receive data samples from the UDP streams
      std::unique_ptr<ska::pst::smrb::DataBlockWrite> db{nullptr};

      //! Output data block that will receive relative weights from the UDP streams
      std::unique_ptr<ska::pst::smrb::DataBlockWrite> wb{nullptr};

      //! UDP format providing the mapping from UDP metadata to Ring Buffer byte offsets
      std::shared_ptr<UDPFormat> format{nullptr};

      //! UDP Socket Receiver that that abstracts the receipt of UDP packets from the UDPFormat
      std::shared_ptr<SocketReceive> socket{nullptr};

    private:

      //! Receive a single block of data in an observation
      void receive_block();

      /**
       * @brief Close the curr bufs for the data and weights ring buffers
       *
       */
      void close_curr_bufs();

      //! List of scan configuration keys to strip from the data and weights headers
      std::vector<std::string> keys_to_remove{"DB_BUFSZ", "DB_NBUFS", "WB_BUFSZ", "WB_NBUFS", "HB_BUFSZ", "HB_NBUFS"};

      //! policy for handling invalid flags in the CBFPSR format
      PacketValidityFlagsPolicy validity_flags_policy{PacketValidityFlagsPolicy::IgnoreAll};

      //! mapped diagnostic context key/value pairs
      std::map<std::string, std::string> log_context = {{"entity", entity}};
  };

} // ska::pst::recv

#endif // __SKA_PST_RECV_CLIENTS_UDPReceiveDB_h
