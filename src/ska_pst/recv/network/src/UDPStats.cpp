/*
 * Copyright 2022 Square Kilometre Array Observatory
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 * contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#include <stdexcept>

#include "ska_pst/common/definitions.h"
#include "ska_pst/common/utils/Logging.h"

#include "ska_pst/recv/network/UDPStats.h"

ska::pst::recv::UDPStats::UDPStats()
{
  PSTLOG_DEBUG(this, "UDPStats constructor");
}

void ska::pst::recv::UDPStats::configure(unsigned _packet_size, unsigned _packet_data_size)
{
  PSTLOG_DEBUG(this, "UDPStats configure");
  packet_size = _packet_size;
  packet_data_size = _packet_data_size;
  reset();
}

void ska::pst::recv::UDPStats::reset()
{
  PSTLOG_DEBUG(this, "UDPStats reset");
  timer.reset();
  timeout_timer.reset();
  curr_counts = {};
  previous_counts = {};
  rates = {};
}

void ska::pst::recv::UDPStats::increment(uint64_t num_packets)
{
  increment_bytes(num_packets * packet_size);
}

void ska::pst::recv::UDPStats::increment_bytes(uint64_t nbytes)
{
  curr_counts.bytes_transmitted += nbytes;
}

void ska::pst::recv::UDPStats::dropped_bytes(uint64_t nbytes)
{
  curr_counts.bytes_dropped += nbytes;
}

void ska::pst::recv::UDPStats::dropped(uint64_t ndropped)
{
  dropped_bytes(ndropped * packet_size);
}

void ska::pst::recv::UDPStats::sleeps(uint64_t to_add)
{
  curr_counts.nsleeps += to_add;
}

void ska::pst::recv::UDPStats::ignored(uint64_t to_add)
{
  curr_counts.ignored_packets += to_add;
}

void ska::pst::recv::UDPStats::malformed(uint64_t to_add)
{
  curr_counts.malformed_packets += to_add;
}

void ska::pst::recv::UDPStats::misdirected(uint64_t to_add)
{
  curr_counts.misdirected_packets += to_add;
}

void ska::pst::recv::UDPStats::misordered(uint64_t to_add)
{
  curr_counts.misordered_packets += to_add;
}

void ska::pst::recv::UDPStats::discarded(uint64_t to_add)
{
  curr_counts.discarded_packets += to_add;
}

void ska::pst::recv::UDPStats::no_valid_polarisation_correction(uint64_t to_add)
{
  curr_counts.no_valid_polarisation_correction_packets += to_add;
}

void ska::pst::recv::UDPStats::no_valid_station_beam(uint64_t to_add)
{
  curr_counts.no_valid_station_beam_packets += to_add;
}

void ska::pst::recv::UDPStats::no_valid_pst_beam(uint64_t to_add)
{
  curr_counts.no_valid_pst_beam_packets += to_add;
}

void ska::pst::recv::UDPStats::packet_receive_timeout()
{
  curr_counts.packet_receive_timeouts++;
}

auto ska::pst::recv::UDPStats::get_packet_receive_timeouts() const -> uint64_t
{
  return curr_counts.packet_receive_timeouts;
}

auto ska::pst::recv::UDPStats::get_packets_transmitted() const -> uint64_t
{
  if (packet_size == 0)
  {
    throw std::runtime_error("UDPStats get_packets_transmitted packet_size not configured");
  }
  return curr_counts.bytes_transmitted / packet_size;
}

auto ska::pst::recv::UDPStats::get_packets_dropped() const -> uint64_t
{
  if (packet_size == 0)
  {
    throw std::runtime_error("UDPStats get_packets_dropped packet_size not configured");
  }
  return curr_counts.bytes_dropped / packet_size;
}

void ska::pst::recv::UDPStats::set_packet_timeout_threshold(uint64_t threshold)
{
  PSTLOG_DEBUG(this, "set_packet_timeout_threshold() threshold={}", threshold);
  packet_timeout_threshold = threshold;
}

void ska::pst::recv::UDPStats::set_initial_packet_timeout_threshold(uint64_t threshold)
{
  PSTLOG_DEBUG(this, "set_initial_packet_timeout_threshold() threshold={}", threshold);
  initial_packet_timeout_threshold = threshold;
}

auto ska::pst::recv::UDPStats::get_packet_timeout_threshold() -> uint64_t
{
  PSTLOG_DEBUG(this, "get_packet_timeout_threshold()");
  return packet_timeout_threshold;
}

auto ska::pst::recv::UDPStats::get_initial_packet_timeout_threshold() -> uint64_t
{
  PSTLOG_DEBUG(this, "get_initial_packet_timeout_threshold()");
  return initial_packet_timeout_threshold;
}

void ska::pst::recv::UDPStats::compute_rates()
{
  compute_rates_mutex.lock();

  double elapsed_seconds = timer.get_elapsed_microseconds() * ska::pst::common::seconds_per_microsecond;
  auto counts_diff = curr_counts - previous_counts;

  // only compute real rates if a minimum sampling time has elapsed
  if (elapsed_seconds > minimum_elapsed)
  {
    rates = counts_diff / elapsed_seconds;

    if (counts_diff.bytes_transmitted > 0)
    {
      timeout_timer.reset();
    }
    else
    {
      double elapsed_timeout_seconds = timeout_timer.get_elapsed_microseconds() * ska::pst::common::seconds_per_microsecond;
      auto timeout_threshold = static_cast<double>(packet_timeout_threshold);
      if (curr_counts.bytes_transmitted == 0)
      {
        timeout_threshold = static_cast<double>(initial_packet_timeout_threshold);
      }
      if (elapsed_timeout_seconds > timeout_threshold)
      {
        packet_receive_timeout();
      }
    }

    // reset the timer
    timer.reset();
  }
  else
  {
    rates = {};
  }

  previous_counts = curr_counts;
  compute_rates_mutex.unlock();
}

auto ska::pst::recv::UDPStats::get_data_transmission_rate() const -> double
{
  return rates.bytes_transmitted_per_second;
}

auto ska::pst::recv::UDPStats::get_data_drop_rate() const -> double
{
  return rates.bytes_dropped_per_second;
}

auto ska::pst::recv::UDPStats::get_sleep_rate() const -> double
{
  return rates.nsleeps_per_second;
}

auto ska::pst::recv::UDPStats::get_no_valid_polarisation_correction_rate() const -> double
{
  return rates.no_valid_polarisation_correction_packet_rate;
}

auto ska::pst::recv::UDPStats::get_no_valid_station_beam_rate() const -> double
{
  return rates.no_valid_station_beam_packet_rate;
}

auto ska::pst::recv::UDPStats::get_no_valid_pst_beam_rate() const -> double
{
  return rates.no_valid_pst_beam_packet_rate;
}
