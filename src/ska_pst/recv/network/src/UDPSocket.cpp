/*
 * Copyright 2022 Square Kilometre Array Observatory
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 * contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#include <cstring>
#include <stdexcept>

#include "ska_pst/common/utils/Logging.h"

#include "ska_pst/recv/network/UDPSocket.h"

void ska::pst::recv::UDPSocket::open(int port)
{
  PSTLOG_DEBUG(this, "UDPSocket open port={}", port);
  if (port < SKA_PST_RECV_MIN_UDP_PORT || port > SKA_PST_RECV_MAX_UDP_PORT)
  {
    PSTLOG_ERROR(this, "UDPSocket open port={} was not in range {}-{}", port, SKA_PST_RECV_MIN_UDP_PORT, SKA_PST_RECV_MAX_UDP_PORT);
    throw std::runtime_error("Invalid UDP port for socket");
  }

  fd = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);
  if (fd < 0)
  {
    PSTLOG_ERROR(this, "UDPSocket open unable to create socket");
    throw std::runtime_error("Could not create socket");
  }

  PSTLOG_DEBUG(this, "UDPSocket open fd={}", fd);

  // ensure the socket is reuseable without the painful timeout
  int on = 1;
  if (setsockopt(fd, SOL_SOCKET, SO_REUSEADDR, &on, sizeof(on)) != 0)
  {
    PSTLOG_ERROR(this, "UDPSocket open unable to set SO_REUSEADDR socket option");
    throw std::runtime_error("Could not set SO_REUSEADDR socket option");
  }

  // fill in the udp socket struct with class, port
  memset(&(udp_sock.sin_zero), 0, sizeof(udp_sock.sin_zero));
  udp_sock.sin_family = AF_INET;
  udp_sock.sin_port = htons(port);

  // fill in the udp socket struct with class, port
  memset(&(other_udp_sock.sin_zero), 0, sizeof(udp_sock.sin_zero));
  other_udp_sock.sin_family = AF_INET;
  other_udp_sock.sin_port = htons(port+1);

  // by default all sockets are blocking
  set_block();
}
