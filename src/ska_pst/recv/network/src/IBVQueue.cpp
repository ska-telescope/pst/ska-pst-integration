/*
 * Copyright 2022-2024 Square Kilometre Array Observatory
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 * contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#include <unistd.h>         // for sleep
#include <stdexcept>
#include <cstdlib>

#include "ska_pst/common/utils/Logging.h"

#include "ska_pst/recv/network/IBVQueue.h"

constexpr size_t ska::pst::recv::IBVQueue::max_queue_length;

ska::pst::recv::IBVQueue::~IBVQueue()
{
  PSTLOG_TRACE(this, "IBVQueue ~IBVQueue deallocate()");
}

void ska::pst::recv::IBVQueue::deconfigure_beam()
{
  if (ipacket < npackets)
  {
    PSTLOG_DEBUG(this, "IBVQueue deallocate draining queue ipacket={} npackets={}", ipacket, npackets);
    while (ipacket < npackets)
    {
      int slot = acquire_packet();
      if (slot >= 0)
      {
        release_packet(slot);
      }
    }
    PSTLOG_DEBUG(this, "IBVQueue deallocate drained");
  }
  n_slots = 0;
  if (fl)
  {
    PSTLOG_TRACE(this, "IBVQueue deallocate fl.reset(nullptr)");
    fl.reset(nullptr);
  }
  if (mr)
  {
    PSTLOG_TRACE(this, "IBVQueue deallocate mr.reset(nullptr)");
    mr.reset(nullptr);
  }
  if (qp)
  {
    PSTLOG_TRACE(this, "IBVQueue deallocate qp.reset(nullptr)");
    qp.reset(nullptr);
  }
  if (pd)
  {
    PSTLOG_TRACE(this, "IBVQueue deallocate pd.reset(nullptr)");
    pd.reset(nullptr);
  }
  if (cq)
  {
    PSTLOG_TRACE(this, "IBVQueue deallocate cq.reset(nullptr)");
    cq.reset(nullptr);
  }
  if (cm)
  {
    PSTLOG_TRACE(this, "IBVQueue deallocate cm.reset(nullptr)");
    cm.reset(nullptr);
  }
  if (ec)
  {
    PSTLOG_TRACE(this, "IBVQueue deallocate ec.reset(nullptr)");
    ec.reset(nullptr);
  }
}

void ska::pst::recv::IBVQueue::allocate_resources(size_t _bufsz, size_t _packet_size)
{
  PSTLOG_DEBUG(this, "IBVQueue allocate_resources bufsz={} packet_size={}", _bufsz, _packet_size);

  if (n_slots != 0)
  {
    throw std::runtime_error("IBVQueue allocate_resources queue cannot be configured after building");
  }

  packet_size = static_cast<ssize_t>(_packet_size);
  static constexpr size_t ethernet_size = 6 + 6 + 4;
  static constexpr size_t ipv4_size = 20;
  static constexpr size_t udp_size = 8;
  static constexpr size_t vlan_size = 4;
  size_t packet_headers = ethernet_size + ipv4_size + udp_size + vlan_size;

  max_raw_size = packet_headers + _packet_size;
  min_raw_size = packet_headers;

  size_t expected_packets = std::min(_bufsz / max_raw_size, max_queue_length);
  receive_buffer_size = expected_packets * max_raw_size;

  PSTLOG_DEBUG(this, "IBVQueue allocate_resources slot_size={}", max_raw_size);
  PSTLOG_DEBUG(this, "IBVQueue allocate_resources receive_buffer_size={}", receive_buffer_size);
}

void ska::pst::recv::IBVQueue::configure(const std::string& ip_addr, int port)
{
  PSTLOG_DEBUG(this, "IBVQueue configure({}, {})", ip_addr, port);

  PSTLOG_DEBUG(this, "IBVQueue configure creating RDMAEventChannel");
  ec = std::make_unique<ska::pst::recv::RDMAEventChannel>();

  PSTLOG_DEBUG(this, "IBVQueue configure creating RDMACommunicationManager");
  cm = std::make_unique<ska::pst::recv::RDMACommunicationManager>(ec.get());

  PSTLOG_DEBUG(this, "IBVQueue configure binding to {}", ip_addr);
  cm->bind(ip_addr);

  PSTLOG_DEBUG(this, "IBVQueue configure buffer_size={} max_raw_size={}", receive_buffer_size, max_raw_size);
  n_slots = static_cast<int>(receive_buffer_size / max_raw_size);

  PSTLOG_DEBUG(this, "IBVQueue configure creating completion queue with {} slots", n_slots);
  cq = std::make_unique<ska::pst::recv::IBVCompletionQueue>(cm.get(), n_slots);

  PSTLOG_DEBUG(this, "IBVQueue configure creating protection domain");
  pd = std::make_unique<ska::pst::recv::IBVProtectionDomain>(cm.get());

  PSTLOG_DEBUG(this, "IBVQueue configure creating QP");
  qp = std::make_unique<ska::pst::recv::IBVQueuePair>(cm.get(), pd.get(), cq.get(), n_slots);

  PSTLOG_DEBUG(this, "IBVQueue configure creating flow to {}:{}", ip_addr, cm->get()->port_num);
  fl = std::make_unique<ska::pst::recv::IBVFlow>(cm.get(), qp.get(), ip_addr, port);

  buffer.resize(receive_buffer_size);
  bzero(&buffer[0], receive_buffer_size); // NOLINT

  mr = std::make_unique<ska::pst::recv::IBVMemoryRegion>(pd.get(), &buffer[0], receive_buffer_size);

  PSTLOG_DEBUG(this, "IBVQueue configure n_slots={}", n_slots);
  slots.resize(n_slots);
  wc.resize(n_slots);
  ptrs.resize(n_slots);
  for (std::size_t i = 0; i < n_slots; i++)
  {
    std::memset(&slots[i], 0, sizeof(slots[i]));
    //slots[i].wr.next = (i + 1 < n_slots) ? &slots[i+1].wr : nullptr;
    slots[i].sge.addr = reinterpret_cast<uintptr_t>(&buffer[i * max_raw_size]);
    slots[i].sge.length = max_raw_size;
    slots[i].sge.lkey = mr->get()->lkey;
    slots[i].wr.sg_list = &slots[i].sge;
    slots[i].wr.num_sge = 1;
    slots[i].wr.wr_id = i;
    qp->post_recv(&slots[i].wr);
  }

  PSTLOG_DEBUG(this, "IBVQueue configure qp.modify(IBV_QPS_RTR)");
  qp->modify(IBV_QPS_RTR);
}

auto ska::pst::recv::IBVQueue::acquire_packet() -> int
{
  // if a slot is already open, return the packet
  if (curr_slot >= 0)
  {
    return curr_slot;
  }

  // if we have processed all packets in the queue, get more
  if (ipacket == npackets)
  {
    #ifdef DEBUG
    PSTLOG_TRACE(this, "IBVQueue acquire_packet polling receive queue for {} slots", n_slots);
    #endif
    npackets = cq->poll(static_cast<int>(n_slots), &wc[0]);
    #ifdef DEBUG
    if (npackets > 0)
    {
      PSTLOG_TRACE(this, "IBVQueue acquire_packet recv_cq.poll returned {} packets", npackets);
    }
    #endif
    ipacket = 0;
    curr_slot = -1;
  }

  // if there are unprocessed packets received in the slots
  if (ipacket < npackets)
  {
    // work request ID indicates which slot the packet is present in
    curr_slot = static_cast<int>(wc[ipacket].wr_id);

#ifdef DEBUG
    PSTLOG_TRACE(this, "IBVQueue acquire_packet processing packet {}/{} in slot {}", ipacket, npackets, curr_slot);
#endif

    if (wc[ipacket].status != IBV_WC_SUCCESS)
    {
      PSTLOG_WARN(this, "Work Request failed with code {}", int(wc[ipacket].status));
      sleep(1);
      curr_slot = -1;
    }
    else
    {
#ifdef DEBUG
      PSTLOG_TRACE(this, "IBVQueue acquire_packet wc[{}].status == IBV_WC_SUCCESS", ipacket);
#endif
      char *ptr = reinterpret_cast<char *>(reinterpret_cast<std::uintptr_t>(slots[curr_slot].sge.addr)); // NOLINT
      std::size_t len = wc[ipacket].byte_len;
      if (len < min_raw_size)
      {
        release_packet(curr_slot);
      }
      else
      {
        // Sanity checks
        try
        {
#ifdef DEBUG
          PSTLOG_TRACE(this, "IBVQueue acquire_packet udp_from_ethernet()");
#endif
          payload = ska::pst::common::udp_from_ethernet(ptr, len);
          ptrs[curr_slot] = payload.data();
#ifdef DEBUG
          PSTLOG_TRACE(this, "IBVQueue acquire_packet received packet of size {} bytes", payload.size());
#endif
          if (payload.size() != packet_size)
          {
            release_packet(curr_slot);
            return MALFORMED_PACKET;
          }
          return curr_slot;
        }
        catch (std::exception &e)
        {
#ifdef DEBUG
          PSTLOG_ERROR(this, "IBVQueue acquire_packet udp_from_ethernet failed: {}", e.what());
#endif
          release_packet(curr_slot);
          return MALFORMED_PACKET;
        }
      }
    }
  }
  nsleeps++;

  // no packets available
  return NO_PACKET;
}

void ska::pst::recv::IBVQueue::release_packet(int slot)
{
#ifdef _TRACE
  PSTLOG_TRACE(this, "IBVQueue release_packet closing packet in slot {}", slot);
#endif
  if (slot != curr_slot)
  {
    PSTLOG_ERROR(this, "IBVQueue release_packet slot={} != curr_slot={}", slot, curr_slot);
    throw std::runtime_error("IBVQueue release_packet slot was not valid");
  }
  else if ((slot >= 0) && (slot < static_cast<int>(n_slots)))
  {
    qp->post_recv(&slots[slot].wr);

    // increment the packet counter
    ipacket++;

    // disable the slot index
    curr_slot = -1;
  }
  else
  {
    PSTLOG_ERROR(this, "IBVQueue release_packet slot={} n_slots={}", slot, n_slots);
    throw std::runtime_error("IBVQueue release_packet slot was not valid");
  }
}

auto ska::pst::recv::IBVQueue::process_sleeps() -> uint64_t
{
  uint64_t val = nsleeps;
  nsleeps = 0;
  return val;
}

auto ska::pst::recv::IBVQueue::get_buf_ptr(int slot_index) -> char *
{
  return ptrs[slot_index];
}

auto ska::pst::recv::IBVQueue::clear_buffered_packets() -> size_t
{
  return 0;
}
