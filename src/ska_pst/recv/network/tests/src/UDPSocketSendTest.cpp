/*
 * Copyright 2022 Square Kilometre Array Observatory
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 * contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#include "ska_pst/common/utils/Logging.h"

#include "ska_pst/common/testutils/GtestMain.h"
#include "ska_pst/recv/network/tests/UDPSocketSendTest.h"

auto main(int argc, char* argv[]) -> int
{
  return ska::pst::common::test::gtest_main(argc, argv);
}

namespace ska::pst::recv::test {

static constexpr unsigned default_udp_port = 12345;

UDPSocketSendTest::UDPSocketSendTest()
    : ::testing::Test()
{
}

void UDPSocketSendTest::SetUp()
{
}

void UDPSocketSendTest::TearDown()
{
}

TEST_F(UDPSocketSendTest, test_default_constructor) // NOLINT
{
  UDPSocketSend sock;
}

TEST_F(UDPSocketSendTest, test_open) // NOLINT
{
  UDPSocketSend sock;

  sock.open("127.0.0.1", default_udp_port, "0.0.0.0");
  ASSERT_GE(sock.get_fd(), 0);
  sock.close_me();

  sock.open("127.0.0.1", default_udp_port, "any");
  ASSERT_GE(sock.get_fd(), 0);
  sock.close_me();

  // bad port number
  EXPECT_THROW(sock.open("127.0.0.1", -1, "0.0.0.0"), std::runtime_error); // NOLINT

  // bad dest IP address
  EXPECT_THROW(sock.open("257.0.0.1", default_udp_port, "0.0.0.0"), std::runtime_error); // NOLINT

  // bad local IP address
  EXPECT_THROW(sock.open("127.0.0.1", default_udp_port, "257.0.0.0"), std::runtime_error); // NOLINT

  // local IP address that is not bindale
  EXPECT_THROW(sock.open("234.234.234.234", default_udp_port, "123.123.123.123"), std::runtime_error); // NOLINT
}

TEST_F(UDPSocketSendTest, test_send) // NOLINT
{
  UDPSocketSend sock;

  EXPECT_THROW(sock.send(), std::runtime_error); // NOLINT
  EXPECT_THROW(sock.send(sock.get_bufsz()), std::runtime_error); // NOLINT

  sock.open("127.0.0.1", default_udp_port, "0.0.0.0");

  EXPECT_EQ(sock.send(), sock.get_bufsz()); // NOLINT
  EXPECT_EQ(sock.send(sock.get_bufsz()), sock.get_bufsz()); // NOLINT

  EXPECT_THROW(sock.send(sock.get_bufsz() * 2), std::runtime_error); // NOLINT

  sock.close_me();
}

} // namespace ska::pst::recv::test
