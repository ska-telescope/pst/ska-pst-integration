/*
 * Copyright 2022-2024 Square Kilometre Array Observatory
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 * contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#include "ska_pst/common/testutils/GtestMain.h"
#include "ska_pst/common/utils/Logging.h"
#include "ska_pst/common/utils/PacketUtils.h"

#include "ska_pst/recv/network/tests/IBVQueueTest.h"

auto main(int argc, char* argv[]) -> int
{
  return ska::pst::common::test::gtest_main(argc, argv);
}

namespace ska::pst::recv::test {

IBVQueueTest::IBVQueueTest() : ::testing::Test()
{
}

void IBVQueueTest::SetUp()
{
}

void IBVQueueTest::TearDown()
{
}

TEST_F(IBVQueueTest, test_default_constructor) // NOLINT
{
  IBVQueue queue;
}

TEST_F(IBVQueueTest, test_process_sleeps) // NOLINT
{
  IBVQueue queue;
  static constexpr size_t npackets = 8;
  static constexpr size_t packet_size = 2048;
  size_t buffer_size = npackets * packet_size;
  queue.allocate_resources(buffer_size, packet_size);
  ASSERT_EQ(queue.process_sleeps(), 0);
}

TEST_F(IBVQueueTest, test_configure_pst_beam2) // NOLINT
{
  // this test is intentionally skipped, as it requires a networking
  // environment that includes an IBVerbs enabled device, the NET_RAW
  // capability and known IP address of the IBV device. pst-beam2 in the
  // Low PSI supports this, and if the test is run in a docker container
  // with the arguments below, this test can be manually included.
  //  --net=host \
  //  --device=/dev/infiniband/rdma_cm \
  //  --device=/dev/infiniband/uverbs0 \
  //  --device=/dev/infiniband/uverbs1 \
  //  --device=/dev/infiniband/uverbs2

  GTEST_SKIP();

  // only permit this test to execute on pst-beam2, as it requires an IBV capable device
  std::string pst_beam2_mac = "94:6d:ae:92:a3:c5";
  std::string data_host = "192.168.0.101";
  static constexpr int data_port = 12345;
  ska::pst::common::mac_address data_mac = ska::pst::common::interface_mac(data_host);

  if (ska::pst::common::mac_to_string(data_mac) == pst_beam2_mac)
  {
    IBVQueue queue;
    static constexpr size_t npackets = 8;
    static constexpr size_t packet_size = 2048;
    size_t buffer_size = npackets * packet_size;

    // configure and then deconfigure
    ASSERT_NO_THROW(queue.allocate_resources(buffer_size, packet_size)); // NOLINT
    ASSERT_NO_THROW(queue.configure(data_host, data_port)); // NOLINT
    ASSERT_NO_THROW(queue.deconfigure_beam()); // NOLINT

    // now ensure that the queue can be re-allocated
    ASSERT_NO_THROW(queue.allocate_resources(buffer_size, packet_size)); // NOLINT
    ASSERT_NO_THROW(queue.configure(data_host, data_port)); // NOLINT
    ASSERT_NO_THROW(queue.deconfigure_beam()); // NOLINT
  }
  else
  {
    PSTLOG_INFO(this, "skipping IBVQueue allocation and deallocation, not supported");
  }
}

} // namespace ska::pst::recv::test
