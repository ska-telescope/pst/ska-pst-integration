/*
 * Copyright 2022 Square Kilometre Array Observatory
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 * contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#include "ska_pst/common/utils/Logging.h"

#include "ska_pst/common/testutils/GtestMain.h"
#include "ska_pst/recv/network/tests/SocketTest.h"

auto main(int argc, char* argv[]) -> int
{
  return ska::pst::common::test::gtest_main(argc, argv);
}

namespace ska::pst::recv::test {

SocketTest::SocketTest()
    : ::testing::Test()
{
}

void SocketTest::SetUp()
{
}

void SocketTest::TearDown()
{
}

TEST_F(SocketTest, default_constructor) // NOLINT
{
  ConcreteSocket sock;
}

TEST_F(SocketTest, test_set_blocking) // NOLINT
{
  ConcreteSocket sock;
  EXPECT_THROW(sock.set_nonblock(), std::runtime_error); // NOLINT
  EXPECT_THROW(sock.set_block(), std::runtime_error); // NOLINT
}

TEST_F(SocketTest, test_get_blocking) // NOLINT
{
  ConcreteSocket sock;
  static constexpr char should_block = 1;
  ASSERT_EQ(sock.get_blocking(), should_block);
}

TEST_F(SocketTest, test_resize) // NOLINT
{
  ConcreteSocket sock;
  static constexpr size_t bufsz = 2048;
  sock.resize(bufsz);
  ASSERT_EQ(sock.get_bufsz(), bufsz);
}

} // namespace ska::pst::recv::test
