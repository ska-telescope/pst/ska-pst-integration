/*
 * Copyright 2022 Square Kilometre Array Observatory
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 * contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#include <sys/socket.h>
#include <netinet/in.h>
#include <netinet/ip.h> /* superset of previous */

#include "ska_pst/recv/network/Socket.h"

#ifndef SKA_PST_RECV_NETWORK_UDPSocket_h
#define SKA_PST_RECV_NETWORK_UDPSocket_h

#define SKA_PST_RECV_MIN_UDP_PORT 1024
#define SKA_PST_RECV_MAX_UDP_PORT 32768

namespace ska::pst::recv {

  /**
   * @brief Abstract class for UDP Send or Receive sockets
   *
   */
  class UDPSocket : public Socket
  {

    public:

      /**
       * @brief Construct a new UDPSocket object
       *
       */
      UDPSocket() = default;

      /**
       * @brief Destroy the UDPSocket object
       *
       */
      ~UDPSocket() = default;

      /**
       * @brief Open the UDP socket configured with the specified port
       *
       * @param port UDP port for use for the socket.
       */
      void open(int port) override;

    protected:

      //! sockaddr_in struct for UDP socket
      struct sockaddr_in udp_sock{};

      //! sockaddr_in struct for other end-point of the UDP socket
      struct sockaddr_in other_udp_sock{};

  };

} // namespace ska::pst::recv

#endif // SKA_PST_RECV_NETWORK_UDPSocket_h
