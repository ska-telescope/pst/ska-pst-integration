/*
 * Copyright 2022 Square Kilometre Array Observatory
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 * contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#include <string>
#include <iostream>

#ifndef SKA_PST_RECV_NETWORK_SocketReceive_h
#define SKA_PST_RECV_NETWORK_SocketReceive_h

namespace ska::pst::recv {

  static constexpr int NO_PACKET = -1;
  static constexpr int VALID_IGNORED_PACKET = -2;
  static constexpr int MALFORMED_PACKET = -3;

  /**
   * @brief Provides implementation of a standard UDP socket for receiving UDP packets.
   * Abstracts the complexity of interacting with linux socket libraries
   *
   */
  class SocketReceive
  {
    public:

      //! Global flag to cease receiving
      static bool keep_receiving;

      /**
       * @brief Construct a new SocketReceive object
       *
       */
      SocketReceive() = default;

      /**
       * @brief Destroy the SocketReceive object
       *
       */
      virtual ~SocketReceive() = default;

      /**
       * @brief Allocate memory resources for the receiving socket.
       *
       * @param bufsz size of the receiving buffer in bytes
       * @param packet_size size of each UDP packet payload in bytes
       */
      virtual void allocate_resources(size_t bufsz, size_t packet_size) = 0;

      /**
       * @brief Configure the receiving socket at the specified endpoint.
       * Opens the socket on the interface associated with the specified address, or if
       * "any" is specified, listen on any interface.
       *
       * @param ip_address IPv4 address for receiving UDP socket
       * @param port Port number for the UDP socket.
       */
      virtual void configure(const std::string& ip_address, int port) = 0;

      /**
       * @brief Release all resources
       *
       */
      virtual void deconfigure_beam() = 0;

      /**
       * @brief Discard all packets that are waiting in the receiving sockets ingest stream
       *
       * @return size_t number of bytes cleared.
       */
      virtual size_t clear_buffered_packets() = 0;

      /**
       * @brief Receive a single UDP packet.
       * If the previously received packet was not consumed via consume_packet, then no packet is received.
       * If the socket is blocking, this method will block until a packet is received.
       * If the socket is non-blocking, this method will busy-wait until a packet is received.
       * In the latter case, the busy-wait may be interrupted by setting SocketReceiver::keep_receiving to false.
       *
       * @return packet slot index of the acquired packet, less than 0 indicates error
       */
      virtual int acquire_packet() = 0;

      /**
       * @brief Release the most recently received packet
       *
       * @param index slot
       */
      virtual void release_packet(int index) = 0;

      /**
       * @brief Return the value of SocketReceiver::keep_receiving.
       *
       * @return true
       * @return false
       */
      inline bool still_receiving() { return keep_receiving; } ;

      /**
       * @brief Return the number of accumulated sleeps, resetting the internal counter to 0.
       *
       * @return uint64_t Number of accumulated sleeps since last reset.
       */
      virtual uint64_t process_sleeps() = 0;

      /**
       * @brief Return a pointer to the packet that is received in the specified slot
       *
       * @param slot_index
       * @return char *
       */
      virtual char * get_buf_ptr(int slot_index) = 0;

    protected:

      //! size of received packet in bytes
      ssize_t packet_size{0};

      //! size of the kernel socket buffer
      size_t receive_buffer_size{0};

      //! number of accumulated sleeps
      uint64_t nsleeps{0};

  };

} // namespace ska::pst::recv

#endif
