/*
 * Copyright 2022 Square Kilometre Array Observatory
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 * contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#include "ska_pst/common/utils/Timer.h"

#include <cstddef>
#include <inttypes.h>
#include <mutex>

#ifndef SKA_PST_RECV_NETWORK_UDPStats_h
#define SKA_PST_RECV_NETWORK_UDPStats_h

namespace ska::pst::recv
{

  typedef struct udp_stat_rates
  {
    //! Instantaneous data rate for bytes
    double bytes_transmitted_per_second{0};

    //! Instantaneous data rate most recently computed
    double bytes_dropped_per_second{0};

    //! Instantaneous number of busy-wait sleeps per second
    double nsleeps_per_second{0};

    //! The instantaneous number of packets received where no valid Jones polarisation corrections have been applied per second
    double no_valid_polarisation_correction_packet_rate{0};

    //! The instantaneous number of packets received where no valid station beam delay polynomials have been applied per second
    double no_valid_station_beam_packet_rate{0};

    //! The instantaneous number of packets received where no valid PST beam delay polynomials have been applied per second
    double no_valid_pst_beam_packet_rate{0};

  } udp_stat_rates_t;

  /**
   * @brief a struct for storing the RECV statistics for
   */
  typedef struct udp_stat_counts
  {
    //! Total number of bytes transmitted
    uint64_t bytes_transmitted{0};

    //! Total number of bytes dropped
    uint64_t bytes_dropped{0};

    //! Total number of busy-wait sleeps
    uint64_t nsleeps{0};

    //! Total number of ignored packets
    uint64_t ignored_packets{0};

    //! Total number of discarded packets
    uint64_t discarded_packets{0};

    //! Total number of malformed packets
    uint64_t malformed_packets{0};

    //! Total number of misdirected packets
    uint64_t misordered_packets{0};

    //! Total number of misordered packets
    uint64_t misdirected_packets{0};

    //! The number of packets received where no valid Jones polarisation corrections have been applied
    uint64_t no_valid_polarisation_correction_packets{0};

    //! The number of packets received where no valid station beam delay polynomials have been applied
    uint64_t no_valid_station_beam_packets{0};

    //! The number of packets received where no valid PST beam delay polynomials have been applied
    uint64_t no_valid_pst_beam_packets{0};

    //! Total number of packet receive timeouts in a scan
    uint64_t packet_receive_timeouts{0};

    udp_stat_counts operator-(const udp_stat_counts &prev) const
    {
      return {
          bytes_transmitted - prev.bytes_transmitted,
          bytes_dropped - prev.bytes_dropped,
          nsleeps - prev.nsleeps,
          ignored_packets - prev.ignored_packets,
          discarded_packets - prev.discarded_packets,
          malformed_packets - prev.malformed_packets,
          misordered_packets - prev.misordered_packets,
          misdirected_packets - prev.misdirected_packets,
          no_valid_polarisation_correction_packets - prev.no_valid_polarisation_correction_packets,
          no_valid_station_beam_packets - prev.no_valid_station_beam_packets,
          no_valid_pst_beam_packets - prev.no_valid_pst_beam_packets,
          packet_receive_timeouts - prev.packet_receive_timeouts};
    }

    udp_stat_rates operator/(const double elapsed_seconds) const
    {
      return {
          static_cast<double>(bytes_transmitted) / elapsed_seconds,
          static_cast<double>(bytes_dropped) / elapsed_seconds,
          static_cast<double>(nsleeps) / elapsed_seconds,
          static_cast<double>(no_valid_polarisation_correction_packets) / elapsed_seconds,
          static_cast<double>(no_valid_station_beam_packets) / elapsed_seconds,
          static_cast<double>(no_valid_pst_beam_packets) / elapsed_seconds};
    }
  } udp_stat_counts_t;

  /**
   * @brief Accumulate statistics for UDP based sockets.
   * Provides information on data capture rates, data loss rates, interface errors.
   *
   */
  class UDPStats
  {

  public:
    /**
     * @brief Construct a new UDPStats object
     *
     */
    UDPStats();

    /**
     * @brief Destroy the UDPStats object
     *
     */
    ~UDPStats() = default;

    /**
     * @brief Configure the object based on UDP packet_size and packet_data_size.
     *
     * The packet_size and packet_data_size should come from a UDPFormat but
     * this would make a circular dependency on the UDPStat and UDPFormat. As such
     * this method takes values of packet_size and packet_data_size
     *
     * @param packet_size total size of UDP data block
     * @param packet_data_size size of data samples in data block
     */
    void configure(unsigned packet_size, unsigned packet_data_size);

    /**
     * @brief Increment bytes_transmitted by the specified number of packets.
     *
     * @param num_packets The number of packets to increment by.
     */
    void increment(uint64_t num_packets = 1);

    /**
     * @brief Increment bytes_transmitted by the specified number of bytes.
     *
     * @param nbytes Number of bytes to increment by.
     */
    void increment_bytes(uint64_t nbytes);

    /**
     * @brief Increment bytes_dropped by the specified number of packets.
     *
     * @param num_packets The number of packets that were dropped.
     */
    void dropped(uint64_t num_packets = 1);

    /**
     * @brief Increment bytes_dropped by the specified amount.
     *
     * @param nbytes Number of bytes to increment bytes_dropped.
     */
    void dropped_bytes(uint64_t nbytes);

    /**
     * @brief Increment the number of valid packets that were ignored due to PSN/timestamps.
     *
     * @param num_packets The number of valid packets that were ignored
     */
    void ignored(uint64_t num_packets = 1);

    /**
     * @brief Increment the number of busy-wait sleeps by the specified amount.
     *
     * @param num_sleeps Number of busy-wait sleeps
     */
    void sleeps(uint64_t num_sleeps = 1);

    /**
     * @brief Increment the number of malformed packets
     *
     * @param num_packets
     */
    void malformed(uint64_t num_packets = 1);

    /**
     * @brief Increment the number of misordered packets
     *
     * @param num_packets
     */
    void misordered(uint64_t num_packets = 1);

    /**
     * @brief Increment the number of misdirected packets
     *
     * @param num_packets
     */
    void misdirected(uint64_t num_packets = 1);

    /**
     * @brief Increment the number of discarded packets
     *
     * @param num_packets
     */
    void discarded(uint64_t num_packets = 1);

    /**
     * @brief Increment the number of packets received where no valid Jones polarisation corrections have been applied
     *
     * @param num_packets
     */
    void no_valid_polarisation_correction(uint64_t num_packets = 1);

    /**
     * @brief Increment the number of packets received where no valid station beam delay polynomials have been applied
     *
     * @param num_packets
     */
    void no_valid_station_beam(uint64_t num_packets = 1);

    /**
     * @brief Increment the number of packets received where no valid PST beam delay polynomials have been applied
     *
     * @param num_packets
     */
    void no_valid_pst_beam(uint64_t num_packets = 1);

    /**
     * @brief Reset the internal counters.
     *
     */
    void reset();

    /**
     * @brief Return the amount of data of data transmitted.
     *
     * @return uint64_t Number of bytes transmitted.
     */
    uint64_t get_data_transmitted() const { return curr_counts.bytes_transmitted; }

    /**
     * @brief Return the number of packets transmitted.
     *
     * @return uint64_t Number of packets transmitted
     */
    uint64_t get_packets_transmitted() const;

    /**
     * @brief Return the number of bytes dropped.
     *
     * @return uint64_t Number of bytes dropped.
     */
    uint64_t get_data_dropped() const { return curr_counts.bytes_dropped; }

    /**
     * @brief Return the number of packets dropped.
     *
     * @return uint64_t Number of packets dropped.
     */
    uint64_t get_packets_dropped() const;

    /**
     * @brief Get the number of discarded packets
     *
     * @return uint64_t Number of discarded packets
     */
    uint64_t get_discarded() const { return curr_counts.discarded_packets; }

    /**
     * @brief Return the number of busy-wait sleeps.
     *
     * @return uint64_t Number of busy-wait sleeps.
     */
    uint64_t get_nsleeps() const { return curr_counts.nsleeps; }

    /**
     * @brief Return the number of valid packets that were ignored
     *
     * @return uint64_t Number of ignored packets
     */
    uint64_t get_ignored() const { return curr_counts.ignored_packets; }

    /**
     * @brief Return the number of malformed packets
     *
     * @return uint64_t Number of malformed packets
     */
    uint64_t get_malformed() const { return curr_counts.malformed_packets; }

    /**
     * @brief Get the number of misdirected packets
     *
     * @return uint64_t Number of misdirected packets
     */
    uint64_t get_misdirected() const { return curr_counts.misdirected_packets; }

    /**
     * @brief Get the number of misordered packets
     *
     * @return uint64_t Number of misordered packets
     */
    uint64_t get_misordered() const { return curr_counts.misordered_packets; }

    /**
     * @brief Get the number of packets received where no valid Jones polarisation corrections have been applied
     *
     * @return uint64_t the number of packets received where no valid Jones polarisation corrections have been applied
     */
    uint64_t get_no_valid_polarisation_correction() const { return curr_counts.no_valid_polarisation_correction_packets; }

    /**
     * @brief Get the number of packets received where no valid station beam delay polynomials have been applied
     *
     * @return uint64_t the number of packets received where no valid station beam delay polynomials have been applied
     */
    uint64_t get_no_valid_station_beam() const { return curr_counts.no_valid_station_beam_packets; }

    /**
     * @brief Get the number of packets received where no valid PST beam delay polynomials have been applied
     *
     * @return uint64_t the number of packets received where no valid PST beam delay polynomials have been applied
     */
    uint64_t get_no_valid_pst_beam() const { return curr_counts.no_valid_pst_beam_packets; }

    /**
     * @brief Compute the instantaneous data rates
     *
     */
    void compute_rates();

    /**
     * @brief Get the data transmission rate.
     *
     * @return float data transmission rate in bytes per second.
     */
    double get_data_transmission_rate() const;

    /**
     * @brief Get the data drop rate
     *
     * @return float data drop rate in bytes per second.
     */
    double get_data_drop_rate() const;

    /**
     * @brief Get the number of busy-wait sleeps per second
     *
     * @return double busy wait sleeps per second.
     */
    double get_sleep_rate() const;

    /**
     * @brief Get the number of packets received where no valid Jones polarisation corrections have been applied per second
     *
     * @return double the number of packets received where no valid Jones polarisation corrections have been applied per second
     */
    double get_no_valid_polarisation_correction_rate() const;

    /**
     * @brief Get the number of packets received where no valid station beam delay polynomials have been applied per second
     *
     * @return double the number of packets received where no valid station beam delay polynomials have been applied per second
     */
    double get_no_valid_station_beam_rate() const;

    /**
     * @brief Get the number of packets received where no valid PST beam delay polynomials have been applied per second
     *
     * @return double the number of packets received where no valid PST beam delay polynomials have been applied per second
     */
    double get_no_valid_pst_beam_rate() const;

    /**
     * @brief Increment the number of packet timeout
     *
     */
    void packet_receive_timeout();

    /**
     * @brief Get the packet receive timeouts
     *
     * @return uint64_t returns packet_receive_timeouts
     */
    uint64_t get_packet_receive_timeouts() const;

    /**
     * @brief Set the packet timeout threshold
     *
     * @param threshold timeout threshold between packets
     */
    void set_packet_timeout_threshold(uint64_t threshold);

    /**
     * @brief Set the initial packet timeout threshold
     *
     * @param threshold Timeout threshold when waiting for the first packet
     */
    void set_initial_packet_timeout_threshold(uint64_t threshold);

    /**
     * @brief Get the packet timeout threshold
     *
     * @return uint64_t returns packet_timeout_threshold. Default value of 1
     */
    uint64_t get_packet_timeout_threshold();

    /**
     * @brief Get the initial packet timeout threshold
     *
     * @return uint64_t returns initial_packet_timeout_threshold. Default value of 10
     */
    uint64_t get_initial_packet_timeout_threshold();

  private:
    //! Size of data/samples in each packet
    unsigned packet_data_size{0};

    //! Total size of packet data, including data, weights and headers
    unsigned packet_size{0};

    //! Current stat counters
    udp_stat_counts_t curr_counts{};

    //! Previous stat counters
    udp_stat_counts_t previous_counts{};

    //! Current stat rates
    udp_stat_rates_t rates{};

    //! Timeout threshold for the first packet
    uint64_t initial_packet_timeout_threshold = 10;

    //! Timeout threshold between packets
    uint64_t packet_timeout_threshold = 1;

    //! Mutex to prevent multiple threads from updating the timer
    std::mutex compute_rates_mutex;

    //! Timer that records the elapsed time between data rate calculations
    ska::pst::common::Timer timer{};

    //! Timer that records the elapsed time between packets received
    ska::pst::common::Timer timeout_timer{};

    //! Minimum elapsed time between sampling in seconds
    double minimum_elapsed{0.001};
  };

} // namespace ska::pst::recv

#endif // SKA_PST_RECV_NETWORK_UDPStats_h
