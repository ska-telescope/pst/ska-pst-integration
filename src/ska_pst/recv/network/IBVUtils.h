/*
 * Copyright 2022 Square Kilometre Array Observatory
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 * contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#include <memory>
#include <vector>
#include <cstdint>

#include <infiniband/verbs.h>
#include <rdma/rdma_cma.h>

#ifndef SKA_PST_RECV_NETWORK_IBVUtils_h
#define SKA_PST_RECV_NETWORK_IBVUtils_h

namespace ska::pst::recv {

  /**
   * @brief Structure for an IBV slot in the receive work queue
   *
   */
  typedef struct ibv_slot
  {
    //! Work request of a received packet
    ibv_recv_wr wr;

    //! Scatter/Gather address for the packet
    ibv_sge sge;

  } ibv_slot_t;

  /**
   * @brief Wrapper class for the rdma_event_channel struct
   *
   */
  class RDMAEventChannel
  {
    public:
      /**
       * @brief Construct a new RDMAEventChannel object
       *
       */
      RDMAEventChannel();

      /**
       * @brief Destroy the RDMAEventChannel object
       *
       */
      ~RDMAEventChannel();

      /**
       * @brief Return a pointer to the RDMA event channel
       *
       * @return rdma_event_channel* RDMA event channel
       */
      rdma_event_channel * get() const { return ec; };

    private:

      //! RDMA event channel
      rdma_event_channel * ec;
  };

  /**
   * @brief Wrapper class for the RDMA communication manager struct
   *
   */
  class RDMACommunicationManager
  {
    public:

      /**
       * @brief Construct a new RDMACommunicationManager object
       *
       */
      RDMACommunicationManager() = default;

      /**
       * @brief Construct a new RDMACommunicationManager object using the RDMAEventChannel
       *
       * @param ec
       */
      explicit RDMACommunicationManager(RDMAEventChannel * ec);

      /**
       * @brief Destroy the RDMACommunicationManager object
       *
       */
      ~RDMACommunicationManager();

      /**
       * @brief Bind the RDMACommunicationManager to the interface
       *
       * @param ip_addr IPv4 address of the interface
       */
      void bind(const std::string &ip_addr);

      /**
       * @brief Return a pointer to the RDMA communication manager struct
       *
       * @return rdma_cm_id* RDMA communication manager
       */
      rdma_cm_id * get() const { return cm_id; };

    private:

      //! RDMA communication manager
      rdma_cm_id * cm_id{nullptr};
  };

  /**
   * @brief Infiniband Verb Completion Queue
   *
   */
  class IBVCompletionQueue
  {
    public:

      /**
       * @brief Construct a new IBVCompletionQueue object
       *
       */
      IBVCompletionQueue() = default;

      /**
       * @brief Construct a new IBVCompletionQueue object with the specified number of slots
       *
       * @param cm RDMACommunicationManager from which to acquire the struct ibv_context
       * @param n_slots number of slots to configure in the completion queue
       */
      IBVCompletionQueue(RDMACommunicationManager * cm, int n_slots);

      /**
       * @brief Destroy the IBVCompletionQueue object
       *
       */
      ~IBVCompletionQueue();

      /**
       * @brief polls the completion queue for work completions and returns
       * the first num_entries in the array wc.
       *
       * @param num_entries maximum number of entries to return
       * @param wc pointer to an array of ibv_wc structs
       * @return int number of work completions returned
       */
      int poll(int num_entries, ibv_wc *wc);

      /**
       * @brief Return a pointer to the ibv_cq struct
       *
       * @return ibv_cq*
       */
      ibv_cq * get() const { return cq; };

    private:

      //! pointer to the ibv_cq struct managed by this class
      ibv_cq * cq;
  };

  /**
   * @brief Infiniband Verb Protection Domain.
   *
   */
  class IBVProtectionDomain
  {
    public:

      /**
       * @brief Construct a new IBVProtectionDomain object
       *
       */
      IBVProtectionDomain() = default;

      /**
       * @brief Construct a new IBVProtectionDomain object using the RDMA device context
       * managed by the RDMACommunicationManager
       *
       * @param cm RDMACommunicationManager from which the RDMA device context is acquired
       */
      explicit IBVProtectionDomain(RDMACommunicationManager * cm);

      /**
       * @brief Destroy the IBVProtectionDomain object
       *
       */
      ~IBVProtectionDomain();

      /**
       * @brief Return a pointer to the ibv_pd struct managed by this class.
       *
       * @return ibv_pd* pointer ibv_pd struct managed by this class
       */
      ibv_pd * get() const { return pd; };

    private:

      //! ibv_pd struct managed by this class
      ibv_pd * pd;
  };

  /**
   * @brief Infiniband Queue Pair that manages the send and recv completion queues.
   *
   */
  class IBVQueuePair
  {
    public:

      /**
       * @brief Construct a new IBVQueuePair object
       *
       */
      IBVQueuePair() = default;

      /**
       * @brief Construct a new IBVQueuePair object and initialise the ibv_qp struct.
       * Creates a queue pair that associated with the protection domain
       *
       * @param cm RDMACommunicationManager that transitions the queue pair to init state.
       * @param pd IBVProtectionDomain with which the queue pair is associated.
       * @param cq IBVCompletionQueue to be used for the send and recv queues.
       * @param n_slots number of receive queue slots.
       */
      IBVQueuePair(RDMACommunicationManager * cm, IBVProtectionDomain * pd, IBVCompletionQueue * cq, int n_slots);

      /**
       * @brief Destroy the IBVQueuePair object
       *
       */
      ~IBVQueuePair();

      /**
       * @brief Return a pointer to the ibv_qp struct managed by this class
       *
       * @return ibv_qp* pointer to the ibv_qp struct managed by this class
       */
      ibv_qp * get() const { return qp; };

      /**
       * @brief posts the work request to the receive queue of the queue pair.
       *
       * @param wr work request to be posted to the receive queue
       */
      void post_recv(ibv_recv_wr *wr);

      /**
       * @brief Modify the state of the queue pair.
       *
       * @param qp_state new state to transition the queue pair to.
       */
      void modify(ibv_qp_state qp_state);

    private:

      /**
       * @brief Modify the attributes of the queue pair
       *
       * @param qp_state new queue pair state
       * @param port_num primary port number
       */
      void modify(ibv_qp_state qp_state, int port_num);

      /**
       * @brief Modify the queue pair attributes
       *
       * @param attr new attributes use in the queue pair
       * @param attr_mask mask of attributes to apply
       */
      void modify(ibv_qp_attr *attr, int attr_mask);

      //! pointer to the ibv_qp struct managed by this class
      ibv_qp * qp{nullptr};
  };

  /**
   * @brief Infiniband Verb Memory Region that manages memory used by the RDMA context
   *
   */
  class IBVMemoryRegion
  {
    public:

      /**
       * @brief Construct a new IBVMemoryRegion object
       *
       */
      IBVMemoryRegion() = default;

      /**
       * @brief Construct a new IBVMemoryRegion object which registers a memory region associated with the protection domain.
       *
       * @param pd IBVProtectionDomain in which the memory region is associated
       * @param addr pointer to the memory region
       * @param length length of the memory region in bytes
       */
      IBVMemoryRegion(IBVProtectionDomain * pd, void *addr, std::size_t length);

      /**
       * @brief Destroy the IBVMemoryRegion object
       *
       */
      ~IBVMemoryRegion();

      /**
       * @brief Returns a pointer to the ibv_mr struct managed by this class.
       *
       * @return ibv_mr* pointer to the ibv_mr struct managed by this class.
       */
      ibv_mr * get() const { return mr; };

    private:

      //! pointer to the ibv_mr struct managed by this class.
      ibv_mr * mr{nullptr};
  };

  /**
   * @brief Infiniband Verb Flow that assigns a data flow in the NIC to the context
   *
   */
  class IBVFlow
  {
    static constexpr uint8_t dst_mac_mask = 0xFF;

    static constexpr uint8_t dst_ip_mask = 0xFF;

    static constexpr uint16_t dst_port_mask = 0xFFFF;

    public:
      /**
       * @brief Construct a new IBVFlow object
       *
       */
      IBVFlow() = default;

      /**
       * @brief Construct a new IBVFlow object
       *
       * @param cm RDMACommunicationManager
       * @param qp IBVQueuePair in which the flow is created
       * @param ip IPv4 address for the destination of the flow
       * @param port UDP port for the destination of the flow
       */
      IBVFlow(RDMACommunicationManager * cm, IBVQueuePair * qp, const std::string &ip, int port);

      /**
       * @brief Destroy the IBVFlow object
       *
       */
      ~IBVFlow();

    private:

      //! pointer to the ibv_flow struct managed by this class.
      ibv_flow * flow{nullptr};
  };

} // namespace ska::pst::recv

#endif // SKA_PST_RECV_NETWORK_IBVUtils_h
