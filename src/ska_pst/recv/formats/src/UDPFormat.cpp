/*
 * Copyright 2022 Square Kilometre Array Observatory
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 * contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#include <cmath>
#include <stdexcept>
#include <cstring>
#include <cstdlib>
#include <regex>
#include <iostream>

#include "ska_pst/common/utils/Logging.h"
#include "ska_pst/common/definitions.h"
#include "ska_pst/recv/formats/UDPFormat.h"


ska::pst::recv::UDPFormat::UDPFormat()
{
  packet_header_size = sizeof(cbf_psr_header_t);
}

ska::pst::recv::UDPFormat::~UDPFormat()
{
  free(zeroed_weights); // NOLINT
  zeroed_weights = nullptr;
}

namespace ska::pst::recv {
  template<typename T>
  void validate_required_field(
    const ska::pst::common::AsciiHeader& config,
    ska::pst::common::ValidationContext *context,
    const std::string& key
  )
  {
    PSTLOG_TRACE(nullptr, "validate_required_field - {}", key);
    if (config.has(key)) {
      try {
        T value;
        config.get(key, &value);
      } catch (std::exception& exc) {
        std::string str_value = config.get_val(key);
        std::string type_name = typeid(T).name();
        PSTLOG_ERROR(nullptr, "validate_required_field: {} present in config with value {} but could not be converted to type {}", key, str_value, type_name); // NOLINT
        context->add_validation_error(key, str_value, std::string("is not of type ") + type_name);
      }
    } else {
      PSTLOG_DEBUG(nullptr, "validate_required_field: {} is missing", key);
      context->add_missing_field_error(key);
    }
  }
} // namespace ska::pst::recv

void ska::pst::recv::UDPFormat::validate_configure_beam(
  const ska::pst::common::AsciiHeader& beam_config,
  ska::pst::common::ValidationContext *context
)
{
  PSTLOG_DEBUG(this, "UDPFormat validate_configure_beam");

  // use a subcontext for validation so later it is known if all
  // the required parameters are met.
  ska::pst::common::ValidationContext subcontext;

  // assert that we have headers
  ska::pst::recv::validate_required_field<uint32_t>(beam_config, &subcontext, "NCHAN");
  ska::pst::recv::validate_required_field<uint32_t>(beam_config, &subcontext, "NDIM");
  ska::pst::recv::validate_required_field<uint32_t>(beam_config, &subcontext, "NBIT");
  ska::pst::recv::validate_required_field<uint32_t>(beam_config, &subcontext, "NPOL");
  ska::pst::recv::validate_required_field<uint32_t>(beam_config, &subcontext, "START_CHANNEL");
  ska::pst::recv::validate_required_field<uint32_t>(beam_config, &subcontext, "END_CHANNEL");
  ska::pst::recv::validate_required_field<double>(beam_config, &subcontext, "TSAMP");
  ska::pst::recv::validate_required_field<double>(beam_config, &subcontext, "BW");
  ska::pst::recv::validate_required_field<double>(beam_config, &subcontext, "FREQ");
  ska::pst::recv::validate_required_field<uint32_t>(beam_config, &subcontext, "BEAM_ID");

  if (beam_config.has("OS_FACTOR"))
  {
    PSTLOG_TRACE(this, "UDPFormat validate_configure_beam - validating OS_FACTOR");
    std::string os_factor = beam_config.get_val("OS_FACTOR");
    std::string os_factor_regex = "\\d+/\\d+";
    std::regex regex_pattern(os_factor_regex);
    if (std::regex_match(os_factor, regex_pattern))
    {
      PSTLOG_TRACE(this, "UDPFormat validate_configure_beam - OS_FACTOR matched regex pattern");
      std::istringstream iss(os_factor);
      char divide = 0;
      iss >> os_numerator >> divide >> os_denominator;

      auto expected_os_factor = get_expected_os_factor();
      if ((os_numerator != expected_os_factor.first) || (os_denominator != expected_os_factor.second))
      {
        std::stringstream ss;
        ss << "not equal to ";
        ss << expected_os_factor.first;
        ss << "/";
        ss << expected_os_factor.second;

        PSTLOG_ERROR(this, "UDPFormat validate_configure_beam config OS_FACTOR={} not {}", os_factor, ss.str());
        context->add_validation_error("OS_FACTOR", os_factor, ss.str());
      }
    }
    else
    {
      context->add_value_regex_error("OS_FACTOR", os_factor, os_factor_regex);
    }
  }
  else
  {
    context->add_missing_field_error("OS_FACTOR");
  }

  if (!subcontext.is_empty()) {
    PSTLOG_DEBUG(this, "UDPFormat validate_configure_beam - required fields not valid");
    context->copy_errors(subcontext);
    return;
  }

  // all required fields are present. Asserting they meet the boundary conditions.
  nant = nbeam = 1;
  nchan = beam_config.get<uint32_t>("NCHAN");
  ndim = beam_config.get<uint32_t>("NDIM");
  nbit = beam_config.get<uint32_t>("NBIT");
  npol = beam_config.get<uint32_t>("NPOL");
  start_channel = beam_config.get<uint32_t>("START_CHANNEL");
  end_channel = beam_config.get<uint32_t>("END_CHANNEL");
  tsamp = beam_config.get<double>("TSAMP");

  if (fabs(tsamp - get_expected_tsamp()) > 1e-6) // NOLINT
  {
    PSTLOG_WARN(this, "TSAMP={} is not {} which is {}/{}", tsamp, get_expected_tsamp(), sample_period_numerator, sample_period_denominator);
  }

  bw = beam_config.get<double>("BW");
  freq = beam_config.get<double>("FREQ");
  beam_id = beam_config.get<uint32_t>("BEAM_ID");

  if (beam_config.has("UDP_NCHAN"))
  {
    PSTLOG_TRACE(this, "UDPFormat validate_configure_beam - validating UDP_NCHAN");

    auto udp_nchan = beam_config.get<uint32_t>("UDP_NCHAN");

    if (udp_nchan != nchan_per_packet)
    {
      PSTLOG_ERROR(this, "UDPFormat validate_configure_beam config UDP_NCHAN={} expected={}", udp_nchan, nchan_per_packet);
      std::stringstream ss;
      ss << "not equal to ";
      ss << nchan_per_packet;

      context->add_validation_error("UDP_NCHAN", udp_nchan, ss.str());
    }
  }

  if (nchan % nchan_per_packet != 0)
  {
    PSTLOG_ERROR(this, "UDPFormat validate_configure_beam config NCHAN={} not a multiple of UDP_NCHAN={}", nchan, nchan_per_packet);
    std::stringstream ss;
    ss << "not a multiple of UDP_NCHAN=";
    ss << nchan_per_packet;
    context->add_validation_error("NCHAN", nchan, ss.str());
  }

  if (nchan != (end_channel - start_channel))
  {
    PSTLOG_ERROR(this, "UDPFormat validate_configure_beam config NCHAN={} END_CHANNEL-START_CHANNEL={}", nchan, (end_channel - start_channel));
    std::stringstream ss;
    ss << "different to END_CHANNEL - START_CHANNEL. Expected ";
    ss << (end_channel - start_channel);

    context->add_validation_error("NCHAN", nchan, ss.str());
  }

  if (beam_config.has("UDP_NSAMP") && beam_config.has("WT_NSAMP"))
  {
    auto udp_nsamp = beam_config.get<uint32_t>("UDP_NSAMP");
    auto wt_nsamp = beam_config.get<uint32_t>("WT_NSAMP");
    if (udp_nsamp % wt_nsamp != 0)
    {
      PSTLOG_ERROR(this, "UDPFormat validate_configure_beam config UDP_NSAMP={} not a multiple of WT_NSAMP={}", udp_nsamp, wt_nsamp);
      std::stringstream ss;
      ss << "not a multiple of WT_NSAMP=";
      ss << wt_nsamp;
      context->add_validation_error("UDP_NSAMP", udp_nsamp, ss.str());
    }
  }

  if (ndim != UDP_FORMAT_CBFPSR_NDIM)
  {
    PSTLOG_ERROR(this, "UDPFormat validate_configure_beam config NDIM={} expected={}", ndim, UDP_FORMAT_CBFPSR_NDIM);
    std::stringstream ss;
    ss << "not equal to ";
    ss << UDP_FORMAT_CBFPSR_NDIM;

    context->add_validation_error("NDIM", ndim, ss.str());
  }

  if (npol != UDP_FORMAT_CBFPSR_NPOL)
  {
    PSTLOG_ERROR(this, "UDPFormat validate_configure_beam config NPOL={} expected={}", npol, UDP_FORMAT_CBFPSR_NPOL);
    std::stringstream ss;
    ss << "not equal to ";
    ss << UDP_FORMAT_CBFPSR_NPOL;

    context->add_validation_error("NPOL", npol, ss.str());
  }

  if ((beam_id < UDP_FORMAT_MIN_BEAM) || (beam_id > UDP_FORMAT_MAX_BEAM))
  {
    std::stringstream ss;
    ss << "not in range [";
    ss << UDP_FORMAT_MIN_BEAM;
    ss << ", ";
    ss << UDP_FORMAT_MAX_BEAM;
    ss << "]";
    PSTLOG_ERROR(this, "UDPFormat validate_configure_beam config BEAM_ID={} not in range {}-{}", beam_id, UDP_FORMAT_MIN_BEAM, UDP_FORMAT_MAX_BEAM);
    context->add_validation_error("BEAM_ID", beam_id, ss.str());
  }

  if ((nbit != get_expected_nbit()))
  {
    std::stringstream ss;
    ss << "not equal to ";
    ss << get_expected_nbit();
    PSTLOG_ERROR(this, "UDPFormat validate_configure_beam config NBIT={} not the expected value {}", nbit, get_expected_nbit());
    context->add_validation_error("NBIT", nbit, ss.str());
  }
}

void ska::pst::recv::UDPFormat::configure_beam(const ska::pst::common::AsciiHeader& beam_config)
{
  PSTLOG_DEBUG(this, "UDPFormat configure_beam");

  check_beam_configured(false);
  check_scan_configured(false);
  check_scan_started(false);

  // ensure the subclass has configured the required parameters
  if (nsamp_per_packet == 0)
  {
    throw std::runtime_error("UDPFormat configure_beam nsamp_per_packet not configured");
  }
  if (nchan_per_packet == 0)
  {
    throw std::runtime_error("UDPFormat configure_beam nchan_per_packet not configured");
  }
  if (nsamp_per_weight == 0)
  {
    throw std::runtime_error("UDPFormat configure_beam nsamp_per_weight not configured");
  }

  // ensure we have validated the configuration
  ska::pst::common::ValidationContext context;
  validate_configure_beam(beam_config, &context);
  context.throw_error_if_not_empty();

  nant = nbeam = 1;
  nchan = beam_config.get<uint32_t>("NCHAN");
  ndim = beam_config.get<uint32_t>("NDIM");
  nbit = beam_config.get<uint32_t>("NBIT");
  npol = beam_config.get<uint32_t>("NPOL");
  start_channel = beam_config.get<uint32_t>("START_CHANNEL");
  end_channel = beam_config.get<uint32_t>("END_CHANNEL");
  tsamp = beam_config.get<double>("TSAMP");
  bw = beam_config.get<double>("BW");
  freq = beam_config.get<double>("FREQ");
  beam_id = beam_config.get<uint32_t>("BEAM_ID");

  // parse the oversampling ratio
  std::string os_factor = beam_config.get_val("OS_FACTOR");
  try
  {
    std::istringstream iss(os_factor);
    char divide = 0;
    iss >> os_numerator >> divide >> os_denominator;
    if (divide != '/')
    {
      // this should not happen if validated correctly
      PSTLOG_ERROR(this, "UDPFormat configure_beam OS_FACTOR did not use '/' as delimiter");
      throw std::runtime_error("UDPFormat read_config invalid config");
    }
  }
  catch(const std::exception& e)
  {
    // this should not happen if validated correctly
    PSTLOG_ERROR(this, "UDPFormat configure_beam failed to parse {} as OS_FACTOR", os_factor);
    throw std::runtime_error("UDPFormat read_config invalid config");
  }

  // packet scales are a single floating-point scale factor for the PST formats
  packet_scales_size = sizeof(packet.header->scale_1);

  // compute the weights strides with channel & psn
  weights_channel_stride = ((nsamp_per_packet / nsamp_per_weight) * UDP_FORMAT_CBFPSR_WEIGHT_NBIT) / ska::pst::common::bits_per_byte;
  weights_psn_stride = (nchan * weights_channel_stride) + (nchan / nchan_per_packet) * packet_scales_size;

  // the packet data size is the voltage time-samples portion of the packet
  packet_data_size = (nsamp_per_packet * nchan_per_packet * npol * ndim * nbit) / ska::pst::common::bits_per_byte;

  // the packet weights size includes padding to make it a multiple of the ska::pst::common::packet_weights_resolution
  packet_weights_size = ((nsamp_per_packet / nsamp_per_weight) * nchan_per_packet * UDP_FORMAT_CBFPSR_WEIGHT_NBIT) / ska::pst::common::bits_per_byte;
  packet_weights_size_remainder = packet_weights_size % ska::pst::common::packet_weights_resolution;
  unsigned packet_weights_size_padding = 0;
  if (packet_weights_size_remainder > 0)
  {
    packet_weights_size_padding = ska::pst::common::packet_weights_resolution - packet_weights_size_remainder;
  }

  // prepare the zeroed packet weights buffer
  free(zeroed_weights); // NOLINT
  static constexpr size_t ideal_posix_alignment = 512;
  if (posix_memalign(&zeroed_weights, ideal_posix_alignment, packet_weights_size) < 0)
  {
    throw std::runtime_error("UDPFormat read_config failed to allocate aligned memory for zeroed_weights");
  }
  memset(zeroed_weights, 0, packet_weights_size);

  // add new parameters to the weights_beam_config
  weights_beam_config.set("PACKET_WEIGHTS_SIZE", packet_weights_size);
  weights_beam_config.set("PACKET_SCALES_SIZE", packet_scales_size);

  // note the packet scales are part of the packet_header_size and not included here
  packet_size = packet_header_size + packet_weights_size + packet_weights_size_padding + packet_data_size;
  PSTLOG_DEBUG(this, "UDPFormat configure_beam packet data_size={} header_size={} weights_size={} weights_size_padding={} packet_size={}",
    packet_data_size, packet_header_size, packet_weights_size, packet_weights_size_padding, packet_size);

  // offsets for the weights and data in the packet
  packet_weights_offset = sizeof(ska::pst::recv::cbf_psr_header_t);
  packet_data_offset = sizeof(ska::pst::recv::cbf_psr_header_t) + packet_weights_size + packet_weights_size_padding;

  // size of the scale factors in each packet, in bytes
  packet_scales_size = get_packet_scales_size();
  packet_scales_offset = get_scale1_byte_offset();

  // compute the data strides with channel & psn
  data_channel_stride = (nsamp_per_packet * npol * ndim * nbit) / ska::pst::common::bits_per_byte;
  data_psn_stride = nchan * data_channel_stride;
  PSTLOG_DEBUG(this, "UDPFormat configure_beam data_channel_stride={} data_psn_stride={}", data_channel_stride, data_psn_stride);

  PSTLOG_TRACE(this, "UDPFormat configure_beam nchan={} weights_channel_stride={} nchan_per_packet={} packet_scales_size={}", nchan, weights_channel_stride, nchan_per_packet, packet_scales_size);

  PSTLOG_DEBUG(this, "UDPFormat configure_beam weights_channel_stride={} weights_psn_stride={}", weights_channel_stride, weights_psn_stride);

  uint32_t packets_per_heap = nchan / nchan_per_packet;
  weights_beam_config.set("BLOCK_HEADER_BYTES", packets_per_heap * packet_scales_size);
  weights_beam_config.set("BLOCK_DATA_BYTES", packets_per_heap * packet_weights_size);
  PSTLOG_DEBUG(this, "UDPFormat configure_beam packets_per_heap={}", packets_per_heap);

  beam_configured = true;
}

void ska::pst::recv::UDPFormat::configure_scan(const ska::pst::common::AsciiHeader& scan_config)
{
  PSTLOG_DEBUG(this, "UDPFormat configure_scan");

  check_beam_configured(true);
  check_scan_configured(false);
  check_scan_started(false);

  // scan_config only includes configuration parameters about the source to be observed
  // e.g. SOURCE, RA, DEC, PROJID, OBSERVER
  weights_scan_config.clone(weights_beam_config);
  weights_scan_config.append_header(scan_config);

  double weights_sampling_time = tsamp * nsamp_per_packet;

  // override the weights_scan_config with the parameters bespoke to the weights stream
  weights_scan_config.set("TSAMP", weights_sampling_time);
  weights_scan_config.set("NBIT", UDP_FORMAT_CBFPSR_WEIGHT_NBIT);
  weights_scan_config.set("NPOL", 1);
  weights_scan_config.set("NDIM", 1);
  weights_scan_config.set("NCHAN", nchan);
  weights_scan_config.set("ORDER", "TSFP");
  weights_scan_config.set("RESOLUTION", weights_psn_stride);
  weights_scan_config.set("NSAMP_PER_WEIGHT", nsamp_per_weight);

  scan_configured = true;
}

void ska::pst::recv::UDPFormat::set_scan_id(uint64_t _scan_id)
{
  PSTLOG_DEBUG(this, "UDPFormat set_scan_id scan_id={}", _scan_id);
  scan_id = _scan_id;
}

void ska::pst::recv::UDPFormat::conclude()
{
  reset();
}

void ska::pst::recv::UDPFormat::reset()
{
  beam_configured = false;
  scan_configured = false;
  scan_started = false;
  start_psn = -1;
  scan_id = 0;
  beam_id = 0;
  weights_beam_config.reset();
  weights_scan_config.reset();
}

auto ska::pst::recv::UDPFormat::get_samples_for_bytes(uint64_t nbytes) -> uint64_t
{
  uint64_t nsamps = nbytes / ((npol * ndim * nchan * nbit) / ska::pst::common::bits_per_byte);
  PSTLOG_DEBUG(this, "UDPFormat get_samples_for_bytes npol={} ndim={} nchan={} nsamps={}", npol, ndim, nchan, nsamps);
  return nsamps;
}

auto ska::pst::recv::UDPFormat::get_resolution() -> uint64_t
{
  check_beam_configured(true);
  uint64_t resolution = (nsamp_per_packet * nchan * ndim * npol * nbit) / ska::pst::common::bits_per_byte;
  PSTLOG_DEBUG(this, "UDPFormat get_resolution nsamp_per_packet={} nchan={} ndim={} npol={} nbit={} resolution={}",
    nsamp_per_packet, nchan, ndim, npol, nbit, resolution);
  return resolution;
}

auto ska::pst::recv::UDPFormat::get_weights_resolution() -> uint64_t
{
  check_beam_configured(true);
  uint64_t packets_per_resolution = nchan / nchan_per_packet;
  uint64_t scale_factor_resolution = packets_per_resolution * sizeof(packet.header->scale_1);
  PSTLOG_TRACE(this, "UDPFormat get_weights_resolution nchan={} nchan_per_packet={} npackets_per_resolution={} scale_factor_resolution={}", nchan, nchan_per_packet, packets_per_resolution, scale_factor_resolution);
  uint64_t weights_data_resolution = ((nsamp_per_packet/nsamp_per_weight) * nchan * UDP_FORMAT_CBFPSR_WEIGHT_NBIT) / ska::pst::common::bits_per_byte;
  PSTLOG_TRACE(this, "UDPFormat get_weights_resolution nsamp_per_packet={} nchan={} nbit={} resolution={}", (nsamp_per_packet/nsamp_per_weight), nchan, UDP_FORMAT_CBFPSR_WEIGHT_NBIT, weights_data_resolution);
  PSTLOG_DEBUG(this, "UDPFormat get_weights_resolution scale_factor_resolution={} weights_data_resolution={} weights_resolution={}", scale_factor_resolution, weights_data_resolution, scale_factor_resolution + weights_data_resolution);
  return scale_factor_resolution + weights_data_resolution;
}

auto ska::pst::recv::UDPFormat::process_stream_start(char * buf) -> ska::pst::recv::UDPFormat::PacketState
{
  PSTLOG_TRACE(this, "UDPFormat process_stream_start");

  if (scan_started)
  {
    return Ok;
  }

  packet.header = reinterpret_cast<ska::pst::recv::cbf_psr_header_t *>(buf); // NOLINT
  const uint32_t channel = packet.header->first_channel_number - start_channel;
  const uint32_t psn = packet.header->packet_sequence_number;

  start_psn = static_cast<int64_t>(psn);

  auto samples_since_ska_epoch = packet.header->samples_since_ska_epoch;
  auto sample_period_numerator = packet.header->sample_period_numerator;
  auto sample_period_denominator = packet.header->sample_period_denominator;

  PSTLOG_DEBUG(this,
      "UDPFormat process_stream_start setting up start_timestamp. samples_since_ska_epoch={}, sample_period_numerator={}, sample_period_denominator={}",
      samples_since_ska_epoch, sample_period_numerator, sample_period_denominator);

  start_timestamp = ska::pst::common::Time(samples_since_ska_epoch, sample_period_numerator, sample_period_denominator);

  PSTLOG_DEBUG(this,
      "UDPFormat process_stream_start packet.timestamp samples_since_ska_epoch={} seconds={} attoseconds={} psn={} start_psn={}",
      samples_since_ska_epoch, start_timestamp.get_time(), start_timestamp.get_fractional_time_attoseconds(), psn, start_psn);

  scan_started = true;
  PSTLOG_INFO(this, "UTC_START={} ATTOSECONDS={}", start_timestamp.get_gmtime(), start_timestamp.get_fractional_time_attoseconds());

  if (!(validate_packet_header(packet.header)))
  {
    return Malformed;
  }

  // check if the packet belongs to this stream
  if (is_misdirected())
  {
    return Misdirected;
  }

  // only start on the zeroth channel [necessary to support LowTestVector format]
  if (channel != 0)
  {
    return Ignored;
  }

  // warn if the psn of the first received packet is unexpected
  if (start_psn != UDP_FORMAT_CBFPSR_FIRST_PSN)
  {
    PSTLOG_WARN(this, "UDPFormat get_packet_timestamp start_psn={}, expecting {}", start_psn, UDP_FORMAT_CBFPSR_FIRST_PSN);
  }

  return Ok;
}

auto ska::pst::recv::UDPFormat::is_misdirected() -> bool
{
  #ifdef TRACE
  if (packet.header->beam_number != beam_id)
  {
    PSTLOG_TRACE(this, "UDPFormat is_misdirected packet.header->beam_number[{}] != beam_id[{}]", uint32_t(packet.header->beam_number), beam_id);
  }
  if (uint32_t(packet.header->scan_id) != scan_id)
  {
    PSTLOG_TRACE(this, "UDPFormat is_misdirected packet.header->scan_id={} scan_id={}", uint32_t(packet.header->scan_id), scan_id);
  }
  if (packet.header->first_channel_number < start_channel)
  {
    PSTLOG_TRACE(this, "UDPFormat is_misdirected first_channel_number={} start_channel={}", uint32_t(packet.header->first_channel_number), start_channel);
  }
  if (packet.header->first_channel_number + packet.header->channels_per_packet > end_channel)
  {
    PSTLOG_TRACE(this, "UDPFormat is_misdirected first_channel_number+channels_per_packet={} end_channel={}", uint32_t(packet.header->first_channel_number + packet.header->channels_per_packet), end_channel);
  }
  #endif

  return
    (packet.header->beam_number != beam_id) ||
    (packet.header->scan_id != scan_id) ||
    (packet.header->first_channel_number < start_channel) ||
    (packet.header->first_channel_number + packet.header->channels_per_packet > end_channel);
}

auto ska::pst::recv::UDPFormat::decode_packet(char * buf, data_and_weights_t * offsets, data_and_weights_t * received) -> ska::pst::recv::UDPFormat::PacketState
{
  check_scan_configured(true);
  check_scan_started(true);

  // use c structure that is mapped directly to the UDP socket
  packet.header = reinterpret_cast<ska::pst::recv::cbf_psr_header_t *>(buf); // NOLINT
  packet.weights = static_cast<void *>(buf + packet_weights_offset); // NOLINT
  packet.data = static_cast<void *>(buf + packet_data_offset); // NOLINT

  if (!(validate_packet_header(packet.header)))
  {
    return Malformed;
  }

  // check if the packet belongs to this stream
  if (is_misdirected())
  {
    return Misdirected;
  }

  if (packet.header->packet_sequence_number < start_psn)
  {
    return Ignored;
  }

  // currently no support for variable packet sizes
  received->data = static_cast<int64_t>(packet_data_size);
  received->weights = static_cast<int64_t>(packet_weights_size + packet_scales_size);

  const uint32_t channel = packet.header->first_channel_number - start_channel;
  const uint32_t channel_block = channel / nchan_per_packet;
  const int64_t psn = static_cast<int64_t>(packet.header->packet_sequence_number) - start_psn;

  // compute the byte offsets for the data and weights
  offsets->data = (psn * data_psn_stride) + (channel * data_channel_stride);
  offsets->weights = (psn * weights_psn_stride) + (channel * weights_channel_stride) + (channel_block * packet_scales_size);

  #if defined(DEBUG)
  PSTLOG_TRACE(this, "UDPFormat decode_packet psn packet={} start={} effective={}", uint64_t(packet.header->packet_sequence_number), start_psn, psn);
  PSTLOG_TRACE(this, "UDPFormat decode_packet channel packet={} start={} effective={} channel_block={}", uint64_t(packet.header->first_channel_number), start_channel, channel, channel_block);
  PSTLOG_TRACE(this, "UDPFormat decode_packet data_psn_stride={} data_channel_stride={} offsets.data={} offsets.weights={}", data_psn_stride, data_channel_stride, offsets->data, offsets->weights);
  #endif

  return Ok;
}

void ska::pst::recv::UDPFormat::insert_last_packet(char * data, char * weights)
{
  check_scan_started(true);

  // copy the data payload
  memcpy(static_cast<void *>(data), packet.data, packet_data_size);

  // copy the floating point scale factor
  memcpy(static_cast<void *>(weights), &(packet.header->scale_1), packet_scales_size);

  if ((validity_flags_policy == PacketValidityFlagsPolicy::IgnoreAll) || ska::pst::recv::packet_weights_valid_for_policy(packet.header, validity_flags_policy))
  {
    // copy the weights vector
    memcpy(static_cast<void *>(weights + packet_scales_size), packet.weights, packet_weights_size); // NOLINT
  }
  else
  {
    // zero the weights vector
    memcpy(static_cast<void *>(weights + packet_scales_size), zeroed_weights, packet_weights_size); // NOLINT
  }
}

void ska::pst::recv::UDPFormat::clear_scales_buffer(char * buffer, uint64_t bufsz)
{
  const uint64_t stride = packet_scales_size + packet_weights_size;
  const uint64_t nvals = bufsz / stride;

  PSTLOG_TRACE(this, "UDPFormat clear_scales_buffer buffer={} bufsz={} nvals={} stride={}", reinterpret_cast<void*>(buffer), bufsz, nvals, stride);
  for (uint64_t i=0; i<nvals; i++, buffer += stride) // NOLINT
  {
    auto scales = reinterpret_cast<float *>(buffer);
    *scales = NAN; // NOLINT
  }
}

void ska::pst::recv::UDPFormat::print_packet_header()
{
  uint64_t psn = packet.header->packet_sequence_number;
  uint32_t fcn = packet.header->first_channel_number;
  uint16_t vcpp = packet.header->valid_channels_per_packet;
  PSTLOG_DEBUG(this, "UDPFormat print_packet_header sequence_number={} channel_number={} chan_per_packet={}", psn, fcn, vcpp);
}

auto ska::pst::recv::UDPFormat::get_weights_scan_config() -> const ska::pst::common::AsciiHeader&
{
  check_beam_configured(true);
  return weights_scan_config;
}

void ska::pst::recv::UDPFormat::check_beam_configured(bool expected)
{
  if (beam_configured != expected)
  {
    PSTLOG_ERROR(this, "UDPFormat check_beam_configured beam_configured={} did not match expected={}", beam_configured, expected);
    throw std::runtime_error("UDPFormat check_beam_configured beam_configured != expected");
  }
}

void ska::pst::recv::UDPFormat::check_scan_configured(bool expected)
{
  if (scan_configured != expected)
  {
    PSTLOG_ERROR(this, "UDPFormat check_scan_configured scan_configured={} did not match expected={}", scan_configured, expected);
    throw std::runtime_error("UDPFormat check_scan_configured scan_configured != expected");
  }
}

void ska::pst::recv::UDPFormat::check_scan_started(bool expected)
{
  if (scan_started != expected)
  {
    PSTLOG_ERROR(this, "UDPFormat:check_scan_started scan_started={} did not match expected={}", scan_started, expected);
    throw std::runtime_error("UDPFormat check_scan_started scan_started != expected");
  }
}

void ska::pst::recv::UDPFormat::update_statistics(UDPStats &stats)
{
  PSTLOG_TRACE(this, "UDPFormat update_statistics - entered");
  check_scan_started(true);

  if (!packet.header->jones_polarisation_corrections_applied)
  {
    stats.no_valid_polarisation_correction();
  }
  if (!packet.header->station_beam_polynomials_applied)
  {
    stats.no_valid_station_beam();
  }

  if (!packet.header->pst_beam_polynomials_applied)
  {
    stats.no_valid_pst_beam();
  }

  PSTLOG_TRACE(this, "UDPFormat update_statistics - exit");
}
