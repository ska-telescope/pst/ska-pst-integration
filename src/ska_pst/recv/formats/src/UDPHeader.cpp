/*
 * Copyright 2023 Square Kilometre Array Observatory
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 * contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#include <stdexcept>

#include "ska_pst/common/utils/Logging.h"
#include "ska_pst/common/utils/AsciiHeader.h"
#include "ska_pst/common/definitions.h"
#include "ska_pst/recv/formats/UDPHeader.h"

ska::pst::recv::UDPHeader::UDPHeader() :
  header(INIT_CBF_PST_HEADER_T)
{
  configure_packet_defaults(&header);
}

void ska::pst::recv::UDPHeader::configure(const ska::pst::common::AsciiHeader& config, const ska::pst::recv::UDPFormat& format)
{
  auto bw = config.get<double>("BW");
  auto freq = config.get<double>("FREQ");
  auto tsamp = config.get<double>("TSAMP");
  auto nchan = config.get<uint32_t>("NCHAN");
  auto nbit = config.get<uint32_t>("NBIT");

  start_channel = config.get<uint32_t>("START_CHANNEL");
  end_channel = config.get<uint32_t>("END_CHANNEL");

  // the number of attoseconds that increment with each packet
  attoseconds_per_packet = static_cast<double>(rint(
      static_cast<double>(ska::pst::common::attoseconds_per_microsecond) *
      static_cast<double>(format.get_samples_per_packet()) * tsamp));
  nchan_per_packet = format.get_nchan_per_packet();
  packet_size = format.get_packet_size();
  increment_psn_every_packet = format.get_increment_psn_every_packet();

  // configure the defaults for the UDP packets
  header.sample_period_numerator = format.get_sample_period_numerator();
  header.sample_period_denominator = format.get_sample_period_denominator();
  header.first_channel_number = start_channel;
  header.scale_1 = 1.0f;
  header.scale_2 = 1.0f;
  header.scale_3 = 1.0f;
  header.scale_4 = 1.0f;
  header.offset_1 = 0.0f;
  header.offset_2 = 0.0f;
  header.offset_3 = 0.0f;
  header.offset_4 = 0.0f;
  header.time_samples_per_packet = static_cast<uint16_t>(format.get_samples_per_packet());
  header.channels_per_packet = static_cast<uint16_t>(format.get_nchan_per_packet());
  header.valid_channels_per_packet = static_cast<uint16_t>(nchan_per_packet);
  header.num_power_samples_averaged = static_cast<uint8_t>(0);
  header.num_time_samples_per_relative_weight = static_cast<uint8_t>(format.get_nsamp_per_weight());
  header.channel_separation = static_cast<uint32_t>(rint((freq / nchan) * ska::pst::common::millihertz_per_megahertz));
  header.first_channel_freq = static_cast<uint64_t>(rint((freq - (bw / 2)) * ska::pst::common::millihertz_per_megahertz));
  header.data_precision = static_cast<uint8_t>(nbit);
  header.station_beam_polynomials_applied = static_cast<uint8_t>(1);
  header.pst_beam_polynomials_applied = static_cast<uint8_t>(1);
  header.jones_polarisation_corrections_applied = static_cast<uint8_t>(1);
  header.reserved_1 = static_cast<uint8_t>(0);
  header.packet_destination = static_cast<uint8_t>(format.get_destination());
  header.beam_number = config.get<uint32_t>("BEAM_ID");
  configured = true;
}

void ska::pst::recv::UDPHeader::set_samples_since_ska_epoch(uint64_t samples_since_ska_epoch)
{
  header.samples_since_ska_epoch = samples_since_ska_epoch;
  PSTLOG_DEBUG(this, "UDPHeader set_samples_since_ska_epoch samples_since_ska_epoch={}", static_cast<uint64_t>(header.samples_since_ska_epoch));
}

void ska::pst::recv::UDPHeader::set_packet_sequence_number(uint64_t psn)
{
  PSTLOG_DEBUG(this, "UDPHeader set_packet_sequence_number({})", psn);
  header.packet_sequence_number = psn;
}

void ska::pst::recv::UDPHeader::set_scan_id(uint64_t scan_id)
{
  PSTLOG_DEBUG(this, "UDPHeader set_scan_id({})", scan_id);
  header.scan_id = scan_id;
}

void ska::pst::recv::UDPHeader::set_beam_id(uint64_t beam_id)
{
  header.beam_number = beam_id;
}

void ska::pst::recv::UDPHeader::set_validity_flag(ska::pst::recv::ValidityType type)
{
  switch (type)
  {
    case ska::pst::recv::ValidityType::AllValid:
      header.station_beam_polynomials_applied = 1;
      header.pst_beam_polynomials_applied = 1;
      header.jones_polarisation_corrections_applied = 1;
      return;

    case ska::pst::recv::ValidityType::StationBeamFlagInvalid:
      header.station_beam_polynomials_applied = 0;
      return;

    case ska::pst::recv::ValidityType::PstBeamFlagInvalid:
      header.pst_beam_polynomials_applied = 0;
      return;

    case ska::pst::recv::ValidityType::JonesMatricesFlagInvalid:
      header.jones_polarisation_corrections_applied = 0;
      return;
  }
}

void ska::pst::recv::UDPHeader::encode_header(char * buf)
{
  if (!configured)
  {
    PSTLOG_ERROR(this, "UDPHeader encode_header UDPHeader not configured");
    throw std::runtime_error("Cannot call encode_header if UDP Header not configured");
  }
  memcpy(static_cast<void *>(buf), static_cast<void *>(&header), sizeof(ska::pst::recv::cbf_psr_header_t)); // NOLINT
}

auto ska::pst::recv::UDPHeader::gen_packet(char * buf) -> size_t
{
  // write local header to the socket buffer
  encode_header(buf);

  // increment the packet counters
  increment_packet();

  return packet_size;
}

void ska::pst::recv::UDPHeader::increment_packet()
{
  // increment the first channel number
  if (increment_psn_every_packet)
  {
    header.packet_sequence_number++;
  }

  header.first_channel_number += nchan_per_packet;

  // if all channels have been encoded and sent
  if (header.first_channel_number >= end_channel)
  {
    header.first_channel_number = start_channel;

    if (!increment_psn_every_packet)
    {
      header.packet_sequence_number++;
    }

    header.samples_since_ska_epoch += static_cast<uint64_t>(header.time_samples_per_packet);
  }

  // ensure the packet final channel doesn't extend past the end_chan
  if (header.first_channel_number + header.channels_per_packet >= end_channel)
  {
    header.channels_per_packet = end_channel - header.first_channel_number;
  }
}

auto ska::pst::recv::UDPHeader::gen_packet_failure(char * buf, ska::pst::recv::FailureType failure_type) -> size_t
{
  cbf_psr_header_t tmp_header{};

  if (failure_type == BadTransmit)
  {
    PSTLOG_DEBUG(this, "UDPHeader gen_packet_failure increment_packet due to BadTransmit");
    increment_packet();
  }

  memcpy(static_cast<void *>(&tmp_header), static_cast<void *>(&header), sizeof(ska::pst::recv::cbf_psr_header_t)); // NOLINT

  size_t pkt_size = packet_size;

  switch (failure_type)
  {
    case BadMagicWord:
      // modify the magic_word in the header
      PSTLOG_DEBUG(this, "UDPHeader gen_packet_failure process BadMagicWord");
      header.magic_word = ska::pst::recv::magic_word + 1;
      break;

    case BadPacketSize:
      // return an incorrect packet size
      PSTLOG_DEBUG(this, "UDPHeader gen_packet_failure process BadPacketSize");
      pkt_size -= 1;
      break;

    case BadTransmit:
      // skip the transmission of a packet in the sequence, see above
      PSTLOG_DEBUG(this, "UDPHeader gen_packet_failure process BadTransmit");
      break;

    case MisorderedPacketSequenceNumber:
      PSTLOG_DEBUG(this, "UDPHeader gen_packet_failure process MisorderedPacketSequenceNumber");
      static constexpr uint64_t misordered_psn_offset = UDP_FORMAT_CBFPSR_FIRST_PSN;
      header.packet_sequence_number = misordered_psn_offset;
      break;

    case BadScanID:
      PSTLOG_DEBUG(this, "UDPHeader gen_packet_failure process BadScanID");
      header.scan_id += 1;
      break;

    case BadChannelNumber:
      PSTLOG_DEBUG(this, "UDPHeader gen_packet_failure process BadChannelNumber");
      static constexpr uint32_t bad_channel_offset = 1000000;
      header.first_channel_number += bad_channel_offset;
      break;

    case BadTimestamp:
      PSTLOG_DEBUG(this, "UDPHeader gen_packet_failure process BadTimestamp");
      static constexpr uint64_t bad_timestamp_offset = 1000000;
      header.samples_since_ska_epoch += bad_timestamp_offset;
      break;

    case BadDataRate:
      PSTLOG_DEBUG(this, "UDPHeader gen_packet_failure process BadBadDataRateTimestamp");
      break;

    default:
      break;
  }

  // encode the bad header
  encode_header(buf);

  // restore the original header
  memcpy(static_cast<void *>(&header), static_cast<void *>(&tmp_header), sizeof(ska::pst::recv::cbf_psr_header_t)); // NOLINT

  // increment the packet counters
  increment_packet();

  return pkt_size;
}
