/*
 * Copyright 2022 Square Kilometre Array Observatory
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 * contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#include <cstring>

#include "ska_pst/common/utils/Logging.h"
#include "ska_pst/recv/formats/PacketStructure.h"
#include "ska_pst/recv/formats/UDPFormat.h"

/**
 * @brief Configure default parameters for the UDP packets
 *
 * @param header
 */
void ska::pst::recv::configure_packet_defaults(cbf_psr_header_t * header)
{
  memset(header, 0, sizeof(cbf_psr_header_t));
  header->cbf_version_major = 0;
  header->cbf_version_minor = 1;
  header->packet_sequence_number = UDP_FORMAT_CBFPSR_FIRST_PSN;
  header->first_channel_number = 0;
  header->samples_since_ska_epoch = 0;
  header->sample_period_numerator = 1;
  header->sample_period_denominator = 1;
  header->station_beam_polynomials_applied = 1;
  header->pst_beam_polynomials_applied = 1;
  header->jones_polarisation_corrections_applied = 1;
  header->reserved_1 = 0;
  header->beam_number = 1;
  header->scan_id = 1;
  header->magic_word = ska::pst::recv::magic_word;
}

auto ska::pst::recv::validate_packet_header(cbf_psr_header_t * header) -> bool
{
  return (header->magic_word == ska::pst::recv::magic_word);
}

auto ska::pst::recv::get_scale1_byte_offset() -> size_t
{
  cbf_psr_header_t header;
  return uintptr_t(&(header.scale_1)) - uintptr_t(&(header)); // NOLINT
};

void ska::pst::recv::print_packet_header(cbf_psr_header_t * header)
{
  if (header == nullptr)
  {
    PSTLOG_WARN(nullptr, "print_packet_header header was not initialised");
    throw std::runtime_error("Cannot print packet header, it was nullptr");
  }

  PSTLOG_INFO(nullptr, "CBF version: {}.{}", uint32_t(header->cbf_version_major), uint32_t(header->cbf_version_minor));
  PSTLOG_INFO(nullptr, "Packet Sequence Number: {}", uint64_t(header->packet_sequence_number));
  PSTLOG_INFO(nullptr, "First Channel Number: {}", uint32_t(header->first_channel_number));
  PSTLOG_INFO(nullptr, "First Channel Freq: {} millihertz", uint64_t(header->first_channel_freq));
  PSTLOG_INFO(nullptr, "Samples since SKA Epoch: {}", uint64_t(header->samples_since_ska_epoch));
  PSTLOG_INFO(nullptr, "Sample period (microseconds): {}/{}", uint16_t(header->sample_period_numerator), uint64_t(header->sample_period_denominator));
  PSTLOG_INFO(nullptr, "Channels per packet: number={} valid={}", uint32_t(header->channels_per_packet), uint64_t(header->valid_channels_per_packet));
  PSTLOG_INFO(nullptr, "Time samples: per packet={} per relative weight={}", uint32_t(header->time_samples_per_packet), uint32_t(header->num_time_samples_per_relative_weight));
  PSTLOG_INFO(nullptr, "Beam Number: {}", uint32_t(header->beam_number));
  PSTLOG_INFO(nullptr, "Packet Destination: {}", uint32_t(header->packet_destination));
  PSTLOG_INFO(nullptr, "Data Precision: {} bits/value", uint32_t(header->data_precision));
  PSTLOG_INFO(nullptr, "Station beam polynomials applied: {}", uint8_t(header->station_beam_polynomials_applied));
  PSTLOG_INFO(nullptr, "PST beam polynomials applied: {}", uint8_t(header->pst_beam_polynomials_applied));
  PSTLOG_INFO(nullptr, "Jones polarisation corrections applied: {}", uint8_t(header->jones_polarisation_corrections_applied));
  PSTLOG_INFO(nullptr, "Scan ID: {}", uint32_t(header->scan_id));
  PSTLOG_INFO(nullptr, "Scales: {} {} {} {}", float(header->scale_1), float(header->scale_2), float(header->scale_3), float(header->scale_4));
  PSTLOG_INFO(nullptr, "Offsets: {} {} {} {}", float(header->offset_1), float(header->offset_2), float(header->offset_3), float(header->offset_4));
}

auto ska::pst::recv::packet_weights_valid_for_policy(cbf_psr_header_t * header, PacketValidityFlagsPolicy policy) -> bool
{
  const uint8_t valid = 1;
  switch (policy)
  {
    case PacketValidityFlagsPolicy::IgnoreAll:
      return true;

    case PacketValidityFlagsPolicy::StationBeamDelayPolynomials:
      return header->station_beam_polynomials_applied == valid;

    case PacketValidityFlagsPolicy::PSTBeamDelayPolynomials:
      return header->pst_beam_polynomials_applied == valid;

    case PacketValidityFlagsPolicy::JonesMatrices:
      return header->jones_polarisation_corrections_applied == valid;

    case PacketValidityFlagsPolicy::AnyDelayPolynomials:
      return header->station_beam_polynomials_applied == valid && header->pst_beam_polynomials_applied == valid;

    case PacketValidityFlagsPolicy::All:
      return
        header->station_beam_polynomials_applied == valid &&
        header->pst_beam_polynomials_applied == valid &&
        header->jones_polarisation_corrections_applied == valid;

    default:
      throw std::runtime_error("packet_weights_valid_for_policy unknown PacketValidityFlagsPolicy");
  }
}
