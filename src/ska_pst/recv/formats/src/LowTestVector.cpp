/*
 * Copyright 2022 Square Kilometre Array Observatory
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 * contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#include <cmath>
#include <stdexcept>

#include "ska_pst/common/utils/Logging.h"
#include "ska_pst/common/definitions.h"
#include "ska_pst/recv/formats/LowTestVector.h"

static constexpr unsigned UDP_FORMAT_CBFPSR_NBIT = 16;
static constexpr unsigned UDP_FORMAT_CBFPSR_PACKET_NCHAN = 24;
static constexpr unsigned UDP_FORMAT_CBFPSR_PACKET_NSAMP = 32;
static constexpr unsigned UDP_FORMAT_CBFPSR_NSAMP_PER_WEIGHT = 32;
static constexpr unsigned UDP_FORMAT_CBFPSR_OS_NUMERATOR = 4;
static constexpr unsigned UDP_FORMAT_CBFPSR_OS_DENOMINATOR = 3;
static constexpr uint16_t UDP_FORMAT_CBFPSR_SAMPLE_PERIOD_NUMERATOR = 5184;
static constexpr uint16_t UDP_FORMAT_CBFPSR_SAMPLE_PERIOD_DENOMINATOR = 25;

ska::pst::recv::LowTestVector::LowTestVector()
{
  nsamp_per_packet = UDP_FORMAT_CBFPSR_PACKET_NSAMP;
  nchan_per_packet = UDP_FORMAT_CBFPSR_PACKET_NCHAN;
  nsamp_per_weight = UDP_FORMAT_CBFPSR_NSAMP_PER_WEIGHT;
  destination = UDP_FORMAT_CBFPSR_LOW_PST;
  increment_psn_every_packet = true;
  sample_period_numerator = UDP_FORMAT_CBFPSR_SAMPLE_PERIOD_NUMERATOR;
  sample_period_denominator = UDP_FORMAT_CBFPSR_SAMPLE_PERIOD_DENOMINATOR;
}

void ska::pst::recv::LowTestVector::configure_beam(const ska::pst::common::AsciiHeader& beam_config)
{
  ska::pst::recv::UDPFormat::configure_beam(beam_config);

  if (nbit != UDP_FORMAT_CBFPSR_NBIT)
  {
    PSTLOG_ERROR(this, "LowTestVector configure_beam config NBIT={} expected={}", nbit, UDP_FORMAT_CBFPSR_NBIT);
    throw std::runtime_error("LowTestVector configure_beam invalid config");
  }

  // override the psn_stride to be a single packet
  data_psn_stride = nchan_per_packet * data_channel_stride;
  weights_psn_stride = nchan_per_packet * weights_channel_stride;
  data_channel_stride = 0;
  weights_channel_stride = 0;
}

auto ska::pst::recv::LowTestVector::decode_packet(char *buf, data_and_weights_t * offsets, data_and_weights_t * received) -> PacketState
{
  #ifdef DEBUG
  PSTLOG_TRACE(this, "LowTestVector decode_packet decode_packet({})", reinterpret_cast<void *>(buf));
  #endif

  // use the parent class decode method to set the weights and data pointer
  PacketState packet_state = ska::pst::recv::UDPFormat::decode_packet(buf, offsets, received);
  #ifdef DEBUG
  PSTLOG_TRACE(this, "LowTestVector decode_packet offsets={}, {}", offsets->data, offsets->weights);
  #endif

  if (packet_state == Ok)
  {
    const int64_t psn = static_cast<int64_t>(packet.header->packet_sequence_number) - start_psn;

    // compute the byte offset for the packet
    offsets->data = psn * data_psn_stride;
    offsets->weights = (psn * weights_psn_stride) + (psn * packet_scales_size);
  }

  return packet_state;
}

auto ska::pst::recv::LowTestVector::get_samples_per_packet() -> unsigned
{
  return UDP_FORMAT_CBFPSR_PACKET_NSAMP;
}

auto ska::pst::recv::LowTestVector::get_expected_nbit()  -> unsigned
{
  return UDP_FORMAT_CBFPSR_NBIT;
}

auto ska::pst::recv::LowTestVector::get_expected_os_factor() -> std::pair<unsigned, unsigned>
{
  return std::make_pair(UDP_FORMAT_CBFPSR_OS_NUMERATOR, UDP_FORMAT_CBFPSR_OS_DENOMINATOR);
}
