/*
 * Copyright 2022 Square Kilometre Array Observatory
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 * contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#include <stdexcept>
#include <algorithm>
#include <sstream>
#include <numeric>

#include "ska_pst/recv/formats/UDPFormatFactory.h"

auto ska::pst::recv::get_supported_formats() -> std::vector<std::string>
{
  std::vector<std::string> supported;
  supported.emplace_back("LowTestVector");
  supported.emplace_back("LowPST");
  supported.emplace_back("MidPSTBand1");
  supported.emplace_back("MidPSTBand2");
  supported.emplace_back("MidPSTBand3");
  supported.emplace_back("MidPSTBand4");
  supported.emplace_back("MidPSTBand5");
  return supported;
}

auto ska::pst::recv::get_supported_formats_list() -> std::string
{
  std::vector<std::string> supported = ska::pst::recv::get_supported_formats();
  std::string delim = ", ";
  return std::accumulate(supported.begin() + 1, supported.end(), supported[0],
    [&delim](const std::string& x, const std::string& y) {
      return x + delim + y;
    }
  );
}

auto ska::pst::recv::is_format_supported(const std::string &format) -> bool
{
  auto supported_formats = get_supported_formats();
  return std::find(supported_formats.begin(), supported_formats.end(), format) != supported_formats.end();
}

auto ska::pst::recv::UDPFormatFactory(const std::string &format_name) -> std::shared_ptr<ska::pst::recv::UDPFormat>
{
  if (format_name == "LowTestVector") {
    return std::shared_ptr<ska::pst::recv::UDPFormat>(new ska::pst::recv::LowTestVector());
  } else if (format_name == "LowPST") {
    return std::shared_ptr<ska::pst::recv::UDPFormat>(new  ska::pst::recv::LowPST());
  } else if (format_name == "MidPSTBand1") {
    return std::shared_ptr<ska::pst::recv::UDPFormat>(new ska::pst::recv::MidPSTBand1());
  } else if (format_name == "MidPSTBand2") {
    return std::shared_ptr<ska::pst::recv::UDPFormat>(new ska::pst::recv::MidPSTBand2());
  } else if (format_name == "MidPSTBand3") {
    return std::shared_ptr<ska::pst::recv::UDPFormat>(new ska::pst::recv::MidPSTBand3());
  } else if (format_name == "MidPSTBand4") {
    return std::shared_ptr<ska::pst::recv::UDPFormat>(new ska::pst::recv::MidPSTBand4());
  } else if (format_name == "MidPSTBand5") {
    return std::shared_ptr<ska::pst::recv::UDPFormat>(new ska::pst::recv::MidPSTBand5());
  } else {
    throw std::runtime_error("UDPFormatFactory unrecognized format_name");
  }
}
