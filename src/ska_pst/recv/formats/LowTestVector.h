/*
 * Copyright 2022 Square Kilometre Array Observatory
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 * contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#include "ska_pst/recv/formats/UDPFormat.h"

#ifndef SKA_PST_RECV_FORMATS_LowTestVector_h
#define SKA_PST_RECV_FORMATS_LowTestVector_h

namespace ska::pst::recv {

  /**
   * @brief UDPFormat for the CBF/PSR configuration used for testing the Low.CBF to Low.PST interface in the Low PSI
   * Overrides the decode_packet and prepare methods to customize the processing of the format
   *
   */
  class LowTestVector : public UDPFormat
  {
    public:

      /**
       * @brief Construct a new LowTestVector object
       *
       */
      LowTestVector();

      /**
       * @brief Destroy the LowTestVector object
       *
       */
      ~LowTestVector() = default;

      /**
       * @brief configure the format using the provided beam configuration
       *
       * @param beam_config fixed configuration parameters common to all observations
       */
      void configure_beam(const ska::pst::common::AsciiHeader& beam_config) override;

      /**
       * @brief Decode the received UDP packet
       *
       * @param buffer pointer to buffer containing received UDP packet payload
       * @param offsets offsets of the data and weights in their respective data streams
       * @param received size of the data and weights portions of the payload
       * @return PacketState status of the received packet (e.g. OK, IGNORE, MALFORMED)
       */
      PacketState decode_packet(char *buffer, data_and_weights_t* offsets, data_and_weights_t * received) override;

      /**
       * @brief Get the expected NBIT value for UDPFormat
       *
       * For LowTestVector expected NBIT = 16.
       *
       * @return the expected NBIT value for the format.
       */
      unsigned get_expected_nbit() override;

      /**
       * @brief Get the expected OS_FACTOR for the UDPFormat
       *
       * For LowTestVector expected OS_FACTOR = 4/3.
       *
       * @return a pair of unsigned values.  The first is the numerator and the
       *      second is the denominator.
       */
      std::pair<unsigned, unsigned> get_expected_os_factor() override;

      /**
       * @brief Return the number of time samples per UDP packet
       *
       * @return unsigned number of time samples per UDP packet
       */
      static unsigned get_samples_per_packet();

    private:

      //! packet sequence number of the previous packet
      int64_t prev_psn{-1};
  };

} // namespace ska::pst::recv

#endif // SKA_PST_RECV_FORMATS_LowTestVector_h
