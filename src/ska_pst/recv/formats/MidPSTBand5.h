/*
 * Copyright 2022 Square Kilometre Array Observatory
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 * contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#include "ska_pst/recv/formats/MidPST.h"

#ifndef SKA_PST_RECV_FORMATS_MidPSTBand5_h
#define SKA_PST_RECV_FORMATS_MidPSTBand5_h

namespace ska::pst::recv {

  /**
   * @brief Concrete UDPFormat class for the CBF/PSR configuration used for the Mid.CBF to Mid.PST interface for Band5
   *
   */
  class MidPSTBand5 : public MidPST {

    public:

      /**
       * @brief Construct a new MidPSTBand5 object
       *
       */
      MidPSTBand5();

      /**
       * @brief Destroy the MidPSTBand5 object
       *
       */
      ~MidPSTBand5() = default;

      /**
       * @brief Get the expected NBIT value for UDPFormat
       *
       * For MidPSTBand5 expected NBIT = 18.
       *
       * @return the expected NBIT value for the format.
       */
      unsigned get_expected_nbit() override;
  };

} // namespace ska::pst::recv

#endif // SKA_PST_RECV_FORMATS_MidPSTBand5_h
