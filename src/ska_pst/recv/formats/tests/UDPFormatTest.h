/*
 * Copyright 2022 Square Kilometre Array Observatory
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 * contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#include <gtest/gtest.h>
#include <memory>
#include <string>
#include <utility>
#include <vector>

#include "ska_pst/recv/formats/UDPFormat.h"
#include "ska_pst/recv/formats/UDPHeader.h"

#ifndef SKA_PST_RECV_FORMATS_TESTS_UDPFormatTest_h
#define SKA_PST_RECV_FORMATS_TESTS_UDPFormatTest_h

namespace ska::pst::recv::test {

class ConcreteUDPFormat : public UDPFormat
{
  public:

    ConcreteUDPFormat()
    {
      nsamp_per_packet = 32;
      nchan_per_packet = 32;
      nsamp_per_weight = 32;
      destination = 4;
      sample_period_numerator = 3125;
      sample_period_denominator = 192;
    }

    unsigned get_nsamp_per_packet() const { return nsamp_per_packet; };
    void set_nsamp_per_packet(unsigned val) { nsamp_per_packet = val; };

    unsigned get_nchan_per_packet() const { return nchan_per_packet; };
    void set_nchan_per_packet(unsigned val) { nchan_per_packet = val; };

    unsigned get_nsamp_per_weight() const { return nsamp_per_weight; };
    void set_nsamp_per_weight(unsigned val) { nsamp_per_weight = val; };

    unsigned get_expected_nbit() override { return 16; }
    std::pair<unsigned, unsigned> get_expected_os_factor() override { return std::make_pair(8, 7); }

};

/**
 * @brief Test the UDPFormat class
 *
 * @details
 *
 */
class UDPFormatTest : public ::testing::TestWithParam<ska::pst::recv::PacketValidityFlagsPolicy>
{
  protected:
    void SetUp() override;
    void TearDown() override;

    unsigned compute_data_resolution(unsigned nchan, unsigned npol, unsigned ndim, unsigned nbit, const ConcreteUDPFormat& format);
    unsigned compute_weights_resolution(unsigned nchan, const ConcreteUDPFormat& format);

    void prepare_packet(std::shared_ptr<ConcreteUDPFormat>& format, std::shared_ptr<ska::pst::recv::UDPHeader>& udp_header, std::vector<char>& packet_data);

    // prepare a range of different combinations of invalid headers
    std::vector<ska::pst::recv::cbf_psr_header_t> headers_with_flags;

  public:
    UDPFormatTest();

    ~UDPFormatTest() = default;

    ska::pst::common::AsciiHeader config;

    ska::pst::common::AsciiHeader header;

  private:

};

} // namespace ska::pst::recv::test

#endif // SKA_PST_RECV_FORMATS_TESTS_UDPFormatTest_h
