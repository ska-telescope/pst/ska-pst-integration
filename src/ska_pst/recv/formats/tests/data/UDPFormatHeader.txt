HDR_SIZE            4096
HDR_VERSION         1.0

# Required Parameters, UDPFormatTest 
NCHAN               768
NBIT                16
NPOL                2
NDIM                2
OS_FACTOR           8/7
TSAMP               100
FREQ                100
BW                  100
START_CHANNEL       0
END_CHANNEL         768

# Additional Parameters, not required
DATA_HOST           127.0.0.1
DATA_PORT           12345
CALFREQ             11.1111111111
OBS_OFFSET          0
UTC_START           2022-01-01-00:00:00