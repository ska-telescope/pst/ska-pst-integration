HDR_SIZE            4096
HDR_VERSION         1.0

# Required Parameters, MidPSTBand2 sub-band 1
NCHAN               3700
NBIT                16
NPOL                2
NDIM                2
OS_FACTOR           8/7
TSAMP               16.276041667
FREQ                1049.456
BW                  198.912
START_CHANNEL       0
END_CHANNEL         3700

# Additional Parameters, not required
DATA_HOST           127.0.0.1
DATA_PORT           12345
CALFREQ             11.1111111111
OBS_OFFSET          0
UTC_START           2017-08-01-15:53:29
BEAM_ID             1