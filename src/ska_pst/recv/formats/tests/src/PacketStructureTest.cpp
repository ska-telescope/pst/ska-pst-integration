/*
 * Copyright 2024 Square Kilometre Array Observatory
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 * contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#include "ska_pst/common/utils/Logging.h"

#include "ska_pst/recv/formats/tests/PacketStructureTest.h"
#include "ska_pst/recv/formats/PacketStructure.h"
#include "ska_pst/common/testutils/GtestMain.h"

auto main(int argc, char* argv[]) -> int
{
  spdlog::set_level(spdlog::level::debug);
  return ska::pst::common::test::gtest_main(argc, argv);
}

namespace ska::pst::recv::test {

PacketStructureTest::PacketStructureTest()
    : ::testing::Test()
{
}

void PacketStructureTest::SetUp()
{
  ska::pst::recv::configure_packet_defaults(&header);
}

void PacketStructureTest::TearDown()
{
}

TEST_F(PacketStructureTest, test_validate_packet_header) // NOLINT
{
  // test that the packet defaults has the correct magic word field
  ASSERT_TRUE(validate_packet_header(&header));
  header.magic_word += 1;

  // test that validate fails when the magic word is wrong
  ASSERT_FALSE(validate_packet_header(&header));
}

TEST_F(PacketStructureTest, test_scale1_byte_offset) // NOLINT
{
  // the CBFPSR header positions the offset of the scale1 parameter in the header struct at byte 32
  static constexpr size_t expected_scale1_byte_offset = 32;
  ASSERT_EQ(get_scale1_byte_offset(), expected_scale1_byte_offset);
}

TEST_F(PacketStructureTest, test_print_packet_header) // NOLINT
{
  // test the default packet header can be printed
  ASSERT_NO_THROW(print_packet_header(&header));

  // test that an empty packet header throws an exception
  cbf_psr_header_t * null_header = nullptr;
  ASSERT_THROW(print_packet_header(null_header), std::runtime_error);
}

TEST_F(PacketStructureTest, test_packet_weights_valid_for_policy) // NOLINT
{
  // prepare a range of different combinations of invalid headers
  cbf_psr_header_t invalid_station_beam{};
  cbf_psr_header_t invalid_pst_beam{};
  cbf_psr_header_t invalid_station_and_pst_beam{};
  cbf_psr_header_t invalid_jones_matrices{};
  cbf_psr_header_t invalid_station_and_pst_beam_and_jones_matrices{};

  // configure default parameters for each header
  configure_packet_defaults(&invalid_station_beam);
  configure_packet_defaults(&invalid_pst_beam);
  configure_packet_defaults(&invalid_station_and_pst_beam);
  configure_packet_defaults(&invalid_jones_matrices);
  configure_packet_defaults(&invalid_station_and_pst_beam_and_jones_matrices);

  invalid_station_beam.station_beam_polynomials_applied = 0;
  invalid_pst_beam.pst_beam_polynomials_applied = 0;
  invalid_station_and_pst_beam.station_beam_polynomials_applied = 0;
  invalid_station_and_pst_beam.pst_beam_polynomials_applied = 0;
  invalid_jones_matrices.jones_polarisation_corrections_applied = 0;
  invalid_station_and_pst_beam_and_jones_matrices.station_beam_polynomials_applied = 0;
  invalid_station_and_pst_beam_and_jones_matrices.pst_beam_polynomials_applied = 0;
  invalid_station_and_pst_beam_and_jones_matrices.jones_polarisation_corrections_applied = 0;

  // test the IgnoreAll policy
  ska::pst::recv::PacketValidityFlagsPolicy policy = PacketValidityFlagsPolicy::IgnoreAll;
  ASSERT_TRUE(ska::pst::recv::packet_weights_valid_for_policy(&invalid_station_beam, policy));
  ASSERT_TRUE(ska::pst::recv::packet_weights_valid_for_policy(&invalid_pst_beam, policy));
  ASSERT_TRUE(ska::pst::recv::packet_weights_valid_for_policy(&invalid_station_and_pst_beam, policy));
  ASSERT_TRUE(ska::pst::recv::packet_weights_valid_for_policy(&invalid_jones_matrices, policy));
  ASSERT_TRUE(ska::pst::recv::packet_weights_valid_for_policy(&invalid_station_and_pst_beam_and_jones_matrices, policy));

  // test the StationBeamDelayPolynomials policy
  policy = PacketValidityFlagsPolicy::StationBeamDelayPolynomials;
  ASSERT_FALSE(ska::pst::recv::packet_weights_valid_for_policy(&invalid_station_beam, policy));
  ASSERT_TRUE(ska::pst::recv::packet_weights_valid_for_policy(&invalid_pst_beam, policy));
  ASSERT_FALSE(ska::pst::recv::packet_weights_valid_for_policy(&invalid_station_and_pst_beam, policy));
  ASSERT_TRUE(ska::pst::recv::packet_weights_valid_for_policy(&invalid_jones_matrices, policy));
  ASSERT_FALSE(ska::pst::recv::packet_weights_valid_for_policy(&invalid_station_and_pst_beam_and_jones_matrices, policy));

  // test the PSTBeamDelayPolynomials policy
  policy = PacketValidityFlagsPolicy::PSTBeamDelayPolynomials;
  ASSERT_TRUE(ska::pst::recv::packet_weights_valid_for_policy(&invalid_station_beam, policy));
  ASSERT_FALSE(ska::pst::recv::packet_weights_valid_for_policy(&invalid_pst_beam, policy));
  ASSERT_FALSE(ska::pst::recv::packet_weights_valid_for_policy(&invalid_station_and_pst_beam, policy));
  ASSERT_TRUE(ska::pst::recv::packet_weights_valid_for_policy(&invalid_jones_matrices, policy));
  ASSERT_FALSE(ska::pst::recv::packet_weights_valid_for_policy(&invalid_station_and_pst_beam_and_jones_matrices, policy));

  // test the JonesMatrices policy
  policy = PacketValidityFlagsPolicy::JonesMatrices;
  ASSERT_TRUE(ska::pst::recv::packet_weights_valid_for_policy(&invalid_station_beam, policy));
  ASSERT_TRUE(ska::pst::recv::packet_weights_valid_for_policy(&invalid_pst_beam, policy));
  ASSERT_TRUE(ska::pst::recv::packet_weights_valid_for_policy(&invalid_station_and_pst_beam, policy));
  ASSERT_FALSE(ska::pst::recv::packet_weights_valid_for_policy(&invalid_jones_matrices, policy));
  ASSERT_FALSE(ska::pst::recv::packet_weights_valid_for_policy(&invalid_station_and_pst_beam_and_jones_matrices, policy));

  // test the All policy
  policy = PacketValidityFlagsPolicy::All;
  ASSERT_FALSE(ska::pst::recv::packet_weights_valid_for_policy(&invalid_station_beam, policy));
  ASSERT_FALSE(ska::pst::recv::packet_weights_valid_for_policy(&invalid_pst_beam, policy));
  ASSERT_FALSE(ska::pst::recv::packet_weights_valid_for_policy(&invalid_station_and_pst_beam, policy));
  ASSERT_FALSE(ska::pst::recv::packet_weights_valid_for_policy(&invalid_jones_matrices, policy));
  ASSERT_FALSE(ska::pst::recv::packet_weights_valid_for_policy(&invalid_station_and_pst_beam_and_jones_matrices, policy));
}

} // namespace ska::pst::recv::test
