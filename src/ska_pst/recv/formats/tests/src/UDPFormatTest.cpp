/*
 * Copyright 2022 Square Kilometre Array Observatory
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 * contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#include "ska_pst/common/utils/Logging.h"
#include "ska_pst/common/utils/ValidationContext.h"
#include "ska_pst/common/utils/GaussianNoiseGenerator.h"
#include "ska_pst/recv/formats/tests/UDPFormatTest.h"
#include "ska_pst/recv/formats/UDPHeader.h"
#include "ska_pst/common/testutils/GtestMain.h"

using ska::pst::common::test::test_data_file;

auto main(int argc, char* argv[]) -> int
{
  return ska::pst::common::test::gtest_main(argc, argv);
}

namespace ska::pst::recv::test {

UDPFormatTest::UDPFormatTest()
    : ::testing::TestWithParam<ska::pst::recv::PacketValidityFlagsPolicy>()
{
}

void UDPFormatTest::SetUp()
{
  config.load_from_file(test_data_file("UDPFormatConfig.txt"));
  header.load_from_file(test_data_file("UDPFormatHeader.txt"));
}

void UDPFormatTest::TearDown()
{
  config.reset();
  header.reset();
}

auto UDPFormatTest::compute_data_resolution(unsigned nchan, unsigned npol, unsigned ndim, unsigned nbit, const ConcreteUDPFormat& format) -> unsigned
{
  unsigned resolution = nchan * npol * ndim * nbit * format.get_samples_per_packet() / ska::pst::common::bits_per_byte;
  return resolution;
}

auto UDPFormatTest::compute_weights_resolution(unsigned nchan, const ConcreteUDPFormat& format) -> unsigned
{
  static constexpr unsigned ndim = 1;
  static constexpr unsigned npol = 1;
  static constexpr unsigned nbit = UDP_FORMAT_CBFPSR_WEIGHT_NBIT;
  unsigned scale_factor_resolution = (nchan / format.get_nchan_per_packet()) * sizeof(float);
  unsigned weights_data_resolution = (nchan * npol * ndim * nbit * format.get_samples_per_packet()) / format.get_nsamp_per_weight() / ska::pst::common::bits_per_byte;
  return scale_factor_resolution + weights_data_resolution;
}

void UDPFormatTest::prepare_packet(std::shared_ptr<ConcreteUDPFormat>& format, std::shared_ptr<ska::pst::recv::UDPHeader>& udp_header, std::vector<char>& packet_data)
{
  // allocate storage for the packet to use in the tests
  format->configure_beam(config);
  format->configure_scan(header);
  udp_header->configure(config, *format.get());

  static constexpr uint64_t scan_id = 12345;
  format->set_scan_id(scan_id);
  udp_header->set_scan_id(scan_id);

  packet_data.resize(format->get_packet_size());

  auto psr_header_ptr = reinterpret_cast<char *>(&packet_data[0]);
  PSTLOG_TRACE(this, "UDPFormatTest prepare_packet psr_header_ptr={}", reinterpret_cast<void*>(psr_header_ptr));
  udp_header->gen_packet(psr_header_ptr);

  // use the ScaleWeightGenerator to generate a the scales and weights with maximum value of 0xFFFF or 65535 for the weights
  ska::pst::common::GaussianNoiseGenerator generator(format);
  generator.configure(config);
  generator.fill_packet(&packet_data[0]);

  // decode the packet to check the weights of the default header
  ASSERT_EQ(format->process_stream_start(psr_header_ptr), ska::pst::recv::UDPFormat::PacketState::Ok);

  // prepare a range of different headers with different validity flags set
  static constexpr size_t headers_to_test = 6;
  headers_with_flags.resize(headers_to_test);
  for (auto & header : headers_with_flags)
  {
    memcpy(&header, psr_header_ptr, sizeof(ska::pst::recv::cbf_psr_header_t));
  }
  // set the custom flags in each of the invalid headers
  headers_with_flags[1].station_beam_polynomials_applied = 0;       // NOLINT
  headers_with_flags[2].pst_beam_polynomials_applied = 0;           // NOLINT
  headers_with_flags[3].station_beam_polynomials_applied = 0;       // NOLINT
  headers_with_flags[3].pst_beam_polynomials_applied = 0;           // NOLINT
  headers_with_flags[4].jones_polarisation_corrections_applied = 0; // NOLINT
  headers_with_flags[5].station_beam_polynomials_applied = 0;       // NOLINT
  headers_with_flags[5].pst_beam_polynomials_applied = 0;           // NOLINT
  headers_with_flags[5].jones_polarisation_corrections_applied = 0; // NOLINT
}

TEST_F(UDPFormatTest, test_default_constructor) // NOLINT
{
  ConcreteUDPFormat format;
}

TEST_F(UDPFormatTest, test_configure) // NOLINT
{
  ConcreteUDPFormat format;
  format.configure_beam(config);
}

TEST_F(UDPFormatTest, test_configure_with_bad_config)  // NOLINT
{
  ConcreteUDPFormat format;

  unsigned nsamp_per_weight = format.get_nsamp_per_weight();
  unsigned nchan_per_packet = format.get_nchan_per_packet();
  unsigned nsamp_per_packet = format.get_nsamp_per_packet();

  format.set_nsamp_per_weight(0);
  EXPECT_THROW(format.configure_beam(config), std::runtime_error); // NOLINT
  format.set_nsamp_per_weight(nsamp_per_weight);

  format.set_nchan_per_packet(0);
  EXPECT_THROW(format.configure_beam(config), std::runtime_error); // NOLINT
  format.set_nchan_per_packet(nchan_per_packet);

  format.set_nsamp_per_packet(0);
  EXPECT_THROW(format.configure_beam(config), std::runtime_error); // NOLINT
  format.set_nsamp_per_packet(nsamp_per_packet);

  static constexpr unsigned bad_nchan_per_packet = 123453453;
  format.set_nchan_per_packet(bad_nchan_per_packet);
  EXPECT_THROW(format.configure_beam(config), ska::pst::common::pst_validation_error); // NOLINT
  format.set_nchan_per_packet(nchan_per_packet);

  format.reset();

  ska::pst::common::AsciiHeader bad_config;
  bad_config.clone(config);

  // test that beam-ids between 1-16 are only valid
  static constexpr uint32_t too_low_beam_id = 0;
  static constexpr uint32_t too_high_beam_id = 17;
  bad_config.set("BEAM_ID", too_low_beam_id);
  EXPECT_THROW(format.configure_beam(bad_config), ska::pst::common::pst_validation_error); // NOLINT
  bad_config.set("BEAM_ID", too_high_beam_id);
  EXPECT_THROW(format.configure_beam(bad_config), ska::pst::common::pst_validation_error); // NOLINT
  for (unsigned beam_id=UDP_FORMAT_MIN_BEAM; beam_id<=UDP_FORMAT_MAX_BEAM; beam_id++)
  {
    bad_config.set("BEAM_ID", beam_id);
    EXPECT_NO_THROW(format.configure_beam(bad_config)); // NOLINT
    format.reset();
  }

  bad_config.clone(config);
  bad_config.set_val("NPOL", "3");
  EXPECT_THROW(format.configure_beam(bad_config), ska::pst::common::pst_validation_error); // NOLINT

  bad_config.clone(config);
  bad_config.set_val("NDIM", "3");
  EXPECT_THROW(format.configure_beam(bad_config), ska::pst::common::pst_validation_error); // NOLINT

  // over-ride nchan from 1024 (in config) to 64
  bad_config.clone(config);
  bad_config.set_val("NCHAN", "64");
  EXPECT_THROW(format.configure_beam(bad_config), ska::pst::common::pst_validation_error); // NOLINT
}

TEST_F(UDPFormatTest, test_prepare) // NOLINT
{
  ConcreteUDPFormat format;
  EXPECT_THROW(format.configure_scan(header), std::runtime_error); // NOLINT

  format.configure_beam(config);
  format.configure_scan(header);
}

TEST_F(UDPFormatTest, test_get_packet_data_size) // NOLINT
{
  ConcreteUDPFormat format;
  format.configure_beam(config);
  ASSERT_EQ(format.get_packet_data_size(), 8192);
}

TEST_F(UDPFormatTest, test_get_packet_size) // NOLINT
{
  ConcreteUDPFormat format;
  format.configure_beam(config);
  ASSERT_EQ(format.get_packet_size(), 8352);
}

TEST_F(UDPFormatTest, test_get_samples_per_packet) // NOLINT
{
  ConcreteUDPFormat format;
  format.configure_beam(config);
  ASSERT_EQ(format.get_samples_per_packet(), 32);
}

TEST_F(UDPFormatTest, test_weights_scan_config) // NOLINT
{
  ConcreteUDPFormat format;
  format.configure_beam(config);
  format.configure_scan(header);

  ska::pst::common::AsciiHeader weights_scan_config = format.get_weights_scan_config();

  auto nchan = weights_scan_config.get<uint32_t>("NCHAN");
  auto npol = weights_scan_config.get<uint32_t>("NPOL");
  auto ndim = weights_scan_config.get<uint32_t>("NDIM");
  auto nbit = weights_scan_config.get<uint32_t>("NBIT");
  auto tsamp = weights_scan_config.get<double>("TSAMP");

  ASSERT_EQ(nchan, header.get<uint32_t>("NCHAN"));
  ASSERT_EQ(npol, 1);
  ASSERT_EQ(ndim, 1);
  ASSERT_EQ(nbit, UDP_FORMAT_CBFPSR_WEIGHT_NBIT);

  auto tsamp_sec = tsamp / ska::pst::common::microseconds_per_second;
  double bytes_per_second = (nchan * nbit) / (ska::pst::common::bits_per_byte * tsamp_sec);
  auto nscales = nchan / format.get_nchan_per_packet();
  auto nscales_nbit = sizeof(float) * ska::pst::common::bits_per_byte;
  double scales_overhead = 1.0 + static_cast<double>(nscales * nscales_nbit) / static_cast<double>(nchan * nbit);
  bytes_per_second *= scales_overhead;
  ASSERT_NEAR(bytes_per_second, weights_scan_config.compute_bytes_per_second(), 1e-3); // NOLINT
}

TEST_F(UDPFormatTest, test_get_resolution) // NOLINT
{
  ConcreteUDPFormat format;
  EXPECT_THROW(format.get_resolution(), std::runtime_error); // NOLINT
  format.configure_beam(config);

  // compute the expected resolution
  auto nchan = config.get<uint32_t>("NCHAN");
  auto npol = config.get<uint32_t>("NPOL");
  auto ndim = config.get<uint32_t>("NDIM");
  auto nbit = config.get<uint32_t>("NBIT");
  unsigned resolution = compute_data_resolution(nchan, npol, ndim, nbit, format);
  ASSERT_EQ(format.get_resolution(), resolution);
}

TEST_F(UDPFormatTest, test_get_weights_resolution) // NOLINT
{
  ConcreteUDPFormat format;
  EXPECT_THROW(format.get_weights_resolution(), std::runtime_error); // NOLINT
  format.configure_beam(config);

  auto nchan = config.get<uint32_t>("NCHAN");
  unsigned resolution = compute_weights_resolution(nchan, format);

  ASSERT_EQ(format.get_weights_resolution(), resolution);
}

TEST_F(UDPFormatTest, test_get_samples_for_bytes) // NOLINT
{
  ConcreteUDPFormat format;
  format.configure_beam(config);
  auto nchan = config.get<uint32_t>("NCHAN");
  auto npol = config.get<uint32_t>("NPOL");
  auto ndim = config.get<uint32_t>("NDIM");
  auto nbit = config.get<uint32_t>("NBIT");
  unsigned resolution = compute_data_resolution(nchan, npol, ndim, nbit, format);

  ASSERT_EQ(format.get_samples_for_bytes(resolution-1), format.get_nsamp_per_packet()-1);
  ASSERT_EQ(format.get_samples_for_bytes(resolution), format.get_nsamp_per_packet());
  ASSERT_EQ(format.get_samples_for_bytes(resolution+1), format.get_nsamp_per_packet());
}

TEST_F(UDPFormatTest, test_decode_packet) // NOLINT
{
  ConcreteUDPFormat format;
  UDPHeader udp_header;

  // local header with which to test
  cbf_psr_header_t psr_header;
  configure_packet_defaults(&psr_header);
  char * header_ptr = reinterpret_cast<char *>(&psr_header);

  ska::pst::recv::UDPFormat::data_and_weights_t offsets{}, sizes{};

  // configure UDP formats
  PSTLOG_DEBUG(this, "UDPReceiver UDPFormatTest::test_decode_packet configure_beam");
  format.configure_beam(config);
  udp_header.configure(config, format);

  // expect thrown exception as format has not been prepared
  EXPECT_THROW(format.decode_packet(header_ptr, &offsets, &sizes), std::runtime_error); // NOLINT

  // prepare the UDP format
  PSTLOG_DEBUG(this, "UDPReceiver UDPFormatTest::test_decode_packet configure_scan");
  format.configure_scan(header);

  static constexpr uint64_t scan_id = 1234;
  format.set_scan_id(scan_id);
  udp_header.set_scan_id(scan_id);
  udp_header.set_beam_id(config.get<uint32_t>("BEAM_ID"));

  // write the header to the header_ptr
  PSTLOG_DEBUG(this, "UDPReceiver UDPFormatTest::test_decode_packet encode_header");
  udp_header.encode_header(header_ptr);

  // process the start of the data stream, configuring the start_psn
  PSTLOG_DEBUG(this, "UDPReceiver UDPFormatTest::test_decode_packet process_stream_start");
  format.process_stream_start(header_ptr);

  // check that malformed packets are correctly reported
  psr_header.magic_word = 0x0;
  PSTLOG_DEBUG(this, "UDPReceiver UDPFormatTest::test_decode_packet decode_packet malformed");
  ASSERT_EQ(format.decode_packet(header_ptr, &offsets, &sizes), ska::pst::recv::UDPFormat::PacketState::Malformed);

  // configure sensible defaults for the packet
  psr_header.magic_word = ska::pst::recv::magic_word;

  // write UDP packet header to the header_ptr, then increment counters
  uint64_t packet_idx = 0;

  ASSERT_EQ(format.decode_packet(header_ptr, &offsets, &sizes), ska::pst::recv::UDPFormat::PacketState::Ok);
  ASSERT_EQ(offsets.data, packet_idx * format.get_packet_data_size());
  ASSERT_EQ(offsets.weights, packet_idx * (format.get_packet_weights_size() + format.get_packet_scales_size()));

  // print packet header to stdout
  udp_header.gen_packet(header_ptr);

  // increment format by 1 packet (32 channels in this case)
  packet_idx++;
  PSTLOG_DEBUG(this, "UDPReceiver UDPFormatTest::test_decode_packet psr_header.first_channel_number={}", uint32_t(psr_header.first_channel_number));
  udp_header.gen_packet(header_ptr);

  PSTLOG_DEBUG(this, "UDPReceiver UDPFormatTest::test_decode_packet psr_header.first_channel_number={}", uint32_t(psr_header.first_channel_number));
  ASSERT_EQ(format.decode_packet(header_ptr, &offsets, &sizes), ska::pst::recv::UDPFormat::PacketState::Ok);
  ASSERT_EQ(offsets.data, packet_idx * format.get_packet_data_size());
  ASSERT_EQ(offsets.weights, packet_idx * (format.get_packet_weights_size() + format.get_packet_scales_size()));

  // increment format by 1 packet (32 channels in this case)
  udp_header.gen_packet(header_ptr);
  packet_idx++;

  format.decode_packet(header_ptr, &offsets, &sizes);
  ASSERT_EQ(offsets.data, packet_idx * format.get_packet_data_size());
  ASSERT_EQ(offsets.weights, packet_idx * (format.get_packet_weights_size() + format.get_packet_scales_size()));
}

TEST_F(UDPFormatTest, test_insert_last_packet_and_weights) // NOLINT
{
  ConcreteUDPFormat format;
  format.configure_beam(config);
  format.configure_scan(header);
  UDPHeader udp_header;
  udp_header.configure(config, format);

  std::vector<char> packet(format.get_packet_size());
  std::vector<char> data_buffer(format.get_packet_data_size());
  std::vector<char> weights_buffer(format.get_packet_weights_size() + format.get_packet_scales_size());
  ska::pst::recv::UDPFormat::data_and_weights_t offsets{}, sizes{};

  char * header_ptr = reinterpret_cast<char *>(&packet[0]);
  udp_header.encode_header(header_ptr);

  // process the start of the data stream, configuring the start_psn
  format.process_stream_start(header_ptr);
  format.decode_packet(header_ptr, &offsets, &sizes);
  format.insert_last_packet(&data_buffer[0], &weights_buffer[0]);
}

TEST_F(UDPFormatTest, test_gen_packet) // NOLINT
{
  ConcreteUDPFormat format;
  UDPHeader udp_header;
  format.configure_beam(config);
  format.configure_scan(header);

  udp_header.configure(config, format);

  static constexpr uint64_t scan_id = 12345;
  format.set_scan_id(scan_id);
  udp_header.set_scan_id(scan_id);

  // allocate storage for the packet
  std::vector<char> packet_data(format.get_packet_size());
  auto psr_header = reinterpret_cast<cbf_psr_header_t *>(&packet_data[0]);
  configure_packet_defaults(psr_header);
  auto psr_header_ptr = reinterpret_cast<char *>(psr_header);
  ska::pst::recv::UDPFormat::data_and_weights_t offsets{}, sizes{};

  // recorder the starting timestamp
  uint32_t samples_since_ska_epoch = psr_header->samples_since_ska_epoch;

  auto nchan = config.get<uint32_t>("NCHAN");
  uint32_t nchan_per_packet = format.get_nchan_per_packet();
  uint32_t npackets = nchan / nchan_per_packet;

  uint32_t psn = UDP_FORMAT_CBFPSR_FIRST_PSN;

  // process the start of the data stream, configuring the start_psn
  format.process_stream_start(psr_header_ptr);

  while (psr_header->samples_since_ska_epoch == samples_since_ska_epoch)
  {
    // generate 1 full set for channels in the packet sequence
    for (unsigned i=0; i<npackets; i++)
    {
      udp_header.gen_packet(psr_header_ptr);
      format.decode_packet(psr_header_ptr, &offsets, &sizes);
      uint32_t pkt_idx = (psn * npackets) + i;
      PSTLOG_TRACE(this, "UDPFormatTest test_gen_packet pkt_idx={} offsets=({},{}) sizes=({},{})",
        pkt_idx, offsets.data, offsets.weights, sizes.data, sizes.weights);
      ASSERT_EQ(pkt_idx * sizes.data, offsets.data);
      ASSERT_EQ(pkt_idx * sizes.weights, offsets.weights);
      ASSERT_EQ(sizes.data, format.get_packet_data_size());
      ASSERT_EQ(sizes.weights, (format.get_packet_weights_size() + format.get_packet_scales_size()));
      ASSERT_EQ(psr_header->packet_sequence_number, psn);
    }
    psn++;
  }
}

TEST_F(UDPFormatTest, test_set_get_validity_flags_policy) // NOLINT
{
  // check the default validity flags policy is to ignore all validity flags
  ConcreteUDPFormat format;
  ASSERT_EQ(format.get_validity_flags_policy(), ska::pst::recv::PacketValidityFlagsPolicy::IgnoreAll);

  // test that each of the known validity flag policies can be set for a UDPFormat
  std::vector<ska::pst::recv::PacketValidityFlagsPolicy> policies = {
    PacketValidityFlagsPolicy::IgnoreAll,
    PacketValidityFlagsPolicy::StationBeamDelayPolynomials,
    PacketValidityFlagsPolicy::PSTBeamDelayPolynomials,
    PacketValidityFlagsPolicy::JonesMatrices,
    PacketValidityFlagsPolicy::AnyDelayPolynomials,
    PacketValidityFlagsPolicy::All
  };
  for (auto & policy : policies)
  {
    format.set_validity_flags_policy(policy);
    ASSERT_EQ(format.get_validity_flags_policy(), policy);
  }
}

TEST_P(UDPFormatTest, test_packet_validity_policies) // NOLINT
{
  auto policy = GetParam();

  // allocate storage for the packet to use in the tests
  std::shared_ptr<ConcreteUDPFormat> format = std::make_shared<ConcreteUDPFormat>();
  std::shared_ptr<ska::pst::recv::UDPHeader> udp_header = std::make_shared<ska::pst::recv::UDPHeader>();
  format->set_validity_flags_policy(policy);
  std::vector<char> packet_data;

  PSTLOG_TRACE(this, "UDPFormatTest test_IgnoreAll_policy prepare_packet(format,udp_header,{})",
    reinterpret_cast<void*>(&packet_data[0]));

  prepare_packet(format, udp_header, packet_data);
  std::vector<char> data_buffer(format->get_packet_data_size());
  std::vector<char> weights_buffer(format->get_packet_weights_size() + format->get_packet_scales_size());

  ska::pst::recv::UDPFormat::data_and_weights_t offsets{}, sizes{};
  auto psr_header_ptr = reinterpret_cast<char *>(&packet_data[0]);
  auto weights_ptr = reinterpret_cast<uint16_t*>(&weights_buffer[format->get_packet_scales_size()]);
  const uint32_t nweights = format->get_packet_weights_size() / sizeof(uint16_t);

  const uint16_t valid_weight = 65535;
  const uint16_t invalid_weight = 0;

  for (auto & hdr : headers_with_flags)
  {
    bool valid = ska::pst::recv::packet_weights_valid_for_policy(&hdr, policy);
    uint16_t expected_weight = valid ? valid_weight : invalid_weight;

    // copy the header with flags into the udp packet
    memcpy(psr_header_ptr, &hdr, sizeof(cbf_psr_header_t));

    // decode the packet, triggering the format to read the header
    ASSERT_EQ(format->decode_packet(psr_header_ptr, &offsets, &sizes), ska::pst::recv::UDPFormat::PacketState::Ok);

    // copy the data/weights from the packet, zeroing the weights if flags and policy are set
    format->insert_last_packet(&data_buffer[0], &weights_buffer[0]);

    for (unsigned i=0; i<nweights; i++)
    {
      ASSERT_EQ(weights_ptr[i], expected_weight); // NOLINT
    }
  }
}

INSTANTIATE_TEST_SUITE_P(UDPFormatTestWithPolicies, UDPFormatTest, testing::Values(
  ska::pst::recv::PacketValidityFlagsPolicy::IgnoreAll,
  ska::pst::recv::PacketValidityFlagsPolicy::StationBeamDelayPolynomials,
  ska::pst::recv::PacketValidityFlagsPolicy::PSTBeamDelayPolynomials,
  ska::pst::recv::PacketValidityFlagsPolicy::JonesMatrices,
  ska::pst::recv::PacketValidityFlagsPolicy::AnyDelayPolynomials,
  ska::pst::recv::PacketValidityFlagsPolicy::All
));

} // namespace ska::pst::recv::test
