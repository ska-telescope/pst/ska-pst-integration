/*
 * Copyright 2022 Square Kilometre Array Observatory
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 * contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#include "ska_pst/common/utils/Logging.h"

#include "ska_pst/recv/formats/tests/UDPFormatFactoryTest.h"
#include "ska_pst/common/testutils/GtestMain.h"

auto main(int argc, char* argv[]) -> int
{
  return ska::pst::common::test::gtest_main(argc, argv);
}

namespace ska::pst::recv::test {

UDPFormatFactoryTest::UDPFormatFactoryTest()
    : ::testing::Test()
{
}

void UDPFormatFactoryTest::SetUp()
{
}

void UDPFormatFactoryTest::TearDown()
{
}

TEST_F(UDPFormatFactoryTest, test_get_supported_formats) // NOLINT
{
  std::vector<std::string> supported_formats = ska::pst::recv::get_supported_formats();
  ASSERT_EQ(supported_formats.size(), 7);
  ASSERT_EQ(supported_formats[0], std::string("LowTestVector"));
  ASSERT_EQ(supported_formats[1], std::string("LowPST"));
  ASSERT_EQ(supported_formats[2], std::string("MidPSTBand1"));
}

TEST_F(UDPFormatFactoryTest, test_get_supported_formats_list) // NOLINT
{
  std::string formats_list = ska::pst::recv::get_supported_formats_list();
  ASSERT_EQ(formats_list, std::string("LowTestVector, LowPST, MidPSTBand1, MidPSTBand2, MidPSTBand3, MidPSTBand4, MidPSTBand5"));
}

TEST_F(UDPFormatFactoryTest, test_format_generation) // NOLINT
{
  std::shared_ptr<ska::pst::recv::UDPFormat> fmt;

  fmt = UDPFormatFactory("LowTestVector");
  auto low_test_vector = dynamic_cast<LowTestVector *>(fmt.get());
  ASSERT_NE(low_test_vector, nullptr);

  fmt = UDPFormatFactory("LowPST");
  auto low_pst = dynamic_cast<LowPST *>(fmt.get());
  ASSERT_NE(low_pst, nullptr);

  fmt = UDPFormatFactory("MidPSTBand1");
  auto mid_pst_band1 = dynamic_cast<MidPSTBand1 *>(fmt.get());
  ASSERT_NE(mid_pst_band1, nullptr);

  fmt = UDPFormatFactory("MidPSTBand2");
  auto mid_pst_band2 = dynamic_cast<MidPSTBand2 *>(fmt.get());
  ASSERT_NE(mid_pst_band2, nullptr);

  fmt = UDPFormatFactory("MidPSTBand3");
  auto mid_pst_band3 = dynamic_cast<MidPSTBand3 *>(fmt.get());
  ASSERT_NE(mid_pst_band3, nullptr);

  fmt = UDPFormatFactory("MidPSTBand4");
  auto mid_pst_band4 = dynamic_cast<MidPSTBand4 *>(fmt.get());
  ASSERT_NE(mid_pst_band4, nullptr);

  fmt = UDPFormatFactory("MidPSTBand5");
  auto mid_pst_band5 = dynamic_cast<MidPSTBand5 *>(fmt.get());
  ASSERT_NE(mid_pst_band5, nullptr);

  EXPECT_THROW(UDPFormatFactory("NotAFormat"), std::runtime_error); // NOLINT
}

} // namespace ska::pst::recv::test
