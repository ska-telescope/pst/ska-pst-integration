/*
 * Copyright 2023 Square Kilometre Array Observatory
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 * contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#include "ska_pst/common/utils/Logging.h"
#include "ska_pst/recv/formats/tests/UDPHeaderTest.h"
#include "ska_pst/recv/formats/UDPHeader.h"
#include "ska_pst/common/testutils/GtestMain.h"

using ska::pst::common::test::test_data_file;

auto main(int argc, char* argv[]) -> int
{
  spdlog::set_level(spdlog::level::debug);
  return ska::pst::common::test::gtest_main(argc, argv);
}

namespace ska::pst::recv::test {

UDPHeaderTest::UDPHeaderTest()
    : ::testing::Test()
{
}

void UDPHeaderTest::SetUp()
{
  config.load_from_file(test_data_file("LowPSTConfig.txt"));
  header.load_from_file(test_data_file("LowPSTConfig.txt"));
}

void UDPHeaderTest::TearDown()
{
  config.reset();
  header.reset();
}

TEST_F(UDPHeaderTest, test_default_constructor) // NOLINT
{
  UDPHeader udp_header;
}

TEST_F(UDPHeaderTest, test_configure) // NOLINT
{
  UDPHeader udp_header;
  udp_header.configure(config, format);
}

TEST_F(UDPHeaderTest, test_encode_header) // NOLINT
{
  format.configure_beam(config);
  format.configure_scan(header);

  cbf_psr_header_t psr_header;
  UDPHeader udp_header;
  udp_header.configure(config, format);
  udp_header.encode_header(reinterpret_cast<char *>(&psr_header));

  uint32_t start_channel{};
  config.get("START_CHANNEL", &start_channel);
  ASSERT_EQ(psr_header.first_channel_number, start_channel);
  ASSERT_EQ(psr_header.time_samples_per_packet, uint16_t(format.get_samples_per_packet()));
  ASSERT_EQ(psr_header.channels_per_packet, uint16_t(format.get_nchan_per_packet()));
  ASSERT_EQ(psr_header.valid_channels_per_packet, uint16_t(format.get_nchan_per_packet()));
  ASSERT_EQ(psr_header.num_time_samples_per_relative_weight, uint8_t(format.get_nsamp_per_weight()));
  ASSERT_EQ(psr_header.num_power_samples_averaged, 0);
}

TEST_F(UDPHeaderTest, test_gen_packet) // NOLINT
{
  UDPHeader udp_header;
  format.configure_beam(config);
  format.configure_scan(header);

  udp_header.configure(config, format);

  static constexpr uint64_t scan_id = 12345;
  format.set_scan_id(scan_id);
  udp_header.set_scan_id(scan_id);
  udp_header.set_beam_id(config.get<uint32_t>("BEAM_ID"));

  // allocate storage for the packet
  std::vector<char> packet_data(format.get_packet_size());
  auto psr_header = reinterpret_cast<cbf_psr_header_t *>(&packet_data[0]);
  auto psr_header_ptr = reinterpret_cast<char *>(psr_header);
  udp_header.encode_header(psr_header_ptr);

  ASSERT_NO_THROW(print_packet_header(psr_header)); // NOLINT

  ska::pst::recv::UDPFormat::data_and_weights_t offsets{}, sizes{};
  uint32_t samples_since_ska_epoch = psr_header->samples_since_ska_epoch;
  auto nchan = config.get<uint32_t>("NCHAN");
  uint32_t nchan_per_packet = format.get_nchan_per_packet();
  uint32_t npackets = nchan / nchan_per_packet;
  uint32_t psn = UDP_FORMAT_CBFPSR_FIRST_PSN;

  // process the start of the data stream, configuring the start_psn
  format.process_stream_start(psr_header_ptr);

  while (psr_header->samples_since_ska_epoch == samples_since_ska_epoch)
  {
    // generate 1 full set for channels in the packet sequence
    for (unsigned i=0; i<npackets; i++)
    {
      udp_header.gen_packet(psr_header_ptr);
      format.decode_packet(psr_header_ptr, &offsets, &sizes);
      uint32_t pkt_idx = (psn * npackets) + i;
      PSTLOG_TRACE(this, "UDPHeaderTest test_gen_packet pkt_idx={} offsets=({},{}) sizes=({},{})", pkt_idx, offsets.data, offsets.weights, sizes.data, sizes.weights);
      ASSERT_EQ(pkt_idx * sizes.data, offsets.data);
      ASSERT_EQ(pkt_idx * sizes.weights, offsets.weights);
      ASSERT_EQ(sizes.data, format.get_packet_data_size());
      ASSERT_EQ(sizes.weights, (format.get_packet_weights_size() + format.get_packet_scales_size()));
      ASSERT_EQ(psr_header->packet_sequence_number, psn);
    }
    psn++;
  }
}

TEST_F(UDPHeaderTest, test_get_failure_type) // NOLINT
{
  ASSERT_EQ(ska::pst::recv::get_failure_type("MagicWord"), ska::pst::recv::FailureType::BadMagicWord);
  ASSERT_EQ(ska::pst::recv::get_failure_type("PacketSize"), ska::pst::recv::FailureType::BadPacketSize);
  ASSERT_EQ(ska::pst::recv::get_failure_type("Transmit"), ska::pst::recv::FailureType::BadTransmit);
  ASSERT_EQ(ska::pst::recv::get_failure_type("MisorderedPSN"), ska::pst::recv::FailureType::MisorderedPacketSequenceNumber);
  ASSERT_EQ(ska::pst::recv::get_failure_type("ScanID"), ska::pst::recv::FailureType::BadScanID);
  ASSERT_EQ(ska::pst::recv::get_failure_type("ChannelNumber"), ska::pst::recv::FailureType::BadChannelNumber);
  ASSERT_EQ(ska::pst::recv::get_failure_type("Timestamp"), ska::pst::recv::FailureType::BadTimestamp);
  ASSERT_EQ(ska::pst::recv::get_failure_type("DataRate"), ska::pst::recv::FailureType::BadDataRate);
  ASSERT_EQ(ska::pst::recv::get_failure_type("None"), ska::pst::recv::FailureType::None);
  ASSERT_EQ(ska::pst::recv::get_failure_type("ShouldNotExit"), ska::pst::recv::FailureType::None);
}

} // namespace ska::pst::recv::test
