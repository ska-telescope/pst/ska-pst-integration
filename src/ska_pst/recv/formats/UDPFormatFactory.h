/*
 * Copyright 2022 Square Kilometre Array Observatory
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 * contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#include <string>
#include <vector>
#include <memory>

#include "ska_pst/recv/formats/LowTestVector.h"
#include "ska_pst/recv/formats/LowPST.h"
#include "ska_pst/recv/formats/MidPSTBand1.h"
#include "ska_pst/recv/formats/MidPSTBand2.h"
#include "ska_pst/recv/formats/MidPSTBand3.h"
#include "ska_pst/recv/formats/MidPSTBand4.h"
#include "ska_pst/recv/formats/MidPSTBand5.h"

#ifndef SKA_PST_RECV_FORMATS_UDPFormatFactory_h
#define SKA_PST_RECV_FORMATS_UDPFormatFactory_h

namespace ska::pst::recv {

/**
 * @brief Construct a UDPFormat from the format_name
 *
 * @param format_name string representation of the format name
 * @return UDPFormat* new UDPFormat object
 */
std::shared_ptr<UDPFormat> UDPFormatFactory(const std::string &format_name);

/**
 * @brief Return a vector of the supported format names
 *
 * @return std::vector<std::string> names of supported formats
 */
std::vector<std::string> get_supported_formats();

/**
 * @brief Check if a given format name is supported.
 *
 */
bool is_format_supported(const std::string &format_name);

/**
 * @brief Return a comma delimited string of supported format names
 *
 * @return std::string supported format names
 */
std::string get_supported_formats_list();

} // namespace ska::pst::recv

#endif // SKA_PST_RECV_FORMATS_UDPFormatFactory_h
