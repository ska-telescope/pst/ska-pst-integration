/*
 * Copyright 2023 Square Kilometre Array Observatory
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 * contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#include <cinttypes>
#include <string>
#include <map>

#include "ska_pst/common/utils/Time.h"
#include "ska_pst/recv/formats/PacketStructure.h"
#include "ska_pst/recv/formats/UDPFormat.h"

#ifndef SKA_PST_RECV_FORMATS_UDPHeader_h
#define SKA_PST_RECV_FORMATS_UDPHeader_h

namespace ska::pst::recv {

  /**
   * @brief Enumeration of different failure types supported by the UDPGenerator
   *
   */
  enum FailureType {
    None,
    BadMagicWord,
    BadPacketSize,
    BadTransmit,
    MisorderedPacketSequenceNumber,
    BadScanID,
    BadChannelNumber,
    BadTimestamp,
    BadDataRate
  };

  /**
   * @brief Get the FailureType corresponding to a string description
   *
   * @param name string description of the failure type
   * @return FailureType enumerated value of the failure type
   */
  static FailureType get_failure_type(const std::string& name)
  {
    if (name == "MagicWord") { return BadMagicWord; }
    if (name == "PacketSize") { return BadPacketSize; }
    if (name == "Transmit") { return BadTransmit; }
    if (name == "MisorderedPSN") { return MisorderedPacketSequenceNumber; }
    if (name == "ScanID") { return BadScanID; }
    if (name == "ChannelNumber") { return BadChannelNumber; }
    if (name == "Timestamp") { return BadTimestamp; }
    if (name == "DataRate") { return BadDataRate; }
    if (name == "None") { return None; }
    return None;
  }

  /**
   * @brief Enumeration of different packet validity types supported by the UDPGenerator
   *
   */
  enum class ValidityType {
    AllValid,
    StationBeamFlagInvalid,
    PstBeamFlagInvalid,
    JonesMatricesFlagInvalid,
  };

  static ValidityType get_validity_type(const std::string& name)
  {
    if (name == "StationBeam") { return ska::pst::recv::ValidityType::StationBeamFlagInvalid; }
    if (name == "PstBeam") { return ska::pst::recv::ValidityType::PstBeamFlagInvalid; }
    if (name == "JonesMatrices") { return ska::pst::recv::ValidityType::JonesMatricesFlagInvalid; }
    if (name == "AllValid") { return ska::pst::recv::ValidityType::AllValid; }
    throw std::runtime_error("get_validity_type unrecognised name");
  }

  /**
   * @brief Mapping of enumerated packet validity type to strings that describe the flag
   *
   */
  static std::map<ValidityType, std::string> validity_type_names{
      {ValidityType::AllValid, "AllValid"},
      {ValidityType::StationBeamFlagInvalid, "StationBeamFlagInvalid"},
      {ValidityType::PstBeamFlagInvalid, "PstBeamFlagInvalid"},
      {ValidityType::JonesMatricesFlagInvalid, "JonesMatricesFlagInvalid"},
  };

  class UDPHeader
  {
    public:

      /**
       * @brief Construct a new UDPHeader object.Initialises packet values by calling
       * ska::pst::recv::PacketStructure::configure_packet_defaults(cbf_psr_header_t * header)
       *
       */
      UDPHeader();

      /**
       * @brief Destroy the UDPHeader object
       *
       */
      ~UDPHeader() = default;

      /**
       * @brief Configures private PacketStructure struct cbf_psr_header header using the input parameters
       * config and format.
       *
       * @param config AsciiHeader object containing PacketStructure definitions
       * @param format UDPFormat object packet format definitions.
       */
      void configure(const ska::pst::common::AsciiHeader& config, const ska::pst::recv::UDPFormat& format);

      /**
       * @brief set number samples since SKA Epoch.
       *
       * This will effectively set the current time of header based on
       * the sample period (TSAMP) and the number of samples since SKA epoch.
       */
      void set_samples_since_ska_epoch(uint64_t samples_since_ska_epoch);

      /**
       * @brief Set the packet sequence number object
       *
       * @param psn sequence number assigned to the packet.
       */
      void set_packet_sequence_number(uint64_t psn);

      /**
       * @brief Set the scan id object
       *
       * @param scan_id scan id assigned to the packet.
       */
      void set_scan_id(uint64_t scan_id);

      /**
       * @brief Set the beam id object
       *
       * @param beam_id beam id assigned to the packet.
       */
      void set_beam_id(uint64_t beam_id);

      /**
       * @brief Adjust the header validity flags.
       *
       * @param type validity type enumeration value to use
       */
      void set_validity_flag(ValidityType type);

      /**
       * @brief Copies the contents of the header into the parameter buf.
       *
       * @param buf socket buffer to be encoded using the local header
       */
      void encode_header(char * buf);

      /**
       * @brief increments header packet_sequence number and updates header timestamp.
       *
       */
      void increment_packet();

      /**
       * @brief passes param buf to encode_header method and calls increment_packet. Returns packet_size.
       *
       * @param buf socket buffer
       * @return size_t size of the packet
       */
      size_t gen_packet(char * buf);

      /**
       * @brief Used for testing. Generates a packet with induced failure.
       *
       * @param buf socket buffer
       * @param failure_type type of error to be induced
       * @return size_t size of the packet
       */
      size_t gen_packet_failure(char * buf, FailureType failure_type);

    private:

      cbf_psr_header_t header;

      uint32_t start_channel{0};

      uint32_t end_channel{0};

      double attoseconds_per_packet{0};

      uint32_t nchan_per_packet{0};

      uint32_t packet_size{0};

      uint32_t samples_per_packet{0};

      bool increment_psn_every_packet{false};

      bool configured{false};

  };

} // namespace ska::pst::recv

#endif // SKA_PST_RECV_FORMATS_UDPHeader_h
