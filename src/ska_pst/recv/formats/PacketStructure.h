/*
 * Copyright 2022 Square Kilometre Array Observatory
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 * contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#include <cinttypes>
#include <map>

#ifndef SKA_PST_RECV_FORMATS_PacketStructure_h
#define SKA_PST_RECV_FORMATS_PacketStructure_h

namespace ska::pst::recv {

  #define INIT_CBF_PST_HEADER_T {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}

  //! definition of the magic word value required for all CBF/PSR packets
  static constexpr uint32_t magic_word = 0xBEADFEED;

  /**
    * @brief Enumeration of the different packet validity flag inspection policies the UDPFormat supports.

    * If a policy inspects a flag and that flag is set in the packet, then the weights from that packet
    * are ignored and zeroed weights are used instead.
    *
    * IgnoreAll                    All packet validity flags are ignored, weights are not modified
    * StationBeamDelayPolynomials  Only station beam delay polynomial validity flags are inspected
    * PSTBeamDelayPolynomials      Only PST beam delay polynomial validity flags are inspected
    * JonesMatrices                Only Jones Matrices validity flags are inspected
    * AnyDelayPolynomials          Both station and PST beam delay polynomial validity flags are inspected
    * All                          All packet validity flags are inspected
    */
  enum class PacketValidityFlagsPolicy
  {
    IgnoreAll,
    StationBeamDelayPolynomials,
    PSTBeamDelayPolynomials,
    JonesMatrices,
    AnyDelayPolynomials,
    All
  };

  /**
   * @brief Mapping of enumerated packet validity flags policy to strings that describe the policy
   *
   */
  static std::map<PacketValidityFlagsPolicy, std::string> packet_validity_flags_policy_names{
      {PacketValidityFlagsPolicy::IgnoreAll, "Ignore All"},
      {PacketValidityFlagsPolicy::StationBeamDelayPolynomials, "Station Beam Delay Polynomials"},
      {PacketValidityFlagsPolicy::PSTBeamDelayPolynomials, "PST Beam Delay Polynomials"},
      {PacketValidityFlagsPolicy::JonesMatrices, "Jones Matrices"},
      {PacketValidityFlagsPolicy::AnyDelayPolynomials, "Any Delay Polynomials"},
      {PacketValidityFlagsPolicy::All, "All"},
  };

  static PacketValidityFlagsPolicy get_validity_flags_policy(const std::string &name)
  {
    if (name == "IgnoreAll")
    {
      return ska::pst::recv::PacketValidityFlagsPolicy::IgnoreAll;
    }
    if (name == "StationBeamDelayPolynomials")
    {
      return ska::pst::recv::PacketValidityFlagsPolicy::StationBeamDelayPolynomials;
    }
    if (name == "PSTBeamDelayPolynomials")
    {
      return ska::pst::recv::PacketValidityFlagsPolicy::PSTBeamDelayPolynomials;
    }
    if (name == "JonesMatrices")
    {
      return ska::pst::recv::PacketValidityFlagsPolicy::JonesMatrices;
    }
    if (name == "AnyDelayPolynomials")
    {
      return ska::pst::recv::PacketValidityFlagsPolicy::AnyDelayPolynomials;
    }
    if (name == "All")
    {
      return ska::pst::recv::PacketValidityFlagsPolicy::All;
    }
    throw std::runtime_error("get_validity_type unrecognised name");
  }

  /**
   * @brief Definition of the header structure which is located in every UDP packet
   * that forms the CBF/PSR UDP data format.
   *
   */
  typedef struct cbf_psr_header
  {
    //! packet sequence number
    uint64_t packet_sequence_number;

    //! samples since SKA epoch
    uint64_t samples_since_ska_epoch;

    //! sample period numerator
    uint16_t sample_period_numerator;

    //! sample period denominator
    uint16_t sample_period_denominator;

    //! The frequency spacing between channels in the packet in units of millihertz
    uint32_t channel_separation;

    //! The channel centre frequency of the First Channel Number in the packet in units of milli hertz
    uint64_t first_channel_freq;

    //! The value by which CBF voltage data is scaled before conversion to the values in the packet
    float scale_1;

    //! Scale factor #2 - Stokes Q [Mid.PSS only]
    float scale_2;

    //! Scale factor #3 - Stokes U [Mid.PSS only]
    float scale_3;

    //! Scale factor #4 - Stokes V [Mid.PSS only]
    float scale_4;

    //! The first frequency channel index within the packet
    uint32_t first_channel_number;

    //! The range of contiguous frequency channels contained in the packet
    uint16_t channels_per_packet;

    //! The number of valid frequency channels in the packet, always <= channels_per_packet
    uint16_t valid_channels_per_packet;

    //! The number time samples for each frequency channel contained within the packet
    uint16_t time_samples_per_packet;

    //! The beam (of 16 possible beams for PST and 501 for Low.PSS and 1500 for Mid.PSS) to which the data in the packet belongs
    uint16_t beam_number;

    //! Enables a basic check of the packet contents during decoding [BEADFEED]
    uint32_t magic_word;

    //! 0=Low.PSS, 1=Mid.PSS, 2=Low.PST, 3=Mid.PST
    uint8_t packet_destination;

    //! the number of bits per integer value stored in the Channel/Sample data section of the packet
    uint8_t data_precision;

    //! The number voltage time samples, whose power is averaged, for each time sample in the packet. Valid only for Mid.PSS
    uint8_t num_power_samples_averaged;

    //! The number of times samples, for each frequency channel, over which a relative weight is calculated
    uint8_t num_time_samples_per_relative_weight;

    //! The station beam delay polynomials have been applied
    uint8_t station_beam_polynomials_applied : 1;

    //! The PST beam delay polynomials have been applied
    uint8_t pst_beam_polynomials_applied : 1;

    //! The Jones polarisation corrections have been applied
    uint8_t jones_polarisation_corrections_applied : 1;

    //! Reserved byte
    uint8_t reserved_1;

    //! The major release number for the beam-former version
    uint8_t cbf_version_major;

    //! The minor release number for the beam-former version
    uint8_t cbf_version_minor;

    //! The scan ID specified by the LMC
    uint64_t scan_id;

    //! to be added to the corresponding Stokes parameters before applying scale_[1..4]
    float offset_1;

    //! Offset #2 - Stokes Q [Mid.PSS only]
    float offset_2;

    //! Offset #3 - Stokes Q [Mid.PSS only]
    float offset_3;

    //! Offset #4 - Stokes Q [Mid.PSS only]
    float offset_4;

  } __attribute__((packed, aligned(1))) cbf_psr_header_t;

  /**
   * @brief Convenience structure for CBF/PSR packet comprising pointers to the header, weights and data
   *
   */
  typedef struct cbf_psr_packet
  {
    //! pointer to CBF/PSR header
    cbf_psr_header_t * header;

    //! pointer to CBF/PSR weights
    void * weights;

    //! pointer to CBF/PSR data
    void * data;

  } cbf_psr_packet_t;

  /**
   * @brief Configure default parameters for a CBF/PSR header structure
   *
   * @param header header to write default parameters to
   */
  void configure_packet_defaults(cbf_psr_header_t * header);

  /**
   * @brief Validate the format of the packet header
   *
   * @param header CBF/PSR header structure to inspect
   * @return true packet header is valid
   * @return false packet header is not valid
   */
  bool validate_packet_header(cbf_psr_header_t * header);

  /**
   * @brief Get the offset in bytes from the scale1 parameter in the cbf_pst_header_t structure
   *
   * @return size_t offset in bytes of the scale1 parameter
   */
  size_t get_scale1_byte_offset();

  /**
   * @brief print packet_header contents
   *
   * @param header
   */
  void print_packet_header(cbf_psr_header_t * header);

  /**
    * @brief Determine if the UDPFormat's policy, when combined with the packet validity flags,
    * results in the weights for this packet being zeroed.
    *
    * @param header pointer to CBFPSR packet header containing weights
    * @param policy the packet validity flag inspection policy to apply
    * @return true packet is valid and weights for packet should not be zeroed; otherwise, false
    */
  bool packet_weights_valid_for_policy(cbf_psr_header_t * header, PacketValidityFlagsPolicy policy);

} // namespace ska::pst::recv

#endif // SKA_PST_RECV_FORMATS_PacketStructure_h
