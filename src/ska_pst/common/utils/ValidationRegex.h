/*
 * Copyright 2024 Square Kilometre Array Observatory
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 * contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#include <string>

#ifndef SKA_PST_COMMON_UTILS_ValidationRegex_h
#define SKA_PST_COMMON_UTILS_ValidationRegex_h

namespace ska::pst::common
{
  //! Regular expression for an SKA Execution Block ID
  static std::string eb_id_regex = "^eb\\-[a-z0-9]+\\-[0-9]{8}\\-[a-z0-9]+$";

  //! Regular expression for any positive integer
  static std::string positive_int_regex = "^[0-9]{1,}$";

  //! Regular expression for any positive floating point number
  static std::string positive_float_regex = "^([0-9]*[.])?[0-9]+$";

  //! Regular expression for a 4-character hexadecimal string
  static std::string dada_key_regex = "^[a-f0-9]{4,}$";

  //! Regular expression for a pulsar source name
  static std::string pulsar_source_regex = "^J[\\d]{4}-[\\d]{4}([a-zA-Z])$";

  //! Regular expression for a generic source name, allow any alphanumeric, _, +, or - chars.
  static std::string source_regex = "^[\\w+-]+$";

  //! Regular expression for an IPv4 address
  static std::string ipv4_regex = "^(([0-9]|[1-9][0-9]|1[0-9][0-9]|2[0-4][0-9]|25[0-5]).){3}([0-9]|[1-9][0-9]|1[0-9][0-9]|2[0-4][0-9]|25[0-5])$";

  //! Regular expression for UDP format
  static std::string udp_format_regex = "^[A-Za-z1-5]{1,}$";

  //! Regular expression for Oversampling Ratio
  static std::string os_ratio_regex = "^[0-9]{1,}/[0-9]{1,}$";

} // namespace ska::pst::common

#endif // SKA_PST_COMMON_UTILS_ValidationRegex_h
