/*
 * Copyright 2023 Square Kilometre Array Observatory
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 * contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#include <spdlog/pattern_formatter.h>
#include <spdlog/spdlog.h>
#include <spdlog/mdc.h>

#include <map>
#include <memory>
#include <type_traits>

#ifndef SKA_PST_COMMON_UTILS_Logging_h
#define SKA_PST_COMMON_UTILS_Logging_h

namespace ska::pst::common {

  template<typename...>
  struct Void { using type = void; };

  template<typename... T>
  using void_t = typename Void<T...>::type;

  //! Default implementation for classes without a get_log_context method
  template<typename T, typename = void>
  class MappedDiagnosticContext
  {
  public:
      MappedDiagnosticContext(T* t) { /* do nothing */ }
  };

  //! Specialization for classes with a get_log_context method
  template<typename T>
  class MappedDiagnosticContext<T, void_t<decltype(std::declval<T>().get_log_context())>>
  {
    //! Pointer to the object with the get_log_context method;
    const T* object = nullptr;

  public:

    //! Constructor adds key/value pairs to spdlog mapped diagnostic context
    MappedDiagnosticContext(T* t)
    {
      object = t;
      if (!object)
        return;

      auto log_context = t->get_log_context();
      auto &mdc_map = spdlog::mdc::get_context();
      mdc_map.insert(log_context.begin(), log_context.end());
    }

    //! Destructor removes key/value pairs from spdlog mapped diagnostic context
    ~MappedDiagnosticContext()
    {
      if (!object)
        return;

      auto log_context = object->get_log_context();
      auto &mdc_map = spdlog::mdc::get_context();
      for (auto it : log_context)
      {
        mdc_map.erase(it.first);
      }
    }
  };

  template<typename T>
  auto mapped_diagnostic_context(T* t) -> MappedDiagnosticContext<T>
  {
    return MappedDiagnosticContext<T>(t);
  }

  //! allows PSTLOG macros to be called with zero (0) or nullptr as first argument
  void* mapped_diagnostic_context(std::nullptr_t);

} // ska::pst::common

#define SKA_LOGGING_FORMAT "1|%Y-%m-%dT%T.%eZ|%l|Thread-%t|%!|%s#%#|%*|%v" // NOLINT

#ifndef PSTLOG_ERROR
  #define PSTLOG_ERROR(entity, ...) { \
      if (spdlog::should_log(spdlog::level::err)) { \
        auto log_context = ska::pst::common::mapped_diagnostic_context(entity); \
        SPDLOG_ERROR(__VA_ARGS__); \
      } \
    }
#endif

#ifndef PSTLOG_WARN
  #define PSTLOG_WARN(entity, ...) { \
      if (spdlog::should_log(spdlog::level::warn)) { \
        auto log_context = ska::pst::common::mapped_diagnostic_context(entity); \
        SPDLOG_WARN(__VA_ARGS__); \
      } \
    }
#endif

#ifndef PSTLOG_INFO
  #define PSTLOG_INFO(entity, ...) { \
      if (spdlog::should_log(spdlog::level::info)) { \
        auto log_context = ska::pst::common::mapped_diagnostic_context(entity); \
        SPDLOG_INFO(__VA_ARGS__); \
      } \
    }
#endif

#ifndef PSTLOG_DEBUG
  #define PSTLOG_DEBUG(entity, ...) { \
      if (spdlog::should_log(spdlog::level::debug)) { \
        auto log_context = ska::pst::common::mapped_diagnostic_context(entity); \
        SPDLOG_DEBUG(__VA_ARGS__); \
      } \
    }
#endif

#ifndef PSTLOG_TRACE
#ifdef NDEBUG
  #define PSTLOG_TRACE(...) {}
#else
  #define PSTLOG_TRACE(entity, ...) { \
      if (spdlog::should_log(spdlog::level::trace)) { \
        auto log_context = ska::pst::common::mapped_diagnostic_context(entity); \
        SPDLOG_TRACE(__VA_ARGS__); \
      } \
    }
#endif
#endif

namespace ska::pst::common {
  class tags_formatter : public spdlog::custom_flag_formatter {
  public:
      void format(const spdlog::details::log_msg &, const std::tm &, spdlog::memory_buf_t &dest) override;
      std::unique_ptr<custom_flag_formatter> clone() const override;
  };


  /**
   * Used to set up the spdlog logging framework
   */
  void setup_spdlog();

} // namespace ska::pst::common

#endif // SKA_PST_COMMON_UTILS_Logging_h

