/*
 * Copyright 2025 Square Kilometre Array Observatory
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 * contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#include "ska_pst/common/testutils/GtestMain.h"
#include "ska_pst/common/utils/tests/FilePathsTest.h"
#include "ska_pst/common/utils/Logging.h"

#include <filesystem>
#include <random>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

auto main(int argc, char* argv[]) -> int
{
  return ska::pst::common::test::gtest_main(argc, argv);
}

namespace ska::pst::common::test {

FilePathsTest::FilePathsTest()
    : ::testing::Test()
{
}

void FilePathsTest::SetUp()
{
  base_path_str = "/tmp";
  base_path = std::filesystem::path(base_path_str);

  // Seed the random number generator
  std::random_device rd;
  std::mt19937 gen(rd());
  std::uniform_int_distribution<unsigned> distribution(1, 99999); // NOLINT
  unsigned _scan_id = distribution(gen);

  scan_id = std::to_string(_scan_id);
  eb_id = "eb-m001-20230712-56789";
  telescope = (_scan_id < 5000) ? "SKALow" : "SKAMid"; // NOLINT
  subsystem_id = FilePaths::get_subsystem_from_telescope(telescope);
}

void FilePathsTest::TearDown()
{
}

TEST_F(FilePathsTest, bad_telescope_name) // NOLINT
{
  EXPECT_THROW(FilePaths::get_subsystem_from_telescope("BadNameOfTelescope"), std::runtime_error);
  EXPECT_EQ(FilePaths::get_subsystem_from_telescope("SKALow"), "pst-low");
  EXPECT_EQ(FilePaths::get_subsystem_from_telescope("SKAMid"), "pst-mid");
}

TEST_F(FilePathsTest, test_str_constructor) // NOLINT
{
  FilePaths fp(base_path_str);
  std::filesystem::path expected_path = base_path / "product" / eb_id / subsystem_id / scan_id;
  EXPECT_EQ(fp.get_scan_product_path(eb_id, telescope, scan_id), expected_path);
}

TEST_F(FilePathsTest, test_path_constructor) // NOLINT
{
  FilePaths fp(base_path);

  std::filesystem::path expected_path = base_path / "product" / eb_id / subsystem_id / scan_id;
  EXPECT_EQ(fp.get_scan_product_path(eb_id, telescope, scan_id), expected_path);
}

} // namespace ska::pst::common::test
