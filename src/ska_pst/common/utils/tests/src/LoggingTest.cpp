/*
 * Copyright 2022 Square Kilometre Array Observatory
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 * contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#include "ska_pst/common/utils/tests/LoggingTest.h"
#include "ska_pst/common/utils/Logging.h"
#include "ska_pst/common/testutils/GtestMain.h"

auto main(int argc, char* argv[]) -> int
{
  return ska::pst::common::test::gtest_main(argc, argv);
}

namespace ska::pst::common::test {

LoggingTest::LoggingTest()
    : ::testing::Test()
{
}

void LoggingTest::SetUp()
{
}

void LoggingTest::TearDown()
{
}

struct HasContextTest
{
  auto get_log_context() const -> std::map<std::string, std::string>
  {
    return {{
      "entity", "HasContextTest"
    }};
  }

  void log_some_messages()
  {
    PSTLOG_ERROR(this, "this is an error message with context");
    PSTLOG_WARN(this, "this is a warn message with context");
    PSTLOG_INFO(this, "this is an info message with context");
    PSTLOG_DEBUG(this, "this is a debug message with context");
  }
};

TEST_F(LoggingTest, class_with_context) // NOLINT
{
  HasContextTest object;
  object.log_some_messages();
}

struct HasNoContextTest
{
  void log_some_messages()
  {
    PSTLOG_ERROR(this, "this is an error message without context");
    PSTLOG_WARN(this, "this is a warn message without context");
    PSTLOG_INFO(this, "this is an info message without context");
    PSTLOG_DEBUG(this, "this is a debug message without context");
  }
};

TEST_F(LoggingTest, class_without_context) // NOLINT
{
  HasNoContextTest object;
  object.log_some_messages();
}

TEST_F(LoggingTest, log_with_zero) // NOLINT
{
  PSTLOG_ERROR(0, "this is an error message with 0"); // NOLINT
  PSTLOG_WARN(0, "this is a warn message with 0"); // NOLINT
  PSTLOG_INFO(0, "this is an info message with 0"); // NOLINT
  PSTLOG_DEBUG(0, "this is a debug message with 0"); // NOLINT
}

TEST_F(LoggingTest, log_with_nullptr) // NOLINT
{
  PSTLOG_ERROR(nullptr, "this is an error message with nullptr");
  PSTLOG_WARN(nullptr, "this is a warn message with nullptr");
  PSTLOG_INFO(nullptr, "this is an info message with nullptr");
  PSTLOG_DEBUG(nullptr, "this is a debug message with nullptr");
}

} // namespace ska::pst::common::test
