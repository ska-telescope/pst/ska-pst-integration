/*
 * Copyright 2022 Square Kilometre Array Observatory
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 * contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#include <chrono>

#include "ska_pst/common/definitions.h"
#include "ska_pst/common/utils/tests/TimeTest.h"
#include "ska_pst/common/utils/Logging.h"
#include "ska_pst/common/utils/Time.h"
#include "ska_pst/common/testutils/GtestMain.h"

auto main(int argc, char* argv[]) -> int
{
  return ska::pst::common::test::gtest_main(argc, argv);
}

namespace ska::pst::common::test {

  static constexpr time_t SKA_EPOCH_TIME(946684768);
  static constexpr uint16_t LOW_SAMPLE_PERIOD_NUMERATOR = 5184;
  static constexpr uint16_t LOW_SAMPLE_PERIOD_DENOMINATOR = 25;
  static constexpr uint16_t MID_SAMPLE_PERIOD_NUMERATOR = 3125;
  static constexpr uint16_t MID_SAMPLE_PERIOD_DENOMINATOR = 192;

  void TimeTest::SetUp()
  {
  }

  void TimeTest::TearDown()
  {
  }

  TEST_F(TimeTest, default_constructor) // NOLINT
  {
    Time epoch;
    EXPECT_EQ(epoch.get_gmtime(), "1970-01-01-00:00:00");
  }

  TEST_F(TimeTest, string_constructor) // NOLINT
  {
    Time epoch("2000-01-01-00:00:00");
    EXPECT_EQ(epoch.get_gmtime(), "2000-01-01-00:00:00");
  }

  TEST_F(TimeTest, set_time) // NOLINT
  {
    Time epoch;
    epoch.set_time("2000-01-01-00:00:00");
    EXPECT_EQ(epoch.get_gmtime(), "2000-01-01-00:00:00");
  }

  TEST_F(TimeTest, get_time) // NOLINT
  {
    Time epoch("2000-01-01-00:00:00");
    EXPECT_EQ(epoch.get_time(), 946684800);
  }

  TEST_F(TimeTest, get_gm_year) // NOLINT
  {
    Time epoch("2000-01-01-00:00:00");
    EXPECT_EQ(epoch.get_gm_year(), 2000);
  }

  TEST_F(TimeTest, get_gm_month) // NOLINT
  {
    Time epoch("2000-01-01-00:00:00");
    EXPECT_EQ(epoch.get_gm_month(), 0);
  }

  TEST_F(TimeTest, get_mjd_day) // NOLINT
  {
    Time epoch("2000-01-01-00:00:00");
    EXPECT_EQ(epoch.get_mjd_day(), 51544);
  }

  TEST_F(TimeTest, mjd2utctm) // NOLINT
  {
    static constexpr double base_mjd = 51544;
    static constexpr time_t base_epoch = 946684800;
    static constexpr double seconds_per_day = 86400;
    static constexpr double milliseconds_per_day = seconds_per_day * ska::pst::common::milliseconds_per_second;
    static constexpr unsigned seconds_to_test = 10;

    auto ntests = seconds_to_test * milliseconds_per_second;
    for (unsigned i = 0; i < ntests; i++)
    {
      double fractional_day = static_cast<double>(i) / milliseconds_per_day;
      double fractional_seconds = static_cast<double>(i) / ska::pst::common::milliseconds_per_second;
      time_t epoch = Time::mjd2utctm(base_mjd + fractional_day);
      time_t expected_epoch = base_epoch + static_cast<time_t>(floor(fractional_seconds));
      if (i % milliseconds_per_second >= (milliseconds_per_second / 2))
      {
        expected_epoch++;
      }
      EXPECT_EQ(epoch, expected_epoch);
    }
  }

  TEST_F(TimeTest, get_fractional_time) // NOLINT
  {
    Time epoch("2000-01-01-00:00:00.123");
    EXPECT_EQ(epoch.get_fractional_time(), 0.123);
  }

  TEST_F(TimeTest, add_seconds) // NOLINT
  {
    Time epoch("2000-01-01-00:00:00.123");
    epoch.add_seconds(1);
    EXPECT_EQ(epoch.get_gmtime(), "2000-01-01-00:00:01");
  }

  TEST_F(TimeTest, sub_seconds) // NOLINT
  {
    Time epoch("2000-01-01-00:00:00.123");
    epoch.sub_seconds(1);
    EXPECT_EQ(epoch.get_gmtime(), "1999-12-31-23:59:59");
  }

  TEST_F(TimeTest, get_localtime) // NOLINT
  {
    // determine the offset between UTC and localtime
    time_t t = time(nullptr);
    struct tm lt = {0};
    localtime_r(&t, &lt);
    std::cerr << "Offset to GMT is " << lt.tm_gmtoff << std::endl;

    Time epoch;
    epoch.set_time("2000-01-01-00:00:00");
    EXPECT_EQ(epoch.get_gmtime(), "2000-01-01-00:00:00");
    EXPECT_EQ(epoch.get_localtime(), "2000-01-01-00:00:00");
  }

  TEST_F(TimeTest, test_set_fractional_time) // NOLINT
  {
    Time epoch("2000-01-01-00:00:00");
    uint64_t attoseconds_per_decisecond = ska::pst::common::attoseconds_per_second / ska::pst::common::deciseconds_per_second;
    epoch.set_fractional_time_attoseconds(attoseconds_per_decisecond);
    EXPECT_EQ(epoch.get_fractional_time(), 0.1);
    EXPECT_EQ(epoch.get_fractional_time_attoseconds(), attoseconds_per_decisecond * 1);

    // ensure that it can be set multiple times with different values
    epoch.set_fractional_time_attoseconds(attoseconds_per_decisecond * 2);
    EXPECT_EQ(epoch.get_fractional_time(), 0.2);
    EXPECT_EQ(epoch.get_fractional_time_attoseconds(), attoseconds_per_decisecond * 2);

    static constexpr double seconds_per_decisecond = 0.1;
    epoch.set_fractional_time(seconds_per_decisecond);
    EXPECT_EQ(epoch.get_fractional_time(), seconds_per_decisecond);
    EXPECT_EQ(epoch.get_fractional_time_attoseconds(), attoseconds_per_decisecond);
  }

  TEST_F(TimeTest, test_set_fractional_time_limits) // NOLINT
  {
    Time epoch("2000-01-01-00:00:00");
    EXPECT_THROW(epoch.set_fractional_time_attoseconds(ska::pst::common::attoseconds_per_second), std::runtime_error);     // NOLINT
    EXPECT_THROW(epoch.set_fractional_time_attoseconds(ska::pst::common::attoseconds_per_second + 1), std::runtime_error); // NOLINT
    double fractional_seconds = 1.0;
    EXPECT_THROW(epoch.set_fractional_time(fractional_seconds), std::runtime_error);       // NOLINT
    EXPECT_THROW(epoch.set_fractional_time(fractional_seconds + 0.1), std::runtime_error); // NOLINT
  }

  TEST_F(TimeTest, test_creation_with_ska_epoch) // NOLINT
  {
    Time epoch(SKA_EPOCH_UTC);
    EXPECT_EQ(epoch.get_time(), SKA_EPOCH_TIME); // NOLINT
    EXPECT_EQ(epoch.get_fractional_time_attoseconds(), 0);
  }

  TEST_F(TimeTest, test_creation_with_samples_since_epoch) // NOLINT
  {
    Time epoch(42, 1, 1); // NOLINT
    EXPECT_EQ(epoch.get_time(), SKA_EPOCH_TIME);
    EXPECT_EQ(epoch.get_fractional_time_attoseconds(), 42000000000000); // NOLINT
  }

  TEST_F(TimeTest, test_creation_with_with_samples_since_epoch_low) // NOLINT
  {
    Time epoch(4766175669824, LOW_SAMPLE_PERIOD_NUMERATOR, LOW_SAMPLE_PERIOD_DENOMINATOR); // NOLINT
    EXPECT_EQ(epoch.get_time(), 1934998954);                                               // NOLINT
    EXPECT_EQ(epoch.get_fractional_time_attoseconds(), 894704640000000000);                // NOLINT

    epoch = Time(7661756698244, LOW_SAMPLE_PERIOD_NUMERATOR, LOW_SAMPLE_PERIOD_DENOMINATOR); // NOLINT
    EXPECT_EQ(epoch.get_time(), 2535426636);                                                 // NOLINT
    EXPECT_EQ(epoch.get_fractional_time_attoseconds(), 947875840000000000);                  // NOLINT

    epoch = Time(14070118556795, LOW_SAMPLE_PERIOD_NUMERATOR, LOW_SAMPLE_PERIOD_DENOMINATOR); // NOLINT
    EXPECT_EQ(epoch.get_time(), 3864264551);                                                  // NOLINT
    EXPECT_EQ(epoch.get_fractional_time_attoseconds(), 937011200000000000);                   // NOLINT
  }

  TEST_F(TimeTest, test_creation_with_with_samples_since_epoch_mid) // NOLINT
  {
    Time epoch(4766175669823, MID_SAMPLE_PERIOD_NUMERATOR, MID_SAMPLE_PERIOD_DENOMINATOR); // NOLINT
    EXPECT_EQ(epoch.get_time(), 1024259241);                                               // NOLINT
    EXPECT_EQ(epoch.get_fractional_time_attoseconds(), 792692057291666667);                // NOLINT

    epoch = Time(7661756698245, MID_SAMPLE_PERIOD_NUMERATOR, MID_SAMPLE_PERIOD_DENOMINATOR); // NOLINT
    EXPECT_EQ(epoch.get_time(), 1071387839);                                                 // NOLINT
    EXPECT_EQ(epoch.get_fractional_time_attoseconds(), 260498046875000000);                  // NOLINT

    epoch = Time(14070118556801, MID_SAMPLE_PERIOD_NUMERATOR, MID_SAMPLE_PERIOD_DENOMINATOR); // NOLINT
    EXPECT_EQ(epoch.get_time(), 1175690603);                                                  // NOLINT
    EXPECT_EQ(epoch.get_fractional_time_attoseconds(), 885432942708333333);                   // NOLINT
  }

  TEST_P(TimeTest, test_adjust_now_instance) // NOLINT
  {
    uint16_t sample_period_numerator{0};
    uint16_t sample_period_denominator{0};
    std::string cbf = std::string(GetParam());

    if (cbf == "Mid")
    {
      sample_period_numerator = MID_SAMPLE_PERIOD_NUMERATOR;
      sample_period_denominator = MID_SAMPLE_PERIOD_DENOMINATOR;
    }
    else if (cbf == "Low")
    {
      sample_period_numerator = LOW_SAMPLE_PERIOD_NUMERATOR;
      sample_period_denominator = LOW_SAMPLE_PERIOD_DENOMINATOR;
    }
    else
    {
      PSTLOG_WARN(this, "test_adjust_now_instance Unknown CBF value {}", cbf);
      FAIL();
    }

    PSTLOG_DEBUG(this, "test_adjust_now_instance cbf={}", cbf);

    auto now = time(nullptr);
    Time ska_epoch(SKA_EPOCH_UTC);
    Time epoch(now);

    // get the current fractional time in microseconds.
    uint64_t microseconds = std::chrono::duration_cast<std::chrono::microseconds>(
                                std::chrono::high_resolution_clock::now().time_since_epoch())
                                .count() %
                            1_mega;

    epoch.set_fractional_time_attoseconds(microseconds * attoseconds_per_microsecond);

    auto microseconds_since_ska_epoch = static_cast<uint64_t>(now - ska_epoch.get_time()) * 1_mega + microseconds;
    PSTLOG_DEBUG(this, "test_adjust_now_instance microseconds_since_ska_epoch={}", microseconds_since_ska_epoch);

    auto expected_samples_since_ska_epoch = microseconds_since_ska_epoch * sample_period_denominator / sample_period_numerator;
    PSTLOG_DEBUG(this, "test_adjust_now_instance expected_samples_since_ska_epoch={}", expected_samples_since_ska_epoch);

    auto frac_samples = (microseconds_since_ska_epoch * sample_period_denominator) % sample_period_numerator;
    PSTLOG_DEBUG(this, "test_adjust_now_instance frac_samples={}", frac_samples);
    if (frac_samples >= (sample_period_numerator + 1) / 2)
    {
      expected_samples_since_ska_epoch += 1;
    }
    auto samples_since_ska_epoch = epoch.adjust_to_nearest_sample(sample_period_numerator, sample_period_denominator);
    PSTLOG_DEBUG(this, "test_adjust_now_instance samples_since_ska_epoch={}", samples_since_ska_epoch);

    Time expected(expected_samples_since_ska_epoch, sample_period_numerator, sample_period_denominator);

    EXPECT_EQ(epoch.get_time(), expected.get_time());
    EXPECT_EQ(epoch.get_fractional_time_attoseconds(), expected.get_fractional_time_attoseconds());
    EXPECT_EQ(samples_since_ska_epoch, expected_samples_since_ska_epoch);
    EXPECT_EQ(epoch.get_samples_since_ska_epoch(sample_period_numerator, sample_period_denominator), expected_samples_since_ska_epoch);
  }

INSTANTIATE_TEST_SUITE_P(CorrelatorBeamFormer, TimeTest, testing::Values("Mid", "Low"), [](const testing::TestParamInfo<TimeTest::ParamType> &info)
                         { return std::string(info.param); });

} // namespace ska::pst::common::test
