/*
 * Copyright 2022 Square Kilometre Array Observatory
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 * contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#include <climits>

#include "ska_pst/common/utils/Logging.h"
#include "ska_pst/common/utils/RandomSequence.h"
#include "ska_pst/common/utils/Time.h"

#ifdef DEBUG
#include <stdio.h>
#endif

void ska::pst::common::RandomSequence::configure(const ska::pst::common::AsciiHeader& header)
{
  std::string utc_start_str = header.get_val("UTC_START");
  PSTLOG_DEBUG(this, "UTC_START={}", utc_start_str);

  ska::pst::common::Time utc_start(utc_start_str.c_str());
  seed_value = static_cast<uint64_t>(utc_start.get_time());
  PSTLOG_DEBUG(this, "seed_value={}", seed_value);

  reset();
  auto obs_offset = header.get<uint64_t>("OBS_OFFSET");
  PSTLOG_DEBUG(this, "OBS_OFFSET={}", obs_offset);
  seek(obs_offset);
}

void ska::pst::common::RandomSequence::reset()
{
  // reinitialise the internal state of the random-number engine
  PSTLOG_DEBUG(this, "generator.seed({})", seed_value);
  generator.seed(seed_value);
}

void ska::pst::common::RandomSequence::generate(uint8_t * buffer, uint64_t bufsz)
{
  // produces random integer values, uniformly distributed on the closed interval
  std::uniform_int_distribution<uint8_t> distribution(0, UCHAR_MAX);
  for (unsigned i=0; i<bufsz; i++)
  {
    buffer[i] = distribution(generator); // NOLINT
  }

  byte_offset += bufsz;
}

void ska::pst::common::RandomSequence::generate_block(uint8_t * buffer, uint64_t bufsz, uint64_t block_size, uint64_t block_stride)
{
  PSTLOG_DEBUG(this, "generate {} bytes of data with block size={} stride={}", bufsz, block_size, block_stride);
  uint64_t offset = 0;
  while (offset + block_size <= bufsz)
  {
    generate(buffer + offset, block_size); // NOLINT
    offset += block_stride;
  }
}

void ska::pst::common::RandomSequence::seek(uint64_t bufsz)
{
  // advances the internal state of the generator bufsz steps
  // note that the generator's discard method is unreliable, hence this manual step
  std::uniform_int_distribution<uint8_t> distribution(0, UCHAR_MAX);
  for (uint64_t i=0; i<bufsz; i++)
  {
    distribution(generator);
  }
}

auto ska::pst::common::RandomSequence::validate_block(uint8_t * buffer, uint64_t bufsz, uint64_t block_size, uint64_t block_stride) -> bool
{
  PSTLOG_DEBUG(this, "validate {} bytes of data with block size={} and stride={}", bufsz, block_size, block_stride);
  uint64_t offset = 0;
  bool valid = true;
  while (offset + block_size <= bufsz)
  {
    valid &= validate(buffer + offset, block_size); // NOLINT
    offset += block_stride;
  }
  PSTLOG_DEBUG(this, "valid={}", valid);
  return valid;
}

auto ska::pst::common::RandomSequence::validate(uint8_t * buffer, uint64_t bufsz) -> bool
{
  // produces random integer values, uniformly distributed on the closed interval
  std::uniform_int_distribution<uint8_t> distribution(0, UCHAR_MAX);

  uint64_t i = 0;
  while (i<bufsz)
  {
    const uint8_t expected = distribution(generator);

    if (buffer[i] == expected)  // NOLINT
    {
      i ++;
    }
    else
    {
      PSTLOG_DEBUG(this, "unexpected byte at index={}", i);

      unsigned zeroes = 0;
      while (i<bufsz && buffer[i] == 0) // NOLINT
      {
        i++;
        zeroes++;
      }

      if (zeroes > 1)
      {
        PSTLOG_DEBUG(this, "skipping {} consecutive zeroes", zeroes);
        seek (zeroes - 1);
      }
      else
      {
        // sequence is broken (data invalid)
        PSTLOG_ERROR(this, "expected sequence broken at i={}; bufsz={}", i, bufsz);
        break;
      }
    }
  }

  byte_offset += i;

  if (i == bufsz)
  {
    // sequence was not broken (data valid)
    PSTLOG_TRACE(this, "valid=true");
    return true;
  }

  uint64_t start_search_index = i + 1;
  uint8_t* remaining_buffer = buffer + start_search_index; // NOLINT
  uint64_t remaining_bufsz = bufsz - start_search_index;

  const unsigned expected_sequence_length = 8;
  if (search_buffer_for_expected_sequence (remaining_buffer, remaining_bufsz, expected_sequence_length) == -1)
  {
    start_search_index += expected_sequence_length;
    if (start_search_index < bufsz)
    {
      remaining_buffer = buffer + start_search_index; // NOLINT
      remaining_bufsz = bufsz - start_search_index;

      const unsigned max_offset = 8 * 1024 * 1024;
      search_expected_sequence_for_buffer (remaining_buffer, remaining_bufsz, max_offset);
    }
  }

  // sequence was broken (data invalid)
  PSTLOG_TRACE(this, "valid=false");
  return false;
}

auto ska::pst::common::RandomSequence::search_buffer_for_expected_sequence(
  uint8_t * buffer, uint64_t bufsz, uint64_t seqlen
) -> int64_t
{
  // avoid searching for a sequence longer than the input buffer
  seqlen = std::min (seqlen, bufsz);

  std::uniform_int_distribution<uint8_t> distribution(0, UCHAR_MAX);
  std::vector<uint8_t> test_sequence (seqlen);

  std::string sequence_str;
  for (uint64_t i=1; i<seqlen; i++)
  {
    test_sequence[i] = distribution(generator);

    const unsigned tmpsize = 8;
    std::array<char,tmpsize> temp; // NOLINT
    snprintf(temp.data(), tmpsize, " %02x", test_sequence[i]); // NOLINT
    sequence_str += temp.data();
  }

  PSTLOG_WARN(this, "sequence_str={}", sequence_str);

  unsigned matched = 0;
  uint64_t i = 0;

  while (i < bufsz && matched < seqlen)
  {
    if (buffer[i] == test_sequence[matched]) // NOLINT
    {
      matched ++;
    }
    else
    {
      matched = 0;
    }
    i++;
  }

  if (matched < seqlen)
  {
    PSTLOG_WARN(this, "sequence not found");
    return -1;
  }

  int64_t offset = static_cast<int64_t>(i) - static_cast<int64_t>(seqlen);
  PSTLOG_WARN(this, "sequence found at offset={}", offset);
  return offset;
}

auto ska::pst::common::RandomSequence::search_expected_sequence_for_buffer(
  uint8_t * buffer, uint64_t bufsz, uint64_t max_offset
) -> int64_t
{
  PSTLOG_WARN(this, "max_offset={}", max_offset);

  uint64_t matched = 0;
  uint64_t longest_match = 0;
  uint64_t offset = 0;

  std::uniform_int_distribution<uint8_t> distribution(0, UCHAR_MAX);

  while (matched < bufsz && offset < max_offset)
  {
    uint8_t expected = distribution(generator);

    offset ++;

    if (buffer[matched] == expected) // NOLINT
    {
      matched ++;
    }
    else
    {
      longest_match = std::max(matched,longest_match);
      matched = 0;
    }
  }

  if (matched == bufsz)
  {
    int64_t result = static_cast<int64_t>(offset) - static_cast<int64_t>(bufsz);
    PSTLOG_WARN(this, "match found at offset={}", result);
    return result;
  }

  PSTLOG_WARN(this, "match not found in first {} samples of expected sequence", max_offset);
  PSTLOG_WARN(this, "longest match={} out of {} buffer samples", longest_match, bufsz);

  return -1;
}
