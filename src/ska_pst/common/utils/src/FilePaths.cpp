/*
 * Copyright 2025 Square Kilometre Array Observatory
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 * contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#include "ska_pst/common/utils/FilePaths.h"
#include "ska_pst/common/utils/Logging.h"

#include <map>

ska::pst::common::FilePaths::FilePaths(const std::string& path):
  base_path(path)
{
}

ska::pst::common::FilePaths::FilePaths(std::filesystem::path path):
  base_path(std::move(path))
{
}

auto ska::pst::common::FilePaths::get_scan_product_path(const std::string& eb_id, const std::string& telescope, const std::string& scan_id) -> std::filesystem::path
{
  std::string subsystem_id = get_subsystem_from_telescope(telescope);
  return base_path / "product" / eb_id / subsystem_id / scan_id;
}

auto ska::pst::common::FilePaths::get_subsystem_from_telescope(const std::string& telescope) -> std::string
{
  //! mapping from telescope name to subsystem path
  std::map<std::string, std::string> subsystem_path_map {
    { "SKALow", "pst-low" },
    { "SKAMid", "pst-mid" },
  };

  if (subsystem_path_map.find(telescope) == subsystem_path_map.end())
  {
    PSTLOG_WARN(nullptr, "telescope={} did not map to a subsystem path", telescope);
    throw std::runtime_error("FilePaths::get_subsystem_from_telescope could not map telescope to a subsystem path.");
  }
  return subsystem_path_map[telescope];
}
