/*
 * Copyright 2023 Square Kilometre Array Observatory
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 * contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#include "ska_pst/common/utils/Logging.h"
#include "ska_pst/common/utils/PacketCaptureFile.h"
#include "ska_pst/common/utils/PacketUtils.h"

ska::pst::common::PacketCaptureFile::PacketCaptureFile(std::shared_ptr<ska::pst::common::PacketLayout> _layout) :
  ska::pst::common::PacketGenerator(std::move(_layout))
{
  PSTLOG_TRACE(this, "ctor data={} scales={} weights={}",
    layout->get_packet_data_size(), layout->get_packet_scales_size(), layout->get_packet_weights_size());
  packet_data.resize(layout->get_packet_data_size());
  packet_scales.resize(layout->get_packet_scales_size());
  packet_weights.resize(layout->get_packet_weights_size());
  pcap_err_buf.resize(PCAP_ERRBUF_SIZE);

  static constexpr size_t max_ethernet_size_bytes = 65536;
  ethernet_frame.resize(max_ethernet_size_bytes, 0);
}

ska::pst::common::PacketCaptureFile::~PacketCaptureFile()
{
  PSTLOG_TRACE(this, "dtor closing pcap file");
  close_pcap_file();
}

void ska::pst::common::PacketCaptureFile::configure(const ska::pst::common::AsciiHeader& config)
{
  PSTLOG_DEBUG(this, "PacketGenerator::configure(config)");
  ska::pst::common::PacketGenerator::configure(config);

  pcap_file_name = config.get_val("PCAP_FILE_NAME");
  open_pcap_file();
}

void ska::pst::common::PacketCaptureFile::open_pcap_file()
{
  PSTLOG_DEBUG(this, "opening pcap file {}", pcap_file_name);
  pcap_handle = pcap_open_offline(pcap_file_name.c_str(), &pcap_err_buf[0]);
  if (pcap_handle == nullptr)
  {
    PSTLOG_ERROR(this, "pcap_open_offline({}) failed: {}", pcap_file_name, std::string(pcap_err_buf.begin(), pcap_err_buf.end()));
    throw std::runtime_error("PacketCaptureFile configure failed to open PCAP file.");
  }
}

void ska::pst::common::PacketCaptureFile::close_pcap_file()
{
  if (pcap_handle)
  {
    PSTLOG_DEBUG(this, "closing PCAP file");
    pcap_close(pcap_handle);
  }
  pcap_handle = nullptr;
}

void ska::pst::common::PacketCaptureFile::read_packet(bool need_new_packet)
{
  if (!need_new_packet)
  {
    return;
  }
  const unsigned char * packet_contents{nullptr};
  int rv = pcap_next_ex(pcap_handle, &header, &packet_contents);
  if (rv == 0)
  {
    PSTLOG_WARN(this, "pcap_next_ex returned 0: {}", pcap_geterr(pcap_handle));
    throw std::runtime_error("PacketCaptureFile read_packet failed to read packet from file");
  }
  else if (rv == -1)
  {
    PSTLOG_ERROR(this, "pcap_next_ex returned -1: {}", pcap_geterr(pcap_handle));
    throw std::runtime_error("PacketCaptureFile read_packet failed to read packet from file");
  }
  else if (rv == -2)
  {
    PSTLOG_INFO(this, "reached end of file");
    throw std::runtime_error("End of PCAP file encountered");
  }

  static constexpr int32_t minimum_useful_packet_length = 64;
  if (header->len < minimum_useful_packet_length)
  {
    PSTLOG_DEBUG(this, "ignoring smaller packet < 64 bytes, len={}", header->len);
    return;
  }

  memcpy(&ethernet_frame[0], packet_contents, header->len);

  // strip the ethernet, IP and UDP wrappers from the UDP payload
  ska::pst::common::PacketBuffer payload = ska::pst::common::udp_from_ethernet(&ethernet_frame[0], header->len);

  #ifdef _TRACE
  ska::pst::recv::cbf_psr_header_t * hdr = reinterpret_cast<ska::pst::recv::cbf_psr_header_t *>(payload.data());
  print_packet_header(hdr);
  #endif

  memcpy(&packet_scales[0], payload.data() + layout->get_packet_scales_offset(), layout->get_packet_scales_size()); // NOLINT
  memcpy(&packet_weights[0], payload.data() + layout->get_packet_weights_offset(), layout->get_packet_weights_size()); // NOLINT
  memcpy(&packet_data[0], payload.data() + layout->get_packet_data_offset(), layout->get_packet_data_size()); // NOLINT

  need_data = false;
  need_weights = false;
  need_scales = false;
  return;
}

void ska::pst::common::PacketCaptureFile::fill_data(char * buf, uint64_t size)
{
  read_packet(need_data);
  if (size != layout->get_packet_data_size())
  {
    PSTLOG_ERROR(this, "requested size [{}] but only single packet size [{}] is supported", size, layout->get_packet_data_size());
    throw std::runtime_error("PacketCaptureFile fill_data unsupported size requested");
  }
  memcpy(buf, &packet_data[0], size);  // NOLINT
  need_data = true;
}

void ska::pst::common::PacketCaptureFile::fill_weights(char * buf, uint64_t size)
{
  read_packet(need_weights);
  if (size != layout->get_packet_weights_size())
  {
    PSTLOG_ERROR(this, "requested size [{}] but only single packet size [{}] is supported", size, layout->get_packet_weights_size());
    throw std::runtime_error("PacketCaptureFile fill_weights unsupported size requested");
  }
  memcpy(buf, &packet_weights[0], size); // NOLINT
  need_weights = true;
}

void ska::pst::common::PacketCaptureFile::fill_scales(char * buf, uint64_t size)
{
  read_packet(need_scales);
  if (size != layout->get_packet_scales_size())
  {
    PSTLOG_ERROR(this, "requested size [{}] but only single packet size [{}] is supported", size, layout->get_packet_scales_size());
    throw std::runtime_error("PacketCaptureFile fill_scales unsupported size requested");
  }
  memcpy(buf, &packet_scales[0], size); // NOLINT
  need_scales = true;
}

auto ska::pst::common::PacketCaptureFile::test_data(char * buf, uint64_t size) -> bool
{
  read_packet(need_data);
  if (size != layout->get_packet_data_size())
  {
    PSTLOG_ERROR(this, "requested size [{}] but only single packet size [{}] is supported", size, layout->get_packet_data_size());
    throw std::runtime_error("PacketCaptureFile test_data unsupported size requested");
  }

  bool data_valid = true;
  for (unsigned i=0; i<size; i++)
  {
    data_valid &= (buf[i] == packet_data[i]); // NOLINT
  }
  need_data = true;
  return data_valid;
}

auto ska::pst::common::PacketCaptureFile::test_weights(char * buf, uint64_t size) -> bool
{
  read_packet(need_weights);
  if (size != layout->get_packet_weights_size())
  {
    PSTLOG_ERROR(this, "requested size [{}] but only single packet size [{}] is supported", size, layout->get_packet_weights_size());
    throw std::runtime_error("PacketCaptureFile test_weights unsupported size requested");
  }

  bool weights_valid = true;
  for (unsigned i=0; i<size; i++)
  {
    weights_valid &= (buf[i] == packet_weights[i]); // NOLINT
  }
  need_weights = true;
  return weights_valid;
}

auto ska::pst::common::PacketCaptureFile::test_scales(char * buf, uint64_t size) -> bool
{
  read_packet(need_scales);
  if (size != layout->get_packet_scales_size())
  {
    PSTLOG_ERROR(this, "requested size [{}] but only single packet size [{}] is supported", size, layout->get_packet_scales_size());
    throw std::runtime_error("PacketCaptureFile test_scales unsupported size requested");
  }

  bool scales_valid = true;
  for (unsigned i=0; i<size; i++)
  {
    scales_valid &= (buf[i] == packet_scales[i]); // NOLINT
  }
  need_scales = true;
  return scales_valid;
}

void ska::pst::common::PacketCaptureFile::reset()
{
  PSTLOG_TRACE(this, "pcap_handle={}", reinterpret_cast<void *>(pcap_handle));
  close_pcap_file();
  open_pcap_file();
  need_data = need_scales = need_weights = true;
}

void ska::pst::common::PacketCaptureFile::print_packet_header(ska::pst::recv::cbf_psr_header_t * header)
{
  PSTLOG_INFO(this, "CBF version: {}.{}", uint32_t(header->cbf_version_major), uint32_t(header->cbf_version_minor));
  PSTLOG_INFO(this, "Packet Sequence Number: {}", uint64_t(header->packet_sequence_number));
  PSTLOG_INFO(this, "First Channel Number: {}", uint32_t(header->first_channel_number));
  PSTLOG_INFO(this, "First Channel Freq: {} millihertz", uint64_t(header->first_channel_freq));
  PSTLOG_INFO(this, "Samples since SKA Epoch: {}", uint64_t(header->samples_since_ska_epoch));
  PSTLOG_INFO(this, "Sample period (microseconds): {}/{}", uint16_t(header->sample_period_numerator), uint64_t(header->sample_period_denominator));
  PSTLOG_INFO(this, "Channels per packet: number={} valid={}", uint32_t(header->channels_per_packet), uint64_t(header->valid_channels_per_packet));
  PSTLOG_INFO(this, "Time samples: per packet={} per relative weight={}", uint32_t(header->time_samples_per_packet), uint32_t(header->num_time_samples_per_relative_weight));
  PSTLOG_INFO(this, "Beam Number: {}", uint32_t(header->beam_number));
  PSTLOG_INFO(this, "Packet Destination: {}", uint32_t(header->packet_destination));
  PSTLOG_INFO(this, "Data Precision: {} bits/value", uint32_t(header->data_precision));
  PSTLOG_INFO(this, "Station beam polynomials applied: {}", uint8_t(header->station_beam_polynomials_applied));
  PSTLOG_INFO(this, "PST beam polynomials applied: {}", uint8_t(header->pst_beam_polynomials_applied));
  PSTLOG_INFO(this, "Jones polarisation corrections applied: {}", uint8_t(header->jones_polarisation_corrections_applied));
  PSTLOG_INFO(this, "Scan ID: {}", uint32_t(header->scan_id));
  PSTLOG_INFO(this, "Scales: {} {} {} {}", float(header->scale_1), float(header->scale_2), float(header->scale_3), float(header->scale_4));
  PSTLOG_INFO(this, "Offsets: {} {} {} {}", float(header->offset_1), float(header->offset_2), float(header->offset_3), float(header->offset_4));
}
