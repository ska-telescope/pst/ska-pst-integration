/*
 * Copyright 2022 Square Kilometre Array Observatory
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 * contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#include "ska_pst/common/definitions.h"
#include "ska_pst/common/utils/Logging.h"
#include "ska_pst/common/utils/Time.h"

#include <cmath>
#include <string>
#include <iostream>
#include <iomanip>
#include <sstream>
#include <vector>

auto ska::pst::common::Time::fractional_multiply(uint64_t value, uint64_t p, uint64_t q) -> std::pair<uint64_t, uint64_t>
{
  // do some integer maths to help avoid overflow
  // value = A * q + r - where A is value div q, and r is value mod q
  //
  // result = value * p / q
  // result = (A * q + r) * p / q
  // result = A * p + r * p / q
  // the A * p is an integer, the integer part of (r*p)/q is r*p div q, and frac is (r * p) mod q / q

  auto value_div_q = value / q;
  auto value_mod_q = value % q;

  auto result_whole = value_div_q * p + (value_mod_q * p) / q;
  auto result_rem = (value_mod_q * p) % q;

  return std::make_pair(result_whole, result_rem);
}

ska::pst::common::Time::Time(const char * s)
{
  set_time(s);
}

ska::pst::common::Time::Time(time_t now) :
  epoch(now)
{
}

ska::pst::common::Time::Time(uint64_t samples_since_ska_epoch, uint16_t sample_period_numerator, uint16_t sample_period_denominator)
{
  set_samples_since_ska_epoch(samples_since_ska_epoch, sample_period_numerator, sample_period_denominator);
}

void ska::pst::common::Time::set_time(const std::string &timestamp)
{
  std::istringstream iss;
  iss.str(std::string(timestamp));
  std::string token;
  char delim = '.';

  // check for YYYY-MM-DD-HH:MM:SS.ssss
  if (std::getline(iss, token, delim))
  {
    std::string utc_string;
    utc_string.append(token);
    utc_string.append(" UTC");
    const char * format = "%Y-%m-%d-%H:%M:%S %Z";
    struct tm time_tm = STRUCT_TM_INIT;
    char * first_unprocessed = strptime(utc_string.c_str(), format, &time_tm);
    if (first_unprocessed == nullptr)
    {
      epoch = 0;
      std::cerr << "Warning: "<< timestamp << " not a valid timestamp, using " << get_gmtime() << " instead" << std::endl;
    }
    else
    {
      epoch = timegm(&time_tm);
    }
  }

  if (std::getline(iss, token, delim))
  {
    auto num_decimal = static_cast<int32_t>(token.size());
    static constexpr int base10 = 10;
    double seconds_conversion = pow(static_cast<double>(base10), num_decimal);
    try
    {
      double fractional_seconds = static_cast<double>(std::stoi(token)) / seconds_conversion;
      set_fractional_time(fractional_seconds);
    }
    catch  (std::exception& exc)
    {
      attoseconds = 0;
    }
  }
}

auto ska::pst::common::Time::mjd2utctm(double mjd) -> time_t
{
  static constexpr int seconds_in_day = 86400;
  static constexpr int seconds_per_minute = 60;
  static constexpr int minutes_per_hour = 60;
  static constexpr int seconds_per_hour = minutes_per_hour * seconds_per_minute;
  static constexpr int julian_day_epoch = 2400001;

  auto days = static_cast<int>(mjd);
  double fdays = mjd - static_cast<double>(days);
  double seconds = fdays * static_cast<double>(seconds_in_day);
  auto secs = static_cast<int>(seconds);
  double fracsec = seconds - static_cast<double>(secs);

  static constexpr int milliseconds_in_second = 1_kilo;
  if (static_cast<int>(rint(fracsec*milliseconds_in_second)) >= milliseconds_in_second/2)
  {
    secs++;
  }

  int julian_day = days + julian_day_epoch;
  int n_four = 4  * (julian_day+((6*((4*julian_day-17918)/146097))/4+1)/2-37); // NOLINT
  int n_dten = 10 * (((n_four-237)%1461)/4) + 5; // NOLINT

  struct tm gregdate = STRUCT_TM_INIT;
  gregdate.tm_year = n_four/1461 - 4712 - 1900; // NOLINT extra -1900 for C struct tm
  gregdate.tm_mon  = (n_dten/306+2)%12;         // NOLINT struct tm mon 0->11
  gregdate.tm_mday = (n_dten%306)/10 + 1;       // NOLINT

  gregdate.tm_hour = secs / seconds_per_hour;
  secs -= seconds_per_hour * gregdate.tm_hour;

  gregdate.tm_min = secs / seconds_per_minute;
  secs -= seconds_per_minute * (gregdate.tm_min);

  gregdate.tm_sec = secs;

  gregdate.tm_isdst = -1;
  time_t date = mktime (&gregdate);

  return date;
}

auto ska::pst::common::Time::get_localtime() -> std::string
{
  return ska::pst::common::Time::format_localtime(epoch);
}

auto ska::pst::common::Time::get_gmtime() -> std::string
{
  return ska::pst::common::Time::format_gmtime(epoch);
}

auto ska::pst::common::Time::get_fractional_time() -> double
{
  return static_cast<double>(attoseconds) / static_cast<double>(ska::pst::common::attoseconds_per_second);
}

auto ska::pst::common::Time::get_fractional_time_attoseconds() -> uint64_t
{
  return attoseconds;
}

void ska::pst::common::Time::set_fractional_time(double fractional_seconds)
{
  if (fractional_seconds < 0)
  {
    throw std::runtime_error("Time set_fractional_time seconds was < 0");
  }
  if (fractional_seconds >= 1.0)
  {
    throw std::runtime_error("Time set_fractional_time seconds was >= 1");
  }
  double fractional_attoseconds = fractional_seconds * static_cast<double>(ska::pst::common::attoseconds_per_second);
  attoseconds = static_cast<uint64_t>(roundl(fractional_attoseconds));
}

void ska::pst::common::Time::set_fractional_time_attoseconds(uint64_t _attoseconds)
{
  if (_attoseconds >= ska::pst::common::attoseconds_per_second)
  {
    throw std::runtime_error("Time set_fractional_time attoseconds >= 1e18");
  }
  attoseconds = _attoseconds;
}

auto ska::pst::common::Time::format_localtime(time_t e) -> std::string
{
  std::ostringstream os;
  const char * format = "%Y-%m-%d-%H:%M:%S";
  struct tm * timeinfo = localtime(&e);
  os << std::put_time(timeinfo, format);
  return os.str();
}

auto ska::pst::common::Time::format_gmtime(time_t e) -> std::string
{
  std::ostringstream os;
  const char * format = "%Y-%m-%d-%H:%M:%S";
  struct tm * timeinfo = gmtime(&e);
  os << std::put_time(timeinfo, format);
  return os.str();
}

auto ska::pst::common::Time::get_gm_year() -> int
{
  struct tm * timeinfo = gmtime(&epoch);
  static constexpr int year_1900 = 1900;
  return timeinfo->tm_year + year_1900;
}

auto ska::pst::common::Time::get_gm_month() -> int
{
  struct tm * timeinfo = gmtime(&epoch);
  return timeinfo->tm_mon;
}

void ska::pst::common::Time::set_samples_since_ska_epoch(uint64_t samples_since_ska_epoch, uint16_t sample_period_numerator, uint16_t sample_period_denominator)
{
  set_time(SKA_EPOCH_UTC);
  // optimisation, setting
  if (samples_since_ska_epoch == 0)
  {
    attoseconds = 0;
    return;
  }

  auto p = static_cast<uint64_t>(sample_period_numerator);
  auto q = static_cast<uint64_t>(sample_period_denominator);
  auto usec_pair = fractional_multiply(samples_since_ska_epoch, p, q);
  PSTLOG_TRACE(this, "usec_pair=({},{})", usec_pair.first, usec_pair.second);

  uint64_t seconds_since_epoch = usec_pair.first / 1_mega;
  PSTLOG_TRACE(this, "seconds_since_epoch={}", seconds_since_epoch);
  add_seconds(static_cast<uint32_t>(seconds_since_epoch));

  uint64_t usec_as_attoseconds = (usec_pair.first % 1_mega) * ska::pst::common::attoseconds_per_microsecond;
  auto usec_frac_as_attoseconds = static_cast<uint64_t>(roundl(
      static_cast<double>(usec_pair.second) / static_cast<double>(q) * static_cast<double>(ska::pst::common::attoseconds_per_microsecond)));

  attoseconds = usec_as_attoseconds + usec_frac_as_attoseconds;
  PSTLOG_TRACE(this, "attoseconds={}", attoseconds);
}

auto ska::pst::common::Time::adjust_to_nearest_sample(uint16_t sample_period_numerator, uint16_t sample_period_denominator) -> uint64_t
{
  auto samples_since_ska_epoch = get_samples_since_ska_epoch(sample_period_numerator, sample_period_denominator);

  set_samples_since_ska_epoch(samples_since_ska_epoch, sample_period_numerator, sample_period_denominator);
  return samples_since_ska_epoch;
}

auto ska::pst::common::Time::get_samples_since_ska_epoch(uint16_t sample_period_numerator, uint16_t sample_period_denominator) -> uint64_t
{
  // need to get seconds from SKA Epoch
  // get the integer num of usec out of attoseconds
  // 1e6 seconds + usec can be converted to whole PSN plus a remainder
  auto ska_epoch = Time(SKA_EPOCH_UTC);
  PSTLOG_TRACE(this, "ska_epoch.epoch={}", ska_epoch.epoch);

  if (ska_epoch.epoch >= epoch)
  {
    // This is to ensure no underflow happening.
    return 0;
  }

  uint64_t seconds_since_ska_epoch = epoch - ska_epoch.epoch;
  uint64_t usecs = seconds_since_ska_epoch * 1_mega + attoseconds / ska::pst::common::attoseconds_per_microsecond;
  PSTLOG_TRACE(this, "seconds_since_ska_epoch={}, usecs={}", seconds_since_ska_epoch, usecs);

  // NOTE - that is opposite for when going from PSN -> time
  auto p = static_cast<uint64_t>(sample_period_denominator);
  auto q = static_cast<uint64_t>(sample_period_numerator);

  // the first part of the pair is the whole number of PSNs.
  // the second part of the pair is remainder. x / q is between 0.0 and 1.0
  auto sample_number_pair = fractional_multiply(usecs, p, q);
  PSTLOG_TRACE(this, "sample_number_pair=({},{})", sample_number_pair.first, sample_number_pair.second);
  auto sample_number = sample_number_pair.first;

  // need to get the fractional part of the microsecond
  auto frac_usecs = static_cast<double>(attoseconds % ska::pst::common::attoseconds_per_microsecond) / static_cast<double>(ska::pst::common::attoseconds_per_microsecond);
  PSTLOG_TRACE(this, "frac_usecs={}", frac_usecs);

  auto frac_sample_number = static_cast<uint64_t>(roundl(
      ((static_cast<double>(p) * frac_usecs) + static_cast<double>(sample_number_pair.second)) / static_cast<double>(q)));

  PSTLOG_TRACE(this, "frac_sample_number={}", frac_sample_number);

  sample_number += frac_sample_number;

  PSTLOG_DEBUG(this, "sample_number={}", sample_number);
  return sample_number;
}
