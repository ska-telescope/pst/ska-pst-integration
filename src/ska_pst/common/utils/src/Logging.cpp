/*
 * Copyright 2022 Square Kilometre Array Observatory
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 * contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#include "ska_pst/common/utils/Logging.h"

#include <algorithm>
#include <sstream>

void ska::pst::common::setup_spdlog() {
  auto formatter = std::make_unique<spdlog::pattern_formatter>();
  formatter->add_flag<ska::pst::common::tags_formatter>('*').set_pattern(SKA_LOGGING_FORMAT);
  spdlog::set_formatter(std::move(formatter));
}

void ska::pst::common::tags_formatter::format(const spdlog::details::log_msg &, const std::tm &, spdlog::memory_buf_t &dest)
{
  auto &mdc_map = spdlog::mdc::get_context();
  std::stringstream ss;
  if (!mdc_map.empty())
  {
    ss << "{";
    auto last_element = --mdc_map.end();
    for (auto it = mdc_map.begin(); it != mdc_map.end(); ++it) {
        auto &pair = *it;
        const auto &key = pair.first;
        const auto &value = pair.second;

        ss << "'" << key << "': " << "'" << value << "'";
        if (it != last_element) {
            ss << ", ";
        }
    }
    ss << "}";
  }
  auto tags = ss.str();
  dest.append(tags.data(), tags.data() + tags.size()); // NOLINT
}

auto ska::pst::common::tags_formatter::clone() const -> std::unique_ptr<custom_flag_formatter>
{
  return spdlog::details::make_unique<tags_formatter>();
}

auto ska::pst::common::mapped_diagnostic_context(std::nullptr_t) -> void *
{
  return nullptr;
}
