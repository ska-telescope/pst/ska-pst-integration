/*
 * Copyright 2023 Square Kilometre Array Observatory
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 * contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#include "ska_pst/common/utils/PacketGenerator.h"
#include "ska_pst/recv/formats/PacketStructure.h"

#include <complex>
#include <pcap.h>
#include <vector>

#ifndef SKA_PST_COMMON_UTILS_PacketCaptureFile_h
#define SKA_PST_COMMON_UTILS_PacketCaptureFile_h

namespace ska::pst::common {

  /**
   * @brief Generates packet streams from a recorded PCAP file
   *
   */
  class PacketCaptureFile : public PacketGenerator
  {
    public:

      /**
       * @brief Construct a new PacketCaptureFile object
       *
       */
      explicit PacketCaptureFile(std::shared_ptr<PacketLayout> layout);

      /**
       * @brief Destroy the PacketCaptureFile object
       *
       */
      ~PacketCaptureFile();

      /**
       * @brief Configure the streams written to data + weights
       *
       * @param config contains the PCAP_FILE_NAME keyword/value pair to extract the filename to be used.
       */
      void configure(const ska::pst::common::AsciiHeader& config) override;

      /**
       * @brief Fill the buffer with a sequence of data
       *
       * @param buf base memory address of the buffer to be filled
       * @param size number of bytes to be written to buffer
       */
      void fill_data(char * buf, uint64_t size) override;

      /**
       * @brief Verify the data stream in the provided buffer
       *
       * @param buf pointer to buffer containing sequence of data to be verified
       * @param size number of bytes in buffer to be tested
       *
       * @return true if data match expectations
       */
      auto test_data(char * buf, uint64_t size) -> bool override;
     /**
       * @brief Fill the buffer with weights from the packet
       *
       * @param buf base memory address of the buffer to be filled
       * @param size number of bytes to be written to buffer
       */
      void fill_weights(char * buf, uint64_t size) override;

      /**
       * @brief Fill the buffer with a scale factor from the packet
       *
       * @param buf base memory address of the buffer to be filled
       * @param size number of bytes to be written to buffer
       */
      void fill_scales(char * buf, uint64_t size) override;

      /**
       * @brief Verify the weights stream in the provided buffer
       *
       * @param buf pointer to buffer containing sequence of weights to be verified
       * @param size number of bytes in buffer to be tested
       *
       * @return true if weights match expectations
       */
      auto test_weights(char * buf, uint64_t size) -> bool override;

      /**
       * @brief Verify the scales stream in the provided buffer
       *
       * @param buf pointer to buffer containing the scale factor to be verified
       * @param size number of bytes in buffer to be tested
       *
       * @return true if scales match expectations
       */
      auto test_scales(char * buf, uint64_t size) -> bool override;

      /**
       * @brief Reset the sequence of packets being read from the PCAP file.
       * The next call to fill_block or test_block will behave as per the first call to these functions.
       *
       */
      void reset() override;

    private:

      /**
       * @brief Open the pcap capture file specified by the pcap_file_name attribute
       *
       */
      void open_pcap_file();

      /**
       * @brief Close the pcap handle, if open
       *
       */
      void close_pcap_file();

      /**
       * @brief Read a single file PCAP file, writing the data, weights and scales to the
       * packet_data, packet_weights and packet_scales buffers. If the need_new_packet is
       * false, this method does nothing.
       *
       * @param need_new_packet flag for reading a new packet from the pcap_handle
       */
      void read_packet(bool need_new_packet);

      /**
       * @brief Print the contents of the CBF/PSR header to stdout
       *
       * @param header CBF/PSR header to print
       */
      void print_packet_header(ska::pst::recv::cbf_psr_header_t * header);

      //! file name of the pcap file
      std::string pcap_file_name;

      //! handle to the opened pcap file
      pcap_t * pcap_handle{nullptr};

      //! buffer to receive error messages from libpcap
      std::vector<char> pcap_err_buf;

      //! pointer to the pcap header structure
      struct pcap_pkthdr * header{nullptr};

      //! memory to hold the received ethernet frame
      std::vector<char> ethernet_frame;

      bool need_data{true};
      bool need_weights{true};
      bool need_scales{true};

      std::vector<char> packet_data;
      std::vector<char> packet_weights;
      std::vector<char> packet_scales;
  };

}  // namespace ska::pst::common

#endif // SKA_PST_COMMON_UTILS_PacketCaptureFile_h
