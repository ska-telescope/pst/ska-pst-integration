/*
 * Copyright 2025 Square Kilometre Array Observatory
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 * contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#include <filesystem>
#include <string>

#ifndef SKA_PST_COMMON_UTILS_FilePaths_h
#define SKA_PST_COMMON_UTILS_FilePaths_h

namespace ska::pst::common {

  class FilePaths {

    public:

      /**
       * @brief Construct a new File Paths object
       *
       * @param base_path base path to which the file paths are relative
       */
      FilePaths(const std::string& base_path);

      /**
       * @brief Construct a new File Paths object
       *
       * @param base_path base path to which the file paths are relative
       */
      FilePaths(std::filesystem::path base_path);

      /**
       * @brief Get the path for scan data products, relative to the base_path.
       *
       * @param eb_id execution block id
       * @param telescope name of the telescope [SKALow or SKAMid]
       * @param scan_id scan id
       * @return std::filesystem::path the data product path, relative to the base_path
       */
      std::filesystem::path get_scan_product_path(const std::string& eb_id, const std::string& telescope, const std::string& scan_id);

      /**
       * @brief Get the subsystem id from telescope
       *
       * @param telescope the telescope name [SKALow or SKAMid]
       * @return std::string the subsystem id matching the telescope name
       */
      static std::string get_subsystem_from_telescope(const std::string& telescope);

    private:

      //! base directory to which files should be written
      std::filesystem::path base_path;

  };

} // namespace ska::pst::common

#endif // SKA_PST_COMMON_UTILS_FilePaths_h
