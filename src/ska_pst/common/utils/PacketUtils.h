/*
 * Copyright 2022 Square Kilometre Array Observatory
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 * contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#include <memory>
#include <vector>
#include <cstdint>
#include <string>

#include "ska_pst/common/utils/Endian.h"

#ifndef SKA_PST_COMMON_UTILS_PacketUtils_h
#define SKA_PST_COMMON_UTILS_PacketUtils_h

namespace ska::pst::common {

  //! representation of the MAC address
  typedef std::array<uint8_t, 6> mac_address;

  //! representation of IPv4 address
  typedef std::array<uint8_t, 4> ip_addr_v4;

  /**
   * @brief Generic Interface to raw packet data stored in memory.
   * Provides a base class implemented for Ethernet Frames, IPV4 and UDP packets.
   *
   */
  class PacketBuffer
  {
    public:
      /**
       * @brief Construct a new Packet Buffer object
       *
       */
      PacketBuffer();

      /**
       * @brief Construct a new Packet Buffer object with a memory address and size
       *
       * @param ptr memory address of the packet
       * @param length length of the packet
       */
      PacketBuffer(char *ptr, size_t length);

      /**
       * @brief Return a pointer to packet memory address
       *
       * @return char* pointer to the packet memory address
       */
      char * data() const;

      /**
       * @brief Return the size of the packet
       *
       * @return size_t size of the packet
       */
      size_t size() const;

    protected:

      /**
       * @brief Return a little endian representation of data in the packet
       *
       * @tparam T type of data to be returned
       * @param offset offset from the base memory address in the packet
       * @return T data returned
       */
      template<typename T>
      T get(size_t offset) const
      {
        T out;
        std::memcpy(&out, ptr + offset, sizeof(out));
        return out;
      }

      /**
       * @brief Return a big endian representation of data in the packet
       *
       * @tparam T type of data to be returned
       * @param offset offset from the base memory address in the packet
       * @return T data returned
       */
      template<typename T>
      T get_be(size_t offset) const
      {
        return betoh(get<T>(offset));
      }

    private:
      char * ptr;
      size_t length;
  };

  /**
   * @brief UDP Packet Interface to raw data stored in memory
   *
   */
  class UDPPacket : public PacketBuffer
  {
    public:
      //! minimum size of the packet
      static constexpr size_t min_size = 8;

      //! definition of the UDP protocol in the packet
      static constexpr uint8_t protocol = 0x11;

      /**
       * @brief Construct a new UDPPacket object
       *
       */
      UDPPacket() = default;

      /**
       * @brief Construct a new UDPPacket object from an existing address and size
       *
       * @param ptr memory address of the packet
       * @param size length of the packet
       */
      UDPPacket(char *ptr, size_t size);

      /**
       * @brief Return the source UDP port in the packet header
       *
       * @return uint16_t source UDP port
       */
      uint16_t source_port() const { return get_be<uint16_t>(0); };

      /**
       * @brief Return the destination UDP port in the packet header
       *
       * @return uint16_t destination UDP port
       */
      uint16_t destination_port() const { return get_be<uint16_t>(2); };

      /**
       * @brief Return the length in the packet header
       *
       * @return uint16_t packet length
       */
      uint16_t length() const { return get_be<uint16_t>(4); };

      /**
       * @brief Return the UDP checksum of the packet header
       *
       * @return uint16_t UDP checksum
       */
      uint16_t checksum() const { return get_be<uint16_t>(6); };

      /**
       * @brief Return the UDP packet payload as a PacketBuffer
       *
       * @return PacketBuffer UDP packet payload
       */
      PacketBuffer payload() const;
  };

  /**
   * @brief Internet Protocol version 4 (IPV4) Interface to raw data stored in memory
   *
   */
  class IPv4Packet : public PacketBuffer
  {
    public:
      //! minimum size of an IPv4 packet
      static constexpr size_t min_size = 20;
      //! value for the ethertype field in an IPv4 packet
      static constexpr uint16_t ethertype = 0x0800;
      //! value of flags_frag_off field for do not fragment
      static constexpr uint16_t flag_do_not_fragment = 0x4000;
      //! value of flags_frag_off field for more fragments
      static constexpr uint16_t flag_more_fragments = 0x2000;
      //! bitmask to apply to flag_more_fragments to extract value
      static constexpr uint16_t flag_more_fragments_bitmask = 0x1fff;
      //! bitmask to apply to version_ihl() output to return header length
      static constexpr uint8_t header_length_bitmask = 0xf;

      /**
       * @brief Construct a new IPv4Packet object
       *
       */
      IPv4Packet() = default;

      /**
       * @brief Construct a new IPv4Packet object from an existing address and size
       *
       * @param ptr memory address of the packet
       * @param size length of the packet
       */
      IPv4Packet(char *ptr, size_t size);

      //! Return the version and internet header length of the packet
      uint8_t version_ihl() const { return get<uint8_t>(0); };
      //! Return the Differentiated Services Code Point of the packet
      uint8_t dscp_ecn() const { return get<uint8_t>(1); };
      //! Return the total length (header and data) of the packet in bytes
      uint16_t total_length() const{ return get_be<uint16_t>(2); };
      //! Return the IPv4 Identification field of the packet
      uint16_t identification() const { return get_be<uint16_t>(4); };
      //! Return the Flags and Fragment Offset field of the packet
      uint16_t flags_frag_off() const { return get_be<uint16_t>(6); };
      //! Return the Time to Live field of the packet
      uint8_t ttl() const { return get<uint8_t>(8); };
      //! Return the Protocol field of the packet
      uint8_t protocol() const { return get<uint8_t>(9); };
      //! Return the Header Checksum field of the packet
      uint16_t checksum() const { return get<uint16_t>(10); };
      //! Return the IPv4 source address field of the packet
      ip_addr_v4 source_address() const { return get<ip_addr_v4>(12); };
      //! Return the IPv4 destination address field of the packet
      ip_addr_v4 destination_address() const { return get<ip_addr_v4>(16); };

      //! Return true if the packet is fragmented
      bool is_fragment() const;
      //! Return the length of the header in bytes
      size_t header_length() const;
      //! Return the version of the packet
      int version() const;
      //! Return a UDP packet payload interface to the IPv4 frame payload
      UDPPacket payload_udp() const;
  };

  /**
   * @brief Ethernet Frame Interface to raw data stored in memory
   *
   */
  class EthernetFrame : public PacketBuffer
  {
    public:
      //! Minimum size of a frame
      static constexpr size_t min_size = 14;

      /**
       * @brief Construct a new Ethernet Frame object which is uninitialised
       *
       */
      EthernetFrame() = default;

      /**
       * @brief Construct a new Ethernet Frame object
       *
       * @param ptr memory address of the frame
       * @param size length of the frame
       */
      EthernetFrame(char * ptr, size_t size);

      //! Return the destination MAC address of the frame
      mac_address destination_mac() const { return get<mac_address>(0); };

      //! Return the source MAC address of the frame
      mac_address source_mac() const { return get<mac_address>(6); };

      //! Return the EtherType of the frame.
      uint16_t ethertype() const { return get_be<uint16_t>(12); };

      //! Return an IPV4 interface to the payload of the Ethernet frame
      IPv4Packet payload_ipv4() const;
  };

  /**
   * @brief Return PacketBuffer corresponding to the UDP payload of the provided pointer.
   * Constructs an EthernetFrame from ptr and if the contents contain a valid UDP packet,
   * returns a PacketBuffer representation of that UDP payload.
   *
   * @param ptr pointer to a raw ethernet frame
   * @param size size of the pointer
   * @return PacketBuffer UDP payload of the EthernetFrame
   */
  PacketBuffer udp_from_ethernet(char *ptr, size_t size);

  /**
   * @brief Return the MAC address of the network device using the specified IPv4 address.
   *
   * @param ip_address IPv4 interface address of a local network interface/device
   * @return mac_address MAC address of the device
   */
  mac_address interface_mac(const std::string &ip_address);

  /**
   * @brief Convert the MAC address into a string representation.
   *
   * Alphabetic characters will in lower case.
   *
   * @param mac integer representation of MAC address
   * @return std::string string representation of MAC address
   */
  std::string mac_to_string(const mac_address mac);

} // namespace ska::pst::common

#endif // SKA_PST_COMMON_UTILS_PacketUtils_h
