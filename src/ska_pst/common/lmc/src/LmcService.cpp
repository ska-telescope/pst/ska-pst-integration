/*
 * Copyright 2022 Square Kilometre Array Observatory
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 * contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#include "ska_pst/common/lmc/LmcLogging.h"
#include "ska_pst/common/lmc/LmcService.h"
#include "ska_pst/common/lmc/LmcServiceHandler.h"
#include "ska_pst/common/statemodel/StateModel.h"
#include "ska_pst/common/utils/Logging.h"

#include <string>
#include <sstream>
#include <thread>
#include <chrono>
#include <uuid/uuid.h>

ska::pst::common::LmcService::LmcService(
  std::string service_name,
  std::shared_ptr<ska::pst::common::LmcServiceHandler> handler,
  int port
) : _service_name(std::move(service_name)), handler(std::move(handler)), _port(port)
{

  PSTLOG_DEBUG(this, "service_name={}", _service_name);
  log_context["entity"] = "LmcService[" + _service_name + "]";

  // generate a UUID for service
  uuid_t uuid;
  uuid_generate_random(uuid);

  // convert 128bit value in a string.
  // uuid_unparse_lower only uses c-strings of 36 chars + 1 for \0
  char uuid_cstr[UUID_STR_LEN];  // NOLINT
  uuid_unparse_lower(uuid, uuid_cstr);
  _uuid = uuid_cstr;

  PSTLOG_DEBUG(this, "uuid={}", _uuid);
  PSTLOG_DEBUG(this, "finished", _service_name);
}

void ska::pst::common::LmcService::start() {
  PSTLOG_INFO(this, "Starting gRPC LMC server '{}'", _service_name);
  grpc::internal::MutexLock lock(&_mu);
  if (_started) {
    return;
  }
  _started = true;

  _background_thread = std::make_unique<std::thread>([this](){
    serve();
  });

  while (!_server_ready) {
    _cond.Wait(&_mu);
  }
  _server_ready = false;
  PSTLOG_INFO(this, "Started gRPC LMC server '{}' on port {}", _service_name, _port);
}

void ska::pst::common::LmcService::serve() {
  // this need to be on a background daemon thread.
  PSTLOG_TRACE(this, "starting");
  std::string server_address("0.0.0.0:");
  server_address.append(std::to_string(_port));
  PSTLOG_TRACE(this, "setting up listen on port {}", _port);

  try
  {
    grpc::ServerBuilder builder;
    PSTLOG_TRACE(this, "adding TCP listener {} to service", server_address);

    PSTLOG_TRACE(this, "creating listener credentials");
    auto credentials = grpc::InsecureServerCredentials();
    PSTLOG_TRACE(this, "created credentials");

    PSTLOG_TRACE(this, "creating listener");
    builder.AddListeningPort(server_address, credentials, &_port);
    PSTLOG_TRACE(this, "listener created");

    PSTLOG_TRACE(this, "registering service.");
    builder.RegisterService(this);

    PSTLOG_TRACE(this, "starting server");
    server = builder.BuildAndStart();
    PSTLOG_TRACE(this, "listening on port {}", _port);

    grpc::internal::MutexLock lock(&_mu);
    _server_ready = true;
    _cond.Signal();
  } catch (std::exception& exc) {
    PSTLOG_ERROR(this, "Error {} raised during starting up of gRPC service", exc.what());
    exit(1);
  }

}

void ska::pst::common::LmcService::stop() {
  PSTLOG_TRACE(this, "stopping");
  PSTLOG_INFO(this, "Stopping gRPC LMC server '{}'", _service_name);
  grpc::internal::MutexLock lock(&_mu);
  if (!_started) {
    return;
  }
  server->Shutdown();
  _background_thread->join();

  _started = false;
}

void ska::pst::common::LmcService::set_state(
  ska::pst::lmc::ObsState state
)
{
  // lock the mutex used by monitor condition and notify that monitoring should stop
  {
    std::lock_guard<std::mutex> lk(_monitor_mutex);
    _state = state;
  }
  _monitor_condition.notify_all();
}

auto ska::pst::common::LmcService::connect(
  grpc::ServerContext* /*context*/,
  const ska::pst::lmc::ConnectionRequest* request,
  ska::pst::lmc::ConnectionResponse* response
) -> grpc::Status
{
  PSTLOG_INFO(this, "gRPC LMC connection received from {} for {}/{}", request->client_id(), service_name(), _uuid);

  response->set_service_name(service_name());
  response->set_uuid(_uuid);

  return grpc::Status::OK;
}

auto ska::pst::common::LmcService::configure_beam(
  grpc::ServerContext* /*context*/,
  const ska::pst::lmc::ConfigureBeamRequest* request,
  ska::pst::lmc::ConfigureBeamResponse* /*response*/
) -> grpc::Status
{
  PSTLOG_TRACE(this, "configuring beam");

  if (!request->dry_run())
  {
    // check if handler has already have had beam configured
    if (handler->is_beam_configured()) {
      PSTLOG_WARN(this, "Received configure beam request but beam configured already.");
      ska::pst::lmc::Status status;
      status.set_code(ska::pst::lmc::ErrorCode::CONFIGURED_FOR_BEAM_ALREADY);
      status.set_message(_service_name + " beam configured already. Beam configuration needs to be deconfigured before reconfiguring.");
      return { grpc::StatusCode::FAILED_PRECONDITION, status.message(), status.SerializeAsString() };
    }

    if (_state != ska::pst::lmc::ObsState::EMPTY) {
      auto curr_state_name = ska::pst::lmc::ObsState_Name(_state);
      PSTLOG_WARN(this, "Received configure beam request but not in EMPTY state. Currently in {} state.", curr_state_name);

      ska::pst::lmc::Status status;
      status.set_code(ska::pst::lmc::ErrorCode::INVALID_REQUEST);

      std::ostringstream ss;
      ss << _service_name << " is not in EMPTY state. Currently in " << curr_state_name << " state.";

      status.set_message(ss.str());
      return { grpc::StatusCode::FAILED_PRECONDITION, status.message(), status.SerializeAsString() };
    }
  }

  try {
    if (request->dry_run()) {
      handler->validate_beam_configuration(request->beam_configuration());
    } else {
      set_state(ska::pst::lmc::ObsState::RESOURCING);
      rethrow_application_manager_runtime_error("RuntimeError before configuring beam");
      handler->configure_beam(request->beam_configuration());
      set_state(ska::pst::lmc::ObsState::IDLE);
    }
    return grpc::Status::OK;
  } catch (ska::pst::common::pst_validation_error& exc) {
    std::ostringstream ss;
    ss << _service_name << " - Error in validating beam configuration: " << exc.what();
    std::string error_message = ss.str();

    PSTLOG_WARN(this, error_message);
    set_state(ska::pst::lmc::ObsState::EMPTY);
    ska::pst::lmc::Status status;
    status.set_code(ska::pst::lmc::ErrorCode::INVALID_REQUEST);
    status.set_message(error_message);
    return { grpc::StatusCode::FAILED_PRECONDITION, status.message(), status.SerializeAsString() };
  } catch (std::exception& exc) {
    // handle exception
    std::ostringstream ss;
    ss << _service_name << " - Error in configuring beam: " << exc.what();
    std::string error_message = ss.str();

    PSTLOG_WARN(this, error_message);
    ska::pst::lmc::Status status;
    status.set_code(ska::pst::lmc::ErrorCode::INTERNAL_ERROR);
    status.set_message(error_message);
    set_state(ska::pst::lmc::ObsState::FAULT);
    return { grpc::StatusCode::INTERNAL, status.message(), status.SerializeAsString() };
  }
}

auto ska::pst::common::LmcService::deconfigure_beam(
  grpc::ServerContext* /*context*/,
  const ska::pst::lmc::DeconfigureBeamRequest* /*request*/,
  ska::pst::lmc::DeconfigureBeamResponse* /*response*/
) -> grpc::Status
{
  PSTLOG_TRACE(this, "ska::pst::common::LmcService::deconfigure_beam()");

  // check if handler has already have had beam configured
  if (!handler->is_beam_configured()) {
    PSTLOG_WARN(this, "Received request to deconfigure beam when no beam configured.");

    ska::pst::lmc::Status status;
    status.set_code(ska::pst::lmc::ErrorCode::NOT_CONFIGURED_FOR_BEAM);
    status.set_message("No " + _service_name + " beam configured.");
    return { grpc::StatusCode::FAILED_PRECONDITION, status.message(), status.SerializeAsString() };
  }


  base_error_message = "Error in deconfiguring beam";
  try {
    rethrow_application_manager_runtime_error("RuntimeError before deconfiguring beam");
    handler->deconfigure_beam();
    set_state(ska::pst::lmc::ObsState::EMPTY);
    return grpc::Status::OK;
  } catch (std::exception& exc) {
    // handle exception
    std::ostringstream ss;
    ss << _service_name << " - " << base_error_message << ": " << exc.what();
    std::string error_message = ss.str();

    PSTLOG_WARN(this, error_message);
    ska::pst::lmc::Status status;
    status.set_code(ska::pst::lmc::ErrorCode::INTERNAL_ERROR);
    status.set_message(error_message);
    set_state(ska::pst::lmc::ObsState::FAULT);
    return { grpc::StatusCode::INTERNAL, status.message(), status.SerializeAsString() };
  }
}

auto ska::pst::common::LmcService::get_beam_configuration(
  grpc::ServerContext* /*context*/,
  const ska::pst::lmc::GetBeamConfigurationRequest* /*request*/,
  ska::pst::lmc::GetBeamConfigurationResponse* response
) -> grpc::Status
{
  PSTLOG_TRACE(this, "ska::pst::common::LmcService::get_beam_configuration()");
  if (!handler->is_beam_configured())
  {
    PSTLOG_WARN(this, "Received request to get beam configuration when no beam configured.");
    ska::pst::lmc::Status status;
    status.set_code(ska::pst::lmc::ErrorCode::NOT_CONFIGURED_FOR_BEAM);
    status.set_message("No " + _service_name + " beam configured.");
    return { grpc::StatusCode::FAILED_PRECONDITION, status.message(), status.SerializeAsString() };
  }

  try {
    auto *beam_configuration = response->mutable_beam_configuration();
    handler->get_beam_configuration(beam_configuration);

    return grpc::Status::OK;
  } catch (std::exception& exc) {
    // handle exception
    std::ostringstream ss;
    ss << _service_name << " - Error in getting beam configuration: " << exc.what();
    std::string error_message = ss.str();

    PSTLOG_WARN(this, error_message);
    ska::pst::lmc::Status status;
    status.set_code(ska::pst::lmc::ErrorCode::INTERNAL_ERROR);
    status.set_message(error_message);
    return { grpc::StatusCode::INTERNAL, status.message(), status.SerializeAsString() };
  }
}

auto ska::pst::common::LmcService::configure_scan(
  grpc::ServerContext* /*context*/,
  const ska::pst::lmc::ConfigureScanRequest* request,
  ska::pst::lmc::ConfigureScanResponse* /*response*/
) -> grpc::Status
{
  if (!request->dry_run()) {
    // check if handler has already been configured
    if (handler->is_scan_configured()) {
      PSTLOG_WARN(this, "Received configure scan request but handler already has scan configured.");
      ska::pst::lmc::Status status;
      status.set_code(ska::pst::lmc::ErrorCode::CONFIGURED_FOR_SCAN_ALREADY);
      status.set_message(_service_name + " already configured for scan. Scan needs to be deconfigured before reconfiguring.");
      return { grpc::StatusCode::FAILED_PRECONDITION, status.message(), status.SerializeAsString() };
    }

    // ensure in IDLE state
    if (_state != ska::pst::lmc::ObsState::IDLE) {
      auto curr_state_name = ska::pst::lmc::ObsState_Name(_state);
      PSTLOG_WARN(this, "Received configure request but not in IDLE state. Currently in {} state.", curr_state_name);
      ska::pst::lmc::Status status;
      status.set_code(ska::pst::lmc::ErrorCode::INVALID_REQUEST);

      std::ostringstream ss;
      ss << _service_name << " is not in IDLE state. Currently in " << curr_state_name << " state.";

      status.set_message(ss.str());
      return { grpc::StatusCode::FAILED_PRECONDITION, status.message(), status.SerializeAsString() };
    }
  }

  try {
    if (request->dry_run()) {
      handler->validate_scan_configuration(request->scan_configuration());
    } else {
      rethrow_application_manager_runtime_error("RuntimeError before configuring scan");
      handler->configure_scan(request->scan_configuration());
      set_state(ska::pst::lmc::ObsState::READY);
    }
  } catch (ska::pst::common::pst_validation_error& exc) {
    std::ostringstream ss;
    ss << _service_name << " - Error in validating scan configuration: " << exc.what();
    std::string error_message = ss.str();

    PSTLOG_WARN(this, error_message);
    set_state(ska::pst::lmc::ObsState::IDLE);
    ska::pst::lmc::Status status;
    status.set_code(ska::pst::lmc::ErrorCode::INVALID_REQUEST);
    status.set_message(error_message);
    return { grpc::StatusCode::FAILED_PRECONDITION, status.message(), status.SerializeAsString() };
  } catch (std::exception& exc) {
    // handle exception
    std::ostringstream ss;
    ss << _service_name << " - Error in configuring scan: " << exc.what();
    std::string error_message = ss.str();

    PSTLOG_WARN(this, error_message);
    ska::pst::lmc::Status status;
    status.set_code(ska::pst::lmc::ErrorCode::INTERNAL_ERROR);
    status.set_message(error_message);
    set_state(ska::pst::lmc::ObsState::FAULT);
    return { grpc::StatusCode::INTERNAL, status.message(), status.SerializeAsString() };
  }
  return grpc::Status::OK;
}

auto ska::pst::common::LmcService::deconfigure_scan(
  grpc::ServerContext* /*context*/,
  const ska::pst::lmc::DeconfigureScanRequest* /*request*/,
  ska::pst::lmc::DeconfigureScanResponse* /*response*/
) -> grpc::Status
{
  // ensure in READY state
  if (_state != ska::pst::lmc::ObsState::READY) {
    auto curr_state_name = ska::pst::lmc::ObsState_Name(_state);
    PSTLOG_WARN(this, "Received deconfigure request but not in READY state. Currently in {} state.", curr_state_name);
    ska::pst::lmc::Status status;
    status.set_code(ska::pst::lmc::ErrorCode::INVALID_REQUEST);

    std::ostringstream ss;
    ss << _service_name << " is not in READY state. Currently in " << curr_state_name << " state.";

    status.set_message(ss.str());
    return { grpc::StatusCode::FAILED_PRECONDITION, status.message(), status.SerializeAsString() };
  }

  base_error_message = "Error in deconfiguring scan";
  try {
    rethrow_application_manager_runtime_error("RuntimeError before deconfiguring scan");
    handler->deconfigure_scan();
    set_state(ska::pst::lmc::ObsState::IDLE);
    return grpc::Status::OK;
  } catch (std::exception& exc) {
    // handle exception
    std::ostringstream ss;
    ss << _service_name << " - " << base_error_message << ": " << exc.what();
    std::string error_message = ss.str();

    PSTLOG_WARN(this, error_message);
    ska::pst::lmc::Status status;
    status.set_code(ska::pst::lmc::ErrorCode::INTERNAL_ERROR);
    status.set_message(error_message);
    set_state(ska::pst::lmc::ObsState::FAULT);
    return { grpc::StatusCode::INTERNAL, status.message(), status.SerializeAsString() };
  }
}

auto ska::pst::common::LmcService::get_scan_configuration(
  grpc::ServerContext* /*context*/,
  const ska::pst::lmc::GetScanConfigurationRequest* /*request*/,
  ska::pst::lmc::GetScanConfigurationResponse* response
) -> grpc::Status
{
  // ensure in READY state
  if (_state != ska::pst::lmc::ObsState::READY &&
    _state != ska::pst::lmc::ObsState::SCANNING
  ) {
    auto curr_state_name = ska::pst::lmc::ObsState_Name(_state);
    PSTLOG_WARN(this, "Get scan configuration request but not in configured state. Currently in {} state.", curr_state_name);
    ska::pst::lmc::Status status;
    status.set_code(ska::pst::lmc::ErrorCode::INVALID_REQUEST);

    std::ostringstream ss;
    ss << _service_name << " is not in a configured state. Currently in " << curr_state_name << " state.";

    status.set_message(ss.str());
    return { grpc::StatusCode::FAILED_PRECONDITION, status.message(), status.SerializeAsString() };
  }

  try {
    handler->get_scan_configuration(response->mutable_scan_configuration());
  } catch (std::exception& exc) {
    // handle exception
    std::ostringstream ss;
    ss << _service_name << " - Error in configuring beam: " << exc.what();
    std::string error_message = ss.str();

    PSTLOG_WARN(this, error_message);
    ska::pst::lmc::Status status;
    status.set_code(ska::pst::lmc::ErrorCode::INTERNAL_ERROR);
    status.set_message(error_message);
    return { grpc::StatusCode::INTERNAL, status.message(), status.SerializeAsString() };
  }

  return grpc::Status::OK;
}

auto ska::pst::common::LmcService::start_scan(
  grpc::ServerContext* /*context*/,
  const ska::pst::lmc::StartScanRequest* request,
  ska::pst::lmc::StartScanResponse* /*response*/
) -> grpc::Status
{
  PSTLOG_TRACE(this, "ska::pst::common::LmcService::scan()");
  if (_state == ska::pst::lmc::ObsState::SCANNING) {
    PSTLOG_WARN(this, "Received scan request but already in SCANNING state.");
    ska::pst::lmc::Status status;
    status.set_code(ska::pst::lmc::ErrorCode::ALREADY_SCANNING);
    status.set_message(_service_name + " is already scanning.");
    return { grpc::StatusCode::FAILED_PRECONDITION, status.message(), status.SerializeAsString() };
  }
  if (_state != ska::pst::lmc::ObsState::READY)
  {
    auto curr_state_name = ska::pst::lmc::ObsState_Name(_state);
    PSTLOG_WARN(this, "Received scan request but not in READY state. Currently in {} state.", curr_state_name);
    ska::pst::lmc::Status status;
    status.set_code(ska::pst::lmc::ErrorCode::INVALID_REQUEST);

    std::ostringstream ss;
    ss << _service_name << " is not in READY state. Currently in " << curr_state_name << " state.";

    status.set_message(ss.str());
    return { grpc::StatusCode::FAILED_PRECONDITION, status.message(), status.SerializeAsString() };
  }

  base_error_message = "Error in starting scan";
  try {
    rethrow_application_manager_runtime_error("RuntimeError before starting scan");
    handler->start_scan(*request);
    set_state(ska::pst::lmc::ObsState::SCANNING);
    return grpc::Status::OK;
  } catch (ska::pst::common::pst_validation_error& exc) {
    std::ostringstream ss;
    ss << _service_name << " - " << base_error_message << ": " << exc.what();
    std::string error_message = ss.str();

    set_state(ska::pst::lmc::ObsState::READY);
    PSTLOG_WARN(this, error_message);
    ska::pst::lmc::Status status;
    status.set_code(ska::pst::lmc::ErrorCode::INVALID_REQUEST);
    status.set_message(error_message);
    return { grpc::StatusCode::FAILED_PRECONDITION, status.message(), status.SerializeAsString() };
  } catch (std::exception& exc) {
    // handle exception
    std::ostringstream ss;
    ss << _service_name << " - " << base_error_message << ": " << exc.what();
    std::string error_message = ss.str();

    PSTLOG_WARN(this, error_message);
    ska::pst::lmc::Status status;
    status.set_code(ska::pst::lmc::ErrorCode::INTERNAL_ERROR);
    status.set_message(error_message);
    set_state(ska::pst::lmc::ObsState::FAULT);
    return { grpc::StatusCode::INTERNAL, status.message(), status.SerializeAsString() };
  }
}

auto ska::pst::common::LmcService::stop_scan(
  grpc::ServerContext* /*context*/,
  const ska::pst::lmc::StopScanRequest* /*request*/,
  ska::pst::lmc::StopScanResponse* /*response*/
) -> grpc::Status
{
  PSTLOG_TRACE(this, "ska::pst::common::LmcService::stop_scan()");
  if (_state != ska::pst::lmc::ObsState::SCANNING) {
    auto curr_state_name = ska::pst::lmc::ObsState_Name(_state);
    PSTLOG_WARN(this, "Received stop scan request but not in SCANNING state. Currently in {} state.", curr_state_name);
    ska::pst::lmc::Status status;
    status.set_code(ska::pst::lmc::ErrorCode::NOT_SCANNING);

    std::ostringstream ss;
    ss << _service_name << " is not in SCANNING state. Currently in " << curr_state_name << " state.";
    status.set_message(ss.str());

    return { grpc::StatusCode::FAILED_PRECONDITION, status.message(), status.SerializeAsString() };
  }

  base_error_message = "Error in stopping scan";
  try {
    rethrow_application_manager_runtime_error("RuntimeError before stopping scan");
    handler->stop_scan();
    set_state(ska::pst::lmc::ObsState::READY);
    return grpc::Status::OK;
  } catch (std::exception& exc) {
    // handle exception
    std::ostringstream ss;
    ss << _service_name << " - " << base_error_message << ": " << exc.what();
    std::string error_message = ss.str();

    PSTLOG_WARN(this, error_message);
    ska::pst::lmc::Status status;
    status.set_code(ska::pst::lmc::ErrorCode::INTERNAL_ERROR);
    status.set_message(error_message);
    set_state(ska::pst::lmc::ObsState::FAULT);
    return { grpc::StatusCode::INTERNAL, status.message(), status.SerializeAsString() };
  }
}

auto ska::pst::common::LmcService::get_state(
  grpc::ServerContext* /*context*/,
  const ska::pst::lmc::GetStateRequest* /*request*/,
  ska::pst::lmc::GetStateResponse* response
) -> grpc::Status
{
  PSTLOG_TRACE(this, "ska::pst::common::LmcService::get_state()");
  response->set_state(_state);
  return grpc::Status::OK;
}

auto ska::pst::common::LmcService::monitor(
  grpc::ServerContext* context,
  const ska::pst::lmc::MonitorRequest* request,
  grpc::ServerWriter<ska::pst::lmc::MonitorResponse>* writer
) -> grpc::Status
{
  PSTLOG_INFO(this, "{} starting monitoring", _service_name);
  if (_state != ska::pst::lmc::ObsState::SCANNING) {
    auto curr_state_name = ska::pst::lmc::ObsState_Name(_state);
    PSTLOG_WARN(this, "Received monitor but not in SCANNING state. Currently in {} state.", curr_state_name);
    ska::pst::lmc::Status status;
    status.set_code(ska::pst::lmc::ErrorCode::NOT_SCANNING);

    std::ostringstream ss;
    ss << _service_name << " is not in SCANNING state. Currently in " << curr_state_name << " state.";

    status.set_message(ss.str());
    return { grpc::StatusCode::FAILED_PRECONDITION, status.message(), status.SerializeAsString() };
  }

  const auto &delay = std::chrono::milliseconds(request->polling_rate());

  while (true) {
    // use a condition variable and wait_for to allow interrupting the sleep, rather
    // than sleeping the thread and only checking condition after the sleep. Using
    // the predicate helps avoid spurious wakeups of blocked thread.
    //
    // See https://en.cppreference.com/w/cpp/thread/condition_variable/wait_for for
    // more details.
    std::unique_lock<std::mutex> lk(_monitor_mutex);
    if (_monitor_condition.wait_for(lk, delay, [this]{
      return _state != ska::pst::lmc::ObsState::SCANNING;
    }))
    {
      PSTLOG_INFO(this, "No longer in SCANNING state. Exiting monitor");
      break;
    }
    PSTLOG_TRACE(this, "Getting latest monitor data");

    ska::pst::lmc::MonitorResponse response;
    auto *monitor_data = response.mutable_monitor_data();
    handler->get_monitor_data(monitor_data);

    if (context->IsCancelled()) {
      PSTLOG_INFO(this, "Monitoring context cancelled. Exiting monitor.");
      break;
    }
    if (!writer->Write(response)) {
      PSTLOG_WARN(this, "Writing monitor response return false. Exiting monitor.");
      break;
    }
  }

  return grpc::Status::OK;
}

auto ska::pst::common::LmcService::abort(
  grpc::ServerContext* /*context*/,
  const ska::pst::lmc::AbortRequest* /*request*/,
  ska::pst::lmc::AbortResponse* /*response*/
) -> grpc::Status
{
  PSTLOG_INFO(this, "{} LMC abort requested", _service_name);
  if (_state == ska::pst::lmc::ObsState::ABORTED)
  {
    PSTLOG_WARN(this, "Received abort request but already in ABORTED state.");
    return grpc::Status::OK;
  }

  if (!(
    _state == ska::pst::lmc::ObsState::IDLE ||
    _state == ska::pst::lmc::ObsState::READY ||
    _state == ska::pst::lmc::ObsState::SCANNING
  )) {
    auto curr_state_name = ska::pst::lmc::ObsState_Name(_state);
    PSTLOG_WARN(this, "Received abort request but not in an abortable state. Currently in {} state.",
      curr_state_name);
    ska::pst::lmc::Status status;
    status.set_code(ska::pst::lmc::ErrorCode::INVALID_REQUEST);

    std::ostringstream ss;
    ss << _service_name << " is not in an abortable state. Currently in " << curr_state_name << " state.";

    status.set_message(ss.str());
    return { grpc::StatusCode::FAILED_PRECONDITION, status.message(), status.SerializeAsString() };
  }

  try {
    if (_state == ska::pst::lmc::ObsState::SCANNING)
    {
      PSTLOG_INFO(this, "Currently in a scanning state, stopping scan");
      handler->stop_scan();
    }
    set_state(ska::pst::lmc::ObsState::ABORTED);
    PSTLOG_DEBUG(this, "Abort completed");
    return grpc::Status::OK;
  } catch (std::exception& exc) {
    // handle exception
    std::ostringstream ss;
    ss << _service_name << " - Error in aborting: " << exc.what();
    std::string error_message = ss.str();

    PSTLOG_WARN(this, error_message);
    ska::pst::lmc::Status status;
    status.set_code(ska::pst::lmc::ErrorCode::INTERNAL_ERROR);
    status.set_message(error_message);
    return { grpc::StatusCode::INTERNAL, status.message(), status.SerializeAsString() };
  }
}

auto ska::pst::common::LmcService::reset(
  grpc::ServerContext* /*context*/,
  const ska::pst::lmc::ResetRequest* /*request*/,
  ska::pst::lmc::ResetResponse* /*response*/
) -> grpc::Status
{
  PSTLOG_INFO(this, "Reset requested");
  try {
    handler->reset();
    if (handler->is_scan_configured())
    {
      PSTLOG_INFO(this, "Handler in scan configured state, deconfiguring scan");
      handler->deconfigure_scan();
    }
    if (handler->is_beam_configured())
    {
      PSTLOG_INFO(this, "Handler in beam configured state, deconfiguring beam");
      handler->deconfigure_beam();
    }
    set_state(ska::pst::lmc::ObsState::EMPTY);
    PSTLOG_INFO(this, "Reset completed");
    return grpc::Status::OK;
  } catch (std::exception& exc) {
    // handle exception
    std::ostringstream ss;
    ss << _service_name << " - Error occurred during reset: " << exc.what();
    std::string error_message = ss.str();

    PSTLOG_WARN(this, error_message);
    ska::pst::lmc::Status status;
    status.set_code(ska::pst::lmc::ErrorCode::INTERNAL_ERROR);
    status.set_message(error_message);
    return { grpc::StatusCode::INTERNAL, status.message(), status.SerializeAsString() };
  }
}

auto ska::pst::common::LmcService::restart(
  grpc::ServerContext* /*context*/,
  const ska::pst::lmc::RestartRequest* /*request*/,
  ska::pst::lmc::RestartResponse* /*response*/
) -> grpc::Status
{
  PSTLOG_INFO(this, "Restart requested");
  handler->restart();
  set_state(ska::pst::lmc::ObsState::EMPTY);
  PSTLOG_DEBUG(this, "Restart completed");
  return grpc::Status::OK;
}

auto ska::pst::common::LmcService::go_to_fault(
  grpc::ServerContext* /*context*/,
  const ska::pst::lmc::GoToFaultRequest* request,
  ska::pst::lmc::GoToFaultResponse* /*response*/
) -> grpc::Status
{
  PSTLOG_INFO(this, "Go To Fault requested");
  try {
    auto message = request->error_message();
    if (message.empty()) {
      message = "gRPC forced update to runtime";
    }
    throw std::runtime_error(message);
  } catch (std::exception& exc) {
    PSTLOG_WARN(this, "{} gRPC service tried to stop scanning but exception {} occurred.", _service_name, exc.what());
    handler->go_to_runtime_error(std::current_exception());
  }
  set_state(ska::pst::lmc::ObsState::FAULT);
  PSTLOG_DEBUG(this, "Go To Fault completed");
  return grpc::Status::OK;
}

auto ska::pst::common::LmcService::get_env(
  grpc::ServerContext* /*context*/,
  const ska::pst::lmc::GetEnvironmentRequest* /*request*/,
  ska::pst::lmc::GetEnvironmentResponse* response
) -> grpc::Status
{
  PSTLOG_DEBUG(this, "Get env requested");
  handler->get_env(response);
  PSTLOG_DEBUG(this, "Get env completed");
  return grpc::Status::OK;
}

auto ska::pst::common::LmcService::rethrow_application_manager_runtime_error(
  const std::string& _base_error_message
) -> void
{
  if (handler->get_application_manager_state() == ska::pst::common::RuntimeError)
  {
    if (handler->get_application_manager_exception())
    {
      base_error_message = _base_error_message;
      std::rethrow_exception(handler->get_application_manager_exception());
    }
  }
}


auto ska::pst::common::LmcService::get_log_level(
  grpc::ServerContext* /*context*/,
  const ska::pst::lmc::GetLogLevelRequest* /*request*/,
  ska::pst::lmc::GetLogLevelResponse* response
) -> grpc::Status
{
  try {
    response->set_log_level(ska::pst::common::get_lmclog_level(spdlog::get_level()));
  } catch (std::exception& exc) {
    std::ostringstream ss;
    ss << _service_name << " - Error getting the log level: " << exc.what();
    std::string error_message = ss.str();

    PSTLOG_WARN(this, error_message);
    ska::pst::lmc::Status status;
    status.set_code(ska::pst::lmc::ErrorCode::INTERNAL_ERROR);
    status.set_message(error_message);
    return { grpc::StatusCode::INTERNAL, status.message(), status.SerializeAsString() };
  }

  return grpc::Status::OK;
}

auto ska::pst::common::LmcService::set_log_level(
  grpc::ServerContext* /*context*/,
  const ska::pst::lmc::SetLogLevelRequest* request,
  ska::pst::lmc::SetLogLevelResponse* /*response*/
) -> grpc::Status
{
  spdlog::level::level_enum old_level = spdlog::get_level();
  try {
    spdlog::level::level_enum new_level = ska::pst::common::get_spdlog_level(request->log_level());

    // set level to info to ensure the change of log level is always logged
    spdlog::set_level(spdlog::level::info);
    PSTLOG_INFO(this, "Changing log level from {} to {}",
      spdlog::level::to_string_view(old_level),
      spdlog::level::to_string_view(new_level));
    // set to the requested log level
    spdlog::set_level(new_level);
  } catch (std::exception& exc) {
    std::ostringstream ss;
    ss << _service_name << " - Error getting the log level: " << exc.what();
    std::string error_message = ss.str();

    PSTLOG_WARN(this, error_message);
    ska::pst::lmc::Status status;
    status.set_code(ska::pst::lmc::ErrorCode::INTERNAL_ERROR);
    status.set_message(error_message);

    return { grpc::StatusCode::INTERNAL, status.message(), status.SerializeAsString() };
  }

  return grpc::Status::OK;
}

auto ska::pst::common::LmcService::health_check(
  grpc::ServerContext* context,
  const ska::pst::lmc::HealthCheckRequest* request,
  grpc::ServerWriter<ska::pst::lmc::HealthCheckResponse>* writer
) -> grpc::Status
{
  PSTLOG_INFO(this, "Health check request initiated. Health check interval = {}ms", request->health_check_interval());
  const auto &health_check_interval = std::chrono::milliseconds(request->health_check_interval());
  auto next_health_check = std::chrono::system_clock::now();

  while (true) {
    PSTLOG_DEBUG(this, "Perform latest health check / heartbeat");
    if (context->IsCancelled()) {
      PSTLOG_INFO(this, "Health check context cancelled. Exiting health check.");
      break;
    }

    ska::pst::lmc::HealthCheckResponse response;
    response.set_service_name(_service_name);
    response.set_uuid(_uuid);
    response.set_obs_state(_state);
    if (_state == ska::pst::lmc::ObsState::FAULT)
    {
      try {
        auto exc_ptr = handler->get_application_manager_exception();
        // need to rethrow exception to be able to get access to it.
        if (exc_ptr)
        {
          std::rethrow_exception(exc_ptr);
        }
      }
      catch (const std::exception& exc)
      {
        response.set_fault_message(exc.what());
      }
    }

    if (!writer->Write(response)) {
      PSTLOG_WARN(this, "Writing health check response return false. Exiting health check.");
      break;
    }

    next_health_check += health_check_interval;

    // sleep
    while (!context->IsCancelled() && std::chrono::system_clock::now() < next_health_check)
    {
      std::this_thread::sleep_for(ska::pst::common::HEALTH_CHECK_SLEEP);
    }
  }

  return grpc::Status::OK;
}
