 /*
 * Copyright 2022 Square Kilometre Array Observatory
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 * contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */


#include <stdexcept>
#include <algorithm>
#include <chrono>
#include <sstream>
#include "ska_pst/common/utils/Logging.h"
#include "ska_pst/common/statemodel/StateModel.h"
#include "ska_pst/common/statemodel/ApplicationManager.h"

ska::pst::common::ApplicationManager::ApplicationManager(const std::string& _entity) : StateModel(_entity), previous_state(Unknown)
{
  PSTLOG_DEBUG(this, "ctor");
  main_thread = std::make_unique<std::thread>(std::thread(&ska::pst::common::ApplicationManager::main, this));
  PSTLOG_DEBUG(this, "ctor main_thread started");
}

ska::pst::common::ApplicationManager::~ApplicationManager()
{
  PSTLOG_DEBUG(this, "dtor main_thread->join()");
  main_thread->join();
  PSTLOG_DEBUG(this, "dtor main_thread joined");
}

void ska::pst::common::ApplicationManager::main()
{
  PSTLOG_DEBUG(this, "initialisation loop");
  previous_state = Unknown;
  while (state == Unknown)
  {
    PSTLOG_DEBUG(this, "state_model.wait_for_command");
    ska::pst::common::Command cmd = wait_for_command();
    PSTLOG_DEBUG(this, "state={} command={}", get_name(state), get_name(cmd));

    if (cmd == Initialise)
    {
      try
      {
        PSTLOG_DEBUG(this, "perform_initialise()");
        perform_initialise();
        PSTLOG_DEBUG(this, "perform_initialise() done state={}", state_names[get_state()]);
        set_state(Idle);
      }
      catch(const std::exception& exc)
      {
        PSTLOG_WARN(this, "exception during cmd={}: {}", get_name(cmd), exc.what());
        go_to_runtime_error(std::current_exception());
        PSTLOG_DEBUG(this, "cmd={} state={}", get_name(cmd), state_names[get_state()]);
        return;
      }

    }
    if(cmd == Terminate)
    {
      return;
    }
  }

  // thread to execute the perform_scan method asynchronously to the ApplicationManager
  std::unique_ptr<std::thread> scan_thread{nullptr};

  // loop through the statemodel
  while(state != Unknown)
  {
    PSTLOG_DEBUG(this, "wait_for_command()");
    ska::pst::common::Command cmd = wait_for_command();
    PSTLOG_DEBUG(this, "state={} cmd={}", get_name(state), get_name(cmd));

    try {
      switch (cmd)
      {
        case ConfigureBeam:
          PSTLOG_TRACE(this, "perform_configure_beam");
          enforce_state(Idle, "cannot perform configure beam");
          previous_state = Idle;
          perform_configure_beam();
          PSTLOG_TRACE(this, "perform_configure_beam done");
          PSTLOG_TRACE(this, "set_state(BeamConfigured)");
          set_state(BeamConfigured);
          PSTLOG_TRACE(this, "state={}", state_names[get_state()]);
          break;

        case ConfigureScan:
          PSTLOG_TRACE(this, "perform_configure_scan");
          enforce_state(BeamConfigured, "cannot perform configure scan");
          previous_state = BeamConfigured;
          perform_configure_scan();
          PSTLOG_TRACE(this, "perform_configure_scan done");
          PSTLOG_TRACE(this, "set_state(ScanConfigured)");
          set_state(ScanConfigured);
          PSTLOG_TRACE(this, "state={}", state_names[get_state()]);
          break;

        case StartScan:
          PSTLOG_TRACE(this, "perform_start_scan");
          enforce_state(ScanConfigured, "cannot perform start scan");
          previous_state = ScanConfigured;
          set_state(StartingScan);
          perform_start_scan();
          PSTLOG_TRACE(this, "perform_start_scan done");
          PSTLOG_TRACE(this, "set_state(Scanning)");
          scan_thread = std::make_unique<std::thread>(std::thread(&ska::pst::common::ApplicationManager::perform_scan_safely, this));
          PSTLOG_TRACE(this, "state={}", state_names[get_state()]);
          break;

        case StopScan:
          PSTLOG_TRACE(this, "perform_stop_scan");
          enforce_state(Scanning, "cannot perform stop scan");
          previous_state = Scanning;
          set_state(StoppingScan);
          perform_stop_scan();
          PSTLOG_TRACE(this, "perform_stop_scan done");
          scan_thread->join();
          scan_thread = nullptr;
          PSTLOG_TRACE(this, "scan_thread joined");
          PSTLOG_TRACE(this, "set_state(ScanConfigured)");
          set_state(ScanConfigured);
          PSTLOG_TRACE(this, "state={}", state_names[get_state()]);
          break;

        case DeconfigureScan:
          PSTLOG_TRACE(this, "perform_deconfigure_scan");
          enforce_state(ScanConfigured, "cannot perform deconfigure scan");
          previous_state = ScanConfigured;
          perform_deconfigure_scan();
          PSTLOG_TRACE(this, "perform_deconfigure_scan done");
          PSTLOG_TRACE(this, "set_state(BeamConfigured)");
          set_state(BeamConfigured);
          PSTLOG_TRACE(this, "state={}", state_names[get_state()]);
          break;

        case DeconfigureBeam:
          PSTLOG_TRACE(this, "perform_deconfigure_beam");
          enforce_state(BeamConfigured, "cannot perform deconfigure beam");
          previous_state = BeamConfigured;
          perform_deconfigure_beam();
          PSTLOG_TRACE(this, "perform_deconfigure_beam done");
          PSTLOG_TRACE(this, "set_state(Idle)");
          set_state(Idle);
          PSTLOG_TRACE(this, "state={}", state_names[get_state()]);
          break;

        case Reset:
          PSTLOG_TRACE(this, "perform_reset");
          perform_reset();
          PSTLOG_TRACE(this, "perform_reset done");
          if (scan_thread)
          {
            PSTLOG_TRACE(this, "joining scan_thread");
            scan_thread->join();
            scan_thread = nullptr;
          }
          PSTLOG_TRACE(this, "set_state(Idle)");
          set_state(Idle);
          PSTLOG_TRACE(this, "state={}", state_names[get_state()]);
          break;

        case Terminate:
          PSTLOG_TRACE(this, "perform_terminate");
          perform_terminate();
          PSTLOG_TRACE(this, "perform_terminate done");
          if (scan_thread)
          {
            PSTLOG_TRACE(this, "joining scan_thread");
            scan_thread->join();
            scan_thread = nullptr;
          }
          PSTLOG_TRACE(this, "set_state(Unknown)");
          set_state(Unknown);
          PSTLOG_TRACE(this, "state={}", state_names[get_state()]);
          break;

        case Initialise:
          PSTLOG_ERROR(this, "Unexpected Initialise command");
          throw std::runtime_error("Received Initialise command after initialisation completed");
          break;

        case None:
          PSTLOG_ERROR(this, "wait_for_command returned None command which was unexpected");
          throw std::runtime_error("Received None command from wait_for_command");
          break;

        default:
          PSTLOG_WARN(this, "Unexpected {} command", get_name(cmd));
          throw std::runtime_error("Received unexpected command");
          break;
      }
    }
    catch (const std::exception& exc)
    {
      PSTLOG_WARN(this, "exception during cmd={}: {}", get_name(cmd), exc.what());
      go_to_runtime_error(std::current_exception());
      PSTLOG_DEBUG(this, "state={}", state_names[get_state()]);
    }
  }

  // ensure the scan_thread is joined on main thread exit
  if (scan_thread)
  {
    PSTLOG_WARN(this, "unexpectedly joining scan_thread on main thread exit");
    scan_thread->join();
    scan_thread = nullptr;
  }
}

void ska::pst::common::ApplicationManager::quit()
{
  if (get_state() == Scanning)
  {
    PSTLOG_DEBUG(this, "set_command(StopScan)");
    set_command(StopScan);
    PSTLOG_DEBUG(this, "wait_for_state(ScanConfigured)");
    wait_for_state(ScanConfigured);
  }

  if (get_state() == ScanConfigured)
  {
    set_command(DeconfigureScan);
    wait_for_state(BeamConfigured);
  }

  if (get_state() == BeamConfigured)
  {
    set_command(DeconfigureBeam);
    wait_for_state(Idle);
  }

  if (get_state() == RuntimeError)
  {
    PSTLOG_DEBUG(this, "set_command(Reset)");
    set_command(Reset);
    PSTLOG_DEBUG(this, "wait_for_state_without_error(Idle)");
    wait_for_state_without_error(Idle);
  }

  if (get_state() == Idle)
  {
    PSTLOG_DEBUG(this, "set_command(Terminate)");
    set_command(Terminate);
    PSTLOG_DEBUG(this, "wait_for_state(Unknown)");
    wait_for_state(Unknown);
  }
  PSTLOG_TRACE(this, "done");
}

void ska::pst::common::ApplicationManager::force_exit(int _exit_code)
{
  PSTLOG_WARN(this, "code={}", _exit_code);
  exit(_exit_code);
}

void ska::pst::common::ApplicationManager::perform_reset()
{
  if (get_state() == ska::pst::common::State::RuntimeError)
  {
    if(get_previous_state() == ska::pst::common::State::Scanning)
    {
      perform_stop_scan();
      perform_deconfigure_scan();
      perform_deconfigure_beam();
    }
    if(get_previous_state() == ska::pst::common::State::ScanConfigured)
    {
      perform_deconfigure_scan();
      perform_deconfigure_beam();
    }
    if(get_previous_state() == ska::pst::common::State::BeamConfigured)
    {
      perform_deconfigure_beam();
    }
  }
}

auto ska::pst::common::ApplicationManager::wait_for_command() -> ska::pst::common::Command
{
  PSTLOG_TRACE(this, "waiting for new command");

  ska::pst::common::Command cmd{};
  {
    std::unique_lock<std::mutex> control_lock(command_mutex);
    command_cond.wait(control_lock, [&]{return (command != None);});
    cmd = command;
    command = None;
  }
  PSTLOG_TRACE(this, "command={}", get_name(cmd));
  return cmd;
}

void ska::pst::common::ApplicationManager::set_state(ska::pst::common::State new_state)
{
  PSTLOG_DEBUG(this, "new_state={}", get_name(new_state));
  ska::pst::common::State state_required = new_state;
  {
    std::unique_lock<std::mutex> control_lock(state_mutex);
    PSTLOG_DEBUG(this, "{} -> {}", get_name(state), get_name(state_required));
    PSTLOG_DEBUG(this, "Setting previous state from {} -> {}", get_name(previous_state), get_name(state));
    previous_state = state;
    state = new_state;
    state_cond.notify_all();
  }
  PSTLOG_DEBUG(this, "done state={}", get_name(get_state()));
}

void ska::pst::common::ApplicationManager::go_to_runtime_error(std::exception_ptr exc)
{
  PSTLOG_DEBUG(this, "setting exception")
  set_exception(std::move(exc));
  PSTLOG_DEBUG(this, "done");
  set_state(RuntimeError);
}

void ska::pst::common::ApplicationManager::set_exception(std::exception_ptr exception)
{
  PSTLOG_DEBUG(this, "set_exception");
  last_exception = std::move(exception);
}

auto ska::pst::common::ApplicationManager::get_previous_state() const -> ska::pst::common::State
{
  return previous_state;
}

void ska::pst::common::ApplicationManager::enforce(bool required, const std::string& contextual_message) const
{
  if (!required)
  {
    PSTLOG_ERROR(this, "enforce required state failure: {}", contextual_message);
    throw std::runtime_error(contextual_message);
  }
}

void ska::pst::common::ApplicationManager::enforce_state(ska::pst::common::State _state, const std::string& contextual_message) const
{
  if (state != _state)
  {
    PSTLOG_ERROR(this, "enforce state[{}] != required state[{}] failure: {}", get_name(state), get_name(_state), contextual_message);

    std::ostringstream ss;
    ss << entity << " " << contextual_message;
    throw std::runtime_error(ss.str());
  }
}

auto ska::pst::common::ApplicationManager::is_idle() const -> bool
{
  return get_state() == Idle;
}

auto ska::pst::common::ApplicationManager::is_beam_configured() const -> bool
{
  return get_state() == BeamConfigured || get_state() == ScanConfigured || get_state() == Scanning;
}

auto ska::pst::common::ApplicationManager::is_scan_configured() const -> bool
{
  return get_state() == ScanConfigured || get_state() == Scanning;
}

auto ska::pst::common::ApplicationManager::is_scanning() const -> bool
{
  return get_state() == Scanning;
}

void ska::pst::common::ApplicationManager::perform_scan_safely()
{
  PSTLOG_DEBUG(this, "executing perform_scan with try/catch protection");
  try
  {
    set_state(Scanning);
    perform_scan();
    PSTLOG_DEBUG(this, "perform_scan returned naturally");
  }
  catch (std::exception& exc)
  {
    PSTLOG_WARN(this, "exception during perform_scan: {}", exc.what());
    // if an error occurs after we have starting stopping scan then don't go into a runtime error
    if (is_scanning()) {
      go_to_runtime_error(std::current_exception());
      PSTLOG_DEBUG(this, "transitioned to runtime_error");
    }
  }
}
