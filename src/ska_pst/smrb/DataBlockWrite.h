/*
 * Copyright 2022 Square Kilometre Array Observatory
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 * contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#include <queue>

#include "ska_pst/smrb/DataBlockAccessor.h"

#ifndef __SKA_PST_SMRB_DataBlockWrite_h
#define __SKA_PST_SMRB_DataBlockWrite_h

namespace ska {
namespace pst {
namespace smrb {

  /**
   * @brief Object Oriented interface to write to a PSRDADA Shared Memory Ring Buffer (SMRB).
   *
   */
  class DataBlockWrite : public DataBlockAccessor
  {
    public:

      /**
       * @brief Construct a new Data Block Write object for the SMRB identified by the key.
       *
       * @param key 4 character hexidecimal key that identifies the SMRB in shared memory.
       */
      explicit DataBlockWrite(const std::string &key);

      /**
       * @brief Destroy the Data Block Write object.
       * Closes the access to the SMRB if required
       */
      ~DataBlockWrite();

      /**
       * @brief Open the SMRB for writing
       *
       */
      void open() override;

      /**
       * @brief Lock write access to the SMRB
       *
       */
      void lock() override;

      /**
       * @brief Close the connection to the SMRB
       *
       */
      void close() override;

      /**
       * @brief Unlock write access to the SMRB
       *
       */
      void unlock() override;

      /**
       * @brief Write the scan configuration string to the header_block.
       *
       * @param scan_config string representation of the scan configuration
       */
      void write_config(const std::string &scan_config);

      /**
       * @brief Write the complete header string to the header_block
       *
       * @param full_header string representation of the full header
       */
      void write_header(const std::string &full_header);

      /**
       * @brief Write the specified amount of data to the SMRB data_block
       *
       * @param ptr pointer to data to be written to the data_block
       * @param bytes number of bytes to write to the data_block
       * @return ssize_t number of bytes written to the data_block
       */
      ssize_t write_data(char * ptr, size_t bytes);

      /**
       * @brief Open a data_block buffer for DMA
       *
       * @return void* pointer to the opened data_block buffer
       */
      char * open_block() override;

      /**
       * @brief Close an opened data_block buffer specifying the number of bytes written
       * @param bytes number of bytes written to the opened data_block buffer
       * @return ssize_t number of bytes written
       */
      ssize_t close_block(uint64_t bytes) override;

      /**
       * @brief Update the number of bytes written to the opened data_block buffer
       *
       * @param bytes number of bytes written
       * @return ssize_t number of bytes updated
       */
      ssize_t update_block(uint64_t bytes);

      /**
       * @brief zero the buffer in the data_block after the currently opened buffer
       *
       */
      void zero_next_block();

      /**
       * @brief Return the number of open blocks
       *
       * @return size_t number of opened blocks
       */
      size_t get_opened_blocks_count() { return opened_blocks.size(); };

    protected:

    private:

      /**
       * @brief Write the contents of the string to the header_block
       *
       * @param str string to write to the header_block
       */
      void write_header_buffer(const std::string &str);

      //! pointers to the currently opened blocks
      std::queue<char *> opened_blocks;

  };

} // smrb
} // pst
} // ska

#endif // __SKA_PST_SMRB_DataBlockWrite_h
