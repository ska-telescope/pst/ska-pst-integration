/*
 * Copyright 2022 Square Kilometre Array Observatory
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 * contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#include <string>

#include "ska_pst/smrb/DataBlock.h"

#ifndef __SKA_PST_SMRB_DataBlockStats_h
#define __SKA_PST_SMRB_DataBlockStats_h

namespace ska {
namespace pst {
namespace smrb {

  /**
   * @brief Object Oriented class that provides an interface for allocation and destruction
   * of PSRDADA Shared Memory Ring Buffers (SMRB).
   *
   */
  class DataBlockStats
  {
    public:

      /**
       * @brief Statistics of the ring buffer
       *
       */
      typedef struct stats
      {
        //! total number of buffers
        uint64_t nbufs;

        //! size of each buffer in bytes
        uint64_t bufsz;

        //! total number of buffers written to the ring
        uint64_t written;

        //! total number of buffers read from the ring
        uint64_t read;

        //! number of filled buffers in the ring
        uint64_t full;

        //! number of cleared buffers in the ring
        uint64_t clear;

        //! number of free buffers in the ring
        uint64_t available;

      } stats_t;

      /**
       * @brief Construct a new DataBlockStats object
       *
       * @param db DataBlock which will be monitored for statistics
       */
      explicit DataBlockStats(DataBlock& db);

      /**
       * @brief Destroy the DataBlockStats object
       *
       */
      ~DataBlockStats() = default;

      /**
       * @brief Return a descriptive string of the header block statistics
       *
       * @return std::string header block stats string
       */
      std::string get_header_stats_string();

      /**
       * @brief Return a descriptive string of the data block statistics
       *
       * @return std::string data block stats string
       */
      std::string get_data_stats_string();

      /**
       * @brief Get a short string describing the current utilisation, in percent, of the data block
       *
       * @return std::string data block utilisation string
       */
      std::string get_utilisation_string();

      /**
       * @brief Print the header and data ring statistics to spdlog
       *
       */
      void print_stats();

      /**
       * @brief Return the header block statistics
       *
       * @return DataBlockStats::stats_t header block statistics
       */
      DataBlockStats::stats_t get_header_stats();

      /**
       * @brief Return the data block statistics
       *
       * @return DataBlockStats::stats_t
       */
      DataBlockStats::stats_t get_data_stats();

    protected:

    private:

      //! Reference to the data block whose statistics are described
      DataBlock& db;

  };

} // smrb
} // pst
} // ska

#endif // __SKA_PST_SMRB_DataBlockStats_h
