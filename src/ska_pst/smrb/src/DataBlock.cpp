/*
 * Copyright 2022 Square Kilometre Array Observatory
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 * contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#include <iostream>
#include <sstream>
#include <iomanip>
#include <stdexcept>
#include <cuda_runtime.h>

#include "ska_pst/common/utils/Logging.h"
#include "ska_pst/smrb/DataBlock.h"

ska::pst::smrb::DataBlock::DataBlock(std::string _key_string) :
  key_string(std::move(_key_string)),
  header_block(IPCBUF_INIT),
  data_block(IPCIO_INIT)
{
  PSTLOG_DEBUG(this, "ctor - parsing key");
  key_t key = parse_psrdada_key(key_string);

  // keys for the header + data unit
  data_block_key = key;
  header_block_key = key + 1;
  PSTLOG_TRACE(this, "data_block_key={} header_block_key={}",
    data_block_key, header_block_key);
}

void ska::pst::smrb::DataBlock::connect(int timeout)
{
  PSTLOG_DEBUG(this, "timeout={}", timeout);
  if (connected)
  {
    throw std::runtime_error("connect already connected to data block");
  }

  int connection_attempts = 0;
  while (!connected)
  {
    try
    {
      if (ipcbuf_connect(&header_block, header_block_key) < 0)
      {
        throw std::runtime_error("connect failed to connect to header block");
      }

      if (ipcio_connect(&data_block, data_block_key) < 0)
      {
        throw std::runtime_error("connect failed to connect to data block");
      }

      on_connect();
    }
    catch (std::exception& error)
    {
      if (connection_attempts < timeout)
      {
        sleep(1);
        connection_attempts++;
      }
      else
      {
        PSTLOG_WARN(this, "could not connect to data block after {} attempts", timeout);
        throw std::runtime_error("failed to connect to data block");
      }
    }
  }
}

void ska::pst::smrb::DataBlock::on_connect()
{
  // need to assert that header and data block aren't nullptr;
  PSTLOG_TRACE(this, "connecting");

  header_bufsz = ipcbuf_get_bufsz(&header_block);
  header_nbufs = ipcbuf_get_nbufs(&header_block);
  data_bufsz = ipcbuf_get_bufsz(reinterpret_cast<ipcbuf_t *>(&data_block));
  data_nbufs = ipcbuf_get_nbufs(reinterpret_cast<ipcbuf_t *>(&data_block));

  auto db = reinterpret_cast<ipcbuf_t *>(&data_block);
  device_id = ipcbuf_get_device(db);
  nreaders = ipcbuf_get_nreaders(db);

  connected = true;
}

void ska::pst::smrb::DataBlock::on_disconnect()
{
  PSTLOG_TRACE(this, "disconnecting, setting connected=false");
  connected = false;
  header_bufsz = 0;
  header_nbufs = 0;
  data_bufsz = 0;
  data_nbufs = 0;
  device_id = -1;
  nreaders = 0;
}

void ska::pst::smrb::DataBlock::disconnect()
{
  if (!connected)
  {
    throw std::runtime_error("disconnect not connected to data block");
  }
  if (ipcio_disconnect(&data_block) < 0)
  {
    throw std::runtime_error("disconnect failed to disconnect from data block");
  }
  if (ipcbuf_disconnect(&header_block) < 0)
  {
    throw std::runtime_error("disconnect failed to disconnect from header block");
  }
  on_disconnect();
}

void ska::pst::smrb::DataBlock::check_connected(bool expected)
{
  PSTLOG_TRACE(this, "expected={}", expected);
  if (connected != expected)
  {
    PSTLOG_ERROR(this, "connected={} did not equal expected={}", connected, expected);
    throw std::runtime_error("check_connected connected != expected");
  }
}

void ska::pst::smrb::DataBlock::page()
{
  PSTLOG_DEBUG(this, "paging data block");
  check_connected(true);

  if (ipcbuf_page(reinterpret_cast<ipcbuf_t*>(&data_block)) < 0)
  {
    throw std::runtime_error("could not page in data block buffers");
  }
}

auto ska::pst::smrb::DataBlock::get_nreaders() -> int
{
  return nreaders;
}

void ska::pst::smrb::DataBlock::lock_memory()
{
  PSTLOG_DEBUG("key_string={} lock_memory()", key_string);
  check_connected(true);

  auto db = reinterpret_cast<ipcbuf_t *>(&data_block);
  locked_memory = (ipcbuf_lock(db) == 0);
  if (locked_memory)
  {
    if (require_locked_memory)
    {
      throw std::runtime_error("lock_memory failed to lock data buffers into RAM");
    }
    else
    {
      PSTLOG_WARN(this, "failed to lock data buffers into RAM");
    }
  }
}

void ska::pst::smrb::DataBlock::unlock_memory()
{
  PSTLOG_DEBUG(this, "unlocking memory");
  check_connected(true);

  auto db = reinterpret_cast<ipcbuf_t *>(&data_block);
  if (locked_memory && ipcbuf_unlock(db) < 0)
  {
    throw std::runtime_error("unlock_memory failed to unlock data buffers from RAM");
  }
}

auto ska::pst::smrb::DataBlock::parse_psrdada_key(const std::string &key_string) -> key_t
{
  if (key_string.length() != 4)
  {
    PSTLOG_WARN(nullptr, "key={} was not 4 characters in length", key_string);
    throw std::runtime_error("parse_psrdada_key invalid key");
  }

  std::stringstream ss;
  key_t key = 0;
  ss << std::hex << key_string;
  ss >> key >> std::dec;

  if (!ss.eof() || ss.fail())
  {
    PSTLOG_WARN(nullptr, "did not parse all characters as hexidecimal from {}", key_string);
    throw std::runtime_error("parse_psrdada_key invalid key");
  }

  return key;
}

void ska::pst::smrb::DataBlock::register_cuda()
{
  PSTLOG_DEBUG(this, "checking data block is connected");
  check_connected(true);

  // do not register buffers if they reside on the GPU device
  if (device_id >= 0)
  {
    PSTLOG_DEBUG(this, "cannot register device memory");
    return;
  }

  unsigned int flags = 0;
  auto db = reinterpret_cast<ipcbuf_t *>(&data_block);
  for (uint64_t ibuf = 0; ibuf < db->sync->nbufs; ibuf++)
  {
    void * buf = reinterpret_cast<void *>(db->buffer[ibuf]); // NOLINT
    cudaError_t rval = cudaHostRegister(buf, data_bufsz, flags);
    if (rval != cudaSuccess)
    {
      PSTLOG_ERROR(this, "cudaHostRegister failed: {}", cudaGetErrorString(rval));
      throw std::runtime_error("could not register host memory with the CUDA driver");
    }
  }
}

void ska::pst::smrb::DataBlock::unregister_cuda()
{
  PSTLOG_DEBUG(this, "checking connected=true");
  check_connected(true);

  // dont unregister buffers if they reside on the GPU device
  if (device_id >= 0)
  {
    PSTLOG_DEBUG(this, "cannot unregister device memory");
    return;
  }

  auto db = reinterpret_cast<ipcbuf_t *>(&data_block);
  for (uint64_t ibuf = 0; ibuf < db->sync->nbufs; ibuf++)
  {
    void * buf = reinterpret_cast<void *>(db->buffer[ibuf]); // NOLINT
    cudaError_t rval = cudaHostUnregister(buf);
    if (rval != cudaSuccess)
    {
      PSTLOG_ERROR(this, "cudaHostUnregister failed: {}", cudaGetErrorString(rval));
      throw std::runtime_error("could not unregister host memory with the CUDA driver");
    }
  }
}

auto ska::pst::smrb::DataBlock::get_header_bufs_written() -> uint64_t
{
  check_connected(true);
  return ipcbuf_get_write_count(&header_block);
}

auto ska::pst::smrb::DataBlock::get_data_bufs_written() -> uint64_t
{
  check_connected(true);
  auto db = reinterpret_cast<ipcbuf_t *>(&data_block);
  return ipcbuf_get_write_count(db);
}

auto ska::pst::smrb::DataBlock::get_header_bufs_read() -> uint64_t
{
  check_connected(true);
  return ipcbuf_get_read_count(&header_block);
}

auto ska::pst::smrb::DataBlock::get_data_bufs_read() -> uint64_t
{
  check_connected(true);
  auto db = reinterpret_cast<ipcbuf_t *>(&data_block);
  return ipcbuf_get_read_count(db);
}

auto ska::pst::smrb::DataBlock::get_header_bufs_clear() -> uint64_t
{
  check_connected(true);
  return ipcbuf_get_nclear(&header_block);
}

auto ska::pst::smrb::DataBlock::get_data_bufs_clear() -> uint64_t
{
  check_connected(true);
  auto db = reinterpret_cast<ipcbuf_t *>(&data_block);
  return ipcbuf_get_nclear(db);
}

auto ska::pst::smrb::DataBlock::get_header_bufs_full() -> uint64_t
{
  check_connected(true);
  return ipcbuf_get_nfull(&header_block);
}

auto ska::pst::smrb::DataBlock::get_data_bufs_full() -> uint64_t
{
  check_connected(true);
  auto db = reinterpret_cast<ipcbuf_t *>(&data_block);
  return ipcbuf_get_nfull(db);
}

auto ska::pst::smrb::DataBlock::get_header_bufs_available() -> uint64_t
{
  check_connected(true);
  return (header_nbufs - ipcbuf_get_nfull(&header_block));
}

auto ska::pst::smrb::DataBlock::get_data_bufs_available() -> uint64_t
{
  check_connected(true);
  auto db = reinterpret_cast<ipcbuf_t *>(&data_block);
  return (data_nbufs - ipcbuf_get_nfull(db));
}

auto ska::pst::smrb::DataBlock::get_data_read_xfer() -> uint64_t
{
  check_connected(true);
  return data_block.buf.sync->r_xfers[0];
}

auto ska::pst::smrb::DataBlock::get_data_write_xfer() -> uint64_t
{
  check_connected(true);
  return data_block.buf.sync->w_xfer;
}

auto ska::pst::smrb::DataBlock::get_writer_state() -> ska::pst::smrb::State
{
  check_connected(true);
  return static_cast<ska::pst::smrb::State>(data_block.buf.sync->w_state);
}

auto ska::pst::smrb::DataBlock::get_reader_state() -> ska::pst::smrb::State
{
  check_connected(true);
  return static_cast<ska::pst::smrb::State>(data_block.buf.sync->r_states[0]);
}

auto ska::pst::smrb::DataBlock::get_data_block_key_str() const -> const std::string
{
  std::stringstream stream;
  stream << std::setfill ('0') << std::setw(4) << std::hex << data_block_key;
  return stream.str();
}

auto ska::pst::smrb::DataBlock::get_header_block_key_str() const -> const std::string
{
  std::stringstream stream;
  stream << std::setfill ('0') << std::setw(4) << std::hex << header_block_key;
  return stream.str();
}
