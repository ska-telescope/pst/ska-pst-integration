/*
 * Copyright 2022 Square Kilometre Array Observatory
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 * contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#include <cstdlib>
#include <cstring>
#include <stdexcept>

#include "ska_pst/common/utils/AsciiHeader.h"
#include "ska_pst/common/utils/Logging.h"

#include "ska_pst/smrb/DataBlockView.h"

ska::pst::smrb::DataBlockView::DataBlockView(const std::string &key_string) : ska::pst::smrb::DataBlockAccessor(key_string)
{
  log_context.insert({"entity", "DataBlockView"});
  PSTLOG_TRACE(this, "ctor");
}

ska::pst::smrb::DataBlockView::~DataBlockView()
{
  PSTLOG_TRACE(this, "dtor");
}

void ska::pst::smrb::DataBlockView::open()
{
  PSTLOG_DEBUG(this, "checking opened=false");
  check_opened(false);

  if (ipcio_open(&data_block, 'r') < 0)
  {
    throw std::runtime_error("open could not open data block for viewing");
  }
  opened = true;
}

void ska::pst::smrb::DataBlockView::close()
{
  PSTLOG_DEBUG(this, "checking opened=true");
  check_opened(true);
  opened = false;
  clear_header();
}

void ska::pst::smrb::DataBlockView::lock()
{
  PSTLOG_DEBUG(this, "checking connected=true");
  check_connected(true);
  locked = true;
}

void ska::pst::smrb::DataBlockView::unlock()
{
  PSTLOG_DEBUG(this, "setting locked=false");
  locked = false;
}

auto ska::pst::smrb::DataBlockView::open_block() -> char *
{
  PSTLOG_DEBUG(this, "checking opened=true");
  check_opened(true);

  ipcbuf_t *buf = &(data_block.buf);
  PSTLOG_TRACE(this, "write_buf={}", ipcbuf_get_write_count(buf));
  PSTLOG_TRACE(this, "viewbuf={}", buf->viewbuf);
  PSTLOG_TRACE(this, "call ipcio_open_block_read");

  curr_buf = ipcio_open_block_read(&data_block, &curr_buf_bytes, &curr_buf_id);

  PSTLOG_TRACE(this, "write_buf={}", ipcbuf_get_write_count(buf));
  PSTLOG_TRACE(this, "viewbuf={}", buf->viewbuf);

  return curr_buf;
}

auto ska::pst::smrb::DataBlockView::close_block(uint64_t bytes) -> ssize_t
{
  PSTLOG_DEBUG(this, "bytes={}", bytes);
  check_opened(true);

  data_block.bytes = 0;
  data_block.curbuf = nullptr;

  return static_cast<ssize_t>(bytes);
}

auto ska::pst::smrb::DataBlockView::read_data(char *buffer, size_t bytes) -> ssize_t
{
  PSTLOG_DEBUG(this, "buffer={} bytes={}", reinterpret_cast<void *>(buffer), bytes);
  check_opened(true);

  return ipcio_read(&data_block, buffer, bytes);
}

auto ska::pst::smrb::DataBlockView::get_current_buffer_index() const -> size_t
{
  const ipcbuf_t *buf = &(data_block.buf);
  return buf->viewbuf;
}

auto ska::pst::smrb::DataBlockView::seek_to_end(uint64_t byte_resolution) -> int
{
  PSTLOG_DEBUG(this, "byte_resolution={}", byte_resolution);
  check_opened(true);

  ipcbuf_t *buf = &(data_block.buf);

  if (ipcbuf_eod(buf))
  {
    PSTLOG_TRACE(this, "end of data");
    data_block.bytes = 0;
    data_block.curbuf = nullptr;
    return 0;
  }

  size_t writebuf = ipcbuf_get_write_count(buf);

  PSTLOG_TRACE(this, "writebuf={}", writebuf);
  PSTLOG_TRACE(this, "incrementing viewbuf={}", buf->viewbuf);

  // this could put the pointer beyond writebuf, we don't want that.
  buf->viewbuf++;
  if (buf->viewbuf < writebuf - 1)
  {
    buf->viewbuf = writebuf - 1;
  }
  else if (buf->viewbuf > writebuf)
  {
    PSTLOG_TRACE(this,
        "{} seek_to_end viewbuf={} > writebuf={}. Setting viewbuf to writebuf", key_string,
        buf->viewbuf, writebuf);
    buf->viewbuf = writebuf;
  }

  PSTLOG_TRACE(this, "viewbuf={}", buf->viewbuf);

  data_block.bytes = 0;
  data_block.curbuf = nullptr;

  if (byte_resolution)
  {
    // find last byte written in the data block
    PSTLOG_TRACE(this, "calling ipcio_tell on data_block");
    uint64_t current = ipcio_tell(&data_block);
    uint64_t too_far = current % byte_resolution;
    PSTLOG_TRACE(this, "too_far={}", too_far);
    if (too_far)
    {
      int64_t absolute_bytes = seek(current + byte_resolution - too_far, SEEK_SET); // NOLINT
      if (absolute_bytes < 0)
      {
        return -1;
      }
    }
  }

  PSTLOG_TRACE(this, "exiting");
  return 0;
}

void ska::pst::smrb::DataBlockView::seek_to_buffer_index(size_t buffer_index)
{
  PSTLOG_DEBUG(this, "buffer_index={}", buffer_index);

  if (buffer_index == 0)
  {
    PSTLOG_TRACE(this, "cannot seek to buffer_index=0");
    throw std::runtime_error("seek_to_buffer_index cannot seek to buffer_index=0");
  }

  check_opened(true);

  ipcbuf_t *buf = &(data_block.buf);

  if (ipcbuf_eod(buf))
  {
    PSTLOG_TRACE(this, "end of data");
    data_block.bytes = 0;
    data_block.curbuf = nullptr;
    return;
  }

  PSTLOG_TRACE(this, "current writer block_index={}", ipcbuf_get_write_count(buf));

  buf->viewbuf = buffer_index - 1; // the next call to ipcio_open_block_read will increment this
  data_block.bytes = 0;
  data_block.curbuf = nullptr;

  PSTLOG_TRACE(this, "exiting");
}

auto ska::pst::smrb::DataBlockView::seek(int64_t offset, int whence) -> ssize_t
{
  PSTLOG_DEBUG(this, "offset={}, whence={}", offset, whence);

  check_opened(true);

  return ipcio_seek(&data_block, offset, whence);
}

void ska::pst::smrb::DataBlockView::read_config()
{
  PSTLOG_DEBUG(this, "checking have_config=false");
  check_have_config(false);

  // read_header_buffer will read the most recently written header
  read_header_buffer(config);

  // check if the UTC_START and SCAN_ID are present, if so the full header has been written
  ska::pst::common::AsciiHeader received;
  received.load_from_string(std::string(config.begin(), config.end()));
  if (received.has("UTC_START") && received.has("SCAN_ID"))
  {
    PSTLOG_DEBUG(this, "read_config received contained UTC_START and SCAN_ID");
    memcpy(&header[0], &config[0], header_bufsz);
    have_header = true;
  }

  have_config = true;
}

void ska::pst::smrb::DataBlockView::read_header()
{
  PSTLOG_DEBUG(this, "checking have_config=true");
  check_have_config(true);

  if (!have_header)
  {
    PSTLOG_DEBUG(this, "calling read_header_buffer()");
    read_header_buffer(header);
    have_header = true;
  }
  PSTLOG_DEBUG(this, "returned");
}

void ska::pst::smrb::DataBlockView::read_header_buffer(std::vector<char> &local_buffer)
{
  PSTLOG_DEBUG(this, "reading header block buffer");

  uint64_t bytes = 0;
  auto header_ptr = ipcbuf_get_next_read(&header_block, &bytes);
  PSTLOG_DEBUG(this, "ipcbuf_get_next_read returned {} bytes", bytes);

  while (!bytes)
  {
    // wait for the next header block
    header_ptr = ipcbuf_get_next_read(&header_block, &bytes);
    PSTLOG_DEBUG(this, "ipcbuf_get_next_read returned {} bytes", bytes);

    // the view doesn't mark the header buffer as cleared
    if (ipcbuf_eod(&(header_block)))
    {
      PSTLOG_TRACE(this, "end of data on header block");
      if (ipcbuf_is_reader(&header_block))
      {
        ipcbuf_reset(&header_block);
      }
    }
    else
    {
      throw std::runtime_error("read_header empty header block");
    }
  }

  PSTLOG_TRACE(this, "copying header_ptr to this->header");
  memcpy(&local_buffer[0], header_ptr, header_bufsz);
}

auto ska::pst::smrb::DataBlockView::get_curr_buf() -> char *
{
  PSTLOG_TRACE(this, "checking opened=true");
  check_opened(true);

  return data_block.curbuf;
}

auto ska::pst::smrb::DataBlockView::check_eod() -> bool
{
  check_opened(true);

  int eod = ipcbuf_eod(&data_block.buf);
  if (eod < 0)
  {
    PSTLOG_ERROR(this, "check_eod ipcbuf_eod failed");
    throw std::runtime_error("check_eod ipcbuf_eod failed");
  }

  return (eod > 0);
}
