/*
 * Copyright 2023 Square Kilometre Array Observatory
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 * contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#include "ska_pst/common/utils/Logging.h"

#include "ska_pst/smrb/SmrbSegmentProducer.h"

ska::pst::smrb::SmrbSegmentProducer::SmrbSegmentProducer(
    const std::string &data_key,
    const std::string &weights_key)
{
  PSTLOG_DEBUG(this, "SmrbSegmentProducer");

  smrb_data_block_producer = std::make_shared<SmrbBlockProducer>(data_key);
  smrb_weights_block_producer = std::make_shared<SmrbBlockProducer>(weights_key);

  data_block_producer = smrb_data_block_producer;
  weights_block_producer = smrb_weights_block_producer;
}

ska::pst::smrb::SmrbSegmentProducer::~SmrbSegmentProducer()
{
  PSTLOG_DEBUG(this, "~SmrbSegmentProducer()");
}

void ska::pst::smrb::SmrbSegmentProducer::connect(int timeout)
{
  PSTLOG_DEBUG(this, "connect()");
  smrb_data_block_producer->connect(timeout);
  smrb_weights_block_producer->connect(timeout);
  PSTLOG_DEBUG(this, "connect() complete");
}

void ska::pst::smrb::SmrbSegmentProducer::open()
{
  PSTLOG_DEBUG(this, "open()");
  smrb_data_block_producer->open();
  smrb_weights_block_producer->open();
  PSTLOG_DEBUG(this, "open() complete");
}

void ska::pst::smrb::SmrbSegmentProducer::disconnect()
{
  PSTLOG_DEBUG(this, "disconnect()");
  smrb_data_block_producer->disconnect();
  smrb_weights_block_producer->disconnect();
  PSTLOG_DEBUG(this, "disconnect() complete");
}

void ska::pst::smrb::SmrbSegmentProducer::close()
{
  PSTLOG_DEBUG(this, "close()");
  smrb_data_block_producer->close();
  smrb_weights_block_producer->close();
  PSTLOG_DEBUG(this, "close() complete");
}

auto ska::pst::smrb::SmrbSegmentProducer::next_segment() -> ska::pst::common::SegmentProducer::Segment
{
  PSTLOG_DEBUG(this, "next_segment");

  ska::pst::common::SegmentProducer::Segment result;
  result.data = data_block_producer->next_block();

  // exit early if our block data block is null
  if (result.data.block == nullptr)
  {
    result.weights.block = nullptr;
    return result;
  }

  // get the current ring buffer index from data_block and wait for the same buffer in the weights block
  auto buffer_index = smrb_data_block_producer->get_current_buffer_index();

  PSTLOG_DEBUG(this, "next_segment data buffer_index={}", buffer_index);

  smrb_weights_block_producer->seek_to_buffer_index(buffer_index);

  result.weights = weights_block_producer->next_block();

  // if either data or weights have reached end-of-data, ensure that both are flagged as end-of-data
  if (result.data.block == nullptr || result.weights.block == nullptr)
  {
    result.data.block = result.weights.block = nullptr;
  }

  return result;
}
