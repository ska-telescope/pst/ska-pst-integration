/*
 * Copyright 2022 Square Kilometre Array Observatory
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 * contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#include <regex>

#include "ska_pst/smrb/DataBlockManager.h"
#include "ska_pst/common/utils/Logging.h"
#include "ska_pst/common/utils/ValidationContext.h"
#include "ska_pst/common/utils/ValidationRegex.h"

void check_header_key(const ska::pst::common::AsciiHeader& config, ska::pst::common::ValidationContext* context, std::string _val_expected_key, std::string _key_pattern)
{
  std::string val_expected_key(std::move(_val_expected_key));
  std::string key_pattern(std::move(_key_pattern));

  if (config.has(val_expected_key)) {
    auto config_value = config.get_val(val_expected_key);
    std::regex regex_pattern(key_pattern);
    if (!std::regex_match(config_value, regex_pattern))
    {
      context->add_value_regex_error(val_expected_key, config_value, key_pattern);
    }
  } else {
    context->add_missing_field_error(val_expected_key);
  }
}

void ska::pst::smrb::DataBlockManager::validate_configure_beam(const ska::pst::common::AsciiHeader& config, ska::pst::common::ValidationContext *context)
{
  PSTLOG_DEBUG(this, "{} validate_configure_beam", entity);

  // Loop through header keys
  for (const auto &val_expected_key : header_keys)
  {
    check_header_key(config, context, val_expected_key, ska::pst::common::dada_key_regex);
  }

  for (const auto &val_expected_key : buffer_keys)
  {
    check_header_key(config, context, val_expected_key, buffer_pattern);
  }

  context->throw_error_if_not_empty();
  PSTLOG_DEBUG(this, "{} validate_configure_beam - is valid", entity);
}

void ska::pst::smrb::DataBlockManager::validate_configure_scan(const ska::pst::common::AsciiHeader& config, ska::pst::common::ValidationContext *context) // NOLINT
{
  PSTLOG_DEBUG(this, "{} validate_configure_scan", entity);
}

void ska::pst::smrb::DataBlockManager::validate_start_scan(const ska::pst::common::AsciiHeader& config) // NOLINT
{
  PSTLOG_DEBUG(this, "{} validate_start_scan", entity);
}

void ska::pst::smrb::DataBlockManager::perform_initialise()
{
  PSTLOG_DEBUG(this, "{} perform_initialise", entity);
}

void ska::pst::smrb::DataBlockManager::perform_configure_beam()
{
  PSTLOG_DEBUG(this, "{} perform_configure_beam", entity);
  PSTLOG_DEBUG(this, "{} configure data_key={}", entity, beam_config.get_val("DATA_KEY"));
  PSTLOG_DEBUG(this, "{} configure weights_key={}", entity, beam_config.get_val("WEIGHTS_KEY"));
  PSTLOG_DEBUG(this, "{} configure hb_nbufs={}", entity, beam_config.get<uint64_t>("HB_NBUFS"));
  PSTLOG_DEBUG(this, "{} configure hb_bufsz={}", entity, beam_config.get<uint64_t>("HB_BUFSZ"));
  PSTLOG_DEBUG(this, "{} configure db_nbufs={}", entity, beam_config.get<uint64_t>("DB_NBUFS"));
  PSTLOG_DEBUG(this, "{} configure db_bufsz={}", entity, beam_config.get<uint64_t>("DB_BUFSZ"));
  PSTLOG_DEBUG(this, "{} configure wb_nbufs={}", entity, beam_config.get<uint64_t>("WB_NBUFS"));
  PSTLOG_DEBUG(this, "{} configure wb_bufsz={}", entity, beam_config.get<uint64_t>("WB_BUFSZ"));

  check_beam_configured(false);

  PSTLOG_INFO(this, "Initializing data and weights ring buffers", entity);
  data_ring_buffer = std::make_shared<ska::pst::smrb::DataBlockCreate>(beam_config.get_val("DATA_KEY"));
  weights_ring_buffer = std::make_shared<ska::pst::smrb::DataBlockCreate>(beam_config.get_val("WEIGHTS_KEY"));

  PSTLOG_INFO(this, "Initializing data and weights stats", entity);
  data_ring_stats = std::make_shared<ska::pst::smrb::DataBlockStats>(*data_ring_buffer);
  weights_ring_stats = std::make_shared<ska::pst::smrb::DataBlockStats>(*weights_ring_buffer);

  PSTLOG_INFO(this, "Creating data and weights ring buffers", entity);
  data_ring_buffer->create(beam_config.get<uint64_t>("HB_NBUFS"), beam_config.get<uint64_t>("HB_BUFSZ"), beam_config.get<uint64_t>("DB_NBUFS"), beam_config.get<uint64_t>("DB_BUFSZ"), num_readers, device_id);
  weights_ring_buffer->create(beam_config.get<uint64_t>("HB_NBUFS"), beam_config.get<uint64_t>("HB_BUFSZ"), beam_config.get<uint64_t>("WB_NBUFS"), beam_config.get<uint64_t>("WB_BUFSZ"), num_readers, device_id);

  beam_configured = true;
}

void ska::pst::smrb::DataBlockManager::perform_configure_scan()
{
  PSTLOG_DEBUG(this, "{} perform_configure_scan", entity);
}

void ska::pst::smrb::DataBlockManager::perform_start_scan()
{
  PSTLOG_DEBUG(this, "{} perform_start_scan", entity);
  std::unique_lock<std::mutex> scanning_lock(scanning_mutex);
  scanning = true;
  PSTLOG_DEBUG(this, "{} perform_start_scan scanning={}", entity, scanning);
  scanning_lock.unlock();
  scanning_cond.notify_all();
}

void ska::pst::smrb::DataBlockManager::perform_scan()
{
  PSTLOG_DEBUG(this, "{} perform_scan scanning={}", entity, scanning);
  while (scanning)
  {
    using namespace std::chrono_literals;
    std::chrono::milliseconds timeout = utilisation_publication_period_ms * 1ms;
    std::unique_lock<std::mutex> scanning_lock(scanning_mutex);
    bool stop = state_cond.wait_for(scanning_lock, timeout, [&]{return (!scanning);});
    scanning_lock.unlock();

    // publish the utilisation statistics
    if (!stop && scanning)
    {
      PSTLOG_INFO(this, "{} {}", entity, get_utilisation_string());
    }
  }
}

void ska::pst::smrb::DataBlockManager::perform_stop_scan()
{
  PSTLOG_DEBUG(this, "{} perform_stop_scan", entity);
  std::unique_lock<std::mutex> scanning_lock(scanning_mutex);
  scanning = false;
  PSTLOG_DEBUG(this, "{} perform_stop_scan scanning={}", entity, scanning);
  scanning_lock.unlock();
  scanning_cond.notify_all();
}

void ska::pst::smrb::DataBlockManager::perform_deconfigure_scan()
{
  PSTLOG_DEBUG(this, "{} perform_deconfigure_scan", entity);
}

void ska::pst::smrb::DataBlockManager::perform_deconfigure_beam()
{
  PSTLOG_DEBUG(this, "{} perform_deconfigure_beam", entity);
  check_beam_configured(true);

  PSTLOG_INFO(this, "{} releasing data and weights ring buffers", entity);
  data_ring_buffer->destroy();
  weights_ring_buffer->destroy();
  beam_configured = false;

  beam_config.reset();

  data_ring_buffer = nullptr;
  data_ring_stats = nullptr;
  weights_ring_buffer = nullptr;
  weights_ring_stats = nullptr;
}

void ska::pst::smrb::DataBlockManager::perform_terminate()
{
  PSTLOG_DEBUG(this, "{} perform_terminate", entity);
}

void ska::pst::smrb::DataBlockManager::configure_beam_from_file(const std::string &config_file)
{
  PSTLOG_TRACE(this, "{} configure_beam_from_file file={}", entity, config_file);
  ska::pst::common::AsciiHeader config;
  config.load_from_file(config_file);
  PSTLOG_TRACE(this, "{} configure_beam_from_file done", entity, config_file);

  PSTLOG_TRACE(this, "{} configure_beam", entity);
  configure_beam(config);
  PSTLOG_TRACE(this, "{} configure_beam done", entity);
}

auto ska::pst::smrb::DataBlockManager::get_beam_configuration_header_key(const std::string &header_key) -> std::string
{
  PSTLOG_DEBUG(this, "{} get_beam_configuration_header_key header_key={}", entity, header_key);
  check_beam_configured(true);
  return beam_config.get_val(header_key);
}

auto ska::pst::smrb::DataBlockManager::get_beam_configuration_buffer_key(const std::string &buffer_key) -> uint64_t
{
  PSTLOG_DEBUG(this, "{} get_beam_configuration_buffer_key buffer_key={}", entity, buffer_key);
  check_beam_configured(true);
  return beam_config.get<uint64_t>(buffer_key.c_str());
}

void ska::pst::smrb::DataBlockManager::print_statistics()
{
  PSTLOG_DEBUG(this, "{} print_statistics()", entity);
  check_beam_configured(true);
  data_ring_stats->print_stats();
  weights_ring_stats->print_stats();
}

auto ska::pst::smrb::DataBlockManager::get_data_header_stats() -> ska::pst::smrb::DataBlockStats::stats_t
{
  check_beam_configured(true);
  return data_ring_stats->get_header_stats();
}

auto ska::pst::smrb::DataBlockManager::get_data_data_stats() -> ska::pst::smrb::DataBlockStats::stats_t
{
  check_beam_configured(true);
  return data_ring_stats->get_data_stats();
}

auto ska::pst::smrb::DataBlockManager::get_weights_header_stats() -> ska::pst::smrb::DataBlockStats::stats_t
{
  check_beam_configured(true);
  return weights_ring_stats->get_header_stats();
}

auto ska::pst::smrb::DataBlockManager::get_weights_data_stats() -> ska::pst::smrb::DataBlockStats::stats_t
{
  check_beam_configured(true);
  return weights_ring_stats->get_data_stats();
}

auto ska::pst::smrb::DataBlockManager::get_utilisation_string() -> std::string
{
  check_beam_configured(true);
  std::ostringstream oss;
  oss << "Data: " << data_ring_stats->get_utilisation_string() << " Weights: " << weights_ring_stats->get_utilisation_string();
  return oss.str();
}

void ska::pst::smrb::DataBlockManager::check_beam_configured(bool required)
{
  if (beam_configured != required)
  {
    PSTLOG_ERROR(this, "{} beam_configured != {}", entity, required);
    throw std::runtime_error("check_beam_configured configured != required");
  }
}

void ska::pst::smrb::DataBlockManager::wait_for_duration(int duration_ms, volatile bool * signal)
{
  check_beam_configured(true);
  static constexpr int sleep_duration_ms = 10;
  static constexpr int microseconds_per_millisecond = 1000;
  bool persist = true;
  int remaining = duration_ms;
  while (persist)
  {
    usleep(sleep_duration_ms * microseconds_per_millisecond);
    remaining -= sleep_duration_ms;
    persist = ((remaining > 0) && (!(*signal)));
  }
}

void ska::pst::smrb::DataBlockManager::wait_for_observation(volatile bool * signal)
{
  check_beam_configured(true);

  static constexpr unsigned sleep_duration_us = 100000;
  bool persist = true;
  PSTLOG_DEBUG(this, "{} wait_for_observation waiting for single observation to complete", entity);
  while (persist)
  {
    usleep(sleep_duration_us);
    bool observation_complete =
      (data_ring_buffer->get_data_write_xfer() == 1) &&
      (data_ring_buffer->get_data_read_xfer() == 1) &&
      (data_ring_buffer->get_writer_state() == State::DISCONNECTED) &&
      (data_ring_buffer->get_reader_state() == State::DISCONNECTED);

    PSTLOG_DEBUG(this, "{} wait_for_observation write_xfer={} read_xfer={} writer_state={} reader_state={}", entity,
      data_ring_buffer->get_data_write_xfer(), data_ring_buffer->get_data_read_xfer(), data_ring_buffer->get_writer_state(), data_ring_buffer->get_reader_state());
    persist = (!*signal && !observation_complete);
  }
  static constexpr unsigned second_in_microseconds = 1000000;
  usleep(second_in_microseconds);
  PSTLOG_DEBUG(this, "{} wait_for_observation exiting", entity);
}

void ska::pst::smrb::DataBlockManager::wait_for_signal(volatile bool * signal)
{
  static constexpr unsigned sleep_duration_us = 100000;
  bool persist = true;
  while (persist)
  {
    usleep(sleep_duration_us);
    persist = !(*signal);
  }
}
