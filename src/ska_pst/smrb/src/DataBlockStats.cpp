/*
 * Copyright 2022 Square Kilometre Array Observatory
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 * contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#include <iomanip>
#include <iostream>
#include <sstream>

#include "ska_pst/common/definitions.h"
#include "ska_pst/common/utils/Logging.h"
#include "ska_pst/smrb/DataBlockStats.h"

ska::pst::smrb::DataBlockStats::DataBlockStats(ska::pst::smrb::DataBlock& _db) :
  db(_db)
{
}

void ska::pst::smrb::DataBlockStats::print_stats()
{
  PSTLOG_INFO(this, "{}", get_header_stats_string());
  PSTLOG_INFO(this, "{}", get_data_stats_string());
}

auto ska::pst::smrb::DataBlockStats::get_header_stats_string() -> std::string
{
  std::ostringstream oss;
  oss << "HEADER nbufs=" << db.get_header_nbufs() << " bufsz=" << db.get_header_bufsz() << " written=" << db.get_header_bufs_written()
      << " read=" << db.get_header_bufs_read() << " full=" << db.get_header_bufs_full() << " clear=" << db.get_header_bufs_clear()
      << " available=" << db.get_header_bufs_available();
  return oss.str();
}

auto ska::pst::smrb::DataBlockStats::get_data_stats_string() -> std::string
{
  std::ostringstream oss;
  oss << "DATA   nbufs=" << db.get_data_nbufs() << " bufsz=" << db.get_data_bufsz() << " written=" << db.get_data_bufs_written()
      << " read=" << db.get_data_bufs_read() << " full=" << db.get_data_bufs_full() << " clear=" << db.get_data_bufs_clear()
      << " available=" << db.get_data_bufs_available();
  return oss.str();
}

auto ska::pst::smrb::DataBlockStats::get_utilisation_string() -> std::string
{
  std::ostringstream oss;
  float percent_full = (static_cast<float>(db.get_data_bufs_full()) / static_cast<float>(db.get_data_nbufs())) * ska::pst::common::percentiles_per_100;
  uint64_t bytes_written = db.get_data_bufs_written() * db.get_data_bufsz();
  uint64_t bytes_read = db.get_data_bufs_read() * db.get_data_bufsz();
  oss << std::setprecision(3) << percent_full << "% bytes_written=" << bytes_written << " bytes_read=" << bytes_read;
  return oss.str();
}

auto ska::pst::smrb::DataBlockStats::get_header_stats() -> ska::pst::smrb::DataBlockStats::stats_t
{
  ska::pst::smrb::DataBlockStats::stats_t stats;
  stats.nbufs = db.get_header_nbufs();
  stats.bufsz = db.get_header_bufsz();
  stats.written = db.get_header_bufs_written();
  stats.read = db.get_header_bufs_read();
  stats.full = db.get_header_bufs_full();
  stats.clear = db.get_header_bufs_clear();
  stats.available = db.get_header_bufs_available();
  return stats;
}

auto ska::pst::smrb::DataBlockStats::get_data_stats() -> ska::pst::smrb::DataBlockStats::stats_t
{
  ska::pst::smrb::DataBlockStats::stats_t stats;
  stats.nbufs = db.get_data_nbufs();
  stats.bufsz = db.get_data_bufsz();
  stats.written = db.get_data_bufs_written();
  stats.read = db.get_data_bufs_read();
  stats.full = db.get_data_bufs_full();
  stats.clear = db.get_data_bufs_clear();
  stats.available = db.get_data_bufs_available();
  return stats;
}
