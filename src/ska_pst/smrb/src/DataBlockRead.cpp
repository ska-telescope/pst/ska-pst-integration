/*
 * Copyright 2022 Square Kilometre Array Observatory
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 * contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#include <cstdlib>
#include <cstring>
#include <stdexcept>
#include "ska_pst/common/utils/Logging.h"

#include "ska_pst/smrb/DataBlockRead.h"

ska::pst::smrb::DataBlockRead::DataBlockRead(const std::string &key_string) : ska::pst::smrb::DataBlockAccessor(key_string)
{
  log_context.insert({"entity", "DataBlockRead"});
  PSTLOG_TRACE(this, "ctor");
}

ska::pst::smrb::DataBlockRead::~DataBlockRead()
{
  PSTLOG_TRACE(this, "dtor");
}

void ska::pst::smrb::DataBlockRead::open()
{
  PSTLOG_DEBUG(this, "checking have_header=true and opened=false");
  check_have_header(true);
  check_opened(false);

  if (ipcio_open(&data_block, 'R') < 0)
  {
    PSTLOG_ERROR(this, "unable to open data_block {} for reading", key_string);
    throw std::runtime_error("open could not open data_block for reading");
  }
  opened = true;
}

void ska::pst::smrb::DataBlockRead::lock()
{
  PSTLOG_DEBUG(this, "checking connected=true and locked=false");
  check_connected(true);
  check_locked(false);

  if (ipcbuf_lock_read(&header_block) < 0)
  {
    PSTLOG_ERROR(this, "unable to lock header block {} for reading", key_string);
    throw std::runtime_error("{} lock could not lock header block for reading");
  }
  locked = true;
}

void ska::pst::smrb::DataBlockRead::unlock()
{
  PSTLOG_DEBUG(this, "checking locked=true and opened=false");
  check_locked(true);
  check_opened(false);

  if (ipcbuf_unlock_read(&header_block) < 0)
  {
    PSTLOG_ERROR(this, "unable to unlock header block {} for reading", key_string);
    throw std::runtime_error("unlock could not unlock header block from reading");
  }
  locked = false;
}

void ska::pst::smrb::DataBlockRead::close()
{
  PSTLOG_DEBUG(this, "checking opened=true");
  check_opened(true);

  if (ipcio_is_open(&data_block))
  {
    PSTLOG_TRACE(this, "calling ipcio_close");
    if (ipcio_close(&data_block) < 0)
    {
      throw std::runtime_error("close could not unlock data block from reading");
    }
  }
  opened = false;
  clear_header();
}

void ska::pst::smrb::DataBlockRead::read_config()
{
  PSTLOG_DEBUG(this, "checking locked=true and have_config=false");
  check_locked(true);
  check_have_config(false);

  if (!skip_read_config)
  {
    uint64_t bytes{};
    char * buf = ipcbuf_get_next_read(&header_block, &bytes);

    // make a local copy of the config
    PSTLOG_TRACE(this, "copying header_buf to config");
    memcpy(&config[0], buf, header_bufsz);

    if (ipcbuf_mark_cleared(&header_block) < 0)
    {
      throw std::runtime_error("read_config could not mark header buffer cleared");
    }
  }
  have_config = true;
}

void ska::pst::smrb::DataBlockRead::read_header()
{
  PSTLOG_DEBUG(this, "checking have_config=true and have_header=false");
  check_have_config(true);
  check_have_header(false);

  uint64_t bytes{};
  char * buf = ipcbuf_get_next_read(&header_block, &bytes);
  PSTLOG_DEBUG(this, "read {} bytes", bytes);

  // make a local copy of the header
  PSTLOG_TRACE(this, "copying header_buf to header");
  memcpy(&header[0], buf, header_bufsz);

  if (ipcbuf_mark_cleared(&header_block) < 0)
  {
    PSTLOG_ERROR(this, "could not mark header buffer cleared");
    throw std::runtime_error("could not mark header buffer cleared");
  }
  have_header = true;
}

auto ska::pst::smrb::DataBlockRead::read_data(char * buffer, size_t bytes) -> ssize_t
{
  PSTLOG_DEBUG(this, "buffer={} bytes={}", reinterpret_cast<void*>(buffer), bytes);
  check_opened(true);

  if (check_eod())
  {
    PSTLOG_DEBUG(this, "end of data is true");
    return 0;
  }

  ssize_t bytes_read = ipcio_read(&data_block, buffer, bytes);

  if (bytes_read < 0)
  {
    PSTLOG_ERROR(this, "could not read bytes from data block");
    throw std::runtime_error("could not read bytes from data block");
  }
  return bytes_read;
}

auto ska::pst::smrb::DataBlockRead::open_block() -> char *
{
  PSTLOG_DEBUG(this, "checking opened=true");
  check_opened(true);

  if (block_open)
  {
    PSTLOG_ERROR(this, "block is already open");
    throw std::runtime_error("open_block block is already open");
  }

  // check for EOD prior to opening a new block
  if (check_eod())
  {
    curr_buf_bytes = 0;
    curr_buf = nullptr;
    PSTLOG_TRACE(this, "EOD encountered, returning nullptr");
    return curr_buf;
  }

  PSTLOG_TRACE(this, "calling ipcio_open_block_read");
  curr_buf = ipcio_open_block_read(&data_block, &curr_buf_bytes ,&curr_buf_id);
  PSTLOG_TRACE(this, "curr_buf_bytes={} curr_buf_id={} curr_buf={}", curr_buf_bytes, curr_buf_id, reinterpret_cast<void*>(curr_buf));

  if (curr_buf_bytes == 0)
  {
    PSTLOG_TRACE(this, "ipcio_open_block_read returned 0 bytes, eod={}", check_eod());
  }

  if (curr_buf == nullptr)
  {
    PSTLOG_ERROR(this, "failed to open block for reading");
    throw std::runtime_error("open_block failed to open block for reading");
  }

  block_open = true;
  return curr_buf;
}

auto ska::pst::smrb::DataBlockRead::close_block(uint64_t bytes) -> ssize_t
{
  PSTLOG_DEBUG(this, "bytes={}", bytes);
  check_opened(true);

  if (!block_open) {
    PSTLOG_ERROR(this, "block already closed");
    throw std::runtime_error("close_block block already closed");
  }

  if (bytes != curr_buf_bytes)
  {
    PSTLOG_ERROR(this, "bytes != curr_buf_bytes");
    throw std::runtime_error("close_block bytes != curr_buf_bytes");
  }

  PSTLOG_TRACE(this, "ipcio_close_block_read(&data_block, {})", bytes);
  ssize_t result = ipcio_close_block_read(&data_block, bytes);

  curr_buf = nullptr;
  curr_buf_bytes = 0;
  block_open = false;

  return result;
}

auto ska::pst::smrb::DataBlockRead::check_eod() -> bool
{
  PSTLOG_TRACE(this, "checking opened=true");
  check_opened(true);

  int eod = ipcbuf_eod(&data_block.buf);
  if (eod < 0)
  {
    PSTLOG_ERROR(this, "ipcbuf_eod failed - eod={}", eod);
    throw std::runtime_error("ipcbuf_eod failed");
  }

  return (eod > 0);
}
