/*
 * Copyright 2022 Square Kilometre Array Observatory
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 * contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#include <stdexcept>
#include "ska_pst/common/utils/Logging.h"

#include "ska_pst/smrb/DataBlockCreate.h"

ska::pst::smrb::DataBlockCreate::DataBlockCreate(const std::string &key_string) : DataBlock(key_string)
{
  log_context.insert({"entity", "DataBlockCreate"});
  PSTLOG_DEBUG(this, "ctor - key={}", key_string);
}

ska::pst::smrb::DataBlockCreate::~DataBlockCreate()
{
  PSTLOG_TRACE(this, "dtor");
  destroy();
}

void ska::pst::smrb::DataBlockCreate::create(uint64_t hdr_nbufs, uint64_t hdr_bufsz, uint64_t data_nbufs, uint64_t data_bufsz, unsigned num_readers, int device_id)
{
  PSTLOG_DEBUG(this, "create data nbufs={} bufsz={} nread={}", data_nbufs, data_bufsz, num_readers);
  if (created){
    throw std::runtime_error("create block already created");
  }
  if (ipcbuf_create_work(reinterpret_cast<ipcbuf_t *>(&data_block), data_block_key, data_nbufs, data_bufsz, num_readers, device_id) < 0)
  {
    PSTLOG_WARN(this, "could not create DADA data block with key={:04x} - attempting to destroy and recreate it", data_block_key);

    // try to connect to the data_block, we do not normally expect this to exist, but delete it if it does
    if (ipcbuf_connect(reinterpret_cast<ipcbuf_t *>(&data_block), data_block_key) == 0)
    {
      PSTLOG_INFO(this, "DADA data block with key={:04x} already exists, attempting to destroy it.", data_block_key);
      ipcbuf_destroy(reinterpret_cast<ipcbuf_t *>(&data_block));
    }

    if (ipcbuf_create_work(reinterpret_cast<ipcbuf_t *>(&data_block), data_block_key, data_nbufs, data_bufsz, num_readers, device_id) < 0)
    {
      PSTLOG_ERROR(this, "could not create DADA data block with key={:04x} bufsz={} nbufsz={} nread={}", data_block_key, data_bufsz, data_nbufs, num_readers);
      throw std::runtime_error("create ipcbuf_create_work failed");
    }
  }

  PSTLOG_DEBUG(this, "header nbufs={} bufsz={} nread={}", hdr_nbufs, hdr_bufsz, num_readers);
  if (ipcbuf_create(&header_block, header_block_key, hdr_nbufs, hdr_bufsz, num_readers) < 0)
  {
    PSTLOG_ERROR(this, "could not create DADA header block with key={:04x}", header_block_key);

    if (ipcbuf_connect(&header_block, header_block_key) == 0)
    {
      PSTLOG_INFO(this, "DADA header block with key={:04x} already exists, attempting to destroy it.", header_block_key);
      ipcbuf_destroy (&header_block);
    }

    if (ipcbuf_create(&header_block, header_block_key, hdr_nbufs, hdr_bufsz, num_readers) < 0)
    {
      PSTLOG_ERROR(this, "could not create DADA header block with key={:04x} bufsz={}, nbufs={}", header_block_key, hdr_bufsz, hdr_nbufs);
      throw std::runtime_error("create ipcbuf_create failed");
    }
  }
  created = true;
  on_connect();
}

void ska::pst::smrb::DataBlockCreate::destroy()
{
  if (created)
  {
    PSTLOG_TRACE(this, "calling ipcbuf_destroy({})", reinterpret_cast<void *>(&data_block));
    ipcbuf_destroy(reinterpret_cast<ipcbuf_t *>(&data_block));
    PSTLOG_TRACE(this, "calling ipcbuf_destroy({})", reinterpret_cast<void *>(&header_block));
    ipcbuf_destroy(&header_block);
    created = false;
    on_disconnect();
  }
}
