/*
 * Copyright 2023 Square Kilometre Array Observatory
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 * contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#include "ska_pst/smrb/tests/SmrbBlockProducerTest.h"
#include "ska_pst/smrb/SmrbBlockProducer.h"
#include "ska_pst/common/testutils/GtestMain.h"

#include <iostream>
#include <csignal>
#include "ska_pst/common/utils/Logging.h"

using ska::pst::common::test::test_data_file;

auto main(int argc, char *argv[]) -> int
{
  return ska::pst::common::test::gtest_main(argc, argv);
}

namespace ska::pst::smrb::test
{

  SmrbBlockProducerTest::SmrbBlockProducerTest() :
    ::testing::Test(),
    helper("dada", 1) // one reader
  {
  }

  void SmrbBlockProducerTest::SetUp()
  {
    helper.setup();
    helper.enable_reader();

    config.load_from_file(test_data_file("scan_config.txt"));
    header.load_from_file(test_data_file("full_header.txt"));

    helper.set_config(config);
    helper.set_header(header);
  }

  void SmrbBlockProducerTest::TearDown()
  {
    helper.teardown();
  }

  TEST_F(SmrbBlockProducerTest, test_construct_delete) // NOLINT
  {
    SmrbBlockProducer db("eeee");
  }

  TEST_F(SmrbBlockProducerTest, fail_open_before_connect) // NOLINT
  {
    SmrbBlockProducer db("dada");
    EXPECT_THROW(db.open(), std::runtime_error);
  }

  TEST_F(SmrbBlockProducerTest, fail_next_block_before_open) // NOLINT
  {
    PSTLOG_TRACE(this, "fail_next_block_before_open helper.start");
    helper.start();

    PSTLOG_TRACE(this, "fail_next_block_before_open construct SmrbBlockProducer");
    SmrbBlockProducer db("dada");

    PSTLOG_TRACE(this, "fail_next_block_before_open db.connect");
    db.connect(0);

    PSTLOG_TRACE(this, "fail_next_block_before_open db.next_block");
    EXPECT_THROW(db.next_block(), std::runtime_error);
  }

  TEST_F(SmrbBlockProducerTest, test_get_header) // NOLINT
  {
    helper.start();

    SmrbBlockProducer db("dada");
    db.connect(0);
    db.open();

    EXPECT_EQ(db.get_header(), header);

    db.close();
    db.disconnect();
  }

  TEST_F(SmrbBlockProducerTest, test_next_block) // NOLINT
  {
    SmrbBlockProducer db("dada");

    helper.start();
    helper.write(1); // write one buffer and clear it

    PSTLOG_TRACE(this, "test_next_block db.connect()");
    db.connect(0);

    PSTLOG_TRACE(this, "test_next_block db.open()");
    db.open();

    PSTLOG_TRACE(this, "test_next_block db.next_block()");
    auto block = db.next_block();
    EXPECT_NE(block.block, nullptr);

    auto curr_buf = db.get_current_buffer_index();
    PSTLOG_TRACE(this, "test_next_block curr_buf={}", curr_buf);
    EXPECT_EQ(curr_buf, 1);
    EXPECT_EQ(block.obs_offset, curr_buf * helper.get_data_block_bufsz());

    db.close();
    db.disconnect();
  }

  TEST_F(SmrbBlockProducerTest, test_multiple_blocks) // NOLINT
  {
    helper.start();

    SmrbBlockProducer db("dada");
    db.connect(0);
    db.open();

    ska::pst::common::BlockProducer::Block previous;

    size_t constexpr test_nloops = 12;
    size_t constexpr bufs_per_loop = 2;

    helper.write(0); // prime the pump
    auto block_bufsz = helper.get_data_block_bufsz();

    for (unsigned i = 0; i < test_nloops; i++)
    {
      PSTLOG_TRACE(this, "test_multiple_next_block {} of {} iterations", i, test_nloops);

      helper.write(bufs_per_loop);
      auto block = db.next_block();

      EXPECT_NE(block.block, nullptr);
      EXPECT_NE(block.block, previous.block);

      auto curr_buf = db.get_current_buffer_index();

      EXPECT_EQ(curr_buf, (i + 1) * bufs_per_loop);
      EXPECT_EQ(block.obs_offset, curr_buf * block_bufsz);
      previous = block;
    }

    db.close();
    db.disconnect();
  }

  TEST_F(SmrbBlockProducerTest, test_multiple_blocks_asynchronous) // NOLINT
  {
    helper.start();

    SmrbBlockProducer db("dada");
    db.connect(0);
    db.open();

    float constexpr delay_ms = 10;
    size_t constexpr test_nblocks = 64;
    auto block_bufsz = helper.get_data_block_bufsz();

    PSTLOG_DEBUG(this, "test_read starting write_and_close({}, {})", delay_ms, test_nblocks);
    std::thread delay_thread = std::thread(&DataBlockTestHelper::write_and_close, &helper, delay_ms, test_nblocks);

    bool eod = false;
    while (!eod)
    {
      PSTLOG_DEBUG(this, "test_read next_block");
      auto block = db.next_block();

      if (block.block == nullptr)
      {
        break;
      }
      auto curr_buf = db.get_current_buffer_index();
      EXPECT_EQ(block.obs_offset, curr_buf * block_bufsz);
    }

    PSTLOG_DEBUG(this, "test_read reading complete");
    delay_thread.join();
    PSTLOG_DEBUG(this, "test_read delay_thread joined");

    db.close();
    db.disconnect();
  }

} // namespace ska::pst::smrb::test
