/*
 * Copyright 2022 Square Kilometre Array Observatory
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 * contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#include "ska_pst/smrb/tests/DataBlockWriteTest.h"
#include "ska_pst/smrb/DataBlockWrite.h"
#include "ska_pst/common/utils/AsciiHeader.h"
#include "ska_pst/common/testutils/GtestMain.h"

#include <iostream>

using ska::pst::common::test::test_data_file;

auto main(int argc, char* argv[]) -> int
{
  return ska::pst::common::test::gtest_main(argc, argv); // NOLINT
}

namespace ska::pst::smrb::test {

DataBlockWriteTest::DataBlockWriteTest()
    : ::testing::Test()
{
}

void DataBlockWriteTest::SetUp()
{
    _db = std::make_shared<DataBlockCreate>("dada"); // NOLINT
    _db->create(hdr_nbufs, hdr_bufsz, data_nbufs, data_bufsz, num_readers, device_id); // NOLINT

    scan_config.load_from_file(test_data_file("scan_config.txt")); // NOLINT
    full_header.load_from_file(test_data_file("full_header.txt")); // NOLINT
}

void DataBlockWriteTest::TearDown()
{
    if (_db)
    {
        _db->destroy(); // NOLINT
    }
    _db = nullptr; // NOLINT
}

TEST_F(DataBlockWriteTest, test_construct_delete) // NOLINT
{
    DataBlockWrite db("eeee"); // NOLINT
}

TEST_F(DataBlockWriteTest, test_lock_unlock) // NOLINT
{
    DataBlockWrite db("dada"); // NOLINT
    db.connect(0); // NOLINT
    EXPECT_EQ(db.get_locked(), false); // NOLINT
    db.lock(); // NOLINT
    EXPECT_EQ(db.get_locked(), true); // NOLINT
    db.unlock(); // NOLINT
    EXPECT_EQ(db.get_locked(), false); // NOLINT
    db.disconnect(); // NOLINT
}

TEST_F(DataBlockWriteTest, test_lock_without_connect) // NOLINT
{
    DataBlockWrite db("dada"); // NOLINT
    EXPECT_THROW(db.lock(), std::runtime_error); // NOLINT
}

TEST_F(DataBlockWriteTest, test_open_close) // NOLINT
{
    DataBlockWrite db("dada"); // NOLINT
    db.connect(0); // NOLINT
    db.lock(); // NOLINT
    EXPECT_EQ(db.get_opened(), false); // NOLINT
    EXPECT_THROW(db.open(), std::runtime_error); // NOLINT
    db.write_config(scan_config.raw()); // NOLINT
    EXPECT_THROW(db.open(), std::runtime_error); // NOLINT
    db.write_header(full_header.raw()); // NOLINT

    db.open(); // NOLINT
    EXPECT_EQ(db.get_opened(), true); // NOLINT
    db.close(); // NOLINT

    // close should clear only the header
    db.check_have_header(false);
    db.check_have_config(true);

    db.clear_config();
    db.check_have_config(false);

    EXPECT_EQ(db.get_opened(), false); // NOLINT
    db.unlock(); // NOLINT
    db.disconnect(); // NOLINT
}

TEST_F(DataBlockWriteTest, test_write_header) // NOLINT
{
    DataBlockWrite db("dada"); // NOLINT
    db.connect(0); // NOLINT
    db.lock(); // NOLINT

    db.write_config(scan_config.raw()); // NOLINT
    db.write_header(full_header.raw()); // NOLINT

    db.unlock(); // NOLINT
    db.disconnect(); // NOLINT
}

TEST_F(DataBlockWriteTest, test_write_data) // NOLINT
{
    DataBlockWrite db("dada"); // NOLINT
    db.connect(0); // NOLINT
    db.lock(); // NOLINT

    static constexpr size_t bytes = 1024; // NOLINT
    std::vector<char> data(bytes); // NOLINT

    EXPECT_THROW(db.write_data(&data[0], bytes), std::runtime_error); // NOLINT
    db.write_config(scan_config.raw()); // NOLINT
    EXPECT_THROW(db.write_data(&data[0], bytes), std::runtime_error); // NOLINT
    db.write_header(full_header.raw()); // NOLINT
    EXPECT_THROW(db.write_data(&data[0], bytes), std::runtime_error); // NOLINT

    db.open(); // NOLINT
    EXPECT_EQ(db.write_data(&data[0], bytes), bytes); // NOLINT
    db.close(); // NOLINT

    db.unlock(); // NOLINT
    db.disconnect(); // NOLINT
}

TEST_F(DataBlockWriteTest, test_write_multiple_scans) // NOLINT
{
    DataBlockWrite db("dada"); // NOLINT
    db.connect(0); // NOLINT
    db.lock(); // NOLINT

    static constexpr size_t bytes = 1024; // NOLINT
    std::vector<char> data(bytes); // NOLINT

    EXPECT_THROW(db.write_data(&data[0], bytes), std::runtime_error); // NOLINT
    db.write_config(scan_config.raw()); // NOLINT

    // perform 3 "scans"
    for (unsigned i=0; i<3; i++)
    {
        EXPECT_THROW(db.write_data(&data[0], bytes), std::runtime_error); // NOLINT
        db.write_header(full_header.raw()); // NOLINT
        EXPECT_THROW(db.write_data(&data[0], bytes), std::runtime_error); // NOLINT

        db.open(); // NOLINT
        EXPECT_EQ(db.write_data(&data[0], bytes), bytes); // NOLINT
        db.close(); // NOLINT
    }

    db.unlock(); // NOLINT
    db.disconnect(); // NOLINT
}

TEST_F(DataBlockWriteTest, test_open_block) // NOLINT
{
    DataBlockWrite db("dada"); // NOLINT
    db.connect(0); // NOLINT
    db.lock(); // NOLINT
    db.write_config(scan_config.raw()); // NOLINT
    db.write_header(full_header.raw()); // NOLINT
    db.open(); // NOLINT

    EXPECT_EQ(db.get_opened_blocks_count(), 0); // NOLINT
    db.open_block(); // NOLINT
    EXPECT_EQ(db.get_opened_blocks_count(), 1); // NOLINT
    db.open_block(); // NOLINT
    EXPECT_EQ(db.get_opened_blocks_count(), 2); // NOLINT
    db.close_block(db.get_data_bufsz()); // NOLINT
    EXPECT_EQ(db.get_opened_blocks_count(), 1); // NOLINT
    db.close_block(db.get_data_bufsz()); // NOLINT
    EXPECT_EQ(db.get_opened_blocks_count(), 0); // NOLINT

    db.close(); // NOLINT
    db.unlock(); // NOLINT
    db.disconnect(); // NOLINT
}

TEST_F(DataBlockWriteTest, test_zero_next_block) // NOLINT
{
    DataBlockWrite db("dada"); // NOLINT
    db.connect(0); // NOLINT
    db.lock(); // NOLINT
    db.write_config(scan_config.raw()); // NOLINT
    db.write_header(full_header.raw()); // NOLINT
    db.open(); // NOLINT

    // require a reader before this test can be run
    db.zero_next_block(); // NOLINT

    db.close(); // NOLINT
    db.unlock(); // NOLINT
    db.disconnect(); // NOLINT
}

} // namespace ska::pst::smrb::test
