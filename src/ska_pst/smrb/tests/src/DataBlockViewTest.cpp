/*
 * Copyright 2022 Square Kilometre Array Observatory
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 * contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#include "ska_pst/smrb/tests/DataBlockViewTest.h"
#include "ska_pst/smrb/DataBlockView.h"
#include "ska_pst/common/testutils/GtestMain.h"
#include "ska_pst/common/utils/Logging.h"

#include <csignal>
#include <iostream>

using ska::pst::common::test::test_data_file;

auto main(int argc, char* argv[]) -> int
{
  return ska::pst::common::test::gtest_main(argc, argv); // NOLINT
}

namespace ska::pst::smrb::test {

DataBlockViewTest::DataBlockViewTest()
  : ::testing::Test()
{
}

void DataBlockViewTest::SetUp()
{
  _db = std::make_shared<DataBlockCreate>("dada"); // NOLINT
  _db->create(hdr_nbufs, hdr_bufsz, data_nbufs, data_bufsz, num_readers, device_id); // NOLINT

  _writer = std::make_shared<DataBlockWrite>("dada"); // NOLINT
  _writer->connect(1); // NOLINT
  _writer->lock(); // NOLINT

  scan_config.load_from_file(test_data_file("scan_config.txt")); // NOLINT
  full_header.load_from_file(test_data_file("full_header.txt")); // NOLINT
}

void DataBlockViewTest::TearDown()
{
  if (_writer)
  {
    if (_writer->get_opened())
    {
      _writer->close(); // NOLINT
    }
    if (_writer->get_locked())
    {
      _writer->unlock(); // NOLINT
    }
    _writer->disconnect(); // NOLINT
  }
  _writer = nullptr; // NOLINT

  if (_db)
  {
    _db->destroy(); // NOLINT
  }
  _db = nullptr; // NOLINT
}

void DataBlockViewTest::write_after_sleep(int delay_ms, size_t nbytes)
{
  static constexpr int microseconds_per_millisecond = 1000; // NOLINT
  int delay_us = delay_ms * microseconds_per_millisecond; // NOLINT
  usleep(delay_us); // NOLINT

  std::vector<char> data(nbytes); // NOLINT
  PSTLOG_DEBUG(this, "write_after_sleep writing {} bytes", nbytes); // NOLINT
  _writer->write_data(&data[0], nbytes); // NOLINT
  PSTLOG_DEBUG(this, "write_after_sleep written {} bytes", nbytes); // NOLINT
}

TEST_F(DataBlockViewTest, test_construct_delete) // NOLINT
{
  DataBlockView db("eeee"); // NOLINT
}

TEST_F(DataBlockViewTest, test_open_block) // NOLINT
{
  DataBlockView db("dada"); // NOLINT

  size_t bytes = data_bufsz * 2; // NOLINT
  std::vector<char> data(bytes); // NOLINT
  _writer->write_config(scan_config.raw()); // NOLINT
  _writer->write_header(full_header.raw()); // NOLINT
  _writer->open(); // NOLINT
  _writer->write_data(&data[0], bytes); // NOLINT

  EXPECT_THROW(db.open_block(), std::runtime_error); // NOLINT
  db.connect(0); // NOLINT
  EXPECT_THROW(db.open_block(), std::runtime_error); // NOLINT
  db.lock(); // NOLINT
  EXPECT_THROW(db.open_block(), std::runtime_error); // NOLINT
  db.read_config(); // NOLINT
  EXPECT_THROW(db.open_block(), std::runtime_error); // NOLINT
  PSTLOG_TRACE(this, "test_open_block db.read_header()"); // NOLINT
  db.read_header(); // NOLINT
  PSTLOG_TRACE(this, "test_open_block db.open_block()"); // NOLINT
  EXPECT_THROW(db.open_block(), std::runtime_error); // NOLINT
  PSTLOG_TRACE(this, "test_open_block db.open()"); // NOLINT
  db.open(); // NOLINT

  PSTLOG_TRACE(this, "test_open_block db.open_block()"); // NOLINT
  auto block = db.open_block(); // NOLINT
  EXPECT_EQ(db.get_curr_buf(), block); // NOLINT

  db.close(); // NOLINT
  db.unlock(); // NOLINT
  db.disconnect(); // NOLINT
}

TEST_F(DataBlockViewTest, test_close_block) // NOLINT
{
  size_t bytes = data_bufsz * 2; // NOLINT
  std::vector<char> data(bytes); // NOLINT
  PSTLOG_TRACE(this, "About to write data"); // NOLINT
  _writer->write_config(scan_config.raw()); // NOLINT
  _writer->write_header(full_header.raw()); // NOLINT
  _writer->open(); // NOLINT
  _writer->write_data(&data[0], bytes); // NOLINT

  DataBlockView db("dada"); // NOLINT

  EXPECT_THROW(db.close_block(0), std::runtime_error); // NOLINT
  db.connect(0); // NOLINT
  EXPECT_THROW(db.close_block(0), std::runtime_error); // NOLINT
  db.lock(); // NOLINT
  EXPECT_THROW(db.close_block(0), std::runtime_error); // NOLINT
  db.read_config(); // NOLINT
  EXPECT_THROW(db.close_block(0), std::runtime_error); // NOLINT
  db.read_header(); // NOLINT
  EXPECT_THROW(db.close_block(0), std::runtime_error); // NOLINT
  db.open(); // NOLINT

  auto block = db.open_block(); // NOLINT
  EXPECT_NE(block, nullptr); // NOLINT
  db.close_block(0); // NOLINT

  db.close(); // NOLINT
  db.unlock(); // NOLINT
  db.disconnect(); // NOLINT
}

TEST_F(DataBlockViewTest, test_lock_unlock) // NOLINT
{
  DataBlockView db("dada"); // NOLINT
  db.connect(0); // NOLINT
  EXPECT_EQ(db.get_locked(), false); // NOLINT
  db.lock(); // NOLINT
  EXPECT_EQ(db.get_locked(), true); // NOLINT
  db.unlock(); // NOLINT
  EXPECT_EQ(db.get_locked(), false); // NOLINT
  db.disconnect(); // NOLINT
}

TEST_F(DataBlockViewTest, test_lock_without_connect) // NOLINT
{
  DataBlockView db("dada"); // NOLINT
  EXPECT_THROW(db.lock(), std::runtime_error); // NOLINT
}

TEST_F(DataBlockViewTest, test_open_close) // NOLINT
{
  PSTLOG_TRACE(this, "About to write data"); // NOLINT
  _writer->write_config(scan_config.raw()); // NOLINT
  _writer->write_header(full_header.raw()); // NOLINT
  _writer->open(); // NOLINT

  DataBlockView db("dada"); // NOLINT
  db.connect(0); // NOLINT
  db.lock(); // NOLINT
  db.read_config(); // NOLINT
  db.read_header(); // NOLINT

  db.check_have_header(true);
  db.check_have_config(true);

  EXPECT_EQ(db.get_opened(), false); // NOLINT
  db.open(); // NOLINT
  EXPECT_EQ(db.get_opened(), true); // NOLINT
  db.close(); // NOLINT
  EXPECT_EQ(db.get_opened(), false); // NOLINT

  // close should clear only the header
  db.check_have_header(false);
  db.check_have_config(true);

  db.clear_config();
  db.check_have_header(false);
  db.check_have_config(false);

  db.unlock(); // NOLINT
  db.disconnect(); // NOLINT
}

TEST_F(DataBlockViewTest, test_read_header_block) // NOLINT
{
  _writer->write_config(scan_config.raw()); // NOLINT
  _writer->write_header(full_header.raw()); // NOLINT
  _writer->open(); // NOLINT

  DataBlockView db("dada"); // NOLINT
  db.connect(0); // NOLINT
  db.lock(); // NOLINT
  db.read_config(); // NOLINT
  db.read_header(); // NOLINT
  EXPECT_EQ(db.get_header(), full_header.raw()); // NOLINT
  db.open(); // NOLINT

  db.close(); // NOLINT
  db.unlock(); // NOLINT
  db.disconnect(); // NOLINT
}

TEST_F(DataBlockViewTest, test_read_data) // NOLINT
{
  _writer->write_config(scan_config.raw()); // NOLINT
  _writer->write_header(full_header.raw()); // NOLINT
  _writer->open(); // NOLINT

  size_t bytes = data_bufsz; // NOLINT
  std::vector<char> data(bytes); // NOLINT
  _writer->write_data(&data[0], bytes); // NOLINT

  uint64_t resolution = scan_config.get<uint64_t>("RESOLUTION"); // NOLINT

  DataBlockView db("dada"); // NOLINT
  db.connect(0); // NOLINT
  db.lock(); // NOLINT
  db.read_config(); // NOLINT
  db.read_header(); // NOLINT
  db.open(); // NOLINT
  PSTLOG_DEBUG(this, "test_read_data seek_to_end({})", resolution); // NOLINT
  db.seek_to_end(resolution); // NOLINT

  static constexpr int delay_ms = 100; // NOLINT
  PSTLOG_DEBUG(this, "test_read_data starting write_after_sleep({}, {})", delay_ms, data_bufsz * 2); // NOLINT
  std::thread delay_thread = std::thread(&DataBlockViewTest::write_after_sleep, this, delay_ms, data_bufsz * 2); // NOLINT

  std::vector<char> output_data(data_bufsz); // NOLINT
  PSTLOG_DEBUG(this, "test_read_data reading {} bytes", data_bufsz); // NOLINT
  db.read_data(&output_data[0], data_bufsz); // NOLINT
  PSTLOG_DEBUG(this, "test_read_data reading complete"); // NOLINT
  delay_thread.join(); // NOLINT
  PSTLOG_DEBUG(this, "test_read_data delay_thread joined"); // NOLINT

  db.close(); // NOLINT
  db.unlock(); // NOLINT
  db.disconnect(); // NOLINT
}

} // namespace ska::pst::smrb::test
