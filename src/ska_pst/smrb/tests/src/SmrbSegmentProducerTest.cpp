/*
 * Copyright 2023 Square Kilometre Array Observatory
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 * contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#include "ska_pst/smrb/tests/SmrbSegmentProducerTest.h"
#include "ska_pst/smrb/SmrbSegmentProducer.h"
#include "ska_pst/common/testutils/GtestMain.h"

#include <iostream>
#include <csignal>

using ska::pst::common::test::test_data_file;

auto main(int argc, char *argv[]) -> int
{
  return ska::pst::common::test::gtest_main(argc, argv);
}

namespace ska::pst::smrb::test
{

  SmrbSegmentProducerTest::SmrbSegmentProducerTest() :
    ::testing::Test(),
    data_helper("dada", 1),   // one reader
    weights_helper("fada", 1) // one reader
  {
    unsigned constexpr weight_block_size = 1024;
    weights_helper.set_data_block_bufsz(weight_block_size);
  }

  void SmrbSegmentProducerTest::SetUp()
  {
    data_helper.setup();
    data_helper.enable_reader();

    weights_helper.setup();
    weights_helper.enable_reader();

    config.load_from_file(test_data_file("scan_config.txt"));
    header.load_from_file(test_data_file("full_header.txt"));

    data_helper.set_config(config);
    data_helper.set_header(header);

    weights_helper.set_config(config);
    weights_helper.set_header(header);
  }

  void SmrbSegmentProducerTest::TearDown()
  {
    data_helper.teardown();
    weights_helper.teardown();
  }

  TEST_F(SmrbSegmentProducerTest, test_construct_delete) // NOLINT
  {
    SmrbSegmentProducer db("eeee", "ffff");
  }

  TEST_F(SmrbSegmentProducerTest, fail_open_before_connect) // NOLINT
  {
    SmrbSegmentProducer db("dada", "fada");
    EXPECT_THROW(db.open(), std::runtime_error);
  }

  TEST_F(SmrbSegmentProducerTest, fail_next_segment_before_open) // NOLINT
  {
    PSTLOG_TRACE(this, "fail_next_segment_before_open helper.start");
    data_helper.start();
    weights_helper.start();

    PSTLOG_TRACE(this, "fail_next_segment_before_open construct SmrbSegmentProducer");
    SmrbSegmentProducer db("dada", "fada");

    PSTLOG_TRACE(this, "fail_next_segment_before_open db.connect");
    db.connect(0);

    PSTLOG_TRACE(this, "fail_next_segment_before_open db.next_segment");
    EXPECT_THROW(db.next_segment(), std::runtime_error);
  }

  TEST_F(SmrbSegmentProducerTest, test_get_data_header) // NOLINT
  {
    data_helper.start();
    weights_helper.start();

    SmrbSegmentProducer db("dada", "fada");
    db.connect(0);
    db.open();

    EXPECT_EQ(db.get_data_header(), header);

    db.close();
    db.disconnect();
  }

  TEST_F(SmrbSegmentProducerTest, test_next_segment) // NOLINT
  {
    SmrbSegmentProducer db("dada", "fada");

    data_helper.start();
    data_helper.write(1); // write one buffer and clear it

    weights_helper.start();
    weights_helper.write(1); // write one buffer and clear it

    auto data_bufsz = data_helper.get_data_block_bufsz();
    auto weights_bufsz = weights_helper.get_data_block_bufsz();

    PSTLOG_TRACE(this, "test_next_segment db.connect()");
    db.connect(0);

    PSTLOG_TRACE(this, "test_next_segment db.open()");
    db.open();

    PSTLOG_TRACE(this, "test_next_segment db.next_segment()");
    auto segment = db.next_segment();
    EXPECT_NE(segment.data.block, nullptr);

    static constexpr size_t expected_buf = 1;
    EXPECT_EQ(segment.data.obs_offset, expected_buf * data_bufsz);
    EXPECT_EQ(segment.weights.obs_offset, expected_buf * weights_bufsz);
    EXPECT_EQ(segment.get_obs_offset(), segment.data.obs_offset);

    db.close();
    db.disconnect();
  }

  TEST_F(SmrbSegmentProducerTest, test_multiple_blocks) // NOLINT
  {
    data_helper.start();
    weights_helper.start();

    SmrbSegmentProducer db("dada", "fada");
    db.connect(0);
    db.open();

    common::SegmentProducer::Segment previous;

    size_t constexpr test_nloops = 12;
    size_t constexpr bufs_per_loop = 2;

    data_helper.write(0);    // prime the pump
    weights_helper.write(0); // prime the pump

    auto data_bufsz = data_helper.get_data_block_bufsz();
    auto weights_bufsz = weights_helper.get_data_block_bufsz();

    size_t expected_buf = bufs_per_loop;

    for (unsigned i = 0; i < test_nloops; i++)
    {
      PSTLOG_TRACE(this, "test_multiple_next_segment {} of {} iterations", i, test_nloops);

      data_helper.write(bufs_per_loop);
      weights_helper.write(bufs_per_loop);
      auto segment = db.next_segment();

      EXPECT_NE(segment.data.block, nullptr);
      EXPECT_NE(segment.data.block, previous.data.block);

      EXPECT_EQ(segment.data.obs_offset, expected_buf * data_bufsz);
      EXPECT_EQ(segment.weights.obs_offset, expected_buf * weights_bufsz);
      EXPECT_EQ(segment.get_obs_offset(), segment.data.obs_offset);
      expected_buf += bufs_per_loop;

      previous = segment;
    }

    db.close();
    db.disconnect();
  }

  TEST_F(SmrbSegmentProducerTest, test_multiple_blocks_asynchronous) // NOLINT
  {
    data_helper.start();
    weights_helper.start();

    SmrbSegmentProducer db("dada", "fada");
    db.connect(0);
    db.open();

    float constexpr delay_ms = 10;
    size_t constexpr test_nblocks = 64;
    PSTLOG_DEBUG(this, "test_multiple_blocks_asynchronous starting data write_and_close({}, {})", delay_ms, test_nblocks);
    std::thread data_thread = std::thread(&DataBlockTestHelper::write_and_close, &data_helper, delay_ms, test_nblocks);

    PSTLOG_DEBUG(this, "test_multiple_blocks_asynchronous starting weights write_and_close({}, {})", delay_ms, test_nblocks);
    std::thread weights_thread = std::thread(&DataBlockTestHelper::write_and_close, &weights_helper, delay_ms, test_nblocks);

    bool eod = false;
    while (!eod)
    {
      PSTLOG_DEBUG(this, "test_multiple_blocks_asynchronous next_segment");
      auto segment = db.next_segment();

      if (segment.data.block == nullptr)
      {
        PSTLOG_DEBUG(this, "test_multiple_blocks_asynchronous end-of-data detected on data block");
        break;
      }

      if (segment.weights.block == nullptr)
      {
        PSTLOG_DEBUG(this, "test_multiple_blocks_asynchronous end-of-data detected on weights block");
        break;
      }

      auto data_count_ptr = reinterpret_cast<uint64_t *>(segment.data.block);
      PSTLOG_DEBUG(this, "test_multiple_blocks_asynchronous data_count={}", *data_count_ptr);
      auto weights_count_ptr = reinterpret_cast<uint64_t *>(segment.weights.block);
      PSTLOG_DEBUG(this, "test_multiple_blocks_asynchronous weights_count={}", *weights_count_ptr);

      EXPECT_EQ(*data_count_ptr, *weights_count_ptr);
    }

    PSTLOG_DEBUG(this, "test_multiple_blocks_asynchronous reading complete");
    data_thread.join();
    PSTLOG_DEBUG(this, "test_multiple_blocks_asynchronous data_thread joined");
    weights_thread.join();
    PSTLOG_DEBUG(this, "test_read weights_thread joined");

    db.close();
    db.disconnect();
  }

} // namespace ska::pst::smrb::test
