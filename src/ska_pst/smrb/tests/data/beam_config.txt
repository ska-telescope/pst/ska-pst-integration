DATA_KEY        a000
WEIGHTS_KEY     a010
NUMA_NODE       0

# common header ring buffer defintion
HB_NBUFS        8
HB_BUFSZ        4096

# data ring buffer defintion
DB_NBUFS        8
DB_BUFSZ        1048576

# weights ring buffer defintion
WB_NBUFS        8
WB_BUFSZ        8192