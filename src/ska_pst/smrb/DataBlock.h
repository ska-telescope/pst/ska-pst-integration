/*
 * Copyright 2022 Square Kilometre Array Observatory
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 * contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#include <cstddef>
#include <string>
#include <map>

// from PSRDADA
#include <ipcio.h>
#include <ipcbuf.h>
#include <dada_def.h>

#ifndef __SKA_PST_SMRB_DataBlock_h
#define __SKA_PST_SMRB_DataBlock_h

namespace ska {
namespace pst {
namespace smrb {

  //! State of DataBlocks that correspond to the PSRDADA ipcbuf state
  enum State {

    //! disconnected
    DISCONNECTED = 0,

    //! connected
    VIEWER = 1,

    //! one process that writes to the buffer
    WRITER = 2,

    //! start-of-data flag has been raised
    WRITING = 3,

    //! next operation will change writing state
    WCHANGE = 4,

    //! one process that reads from the buffer
    READER = 5,

    //! start-of-data flag has been raised
    READING = 6,

    //! end-of-data flag has been raised
    RSTOP = 7,

    //! currently viewing
    VIEWING = 8,

    //! end-of-data flag has been raised
    VSTOP = 9
  };

  /**
   * @brief Abstract base class that provides an Object Oriented interface
   * to a PSRDADA Shared Memory Ring Buffer (SMRB).
   *
   */
  class DataBlock
  {
    public:

      /**
       * @brief Construct a new DataBlock with specified key
       *
       * @param key_string 4 character hexidecimal PSRDADA key
       */
      explicit DataBlock(std::string _key_string);

      /**
       * @brief Destroy the Data Block object
       *
       */
      virtual ~DataBlock() = default;

      /**
       * @brief Attempt to connect to the existing SMRB within the specified timeout
       *
       * @param timeout timeout
       */
      virtual void connect(int timeout);

      /**
       * @brief Disconnect from the SMRB
       *
       */
      virtual void disconnect();

      /**
       * @brief page the buffers from the data block into RAM by writing zero to each one
       *
       */
      void page();

      /**
       * @brief Return the size of DataBlock elements
       *
       * @return const uint64_t size of data block element in bytes
       */
      uint64_t get_data_bufsz() const { return data_bufsz; };

      /**
       * @brief Return the number of DataBlock elements
       *
       * @return const uint64_t number of DataBlock elements
       */
      uint64_t get_data_nbufs() const { return data_nbufs; };

      /**
       * @brief Return the size of HeaderBlock elements
       *
       * @return const uint64_t size of HeaderBlock elements in bytes
       */
      uint64_t get_header_bufsz() const { return header_bufsz; };

      /**
       * @brief Return the number of HeaderBlock elements
       *
       * @return const uint64_t number of HeaderBlock elements
       */
      uint64_t get_header_nbufs() const { return header_nbufs; };

      /**
       * @brief Return the CUDA device ID for the data block.
       * If the DataBlock buffers exist in CUDA memory, the device will be >=0
       * @return int CUDA device on which the DataBlock buffers reside, -1 if in CPU memory
       */
      int get_device() { return device_id; };

      /**
       * @brief Return the number of readers required by the DataBlock
       *
       * @return int number of readers
       */
      int get_nreaders();

      /**
       * @brief Lock the DataBlock buffer elements into memory
       *
       */
      void lock_memory();

      /**
       * @brief Unlock the DataBlock buffer elements into memory
       *
       */
      void unlock_memory();

      /**
       * @brief Register the DataBlock buffers with the CUDA driver.
       * Registration of the DataBlock buffers enable more efficient RDMA transfers between CPU and GPU RAM
       *
       */
      void register_cuda();

      /**
       * @brief Deregister the DataBlock buffers with the CUDA driver.
       *
       */
      void unregister_cuda();

      /**
       * @brief Parse the provided string as a PSRDADA data block key.
       * If valid assign the data_block_key and header_block_key, else throw runtime_exception
       *
       * @param key string representation of PSRDADA key
       * @return key_t encoded key_t
       */
      static key_t parse_psrdada_key(const std::string &key);

      /**
       * @brief Check if data block is connected.
       *
       * @return true data block is connected
       * @return false data block is not connected
       */
      bool is_connected() const { return connected; };

      /**
       * @brief Get the number of buffers written to the header block
       *
       * @return uint64_t number of header buffers written
       */
      uint64_t get_header_bufs_written();

      /**
       * @brief Get the number of buffers written to the data block
       *
       * @return uint64_t number of data buffers written
       */
      uint64_t get_data_bufs_written();

      /**
       * @brief Get the number of buffers read from the header block
       *
       * @return uint64_t number of header buffers read
       */
      uint64_t get_header_bufs_read();

      /**
       * @brief Get the total number of buffers read in the data block
       *
       * @return uint64_t number of data buffers read
       */
      uint64_t get_data_bufs_read();

      /**
       * @brief Get the number of clear header buffers
       *
       * @return uint64_t number of clear header buffers
       */
      uint64_t get_header_bufs_clear();

      /**
       * @brief Get the number of clear data buffers
       *
       * @return uint64_t number of clear data buffers
       */
      uint64_t get_data_bufs_clear();

      /**
       * @brief Get the number of full header buffers
       *
       * @return uint64_t number of full header buffers
       */
      uint64_t get_header_bufs_full();

      /**
       * @brief Get the number of full data buffers
       *
       * @return uint64_t number of full data buffers
       */
      uint64_t get_data_bufs_full();

      /**
       * @brief Get the number of empty header buffers
       *
       * @return uint64_t number of empty header buffers
       */
      uint64_t get_header_bufs_available();

      /**
       * @brief Get the number of empty data buffers
       *
       * @return uint64_t number of empty data buffers
       */
      uint64_t get_data_bufs_available();

      /**
       * @brief Return the current observation number of the writer
       *
       * @return uint64_t current observation of the writer to the DataBlock
       */
      uint64_t get_data_write_xfer();

      /**
       * @brief Return the current observation number of reader
       *
       * @return uint64_t current observation number of the reader
       */
      uint64_t get_data_read_xfer();

      /**
       * @brief Get the current state of the writer
       *
       * @return State State of the writer
       */
      State get_writer_state();

      /**
       * @brief Return the current state of the reader
       *
       * @return State State of the reader
       */
      State get_reader_state();

      /**
       * @brief Get the DADA key for the data block.
       *
       * @return the DADA key for the data block.
       */
      const std::string get_data_block_key_str() const;

      /**
       * @brief Get the DADA key for the header block.
       *
       * @return the DADA key for the header block.
       */
      const std::string get_header_block_key_str() const;

      //! return mapped diagnostic context key/value pairs
      const std::map<std::string, std::string>& get_log_context() const
      {
        return log_context;
      }

    protected:

      //! helper method that throws runtime_error if connected does not match expected value
      void check_connected(bool expected);

      //! pointer to PSRDADA header block
      ipcbuf_t header_block;

      //! pointer to PSRDADA data block
      ipcio_t data_block;

      //! Size of a header_block buffer in bytes
      uint64_t header_bufsz{0};

      //! Number of header_block buffers
      uint64_t header_nbufs{0};

      //! Size of a data_block buffer in bytes
      uint64_t data_bufsz{0};

      //! Number of data_block buffers
      uint64_t data_nbufs{0};

      //! CUDA device index on which the data_block buffers are allocated, -1 if on host
      int device_id{-1};

      //! Number of readers for the data block
      int nreaders{0};

      //! True if connected to the PSRDADA SMRB
      bool connected{false};

      //! hexidecimal PSRDADA key that identifies the IPC shared memory segment for the data_block
      key_t data_block_key;

      //! hexidecimal PSRDADA key that identifies the IPC shared memory segment for the header_block
      key_t header_block_key;

      //! Populate connected data block properties
      void on_connect();

      //! Reset data block properties to their default values
      void on_disconnect();

      //! flag to track whether the data block memory has been locked
      bool locked_memory{false};

      //! flag to track whether the data block memory must be locked
      bool require_locked_memory{false};

      //! data block key string
      const std::string key_string;

      //! mapped diagnostic context key/value pairs
      std::map<std::string, std::string> log_context = {{"key", key_string}};
  };

} // smrb
} // pst
} // ska

#endif // __SKA_PST_SMRB_DataBlock_h
