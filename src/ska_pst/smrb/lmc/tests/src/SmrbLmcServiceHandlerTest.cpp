/*
 * Copyright 2022 Square Kilometre Array Observatory
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 * contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#include <iostream>
#include <grpc/grpc.h>
#include <grpc++/grpc++.h>

#include "ska_pst/common/lmc/LmcServiceException.h"
#include "ska_pst/common/statemodel/StateModel.h"

#include "ska_pst/smrb/lmc/tests/SmrbLmcServiceHandlerTest.h"
#include "ska_pst/common/utils/ValidationRegex.h"
#include "ska_pst/common/testutils/GtestMain.h"
#include "ska_pst/common/utils/Logging.h"

auto main(int argc, char* argv[]) -> int
{
  return ska::pst::common::test::gtest_main(argc, argv);
}

namespace ska::pst::smrb::test {

SmrbLmcServiceHandlerTest::SmrbLmcServiceHandlerTest()
    : ::testing::Test()
{
}

void SmrbLmcServiceHandlerTest::SetUp()
{
    PSTLOG_TRACE(this, "creating shared data block manager");
    _smrb = std::make_shared<ska::pst::smrb::DataBlockManager>();

    PSTLOG_TRACE(this, "creating handler");
    handler = std::make_shared<ska::pst::smrb::SmrbLmcServiceHandler>(_smrb);

    _data_writer = std::make_shared<ska::pst::smrb::DataBlockWrite>("a000");
    _data_reader = std::make_shared<ska::pst::smrb::DataBlockRead>("a000");
    _weights_writer = std::make_shared<ska::pst::smrb::DataBlockWrite>("a010");
    _weights_reader = std::make_shared<ska::pst::smrb::DataBlockRead>("a010");
}

void SmrbLmcServiceHandlerTest::TearDown()
{
    if (_data_writer)
    {
        if (_data_writer->get_opened())
        {
            _data_writer->close();
        }
        if (_data_writer->get_locked())
        {
            _data_writer->unlock();
        }
        if (_data_writer->is_connected())
        {
            _data_writer->disconnect();
        }
    }
    _data_writer = nullptr;

    if (_weights_writer)
    {
        if (_weights_writer->get_opened())
        {
            _weights_writer->close();
        }
        if (_weights_writer->get_locked())
        {
            _weights_writer->unlock();
        }
        if (_weights_writer->is_connected())
        {
            _weights_writer->disconnect();
        }
    }
    _weights_writer = nullptr;

    if (_data_reader)
    {
        if (_data_reader->get_opened())
        {
            _data_reader->close();
        }
        if (_data_reader->get_locked())
        {
            _data_reader->unlock();
        }
        if (_data_reader->is_connected())
        {
            _data_reader->disconnect();
        }
    }
    _data_reader = nullptr;

    if (_weights_reader)
    {
        if (_weights_reader->get_opened())
        {
            _weights_reader->close();
        }
        if (_weights_reader->get_locked())
        {
            _weights_reader->unlock();
        }
        if (_weights_reader->is_connected())
        {
            _weights_reader->disconnect();
        }
    }
    _weights_reader = nullptr;
    _smrb->quit();
    _smrb = nullptr;
}

auto SmrbLmcServiceHandlerTest::default_test_beam_configuration() -> ska::pst::lmc::BeamConfiguration
{
    ska::pst::lmc::BeamConfiguration request;
    auto smrb_resources_request = request.mutable_smrb();
    smrb_resources_request->set_data_key(data_key);
    smrb_resources_request->set_weights_key(weights_key);
    smrb_resources_request->set_hb_nbufs(hb_nbufs);
    smrb_resources_request->set_hb_bufsz(hb_bufsz);
    smrb_resources_request->set_db_nbufs(db_nbufs);
    smrb_resources_request->set_db_bufsz(db_bufsz);
    smrb_resources_request->set_wb_nbufs(wb_nbufs);
    smrb_resources_request->set_wb_bufsz(wb_bufsz);

    return request;
}

void SmrbLmcServiceHandlerTest::validate_beam_configuration()
{
    auto config = default_test_beam_configuration();
    validate_beam_configuration(config);
}

void SmrbLmcServiceHandlerTest::validate_beam_configuration(const ska::pst::lmc::BeamConfiguration& config)
{
    PSTLOG_INFO(this, "config = {}", config.DebugString());
    handler->validate_beam_configuration(config);
}

void SmrbLmcServiceHandlerTest::configure_beam()
{
    configure_beam(default_test_beam_configuration());
}

void SmrbLmcServiceHandlerTest::configure_beam(const ska::pst::lmc::BeamConfiguration& request)
{
    handler->configure_beam(request);
}

void SmrbLmcServiceHandlerTest::validate_scan_configuration()
{
    auto config = ska::pst::lmc::ScanConfiguration();
    validate_scan_configuration(config);
}

void SmrbLmcServiceHandlerTest::validate_scan_configuration(const ska::pst::lmc::ScanConfiguration& config)
{
    handler->validate_scan_configuration(config);
}

void SmrbLmcServiceHandlerTest::configure_scan()
{
    ska::pst::lmc::ScanConfiguration request;
    request.mutable_smrb();
    handler->configure_scan(request);
}

void SmrbLmcServiceHandlerTest::start_scan()
{
    handler->start_scan(ska::pst::lmc::StartScanRequest());
}

TEST_F(SmrbLmcServiceHandlerTest, configure_deconfigure_beam) // NOLINT
{
    PSTLOG_TRACE(this, "configure_deconfigure_beam - configuring beam");

    EXPECT_FALSE(handler->is_beam_configured()); // NOLINT
    ASSERT_EQ(ska::pst::common::State::Idle, _smrb->get_state());

    configure_beam();

    EXPECT_TRUE(handler->is_beam_configured()); // NOLINT
    EXPECT_TRUE(_smrb->is_beam_configured()); // NOLINT
    ASSERT_EQ(ska::pst::common::State::BeamConfigured, _smrb->get_state());
    PSTLOG_TRACE(this, "configure_deconfigure_beam - beam configured");

    PSTLOG_TRACE(this, "configure_deconfigure_beam - getting beam configuration");
    ska::pst::lmc::BeamConfiguration resources_configuration;
    handler->get_beam_configuration(&resources_configuration);

    EXPECT_TRUE(resources_configuration.has_smrb()); // NOLINT
    auto get_response_smrb = resources_configuration.smrb();

    EXPECT_EQ(data_key, get_response_smrb.data_key()); // NOLINT
    EXPECT_EQ(weights_key, get_response_smrb.weights_key()); // NOLINT
    EXPECT_EQ(hb_nbufs, get_response_smrb.hb_nbufs()); // NOLINT
    EXPECT_EQ(hb_bufsz, get_response_smrb.hb_bufsz()); // NOLINT
    EXPECT_EQ(db_nbufs, get_response_smrb.db_nbufs()); // NOLINT
    EXPECT_EQ(db_bufsz, get_response_smrb.db_bufsz()); // NOLINT
    EXPECT_EQ(wb_nbufs, get_response_smrb.wb_nbufs()); // NOLINT
    EXPECT_EQ(wb_bufsz, get_response_smrb.wb_bufsz()); // NOLINT

    EXPECT_EQ(data_key, _smrb->get_beam_configuration_header_key("DATA_KEY")); // NOLINT
    EXPECT_EQ(weights_key, _smrb->get_beam_configuration_header_key("WEIGHTS_KEY")); // NOLINT
    EXPECT_EQ(hb_nbufs, _smrb->get_beam_configuration_buffer_key("HB_NBUFS")); // NOLINT
    EXPECT_EQ(hb_bufsz, _smrb->get_beam_configuration_buffer_key("HB_BUFSZ")); // NOLINT
    EXPECT_EQ(db_nbufs, _smrb->get_beam_configuration_buffer_key("DB_NBUFS")); // NOLINT
    EXPECT_EQ(db_bufsz, _smrb->get_beam_configuration_buffer_key("DB_BUFSZ")); // NOLINT
    EXPECT_EQ(wb_nbufs, _smrb->get_beam_configuration_buffer_key("WB_NBUFS")); // NOLINT
    EXPECT_EQ(wb_bufsz, _smrb->get_beam_configuration_buffer_key("WB_BUFSZ")); // NOLINT

    PSTLOG_TRACE(this, "configure_deconfigure_beam - deconfiguring beam");
    handler->deconfigure_beam();
    EXPECT_FALSE(handler->is_beam_configured()); // NOLINT
    EXPECT_FALSE(_smrb->is_beam_configured()); // NOLINT
    ASSERT_EQ(ska::pst::common::State::Idle, _smrb->get_state());
    PSTLOG_TRACE(this, "configure_deconfigure_beam - beam deconfigured");
}

TEST_F(SmrbLmcServiceHandlerTest, configure_beam_again_should_throw_exception) // NOLINT
{
    PSTLOG_TRACE(this, "configure_beam_again_should_throw_exception - configuring beam");

    EXPECT_FALSE(handler->is_beam_configured()); // NOLINT

    configure_beam();

    EXPECT_TRUE(handler->is_beam_configured()); // NOLINT
    EXPECT_TRUE(_smrb->is_beam_configured()); // NOLINT
    PSTLOG_TRACE(this, "configure_beam_again_should_throw_exception - beam configured");

    try {
        configure_beam();
        FAIL() << " expected configure_beam to throw exception due to beam already configured.\n";
    } catch (ska::pst::common::LmcServiceException& ex) {
        EXPECT_EQ(std::string(ex.what()), "SMRB already configured for beam.");
        EXPECT_EQ(ex.error_code(), ska::pst::lmc::ErrorCode::CONFIGURED_FOR_BEAM_ALREADY);
        EXPECT_EQ(ex.status_code(), grpc::StatusCode::FAILED_PRECONDITION);
    }
}

TEST_F(SmrbLmcServiceHandlerTest, configure_beam_should_have_smrb_object) // NOLINT
{
    PSTLOG_TRACE(this, "configure_beam_should_have_smrb_object - configuring beam");

    EXPECT_FALSE(handler->is_beam_configured()); // NOLINT

    ska::pst::lmc::BeamConfiguration beam_configuration;
    beam_configuration.mutable_test();

    try {
        handler->configure_beam(beam_configuration);
        FAIL() << " expected configure_beam to throw exception due not having SMRB field.\n";
    } catch (ska::pst::common::LmcServiceException& ex) {
        EXPECT_EQ(std::string(ex.what()), "Expected an SMRB resources object, but none were provided.");
        EXPECT_EQ(ex.error_code(), ska::pst::lmc::ErrorCode::INVALID_REQUEST);
        EXPECT_EQ(ex.status_code(), grpc::StatusCode::INVALID_ARGUMENT);
    }
}

TEST_F(SmrbLmcServiceHandlerTest, get_beam_configuration_when_no_beam_configured) // NOLINT
{
    PSTLOG_TRACE(this, "get_beam_configuration_when_no_beam_configured - getting beam configuration");
    EXPECT_FALSE(handler->is_beam_configured());

    ska::pst::lmc::BeamConfiguration response;
    try {
        handler->get_beam_configuration(&response);
    } catch (ska::pst::common::LmcServiceException& ex) {
        EXPECT_EQ(std::string(ex.what()), "SMRB not configured for beam.");
        EXPECT_EQ(ex.error_code(), ska::pst::lmc::ErrorCode::NOT_CONFIGURED_FOR_BEAM);
        EXPECT_EQ(ex.status_code(), grpc::StatusCode::FAILED_PRECONDITION);
    }
    PSTLOG_TRACE(this, "get_beam_configuration_when_no_beam_configured - no beam configured");
}

TEST_F(SmrbLmcServiceHandlerTest, deconfigure_beam_when_no_beam_configured) // NOLINT
{
    PSTLOG_TRACE(this, "deconfigure_beam_when_no_beam_configured - deconfiguring beam");

    EXPECT_FALSE(handler->is_beam_configured()); // NOLINT

    try {
        handler->deconfigure_beam();
        FAIL() << " expected deconfigure_beam to throw exception due to not configure for beam.\n";
    } catch (ska::pst::common::LmcServiceException& ex) {
        EXPECT_EQ(std::string(ex.what()), "SMRB not configured for beam.");
        EXPECT_EQ(ex.error_code(), ska::pst::lmc::ErrorCode::NOT_CONFIGURED_FOR_BEAM);
        EXPECT_EQ(ex.status_code(), grpc::StatusCode::FAILED_PRECONDITION);
    }
}

TEST_F(SmrbLmcServiceHandlerTest, configure_deconfigure_scan) // NOLINT
{

    PSTLOG_TRACE(this, "configure_deconfigure_scan - configuring beam");
    configure_beam();
    PSTLOG_TRACE(this, "configure_deconfigure_scan - beam configured");

    PSTLOG_TRACE(this, "configure_deconfigure_scan - configuring scan");
    EXPECT_FALSE(handler->is_scan_configured());
    configure_scan();
    EXPECT_TRUE(handler->is_scan_configured());
    PSTLOG_TRACE(this, "configure_deconfigure_scan - scan configured");

    PSTLOG_TRACE(this, "configure_deconfigure_scan - getting configuration");
    ska::pst::lmc::ScanConfiguration get_response;
    handler->get_scan_configuration(&get_response);
    EXPECT_TRUE(get_response.has_smrb()); // NOLINT
    PSTLOG_TRACE(this, "configure_deconfigure_scan - checked configuration");

    PSTLOG_TRACE(this, "configure_deconfigure_scan - deconfiguring scan");
    handler->deconfigure_scan();
    EXPECT_FALSE(handler->is_scan_configured());
    PSTLOG_TRACE(this, "configure_deconfigure_scan - scan deconfigured");

    PSTLOG_TRACE(this, "configure_deconfigure_scan - deconfiguring beam");
    handler->deconfigure_beam();
    PSTLOG_TRACE(this, "configure_deconfigure_scan - beam deconfigured");
}

TEST_F(SmrbLmcServiceHandlerTest, configure_scan_when_not_beam_configured) // NOLINT
{
    PSTLOG_TRACE(this, "configure_scan_when_not_beam_configured - configuring scan");

    try {
        configure_scan();
        FAIL() << " expected configure to throw exception due to not having beam configured.\n";
    } catch (ska::pst::common::LmcServiceException& ex) {
        EXPECT_EQ(std::string(ex.what()), "SMRB not configured for beam.");
        EXPECT_EQ(ex.error_code(), ska::pst::lmc::ErrorCode::NOT_CONFIGURED_FOR_BEAM);
        EXPECT_EQ(ex.status_code(), grpc::StatusCode::FAILED_PRECONDITION);
    }
}

TEST_F(SmrbLmcServiceHandlerTest, configure_start_scan_again_should_throw_exception) // NOLINT
{
    PSTLOG_TRACE(this, "configure_start_scan_again_should_throw_exception - configuring beam");

    EXPECT_FALSE(handler->is_beam_configured()); // NOLINT

    configure_beam();

    EXPECT_TRUE(handler->is_beam_configured()); // NOLINT
    EXPECT_TRUE(_smrb->is_beam_configured()); // NOLINT
    PSTLOG_TRACE(this, "configure_start_scan_again_should_throw_exception - beam configured");

    PSTLOG_TRACE(this, "configure_start_scan_again_should_throw_exception - configuring scan");
    EXPECT_FALSE(handler->is_scan_configured());
    configure_scan();
    EXPECT_TRUE(handler->is_scan_configured());
    PSTLOG_TRACE(this, "configure_start_scan_again_should_throw_exception - scan configured");

    try {
        configure_scan();
        FAIL() << " expected configure to throw exception due to already configured.\n";
    } catch (ska::pst::common::LmcServiceException& ex) {
        EXPECT_EQ(std::string(ex.what()), "Scan already configured for SMRB.");
        EXPECT_EQ(ex.error_code(), ska::pst::lmc::ErrorCode::CONFIGURED_FOR_SCAN_ALREADY);
        EXPECT_EQ(ex.status_code(), grpc::StatusCode::FAILED_PRECONDITION);
    }
}

TEST_F(SmrbLmcServiceHandlerTest, configure_scan_should_have_smrb_object) // NOLINT
{
    PSTLOG_TRACE(this, "configure_scan_should_have_smrb_object - configuring beam");

    EXPECT_FALSE(handler->is_beam_configured()); // NOLINT

    PSTLOG_TRACE(this, "configure_scan_should_have_smrb_object - configuring beam");
    configure_beam();
    PSTLOG_TRACE(this, "configure_scan_should_have_smrb_object - beam configured");

    PSTLOG_TRACE(this, "configure_scan_should_have_smrb_object - configuring scan");
    try {
        ska::pst::lmc::ScanConfiguration configuration;
        configuration.mutable_test();
        handler->configure_scan(configuration);
        FAIL() << " expected configure to throw exception due not having SMRB field.\n";
    } catch (ska::pst::common::LmcServiceException& ex) {
        EXPECT_EQ(std::string(ex.what()), "Expected an SMRB resources object, but none were provided.");
        EXPECT_EQ(ex.error_code(), ska::pst::lmc::ErrorCode::INVALID_REQUEST);
        EXPECT_EQ(ex.status_code(), grpc::StatusCode::INVALID_ARGUMENT);
    }
}

TEST_F(SmrbLmcServiceHandlerTest, deconfigure_scan_when_not_scan_configured) // NOLINT
{
    PSTLOG_TRACE(this, "deconfigure_scan_when_not_scan_configured - deconfiguring scan");

    try {
        handler->deconfigure_scan();
        FAIL() << " expected deconfigure_beam to throw exception due to no beam configured.\n";
    } catch (ska::pst::common::LmcServiceException& ex) {
        EXPECT_EQ(std::string(ex.what()), "Not currently configured for SMRB.");
        EXPECT_EQ(ex.error_code(), ska::pst::lmc::ErrorCode::NOT_CONFIGURED_FOR_SCAN);
        EXPECT_EQ(ex.status_code(), grpc::StatusCode::FAILED_PRECONDITION);
    }
}

TEST_F(SmrbLmcServiceHandlerTest, get_scan_configuration_when_not_scan_configured) // NOLINT
{
    PSTLOG_TRACE(this, "get_scan_configuration_when_not_scan_configured - getting scan configuration"); // NOLINT

    try {
        ska::pst::lmc::ScanConfiguration scan_configuration;
        handler->get_scan_configuration(&scan_configuration);
        FAIL() << " expected deconfigure_beam to throw exception due to no beam configured.\n";
    } catch (ska::pst::common::LmcServiceException& ex) {
        EXPECT_EQ(std::string(ex.what()), "Not currently configured for SMRB.");
        EXPECT_EQ(ex.error_code(), ska::pst::lmc::ErrorCode::NOT_CONFIGURED_FOR_SCAN);
        EXPECT_EQ(ex.status_code(), grpc::StatusCode::FAILED_PRECONDITION);
    }
}

TEST_F(SmrbLmcServiceHandlerTest, start_stop_scan) // NOLINT
{
    PSTLOG_TRACE(this, "start_stop_scan - configuring beam");
    configure_beam();
    PSTLOG_TRACE(this, "start_stop_scan - beam configured");

    PSTLOG_TRACE(this, "start_stop_scan - configuring scan");
    configure_scan();
    PSTLOG_TRACE(this, "start_stop_scan - scan configured");

    PSTLOG_TRACE(this, "start_stop_scan - starting scan");
    EXPECT_FALSE(handler->is_scanning());
    start_scan();
    EXPECT_TRUE(handler->is_scanning());
    PSTLOG_TRACE(this, "start_stop_scan - scanning");

    PSTLOG_TRACE(this, "start_stop_scan - ending scan");
    handler->stop_scan();
    EXPECT_FALSE(handler->is_scanning());
    PSTLOG_TRACE(this, "start_stop_scan - scan ended");

    PSTLOG_TRACE(this, "start_stop_scan - deconfiguring scan");
    handler->deconfigure_scan();
    PSTLOG_TRACE(this, "start_stop_scan - scan deconfigured");

    PSTLOG_TRACE(this, "start_stop_scan - deconfiguring beam");
    handler->deconfigure_beam();
    PSTLOG_TRACE(this, "start_stop_scan - beam deconfigured");
}

TEST_F(SmrbLmcServiceHandlerTest, start_scan_when_not_configured_should_throw_exception) // NOLINT
{
    PSTLOG_TRACE(this, "start_scan_when_not_configured_should_throw_exception - configuring beam");

    EXPECT_FALSE(handler->is_beam_configured()); // NOLINT
    configure_beam();
    PSTLOG_TRACE(this, "start_scan_when_not_configured_should_throw_exception - beam configured");

    PSTLOG_TRACE(this, "start_scan_when_not_configured_should_throw_exception - start scan");
    EXPECT_FALSE(handler->is_scan_configured());

    try {
        start_scan();
        FAIL() << " expected start_scan to throw exception due to not being configured for scan.\n";
    } catch (ska::pst::common::LmcServiceException& ex) {
        EXPECT_EQ(std::string(ex.what()), "Not currently configured for SMRB.");
        EXPECT_EQ(ex.error_code(), ska::pst::lmc::ErrorCode::NOT_CONFIGURED_FOR_SCAN);
        EXPECT_EQ(ex.status_code(), grpc::StatusCode::FAILED_PRECONDITION);
    }
}

TEST_F(SmrbLmcServiceHandlerTest, start_scan_again_should_throw_exception) // NOLINT
{
    PSTLOG_TRACE(this, "start_scan_again_should_throw_exception - configuring beam");

    EXPECT_FALSE(handler->is_beam_configured()); // NOLINT
    configure_beam();
    PSTLOG_TRACE(this, "start_scan_again_should_throw_exception - beam configured");

    PSTLOG_TRACE(this, "start_scan_again_should_throw_exception - configuring scan");
    configure_scan();
    PSTLOG_TRACE(this, "start_scan_again_should_throw_exception - scan configured");

    PSTLOG_TRACE(this, "start_scan_again_should_throw_exception - start scan");
    EXPECT_FALSE(handler->is_scanning());
    start_scan();
    EXPECT_TRUE(handler->is_scanning());
    PSTLOG_TRACE(this, "start_scan_again_should_throw_exception - scanning");

    try {
        start_scan();
        FAIL() << " expected start_scan to throw exception due to already scanning.\n";
    } catch (ska::pst::common::LmcServiceException& ex) {
        EXPECT_EQ(std::string(ex.what()), "SMRB is already scanning.");
        EXPECT_EQ(ex.error_code(), ska::pst::lmc::ErrorCode::ALREADY_SCANNING);
        EXPECT_EQ(ex.status_code(), grpc::StatusCode::FAILED_PRECONDITION);
    }
}

TEST_F(SmrbLmcServiceHandlerTest, stop_scan_when_not_scanning_should_throw_exception) // NOLINT
{
    PSTLOG_TRACE(this, "stop_scan_when_not_scanning_should_throw_exception - end scan");
    EXPECT_FALSE(handler->is_scanning());

    try {
        handler->stop_scan();
        FAIL() << " expected stop_scan to throw exception due to not currently scanning.\n";
    } catch (ska::pst::common::LmcServiceException& ex) {
        EXPECT_EQ(std::string(ex.what()), "Received stop_scan request when SMRB is not scanning.");
        EXPECT_EQ(ex.error_code(), ska::pst::lmc::ErrorCode::NOT_SCANNING);
        EXPECT_EQ(ex.status_code(), grpc::StatusCode::FAILED_PRECONDITION);
    }
}

// test getting monitoring data
TEST_F(SmrbLmcServiceHandlerTest, get_monitor_data) // NOLINT
{
    // configure beam
    PSTLOG_TRACE(this, "get_monitor_data - configuring beam");
    ska::pst::lmc::BeamConfiguration configure_beam_request;
    auto smrb_resources_request = configure_beam_request.mutable_smrb();

    static constexpr uint64_t hb_nbufs = 8;
    static constexpr uint64_t hb_bufsz = 4096;
    static constexpr uint64_t db_nbufs = 8;
    static constexpr uint64_t db_bufsz = 1048576;
    static constexpr uint64_t wb_nbufs = 8;
    static constexpr uint64_t wb_bufsz = 4096;

    smrb_resources_request->set_data_key("a000");
    smrb_resources_request->set_weights_key("a010");
    smrb_resources_request->set_hb_nbufs(hb_nbufs);
    smrb_resources_request->set_hb_bufsz(hb_bufsz);
    smrb_resources_request->set_db_nbufs(db_nbufs);
    smrb_resources_request->set_db_bufsz(db_bufsz);
    smrb_resources_request->set_wb_nbufs(wb_nbufs);
    smrb_resources_request->set_wb_bufsz(wb_bufsz);

    handler->configure_beam(configure_beam_request);
    PSTLOG_TRACE(this, "get_monitor_data - beam configured");

    // configure
    PSTLOG_TRACE(this, "get_monitor_data - configuring scan");
    configure_scan();
    PSTLOG_TRACE(this, "get_monitor_data - scan configured");

    PSTLOG_TRACE(this, "get_monitor_data - setting up reader/writers");
    _data_writer->connect(1);
    _data_reader->connect(1);
    _weights_writer->connect(1);
    _weights_reader->connect(1);

    // scan
    PSTLOG_TRACE(this, "get_monitor_data - starting scan");
    start_scan();
    PSTLOG_TRACE(this, "get_monitor_data - scanning");

    // want to write 1 more byte than a buffer size
    size_t data_size_bytes = db_bufsz + 1;
    std::vector<char> data(data_size_bytes);
    _data_writer->lock();
    _data_writer->write_config("");
    _data_writer->write_header("");
    _data_writer->open();
    _data_writer->write_data(&data[0], data_size_bytes);
    _data_writer->close();
    _data_writer->unlock();

    // want to write 1 more byte than a buffer size
    size_t weights_size_bytes = wb_bufsz + 1;
    std::vector<char> weights(weights_size_bytes);
    _weights_writer->lock();
    _weights_writer->write_config("");
    _weights_writer->write_header("");
    _weights_writer->open();
    _weights_writer->write_data(&weights[0], weights_size_bytes);
    _weights_writer->close();
    _weights_writer->unlock();

    PSTLOG_TRACE(this, "get_monitor_data - monitoring");
    ska::pst::lmc::MonitorData monitor_data;
    handler->get_monitor_data(&monitor_data);

    EXPECT_TRUE(monitor_data.has_smrb()); // NOLINT
    auto smrb_data = monitor_data.smrb();
    auto data_stats = smrb_data.data();
    auto weights_stats = smrb_data.weights();

    auto expected_data_stats = _smrb->get_data_data_stats();
    auto expected_weights_stats = _smrb->get_weights_data_stats();

    EXPECT_EQ(data_stats.nbufs(), expected_data_stats.nbufs); // NOLINT
    EXPECT_EQ(data_stats.bufsz(), expected_data_stats.bufsz); // NOLINT
    EXPECT_EQ(data_stats.written(), expected_data_stats.written); // NOLINT
    EXPECT_EQ(data_stats.read(), 0); // NOLINT
    EXPECT_TRUE(expected_data_stats.written > 0); // NOLINT
    EXPECT_EQ(data_stats.full(), expected_data_stats.full); // NOLINT
    EXPECT_EQ(data_stats.clear(), expected_data_stats.clear); // NOLINT
    EXPECT_EQ(data_stats.available(), expected_data_stats.available); // NOLINT

    EXPECT_EQ(weights_stats.nbufs(), expected_weights_stats.nbufs); // NOLINT
    EXPECT_EQ(weights_stats.bufsz(), expected_weights_stats.bufsz); // NOLINT
    EXPECT_EQ(weights_stats.written(), expected_weights_stats.written); // NOLINT
    EXPECT_TRUE(expected_weights_stats.written > 0); // NOLINT
    EXPECT_EQ(weights_stats.read(), 0); // NOLINT
    EXPECT_EQ(weights_stats.full(), expected_weights_stats.full); // NOLINT
    EXPECT_EQ(weights_stats.clear(), expected_weights_stats.clear); // NOLINT
    EXPECT_EQ(weights_stats.available(), expected_weights_stats.available); // NOLINT

    // read the data
    PSTLOG_TRACE(this, "get_monitor_data - reading data");
    _data_reader->lock();
    _data_reader->read_config();
    _data_reader->read_header();
    _data_reader->open();
    _data_reader->open_block();
    uint64_t data_bytes_read = _data_reader->get_buf_bytes();
    PSTLOG_TRACE(this, "data_bytes_read = {}", data_bytes_read);
    _data_reader->close_block(data_bytes_read);
    _data_reader->close();
    _data_reader->unlock();

    _weights_reader->lock();
    _weights_reader->read_config();
    _weights_reader->read_header();
    _weights_reader->open();
    _weights_reader->open_block();
    uint64_t weights_bytes_read = _weights_reader->get_buf_bytes();
    PSTLOG_TRACE(this, "weights_bytes_read = {}", weights_bytes_read);
    _weights_reader->close_block(weights_bytes_read);
    _weights_reader->close();
    _weights_reader->unlock();

    // get latest monitoring data
    monitor_data.Clear();
    handler->get_monitor_data(&monitor_data);

    EXPECT_TRUE(monitor_data.has_smrb()); // NOLINT
    smrb_data = monitor_data.smrb();
    data_stats = smrb_data.data();
    weights_stats = smrb_data.weights();

    expected_data_stats = _smrb->get_data_data_stats();
    expected_weights_stats = _smrb->get_weights_data_stats();

    EXPECT_EQ(data_stats.nbufs(), expected_data_stats.nbufs); // NOLINT
    EXPECT_EQ(data_stats.bufsz(), expected_data_stats.bufsz); // NOLINT
    EXPECT_EQ(data_stats.written(), expected_data_stats.written); // NOLINT
    EXPECT_EQ(data_stats.read(), expected_data_stats.read); // NOLINT
    EXPECT_TRUE(expected_data_stats.read > 0); // NOLINT
    EXPECT_EQ(data_stats.full(), expected_data_stats.full); // NOLINT
    EXPECT_EQ(data_stats.clear(), expected_data_stats.clear); // NOLINT
    EXPECT_EQ(data_stats.available(), expected_data_stats.available); // NOLINT

    EXPECT_EQ(weights_stats.nbufs(), expected_weights_stats.nbufs); // NOLINT
    EXPECT_EQ(weights_stats.bufsz(), expected_weights_stats.bufsz); // NOLINT
    EXPECT_EQ(weights_stats.written(), expected_weights_stats.written); // NOLINT
    EXPECT_EQ(weights_stats.read(), expected_weights_stats.read); // NOLINT
    EXPECT_TRUE(expected_weights_stats.read > 0); // NOLINT
    EXPECT_EQ(weights_stats.full(), expected_weights_stats.full); // NOLINT
    EXPECT_EQ(weights_stats.clear(), expected_weights_stats.clear); // NOLINT
    EXPECT_EQ(weights_stats.available(), expected_weights_stats.available); // NOLINT

    // end scan
    PSTLOG_TRACE(this, "get_monitor_data - ending scan");
    handler->stop_scan();
    PSTLOG_TRACE(this, "get_monitor_data - scan ended");

    PSTLOG_TRACE(this, "get_monitor_data - deconfiguring scan");
    handler->deconfigure_scan();
    PSTLOG_TRACE(this, "get_monitor_data - scan deconfigured");

    PSTLOG_TRACE(this, "get_monitor_data - deconfiguring beam");
    handler->deconfigure_beam();
    PSTLOG_TRACE(this, "get_monitor_data - beam deconfigured");

}

TEST_F(SmrbLmcServiceHandlerTest, get_monitor_data_when_not_scanning_should_throw_exception) // NOLINT
{
    PSTLOG_TRACE(this, "get_monitor_data_when_not_scanning_should_throw_exception - end scan");
    EXPECT_FALSE(handler->is_scanning());

    try {
        ska::pst::lmc::MonitorData monitor_data;
        handler->get_monitor_data(&monitor_data);
        FAIL() << " expected get_monitor_data to throw exception due to not currently scanning.\n";
    } catch (ska::pst::common::LmcServiceException& ex) {
        EXPECT_EQ(std::string(ex.what()), "Received get_monitor_data request when SMRB is not scanning.");
        EXPECT_EQ(ex.error_code(), ska::pst::lmc::ErrorCode::NOT_SCANNING);
        EXPECT_EQ(ex.status_code(), grpc::StatusCode::FAILED_PRECONDITION);
    }
}

TEST_F(SmrbLmcServiceHandlerTest, configure_beam_error_should_throw_validation_error) // NOLINT
{
    PSTLOG_TRACE(this, "configure_beam_error_should_throw_validation_error - configuring beam");

    EXPECT_FALSE(handler->is_beam_configured()); // NOLINT

    try {
        ska::pst::lmc::BeamConfiguration beam_configuration;
        ska::pst::lmc::BeamConfiguration request;
        auto smrb_resources_request = request.mutable_smrb();
        smrb_resources_request->set_data_key("!@#$%");
        smrb_resources_request->set_weights_key("!@#$%");
        smrb_resources_request->set_hb_nbufs(hb_nbufs);
        smrb_resources_request->set_hb_bufsz(hb_bufsz);
        smrb_resources_request->set_db_nbufs(db_nbufs);
        smrb_resources_request->set_db_bufsz(db_bufsz);
        smrb_resources_request->set_wb_nbufs(wb_nbufs);
        smrb_resources_request->set_wb_bufsz(wb_bufsz);

        handler->configure_beam(request); // NOLINT
    } catch (ska::pst::common::pst_validation_error& ex) {
        EXPECT_EQ(std::string(ex.what()), "DATA_KEY with value !@#$% failed validation: failed regex validation of \"" + ska::pst::common::dada_key_regex + "\"\nWEIGHTS_KEY with value !@#$% failed validation: failed regex validation of \"" + ska::pst::common::dada_key_regex + "\"");
    }
}

TEST_F(SmrbLmcServiceHandlerTest, go_to_runtime_error) // NOLINT
{
    PSTLOG_TRACE(this, "go_to_runtime_error");

    try {
        throw std::runtime_error("this is a test error");
    } catch (...) {
        handler->go_to_runtime_error(std::current_exception());
    }

    ASSERT_EQ(ska::pst::common::RuntimeError, _smrb->get_state());
    ASSERT_EQ(ska::pst::common::RuntimeError, handler->get_application_manager_state());

    try {
        if (handler->get_application_manager_exception()) {
            std::rethrow_exception(handler->get_application_manager_exception());
        } else {
            // the exception should be set and not null
            ASSERT_FALSE(true);
        }
    } catch (const std::exception& exc) {
        ASSERT_EQ("this is a test error", std::string(exc.what()));
    }
}

TEST_F(SmrbLmcServiceHandlerTest, validate_configure_beam) // NOLINT
{
    ska::pst::lmc::BeamConfiguration request;
    auto smrb_resources_request = request.mutable_smrb();
    smrb_resources_request->set_data_key("a000"); // NOLINT
    smrb_resources_request->set_weights_key("a010"); // NOLINT
    smrb_resources_request->set_hb_nbufs(8); // NOLINT
    smrb_resources_request->set_hb_bufsz(4096); // NOLINT
    smrb_resources_request->set_db_nbufs(8); // NOLINT
    smrb_resources_request->set_db_bufsz(1048576); // NOLINT
    smrb_resources_request->set_wb_nbufs(8); // NOLINT
    smrb_resources_request->set_wb_bufsz(8192); // NOLINT

    // just want to perform validation
    EXPECT_FALSE(handler->is_beam_configured());
    validate_beam_configuration(request);
    EXPECT_FALSE(handler->is_beam_configured());
}


TEST_F(SmrbLmcServiceHandlerTest, validate_configure_beam_raises_validation_exception) // NOLINT
{
    ska::pst::lmc::BeamConfiguration request;
    auto smrb_resources_request = request.mutable_smrb();
    smrb_resources_request->set_data_key("@!00"); // NOLINT
    smrb_resources_request->set_weights_key("a010"); // NOLINT
    smrb_resources_request->set_hb_nbufs(8); // NOLINT
    smrb_resources_request->set_hb_bufsz(4096); // NOLINT
    smrb_resources_request->set_db_nbufs(8); // NOLINT
    smrb_resources_request->set_db_bufsz(1048576); // NOLINT
    smrb_resources_request->set_wb_nbufs(8); // NOLINT
    smrb_resources_request->set_wb_bufsz(8192); // NOLINT

    // just want to perform validation
    EXPECT_FALSE(handler->is_beam_configured());
    try {
        validate_beam_configuration(request);
        EXPECT_TRUE(false);
    } catch (ska::pst::common::pst_validation_error& ex) {
        EXPECT_EQ(std::string(ex.what()), "DATA_KEY with value @!00 failed validation: failed regex validation of \"" + ska::pst::common::dada_key_regex + "\"");
    }
    EXPECT_FALSE(handler->is_beam_configured());
}

TEST_F(SmrbLmcServiceHandlerTest, validate_configure_scan) // NOLINT
{
    PSTLOG_TRACE(this, "validate_configure_scan - configuring beam");
    configure_beam();
    PSTLOG_TRACE(this, "validate_configure_scan - beam configured");

    // just want to perform validation - shouldn't move state
    PSTLOG_TRACE(this, "validate_configure_scan - validating scan configuration");
    EXPECT_TRUE(handler->is_beam_configured());
    EXPECT_FALSE(handler->is_scan_configured());
    validate_scan_configuration();
    EXPECT_TRUE(handler->is_beam_configured());
    EXPECT_FALSE(handler->is_scan_configured());
    PSTLOG_TRACE(this, "validate_configure_scan - scan configuration validated");
}

TEST_F(SmrbLmcServiceHandlerTest, validate_configure_scan_even_when_not_beam_configured) // NOLINT
{
    // just want to perform validation - shouldn't move state
    EXPECT_FALSE(handler->is_beam_configured());
    EXPECT_FALSE(handler->is_scan_configured());
    validate_scan_configuration();
    EXPECT_FALSE(handler->is_scan_configured());
    EXPECT_FALSE(handler->is_beam_configured());
}


TEST_F(SmrbLmcServiceHandlerTest, reset_non_runtime_error_state) // NOLINT
{
    PSTLOG_TRACE(this, "reset_non_runtime_error_state - configuring beam");
    configure_beam();
    PSTLOG_TRACE(this, "reset_non_runtime_error_state - beam configured");

    PSTLOG_TRACE(this, "reset_non_runtime_error_state - configuring scan");
    EXPECT_FALSE(handler->is_scan_configured());
    configure_scan();
    EXPECT_TRUE(handler->is_scan_configured());
    PSTLOG_TRACE(this, "reset_non_runtime_error_state - scan configured");

    PSTLOG_TRACE(this, "reset_non_runtime_error_state - resetting");
    handler->reset();
    EXPECT_TRUE(handler->is_scan_configured());
    EXPECT_TRUE(handler->is_beam_configured());

    EXPECT_EQ(ska::pst::common::State::ScanConfigured, handler->get_application_manager_state());
    PSTLOG_TRACE(this, "reset_non_runtime_error_state - reset");
}

TEST_F(SmrbLmcServiceHandlerTest, reset_from_runtime_error_state) // NOLINT
{
    PSTLOG_TRACE(this, "reset_from_runtime_error_state - configuring beam");
    configure_beam();
    PSTLOG_TRACE(this, "reset_from_runtime_error_state - beam configured");

    PSTLOG_TRACE(this, "reset_from_runtime_error_state - configuring scan");
    EXPECT_FALSE(handler->is_scan_configured());
    configure_scan();
    EXPECT_TRUE(handler->is_scan_configured());
    PSTLOG_TRACE(this, "reset_from_runtime_error_state - scan configured");

    try {
        throw std::runtime_error("force fault state");
    } catch (...) {
        handler->go_to_runtime_error(std::current_exception());
    }

    PSTLOG_TRACE(this, "reset_from_runtime_error_state - resetting");
    handler->reset();
    EXPECT_FALSE(handler->is_scan_configured());
    EXPECT_FALSE(handler->is_beam_configured());

    EXPECT_EQ(ska::pst::common::State::Idle, handler->get_application_manager_state());
    PSTLOG_TRACE(this, "reset_from_runtime_error_state - reset");
}

TEST_F(SmrbLmcServiceHandlerTest, restart_from_beam_configured) // NOLINT
{
  GTEST_FLAG_SET(death_test_style, "threadsafe");
  PSTLOG_INFO(this, "restart_from_beam_configured - restart");
  // Assert that calling restart will eventually call exit(1)
  ASSERT_DEATH(
      {
          configure_beam();
          handler->restart();  // Call the restart method
      },".*"  // Optional: Regex to match any output from the process (can be empty)
  );
  PSTLOG_INFO(this, "restart_from_beam_configured - restarted");
  EXPECT_FALSE(handler->is_beam_configured());
}

TEST_F(SmrbLmcServiceHandlerTest, restart_from_scan_configured) // NOLINT
{
  GTEST_FLAG_SET(death_test_style, "threadsafe");
  PSTLOG_INFO(this, "restart_from_scan_configured - restart");
  // Assert that calling restart will eventually call exit(1)
  ASSERT_DEATH(
      {
          configure_beam();
          configure_scan();
          handler->restart();  // Call the restart method
      },".*"  // Optional: Regex to match any output from the process (can be empty)
  );
  PSTLOG_INFO(this, "restart_from_scan_configured - restarted");
  EXPECT_FALSE(handler->is_scan_configured());
  EXPECT_FALSE(handler->is_beam_configured());
}

TEST_F(SmrbLmcServiceHandlerTest, restart_from_scanning) // NOLINT
{
  GTEST_FLAG_SET(death_test_style, "threadsafe");
  PSTLOG_INFO(this, "restart_from_scanning - restart");
  // Assert that calling restart will eventually call exit(1)
  ASSERT_DEATH(
      {
          configure_beam();
          configure_scan();
          start_scan(); //NOLINT
          handler->restart();  // Call the restart method
      },".*"  // Optional: Regex to match any output from the process (can be empty)
  );
  PSTLOG_INFO(this, "restart_from_scanning - restarted");
  EXPECT_FALSE(handler->is_scanning());
  EXPECT_FALSE(handler->is_scan_configured());
  EXPECT_FALSE(handler->is_beam_configured());
}

TEST_F(SmrbLmcServiceHandlerTest, restart_from_runtime_error_state) // NOLINT
{
  GTEST_FLAG_SET(death_test_style, "threadsafe");
  PSTLOG_INFO(this, "restart_from_runtime_error_state - restart");
  // Assert that calling restart will eventually call exit(1)
  ASSERT_DEATH(
      {
          configure_beam();
          configure_scan();

          // induce error
          try {
              throw std::runtime_error("force fault state");
          } catch (...) {
              handler->go_to_runtime_error(std::current_exception());
          }
          handler->restart();  // Call the restart method
      },".*"  // Optional: Regex to match any output from the process (can be empty)
  );
  PSTLOG_INFO(this, "restart_from_runtime_error_state - restarted");
  EXPECT_FALSE(handler->is_scan_configured());
  EXPECT_FALSE(handler->is_beam_configured());
}

} // namespace ska::pst::smrb::test
