/*
 * Copyright 2022 Square Kilometre Array Observatory
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 * contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#include <iostream>
#include <grpc/grpc.h>
#include <grpc++/grpc++.h>

#include "ska_pst/common/lmc/LmcService.h"
#include "ska_pst/common/lmc/LmcServiceException.h"
#include "ska_pst/smrb/lmc/tests/SmrbLmcServiceIntegrationTest.h"
#include "ska_pst/common/testutils/GtestMain.h"
#include "ska_pst/common/utils/ValidationRegex.h"
#include "ska_pst/common/utils/Logging.h"

#define DRY_RUN true

auto main(int argc, char* argv[]) -> int
{
  return ska::pst::common::test::gtest_main(argc, argv); // NOLINT
}

namespace ska::pst::smrb::test {

SmrbLmcServiceIntegrationTest::SmrbLmcServiceIntegrationTest()
    : ::testing::Test()
{
}

void SmrbLmcServiceIntegrationTest::SetUp()
{
    PSTLOG_TRACE(this, "creating shared data block manager"); // NOLINT
    _smrb = std::make_shared<ska::pst::smrb::DataBlockManager>(); // NOLINT

    PSTLOG_TRACE(this, "creating handler"); // NOLINT
    _handler = std::make_shared<ska::pst::smrb::SmrbLmcServiceHandler>(_smrb); // NOLINT

    PSTLOG_TRACE(this, "creating service"); // NOLINT
    _service = std::make_shared<ska::pst::common::LmcService>("test_integration_service", _handler, _port); // NOLINT

    // force getting a port set, we need gRPC to start to bind to get port.
    _service->start(); // NOLINT

    _port = _service->port(); // NOLINT

    std::string server_address("127.0.0.1:"); // NOLINT
    PSTLOG_TRACE(this, "creating client connection on port {}", _port); // NOLINT
    server_address.append(std::to_string(_port)); // NOLINT
    _channel = grpc::CreateChannel(server_address, grpc::InsecureChannelCredentials()); // NOLINT
    _stub = ska::pst::lmc::PstLmcService::NewStub(_channel); // NOLINT

    _data_writer = std::make_shared<ska::pst::smrb::DataBlockWrite>("a000"); // NOLINT
    _data_reader = std::make_shared<ska::pst::smrb::DataBlockRead>("a000"); // NOLINT
    _weights_writer = std::make_shared<ska::pst::smrb::DataBlockWrite>("a010"); // NOLINT
    _weights_reader = std::make_shared<ska::pst::smrb::DataBlockRead>("a010"); // NOLINT
}

void SmrbLmcServiceIntegrationTest::TearDown()
{
    if (_data_writer)
    {
        if (_data_writer->get_opened())
        {
            _data_writer->close(); // NOLINT
        }
        if (_data_writer->get_locked())
        {
            _data_writer->unlock(); // NOLINT
        }
        if (_data_writer->is_connected())
        {
            _data_writer->disconnect(); // NOLINT
        }
    }
    _data_writer = nullptr; // NOLINT

    if (_weights_writer)
    {
        if (_weights_writer->get_opened())
        {
            _weights_writer->close(); // NOLINT
        }
        if (_weights_writer->get_locked())
        {
            _weights_writer->unlock(); // NOLINT
        }
        if (_weights_writer->is_connected())
        {
            _weights_writer->disconnect(); // NOLINT
        }
    }
    _weights_writer = nullptr; // NOLINT

    if (_data_reader)
    {
        if (_data_reader->get_opened())
        {
            _data_reader->close(); // NOLINT
        }
        if (_data_reader->get_locked())
        {
            _data_reader->unlock(); // NOLINT
        }
        if (_data_reader->is_connected())
        {
            _data_reader->disconnect(); // NOLINT
        }
    }
    _data_reader = nullptr; // NOLINT

    if (_weights_reader)
    {
        if (_weights_reader->get_opened())
        {
            _weights_reader->close(); // NOLINT
        }
        if (_weights_reader->get_locked())
        {
            _weights_reader->unlock(); // NOLINT
        }
        if (_weights_reader->is_connected())
        {
            _weights_reader->disconnect(); // NOLINT
        }
    }
    _weights_reader = nullptr; // NOLINT

    _service->stop(); // NOLINT
    _smrb->quit(); // NOLINT
    _smrb = nullptr; // NOLINT
}

auto SmrbLmcServiceIntegrationTest::configure_beam(bool dry_run) -> grpc::Status
{
    ska::pst::lmc::ConfigureBeamRequest request; // NOLINT
    request.set_dry_run(dry_run);
    auto beam_configuration = request.mutable_beam_configuration(); // NOLINT
    auto smrb_resources_request = beam_configuration->mutable_smrb(); // NOLINT
    smrb_resources_request->set_data_key(data_key); // NOLINT
    smrb_resources_request->set_weights_key(weights_key); // NOLINT
    smrb_resources_request->set_hb_nbufs(hb_nbufs); // NOLINT
    smrb_resources_request->set_hb_bufsz(hb_bufsz); // NOLINT
    smrb_resources_request->set_db_nbufs(db_nbufs); // NOLINT
    smrb_resources_request->set_db_bufsz(db_bufsz); // NOLINT
    smrb_resources_request->set_wb_nbufs(wb_nbufs); // NOLINT
    smrb_resources_request->set_wb_bufsz(wb_bufsz); // NOLINT

    return configure_beam(request); // NOLINT
}

auto SmrbLmcServiceIntegrationTest::configure_beam(const ska::pst::lmc::ConfigureBeamRequest &request) -> grpc::Status
{
    grpc::ClientContext context; // NOLINT
    ska::pst::lmc::ConfigureBeamResponse response; // NOLINT

    return _stub->configure_beam(&context, request, &response); // NOLINT
}

auto SmrbLmcServiceIntegrationTest::get_beam_configuration(
    ska::pst::lmc::GetBeamConfigurationResponse* response
) -> grpc::Status
{
    grpc::ClientContext context; // NOLINT
    ska::pst::lmc::GetBeamConfigurationRequest request; // NOLINT

    return _stub->get_beam_configuration(&context, request, response); // NOLINT
}

auto SmrbLmcServiceIntegrationTest::deconfigure_beam() -> grpc::Status
{
    grpc::ClientContext context; // NOLINT
    ska::pst::lmc::DeconfigureBeamRequest request; // NOLINT
    ska::pst::lmc::DeconfigureBeamResponse response; // NOLINT

    return _stub->deconfigure_beam(&context, request, &response); // NOLINT
}

auto SmrbLmcServiceIntegrationTest::configure_scan(bool dry_run) -> grpc::Status
{
    grpc::ClientContext context; // NOLINT
    ska::pst::lmc::ConfigureScanRequest request; // NOLINT
    request.set_dry_run(dry_run);
    auto scan_configuration = request.mutable_scan_configuration(); // NOLINT
    scan_configuration->mutable_smrb(); // NOLINT

    return configure_scan(request); // NOLINT
}

auto SmrbLmcServiceIntegrationTest::configure_scan(const ska::pst::lmc::ConfigureScanRequest &request) -> grpc::Status
{
    grpc::ClientContext context; // NOLINT
    ska::pst::lmc::ConfigureScanResponse response; // NOLINT

    return _stub->configure_scan(&context, request, &response); // NOLINT
}

auto SmrbLmcServiceIntegrationTest::deconfigure_scan() -> grpc::Status
{
    grpc::ClientContext context; // NOLINT
    ska::pst::lmc::DeconfigureScanRequest request; // NOLINT
    ska::pst::lmc::DeconfigureScanResponse response; // NOLINT

    return _stub->deconfigure_scan(&context, request, &response); // NOLINT
}

auto SmrbLmcServiceIntegrationTest::get_scan_configuration(
    ska::pst::lmc::GetScanConfigurationResponse *response
) -> grpc::Status
{
    grpc::ClientContext context; // NOLINT
    ska::pst::lmc::GetScanConfigurationRequest request; // NOLINT

    return _stub->get_scan_configuration(&context, request, response); // NOLINT
}

auto SmrbLmcServiceIntegrationTest::start_scan() -> grpc::Status
{
    grpc::ClientContext context; // NOLINT
    ska::pst::lmc::StartScanRequest request; // NOLINT
    ska::pst::lmc::StartScanResponse response; // NOLINT

    return _stub->start_scan(&context, request, &response); // NOLINT
}

auto SmrbLmcServiceIntegrationTest::stop_scan() -> grpc::Status
{
    grpc::ClientContext context; // NOLINT
    ska::pst::lmc::StopScanRequest request; // NOLINT
    ska::pst::lmc::StopScanResponse response; // NOLINT

    return _stub->stop_scan(&context, request, &response); // NOLINT
}

auto SmrbLmcServiceIntegrationTest::abort() -> grpc::Status
{
    grpc::ClientContext context; // NOLINT
    ska::pst::lmc::AbortRequest request; // NOLINT
    ska::pst::lmc::AbortResponse response; // NOLINT

    return _stub->abort(&context, request, &response); // NOLINT
}

auto SmrbLmcServiceIntegrationTest::reset() -> grpc::Status
{
    grpc::ClientContext context; // NOLINT
    ska::pst::lmc::ResetRequest request; // NOLINT
    ska::pst::lmc::ResetResponse response; // NOLINT

    return _stub->reset(&context, request, &response); // NOLINT
}

auto SmrbLmcServiceIntegrationTest::restart() -> grpc::Status
{
    grpc::ClientContext context; // NOLINT
    ska::pst::lmc::RestartRequest request; // NOLINT
    ska::pst::lmc::RestartResponse response; // NOLINT

    return _stub->restart(&context, request, &response); // NOLINT
}

auto SmrbLmcServiceIntegrationTest::go_to_fault() -> grpc::Status
{
    grpc::ClientContext context; // NOLINT
    ska::pst::lmc::GoToFaultRequest request; // NOLINT
    ska::pst::lmc::GoToFaultResponse response; // NOLINT

    return _stub->go_to_fault(&context, request, &response); // NOLINT
}

auto SmrbLmcServiceIntegrationTest::get_state(
    ska::pst::lmc::GetStateResponse* response
) -> grpc::Status
{
    grpc::ClientContext context; // NOLINT
    ska::pst::lmc::GetStateRequest request; // NOLINT

    return _stub->get_state(&context, request, response); // NOLINT
}

auto SmrbLmcServiceIntegrationTest::set_log_level(
    ska::pst::lmc::LogLevel required_log_level
) -> grpc::Status
{
    grpc::ClientContext context;
    ska::pst::lmc::SetLogLevelResponse response;
    ska::pst::lmc::SetLogLevelRequest request;
    request.set_log_level(required_log_level);
    return _stub->set_log_level(&context, request, &response);
}


auto SmrbLmcServiceIntegrationTest::get_log_level(
    ska::pst::lmc::GetLogLevelResponse& response
) -> grpc::Status
{
    grpc::ClientContext context;
    ska::pst::lmc::GetLogLevelRequest request;
    return _stub->get_log_level(&context, request, &response);
}


void SmrbLmcServiceIntegrationTest::assert_log_level(
    ska::pst::lmc::LogLevel expected_loglevel
)
{
    switch (expected_loglevel) {
        case ska::pst::lmc::LogLevel::INFO:
            ASSERT_EQ(spdlog::level::info, spdlog::get_level());
            break;
        case ska::pst::lmc::LogLevel::DEBUG:
            ASSERT_EQ(spdlog::level::debug, spdlog::get_level());
            break;
        case ska::pst::lmc::LogLevel::WARNING:
            ASSERT_EQ(spdlog::level::warn, spdlog::get_level());
            break;
        case ska::pst::lmc::LogLevel::CRITICAL:
            ASSERT_EQ(spdlog::level::critical, spdlog::get_level());
            break;
        case ska::pst::lmc::LogLevel::ERROR:
            ASSERT_EQ(spdlog::level::err, spdlog::get_level());
            break;
        default:
            FAIL() << "LogLevel enum not implemented yet";
            break;
    }
}

void SmrbLmcServiceIntegrationTest::assert_state(
    ska::pst::lmc::ObsState expected_state
)
{
    ska::pst::lmc::GetStateResponse get_state_response; // NOLINT
    auto status = get_state(&get_state_response); // NOLINT
    EXPECT_TRUE(status.ok()); // NOLINT
    EXPECT_EQ(get_state_response.state(), expected_state); // NOLINT

    switch (expected_state) {
        case ska::pst::lmc::ObsState::EMPTY:
            assert_manager_state(ska::pst::common::Idle);
            break;
        case ska::pst::lmc::ObsState::IDLE:
            assert_manager_state(ska::pst::common::BeamConfigured);
            break;
        case ska::pst::lmc::ObsState::READY:
            assert_manager_state(ska::pst::common::ScanConfigured);
            break;
        case ska::pst::lmc::ObsState::SCANNING:
            assert_manager_state(ska::pst::common::Scanning);
            break;
        case ska::pst::lmc::ObsState::FAULT:
            assert_manager_state(ska::pst::common::RuntimeError);
            break;
        case ska::pst::lmc::ObsState::ABORTED:
            // we can't assert the manager state
            break;
        default:
            FAIL() << "ObsState enum not implemented yet";
            break;
    }
}

void SmrbLmcServiceIntegrationTest::assert_manager_state(
    ska::pst::common::State expected_state
)
{
    auto curr_state = _smrb->get_state();
    EXPECT_EQ(curr_state, expected_state); // NOLINT
}

TEST_F(SmrbLmcServiceIntegrationTest, scan_endscan) // NOLINT
{
    EXPECT_TRUE(_service->is_running()); // NOLINT
    assert_state(ska::pst::lmc::ObsState::EMPTY); // NOLINT

    PSTLOG_TRACE(this, "scan_endscan - configuring beam"); // NOLINT
    auto status = configure_beam(); // NOLINT

    EXPECT_TRUE(status.ok()); // NOLINT
    EXPECT_TRUE(_smrb->is_beam_configured()); // NOLINT
    assert_state(ska::pst::lmc::ObsState::IDLE); // NOLINT
    PSTLOG_TRACE(this, "scan_endscan - beam configured"); // NOLINT

    PSTLOG_TRACE(this, "scan_endscan - configuring scan"); // NOLINT
    status = configure_scan(); // NOLINT
    EXPECT_TRUE(status.ok()); // NOLINT
    assert_state(ska::pst::lmc::ObsState::READY); // NOLINT
    PSTLOG_TRACE(this, "scan_endscan - scan configured"); // NOLINT

    PSTLOG_TRACE(this, "scan_endscan - starting scan"); // NOLINT
    status = start_scan(); // NOLINT
    EXPECT_TRUE(status.ok()); // NOLINT
    assert_state(ska::pst::lmc::ObsState::SCANNING); // NOLINT
    PSTLOG_TRACE(this, "scan_endscan - scanning"); // NOLINT

    PSTLOG_TRACE(this, "scan_endscan - stopping scan"); // NOLINT
    status = stop_scan(); // NOLINT
    EXPECT_TRUE(status.ok()); // NOLINT
    assert_state(ska::pst::lmc::ObsState::READY); // NOLINT
    PSTLOG_TRACE(this, "scan_endscan - scan ended"); // NOLINT

    PSTLOG_TRACE(this, "configure_deconfigure - deconfiguring scan"); // NOLINT
    status = deconfigure_scan(); // NOLINT
    EXPECT_TRUE(status.ok()); // NOLINT
    assert_state(ska::pst::lmc::ObsState::IDLE); // NOLINT
    PSTLOG_TRACE(this, "configure_deconfigure - scan deconfigured"); // NOLINT

    PSTLOG_TRACE(this, "configure_deconfigure - deconfiguring beam"); // NOLINT
    status = deconfigure_beam(); // NOLINT
    EXPECT_TRUE(status.ok()); // NOLINT
    EXPECT_FALSE(_smrb->is_beam_configured()); // NOLINT
    assert_state(ska::pst::lmc::ObsState::EMPTY); // NOLINT
    PSTLOG_TRACE(this, "configure_deconfigure - beam deconfigured"); // NOLINT
}

TEST_F(SmrbLmcServiceIntegrationTest, invalid_beam_configuration) // NOLINT
{
    // Induce error in config to simulate RuntimeError
    ska::pst::lmc::ConfigureBeamRequest request_with_error; // NOLINT
    auto beam_configuration_with_error = request_with_error.mutable_beam_configuration(); // NOLINT
    auto smrb_resources_requestwith_error = beam_configuration_with_error->mutable_smrb(); // NOLINT
    smrb_resources_requestwith_error->set_data_key("!@#$%"); // NOLINT
    smrb_resources_requestwith_error->set_weights_key("!@#$%"); // NOLINT
    smrb_resources_requestwith_error->set_hb_nbufs(hb_nbufs); // NOLINT
    smrb_resources_requestwith_error->set_hb_bufsz(hb_bufsz); // NOLINT
    smrb_resources_requestwith_error->set_db_nbufs(db_nbufs); // NOLINT
    smrb_resources_requestwith_error->set_db_bufsz(db_bufsz); // NOLINT
    smrb_resources_requestwith_error->set_wb_nbufs(wb_nbufs); // NOLINT
    smrb_resources_requestwith_error->set_wb_bufsz(wb_bufsz); // NOLINT

    EXPECT_TRUE(_service->is_running()); // NOLINT
    assert_state(ska::pst::lmc::ObsState::EMPTY); // NOLINT

    PSTLOG_TRACE(this, "invalid_beam_configuration - configuring beam with error"); // NOLINT
    auto status = configure_beam(request_with_error); // NOLINT
    EXPECT_FALSE(status.ok()); // NOLINT
    EXPECT_FALSE(_smrb->is_beam_configured()); // NOLINT
    assert_state(ska::pst::lmc::ObsState::EMPTY); // NOLINT
}

TEST_F(SmrbLmcServiceIntegrationTest, reset_when_in_fault_state) // NOLINT
{
    EXPECT_TRUE(_service->is_running()); // NOLINT
    assert_state(ska::pst::lmc::ObsState::EMPTY); // NOLINT

    auto status = go_to_fault();
    EXPECT_TRUE(status.ok()); // NOLINT
    assert_state(ska::pst::lmc::ObsState::FAULT); // NOLINT

    status = reset();
    EXPECT_TRUE(status.ok()); // NOLINT
    assert_state(ska::pst::lmc::ObsState::EMPTY);
}

TEST_F(SmrbLmcServiceIntegrationTest, validate_configure_beam) // NOLINT
{
    EXPECT_TRUE(_service->is_running()); // NOLINT
    assert_state(ska::pst::lmc::ObsState::EMPTY); // NOLINT

    PSTLOG_TRACE(this, "validate_configure_beam - validating beam config"); // NOLINT
    auto status = configure_beam(DRY_RUN); // NOLINT

    EXPECT_TRUE(status.ok()); // NOLINT
    EXPECT_FALSE(_smrb->is_beam_configured()); // NOLINT
    assert_state(ska::pst::lmc::ObsState::EMPTY); // NOLINT
    PSTLOG_TRACE(this, "validate_configure_beam - beam config validated"); // NOLINT
}

TEST_F(SmrbLmcServiceIntegrationTest, validate_configure_beam_invalid_config) // NOLINT
{
    EXPECT_TRUE(_service->is_running()); // NOLINT
    assert_state(ska::pst::lmc::ObsState::EMPTY); // NOLINT

    PSTLOG_TRACE(this, "validate_configure_beam_invalid_config - validating beam config"); // NOLINT

    ska::pst::lmc::ConfigureBeamRequest request;
    request.set_dry_run(true);
    auto beam_config = request.mutable_beam_configuration();
    auto smrb_resources_request = beam_config->mutable_smrb();
    smrb_resources_request->set_data_key("@!00"); // NOLINT
    smrb_resources_request->set_weights_key("a010"); // NOLINT
    smrb_resources_request->set_hb_nbufs(8); // NOLINT
    smrb_resources_request->set_hb_bufsz(4096); // NOLINT
    smrb_resources_request->set_db_nbufs(8); // NOLINT
    smrb_resources_request->set_db_bufsz(1048576); // NOLINT
    smrb_resources_request->set_wb_nbufs(8); // NOLINT
    smrb_resources_request->set_wb_bufsz(8192); // NOLINT

    auto status = configure_beam(request);
    EXPECT_FALSE(status.ok());

    EXPECT_EQ(grpc::StatusCode::FAILED_PRECONDITION, status.error_code()); // NOLINT
    EXPECT_EQ(_service->service_name() + " - Error in validating beam configuration: DATA_KEY with value @!00 failed validation: failed regex validation of \"" + ska::pst::common::dada_key_regex + "\"", status.error_message()); // NOLINT

    ska::pst::lmc::Status lmc_status;
    lmc_status.ParseFromString(status.error_details());
    EXPECT_EQ(ska::pst::lmc::ErrorCode::INVALID_REQUEST, lmc_status.code()); // NOLINT
    EXPECT_EQ(status.error_message(), lmc_status.message()); // NOLINT


    EXPECT_FALSE(_handler->is_beam_configured());
    assert_state(ska::pst::lmc::ObsState::EMPTY); // NOLINT
    PSTLOG_TRACE(this, "validate_configure_beam_invalid_config - beam config validated"); // NOLINT
}

TEST_F(SmrbLmcServiceIntegrationTest, validate_configure_scan) // NOLINT
{
    EXPECT_TRUE(_service->is_running()); // NOLINT
    assert_state(ska::pst::lmc::ObsState::EMPTY); // NOLINT

    PSTLOG_TRACE(this, "validate_configure_scan - configuring beam"); // NOLINT
    auto status = configure_beam(); // NOLINT
    assert_state(ska::pst::lmc::ObsState::IDLE); // NOLINT
    PSTLOG_TRACE(this, "validate_configure_scan - beam configured"); // NOLINT

    PSTLOG_TRACE(this, "validate_configure_scan - validating scan configuration"); // NOLINT
    status = configure_scan(DRY_RUN); // NOLINT
    EXPECT_TRUE(status.ok()); // NOLINT
    assert_state(ska::pst::lmc::ObsState::IDLE); // NOLINT
    PSTLOG_TRACE(this, "validate_configure_scan - scan config validated"); // NOLINT
}

TEST_F(SmrbLmcServiceIntegrationTest, set_log_levels) // NOLINT
{
    EXPECT_TRUE(_service->is_running()); // NOLINT
    ska::pst::lmc::GetLogLevelResponse response;

    PSTLOG_TRACE(this, "set_loglevel - DEBUG"); // NOLINT
    set_log_level(ska::pst::lmc::LogLevel::DEBUG);
    assert_log_level(ska::pst::lmc::LogLevel::DEBUG);
    auto status = get_log_level(response);
    EXPECT_TRUE(status.ok());
    ASSERT_EQ(ska::pst::lmc::LogLevel::DEBUG, response.log_level());

    PSTLOG_TRACE(this, "set_log_level - INFO"); // NOLINT
    set_log_level(ska::pst::lmc::LogLevel::INFO);
    assert_log_level(ska::pst::lmc::LogLevel::INFO);
    status = get_log_level(response);
    EXPECT_TRUE(status.ok());
    ASSERT_EQ(ska::pst::lmc::LogLevel::INFO, response.log_level());

    PSTLOG_TRACE(this, "set_log_level - WARNING"); // NOLINT
    set_log_level(ska::pst::lmc::LogLevel::WARNING);
    assert_log_level(ska::pst::lmc::LogLevel::WARNING);
    status = get_log_level(response);
    EXPECT_TRUE(status.ok());
    ASSERT_EQ(ska::pst::lmc::LogLevel::WARNING, response.log_level());

    PSTLOG_TRACE(this, "set_log_level - CRITICAL"); // NOLINT
    set_log_level(ska::pst::lmc::LogLevel::CRITICAL);
    assert_log_level(ska::pst::lmc::LogLevel::CRITICAL);
    status = get_log_level(response);
    EXPECT_TRUE(status.ok());
    ASSERT_EQ(ska::pst::lmc::LogLevel::CRITICAL, response.log_level());

    PSTLOG_TRACE(this, "set_log_level - ERROR"); // NOLINT
    set_log_level(ska::pst::lmc::LogLevel::ERROR);
    assert_log_level(ska::pst::lmc::LogLevel::ERROR);
    status = get_log_level(response);
    EXPECT_TRUE(status.ok());
    ASSERT_EQ(ska::pst::lmc::LogLevel::ERROR, response.log_level());
}

TEST_F(SmrbLmcServiceIntegrationTest, restart_from_beam_configured) // NOLINT
{
  GTEST_FLAG_SET(death_test_style, "threadsafe");
  ASSERT_DEATH(
      {
          _service->start(); // NOLINT
          EXPECT_TRUE(_service->is_running()); // NOLINT
          assert_state(ska::pst::lmc::ObsState::EMPTY); // NOLINT
          ASSERT_EQ(ska::pst::common::State::Idle, _smrb->get_state()); // NOLINT

          PSTLOG_TRACE(this, "restart_from_beam_configured - configuring beam"); // NOLINT
          auto status = configure_beam(); // NOLINT
          EXPECT_TRUE(status.ok()); // NOLINT
          assert_state(ska::pst::lmc::ObsState::IDLE); // NOLINT
          PSTLOG_TRACE(this, "restart_from_beam_configured - beam configured"); // NOLINT

          status = restart();
          EXPECT_TRUE(status.ok()); // NOLINT
      },".*"  // Optional: Regex to match any output from the process (can be empty)
  );
  assert_state(ska::pst::lmc::ObsState::EMPTY);
}

TEST_F(SmrbLmcServiceIntegrationTest, restart_from_scan_configured) // NOLINT
{
  GTEST_FLAG_SET(death_test_style, "threadsafe");
  ASSERT_DEATH(
      {
          _service->start(); // NOLINT
          EXPECT_TRUE(_service->is_running()); // NOLINT
          assert_state(ska::pst::lmc::ObsState::EMPTY); // NOLINT
          ASSERT_EQ(ska::pst::common::State::Idle, _smrb->get_state()); // NOLINT

          PSTLOG_TRACE(this, "restart_from_scan_configured - configuring beam"); // NOLINT
          auto status = configure_beam(); // NOLINT
          EXPECT_TRUE(status.ok()); // NOLINT
          assert_state(ska::pst::lmc::ObsState::IDLE); // NOLINT
          PSTLOG_TRACE(this, "restart_from_scan_configured - beam configured"); // NOLINT

          PSTLOG_TRACE(this, "restart_from_scan_configured - configuring scan"); // NOLINT
          status = configure_scan(); // NOLINT
          EXPECT_TRUE(status.ok()); // NOLINT
          assert_state(ska::pst::lmc::ObsState::READY); // NOLINT
          PSTLOG_TRACE(this, "restart_from_scan_configured - scan configured"); // NOLINT

          status = restart();
          EXPECT_TRUE(status.ok()); // NOLINT
      },".*"  // Optional: Regex to match any output from the process (can be empty)
  );
  assert_state(ska::pst::lmc::ObsState::EMPTY);
}

TEST_F(SmrbLmcServiceIntegrationTest, restart_from_scanning) // NOLINT
{
  GTEST_FLAG_SET(death_test_style, "threadsafe");
  ASSERT_DEATH(
      {
          _service->start(); // NOLINT
          EXPECT_TRUE(_service->is_running()); // NOLINT
          assert_state(ska::pst::lmc::ObsState::EMPTY); // NOLINT
          ASSERT_EQ(ska::pst::common::State::Idle, _smrb->get_state()); // NOLINT

          PSTLOG_TRACE(this, "restart_from_scanning - configuring beam"); // NOLINT
          auto status = configure_beam(); // NOLINT
          EXPECT_TRUE(status.ok()); // NOLINT
          assert_state(ska::pst::lmc::ObsState::IDLE); // NOLINT
          PSTLOG_TRACE(this, "restart_from_scanning - beam configured"); // NOLINT

          PSTLOG_TRACE(this, "restart_from_scanning - configuring scan"); // NOLINT
          status = configure_scan(); // NOLINT
          EXPECT_TRUE(status.ok()); // NOLINT
          assert_state(ska::pst::lmc::ObsState::READY); // NOLINT
          PSTLOG_TRACE(this, "restart_from_scanning - scan configured"); // NOLINT

          PSTLOG_TRACE(this, "restart_from_scanning - starting scan"); // NOLINT
          EXPECT_FALSE(_smrb->is_scanning()); // NOLINT
          status = start_scan(); // NOLINT
          EXPECT_TRUE(status.ok()); // NOLINT
          assert_state(ska::pst::lmc::ObsState::SCANNING); // NOLINT
          EXPECT_TRUE(_smrb->is_scanning()); // NOLINT

          PSTLOG_TRACE(this, "restart_from_scanning - scanning"); // NOLINT

          status = restart();
          EXPECT_TRUE(status.ok()); // NOLINT
      },".*"  // Optional: Regex to match any output from the process (can be empty)
  );
  assert_state(ska::pst::lmc::ObsState::EMPTY);
}

TEST_F(SmrbLmcServiceIntegrationTest, restart_from_fault) // NOLINT
{
  GTEST_FLAG_SET(death_test_style, "threadsafe");
  ASSERT_DEATH(
      {
        _service->start(); // NOLINT
        EXPECT_TRUE(_service->is_running()); // NOLINT
        assert_state(ska::pst::lmc::ObsState::EMPTY); // NOLINT
        ASSERT_EQ(ska::pst::common::State::Idle, _smrb->get_state()); // NOLINT

        PSTLOG_TRACE(this, "restart_from_fault - configuring beam"); // NOLINT
        auto status = configure_beam(); // NOLINT
        EXPECT_TRUE(status.ok()); // NOLINT
        assert_state(ska::pst::lmc::ObsState::IDLE); // NOLINT
        PSTLOG_TRACE(this, "restart_from_fault - beam configured"); // NOLINT

        status = go_to_fault();
        EXPECT_TRUE(status.ok()); // NOLINT
        assert_state(ska::pst::lmc::ObsState::FAULT); // NOLINT

        status = restart();
        EXPECT_TRUE(status.ok()); // NOLINT
      },".*"  // Optional: Regex to match any output from the process (can be empty)
  );
  assert_state(ska::pst::lmc::ObsState::EMPTY);
}
} // namespace ska::pst::smrb::test
