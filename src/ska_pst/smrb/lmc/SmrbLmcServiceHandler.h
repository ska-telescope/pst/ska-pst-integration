/*
 * Copyright 2022 Square Kilometre Array Observatory
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 * contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef __SKA_PST_SMRB_SmrbLmcServiceHandler_h
#define __SKA_PST_SMRB_SmrbLmcServiceHandler_h

#include <memory>

#include "ska_pst/smrb/DataBlockManager.h"
#include "ska_pst/common/lmc/LmcServiceHandler.h"

namespace ska::pst::smrb
{
    /**
     * @brief Class to act as a bridge between the local monitoring and control
     *    service of the SMRB and the @see ska::pst::smrb::DataBlockManager
     *
     */
    class SmrbLmcServiceHandler final : public ska::pst::common::LmcServiceHandler
    {
    private:
        /**
         * @brief A share pointer to a @see ska::pst::smrb::DataBlockManager
         *      which is used to manage the ring buffers.
         */
        std::shared_ptr<ska::pst::smrb::DataBlockManager> smrb;

        // values used as an offset when a scan starts
        /**
         * @brief The amount of data previously written to the data ring buffer
         *    before a scan starts.
         */
        uint64_t data_written{0};
        /**
         * @brief The amount of data previously read from the data ring buffer
         *    before a scan starts.
         */
        uint64_t data_read{0};
        /**
         * @brief The amount of data previously written to the weights ring buffer
         *    before a scan starts.
         */
        uint64_t weights_written{0};
        /**
         * @brief The amount of data previously read from the weights ring buffer
         *    before a scan starts.
         */
        uint64_t weights_read{0};

    public:
        SmrbLmcServiceHandler(std::shared_ptr<ska::pst::smrb::DataBlockManager> smrb) : smrb(std::move(smrb)) {}
        virtual ~SmrbLmcServiceHandler() = default;

        // validation methods
        /**
         * @brief Validate a beam configuration.
         *
         * Validate a beam configuration for correctness but does not apply the configuration.
         *
         * @throw std::exception if there is a problem with the beam configuration of the service.
         * @throw ska::pst::common::pst_validation_error if there are validation errors in the request.
         */
        void validate_beam_configuration(const ska::pst::lmc::BeamConfiguration &configuration) override;

        /**
         * @brief Validate a scan configuration.
         *
         * Validate a scan configuration for correctness but does not apply the configuration.
         *
         * @throw std::exception if there is a problem with the beam configuration of the service.
         * @throw ska::pst::common::pst_validation_error if there are validation errors in the request.
         */
        void validate_scan_configuration(const ska::pst::lmc::ScanConfiguration &configuration) override;

        // beam configuration methods
        /**
         * @brief Handle configuring the service to be a part of a beam.
         *
         * This implementation expects that there is an smrb sub-field in the configuration
         * request. It takes the configuration and uses a data block manager to set up the
         * data and weights ring buffers that can be used by the PST signal processing
         * pipeline.
         *
         * @param configuration the configuration for the beam, which should include an 'smrb' sub-field message.
         */
        void configure_beam(const ska::pst::lmc::BeamConfiguration &configuration) override;

        /**
         * @brief Handle deconfiguring the service from a beam.
         *
         * This will attempt to release and destroy the created ring buffers for the PST signal
         * processing pipeline. This should only be called after all other applications have
         * disconnected from the ring buffers.
         */
        void deconfigure_beam() override;

        /**
         * @brief Handle getting the current beam configuration for the service.
         *
         * This will return the current configuration used to create the the ring buffers.
         *
         * @param configuration Pointer to the protobuf message to return. This implementation
         *      will set the smrb sub-field in configuration message.
         */
        void get_beam_configuration(ska::pst::lmc::BeamConfiguration *configuration) override;

        /**
         * @brief Check if this service is configured for a beam.
         *
         * Will return true if the @see DataBlockManager is configured for a beam.
         */
        bool is_beam_configured() const noexcept override;

        // scan configuration methods
        /**
         * @brief Handle configuring the service for a scan.
         *
         * This is a no-op method for SMRB, though checks if already configured.
         */
        void configure_scan(const ska::pst::lmc::ScanConfiguration &configuration) override;

        /**
         * @brief Handle deconfiguring service for a scan.
         *
         * This is a no-op method for SMRB.
         */
        void deconfigure_scan() override;

        /**
         * @brief Handle getting the current scan configuration for the service.
         *
         * This will return an empty smrb sub-field message, other than that it
         * is a no-op method.
         */
        void get_scan_configuration(ska::pst::lmc::ScanConfiguration *configuration) override;

        /**
         * @brief Check if the service has been configured for a scan.
         *
         * @return This will return true if there had been a call to configure.
         */
        bool is_scan_configured() const noexcept override;

        // scan method
        /**
         * @brief Handle initiating a scan.
         *
         * This method is effectively a no-op method. However, this will set state
         * that the service is scanning and also reset internal stats to allow monitoring
         * to have stats specifically for the current scan.
         */
        void start_scan(const ska::pst::lmc::StartScanRequest &request) override;

        /**
         * @brief Handle ending a scan.
         *
         * This will mark the application as not scanning.
         */
        void stop_scan() override;

        /**
         * @brief Handle resetting DatablockManager State into Idle
         *
         * This ties the states between LmcService ObsState::EMPTY with ApplicationManager State::Idle
         */
        void reset() override;

        /**
         * @brief Handle restarting the DatablockManager State into Idle
         *
         * This ties the states between LmcService ObsState::EMPTY with ApplicationManager State::Idle
         */
        void restart() override;

        /**
         * @brief Check if the service is currently performing a scan.
         *
         */
        bool is_scanning() const noexcept override;

        // monitoring
        /**
         * @brief Handle getting the monitoring data for the service.
         *
         * This will get the current monitoring data for the data and weights
         * ring buffers. It is only valid to call this if the handler is currently
         * scanning.
         *
         * @param data Pointer to the protobuf message to return. Implementations should get
         *      mutable references to the sub-field they are responding to and update that message.
         * @throw std::exception if there is a validation issue or problem during monitoring.
         */
        void get_monitor_data(ska::pst::lmc::MonitorData *data) override;

        /**
         * @brief Get the DatablockManager state
         *
         * @return ska::pst::common::State returns current the enum State of the DatablockManager
         */
        ska::pst::common::State get_application_manager_state() override { return smrb->get_state(); }

        /**
         * @brief Get the DatablockManager exception pointer
         *
         * @return std::exception_ptr returns the current captured exception caught by the DatablockManager
         */
        std::exception_ptr get_application_manager_exception() override { return smrb->get_exception(); }

        /**
         * @brief Put application into a runtime error state.
         *
         * @param exc an exception pointer to store on the application manager.
         */
        void go_to_runtime_error(std::exception_ptr exc) override;
    };

} // smrb::pst::ska

#endif // __SKA_PST_SMRB_SmrbLmcServiceHandler_h
