/*
 * Copyright 2022 Square Kilometre Array Observatory
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 * contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#include <string>
#include <sstream>
#include <thread>
#include <chrono>
#include "ska_pst/smrb/lmc/SmrbLmcServiceHandler.h"
#include "ska_pst/common/lmc/LmcServiceException.h"
#include "ska_pst/common/utils/ValidationContext.h"
#include "ska_pst/common/utils/Logging.h"

auto beam_configuration_as_ascii_header(
    const ska::pst::lmc::BeamConfiguration &configuration) -> ska::pst::common::AsciiHeader
{
    if (!configuration.has_smrb())
    {
        // PSTLOG_WARN(this, "Received configure beam request no SMRB details provided.");

        throw ska::pst::common::LmcServiceException(
            "Expected an SMRB resources object, but none were provided.",
            ska::pst::lmc::ErrorCode::INVALID_REQUEST,
            grpc::StatusCode::INVALID_ARGUMENT);
    }

    const auto &smrb_resources = configuration.smrb();
    const auto &data_key = smrb_resources.data_key();
    const auto &weights_key = smrb_resources.weights_key();

    ska::pst::common::AsciiHeader config;
    config.set("DATA_KEY", smrb_resources.data_key());
    config.set("WEIGHTS_KEY", smrb_resources.weights_key());
    config.set("HB_NBUFS", smrb_resources.hb_nbufs());
    config.set("HB_BUFSZ", smrb_resources.hb_bufsz());
    config.set("DB_NBUFS", smrb_resources.db_nbufs());
    config.set("DB_BUFSZ", smrb_resources.db_bufsz());
    config.set("WB_NBUFS", smrb_resources.wb_nbufs());
    config.set("WB_BUFSZ", smrb_resources.wb_bufsz());

    return config;
}

void ska::pst::smrb::SmrbLmcServiceHandler::validate_beam_configuration(
    const ska::pst::lmc::BeamConfiguration &configuration)
{
    PSTLOG_TRACE(this, "validate_beam_configuration()");

    auto config = beam_configuration_as_ascii_header(configuration);
    PSTLOG_DEBUG(this, "validate_beam_configuration - config = {}", config.raw());
    ska::pst::common::ValidationContext context;
    smrb->validate_configure_beam(config, &context);
    context.throw_error_if_not_empty();
}

void ska::pst::smrb::SmrbLmcServiceHandler::configure_beam(
    const ska::pst::lmc::BeamConfiguration &request)
{
    PSTLOG_TRACE(this, "configure_beam()");

    if (is_beam_configured())
    {
        PSTLOG_WARN(this, "Received configure beam when beam configured already.");

        throw ska::pst::common::LmcServiceException(
            "SMRB already configured for beam.",
            ska::pst::lmc::ErrorCode::CONFIGURED_FOR_BEAM_ALREADY,
            grpc::StatusCode::FAILED_PRECONDITION);
    }

    auto config = beam_configuration_as_ascii_header(request);

    // validation will happens on the state model as first part of configure_beam
    // so no need to do validation here.
    smrb->configure_beam(config);

    PSTLOG_TRACE(this, "configure_beam done");
}

void ska::pst::smrb::SmrbLmcServiceHandler::deconfigure_beam()
{
    PSTLOG_TRACE(this, "deconfigure_beam()");
    if (!is_beam_configured())
    {
        PSTLOG_WARN(this, "Received deconfigure beam when no beam configured already.");

        throw ska::pst::common::LmcServiceException(
            "SMRB not configured for beam.",
            ska::pst::lmc::ErrorCode::NOT_CONFIGURED_FOR_BEAM,
            grpc::StatusCode::FAILED_PRECONDITION);
    }

    PSTLOG_TRACE(this, "deconfigure_beam() smrb->deconfigure_beam()");
    try
    {
        smrb->deconfigure_beam();
    }
    catch (const std::exception &e)
    {
        throw ska::pst::common::LmcServiceException(
            e.what(),
            ska::pst::lmc::ErrorCode::INTERNAL_ERROR,
            grpc::StatusCode::INTERNAL);
    }
    PSTLOG_TRACE(this, "deconfigure_beam() smrb->deconfigure_beam() done");
}

void ska::pst::smrb::SmrbLmcServiceHandler::get_beam_configuration(
    ska::pst::lmc::BeamConfiguration *response)
{
    PSTLOG_TRACE(this, "get_beam_configuration()");
    if (!is_beam_configured())
    {
        PSTLOG_WARN(this, "Received request to get beam configuration when no beam configured.");
        throw ska::pst::common::LmcServiceException(
            "SMRB not configured for beam.",
            ska::pst::lmc::ErrorCode::NOT_CONFIGURED_FOR_BEAM,
            grpc::StatusCode::FAILED_PRECONDITION);
    }

    ska::pst::lmc::SmrbBeamConfiguration *smrb_resources = response->mutable_smrb();
    smrb_resources->set_data_key(smrb->get_beam_configuration_header_key("DATA_KEY"));
    smrb_resources->set_weights_key(smrb->get_beam_configuration_header_key("WEIGHTS_KEY"));
    smrb_resources->set_hb_nbufs(smrb->get_beam_configuration_buffer_key("HB_NBUFS"));
    smrb_resources->set_hb_bufsz(smrb->get_beam_configuration_buffer_key("HB_BUFSZ"));
    smrb_resources->set_db_nbufs(smrb->get_beam_configuration_buffer_key("DB_NBUFS"));
    smrb_resources->set_db_bufsz(smrb->get_beam_configuration_buffer_key("DB_BUFSZ"));
    smrb_resources->set_wb_nbufs(smrb->get_beam_configuration_buffer_key("WB_NBUFS"));
    smrb_resources->set_wb_bufsz(smrb->get_beam_configuration_buffer_key("WB_BUFSZ"));
}

auto ska::pst::smrb::SmrbLmcServiceHandler::is_beam_configured() const noexcept -> bool
{
    PSTLOG_TRACE(this, "is_beam_configured()");
    return smrb->is_beam_configured();
}

void ska::pst::smrb::SmrbLmcServiceHandler::validate_scan_configuration(
    const ska::pst::lmc::ScanConfiguration&)
{
    PSTLOG_TRACE(this, "validate_scan_configuration()");
}

void ska::pst::smrb::SmrbLmcServiceHandler::configure_scan(
    const ska::pst::lmc::ScanConfiguration &configuration)
{
    PSTLOG_TRACE(this, "configure_beam()");
    if (!is_beam_configured())
    {
        PSTLOG_WARN(this, "Received scan configuration request when beam not configured.");

        throw ska::pst::common::LmcServiceException(
            "SMRB not configured for beam.",
            ska::pst::lmc::ErrorCode::NOT_CONFIGURED_FOR_BEAM,
            grpc::StatusCode::FAILED_PRECONDITION);
    }

    if (is_scan_configured())
    {
        PSTLOG_WARN(this, "Received configure_scan when scan already configured already.");

        throw ska::pst::common::LmcServiceException(
            "Scan already configured for SMRB.",
            ska::pst::lmc::ErrorCode::CONFIGURED_FOR_SCAN_ALREADY,
            grpc::StatusCode::FAILED_PRECONDITION);
    }

    if (!configuration.has_smrb())
    {
        PSTLOG_WARN(this, "Received scan configuration request no SMRB details provided.");

        throw ska::pst::common::LmcServiceException(
            "Expected an SMRB resources object, but none were provided.",
            ska::pst::lmc::ErrorCode::INVALID_REQUEST,
            grpc::StatusCode::INVALID_ARGUMENT);
    }

    try
    {
        // configure_scan for SMRB is a no-op step that just puts the
        // state into ScanConfigured.  There is no validation
        ska::pst::common::AsciiHeader config;
        smrb->configure_scan(config);
    }
    catch (const std::exception &e)
    {
        throw ska::pst::common::LmcServiceException(
            e.what(),
            ska::pst::lmc::ErrorCode::INTERNAL_ERROR,
            grpc::StatusCode::INTERNAL);
    }
}

void ska::pst::smrb::SmrbLmcServiceHandler::deconfigure_scan()
{
    PSTLOG_TRACE(this, "deconfigure_beam()");
    if (!is_scan_configured())
    {
        PSTLOG_WARN(this, "Received deconfigure_scan when not already configured.");

        throw ska::pst::common::LmcServiceException(
            "Not currently configured for SMRB.",
            ska::pst::lmc::ErrorCode::NOT_CONFIGURED_FOR_SCAN,
            grpc::StatusCode::FAILED_PRECONDITION);
    }

    try
    {
        smrb->deconfigure_scan();
    }
    catch (const std::exception &e)
    {
        throw ska::pst::common::LmcServiceException(
            e.what(),
            ska::pst::lmc::ErrorCode::INTERNAL_ERROR,
            grpc::StatusCode::INTERNAL);
    }
}

void ska::pst::smrb::SmrbLmcServiceHandler::get_scan_configuration(
    ska::pst::lmc::ScanConfiguration *configuration)
{
    PSTLOG_TRACE(this, "get_scan_configuration()");
    if (!is_scan_configured())
    {
        PSTLOG_WARN(this, "Received get_scan_configuration when scan not configured.");

        throw ska::pst::common::LmcServiceException(
            "Not currently configured for SMRB.",
            ska::pst::lmc::ErrorCode::NOT_CONFIGURED_FOR_SCAN,
            grpc::StatusCode::FAILED_PRECONDITION);
    }

    configuration->mutable_smrb();
}

auto ska::pst::smrb::SmrbLmcServiceHandler::is_scan_configured() const noexcept -> bool
{
    PSTLOG_TRACE(this, "is_scan_configured()");
    return smrb->is_scan_configured();
}

void ska::pst::smrb::SmrbLmcServiceHandler::start_scan(const ska::pst::lmc::StartScanRequest &request)
{
    PSTLOG_TRACE(this, "start_scan()");

    if (!is_scan_configured())
    {
        PSTLOG_WARN(this, "Received scan request when not already configured.");

        throw ska::pst::common::LmcServiceException(
            "Not currently configured for SMRB.",
            ska::pst::lmc::ErrorCode::NOT_CONFIGURED_FOR_SCAN,
            grpc::StatusCode::FAILED_PRECONDITION);
    }

    if (is_scanning())
    {
        PSTLOG_WARN(this, "Received scan request when already.");

        throw ska::pst::common::LmcServiceException(
            "SMRB is already scanning.",
            ska::pst::lmc::ErrorCode::ALREADY_SCANNING,
            grpc::StatusCode::FAILED_PRECONDITION);
    }

    try
    {
        ska::pst::common::AsciiHeader start_scan_config;
        start_scan_config.set("SCAN_ID", request.scan_id());
        smrb->start_scan(start_scan_config);
    }
    catch (const std::exception &e)
    {
        throw ska::pst::common::LmcServiceException(
            e.what(),
            ska::pst::lmc::ErrorCode::INTERNAL_ERROR,
            grpc::StatusCode::INTERNAL);
    }

    // Capture the number of buffers written/read of data and weights.
    // this is used in monitoring to calculate the delta for the whole
    // scan. This is because a ring buffer instance can be reused in
    // multiple scans.

    const auto &data_stats = smrb->get_data_data_stats();
    data_written = data_stats.written;
    data_read = data_stats.read;

    const auto &weights_stats = smrb->get_weights_data_stats();
    weights_written = weights_stats.written;
    weights_read = weights_stats.read;
}

void ska::pst::smrb::SmrbLmcServiceHandler::stop_scan()
{
    if (!is_scanning())
    {
        PSTLOG_WARN(this, "Received stop_scan request when not scanning.");

        throw ska::pst::common::LmcServiceException(
            "Received stop_scan request when SMRB is not scanning.",
            ska::pst::lmc::ErrorCode::NOT_SCANNING,
            grpc::StatusCode::FAILED_PRECONDITION);
    }

    try
    {
        smrb->stop_scan();
    }
    catch (const std::exception &e)
    {
        throw ska::pst::common::LmcServiceException(
            e.what(),
            ska::pst::lmc::ErrorCode::INTERNAL_ERROR,
            grpc::StatusCode::INTERNAL);
    }
}

auto ska::pst::smrb::SmrbLmcServiceHandler::is_scanning() const noexcept -> bool
{
    PSTLOG_TRACE(this, "is_scanning()");
    return smrb->is_scanning();
}

void ska::pst::smrb::SmrbLmcServiceHandler::reset()
{
    PSTLOG_INFO(this, "reset()");
    if (smrb->get_state() == ska::pst::common::State::RuntimeError)
    {
        smrb->reset();
    }
}

void ska::pst::smrb::SmrbLmcServiceHandler::restart()
{
    PSTLOG_INFO(this, "restart received restart request.");
    smrb->force_exit(ska::pst::common::ApplicationManager::restart_exit_code);
}

void ska::pst::smrb::SmrbLmcServiceHandler::get_monitor_data(
    ska::pst::lmc::MonitorData *data)
{
    PSTLOG_TRACE(this, "get_monitor_data()");
    if (!is_scanning())
    {
        PSTLOG_WARN(this, "Received get_monitor_data request when not scanning.");

        throw ska::pst::common::LmcServiceException(
            "Received get_monitor_data request when SMRB is not scanning.",
            ska::pst::lmc::ErrorCode::NOT_SCANNING,
            grpc::StatusCode::FAILED_PRECONDITION);
    }

    const auto &data_stats = smrb->get_data_data_stats();
    const auto &weights_stats = smrb->get_weights_data_stats();

    auto *smrb_resources = data->mutable_smrb();

    auto *smrb_data_stats = smrb_resources->mutable_data();
    smrb_data_stats->set_nbufs(data_stats.nbufs);
    smrb_data_stats->set_bufsz(data_stats.bufsz);
    smrb_data_stats->set_written(data_stats.written - data_written);
    smrb_data_stats->set_read(data_stats.read - data_read);
    smrb_data_stats->set_full(data_stats.full);
    smrb_data_stats->set_clear(data_stats.clear);
    smrb_data_stats->set_available(data_stats.available);

    auto *smrb_weights_stats = smrb_resources->mutable_weights();
    smrb_weights_stats->set_nbufs(weights_stats.nbufs);
    smrb_weights_stats->set_bufsz(weights_stats.bufsz);
    smrb_weights_stats->set_written(weights_stats.written - weights_written);
    smrb_weights_stats->set_read(weights_stats.read - weights_read);
    smrb_weights_stats->set_full(weights_stats.full);
    smrb_weights_stats->set_clear(weights_stats.clear);
    smrb_weights_stats->set_available(weights_stats.available);
}

void ska::pst::smrb::SmrbLmcServiceHandler::go_to_runtime_error(
    std::exception_ptr exc)
{
    PSTLOG_TRACE(this, "go_to_runtime_error()");
    smrb->go_to_runtime_error(std::move(exc));
}
