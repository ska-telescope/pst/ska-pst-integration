/*
 * Copyright 2024 Square Kilometre Array Observatory
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 * contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#include "ska_pst/common/config.h"
#include "ska_pst/common/definitions.h"
#include "ska_pst/common/lmc/LmcService.h"
#include "ska_pst/common/utils/AsciiHeader.h"
#include "ska_pst/common/utils/Logging.h"

#include "ska_pst/dsp/ft/FlowThroughManager.h"
#include "ska_pst/dsp/ft/lmc/FlowThroughLmcServiceHandler.h"

#include <csignal>
#include <cuda_runtime.h>
#include <iostream>
#include <thread>
#include <vector>
#include <unistd.h>

void usage();
auto check_cuda_id(const std::string& _device_id) -> int;
void signal_handler(int signal_value);
bool signal_received = false; // NOLINT

auto main(int argc, char *argv[]) -> int
{
  ska::pst::common::setup_spdlog();
  static constexpr int test_duration_ms = 100;

  std::string config_file{};

  std::string recording_path = "/tmp";

  bool use_o_direct = false;

  std::string service_name = "DSP.FT";
  int control_port = -1;
  int device_id = -1;

  int verbosity = 0;
  int dsp_verbosity = 0;

  opterr = 0;

  int c = 0;
  while ((c = getopt(argc, argv, "c:d:f:g:hovV")) != EOF)
  {
    switch (c)
    {
    case 'c':
      control_port = atoi(optarg);
      break;

    case 'd':
      recording_path = std::string(optarg);
      break;

    case 'f':
      config_file = std::string(optarg);
      break;

    case 'g':
      device_id = check_cuda_id(optarg);
      break;

    case 'h':
      usage();
      exit(EXIT_SUCCESS);
      break;

    case 'o':
      use_o_direct = true;
      break;

    case 'v':
      verbosity++;
      break;

   case 'V':
      dsp_verbosity++;
      break;

    default:
      std::cerr << "Unrecognised option [" << static_cast<char>(optopt) << "]" << std::endl;
      usage();
      return EXIT_FAILURE;
      break;
    }
  }

  if (verbosity > 0)
  {
    spdlog::set_level(spdlog::level::debug);
    if (verbosity > 1)
    {
      spdlog::set_level(spdlog::level::trace);
    }
  }

  // increase the libDSPSR verbosity level, separate to SPDLOG verbosity due to SKAO logging compliance
  ::dsp::Operation::verbose = (dsp_verbosity > 0);
  ::dsp::Observation::verbose = (dsp_verbosity > 1);

  PSTLOG_DEBUG(nullptr, "config_file={} control_port={}", config_file, control_port);

  // Check arguments
  int nargs = argc - optind;
  if (nargs != 0)
  {
    PSTLOG_ERROR(nullptr, "additional unexpected command line arguments detected");
    usage();
    return EXIT_FAILURE;
  }

  if (config_file.length() == 0 && control_port == -1)
  {
    PSTLOG_ERROR(nullptr, "require either a configuration file or control port");
    usage();
    return EXIT_FAILURE;
  }

  signal(SIGINT, signal_handler);
  signal(SIGTERM, signal_handler);

  try
  {
    std::shared_ptr<ska::pst::dsp::FlowThroughManager> dsp_flow_through = std::make_shared<ska::pst::dsp::FlowThroughManager>(recording_path, use_o_direct);

    if (device_id >= 0)
    {
      dsp_flow_through->assign_cuda_device(device_id);
    }

    std::unique_ptr<ska::pst::common::LmcService> lmc_service;
    if (control_port != -1)
    {
      PSTLOG_INFO(nullptr, "Setting up gRPC LMC service on port {}", control_port);
      auto lmc_handler = std::make_shared<ska::pst::dsp::FlowThroughLmcServiceHandler>(dsp_flow_through);
      lmc_service = std::make_unique<ska::pst::common::LmcService>(service_name, lmc_handler, control_port);
      lmc_service->start();
    }
    else
    {
      // performs configure_beam, configure_scan and start_scan using config_file
      dsp_flow_through->configure_from_file(config_file);

      dsp_flow_through->stop_scan();
      dsp_flow_through->deconfigure_scan();
      dsp_flow_through->deconfigure_beam();
    }

    if (lmc_service)
    {
      while (!signal_received)
      {
        usleep(ska::pst::common::microseconds_per_decisecond);
      }

      // signal the gRPC server to exit
      PSTLOG_INFO(nullptr, "Stopping gRPC LMC service");
      lmc_service->stop();
      PSTLOG_TRACE(nullptr, "gRPC LMC service has stopped");
    }

    dsp_flow_through->quit();
  }
  catch (std::exception &exc)
  {
    PSTLOG_ERROR(nullptr, "Exception caught: {}", exc.what());
    return EXIT_FAILURE;
  }

  return 0;
}

void usage()
{
  std::cout << "Usage: ska_pst_dsp_ft [options]" << std::endl;
  std::cout << "Execute DSP Flow Through from the configuration file or via control command." << std::endl;
  std::cout << "Quit with CTRL+C." << std::endl;
  std::cout << std::endl;
  std::cout << "  -c port     port on which to accept control commands" << std::endl;
  std::cout << "  -d path     write output files to the recording path [default /tmp]" << std::endl;
  std::cout << "  -f config   ascii file containing observation configuration" << std::endl;
  std::cout << "  -g gpu_id   CUDA capable GPU id" << std::endl;
  std::cout << "  -h          print this help text" << std::endl;
  std::cout << "  -o          use O_DIRECT for I/O transactions" << std::endl;
  std::cout << "  -v          increase verbosity" << std::endl;
  std::cout << "  -V          increase verbosity of libDSPSR" << std::endl;
}

auto check_cuda_id(const std::string& _device_id) -> int
{
  int cuda_device_id = -1;
  try
  {
    cuda_device_id = stoi(_device_id);
  }
  catch (const std::invalid_argument& e)
  {
    // Handle case where conversion fails (e.g., "abc" is passed)
    PSTLOG_ERROR(nullptr, "Invalid device ID input: {}. Input must be an integer.", _device_id);
    exit(EXIT_FAILURE);
  }
  catch (const std::out_of_range& e)
  {
    // Handle case where the input string is a valid integer but out of range
    PSTLOG_ERROR(nullptr, "Device ID input out of range: {}", _device_id);
    exit(EXIT_FAILURE);
  }

  // Get the number of available GPUs
  int deviceCount{0};
  cudaGetDeviceCount(&deviceCount);
  PSTLOG_INFO(nullptr, "deviceCount: {} cuda_device_id: {}", deviceCount, cuda_device_id);

  if (deviceCount == 0)
  {
    PSTLOG_WARN(nullptr, "No GPU devices found!");
  }

  if (cuda_device_id < -1 || cuda_device_id > deviceCount-1)
  {
    PSTLOG_ERROR(nullptr, "Invalid device ID");
    exit(EXIT_FAILURE);
  }

  return cuda_device_id;
}

void signal_handler(int signal_value)
{
  PSTLOG_INFO(nullptr, "received signal {}", signal_value);
  if (signal_received)
  {
    PSTLOG_WARN(nullptr, "received signal {} twice, exiting", signal_value);
    exit(EXIT_FAILURE);
  }
  signal_received = true;
}
