/*
 * Copyright 2022 Square Kilometre Array Observatory
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 * contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#include "ska_pst/common/utils/Logging.h"
#include "ska_pst/common/utils/RandomSequence.h"
#include "ska_pst/common/utils/FileReader.h"
#include "ska_pst/common/definitions.h"

#include <iostream>
#include <unistd.h>

void usage();

auto main(int argc, char *argv[]) -> int
{
  ska::pst::common::setup_spdlog();
  char verbose = 0;

  opterr = 0;

  int c = 0;

  while ((c = getopt(argc, argv, "hv")) != EOF)
  {
    switch (c)
    {
    case 'h':
      usage();
      exit(EXIT_SUCCESS);
      break;

    case 'v':
      verbose++;
      break;

    default:
      PSTLOG_ERROR(nullptr, "unrecognised option: -{}", static_cast<char>(optopt));
      usage();
      return EXIT_FAILURE;
      break;
    }
  }

  if (verbose > 0)
  {
    spdlog::set_level(spdlog::level::debug);
    if (verbose > 1)
    {
      spdlog::set_level(spdlog::level::trace);
    }
  }

  // Check arguments
  if ((argc - optind) != 1)
  {
    PSTLOG_ERROR(nullptr, "1 command line argument expected");
    usage();
    return EXIT_FAILURE;
  }

  std::string data_file(argv[optind]); // NOLINT
  int return_code = 0;

  try
  {
    ska::pst::common::FileReader file_reader(data_file);
    ska::pst::common::RandomSequence random_sequence;
    auto bytes_remaining = static_cast<ssize_t>(file_reader.get_file_size());

    auto bytes_read = file_reader.read_header();
    bytes_remaining -= bytes_read;
    PSTLOG_DEBUG(nullptr, "configuring random_sequence");
    random_sequence.configure(file_reader.get_header());

    static constexpr ssize_t buffer_size = 1048576;
    std::vector<char> buffer(buffer_size);

    uint64_t data_bytes_read = 0;
    bool data_valid = true;
    while (data_valid && bytes_remaining > 0)
    {
      ssize_t bytes_to_read = std::min(bytes_remaining, buffer_size);
      file_reader.read_data(&buffer[0], bytes_to_read);
      data_valid &= random_sequence.validate(reinterpret_cast<uint8_t *>(&buffer[0]), bytes_to_read);
      if (!data_valid)
      {
        PSTLOG_WARN(nullptr, "invalid data found in byte {}", data_bytes_read);
      }
      data_bytes_read += bytes_to_read;
      bytes_remaining -= bytes_to_read;
    }

    file_reader.close_file();
    PSTLOG_DEBUG(nullptr, "data_valid={}", data_valid);
  }
  catch (std::exception &exc)
  {
    PSTLOG_ERROR(nullptr, "Exception caught: {}", exc.what());
    return_code = 1;
  }

  PSTLOG_DEBUG(nullptr, "return return_code={}", return_code);
  return return_code;
}

void usage()
{
  std::cout << "Usage: ska_pst_dsp_disk_rand_validate [options] data_file" << std::endl;
  std::cout << std::endl;
  std::cout << "  data_file   raw data file that will be validated against the pseudo-random sequence" << std::endl;
  std::cout << "  -h          print this help text" << std::endl;
  std::cout << "  -v          verbose output" << std::endl;
}
