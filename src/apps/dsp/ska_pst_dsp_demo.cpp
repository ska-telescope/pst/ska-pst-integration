/*
 * Copyright 2024 Square Kilometre Array Observatory
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 * contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#include "ska_pst/common/utils/Logging.h"

#include <cuda_runtime.h>
#include <cassert>
#include <cstdlib>
#include <unistd.h>


#include "dsp/Detection.h"
#include "dsp/DetectionCUDA.h"
#include "dsp/TimeSeries.h"
#include "dsp/TransferCUDA.h"
#include "dsp/MemoryCUDA.h"
#include "Types.h"

void usage();

auto main(int argc, char *argv[]) -> int
try
{
  ska::pst::common::setup_spdlog();

  int return_code = EXIT_SUCCESS;

  char verbose = 0;

  opterr = 0;

  int c = 0;

  while ((c = getopt(argc, argv, "hv")) != EOF)
  {
    switch (c)
    {
    case 'h':
      usage();
      exit(EXIT_SUCCESS);
      break;

    case 'v':
      verbose++;
      break;

    default:
      PSTLOG_ERROR(nullptr, "unrecognised option: -{}", static_cast<char>(optopt));
      usage();
      return EXIT_FAILURE;
      break;
    }
  }

  if (verbose > 0)
  {
    spdlog::set_level(spdlog::level::debug);
    dsp::Operation::verbose = true;
    if (verbose > 1)
    {
      spdlog::set_level(spdlog::level::trace);
    }
  }

  auto *input = new dsp::TimeSeries;  // NOLINT
  auto *output = new dsp::TimeSeries; // NOLINT
  dsp::Detection detection_cpu;

  unsigned nchan{4}, npol{2}, ndim{2}, ndat{16384}; // NOLINT
  PSTLOG_INFO(nullptr, "Configuring input timeseries with nchan={} npol={} ndim={} ndat={}", nchan, npol, ndim, ndat);

  input->set_nchan(nchan);
  input->set_npol(npol);
  input->set_ndim(ndim);
  input->resize(ndat);
  input->set_state(Signal::Analytic);
  input->set_order(dsp::TimeSeries::OrderFPT);

  for (unsigned ichan=0; ichan<nchan; ichan++)
  {
    for (unsigned ipol=0; ipol<npol; ipol++)
    {
      float * ptr = input->get_datptr(ichan, ipol);
      for (unsigned idat=0; idat<ndat; idat++)
      {
        for (unsigned idim=0; idim<ndim; idim++)
        {
          ptr[idat * ndim + idim] = static_cast<float>(idat); // NOLINT
        }
      }
    }
  }

  detection_cpu.set_input(input);
  detection_cpu.set_output(output);
  detection_cpu.set_output_state(Signal::Intensity);
  detection_cpu.prepare();
  detection_cpu.operate();

  int gpu_device = 0;
  std::cerr << "Using GPU device " << gpu_device << std::endl;
  cudaSetDevice(gpu_device);

  cudaStream_t stream{nullptr};
  PSTLOG_DEBUG(nullptr, "Creating CUDA Stream");

  cudaError_t result = cudaStreamCreate(&stream);
  if (result != cudaSuccess)
  {
    PSTLOG_ERROR(nullptr, "cudaStreamCreate failed: {}", cudaGetErrorString(result));
    throw std::runtime_error("could not register host memory with the CUDA driver");
  }

  PSTLOG_INFO(nullptr, "Preparing timeseries and transformations");
  dsp::Detection detection_gpu;
  auto *input_gpu = new dsp::TimeSeries;  // NOLINT
  auto *output_gpu = new dsp::TimeSeries; // NOLINT

  auto *device_memory = new CUDA::DeviceMemory(stream, gpu_device); // NOLINT
  input_gpu->set_memory(device_memory);
  output_gpu->set_memory(device_memory);

  PSTLOG_INFO(nullptr, "Copy input timeseries from host to device");
  dsp::TransferCUDA h2d(stream);
  h2d.set_kind(cudaMemcpyHostToDevice);
  h2d.set_input(input);
  h2d.set_output(input_gpu);
  h2d.operate();

  PSTLOG_INFO(nullptr, "Performing CUDA Detection");
  detection_gpu.set_input(input_gpu);
  detection_gpu.set_output(output_gpu);
  detection_gpu.set_output_state(Signal::Intensity);
  detection_gpu.set_engine(new CUDA::DetectionEngine(stream)); // NOLINT

  detection_gpu.prepare();
  detection_gpu.operate();

  PSTLOG_INFO(nullptr, "Performing Device To Host Transfer");
  auto *output_cpu = new dsp::TimeSeries; // NOLINT
  dsp::TransferCUDA d2h(stream);
  d2h.set_kind(cudaMemcpyDeviceToHost);
  d2h.set_input(output_gpu);
  d2h.set_output(output_cpu);
  d2h.operate();

  PSTLOG_DEBUG(nullptr, "output nchans={},{}", output->get_nchan(), output_cpu->get_nchan());
  PSTLOG_DEBUG(nullptr, "output npols={},{}", output->get_npol(), output_cpu->get_npol());
  PSTLOG_DEBUG(nullptr, "output ndims={},{}", output->get_ndim(), output_cpu->get_ndim());
  PSTLOG_DEBUG(nullptr, "output ndats={},{}", output->get_ndat(), output_cpu->get_ndat());

  PSTLOG_INFO(nullptr, "Checking Computed Results");
  for (unsigned ichan=0; ichan<nchan; ichan++)
  {
    float * cpu = output->get_datptr(ichan, 0);
    float * gpu = output_cpu->get_datptr(ichan, 0);
    for (unsigned idat=0; idat<ndat; idat++)
    {
      // compute the square law (Re*Re + Im*Im) for both polarisations
      auto square_law_power = static_cast<float>(npol * ndim * idat * idat);
      assert(cpu[idat] == square_law_power); // NOLINT
      assert(gpu[idat] == square_law_power); // NOLINT
    }
  }

  PSTLOG_DEBUG(nullptr, "return return_code={}", return_code);
  return return_code;
}
catch (Error &exc)
{
  std::cerr << exc << std::endl;
  PSTLOG_DEBUG(nullptr, "return return_code={}", EXIT_FAILURE);
  return EXIT_FAILURE;
}

void usage()
{
  std::cout << "Usage: ska_pst_dsp_demo [options]" << std::endl;
  std::cout << std::endl;
  std::cout << "options:" << std::endl;
  std::cout << "  -h            print this help text" << std::endl;
  std::cout << "  -v            verbose output" << std::endl;
}
