/*
 * Copyright 2022 Square Kilometre Array Observatory
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 * contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#include "ska_pst/common/utils/Logging.h"
#include "ska_pst/common/definitions.h"
#include "ska_pst/recv/clients/UDPReceiver.h"
#include "ska_pst/recv/network/IBVQueue.h"
#include "ska_pst/recv/network/UDPSocketReceive.h"

#include <csignal>
#include <iostream>
#include <unistd.h>

void usage();
void signal_handler(int signal_value);
char quit_threads = 0; // NOLINT

auto main(int argc, char *argv[]) -> int
{
  ska::pst::common::setup_spdlog();
  int timeout = 0;

  char verbose = 0;

  bool exit_on_packet_receive_timeout = false;

  bool use_ibv = false;

  int udp_port = -1;

  opterr = 0;

  int c = 0;

  while ((c = getopt(argc, argv, "hiep:v")) != EOF)
  {
    switch(c)
    {
      case 'h':
        usage();
        exit(EXIT_SUCCESS);
        break;

      case 'i':
        use_ibv = true;
        break;

      case 'e':
        exit_on_packet_receive_timeout = true;
        break;

      case 'p':
        udp_port = atoi(optarg);
        break;

      case 'v':
        verbose++;
        break;

      default:
        PSTLOG_ERROR(nullptr, "unrecognised option: -{}", static_cast<char>(optopt));
        usage();
        return EXIT_FAILURE;
        break;
    }
  }

  int return_code = 0;

  if (verbose > 0)
  {
    spdlog::set_level(spdlog::level::debug);
    if (verbose > 1)
    {
      spdlog::set_level(spdlog::level::trace);
    }
  }

  // Check arguments
  if ((argc - optind) != 2)
  {
    PSTLOG_ERROR(nullptr, "2 command line arguments expected");
    usage();
    return EXIT_FAILURE;
  }

  signal(SIGINT, signal_handler);
  signal(SIGTERM, signal_handler);

  std::string recv_ipv4 = std::string(argv[optind]); // NOLINT
  std::string config_file(argv[optind+1]); // NOLINT
  std::shared_ptr<ska::pst::recv::SocketReceive> socket{nullptr};
  if (use_ibv)
  {
    socket = std::make_shared<ska::pst::recv::IBVQueue>();
  }
  else
  {
    socket = std::make_shared<ska::pst::recv::UDPSocketReceive>();
  }

  // construct a UDPReceiver using the socket
  ska::pst::recv::UDPReceiver udprecv(socket, recv_ipv4, udp_port);

  try
  {
    // prepare the config and header
    ska::pst::common::AsciiHeader config;

    // config file for this data stream
    PSTLOG_DEBUG(nullptr, "loading configuration from {}", config_file);
    config.load_from_file(config_file);
    ska::pst::common::AsciiHeader header(config);
    header.set_val("SOURCE", "J0437-4715");

    PSTLOG_DEBUG(nullptr, "Start Receiver::main");

    try
    {
      PSTLOG_DEBUG(nullptr, "Configuring Beam");
      udprecv.configure_beam(config);

      PSTLOG_DEBUG(nullptr, "Configuring Scan");
      udprecv.configure_scan(header);

      uint64_t scan_id{0};
      if(config.has("SCAN_ID"))
      {
        scan_id = config.get<uint64_t>("SCAN_ID");
      }
      else
      {
        scan_id = static_cast<uint64_t>(time(nullptr));
      }

      ska::pst::common::AsciiHeader startscan_config;
      startscan_config.set("SCAN_ID", scan_id);
      PSTLOG_DEBUG(nullptr, "Starting scan with generated SCAN_ID={}", scan_id);
      udprecv.start_scan(startscan_config);

      const ska::pst::recv::UDPStats& stats = udprecv.get_stats();
      bool persist = true;
      while (persist)
      {
        usleep(ska::pst::common::microseconds_per_decisecond);
        PSTLOG_TRACE(nullptr, "state={}", ska::pst::common::state_names[udprecv.get_state()]);
        if(exit_on_packet_receive_timeout)
        {
          persist &= stats.get_packet_receive_timeouts() == 0;
        }
        persist &= !quit_threads;
        PSTLOG_TRACE(nullptr, "persist={}", persist);
      }
      PSTLOG_DEBUG(nullptr, "Stopping scan");
      udprecv.stop_scan();

      PSTLOG_DEBUG(nullptr, "Deconfiguring Scan");
      udprecv.deconfigure_scan();

      // release beam configuration
      PSTLOG_DEBUG(nullptr, "Deconfiguring Beam");
      udprecv.deconfigure_beam();

      PSTLOG_DEBUG(nullptr, "stats.get_packet_receive_timeouts()={}", stats.get_packet_receive_timeouts());

    }
    catch(const std::exception& e)
    {
      PSTLOG_WARN(nullptr, "Error={}", e.what());
      udprecv.reset();
      return_code = 1;
    }
  }
  catch (std::exception& exc)
  {
    PSTLOG_ERROR(nullptr, "Exception caught: {}", exc.what());
    PSTLOG_DEBUG(nullptr, "udprecv::terminate()");
    udprecv.quit();
    return_code = 1;
  }

  PSTLOG_DEBUG(nullptr, "udprecv::stop_main return return_code={}", return_code);
  return return_code;
}

void usage()
{
  std::cout << "Usage: ska_pst_recv_udprecv [options] data_host config" << std::endl;
  std::cout << "Use UDP sockets to receive a data stream from ska_pst_recv_udpgen" << std::endl;
  std::cout << "and report the UDP capture statistics. Quit with CTRL+C." << std::endl;
  std::cout << std::endl;
  std::cout << "  data_host   network interface to receive incoming data" << std::endl;
  std::cout << "  config      ascii file containing observation configuration" << std::endl;
  std::cout << "  -h          print this help text" << std::endl;
  std::cout << "  -i          use Infiniband Verbs for socket receive [default UDP socket]" << std::endl;
  std::cout << "  -e          exit if packet receive timeout occurs" << std::endl;
  std::cout << "  -p port     Override the port on which to receive data [default DATA_PORT]" << std::endl;
  std::cout << "  -v          verbose output" << std::endl;
}

void signal_handler(int signalValue)
{
  PSTLOG_INFO(nullptr, "received signal {}", signalValue);
  if (quit_threads)
  {
    PSTLOG_WARN(nullptr, "received signal {} twice, exiting", signalValue);
    exit(EXIT_FAILURE);
  }
  quit_threads = 1;
}
