/*
 * Copyright 2022-2024 Square Kilometre Array Observatory
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 * contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#include "ska_pst/common/utils/Logging.h"
#include "ska_pst/common/definitions.h"
#include "ska_pst/common/lmc/LmcService.h"
#include "ska_pst/recv/clients/UDPReceiveDB.h"
#include "ska_pst/recv/lmc/RecvLmcServiceHandler.h"
#include "ska_pst/recv/network/IBVQueue.h"
#include "ska_pst/recv/network/UDPSocketReceive.h"
#include "ska_pst/recv/formats/PacketStructure.h"

#include <arpa/inet.h>
#include <csignal>
#include <iostream>
#include <net/if.h>
#include <netinet/in.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/ioctl.h>
#include <unistd.h>

void usage();
void signal_handler(int signal_value);
auto ipv4_from_network(std::string dev) -> std::string;
char quit_threads = 0; // NOLINT

auto main(int argc, char *argv[]) -> int
{
  ska::pst::common::setup_spdlog();
  std::string config_file{};

  int control_port = -1;

  int udp_port = -1;

  std::string service_name = "RECV";

  bool exit_on_packet_receive_timeout = false;

  char verbose = 0;

  bool use_ibv = false;

  int timeout = 0;

  opterr = 0;
  int c = 0;

  ska::pst::recv::PacketValidityFlagsPolicy validity_flags_policy{ska::pst::recv::PacketValidityFlagsPolicy::IgnoreAll};

  while ((c = getopt(argc, argv, "c:n:f:iehp:u:v")) != EOF)
  {
    switch(c)
    {
      case 'c':
        control_port = atoi(optarg);
        break;

      case 'n':
        service_name = std::string(optarg);
        break;

      case 'f':
        config_file = std::string(optarg);
        break;

      case 'i':
        use_ibv = true;
        break;

      case 'e':
        exit_on_packet_receive_timeout = true;
        break;

      case 'h':
        usage();
        exit(EXIT_SUCCESS);
        break;

      case 'p':
        udp_port = atoi(optarg);
        break;

      case 'u':
        validity_flags_policy = ska::pst::recv::get_validity_flags_policy(std::string(optarg));
        break;

      case 'v':
        verbose++;
        break;

      default:
        PSTLOG_ERROR(nullptr, "unrecognised option: -{}", static_cast<char>(optopt));
        usage();
        return EXIT_FAILURE;
        break;
    }
  }

  if (verbose > 0)
  {
    spdlog::set_level(spdlog::level::debug);
    if (verbose > 1)
    {
      spdlog::set_level(spdlog::level::trace);
    }
  }

  // Check arguments
  if ((argc - optind) != 1)
  {
    PSTLOG_ERROR(nullptr, "1 command line argument expected");
    usage();
    return EXIT_FAILURE;
  }

  // parse the argument as a network device name or an IPv4 address
  std::string data_host = std::string(argv[optind]); // NOLINT
  std::string recv_ipv4 = ipv4_from_network(data_host);

  PSTLOG_DEBUG(nullptr, "control_port={} data_host={} recv_ipv4={} config_file={}, udp_port={}, validity_flags_policy='{}'",
              control_port, data_host, recv_ipv4, config_file, udp_port, ska::pst::recv::packet_validity_flags_policy_names[validity_flags_policy]);

  // Check that a configuration file or control port are specified
  if ((control_port <= 0) and (config_file.length() == 0))
  {
    PSTLOG_ERROR(nullptr, "Either a control_port or config file must be provided");
    usage();
    return EXIT_FAILURE;
  }

  signal(SIGINT, signal_handler);
  signal(SIGTERM, signal_handler);

  std::shared_ptr<ska::pst::recv::SocketReceive> socket{nullptr};
  if (use_ibv)
  {
    socket = std::make_shared<ska::pst::recv::IBVQueue>();
  }
  else
  {
    socket = std::make_shared<ska::pst::recv::UDPSocketReceive>();
  }
  std::shared_ptr<ska::pst::recv::UDPReceiveDB> recv = std::make_shared<ska::pst::recv::UDPReceiveDB>(socket, recv_ipv4, udp_port);
  recv->set_validity_flags_policy(validity_flags_policy);

  std::shared_ptr<ska::pst::common::LmcService> grpc_control = nullptr;

  // start the main method
  PSTLOG_DEBUG(nullptr, "Start Receiver::main");
  bool lmc_control = (control_port > 0);

  int return_code = 0;
  try
  {
    if (lmc_control)
    {
      // The ControlledReceiver class implements or inherits the callbacks that will be used by
      // the LmcService:
      //   configure_beam(AsciiHeader)
      //   deconfigure_beam()
      //   configure_scan(AsciiHeader)
      //   deconfigure_beam()
      //   start_scan()
      //   end_scan(AsciiHeader)
      //
      //   The LmcService (or perhaps the upstream component manager) will be
      //   have the responsibility to convert JSON commands into AsciiHeader
      //   keywords as per the PST DDD interface specification.

      PSTLOG_INFO(nullptr, "Setting up gRPC controller on port {}", control_port);
      auto grpc_handler = std::make_shared<ska::pst::recv::RecvLmcServiceHandler>(recv);
      grpc_control = std::make_shared<ska::pst::common::LmcService>(service_name, grpc_handler, control_port);
      grpc_control->start();
    }
    else
    {
      // config file for this data stream
      ska::pst::common::AsciiHeader config;
      PSTLOG_DEBUG(nullptr, "loading configuration from {}", config_file);
      config.load_from_file(config_file);

      ska::pst::common::AsciiHeader header(config);
      if (!header.has("SOURCE"))
      {
        header.set_val("SOURCE", "J0437-4715");
      }

      try
      {
        PSTLOG_DEBUG(nullptr, "Configuring Beam");
        recv->configure_beam(config);
        PSTLOG_DEBUG(nullptr, "Configuring Scan");
        recv->configure_scan(header);
        ska::pst::common::AsciiHeader startscan_config;
        startscan_config.set("SCAN_ID", config.get<uint64_t>("SCAN_ID"));
        PSTLOG_DEBUG(nullptr, "Starting the scan with generated SCAN_ID={}", config.get<uint64_t>("SCAN_ID"));
        recv->start_scan(startscan_config);
      }
      catch(const std::exception& e)
      {
        PSTLOG_WARN(nullptr, "Error during configuration. Error={}", e.what());
        recv->reset();
        return_code = 1;
      }
    }

    bool persist = true;
    const ska::pst::recv::UDPStats & stats = recv->get_stats();
    while (persist && return_code == 0)
    {
      usleep(ska::pst::common::microseconds_per_decisecond);
      if (!lmc_control)
      {
        if(exit_on_packet_receive_timeout)
        {
          persist &= stats.get_packet_receive_timeouts() == 0;
        }
      }
      persist &= !quit_threads;

      PSTLOG_TRACE(nullptr, "state={}", recv->get_state());
    }

    if (grpc_control)
    {
      // signal the gRPC server to exit
      PSTLOG_INFO(nullptr, "Stopping gRPC controller");
      grpc_control->stop();
      PSTLOG_TRACE(nullptr, "gRPC controller has stopped");
    }
  }
  catch (std::exception& exc)
  {
    PSTLOG_ERROR(nullptr, "Exception caught: {}", exc.what());
    recv->reset();
    return_code = 1;
  }

  PSTLOG_DEBUG(nullptr, "Stopping Receiver::main");
  recv->quit();

  PSTLOG_DEBUG(nullptr, "returning {}", return_code);
  return return_code;
}

void usage()
{
  std::cout << "Usage: ska_pst_recv_udpdb [options] data_host" << std::endl;
  std::cout << "Use UDP sockets to receive a data stream that matches the format, writing the data" << std::endl;
  std::cout << "samples to PSRDADA ring buffers for the data (data_key) and weights (weights_key)." << std::endl;
  std::cout << "Quit with CTRL+C." << std::endl;
  std::cout << std::endl;
  std::cout << "  data_host      network device name or IP address on which to receive incoming data" << std::endl;
  std::cout << "  -c port        port on which to accept control commands" << std::endl;
  std::cout << "  -n name        service name [default is RECV]" << std::endl;
  std::cout << "  -f config      ascii file containing observation configuration" << std::endl;
  std::cout << "  -h             print this help text" << std::endl;
  std::cout << "  -i             use Infiniband Verbs for socket receive [default UDP socket]" << std::endl;
  std::cout << "  -e             exit if packet receive timeout occurs" << std::endl;
  std::cout << "  -p port        Override the port on which to receive data [default DATA_PORT]" << std::endl;
  std::cout << "  -u udp_policy  CBF UDP packet validity flags policy (see below) [default IgnoreAll]" << std::endl;
  std::cout << "  -v             verbose output" << std::endl;
  std::cout << std::endl;
  std::cout << "Packet validity policy: IgnoreAll, StationBeamDelayPolynomial, PSTBeamDelayPolynomials, JonesMatrices, AnyDelayPolynomials, All" << std::endl;
}

void signal_handler(int signalValue)
{
  PSTLOG_INFO(nullptr, "received signal {}", signalValue);
  if (quit_threads)
  {
    PSTLOG_WARN(nullptr, "received signal {} twice, exiting", signalValue);
    exit(EXIT_FAILURE);
  }
  quit_threads = 1;
}

auto ipv4_from_network(std::string dev) -> std::string
{
  struct ifreq ifc{};
  int res = 0;
  int sockfd = socket(AF_INET, SOCK_DGRAM, 0);

  if (sockfd < 0)
  {
    throw std::runtime_error("failed to create unbound socket");
  }

  // try to parse the dev as a network device name, if fails, assume it is an IPv4 address
  strncpy(ifc.ifr_name, dev.c_str(), IFNAMSIZ);
  res = ioctl(sockfd, SIOCGIFADDR, &ifc); // NOLINT
  ::close(sockfd);
  if (res < 0)
  {
    return dev;
  }

  static constexpr size_t max_ipv4_len = 16;
  char ipv4_addr[max_ipv4_len]; // NOLINT
  strncpy(ipv4_addr, inet_ntoa((reinterpret_cast<struct sockaddr_in*>(&ifc.ifr_addr)->sin_addr)), max_ipv4_len);
  return {ipv4_addr};
}
