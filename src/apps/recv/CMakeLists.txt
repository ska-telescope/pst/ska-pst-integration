include_directories(
  $<BUILD_INTERFACE:${PROJECT_SOURCE_DIR}/src>
)

add_executable(ska_pst_recv_info ska_pst_recv_info.cpp)
add_executable(ska_pst_recv_udpgen ska_pst_recv_udpgen.cpp)
add_executable(ska_pst_recv_udpdb ska_pst_recv_udpdb.cpp)
add_executable(ska_pst_recv_udprecv ska_pst_recv_udprecv.cpp)

target_link_libraries(ska_pst_recv_info PUBLIC ska-pst-recv)
target_link_libraries(ska_pst_recv_udprecv PUBLIC ska-pst-recv)
target_link_libraries(ska_pst_recv_udpdb PUBLIC ska-pst-recv)
target_link_libraries(ska_pst_recv_udpgen PUBLIC ska-pst-recv)

install (
  TARGETS
    ska_pst_recv_info
    ska_pst_recv_udpgen
    ska_pst_recv_udpdb
    ska_pst_recv_udprecv
  DESTINATION
    bin
)

add_test(NAME ska_pst_recv_info COMMAND ska_pst_recv_info)
add_test(NAME ska_pst_recv_udpgen COMMAND ska_pst_recv_udpgen "${CMAKE_CURRENT_LIST_DIR}/data/pst-beam1.txt" -t 1)
add_test(NAME ska_pst_recv_udpdb COMMAND ska_pst_recv_udpdb -h)
add_test(NAME ska_pst_recv_udprecv COMMAND ska_pst_recv_udprecv -h)
