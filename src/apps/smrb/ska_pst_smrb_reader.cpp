/*
 * Copyright 2022 Square Kilometre Array Observatory
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 * contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#include "ska_pst/smrb/DataBlockRead.h"
#include "ska_pst/common/utils/AsciiHeader.h"
#include "ska_pst/common/utils/Logging.h"
#include "ska_pst/common/utils/Time.h"
#include "ska_pst/common/utils/Timer.h"

#include <csignal>
#include <iostream>
#include <random>
#include <unistd.h>

void usage();
void signal_handler(int signal_value);
static bool sigint_received = false; // NOLINT

auto main(int argc, char *argv[]) -> int
{
  ska::pst::common::setup_spdlog();
  spdlog::set_level(spdlog::level::info);
  bool use_block_io = false;

  bool zero_copy = false;

  bool random = false;

  int verbose = 0;

  opterr = 0;

  int c = 0;

  while ((c = getopt(argc, argv, "bchtvz")) != EOF)
  {
    switch(c)
    {
      case 'b':
        use_block_io = true;
        break;

      case 'c':
        random = true;
        break;

      case 'h':
        usage();
        exit(EXIT_SUCCESS);
        break;

      case 'v':
        verbose++;
        break;

      case 'z':
        zero_copy = true;
        break;

      default:
        PSTLOG_ERROR(nullptr, "unrecognised option: -{}", static_cast<char>(optopt));
        usage();
        return EXIT_FAILURE;
        break;
    }
  }

  if (verbose > 0)
  {
    spdlog::set_level(spdlog::level::debug);
    if (verbose > 1)
    {
      spdlog::set_level(spdlog::level::trace);
    }
  }

  if (random)
  {
    zero_copy = false;
  }

  // Check arguments
  int nargs = argc - optind;
  if (nargs != 1)
  {
    PSTLOG_ERROR(nullptr, "1 command line argument expected");
    usage();
    return EXIT_FAILURE;
  }

  std::string config_file = std::string(argv[optind+0]); // NOLINT
  PSTLOG_DEBUG(nullptr, "config_file={}", config_file);

  signal(SIGINT, signal_handler);
  try
  {
    ska::pst::common::AsciiHeader config;
    config.load_from_file(config_file);

    std::string data_key = config.get_val(std::string("DATA_KEY"));
    std::string weights_key = config.get_val(std::string("WEIGHTS_KEY"));

    PSTLOG_INFO(nullptr, "creating readers for data and weights ring buffers");
    ska::pst::smrb::DataBlockRead db(data_key);
    ska::pst::smrb::DataBlockRead wb(weights_key);

    db.connect(0);
    wb.connect(0);

    db.lock();
    wb.lock();

    db.read_config();
    wb.read_config();

    db.read_header();
    wb.read_header();

    db.open();
    wb.open();

    uint64_t data_bufsz = db.get_data_bufsz();
    uint64_t weights_bufsz = wb.get_data_bufsz();

    std::vector<char> data_received(data_bufsz, 0);
    std::vector<char> weights_received(weights_bufsz, 0);

    std::vector<char> data(data_bufsz, 0);
    std::vector<char> weights(weights_bufsz, 0);

    if (random)
    {
      ska::pst::common::AsciiHeader data_header;
      data_header.load_from_str(db.get_header());
      std::string utc_start_str = data_header.get_val("UTC_START");
      ska::pst::common::Time utc_start(utc_start_str.c_str());
      time_t seed = utc_start.get_time();
      PSTLOG_DEBUG(nullptr, "Using random seed: {}", seed);

      // create Standard mersenne_twister_engine seeded with
      std::mt19937 gen(seed);
      static constexpr int integers_per_char = 256;
      std::uniform_int_distribution<unsigned int> distrib(0, integers_per_char);

      PSTLOG_DEBUG(nullptr, "Generating {} bytes of random data", data_bufsz);
      for (unsigned i=0; i<data_bufsz; i++)
      {
        data[i] = static_cast<char>(distrib(gen));
      }
      PSTLOG_DEBUG(nullptr, "Generating {} bytes of random weights", weights_bufsz);
      for (unsigned i=0; i<weights_bufsz; i++)
      {
        weights[i] = static_cast<char>(distrib(gen));
      }
    }

    ska::pst::common::Timer timer;
    uint64_t bytes_read = 0;

    if (use_block_io)
    {
      PSTLOG_DEBUG(nullptr, "reading blocks of data until end of data");
      bool eod = false;
      while (!eod)
      {
        char * data_buffer = db.open_block();
        char * weights_buffer = wb.open_block();

        if (data_buffer == nullptr)
        {
          eod = true;
        }
        else
        {
          uint64_t data_bytes_read = db.get_buf_bytes();
          if (!zero_copy)
          {
            memcpy(&data_received[0], data_buffer, data_bytes_read);
          }

          if (random)
          {
            for (unsigned i=0; i<data_bytes_read; i++)
            {
              if (data_received[i] != data[i])
              {
                throw std::runtime_error("random data sequence did not match expected");
              }
            }
          }

          db.close_block(data_bytes_read);
          bytes_read += data_bytes_read;
        }

        if (weights_buffer == nullptr)
        {
          eod = true;
        }
        else
        {
          uint64_t weights_bytes_read = wb.get_buf_bytes();
          if (!zero_copy)
          {
            memcpy(&weights_received[0], weights_buffer, weights_bytes_read);
          }
          for (unsigned i=0; i<weights_bytes_read; i++)
          {
            if (data_received[i] != data[i])
            {
              throw std::runtime_error("random data sequence did not match expected");
            }
          }
          wb.close_block(weights_bytes_read);
          bytes_read += weights_bytes_read;
        }
      }
    }
    else
    {
      PSTLOG_DEBUG(nullptr, "reading data until end of data");
      bool eod = false;
      while (!eod)
      {
        ssize_t data_bytes_read = db.read_data(&data_received[0], data.size());
        ssize_t weights_bytes_read = wb.read_data(&weights_received[0], weights.size());
        PSTLOG_DEBUG(nullptr, "Read {} data bytes and {} weights bytes", data_bytes_read, weights_bytes_read);

        if (random)
        {
          for (unsigned i=0; i<data_bytes_read; i++)
          {
            if (data_received[i] != data[i])
            {
              throw std::runtime_error("random data sequence did not match expected");
            }
          }
          for (unsigned i=0; i<weights_bytes_read; i++)
          {
            if (data_received[i] != data[i])
            {
              throw std::runtime_error("random data sequence did not match expected");
            }
          }
        }

        eod = db.check_eod() || wb.check_eod();
        bytes_read += (data_bytes_read + weights_bytes_read);
      }
    }
    timer.print_rates(bytes_read);

    db.close();
    wb.close();

    db.unlock();
    wb.unlock();

    db.disconnect();
    wb.disconnect();
  }
  catch (std::exception& exc)
  {
    PSTLOG_ERROR(nullptr, "Exception caught: {}", exc.what());
    return EXIT_FAILURE;
  }

  return EXIT_SUCCESS;
}

void usage()
{
  std::cout << "Usage: ska_pst_smrb_reader [options] config\n"
    "Reads from shared memory buffers for data and weights streams into local memory buffers.\n"
    "Quit with CTRL+C.\n"
    "\n"
    "  config           configuration file describing the ring buffers\n"
    "  -b               use block io methods (open_block/close_block)\n"
    "  -c               check the received data matches the random sequence seeded by the UTC_START\n"
    "  -h               print this help text\n"
    "  -z               do not simulate memory reading from the data blocks\n"
    "  -v               verbose output\n"
    << std::endl;
}

void signal_handler(int signalValue)
{
  PSTLOG_INFO(nullptr, "received signal {}", signalValue);
  if (sigint_received)
  {
    PSTLOG_WARN(nullptr, "received signal {} twice, exiting", signalValue);
    exit(EXIT_FAILURE);
  }
  sigint_received = true;
}
