
DATA_KEY        a000
WEIGHTS_KEY     a010
NUMA_NODE       0

# common header ring buffer defintion
HB_NBUFS        8
HB_BUFSZ        4096

# data ring buffer defintion [256 packets for MidPSTBand5]
DB_NBUFS        8
DB_BUFSZ        47738880

# weights ring buffer defintion [256 packets for MidPSTBand5]
WB_NBUFS        8
WB_BUFSZ        47738880